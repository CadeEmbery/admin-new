app.directive('volunteerSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel'
        },
        templateUrl: 'admin-volunteer-select/admin-volunteer-select.html',
        controller: 'VolunteerSelectController',
    };
});

app.controller('VolunteerSelectController', function($scope) {

    if (!$scope.model) {
        $scope.model = [];
    }

    $scope.proposed = {
        _new:{
            title: '',
            minimum: 1,
            maximum: 0,
            contacts: [],
            fromTeams: [],
        }
    }

    ///////////////////////////////////////////////////////////

    $scope.isFullfilled= function(slot) {
        if(!slot.minimum) {
            return true;
        }

        if(!slot.contacts) {
            return false;
        }

        if(slot.contacts.length >= slot.minimum) {
            return true;
        }
    }

    ///////////////////////////////////////////////////////////

    $scope.placesLeft = function(slot) {
        if(!slot.contacts) {
            return slot.minimum;
        }

        if(!slot.minimum) {
            slot.minimum = 0;
        }
        var res = slot.minimum - slot.contacts.length;
        if(res < 0) {
            return 0;
        } else {
            return res;
        }
    }
    ///////////////////////////////////////////////////////////

    $scope.getVolClassName = function(slot) {
        if(!slot.minimum) {
            return 'default';
        }

        if(!slot.contacts) {
            return 'warning';
        }

        if(slot.contacts.length >= slot.minimum) {
            return 'success';
        }

        return 'warning';


    }

    ///////////////////////////////////////////////////////////

    $scope.addSlot = function() {

        if (!$scope.proposed._new.title || !$scope.proposed._new.title.length) {
            //Do nothing without a title
            return;
        }

        //Proposed new
        var copy = angular.copy($scope.proposed._new);
        $scope.model.push(copy);

        $scope.proposed._new = {
            title: '',
            minimum: 1,
            maximum: 0,
            contacts: [],
            fromTeams: [],
        };
    }

    $scope.removeSlot = function(slot) {
        _.pull($scope.model, slot);
    }
});