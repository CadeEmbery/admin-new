app.directive('attendanceAreaInput', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'attendance-area-input/attendance-area-input.html',
        // controller: 'FilterSelectController',
        link: function($scope, $element, $attrs) {

            //Create a model if it doesn't exist
            if (!$scope.model) {
                $scope.model = [];
            }


            //Create a new object
            $scope._new = {}

            ////////////////////////////////////////////////////

            $scope.add = function() {
                var insert = angular.copy($scope._new);
                $scope.model.push(insert);
                $scope._new = {};

                // console.log('FOCUS BACK')
                $element.find('.focus-back').focus();

            }

            ////////////////////////////////////////////////////

            $scope.remove = function(entry) {
                _.pull($scope.model, entry);
            }

            ////////////////////////////////////////////////////

            $scope.contains = function(entry) {
                return _.contains($scope.model, entry);
            }
        }
    };
});


