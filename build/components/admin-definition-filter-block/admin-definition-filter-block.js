app.directive('definitionFilterBlock', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            source: '=ngSource',
            filterKey: '=ngFilterKey',
            type: '=type',
            enableDiscriminators:'=',
        },



        templateUrl: 'admin-definition-filter-block/admin-definition-filter-block.html',
        controller: 'DefinitionFilterBlock',
    };
});

///////////////////////////////////////////////////////

app.controller('DefinitionFilterBlock', function($scope, TypeService) {


    // //Get all the Capability Definitions
    // var definitions = _.reduce(TypeService.getSubTypes($scope.type.path), function(set, definition) {
    //     set[definition.definitionName] = definition;
    //     return set;
    // }, {});


    ///////////////////////////////


    $scope.isVisible = function() {
        return true;
    }
    ///////////////////////////////


    $scope.$watch('source', function(sourceItems) {

        //Get all the capabilities from our users
        $scope.definitions = _.chain(sourceItems)
        .map($scope.filterKey)
        .flatten()
        .compact()
        .uniq(function(entry) {
            // console.log('ENTRY', entry)
            return entry._id
        })
        .filter(function(realm) {
            if($scope.enableDiscriminators) {
                return true;
            }
                return !realm._discriminatorType;
           
        })
        .reduce(function(set, entry) {

            var entryDefinition = entry.definition || entry._discriminator || $scope.type.path;
            var existing = set[entryDefinition];

            if(!existing) {

                //Get or create a set
                // definitions[entryDefinition] || 
                var matchingSet = TypeService.dictionary[entryDefinition];

                if(!matchingSet) {
                    matchingSet = $scope.type;//{title:'Capability', plural:'Capabilities', definition:'entry'};
                }

                // console.log('MATCHED SET', entry.title, entryDefinition);

                set[entryDefinition] =
                existing = {
                    title:matchingSet.title,
                    plural:matchingSet.plural,
                    definition:matchingSet.definitionName,
                    items:[],
                }
            }

            //Add the option into the definition set
            entry.key = entry._id;
            existing.items.push(entry);

            return set;
        }, {})
        .values()
        .value()




    })

});
