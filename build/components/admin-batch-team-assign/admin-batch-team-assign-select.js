app.directive('batchTeamAssign', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            selection: '=ngModel',
            // type:'=ngType',
            // definition:'=ngDefinition',
        },
        templateUrl: 'admin-batch-team-assign/admin-batch-team-assign-select.html',
        controller: 'BatchTeamAssignController',
    };
});

app.controller('BatchTeamAssignController', function($scope, $state, Notifications, Batch, ModalService) {


    $scope.open = function() {


            var modelSource = {};
            var params = {
                allDefinitions:true,
            };

            ////////////////////////////////////////////////////

            ModalService.browse('team', modelSource, params).result.then(closed, closed);

            ////////////////////////////////////////////////////

            function done(err, data) {

                if (err) {
                    //console.log('DONE ERR', err)
                    return Notifications.error(err);
                }

                ///////////////////////

                //Reload the state
                $state.reload();

                ///////////////////////

                //Deselect all
                // $scope.selection.deselectAll();
                Notifications.status('Reassigned ' + data.success.length + ' cards');
            }

            ////////////////////////////////////////////////////

            function closed(result) {

                var cardIDs = $scope.selection.ids;
                var assignees = modelSource.items;

                //If we have people to assign to
                if(assignees && assignees.length) {


                        var array = [
                        {
                            title: 'Leave as is',
                            description: 'Won\'t change cards that are already assigned to someone.',
                        },
                        {
                            title: 'Auto Assign to a new team member',
                            description: 'Removes the existing assignee(s) and automatically assigns to a member of the selected team(s)',
                            behaviour:'automatic',
                        },
                        {
                            title: 'Wait for a team member to be assigned',
                            description: 'Removes the existing assignee(s) and waits for a team member to take it from the pool',
                            behaviour:'pool',
                        },
                        {
                            title: 'Assign to all team members',
                            description: 'Removes the existing assignee(s) and assigns to all team members of the selected team(s)',
                            behaviour:'all',
                        },
                        // {
                        //     title: `Don't add them`,
                        //     description: `Don't add them anymore`,
                        // }
                    ];

                    var message = 'If one of the cards you have selected is already assigned, what would you like us to do?'
                    //Check here how to reassign
                    return ModalService.actions(array, 'Reassign to team', message).then(function(action) {
                        

                        
                   



                        var details = {
                            ids: cardIDs,
                            teams:assignees, 
                            behaviour:action.behaviour,
                            // track: trackID, //$scope.selectedProcesses,
                        }

                        return Batch.assignToTeams(details, done);

                         });
                }
            }

        // var modalInstance = $uibModal.open({
        //     template: '<test-event-configure ng-model="test" close="closeModal" remove="removeTest"></test-event-configure>',
        //     size: 'md',
        //     controller:function($scope) {
        //         $scope.test = testEvent;
        //         $scope.isModal =true;
        //         $scope.closeModal = function() {
        //             console.log('close modal')
        //             $scope.$close();
        //         }

        //         $scope.removeTest = function() {
        //             $scope.$close();
        //             _.pull(tests, testEvent);
        //         }
        //     }
        // });
    }

    /**

    //////////////////////////////////////////////////////////

    //Get all the options
    FluroContent.resource('eventtrack').query({
        simple: true,
    }, function(res) {
        $scope.options = res;
    });

    //////////////////////////////////////////////////////////

    $scope.dynamicPopover = {
        templateUrl: 'admin-batch-assign/admin-batch-assign-popover.html',
    };

    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    

    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    $scope.setEventTrack = function(trackID) {

        var details = {
            ids: $scope.selection.ids,
            track: trackID, //$scope.selectedProcesses,
        }

        Batch.setEventTrack(details, done);
    }
    /**/

});