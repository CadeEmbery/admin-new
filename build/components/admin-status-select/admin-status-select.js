app.directive('statusSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            type:'=',
            callback:'=',
        },
        templateUrl: 'admin-status-select/admin-status-select.html',
        controller: 'StatusSelectController',
    };
});

app.controller('StatusSelectController', function($scope, $rootScope) {

    $scope.$watch('type', function(type) {
        switch(type) {
            case 'purchase':
                $scope.statusOptions = [
                    'active',
                    'cancelled',
                    'expired',
                ]
            break;
            case 'account':
                $scope.statusOptions = [
                    'active',
                    'suspended',
                    'cancelling',
                    'cancelled',
                    'onhold',
                    'exempt',
                    'trial',
                ]
            break;
            case 'contact':
                $scope.statusOptions = [
                    'active',
                    'draft',
                    'archived',
                   
                ]
            break;
            default:
                $scope.statusOptions = [
                    'active',
                    'draft',
                    'archived',
                    'template',
                ]
            break;
        }
    })
    
 
    $scope.$watch('model.status', function(status, oldStatus) {

        

        if(status == oldStatus) {
            return;
        }

        console.log('STATUS CHANGE', status, oldStatus);
        
        if($scope.callback) {
            $scope.callback($scope.model, status);
        }
    });

    if(!$scope.model.status) {
        $scope.model.status = 'active';
    }


	$scope.dynamicPopover = {
        templateUrl: 'admin-status-select/admin-status-popover.html',
	  };
});