app.directive('realmTreeManager', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            items: '=ngItems',
            search: '=ngSearch',
        },
        templateUrl: 'admin-realm-tree-manager/admin-realm-tree.html',
        controller: 'RealmTreeController',
    };
});


////////////////////////////////////////////////////

app.controller('RealmTreeController', function($scope, FluroStorage, $timeout, $state, $rootScope, FluroAccess, ModalService, Notifications, FluroContent, $sessionStorage) {



    $scope.server = {
        processing: true,
    }

    $scope.context = {};



    ///////////////////////////////////////////////

    // //console.log('RESTART TREE')
    FluroContent.endpoint('realm/tree', true, true).query().$promise.then(function(res) {

        $scope.model = _.chain(res)
        .filter(function(realm) {
            return !realm._discriminator;
        })
            .sortBy(function(realm) {
                return realm.title;
            })
            .value();

            //console.log('REALM TREE', res);
        $scope.server.processing = false;
    });


    ///////////////////////////////////////////////

    function moveRealm(moving, parent) {

        var updateData = {};
        var message;

        //////////////////////////////////////////////

        if (parent) {
            updateData = {
                realm: moving._id,
                parent: parent._id,
            }
            message = moving.title + ' under ' + parent.title;
        } else {

            updateData = {
                realm: moving._id,
                parent: 'top',
            }

            message = moving.title + ' under ' + $rootScope.user.account.title;
        }

        moving.processing = true;

        //////////////////////////////////////////////

        var promise = FluroContent.endpoint('realm/move').update(updateData).$promise;

        promise.then(function(res) {
            moving.processing = false;
            Notifications.status('Moved ' + message);
        }, function(err) {

            moving.processing = false;
            Notifications.error('Failed to move ' + message);
        });

        return promise;
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    $scope.treeOptions = {
        beforeDrop: function(event) {
            console.log('Dropped', event);

            if(event.source.index == -1) {
                return false;
            }

            $scope.context = {};

            var moving = event.source.nodeScope.$modelValue;
            var parent = event.dest.nodesScope.$parent.$modelValue;
            var previousParent = event.source.nodesScope.$parent.$modelValue;

            //////////////////////////////////////////////////
            var parentArray = event.dest.nodesScope.$modelValue;
            var previousParentArray = event.source.nodesScope.$modelValue;


            if(previousParent == parent) {
                console.log('NO CHANGE');
                return false;
            }

            /////////////////////////////////////////

            moveRealm(moving, parent).then(function(res) {
                if(parent) {
                    //All done successfully
                    // console.log('MOVED', moving.title, 'to', parent.title);
                    $scope.expand(parent);
                }
            }, function(err) {
                //We need to move it back
                // console.log('PULLBACK', err);
                _.pull(parentArray, moving);    
                previousParentArray.push(moving);
            })
        },
        accept: function(sourceNodeScope, destNodesScope, destIndex) {

            
            //Get the parent model
            var moving = sourceNodeScope.$modelValue;
            var parent = destNodesScope.$parent.$modelValue;
            

            if(!parent) {
                // console.log('MOVING', moving.title, 'to root');
                $scope.context.target = null;
                return true;
            }

            //Highlight the parent
            // console.log('MOVING', moving.title, 'to', parent.title)
            $scope.context.target = parent._id;

            return true;
        },
        dragStart:function(event) {
            $scope.context.dragging = true;
            $scope.context.target = null;
        },
        dragEnd:function(event) {
            $scope.context.dragging = false;
            $scope.context.target = null;
        }
        /**/
    };

    ///////////////////////////////////////////


    $scope.editRealm = function(realm) {
        console.log('EDIT REALM', realm);
        realm._type = 'realm';
        return ModalService.edit(realm);
    }





    $scope.canDelete = function(realm) {

        realm._type = 'realm';
        var allowed = FluroAccess.canDeleteItem(realm);

        // console.log('CHECK IF CAN EDIT REALM', realm, allowed);

        return allowed;
        // return FluroAccess.can('edit any', 'realm', 'realm');

    }


    $scope.canEdit = function(realm) {

        realm._type = 'realm';
        var allowed = FluroAccess.canEditItem(realm);

        // console.log('CHECK IF CAN EDIT REALM', realm, allowed);

        return allowed;
        // return FluroAccess.can('edit any', 'realm', 'realm');

    }

    $scope.canCreate = function() {
        return FluroAccess.can('create', 'realm', 'realm');
    }

    ///////////////////////////////////////////

    $scope.createChild = function(parent) {


        var realmID = parent ? parent._id || parent : null;


        console.log('CREATE CHILD', parent);


        ModalService.create('realm', {
            template:{
            trail:[realmID]
        }
        }, function(newRealm) {

            $state.reload();
            //console.log('CREATED', newRealm);
            // moveRealm(newRealm, parent).then(function(res) {

            //     //Reload the state
            //     $state.reload();
            // });
        })

    }

    ///////////////////////////////////////////

    var collapseData = FluroStorage.localStorage('realmtree');

    if(!collapseData.collapsed || !collapseData.collapsed.length) {
        collapseData.collapsed = [];
    }

    ///////////////////////////////////////////

    $scope.match = function(realm) {

        if($scope.context.target == realm._id) {
            return true;
        }

        if(!$scope.search || !$scope.search.terms || !$scope.search.terms.length) {
            return;
        }
        var str = realm.title.toLowerCase();
        var search = String($scope.search.terms).toLowerCase();

        if(str.indexOf(search) != -1) {
            return true;
        }
    }

    ///////////////////////////////////////////

    $scope.expandTree = function() {
        // //console.log('Expand All')
        collapseData.collapsed = [];
    }

    ///////////////////////////////////////////

    function getChildren(array) {
        return _.chain(array)
            .map(function(realm) {
                if (realm.children && realm.children.length) {
                    var children = getChildren(realm.children);
                    return [realm, children];
                } else {
                    return realm;
                }
            })
            .flattenDeep()
            .value();
    }

    ///////////////////////////////////////////

    $scope.collapseTree = function() {
        var flatRealms = getChildren($scope.model);
        collapseData.collapsed = _.map(flatRealms, '_id');
    }

    ///////////////////////////////////////////

    $scope.isCollapsed = function(realm) {

        if(!realm) {
            return;
        }

        if ($scope.search.terms && $scope.search.terms.length) {
            return false;
        }

        if(!realm.children.length) {
            return false;
        }

        var realmID = realm;
        if (realmID._id) {
            realmID = realmID._id;
        }

        return _.includes(collapseData.collapsed, realmID);

    }

    ///////////////////////////////////////////

    $scope.expand = function(realm) {

        if(!realm) {
            return;
        }
        var realmID = realm;
        if (realmID._id) {
            realmID = realmID._id;
        }

        _.pull(collapseData.collapsed, realmID);

    }

    ///////////////////////////////////////////

    $scope.toggleCollapse = function(realm) {

        if(!realm) {
            return;
        }

        var realmID = realm;
        if (realmID._id) {
            realmID = realmID._id;
        }

        if ($scope.isCollapsed(realm)) {
            _.pull(collapseData.collapsed, realmID);
        } else {
            collapseData.collapsed.push(realmID);
        }
    }


});