app.directive('taskList', function() {

    return {
        restrict: 'E',
        replace:true,
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            host: '=ngHost',
            definition: '=ngDefinition',
            showLink:'=',
        },
        templateUrl: 'admin-task-list/admin-task-list.html',
        controller: 'TaskListController',
    };
});




app.directive('contenteditable', ['$sce', function($sce) {
    return {
        restrict: 'A', // only activate on element attribute
        require: '?ngModel', // get a hold of NgModelController
        link: function(scope, element, attrs, ngModel) {
            if (!ngModel) return; // do nothing if no ng-model



            // Specify how UI should be updated
            ngModel.$render = function() {
// console.log('TESTING', ngModel)
                element.html($sce.getTrustedHtml(ngModel.$viewValue || ''));
            };

            // Listen for change events to enable binding
            element.on('blur keyup change', function() {
                scope.$evalAsync(read);
            });
            // read(); // initialize

            // Write data to the model
            function read() {

                console.log(ngModel.$modelValue);
                var html = element.html();
                // When we clear the content editable the browser leaves a <br> behind
                // If strip-br attribute is provided then we strip this out
                if (html == '<br>') {
                    html = '';
                }
                ngModel.$setViewValue(html);
            }
        }
    };
}]);




app.controller('TaskListController', function($scope, TypeService) {

    // console.log('MODEL', $scope.model, $scope.host);
    $scope._proposed = {};

    $scope.focus = {}

    ////////////////////////////////////////////////////

    //Create a model if it doesn't exist
    if (!$scope.model) {
        $scope.model = {
            tasks: []
        };
    }

    ////////////////////////////////////////////////////

    

    ////////////////////////////////////////////////////

    $scope.$watch('host.item', function(item) {

        var hostType = 'process';
        if(item && item._type) {
            hostType = item._type;
            $scope.postTypes = TypeService.getPostTypes(hostType);

        } else {
            $scope.postTypes = TypeService.getSubTypes('post');
        }



    })

    ////////////////////////////////////////////////////

    $scope.deselectFocus = function(task) {
        if($scope.focus.task == task) {
            $scope.focus.task =  null;
        }
    }

    $scope.getProgress = function() {
        var total = $scope.model.tasks.length;
        var completed = _.filter($scope.model.tasks, function(task) {
            // return task.complete;
            return task.status == 'complete';
        }).length;


        return Math.round(completed / total * 100);
    }

    ////////////////////////////////////////////////////

    $scope.add = function() {
        if (!$scope._proposed || !$scope._proposed.name || !$scope._proposed.name.length) {
            return;
        }

        var insert = angular.copy($scope._proposed);
        insert.required = true;

        var alreadyThere = _.some($scope.model.tasks, {
            name: insert.name,
        });

        if (alreadyThere) {
            return;
        }

        if (!$scope.model.tasks) {
            $scope.model.tasks = [];
        }

        $scope.model.tasks.push(insert);
        $scope._proposed = {};
    }


    ////////////////////////////////////////////////////

    $scope.toggleTask = function(task) {
        switch(task.status) {
            case 'complete':
                task.status = 'incomplete';
            break;
            default:
                task.status = 'complete';
            break;
        }
    }


    ////////////////////////////////////////////////////

    $scope.toggleTaskStatus = function(task) {
        switch(task.status) {
            case 'failed':
                task.status ='incomplete';
            break;
            case 'complete':
                task.status = 'pending';
            break;
            case 'pending':
                task.status = 'failed';
            break;
            default:
                task.status = 'complete';
            break;
        }
    }

    ////////////////////////////////////////////////////

    $scope.isComplete = function() {
        return _.every($scope.model.tasks, {status:'complete'});
    }

    $scope.countCompleted = function() {
        return _.filter($scope.model.tasks, {status:'complete'}).length;
    }

    ////////////////////////////////////////////////////

    $scope.remove = function(entry) {
        console.log('REMOVE')
        _.pull($scope.model.tasks, entry);
    }

    ////////////////////////////////////////////////////

    $scope.removeList = function() {
        if ($scope.host) {
            _.pull($scope.host.taskLists, $scope.model);
        }
    }
})