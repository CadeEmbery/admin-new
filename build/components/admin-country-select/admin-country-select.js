app.directive('countryCodeSelect', function(FluroContent) {
    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-country-select/admin-country-select.html',
        // controller: 'TimezoneSelectController',
        link:function($scope, element, attrs) {

            $scope.placeholder = 'Automatic (Use account default)';

            FluroContent.endpoint('system/countries')
            .query()
            .$promise
            .then(function(countries) {

                $scope.countries = _.chain(countries)
                .map(mapCountryCode)
                .compact()
                .sortBy(function(country) {
                    return country.name;
                })
                .value();


                $scope.popular = _.filter($scope.countries, function(country) {
                    switch(country.code) {
                        case 'AU':
                        case 'US':
                        case 'GB':
                            return true;
                        break;
                    }
                });

            }, function(err) {
                return;
            })

        }
    };
});

app.directive('countryNameSelect', function(FluroContent) {
    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-country-select/admin-country-select.html',
        // controller: 'TimezoneSelectController',
        link:function($scope, element, attrs) {

            $scope.placeholder = 'Select a country';

            FluroContent.endpoint('system/countries')
            .query()
            .$promise
            .then(function(countries) {
                


                 $scope.countries = _.chain(countries)
                .map(mapCountryName)
                .compact()
                .sortBy(function(country) {
                    return country.name;
                })
                .value();



                $scope.popular = _.filter($scope.countries, function(country) {
                    switch(country.code) {
                        case 'AU':
                        case 'US':
                        case 'GB':
                            return true;
                        break;
                    }
                });


            }, function(err) {
                return;
            })

        }
    };
});

///////////////////////////////////////////////////////

function mapCountryName(country) {

    return {
        name:country.name,
        value:country.name,
        code:country.alpha2,
    }
}

///////////////////////////////////////////////////////

function mapCountryCode(country) {
    if(!country.countryCallingCodes || !country.countryCallingCodes.length) {
        return;
    }

    var match = {
        name:country.name + ' - '+ country.alpha2 + ' (' + country.countryCallingCodes[0]+')',
        value:country.alpha2,
        code:country.alpha2,
    }


    return match;
}




