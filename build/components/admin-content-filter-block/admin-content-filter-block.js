app.directive('filterBlock', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            source: '=ngSource',
            getFilterKey: '&ngFilterKey',
            getFilterTitle: '&ngFilterTitle',
            persist: '@',
            filterType: "=ngFilterType",
            reword: "=ngReword",
            specificOptions: "=ngFilterOptions",
        },
        templateUrl: 'admin-content-filter-block/admin-content-filter-block.html',
        controller: 'ContentFilterBlock',
    };
});

////////////////////////////////////////////////////////////////////////

app.filter('toArray', function() {
    return function(obj, addKey) {
        if (!angular.isObject(obj)) return obj;
        if (addKey === false) {
            return Object.keys(obj).map(function(key) {
                return obj[key];
            });
        } else {
            return Object.keys(obj).map(function(key) {
                var value = obj[key];
                return angular.isObject(value) ?
                    Object.defineProperty(value, '$key', {
                        enumerable: false,
                        value: key
                    }) : {
                        $key: key,
                        $value: value
                    };
            });
        }
    };
});



var NULL_STRING = 'No value';
var BOOLEAN_TRUE_STRING = 'True';
var BOOLEAN_FALSE_STRING = 'False';

////////////////////////////////////////////////////////////////////////

function filterListReferences(items, fieldName, id) {

    if (id == BOOLEAN_TRUE_STRING) {
        id = true;
    }

    if (id == BOOLEAN_FALSE_STRING) {
        id = false;
    }

    // if(id == NULL_STRING) {
    //     id = false;
    // }


    // if (obj === undefined || obj === null) {
    //     addFilterOption(result, item, idField, 'no value');
    //     return result;
    // }

    //Find where item author is the id specified
    var results = _.filter(items, function(item) {

        var key = _.get(item, fieldName);

        if (id === false) {
            return (!key);
        }

        if (id === NULL_STRING) {
            return key === undefined || key === null;
        }

        if (key) {
            // console.log('FILTER KEY', key, id);

            if (_.isArray(key)) {

                var array = key;

                return _.some(array, function(e) {

                    //Check if it's an object
                    if (_.isObject(e)) {

                        //If it is with a fluro id
                        if (e._id) {
                            //Return if the id matches
                            return e._id == id;
                        }

                        //If it has an id from another system
                        if (e.id) {
                            //Return the match
                            return e.id == id;
                        }

                        //If it's an generic object literal
                        if (e.title) {
                            //Return the match
                            return e.title == id;
                        }

                        //If it's an generic object literal
                        if (e.name) {
                            //Return the match
                            return e.name == id;
                        }


                    } else {
                        return e == id;
                    }
                })
            } else {



                //Return if the fluro id matches, another id matches or the key matches the id
                return (key._id == id || key.id == id || key == id);
            }
        }
    });

    return results;



};


function startOfWeek(date) {
    var diff = date.getDate() - date.getDay() + (date.getDay() === 0 ? -6 : 1);
    var firstDay = new Date(date.setDate(diff));
    firstDay.setHours(0, 0, 0, 0);
    return firstDay
}

function startOfMonth(date) {
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    firstDay.setHours(0, 0, 0, 0);
    return firstDay
}

function startOfYear(date) {
    var firstDay = new Date(date.getFullYear(), 0, 1);
    firstDay.setHours(0, 0, 0, 0);
    return firstDay
}




////////////////////////////////////////////////////////////////////////

app.controller('ContentFilterBlock', function($scope, ContentFieldService, $timeout, $filter, Tools) {

    var filterKey = $scope.getFilterKey();
    var filterTitle = $scope.getFilterTitle();

    $scope.filterKey = filterKey;
    $scope.filterTitle = filterTitle;

    if (!$scope.model) {
        $scope.model = {};
    }

    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////

    $scope.range = {};

    // $scope.isRangedFilter = function() {
    switch ($scope.filterType) {
        case 'range':
        case 'daterange':
        case 'weekrange':
        case 'monthrange':
        case 'yearrange':
        case 'range':
            $scope.rangedFilter = true;
            break;
    }
    // }

    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////

    $scope.expandOptions = {};

    ///////////////////////////////////////////////////

    $scope.isCollapsible = function() {
        return $scope.count >= 5;
    }

    $scope.isExpanded = function() {
        return $scope.expandOptions.expanded || $scope.filtersUsed();
    }

    $scope.isCollapsed = function() {
        return !$scope.isExpanded() && $scope.isCollapsible();
        // ng-class="{expanded:(expanded || filtersUsed()), collapsible:(count > 5)}"
    }

    $scope.isVisible = function() {
        return $scope.expandOptions.expanded || $scope.filtersUsed() || $scope.count < 5;
    }

    ///////////////////////////////////////////////

    $scope.filterIsActive = function(value) {
        if (!$scope.model) {
            $scope.model = {};
        }

        // if(value === NULL_STRING) {
        //     value = undefined;
        //     // console.log('TEST', filterKey, value, $scope.model[filterKey])
        //     // return _.includes($scope.model[filterKey], null, undefined) || $scope.model[filterKey] == [];
        // }

        return _.includes($scope.model[filterKey], value);
    }

    ///////////////////////////////////////////////

    $scope.filtersUsed = function() {
        if (!$scope.model) {
            $scope.model = {};
        }
        return ($scope.model[filterKey] && $scope.model[filterKey].length)
    }

    ///////////////////////////////////////////////

    $scope.toggleFilter = function(value) {
        if (!$scope.model) {
            $scope.model = {};
        }


        var active = _.includes($scope.model[filterKey], value);

        if ($scope.persist) {

            if (active) {
                //Always select
                $scope.model[filterKey] = [];
            } else {
                //Always select
                $scope.model[filterKey] = [value];
            }

        } else {

            if (active) {
                $scope.removeFilter(value);
            } else {
                $scope.addFilter(value);
            }
        }
    }

    ///////////////////////////////////////////////

    $scope.removeFilter = function(value) {
        if (!$scope.model) {
            $scope.model = {};
        }

        //console.log('remove filter', key)
        if (!value) {
            delete $scope.model[filterKey];
        } else {
            _.pull($scope.model[filterKey], value);
        }
    }

    ///////////////////////////////////////////////

    $scope.addFilter = function(value) {
        if (!$scope.model) {
            $scope.model = {};
        }

        if (!$scope.model[filterKey]) {
            $scope.model[filterKey] = [];
        }

        if (!_.contains($scope.model[filterKey], value)) {
            $scope.model[filterKey].push(value);
        }
    }

    ///////////////////////////////////////////////

    function addFilterOption(result, item, field_key, source) {


        if (!source) {
            return;
        }

        //////////////////////////////////
        switch ($scope.filterType) {
            case 'range':
                var n = Number(source);
                if (!isNaN(n)) {
                    $scope.rangeValues.push(n);
                }
                break;
            case 'daterange':


                var date = new Date(source);
                if (date instanceof Date && !isNaN(date)) {
                    date.setHours(0, 0, 0, 0);
                    $scope.rangeValues.push(date);
                    // console.log('PUSH DATE OPTION', date);
                }
                break;
            case 'weekrange':


                var date = new Date(source);
                if (date instanceof Date && !isNaN(date)) {
                    var week = startOfWeek(date);
                    $scope.rangeValues.push(week);
                }
                break;

            case 'monthrange':


                var date = new Date(source);
                if (date instanceof Date && !isNaN(date)) {
                    var start = startOfMonth(date);
                    $scope.rangeValues.push(start);
                }
                break;

            case 'yearrange':


                var date = new Date(source);
                if (date instanceof Date && !isNaN(date)) {
                    var start = startOfYear(date);
                    $scope.rangeValues.push(start);
                }
                break;
        }

        //////////////////////////////////

        var key = source[field_key];
        var title;


        if (_.isString(source)) {
            title = key = source;
        } else {

            //If it's an object with keys
            //try and use either it's title or the name

            if (_.isObject(source)) {

                if (source._id) {
                    key = source._id;
                } else if (source.id) {
                    key = source.id;
                }

                if (source.title) {
                    title = source.title;
                } else if (source.name) {
                    title = source.name;
                } else {
                    title = String(source);
                }
            } else {
                title = key = String(source);
            }


        }

        if ($scope.reword) {
            title = $scope.reword(title);
        }

        if (!result[key]) {
            result[key] = {
                title: title,
                key: key,
                //items: [item],
                count: 1,
            }
        } else {
            //result[key].items.push(item);
            result[key].count++;
        }

        if (source.bgColor) {
            result[key].color = source.bgColor;
        }

        if (source.bgColor) {
            result[key].color = source.bgColor;
        }
    }


    switch (filterKey) {
        case 'managedOwners':
        case 'managedAuthor':
            // console.log('AVATAR STYLE', filterKey)
            $scope.avatarStyle = 'persona';
            break;
        case 'owners':
        case 'author':
            // console.log('AVATAR STYLE', filterKey)
            $scope.avatarStyle = 'user';
            break;
        case 'assignedTo':
            // console.log('AVATAR STYLE', filterKey)
            $scope.avatarStyle = 'contact';
            break;
    }

    ///////////////////////////////////////////////

    $scope.$watch('source', sourceItemsChanged);
    // $scope.$watch('source', debounceChanged);

   

    ///////////////////////////////////////////////

    var debouncer;


    function debounceChanged() {
        if (debouncer) {
            $timeout.cancel(debouncer);
        }

        debouncer = $timeout(function() {
            // console.log('filter block update', $scope.filterKey)
            // //console.log('update keywords', searchData)
            // $scope.updateFilters();
            sourceItemsChanged();
        }, ContentFieldService.filterDebounce);
    }

    ///////////////////////////////////////////////
    function sourceItemsChanged() {

        var items = $scope.source;

        switch ($scope.filterType) {
            case 'daterange':
            case 'weekrange':
            case 'yearrange':
            case 'monthrange':
            case 'range':
                console.log('Reset range values', $scope.filterKey);
                $scope.rangeValues = [];
                break;
        }


        ///////////////////////////////////////////////

        var filterOptions = _.chain(items)
            // .filter(function(item) {
            //     var obj = _.get(item, filterKey);



            //     if (obj === undefined || obj === null) {
            //         return;
            //     } else {
            //         return true;
            //     }
            // })
            .reduce(function(result, item, key) {


                var obj = _.get(item, filterKey);

                if (obj === undefined || obj === null) {
                    addFilterOption(result, item, idField, NULL_STRING);
                    return result;
                }

                if (!obj) {
                    addFilterOption(result, item, idField, BOOLEAN_FALSE_STRING);
                    return result;
                }

                if (obj === true) {
                    addFilterOption(result, item, idField, BOOLEAN_TRUE_STRING);
                    return result;
                }

                //////////////////////////////////

                if (_.isArray(obj)) {
                    _.each(obj, function(sub) {

                        if (!sub) {
                            return;
                        }

                        //For filtering objects usually
                        //not from Fluro (they don't have an _id)
                        var idField;
                        if (sub._id) {
                            idField = '_id';
                        } else if (sub.id) {
                            idField = 'id';
                        } else if (sub.title) {
                            idField = 'title';
                        } else if (sub.name) {
                            idField = 'name';
                        }


                        addFilterOption(result, item, idField, sub)
                    });

                } else {

                    if (_.isString(obj)) {
                        addFilterOption(result, item, filterKey, obj);
                    } else {


                        if (_.isObject(obj)) {

                            //For filtering objects usually
                            //not from Fluro (they don't have an _id)
                            var idField;

                            // console.log('ITEM', item);

                            if (obj._id) {
                                idField = '_id';
                            } else if (obj.id) {
                                idField = 'id';
                            } else if (obj.title) {
                                idField = 'title';
                            } else if (obj.name) {
                                idField = 'name';
                            }

                            addFilterOption(result, item, idField, obj);


                        } else {



                            if (item) {
                                //For filtering objects usually
                                //not from Fluro (they don't have an _id)
                                var idField;
                                if (item._id) {
                                    idField = '_id';
                                } else if (sub.id) {
                                    idField = 'id';
                                } else if (sub.title) {
                                    idField = 'title';
                                } else if (sub.name) {
                                    idField = 'name';
                                }



                                addFilterOption(result, item, idField, obj);
                            }
                        }
                    }
                }


                return result;

            }, {})
            .values()
            .sortBy(function(option) {
                if (option.key == NULL_STRING) {
                    return '0';
                } else {
                    return option.title;
                }
            })
            .value();

        ///////////////////////////////////////////////

        if (!$scope.filtersUsed()) {
            if (filterOptions.length == 1) {

                var option = filterOptions[0];


                //If it's a no value set
                if (option && option.key == NULL_STRING) {
                    filterOptions = [];
                }

                //If it's a no value set
                if (option && option.count == items.length) {
                    filterOptions = [];
                }
            }
        }

        ///////////////////////////////////////////////

        if ($scope.specificOptions && $scope.specificOptions.length) {
            $scope.filterOptions = _.filter(filterOptions, function(opt) {
                return _.some($scope.specificOptions, {
                    key: opt.key
                });
            });
        } else {
            $scope.filterOptions = filterOptions;
        }

        ///////////////////////////////////////////////////

        $scope.count = _.keys($scope.filterOptions).length;

        ///////////////////////////////////////////////////

        switch ($scope.filterType) {
            case 'daterange':
            case 'weekrange':
            case 'yearrange':
            case 'monthrange':

                var labelFormat = 'j M Y';
                var labelPrefix = '';
                var labelSuffix = '';
                switch ($scope.filterType) {
                    case 'monthrange':
                        labelFormat = 'F Y';
                        break;
                    case 'yearrange':
                        labelFormat = 'Y';
                        break;
                    case 'weekrange':
                        labelPrefix = 'Week ';
                        labelFormat = 'W Y';
                        break;

                }


                //Uniquify the values
                $scope.rangeValues = _.chain($scope.rangeValues)
                    .uniq(function(date) {
                        return date.format(labelFormat);
                    })
                    .sortBy(function(date) {
                        return date.getTime();
                    })
                    .value();

                //////////////////////////////

                if (!$scope.rangeValues || !$scope.rangeValues.length) {
                    delete $scope.range;
                } else {

                    if (!$scope.range) {
                        $scope.range = {}
                    }

                    var minimum = _.first($scope.rangeValues);
                    var maximum = _.last($scope.rangeValues);
                    $scope.range.minimum = minimum;
                    $scope.range.maximum = maximum;
                    $scope.range.from = minimum;
                    $scope.range.to = maximum;



                    $scope.range.steps = _.map($scope.rangeValues, function(date) {
                        return {
                            label: labelPrefix + date.format(labelFormat) + labelSuffix,
                            value: date.getTime(),
                        }
                    });
                }


                break;
            case 'range':

                // console.log('UPDATE THE RANGE VALUES', $scope.filterKey);
                //Uniquify the values
                $scope.rangeValues = _.chain($scope.rangeValues)
                    .sortBy(function(value) {
                        return value;
                    })
                    .value();

                //////////////////////////////

                if (!$scope.rangeValues || !$scope.rangeValues.length) {
                    delete $scope.range;
                } else {
                    if (!$scope.range) {
                        $scope.range = {}
                    }
                    var minimum = _.min($scope.rangeValues);
                    var maximum = _.max($scope.rangeValues);
                    $scope.range.minimum = minimum;
                    $scope.range.maximum = maximum;
                    $scope.range.from = minimum;
                    $scope.range.to = maximum;

                    //Get the difference
                    var difference = maximum - minimum;

                    // var stepMax = difference;
                    // if(difference > 180) {
                    stepMax = Math.min(1, parseInt(difference / 180));
                    // }

                    var steps = _.range(minimum, maximum)

                    if ($scope.rangeValues.length > steps.length) {
                        $scope.range.steps = _.uniq(steps);
                    } else {
                        $scope.range.steps = _.uniq($scope.rangeValues);
                    }


                }
                break;
        }


    }


    // console.log('FILTER TYPE', $scope.filterType);

    // ///////////////////////////////////////////////

    // $scope.$watch('range.to + range.from', function() {
    //     if($scope.filterType == 'range'){
    //         if($scope.range) {
    //             console.log('CHANGES');
    //             _.set($scope.model, $scope.filterKey + '.from', $scope.range.from);
    //             _.set($scope.model, $scope.filterKey + '.to', $scope.range.to);
    //         }
    //     }
    // })


});