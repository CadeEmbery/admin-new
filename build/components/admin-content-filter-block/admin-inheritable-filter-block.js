app.directive('inheritableFilterBlock', function() {

    return {
        restrict: 'E',
        replace: true,
        // scope: {
        //     model: '=ngModel',
        //     source: '=ngSource',
        //     getFilterKey: '&ngFilterKey',
        //     getFilterTitle: '&ngFilterTitle',
        //     persist: '@',
        // },
        templateUrl: 'admin-content-filter-block/admin-inheritable-filter-block.html',
        controller: 'InheritableFilterBlock',
    };
});



app.controller('InheritableFilterBlock', function($scope, $state, CacheManager, $stateParams) {
    ///////////////////////////////////////////////

    $scope.blockSettings = {
        showInherited: $stateParams.searchInheritable,
        searchArchived: $stateParams.searchArchived,
    }


    $scope.inheritableType = false;
    switch ($scope.type.path) {
        case 'application':
        case 'article':
        case 'asset':
        case 'audio':
        case 'image':
        case 'code':
        case 'component':
        case 'definition':
        case 'integration':
        case 'query':
        case 'role':
        case 'video':
        case 'reaction':
            $scope.inheritableType = true;
            break;
    }

    ///////////////////////////////////////////////

    var definitionName = _.get($scope, 'definition.definitionName');

    if(definitionName == 'report') {
        $scope.inheritableType = false;
    }
    
    ///////////////////////////////////////////////

    $scope.toggleInherited = function() {
        console.log('Toggle');

        var extended = angular.copy($stateParams);

        if (!extended.searchInheritable || extended.searchInheritable == 'false') {
            extended.searchInheritable = true;
        } else {
            extended.searchInheritable = null;
        }

        ////////////////////////////////////////////

        CacheManager.clear($scope.type.path);
        if ($scope.definition) {
            CacheManager.clear($scope.definition.definitionName);
        }

        ////////////////////////////////////////////

        console.log('Switch From', $stateParams, extended);
        return $state.go($state.current, extended, {
            reload: true
        });
    }


    
});