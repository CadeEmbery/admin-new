app.directive('contactEngagement', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            period: '@',
        },
        templateUrl: 'contact-engagement/contact-engagement.html',
        controller: 'ContactEngagementController',
    };
});

app.controller('ContactEngagementController', function($scope, FluroContent) {


    $scope.status = {}

    // $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
    //   $scope.series = ['Series A', 'Series B'];

    // $scope.data = [
    //   [65, 59, 80, 81, 56, 55, 40],
    //   [28, 48, 40, 19, 86, 27, 90]
    // ];

    $scope.onClick = function(points, evt) {
        //console.log(points, evt);
    };


    // $scope.datasetOverride = [{
    //     yAxisID: 'y-axis-1'
    // }, {
    //     yAxisID: 'y-axis-2'
    // }];


    // $scope.options = {
    //     scales: {
    //         yAxes: [{
    //             id: 'y-axis-1',
    //             type: 'linear',
    //             display: true,
    //             position: 'left'
    //         }, {
    //             id: 'y-axis-2',
    //             type: 'linear',
    //             display: true,
    //             position: 'right'
    //         }]
    //     }
    // };


    // $scope.datasetOverride = [
    //   {
    //     // label: "Bar chart",
    //     borderWidth: 1,
    //     type: 'bar'
    //   },
    //   {
    //     // label: "Line chart",
    //     borderWidth: 3,
    //     hoverBackgroundColor: "rgba(255,99,132,0.4)",
    //     hoverBorderColor: "rgba(255,99,132,1)",
    //     type: 'line'
    //   }
    // ];

    //////////////////////////////////////////////

    var today = new Date();
    today.setHours(23, 59, 0, 0);

    //////////////////////////////////////////////

    var dateStringFormat = 'Y-W';

    //////////////////////////////////////////////

    $scope.labels = [];

    var periods;

    //////////////////////////////////////////////

    switch ($scope.period) {
        case 'day':
            dateStringFormat = 'Y-m-d';
            periods = _.reduce(_.range(90), function(set, i) {
                var days = 1 * 86400 * 1000;
                var date = new Date(today.getTime() - (i * days))
                var dateString = date.format(dateStringFormat);
                
                $scope.labels.push(date.format('D j M Y'));
                // $scope.labels.push(dateString);

                set[dateString] = {
                    label: dateString,
                    data: [],
                }

                return set;
            }, {});
            break;
        case 'month':
            dateStringFormat = 'Y-m';
            periods = _.reduce(_.range(24), function(set, i) {
                var days = 31 * 86400 * 1000;
                var date = new Date(today.getTime() - (i * days))
                var dateString = date.format(dateStringFormat);
                
                $scope.labels.push(date.format('M Y'));
                // $scope.labels.push(dateString);

                set[dateString] = {
                    label: dateString,
                    data: [],
                }

                return set;
            }, {});
            break;
        default:

            periods = _.reduce(_.range(12), function(set, i) {
                var days = 7 * 86400 * 1000;
                var date = new Date(today.getTime() - (i * days))
                var dateString = date.format(dateStringFormat);
                
                $scope.labels.push('Week ' + date.format('W Y'));
                // $scope.labels.push(dateString);

                set[dateString] = {
                    label: dateString,
                    data: [],
                }

                return set;
            }, {});
            break;
    }




    //////////////////////////////////////////////

    //Get all the labels

    $scope.colors = [
        '#0bd6be', //Cyan
        '#670bd6', //Purple
        '#4bd60b', //Green
    ]

    //////////////////////////////////////////////

    console.log('PERIOD', $scope.period);


    FluroContent.endpoint('contact/' + $scope.model._id + '/stats').query({
        period:$scope.period
    }).$promise.then(function(res) {
        // $scope.stats = res;
        // 
        // 

        var allKeys = [
            'checkin',
            'interaction',
            'assignmentConfirmed',
            // 'assignmentDenied',
        ]
        // var allKeys = _.chain(res)
        //     .map('entries')
        //     .flatten()
        //     .map('key')
        //     .flatten()
        //     .uniq()
        //     .value();


        $scope.series = allKeys.map(function(input) {
            switch(input) {
                case 'checkin':
                    return 'Checked in';
                break;
                case 'interaction':
                    return 'Form Submitted';
                break;
                case 'assignmentConfirmed':
                    return 'Served';
                break;
            }
        });
        
        $scope.labels.reverse();
        
        $scope.data = _.map(allKeys, function(seriesKey) {

            return _.chain(periods)
            .map(function(existingWeek, weekKey) {
                var match = _.find(res, {
                    _id: weekKey
                });

                if (!match) {
                    return 0;
                }

                var matchSet = _.find(match.entries, {
                    key: seriesKey
                });

                if (!matchSet) {
                    return 0;
                }

                return matchSet.count || 0;

            })
            // .map(function(val) {
            //     switch(seriesKey) {
            //         // case 'assignmentConfirmed':
            //         //     return val *3;
            //         // break;
            //         case 'assignmentConfirmed':
            //             return val *2;
            //         break;
            //         case 'interaction':
            //             return val *1.5;
            //         break;
            //         default:
            //             return val;
            //         break;
            //     }
            // })
            .value()
            .reverse()
        })


        $scope.status.loaded = true;

        // //////////////////////////////////////////

        // // $scope.series = allKeys;
        // // $scope.data = _.map(master, 'data');
        // // $scope.labels = _.map(master, 'label');

        // //console.log('MASTER', master);
        // //console.log('SERIES', allKeys);
        // //console.log('DATA', $scope.data);
        // //console.log('LABELS', $scope.labels);

    })


});