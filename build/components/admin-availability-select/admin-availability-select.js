app.directive('availabilitySelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            contact: '=ngContact',
        },
        templateUrl: 'admin-availability-select/admin-availability-select.html',
        controller: 'AvailabilitySelectController',
    };

});


app.controller('AvailabilitySelectController', function($scope, FluroContent, Notifications) {

    $scope.sendStatus = 'ready';

    //Get the contact ID
    var contactID = $scope.contact._id;

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    function mapEntries(entries) {
        _.each(entries, function(entry) {
            var startDate = moment(entry.startDate).format('DDMMYYYY');
            var endDate = moment(entry.endDate).format('DDMMYYYY');
            if(startDate == endDate) {
                entry.sameDay = true;
            }
        })
    }
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    function loadEntries() {
        function entriesLoaded(res) {
            $scope.entries = res;
            mapEntries(res);
        }

        function entriesFailed(err) {
            console.log('Couldnt load availability', err);
            // return Notifications.post(err, 'error');
        }

        //Post to the server
        FluroContent.endpoint('contact/'+ contactID +'/unavailability', null, true).query().$promise.then(entriesLoaded, entriesFailed);
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////


    //Create a new object
    $scope._new = {}

    ////////////////////////////////////////////////////

    $scope.remove = function(item) {
        // _.pull($scope.entries, item);
        item.state = 'processing';

        function entryRemoved(res) {
            return loadEntries();
        }

        function entryRemoveFailed(err) {
            item.state = 'error';
            console.log('Removed entry fail', err);
            // return Notifications.post(err, 'error');
        }

        //Post to the server
        FluroContent.endpoint('contact/'+ contactID +'/unavailability/'+ item._id).delete().$promise.then(entryRemoved, entryRemoveFailed);



    }

    ////////////////////////////////////////////////////

    $scope.add = function() {

        var insert = angular.copy($scope._new);

        if (!$scope.entries) {
            $scope.entries = [];
        }

        ///////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////

        //Validation
        if(!insert.description || !insert.description.length) {
            return Notifications.post('Please enter a description for unavailability', 'warning');
        }

        if(!insert.startDate) {
            return Notifications.post('Unavailability start date is required', 'warning');
        }

        if(!insert.endDate) {
            return Notifications.post('Unavailability end date is required', 'warning');
        }


        ///////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////

        //Send status
        $scope.sendStatus = 'processing';


        

        ///////////////////////////////////////////////////////////

        function addSuccess(res) {

            //Send status
            $scope.sendStatus = 'ready';

            Notifications.post('Unavailability for '+$scope.contact.firstName +' was saved successfully');
            $scope.entries.push(res);
            mapEntries($scope.entries);

             $scope._new = {
                startDate:new Date(),
                endDate:new Date(),
                description:'',
            }
        }

        function addFailed(err) {
            
            //Send status
            $scope.sendStatus = 'error';

            console.log('Failed to add availability', err);
            // Notifications.post(err, 'error');
        }

        ///////////////////////////////////////////////////////////


        //Post to the server
        FluroContent.endpoint('contact/'+ contactID +'/unavailability').save(insert).$promise.then(addSuccess, addFailed);
    }



        //Load the entries
       loadEntries();

})