app.directive('schoolSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-school-select/admin-school-select.html',
        controller: 'SchoolSelectController',
    };
});

/////////////////////////////////////////////////////

app.controller('SchoolSelectController', function($scope, FluroContent) {

// console.log('HELOOOOO!!!')
	function getID(input) {

		if(!input) {
			return;
		}

		if(input._id) {
			return input._id;
		}

		return input;
	}

	/////////////////////////////////////////////////////

	var initialCalendarID = getID($scope.model.academicCalendar);
	if(initialCalendarID) {
		$scope.model.academicCalendar = initialCalendarID;
	}

	/////////////////////////////////////////////////////

	//Set the id of the current selected calendar
	$scope.selected ={}

	/////////////////////////////////////////////////////

	//Load all the calendar options
	FluroContent.endpoint('contact/schools', true, true).query({
		allDefinition:true,
		searchInheritable:true,
	})
	.$promise
	.then(function(calendars) {
		$scope.calendars = calendars;
		getSelectedCalendar();
	});

	/////////////////////////////////////////////////////

	function getSelectedCalendar() {

		var calendarID = getID($scope.model.academicCalendar);

		if(!calendarID) {
			return;
		}

		if(!$scope.calendars || !$scope.calendars.length) {
			console.log('No calendars')
			return;
		}



		var match = _.find($scope.calendars, {_id:calendarID})
		if(match) {
			$scope.selected.calendar = match;
		}
	}

	//One for good luck
	getSelectedCalendar();

	/////////////////////////////////////////////////////

	//Watch for if we change the select box
	$scope.$watch('model.academicCalendar', getSelectedCalendar);
	// $scope.$watch('model.academicCalendar', update);

})