app.service('AccessPassService', function($rootScope, $uibModal, Notifications) {

    var service = {};

    //////////////////////////////////////

    service.share = function(policy, contacts) {


        ///////////////////////////////

        var modalInstance = $uibModal.open({
            templateUrl: 'admin-pass-service/admin-pass-service.html',
            controller: 'AccessPassServiceShareController',
            size: 'md',
            backdrop: 'static',
            resolve: {
                // item: function($q) {
                //     return policy;
                // },
                contacts:function() {
                    return contacts;
                },
                item: function(FluroContent, $q) {
                    var deferred = $q.defer();

                     FluroContent.endpoint('policy/grantable/' + policy._id).get().$promise.then(deferred.resolve, function(err) {
                         deferred.reject(err);


                        var message = _.get(err, 'data.message') || _.get(err, 'data') || 'There was an error';

                        Notifications.error(message)



                     })


                     return deferred.promise;
                },
            }
        });

        return modalInstance.result;

    }


    //////////////////////////////////////

    service.shareToContacts = function(contacts) {


        ///////////////////////////////

        var modalInstance = $uibModal.open({
            templateUrl: 'admin-pass-service/admin-pass-contacts.html',
            controller: 'AccessPassServiceShareContactsController',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                contacts: function($q) {
                    return contacts;
                },
                policies: function(FluroContent) {
                    return FluroContent.endpoint('policy/grantable').query().$promise
                },
            }
        });

        return modalInstance.result;

    }


    //////////////////////////////////////

    //////////////////////////////////////

    return service;
});

////////////////////////////////////////////////////////////////////////////

app.controller('AccessPassServiceShareContactsController', function(AccessPassService, $scope, policies, contacts) {

    $scope.policies = policies;
    $scope.contacts = contacts;



    $scope.cancel = function() {
        $scope.$dismiss();
    }


    $scope.select = function(policy) {

        $scope.$dismiss();
        AccessPassService.share(policy, contacts);
    }



});


////////////////////////////////////////////////////////////////////////////

app.controller('AccessPassServiceShareController', function($scope, $rootScope, Notifications,FluroContent, item, contacts) {

    $scope.policy = item;
    $scope.settings = {}

    ///////////////////////////////////////////////////

    $scope.submission = {}

    ///////////////////////////////////////////////////////


    if(contacts && contacts.length) {
        $scope.submission.contacts = contacts;
    }


    ///////////////////////////////////////////////////////

    $scope.$watch('submission.phoneNumber', function(phoneNumber) {
        if (!phoneNumber || !phoneNumber.length) {
            return clearPhone();
        }

        FluroContent.endpoint('contact/phone/search/' + phoneNumber).query().$promise.then(function(res) {
            $scope.submission.phoneContacts = res;
        }, clearEmail)

    })

    $scope.$watch('submission.emailAddress', function(emailAddress) {
        if (!emailAddress || !emailAddress.length) {
            return clearEmail();
        }

        FluroContent.endpoint('contact/email/search/' + emailAddress).query().$promise.then(function(res) {
            $scope.submission.emailContacts = res;
        }, clearEmail)

    })

    ///////////////////////////////////////////////////


    $scope.cancel = function() {
    	$scope.$dismiss();
    }

    ///////////////////////////////////////////////////

    $scope.sendSMS = function() {
        
        $scope.settings.processing = true;

        console.log('SEND SMS');

        var body = {
            phoneNumber:$scope.submission.phoneNumber,
            policy:$scope.policy,
            application:'admin',
            countryCode:$scope.submission.countryCode,
        }

        return FluroContent.endpoint('policy/invite').save(body).$promise.then(inviteSuccess, inviteFailed);

    }


    ///////////////////////////////////////////////////

    $scope.sendContacts = function() {
        
        $scope.settings.processing = true;

        console.log('SEND CONTACTS');

        //////////////////////////////////

        var contactIDs = _.chain($scope.submission.contacts)
        .map(function(contactID) {
            if(contactID._id) {
                contactID = contactID._id;
            }
            return contactID;
        })
        .compact()
        .uniq()
        .value();

        //////////////////////////////////

        var body = {
            contacts:contactIDs,
            policy:$scope.policy,
            style:'sms',
        }

        //////////////////////////////////

        return FluroContent.endpoint('policy/invite/contacts').save(body).$promise.then(inviteSuccess, inviteFailed);

    }

    ///////////////////////////////////////////////////

    $scope.sendEmail = function() {
    	$scope.settings.processing = true;
    	var body = {
    		email:$scope.submission.emailAddress,
    		policy:$scope.policy,
    		application:'admin',
    	}
		return FluroContent.endpoint('policy/invite').save(body).$promise.then(inviteSuccess, inviteFailed);
    }

    ///////////////////////////////////////////////////

    function inviteSuccess(res) {
    	$scope.settings.processing = false;

        var count = _.get(res, 'contacts.length') || 1;

    	Notifications.status('Access pass shared with ' + count + ' contacts');
    	$scope.$dismiss();
    }

    function inviteFailed(err) {
    	$scope.settings.processing = false;
        return Notifications.error(err);
    }

    

    ///////////////////////////////////////////////////

    var emailHTML = '';
    emailHTML += '<p>Hi there!</p>';

    emailHTML += '<p>';
    emailHTML += $rootScope.user.firstName + ' ' + $rootScope.user.lastName + ' would like you to join them at ' + $rootScope.user.account.title;
    emailHTML += '</p>';
    emailHTML += '<p>';
    emailHTML += 'Here is your <em>\'' + item.title + '\'</em> invite code: <br/>';
    emailHTML += '<strong>' + String(item.inviteCode).toUpperCase() + '</strong>';
    emailHTML += '</p>';

    emailHTML += '<hr/>';

    emailHTML += '<p>';
    emailHTML += 'To collect this code login to Fluro or create an account by clicking the link below<br/>';
    emailHTML += '<a href="https://app.fluro.io/invited">https://app.fluro.io/invited</a> <br/>';
    emailHTML += '</p>';


    /**/
    // emailHTML += '<table style="max-width:320px; width:100%;"><tbody><tr>';

    ///////////////////////////////////////////////////////

    //Apple App Store

    // if (IOS_SHARE_LINK) {
    //     emailHTML += '<td style="width:50% padding: 5px;">';
    //     emailHTML += '<a href="' + IOS_SHARE_LINK + '">';
    //     emailHTML += '<img style="width: 100%; height:auto;" src="https://api.fluro.io/get/59b1ea3a264b3e1de1022c38" alt="iOS App Store"/>';
    //     emailHTML += '</a>';
    //     emailHTML += '</td>';
    // }

    // //Google Play Store
    // if (ANDROID_SHARE_LINK) {
    //     emailHTML += '<td style="width:50% padding: 5px;">';
    //     emailHTML += '<a href="' + ANDROID_SHARE_LINK + '">';
    //     emailHTML += '<img style="width: 100%; height:auto;" src="https://api.fluro.io/get/59b1ea25264b3e1de1022be4" alt="Google Play Store"/>';
    //     emailHTML += '</a>';
    //     emailHTML += '</td>';
    // }

    // emailHTML += '</tr></tbody></table>';

    ///////////////////////////////////////////////////////


    var phoneMessage = '';
     phoneMessage += $rootScope.user.firstName + ' ' + $rootScope.user.lastName + " has given you an access pass\n";
    phoneMessage += "Here is your '" + item.title + "' invite code:\n";
    phoneMessage += String(item.inviteCode).toUpperCase() + '\n\n';

    phoneMessage += "Login to the '" + APPLICATION_SHARE_NAME + "' app to collect it.\n\n";

    if (IOS_SHARE_LINK) {
        phoneMessage += "Get the app for iPhone\n";
        phoneMessage += IOS_SHARE_LINK + '\n\n';
    }

    if (ANDROID_SHARE_LINK) {
        phoneMessage += "Get the app for Android\n";
        phoneMessage += ANDROID_SHARE_LINK + '\n\n';
    }

    phoneMessage += '-';

    ///////////////////////////////////////////////////////

    $scope.submission.emailMessage = emailHTML;
    $scope.submission.phoneMessage = phoneMessage;

    ///////////////////////////////////////////////////////

    function clearPhone() {
        $scope.submission.phoneContacts = null;
    }

    function clearEmail() {
        $scope.submission.emailContacts = null;
    }

    

    // /**/

    // // message += 'fluro://access/' + item.inviteCode;

    // var emailDetails = {
    //     subject: item.title + ' Access Pass',
    //     body: message,
    //     isHtml: true,
    // }


});