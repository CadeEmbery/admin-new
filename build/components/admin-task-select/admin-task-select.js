app.directive('taskSelect', function() {

    return {
        restrict: 'E',
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-task-select/admin-task-select.html',
        controller: 'TaskSelectController',
    };
});


app.controller('TaskSelectController', function($scope) {

	if(!$scope.model) {
		$scope.model = [];
	}

	$scope._new = {}

	///////////////////////////////////////

	$scope.remove = function(item) {
		return _.pull($scope.model, item);
	}

	$scope.create = function() {

		var copy = angular.copy($scope._new);

		if(!copy.name || !copy.name.length) {
			return;
		}

		// if(!copy.type || !copy.type.length) {
		// 	copy.type = 'confirm';
		// }

		var existing = _.find($scope.model, {from:copy.name});


		$scope.model.push(copy);
		$scope._new = {};

	}
})
