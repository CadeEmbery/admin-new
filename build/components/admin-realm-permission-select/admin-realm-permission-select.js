app.directive('realmPermissionSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-realm-permission-select/admin-realm-permission-select.html',
        controller: 'RealmPermissionSelectController',
    };
});


app.controller('RealmPermissionSelectController', function($scope, TypeService) {


	if(!$scope.model) {
		$scope.model = [];
	}
	
    $scope.newField = {}


    $scope.definedTypes = TypeService.definedTypes;
    $scope.types = TypeService.types;


    $scope.add = function() {

        console.log('Add')
    	var copy = angular.copy($scope.newField);

    	$scope.model.push(copy);

    	$scope.newField = {}
    }

    $scope.remove = function(field) {
    	_.pull($scope.model, field);
    }

});