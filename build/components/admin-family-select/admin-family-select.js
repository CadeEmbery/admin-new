app.directive('familySelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            type: '=ngType',
        },
        templateUrl: 'admin-family-select/admin-family-select.html',
        controller: 'FamilySelectController',
    };
});

app.controller('FamilySelectController', function($scope, $rootScope, CacheManager, Fluro, $http, FluroContent) {


    if (!$scope.model) {
        $scope.model = [];
    }
    
    $scope.dynamicPopover = {
         templateUrl: 'admin-family-select/admin-family-popover.html',
    };


    $scope.proposed = {};

    /////////////////////////////////////

    $scope.add = function(item) {
        $scope.model.push(item);
        $scope.proposed = {};
    }


    $scope.removeFamily = function(family) {
       _.pull($scope.model, family);
    }

    
    $scope.getFamilys = function(val) {

        var url = Fluro.apiURL + '/content/family/search/' + val;
        return $http.get(url, {
            ignoreLoadingBar: true,
            params: {
                limit: 20,
            }
        }).then(function(response) {
            var results = response.data;
            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.model, {
                    '_id': item._id
                });
                if (!exists) {
                    filtered.push(item);
                }
                return filtered;
            }, [])
        });

    };


    $scope.create = function() {

       
        var details = {
            title: $scope.proposed.value,
        }

        //Immediately create a family   
        FluroContent.resource('family').save(details, function(family) {

            //$scope.familys.push(family);
            $scope.model.push(family);

            //We need to clear the family cache now
            CacheManager.clear('family');

            //Clear
            $scope.proposed = {
            }
        }, function(data) {
            console.log('Failed to create family', data);
        });

    }
});