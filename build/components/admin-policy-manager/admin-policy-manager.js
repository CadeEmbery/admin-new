app.directive('policyManager', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-policy-manager/admin-policy-manager.html',
        controller: 'PolicyManagerController',
    };
});


app.controller('PolicyManagerController', function($scope, $state, Fluro, $http, ModalService, $rootScope, FluroAccess) {

    if (!$scope.model) {
        $scope.model = [];
    }

    ///////////////////////////////////////

    $scope.proposed = {}

    ///////////////////////////////////////

    $scope.loadingItems = false;

    ///////////////////////////////////////

    $scope.canCreate = function() {
         return FluroAccess.can('create', 'policy');   
    }

    ///////////////////////////////////////

    $scope.create = function() {
        ModalService.create('policy', {}, $scope.add)
    }

    ///////////////////////////////////////

    $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item, $rootScope.user);
    }

    $scope.edit = function(item) {
        ModalService.edit(item, function(result) {
            $state.reload();
        }, function(result) {});
    }

    ///////////////////////////////////////////

    $scope.add = function(item) {
        $scope.model.push(item);
        $scope.proposed = {}
        /**/

    }

    ///////////////////////////////////////////

    $scope.remove = function(item) {
        _.pull($scope.model, item);
    }


    $scope.$watch('model', function(items) {
        $scope.visibleModel = _.filter(items, function(item) {
            return item.account == $rootScope.user.account._id;
        })
    }, true)


    /////////////////////////////////////

    $scope.browse = function() {

        

        var modal = ModalService.browse('policy', {items:$scope.model});

        modal.result.then(closed, closed)

    }

    /////////////////////////////////////

    $scope.getOptions = function(val) {

        $scope.loadingItems = true;
        ////////////////////////

        //Create Search Url
        var searchUrl = Fluro.apiURL + '/content/policy/search';

        ////////////////////////

        return $http.get(searchUrl + '/' + val, {
            ignoreLoadingBar: true,
            params: {
                limit: 10,
            }
        }).then(function(response) {

            $scope.loadingItems = false;
            var results = response.data;

            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.model, {
                    '_id': item._id
                });
                if (!exists) {
                    filtered.push(item);
                }
                return filtered;
            }, []);

        });

    };

});