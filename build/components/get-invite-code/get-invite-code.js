app.directive('getInviteCode', function(FluroContent, Notifications, $timeout) {

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'get-invite-code/get-invite-code.html',
        link: function($scope, $element) {



        function clipboard(shortURL){

            var element = angular.element('<input id="clipper">');
            element.css({
                position:'fixed',
                top:999999999,
                left:99999999999,
                'z-index':-999,
            });
            element[0].value = shortURL;
            // console.log(shortURL, element[0].value);
            angular.element('body').append(element);
            element[0].select();

            var copied;

            try {
                copied = document.execCommand('copy');

                if(!copied) {
                    console.error("Cannot copy text");
                } else {
                    Notifications.status('Copied to clipboard')
                    console.log("Copied text", copied);
                }
            } catch (err) {
                console.log('Unable to copy', err);
            }
        }



            //////////////////////////////////////////


            $scope.getCode = function() {
                $scope.item.gettingCode = true;


                FluroContent.endpoint('policy/grantable/' + $scope.item._id).get().$promise.then(function(policy) {
                    $scope.item.inviteCode = policy.inviteCode;
                    $scope.item.gettingCode = false;



                    

                    
                        clipboard(policy.inviteCode)
                    
                }, function(err) {
                    $scope.item.gettingCode = false;
                });
            }
        }
    };
});



// var copyBobBtn = document.querySelector('.js-copy-bob-btn'),
//     copyJaneBtn = document.querySelector('.js-copy-jane-btn');

// copyBobBtn.addEventListener('click', function(event) {
//     copyTextToClipboard('Bob');
// });


// copyJaneBtn.addEventListener('click', function(event) {
//     copyTextToClipboard('Jane');
// });