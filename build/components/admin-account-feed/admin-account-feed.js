app.directive('adminAccountFeed', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            accountID: '=ngModel'
        },
        templateUrl: 'admin-account-feed/admin-account-feed.html',
        controller: 'AccountFeedController',
    };
});

app.controller('AccountFeedController', function($scope, $timeout, FluroContent, FluroSocket, $filter) {


    $scope.logs = [];

    $scope.$watch('accountID', function(accountID) {

        if (accountID) {

            //Now
            var now = new Date();

            //////////////////////////////////////////////////////////

            //Get the initial logs
            FluroContent.endpoint('log').query({
                account: accountID,
                limit: 120,
            }).$promise.then(function(res) {
                $scope.logs = res;
            }, function(res) {
                console.log('Error loading logs');
            });

            //////////////////////////////////////////////////////////

            $scope.$watch(function() {
                return $scope.logs.length;
            }, function() {

                var logs = $scope.logs;
                $scope.timeline = _.chain(logs)
                    .map(function(log) {

                        var card = log;
                        log.date = new Date(log.created);

                        // var card = {
                        //     _id:log._id,
                        //     date:new Date(log.created),
                        //     log:log,
                        //     title:log.message,
                        //     key:log.key,
                        //     realms:log.realms,
                        //     item:log.item,
                        // }

                        switch (log.key) {
                            case 'correspondence.create':
                            case 'correspondence.sent':
                            case 'correspondence.error':
                            case 'content.destroy':
                            //Dont show
                                return;
                            case 'sms.replied':
                                //Dont show
                                return;
                            break;
                            case 'content.create':
                            case 'content.update':
                            case 'content.edit':
                            case 'content.delete':
                            case 'content.restore':
                            // case 'content.destroy':
                                log.type = 'content';
                                break;
                            case 'confirmation.unavailable':
                            case 'confirmation.confirmed':
                            case 'confirmation':
                                log.type = 'confirmation';
                                break;
                            case 'checkin':
                            case 'checkout':
                                log.type = 'checkin';
                                break;
                            case 'reaction.email.success':
                                log.type = 'reaction';
                                break;
                            case 'reaction.success':
                                log.type = 'success';
                                break;
                            case 'reaction.error':
                            case 'payment.failed':
                                log.type = 'error';
                                break;
                            case 'payment.success':
                                log.type = 'payment'
                            break;
                            case 'process.move':
                                log.type = 'process'
                            break;
                        }

                        ////////////////////////////////////

                        if (log.managedUser) {
                            card.user = {
                                name: log.managedUser.title,
                                _id: log.managedUser._id,
                            }
                        }

                        if (log.user) {
                            card.user = {
                                name: log.user.name,
                                _id: log.user._id,
                            }
                        }

                        ////////////////////////////////////


                        if (log.type == 'content') {

                            var contentDataType = _.get(log, 'data._type');
                            
                            // var types = [contentItemType, contentDataType];
                            // var excluded = _.includes(types, [
                            //     'checkin',
                            //     'method',
                            // ])

                            switch (contentDataType) {
                                case 'checkin':
                                    if(log.key == 'content.create') {
                                        return;
                                    }
                                    // card.message = 'Created new payment method';
                                    break;
                                case 'method':
                                    card.message = 'Created new payment method';
                                    break;
                            }
                        }



                        return card;
                    })
                    .compact()
                    .sortBy(function(card) {
                        return card.date;
                    })
                    .reverse()
                    .reduce(function(results, card) {

                        /////////////////////////////////////////

                        var cardDate = card.date;

                        /////////////////////////////////////////

                        //Create the keys for the years and months
                        var year = new Date(cardDate);
                        year.setHours(0);
                        year.setMinutes(0);
                        year.setSeconds(0);
                        year.setMilliseconds(0);

                        var yearKey = year.format('Y')

                        /////////////////////////////////////////

                        var month = new Date(cardDate);
                        month.setHours(0);
                        month.setMinutes(0);
                        month.setSeconds(0);
                        month.setMilliseconds(0);

                        var monthKey = month.format('M')

                        /////////////////////////////////////////

                        var day = new Date(cardDate);
                        day.setHours(0);
                        day.setMinutes(0);
                        day.setSeconds(0);
                        day.setMilliseconds(0);

                        var dayKey = day.format('j')

                        /////////////////////////////////////////

                        var existingYear = results[yearKey];
                        // _.find(results, {
                        //     key: yearKey
                        // });

                        if (!existingYear) {
                            existingYear = {
                                date: year,
                                key: yearKey,
                                months: {},
                            }

                            results[yearKey] = existingYear;
                            // results.push(existingYear);
                        }

                        /////////////////////////////////////////

                        var existingMonth = existingYear.months[monthKey];

                        // _.find(existingYear.months, {
                        //     key: monthKey
                        // });

                        if (!existingMonth) {
                            existingMonth = {
                                date: month,
                                key: monthKey,
                                days: {}
                            };

                            existingYear.months[monthKey] = existingMonth;
                            // existingYear.months.push(existingMonth);
                        }

                        /////////////////////////////////////////

                        var existingDay = existingMonth.days[dayKey];
                        // _.find(existingMonth.days, {
                        //     key: dayKey
                        // });

                        if (!existingDay) {
                            existingDay = {
                                date: day,
                                key: dayKey,
                                items: [],
                            };

                            // existingMonth.days.push(existingDay);
                            existingMonth.days[dayKey] = existingDay;
                        }

                        //Add the card
                        existingDay.items.push(card);

                        return results;

                    }, {})
                    .values()
                    .sortBy(function(year) {
                        return year.date;
                    })
                    .reverse()
                    .map(function(year) {

                        //Map the months object to values on each year
                        year.months = _.chain(year.months)
                        .values()
                        .sortBy(function(month) {
                            return month.date;
                        })
                        .reverse()
                        .map(function(month) {

                            //Map the days object to values on each month
                            month.days = _.chain(month.days)
                            .values()
                            .sortBy(function(day) {
                                return day.date;
                            })
                            .reverse()
                            .value();

                            return month;
                        })
                        .value();

                        //Return the year
                        return year;
                        
                    })

                .value();
            })
        }
    });




    //////////////////////////////////////////////////////////

    FluroSocket.on('content.create', socketUpdate);
    FluroSocket.on('content.edit', socketUpdate);
    FluroSocket.on('content.destroy', socketUpdate);
    FluroSocket.on('content.delete', socketUpdate);
    FluroSocket.on('content.restore', socketUpdate);

    //Payments
    FluroSocket.on('payment.refund', socketUpdate);
    FluroSocket.on('payment.success', socketUpdate);
    FluroSocket.on('payment.failed', socketUpdate);

    //Process
    FluroSocket.on('process.move', socketUpdate);

    //Reactions
    FluroSocket.on('reaction.error', socketUpdate);
    FluroSocket.on('reaction.success', socketUpdate);
    FluroSocket.on('reaction.email.error', socketUpdate);
    FluroSocket.on('reaction.email.success', socketUpdate);
    FluroSocket.on('reaction.request.success', socketUpdate);
    FluroSocket.on('reaction.request.error', socketUpdate);

    //Users
    FluroSocket.on('user.edit', socketUpdate);
    FluroSocket.on('user.delete', socketUpdate);
    FluroSocket.on('user.destroy', socketUpdate);

    //Batch process
    FluroSocket.on('batch.init', socketUpdate);

    //Checkins
    FluroSocket.on('checkin', socketUpdate);
    FluroSocket.on('checkout', socketUpdate);

    //Confirmations
    FluroSocket.on('confirmation.confirmed', socketUpdate);
    FluroSocket.on('confirmation.unavailable', socketUpdate);

    //Contacts and families
    FluroSocket.on('family.merge', socketUpdate);
    FluroSocket.on('contact.merge', socketUpdate);

    //Contacts and families
    FluroSocket.on('message.sms', socketUpdate);
    FluroSocket.on('message.email', socketUpdate);

    function socketUpdate(socketEvent) {

        console.log('Socket UPDATE', socketEvent);
        $timeout(function() {
            $scope.logs.unshift(socketEvent);
        })
    }

});