app.directive('mailoutPreview', function() {

    return {
        restrict: 'E',
        replace: true,
        // Replace the div with our template
        scope: {
            definition: '=',
            mailout: '=?',
            cache: '=?',
            live: '=?',
        },
        templateUrl: 'admin-mailout-preview/admin-mailout-preview.html',
        controller: 'MailoutPreviewController',
    };
});


app.controller('MailoutPreviewController', function($scope, Fluro, FluroSocket) {


    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    $scope.$watch('definition + mailout + cache', function() {
        // $scope.previewURL = Fluro.apiURL + '/mailout/render/' + $scope.item.definition + '?mailout=' + $scope.item._id;

        if (!$scope.definition) {
            return;
        }



        ////////////////////////////////////////////////////

        var url = Fluro.apiURL + '/mailout/render/' + $scope.definition + '?';

        if ($scope.mailout) {
            url += '&mailout=' + $scope.mailout;
        }

        if ($scope.cache) {
            url += '&cb=' + $scope.cache;
        }

        if ($scope.live) {
            url += '&live=' + true;
        }

        if(Fluro.token) {
            url += '&access_token=' + Fluro.token;
        }

        $scope.url = url;


    })

});