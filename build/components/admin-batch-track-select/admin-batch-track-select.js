app.directive('batchTrackSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            selection: '=ngModel',
            // type:'=ngType',
            // definition:'=ngDefinition',
        },
        templateUrl: 'admin-batch-track-select/admin-batch-track-select.html',
        controller: 'BatchTrackSelectController',
    };
});

app.controller('BatchTrackSelectController', function($scope, $state, Batch, Notifications, $rootScope, CacheManager, Fluro, FluroContent, FluroAccess, $http) {


    //////////////////////////////////////////////////////////

    //Get all the options
    FluroContent.resource('eventtrack').query({
        simple: true,
    }, function(res) {
        $scope.options = res;
    });

    //////////////////////////////////////////////////////////

    $scope.dynamicPopover = {
        templateUrl: 'admin-batch-track-select/admin-batch-track-popover.html',
    };

    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    function done(err, data) {

        // console.log('Batch done', data);
        if (err) {
            //console.log('DONE ERR', err)
            return Notifications.error(err);
        }

        

        ///////////////////////

        //Reload the state
        $state.reload();

        ///////////////////////

        //Deselect all
        $scope.selection.deselectAll();
        Notifications.status('Updated  ' + data.success.length + ' events');
    }

    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    $scope.setEventTrack = function(trackID) {

        var details = {
            ids: $scope.selection.ids,
            track: trackID, //$scope.selectedProcesses,
        }

        Batch.setEventTrack(details, done);
    }

});