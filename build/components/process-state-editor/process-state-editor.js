app.directive('processStateEditor', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            states: '=ngModel',
            definition: '=ngDefinition',
        },
        templateUrl: 'process-state-editor/process-state-editor.html',
        controller: 'ProcessStateEditorController',
    };
});



app.controller('ProcessStateEditorController', function($rootScope, $uibModal, $scope) {

    ////////////////////////////////////////////////////

    //Create a states if it doesn't exist
    if (!$scope.states) {
        $scope.states = [];
    }

    ////////////////////////////////////////////////////

    //Create a new object
    $scope._new = {
        name: ''
    }



    $scope.addTaskList = function(state) {
        console.log('Add Task List');
        if (!state.taskLists) {
            state.taskLists = [];
        }

        state.taskLists.push({
            title: state.title + " tasks",
            adding: true,
            tasks: [],
        })
    }

    ////////////////////////////////////////////////////

    $scope.addItem = function() {

        var submittedKey = $scope._new.key;

        if (!submittedKey || !submittedKey.length) {
            return;
        }

        var exists = _.some($scope.states, {
            key: submittedKey
        });

        if (exists) {
            return;
        }

        /////////////////////////////////////
        //Insert entry
        var insert = angular.copy($scope._new);

        insert.key = insert.key.toLowerCase();
        $scope.states.push(insert);

        $scope._new = {};

        $scope.$broadcast('add');
    }

    ////////////////////////////////////////////////////

    $scope.remove = function(entry) {
        _.pull($scope.states, entry);
    }


    ////////////////////////////////////////////////////

    $scope.editState = function(column) {

        var definition = $scope.definition;
        var modalStatus = {};

        //////////////////////////////////

        var modalInstance = $uibModal.open({
            template:'<process-column-editor ng-model="column" ng-definition="definition" process-definitions="processDefinitions" detail-definitions="detailDefinitions" processing="modalStatus.processing" close="closeModal" cancel="cancelModal"></process-column-editor>',
            // template: '<process-column-editor ng-model="column" close="closeModal"></process-column-editor>',
            size: 'md',
            resolve:{
                processDefinitions:function(FluroContent) {
                    var promise = FluroContent.endpoint('defined/types/process').query().$promise;


                    return promise;
                },
                detailDefinitions:function(FluroContent) {
                    var promise = FluroContent.endpoint('defined/types/contactdetail').query().$promise;

                  
                    return promise;
                }
            },
            controller: function($scope, processDefinitions, detailDefinitions) {


                 $scope.cancelModal = function() {
                    console.log('CLOSE MODAL', modalInstance);
                    modalInstance.close();
                }

                //////////////////////////////////

                $scope.column = column;
                $scope.definition = definition;
                $scope.modalStatus = modalStatus;
                $scope.processDefinitions = processDefinitions,
                    $scope.detailDefinitions = detailDefinitions;

                //////////////////////////////////

                // $scope.test = testEvent;
                // $scope.isModal =true;
                $scope.closeModal = function() {
                    console.log('close modal')
                    $scope.$close();
                }
            }
        });
    }

    ////////////////////////////////////////////////////

    $scope.contains = function(entry) {
        return _.contains($scope.states, entry);
    }

})