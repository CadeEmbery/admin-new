
app.controller('TimeoutLoginController', function($scope, $rootScope, FluroContent, Notifications, $rootScope) {

    console.log('TIMEOUT CONTROLLER');

    ////////////////////////////////////////

    $scope.submission ={
        username:$rootScope.user.email
    }

    ////////////////////////////////////////

    $scope.submit = function() {

        $scope.submission.state = 'processing';

        console.log('SIGN IN PLEASE NOW')
        var promise = FluroContent.endpoint('auth/login').save($scope.submission).$promise;

        promise.then(function(res) {
            console.log('LOGGED IN SCOPE!!!', $scope);

            Notifications.status('Welcome back ' + $rootScope.user.firstName)
            // $scope.$$close(res);

            if($scope.$close) {
                $scope.$close(res);
            }

        }, function(err) {
            console.log('ERROR', err);
            $scope.submission.state = 'error';
            $scope.error = err.data;
        })


    }

})