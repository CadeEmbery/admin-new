
(function($) {


$.Redactor.prototype.insertFluroVideo = function() {

    var controller = {}




    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('insertFluroVideo', 'Insert Video');
        this.button.addCallback(button, controller.select);
        this.button.setAwesome('insertFluroVideo', 'fa-video');
    }

    ////////////////////////////////////

    controller.select = function() {

        //Save Selection
        this.selection.save();

        /////////////////////////////////////////////////////////

        //Get the Scope
        var $rootScope = angular.element('body').scope().$root;

        var browseModel = {};
        var params = {};

        //Browse for images
        $rootScope.modal.browse('video', browseModel, params).result
            .then(finish, finish);

        function finish(res) {
            //Insert all the images into the text area
            controller.insert(browseModel.items);
        }
    }


    /////////////////////////////////////////////////////////

    controller.insert = function(videos) {

        //Get the Scope
        var $rootScope = angular.element('body').scope().$root;

        //Get ready for Undo
        this.buffer.set();

        //Restore the selection
        this.selection.restore();

        var insert = this.insert;

        //Only proceed if we actually selected some images
        if (videos.length) {
            var str = '';


            _.each(videos, function(video) {
                console.log('VIDEO', video);

                var simple = {
                    _id: video._id,
                    _type: 'video',
                    external: video.external,
                    assetType: video.assetType,
                }

                var json = JSON.stringify(simple);
                json = json.replace('"', '\"', 'g')
               

                console.log(insert);

                str += '<fluro-video ng-model="' + json + '"></fluro-video>';

            })


            console.log('INSERT', str);

            //Insert
            this.insert.htmlWithoutClean(str);
            this.code.sync();



        }

    }


    /////////////////////////////////////////////////////////

    return controller;
};



})(jQuery);