
(function($) {

$.Redactor.prototype.insertImage = function() {

    var controller = {}


    

    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('insertImage', 'Insert image');
        this.button.addCallback(button, controller.select);
        this.button.setAwesome('insertImage', 'fa-image');
    }

    ////////////////////////////////////

    controller.select = function() {

        //Save Selection
        this.selection.save();

        /////////////////////////////////////////////////////////

        //Get the Scope
        var $rootScope = angular.element('body').scope().$root;

        var browseModel = {};
        var params = {};

        //Browse for images
        $rootScope.modal.browse('image', browseModel, params).result
        .then(finish, finish);

        function finish(res) {

            //Map each image to id and url             
            var mapped = _.map(browseModel.items, function(img) {
                return {
                    _id: img._id,
                    url: $rootScope.asset.imageUrl(img._id)
                }
            });

            //Insert all the images into the text area
            controller.insert(mapped);

        }

/**
        //Get the Scope
        //var $scope = angular.element(this.$element).scope();
        
        
        /////////////////////////////////////////////////////////

        console.log('Scope', $uibModal);

            var modalInstance = $scope.modal.open({
                template: '<content-browser ng-model="images" ng-done="submit" ng-type="' + "'image'" + '"></content-browser>',
                size: 'lg',
                controller: function($uibModalInstance, $rootScope, $scope) {
                    $scope.images = [];
                    $scope.submit = function() {

                        //Map each image to id and url             
                        var mapped = _.map($scope.images, function(img) {
                            return {
                                _id: img._id,
                                url: $rootScope.asset.imageUrl(img._id)
                            }
                        });

                        //Insert all the images into the text area
                        controller.insert(mapped);

                        //Close the modal
                        $uibModalInstance.close($scope.images);
                    }
                }
            });
        /**/
        
    }

    /////////////////////////////////////////////////////////

    controller.insert = function(images) {

        //Get ready for Undo
        this.buffer.set();

        //Restore the selection
        this.selection.restore();

        //Only proceed if we actually selected some images
        if (images.length) {
            var str = '';
            _.each(images, function(image) {
                str += '<img src="' + image.url + '" ng-src="{{$root.asset.imageUrl(' + "'" + image._id + "'" + ')}}"/>';
            })

            //Insert
            this.insert.html(str);
            this.code.sync();
        }
    }

    /////////////////////////////////////////////////////////

    return controller;
};



})(jQuery);