
(function($) {


$.Redactor.prototype.uploadFluroAsset = function() {


    var controller = {}


    /////////////////////////////////////////////////////////

    //Get the Scope


    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('uploadFluroAsset', 'Upload Asset');
        this.button.addCallback(button, controller.select);
        this.button.setAwesome('uploadFluroAsset', 'fa-upload');
    }

    ////////////////////////////////////

    controller.select = function() {

        //Save Selection
        this.selection.save();

        //Init the scope
        var $scope = angular.element('body').scope();
        var $rootScope = $scope.$root;


        /////////////////////////////////////////////////////////

        $rootScope.modal.create('asset', {
            template:{
               privacy:'public',
            }
        }, successCallback, cancelCallback);

        /////////////////////////////////////////////////////////

        function successCallback(res) {

            switch(res._type) {
                case 'asset':
                case 'audio':
                case 'image':
                case 'video':
                    controller.insert(res);
                break;
                default:
                console.log('Uploaded an asset', res);
                break;
            }
        }

        /////////////////////////////////////////////////////////

        function cancelCallback(res) {
            console.log('Failed', res)
        }
    }

    /////////////////////////////////////////////////////////

    controller.insert = function(asset) {

        //Init the scope
        var $scope = angular.element('body').scope();
        var $rootScope = $scope.$root;
        
        //Set Undo point
        this.buffer.set();

        //Restore the selection
        this.selection.restore();

        var url = $rootScope.asset.getUrl(asset._id, {extension:asset.extension, filename:asset.filename});
        //Only proceed if we actually selected some assets
        var str;
        switch(asset._type) {
            case 'image':
                str = '<img src="' + url + '" ng-src="{{$root.asset.imageUrl(' + "'" + asset._id + "'" + ')}}"/>';
            break;
            default:
                str = '<a class="btn btn-primary" href="' + url + '" target="_blank" ng-href="{{$root.asset.getUrl(' + "'" + asset._id + "'" + ', {filename:'+ asset.filename + ', extension:'+asset.extension+'})}}">'+ asset.title+'</a>';
            break;
        }

        //Insert
        this.insert.html(str);
        this.code.sync();

    }

    /////////////////////////////////////////////////////////

    return controller;
};


})(jQuery);