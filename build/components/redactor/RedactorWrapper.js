var redactorOptions = {};

//Setup Basic Options
app.constant('redactorOptions', redactorOptions);


//Setup the directive
app.directive('redactor', function($timeout, Fluro) {
    return {
        restrict: 'A',
        require: 'ngModel',
        controller: function($scope, $uibModal) {
            $scope.modal = $uibModal;

        },
        link: function(scope, element, attrs, ngModel) {

            ////////////////////////////////////////////

            // Expose scope var with loaded state of Redactor
            scope.redactorLoaded = false;

            var updateModel = function updateModel(value) {
                // $timeout to avoid $digest collision
                $timeout(function() {
                    scope.$apply(function() {
                        ngModel.$setViewValue(value);
                    });
                });
            };




            var options = {
                changeCallback: updateModel,
               /** 
               imageUpload: uploadURL,   
                callbacks: {
                    uploadStart: function(e, formData)
                    {
                        console.log('My upload started!', e, formData); 
                    }
                }  
                /**/
            }

            scope.redactorOptions = options;

            /////////////////////////////////

            var additionalOptions = attrs.redactor ? scope.$eval(attrs.redactor) : {};

            /////////////////////////////////

            if(!additionalOptions.buttons) {
            	additionalOptions.buttons = [
            		'html',
            		'formatting', 
            		'bold', 
            		'underline', 
            		'italic', 
					'unorderedlist', 
					'orderedlist', 
					'insertImage', 
					'video', 
					'table', 
					'link', 
					'indent', 
					'outdent', 
					'horizontalrule',
                    
					]
            }

            /////////////////////////////////

            //Format Options
            additionalOptions.formatting = [];
            additionalOptions.formattingAdd = [];

            additionalOptions.formattingAdd.push({
                tag:'p',
                title:'Normal text',
                clear: true,
            });

            

            additionalOptions.formattingAdd.push({
            	tag:'h1',
            	title:'Heading 1',
            });

            additionalOptions.formattingAdd.push({
            	tag:'h2',
            	title:'Heading 2',
            });

            additionalOptions.formattingAdd.push({
            	tag:'h3',
            	title:'Heading 3',
            });

            additionalOptions.formattingAdd.push({
            	tag:'h4',
            	title:'Heading 4',
            });

            additionalOptions.formattingAdd.push({
            	tag:'h5',
            	title:'Heading 5',
            });

            additionalOptions.formattingAdd.push({
            	tag:'blockquote',
            	title:'Quote',
            });

            additionalOptions.formattingAdd.push({
                tag:'p',
                title:'Lead text',
                class:'lead',
                clear: true,
            });

            additionalOptions.formattingAdd.push({
                tag:'span',
                title:'Small text',
                class:'small',
                // clear: true,
            });

            additionalOptions.formattingAdd.push({
                tag:'span',
                title:'Muted text',
                class:'text-muted',
                // clear: true,
            });

            additionalOptions.formattingAdd.push({
                tag:'code',
                title:'Code',
            });

          


            

            /////////////////////////////////

            // //Add allowed attributes
            // additionalOptions.allowedAttr = [
            //     ['p', 'class'],
            //     ['img', ['src', 'alt', 'title', 'width', 'height', 'style', 'class', 'ng-src']],
            //     ['fluro-video', 'ng-model', 'ng-params'],
            //     ['iframe', ['src','style', 'width', 'height', 'allowfullscreen', 'frameborder']],
            //     ['tr', ['class']],
            //     ['td', ['class', 'colspan']],
            //     ['table', ['class']],
            //     ['div', ['class']],
            //     ['a', '*'],
            //     ['span', ['class', 'rel', 'data-verified']],
            //     ['iframe', '*'],
            //     ['video', '*'],
            //     ['audio', '*'],
            //     ['embed', '*'],
            //     ['object', '*'],
            //     ['param', '*'],
            //     ['source', '*']
            // ];

            additionalOptions.allowedAttr = [
                ['h1', ['class', 'style']],
                ['h2', ['class', 'style']],
                ['h3', ['class', 'style']],
                ['h4', ['class', 'style']],
                ['h5', ['class', 'style']],
                ['h6', ['class', 'style']],
                ['p', ['class', 'style']],
                ['img', ['src', 'alt', 'title', 'width', 'height', 'style', 'class', 'ng-src']],
                ['fluro-video', ['ng-model', 'ng-params']],
                ['iframe', ['src', 'style', 'width', 'height', 'allowfullscreen', 'frameborder']],
                ['tr', ['class']],
                ['td', ['class', 'colspan']],
                ['table', ['class']],
                ['div', ['class']],
                ['a', ['*']],
                ['span', ['class', 'rel', 'data-verified']],
                ['iframe', ['*']],
                ['video', ['*']],
                ['audio', ['*']],
                ['embed', ['*']],
                ['object', ['*']],
                ['param', ['*']],
                ['source', ['*']]
            ];

            /////////////////////////////////

            additionalOptions.replaceTags = [
                ['strike', 'del'],
                //['i', 'em'],
                ['b', 'strong'],
            ]

            /////////////////////////////////

            additionalOptions.replaceDivs = false;

            /////////////////////////////////

            if(!additionalOptions.plugins) {
                //Settings
                additionalOptions.plugins = [
                'insertImage',
                // 'insertFluroVideo',
                'video',
                // 'uploadFluroImage',
                'insertAsset',
                'uploadFluroAsset',
                
                'table',
                'undoAction',
                'redoAction',
                // 'insertButton',
                ];
            }

            if(!additionalOptions.toolbarExternal) {
                additionalOptions.toolbarExternal = '#article-toolbar';
            }
           

            /////////////////////////////////

            var editor;

            /////////////////////////////////

            angular.extend(options, redactorOptions, additionalOptions);

            /////////////////////////////////

            // prevent collision with the constant values on ChangeCallback
            var changeCallback = additionalOptions.changeCallback || redactorOptions.changeCallback;
            if (changeCallback) {
                options.changeCallback = function(value) {
                    updateModel.call(this, value);
                    changeCallback.call(this, value);
                }
            }

            /////////////////////////////////

            // put in timeout to avoid $digest collision.  call render() to
            // set the initial value.
            $timeout(function() {
                editor = element.redactor(options);
                ngModel.$render();
                element.on('remove', function() {
                    element.off('remove');
                    element.redactor('core.destroy');
                });
            });

            ngModel.$render = function() {
                if (angular.isDefined(editor)) {
                    $timeout(function() {
                        element.redactor('code.set', ngModel.$viewValue || '');
                        element.redactor('placeholder.toggle');
                        scope.redactorLoaded = true;
                    });
                }
            };
        }
    };
});