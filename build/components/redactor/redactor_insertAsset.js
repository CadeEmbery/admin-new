
(function($) {



$.Redactor.prototype.insertAsset = function() {

    var controller = {}


    

    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('insertAsset', 'Add attachments');
        this.button.addCallback(button, controller.select);
        this.button.setAwesome('insertAsset', 'fa-paperclip');
    }

    ////////////////////////////////////

    controller.select = function() {

        //Save Selection
        this.selection.save();

        /////////////////////////////////////////////////////////

        //Get the Scope
        var $rootScope = angular.element('body').scope().$root;

        var browseModel = {};
        var params = {};

        //Browse for images
        $rootScope.modal.browse('asset', browseModel, params).result
        .then(finish, finish);

        function finish(res) {
            //Insert all the images into the text area
            controller.insert(browseModel.items);
        }
    }


    /////////////////////////////////////////////////////////

    controller.insert = function(assets) {

        //Get the Scope
        var $rootScope = angular.element('body').scope().$root;

        //Get ready for Undo
        this.buffer.set();

        //Restore the selection
        this.selection.restore();

        //Only proceed if we actually selected some images
        if (assets.length) {
            var str = '';

                if(assets.length == 1) {
                    str += '<ul>';
                }
                _.each(assets, function(asset) {

                    console.log('ASSET', asset);
                    //Get the url
                    var url = $rootScope.asset.getUrl(asset._id, {extension:asset.extension, filename:asset.filename});
                    if(assets.length == 1) {
                        str += '<a href="' + url + '" target="_blank" ng-href="{{$root.asset.getUrl(' + "'" + asset._id + "'" + ', {filename:'+ asset.filename + ', extension:'+asset.extension+'})}}">'+ asset.title+'</a>';
                    } else {
                        str += '<li><a href="' + url + '" target="_blank" ng-href="{{$root.asset.getUrl(' + "'" + asset._id + "'" + ', {filename:'+ asset.filename + ', extension:'+asset.extension+'})}}">'+ asset.title+'</a></li>';
                    }
                })

                if(assets.length == 1) {
                    str += '</ul>';
                }

                //Insert
                this.insert.html(str);
                this.code.sync();
            

            
        }

    }

    /**

    /////////////////////////////////////////////////////////

    controller.insert = function(images) {

        //Get ready for Undo
        this.buffer.set();

        //Restore the selection
        this.selection.restore();

        //Only proceed if we actually selected some images
        if (images.length) {
            var str = '';
            _.each(images, function(image) {
                str += '<img src="' + image.url + '" ng-src="{{$root.asset.imageUrl(' + "'" + image._id + "'" + ')}}"/>';
            })

            //Insert
            this.insert.html(str);
            this.code.sync();
        }
    }
    /**/

    /////////////////////////////////////////////////////////

    return controller;
};


})(jQuery);