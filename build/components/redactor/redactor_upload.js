

(function($) {

$.Redactor.prototype.uploadFluroImage = function() {


    var controller = {}


    /////////////////////////////////////////////////////////

    //Get the Scope


    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('uploadFluroImage', 'Upload Image');
        this.button.addCallback(button, controller.select);
        this.button.setAwesome('uploadFluroImage', 'fa-cloud-upload');
    }

    ////////////////////////////////////

    controller.select = function() {

        //Save Selection
        this.selection.save();

        //Init the scope
        var $scope = angular.element('body').scope();
        var $rootScope = $scope.$root;


        /////////////////////////////////////////////////////////

        $rootScope.modal.create('image', {}, successCallback, cancelCallback);

        /////////////////////////////////////////////////////////

        function successCallback(res) {
            if (res._type == 'image') {

                var imageItem = {
                    _id: res._id,
                    url: $rootScope.asset.imageUrl(res._id)
                }

                //Insert
                controller.insert(imageItem);
            } else {
                console.log('Uploaded an asset', res);
            }
        }

        /////////////////////////////////////////////////////////

        function cancelCallback(res) {
            console.log('Failed', res)
        }
    }

    /////////////////////////////////////////////////////////

    controller.insert = function(image) {
        
        //Set Undo point
        this.buffer.set();

        //Restore the selection
        this.selection.restore();

        //Only proceed if we actually selected some images
        var str = '<img src="' + image.url + '" ng-src="{{$root.asset.imageUrl(' + "'" + image._id + "'" + ')}}"/>';

        //Insert
        this.insert.html(str);
        this.code.sync();

    }

    /////////////////////////////////////////////////////////

    return controller;
};


})(jQuery);