


(function($) {

$.Redactor.prototype.undoAction = function() {

    var controller = {}

    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('undoAction', 'Undo');
        this.button.addCallback(button, this.buffer.undo);
        this.button.setAwesome('undoAction', 'fa-undo');
    }

    /////////////////////////////////////////////////////////

    return controller;
};


/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////

$.Redactor.prototype.redoAction = function() {


    var controller = {}

    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('redoAction', 'Redo');
        this.button.addCallback(button, this.buffer.redo);
        this.button.setAwesome('redoAction', 'fa-repeat');
    }

    /////////////////////////////////////////////////////////

    return controller;
};



})(jQuery);