// var RedactorPlugins = window.RedactorPlugins || {};

// RedactorPlugins.toggleButton = function()
// {
//     return {
//         init: function()
//         {
//             var button = this.button.add('toggle-button', 'Toggle Button');

//             this.button.setAwesome('toggle-button', 'fa-minus-square-o');

//             this.button.addCallback(button, function()
//             {
//                 this.link.toggleClass('button');
//                 this.code.sync();
//             });
//         }
//     };
// };


(function($) {


$.Redactor.prototype.insertButton = function() {

    var controller = {}

    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('insertButton', 'Insert button');
        this.button.addCallback(button, controller.select);
        this.button.setAwesome('insertButton', ' fa-hand-pointer-o');
    }

    ////////////////////////////////////

    controller.select = function() {
    	this.link.toggleClass('button');
        this.code.sync();
    }

    /////////////////////////////////////////////////////////

    // controller.insert = function(images) {
    //     var self = this;

    //     //Restore the selection
    //     self.selection.restore();

    //     //Only proceed if we actually selected some images
    //     if (images.length) {
    //         var str = '';
    //         _.each(images, function(image) {
    //             str += '<img src="' + image.url + '" ng-src="{{$root.asset.imageUrl(' + "'" + image._id + "'" + ')}}"/>';
    //         })

    //         //Insert
    //         self.insert.html(str);
    //         self.code.sync();
    //     }
    // }

    /////////////////////////////////////////////////////////

    return controller;
};

})(jQuery);


		