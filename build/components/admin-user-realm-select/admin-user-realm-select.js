app.directive('userRealmSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-user-realm-select/admin-user-realm-select.html',
        controller: 'UserRealmSelectController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('UserRealmSelectController', function($scope,  $q, TypeService, $rootScope, FluroAccess, Selection, ModalService, FluroContent, $controller) {

    $scope.search = {};


    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }

    $scope.createText = 'Create new realm';

    /////////////////////////////////////
    /////////////////////////////////////

    //See whether a use is allowed to create new objects
    $scope.createEnabled = FluroAccess.can('create', 'realm');

    /////////////////////////////////////


    $scope.create = function() {
        ModalService.create('realm', {}, function(res) {

            res.account = $rootScope.user.account;
            $scope.realmTree.push(res);
            $scope.model.push(res);
            updateLists();
        })
    }



    //////////////////////////////////

    $scope.toggle = function(item) {
        if ($scope.contains(item)) {
            $scope.deselect(item)
        } else {
            $scope.select(item);
        }
    }

    //////////////////////////////////

    $scope.select = function(item) {
        if (!$scope.contains(item)) {
            $scope.model.push(item)
        }
    }

    //////////////////////////////////

    $scope.deselect = function(item) {

        var i = _.find($scope.model, function(e) {
            if (_.isObject(e)) {
                return e._id == item._id;
            } else {
                return e == item._id;
            }

        });
        if (i) {
            _.pull($scope.model, i)
        }
    }

    //////////////////////////////////

    $scope.contains = function(item) {
        return _.some($scope.model, function(i) {
            if (_.isObject(i)) {
                return i._id == item._id;
            } else {
                return i == item._id;
            }
        });
    }




    //////////////////////////////////////////////////////////

    //Now we get the realms from FluroAccess
    //$scope.realms = $rootScope.user.realms;



    //////////////////////////////////////////////////////////

    //Now we get the realms from FluroAccess
    //$scope.realms = $rootScope.user.realms;
    // controller.retrieveSelectableRealms = function(action, type, parentType, noCache) {


    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    $q.all([
        FluroAccess.retrieveSelectableRealms('create', 'persona'),
        FluroAccess.retrieveSelectableRealms('edit any', 'persona'),
        FluroAccess.retrieveSelectableRealms('edit own', 'persona'),
    ])
    .then(function(arr) {
        var create = arr[0];
        var editany = arr[1];
        var editown = arr[2];


        //TODO Merge here
        $scope.realmTree = create;

        updateLists();
    })




    ////////////////////////////////////////////////////

    $scope.$watch('search.terms', function() {
        updateLists();
    });

    
    function updateLists(selectFirst) {


        return formatRealmTrees($scope.model, $scope, $scope.realmTree, $scope.search.terms, null, selectFirst, TypeService.dictionary);

    }



});