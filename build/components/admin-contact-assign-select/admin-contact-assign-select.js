app.directive('contactAssignSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            event: '=ngEvent',
            defaultMode: '=defaultMode',
            confirmations: '=ngConfirmations',
        },
        templateUrl: 'admin-contact-assign-select/admin-contact-assign-select.html',
        controller: 'ContactAssignSelectController',
    };
});

app.controller('ContactAssignSelectController', function($scope, FluroContent, $uibModal, ModalService) {

    if (!$scope.model) {
        $scope.model = [];
    }

    ////////////////////////////////////////////////////

    $scope.proposed = {
        _new: {
            title: '',
            contacts: [],
        }
    }

    ////////////////////////////////////////////////////

    $scope.$watch('model', function() {

        $scope.grouped = _.chain($scope.model)
            .map(function(assignment) {


                return _.map(assignment.contacts, function(contact) {
                    return {
                        contact: contact,
                        assignment: assignment
                    }
                });

            })
            .flatten()
            .groupBy(function(item) {

                var denied = $scope.isUnavailable(item.contact, item.assignment);
                

                if(denied) {
                    return 'denied';
                }

                var confirmed = $scope.isConfirmed(item.contact, item.assignment);
                if(confirmed) {
                    return 'confirmed';
                }

                return 'unknown'


            })
            .value();

    }, true);

    ////////////////////////////////////////////////////

    $scope.sendRequest = function(assignment) {
        console.log('Send request to an assignment', assignment, $scope.event);

        FluroContent.endpoint('event/' + $scope.event._id + '/notify/' + assignment._id).save().$promise.then(function(res) {
            console.log('Reminder', res);
        }, function(err) {
            console.log('ERROR', err);
        });

    }

    /////////////////////////////////////////////

    $scope.removeContact = function(contact, assignment) {
        assignment.contacts = _.filter(assignment.contacts, function(c) {
                return (c._id != contact._id);
            })
            //_.pull(contact, assignment.contacts);
    }

    /////////////////////////////////////////////

    $scope.isUnavailable = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = contact._id == confirmation.contact._id;
            return correctAssignment && correctContact && confirmation.status == 'denied';
        })
    }

    /////////////////////////////////////////////

    $scope.isConfirmed = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = contact._id == confirmation.contact._id;
            return correctAssignment && correctContact && confirmation.status == 'confirmed';
        })
    }

    ///////////////////////////////////////////////////////////

    $scope.selectTeam = function() {

        var selection = {};
        var modal = ModalService.browse('team', selection);
        modal.result.then(addTeams,
            addTeams)

        function addTeams() {
            if (selection.items && selection.items.length) {
                var assignments = _.chain(selection.items)
                    .map(function(team) {
                        return team.assignments;
                    })
                    .flatten()
                    .value();

                //console.log('Add assignments', assignments)
                _.each(assignments, function(assignment) {
                    $scope.mergeAssignment(assignment);
                });
            }
        }
    }

    ///////////////////////////////////////////////////////////

    //Merge and reduce duplicates
    $scope.mergeAssignment = function(assignment) {

        //Find if this role is already included
        var existingAssignment = _.find($scope.model, function(ass) {

            //Match and minimize duplicates
            return ass.title.toLowerCase() == assignment.title.toLowerCase();
            //title: assignment.title
        });


        if (existingAssignment) {

            //Merge into the existing assignment
            existingAssignment.contacts = existingAssignment.contacts.concat(assignment.contacts);

            //Remove duplicates
            existingAssignment.contacts = _.uniq(existingAssignment.contacts, function(contact) {
                if (contact._id) {
                    return contact._id;
                } else {
                    return contact;
                }
            });

        } else {
            //Push the new assignment
            var copy = angular.copy(assignment);
            $scope.model.push(copy);
        }
    }

    ///////////////////////////////////////////////////////////

    $scope.addAssignment = function() {

        if (!$scope.proposed._new.title || !$scope.proposed._new.title.length) {
            //Do nothing without a title
            return;
        }

        //Merge or add proposed assignment
        $scope.mergeAssignment($scope.proposed._new);

        $scope.proposed._new = {
            title: '',
            contacts: [],
        };
    }


    ////////////////////////////////////////////////////

    // $scope.addSlot = function() {

    //     var mergeAssignment = $scope.mergeAssignment;

    //     ///////////////////////////////////////////////

    //     var modalInstance = $uibModal.open({
    //         template: '<contact-assign-modal ng-model="column" close="closeModal"></contact-assign-modal>',
    //         size: 'md',
    //         controller: function($scope) {


    //             // $scope.test = testEvent;
    //             // $scope.isModal =true;
    //             $scope.closeModal = function(result) {
    //                 console.log('close modal')
    //                 mergeAssignment(result);
    //                 $scope.$close();
    //             }
    //         }
    //     });
    // }

    $scope.dialog = {};


    $scope.addAssignment = function() {

        if (!$scope.proposed._new.title || !$scope.proposed._new.title.length) {
            //Do nothing without a title
            return;
        }

        //Merge or add proposed assignment
        $scope.mergeAssignment($scope.proposed._new);

        $scope.proposed._new = {
            title: '',
            contacts: [],
        };
    }

    $scope.removeAssignment = function(assignment) {
        _.pull($scope.model, assignment);
    }

})