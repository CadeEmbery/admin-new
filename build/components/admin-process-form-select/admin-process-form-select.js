app.directive('processFormSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-process-form-select/admin-process-form-select.html',
        controller: 'ProcessFormSelectController',
        link: function($scope, $element) {
            $scope.$element = $element;
        }
    };
});



app.controller('ProcessFormSelectController', function($scope) {

    ////////////////////////////////////////////////////

    //Create a model if it doesn't exist
    if (!$scope.model) {
        $scope.model = [];
    }


    $scope.reset = function() {
        //Create a new object
        $scope._new = {
            required: true,
        }
    }

    $scope.reset();

    ////////////////////////////////////////////////////

    $scope.add = function() {
        var insert = angular.copy($scope._new);
        insert.expanded = true;

        if (!$scope.model) {
            $scope.model = [];
        }

        $scope.model.push(insert);

        $scope.reset();

        console.log('REFOCUS')
        $scope.$element.find('.focus-back').focus();
    }

    ////////////////////////////////////////////////////

    $scope.remove = function(entry) {
        _.pull($scope.model, entry);
    }

    ////////////////////////////////////////////////////

    $scope.contains = function(entry) {
        return _.contains($scope.model, entry);
    }

})