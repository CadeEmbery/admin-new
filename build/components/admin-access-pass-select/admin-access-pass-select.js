app.directive('accessPassSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            host: '=ngHost',
        },
        templateUrl: 'admin-access-pass-select/admin-access-pass-select.html',
        controller: 'AccessPassSelectController',
    };
});

/////////////////////////////////////////////////////

app.controller('AccessPassSelectController', function($scope, $filter, FluroAccess, ModalService, FluroContent) {

	if(!$scope.model) {
		$scope.model = [];
	}

	$scope.search = {};
	$scope.settings = {};

	$scope.cardHeight = 75;



	$scope.settings.createEnabled = FluroAccess.can('create', 'policy');
	// console.log('CHECK', $scope.createEnabled);


	$scope.createNew = function() {

		var params = {};
    	var modal = ModalService.create('policy', params, function(res) {
    		console.log('RES',res);

    		$scope.model.push(res);
    		$scope.policies.push(res);
    		return updateList();
    	});

    	

	}


	////////////////////////////////////////
	////////////////////////////////////////

	$scope.$watch('search.terms', updateList);
	$scope.$watch('policies', updateList);


	function updateList() {

		// console.log('UPDATE LIST');
		var searchTerms = '';

		/////////////////////////////////////////////

		if($scope.search.terms && $scope.search.terms.length) {
			searchTerms = $scope.search.terms.toLowerCase();
		}

		/////////////////////////////////////////////

		$scope.filteredItems = _.chain($scope.policies)
		.filter(function(policy) {
			if(searchTerms && searchTerms) {
				return policy.title.toLowerCase().indexOf(searchTerms) != -1;  
			} else {
				return true;
			}
		})
		.sortBy(function(policy) {
			return policy.title;
		})
		.value();

		/////////////////////////////////////////////

		$scope.reshuffle();
	}


	////////////////////////////////////////

	FluroContent.resource('policy', true, true)
	.query()
	.$promise
	.then(function(res) {
		$scope.policies = res
	});

	////////////////////////////////////////
	////////////////////////////////////////

	////////////////////////////////////////
	////////////////////////////////////////
	////////////////////////////////////////
	////////////////////////////////////////

	$scope.reshuffle = function() {

		_.each($scope.policies, function(policy) {
			policy.order = null;
		});

		////////////////////////////////////////////////

		_.chain($scope.filteredItems)
		.sortBy(function(policy) {
			var selected = $scope.isSelected(policy);
			policy.selected = selected;
			if(selected) {
				return '0' + policy.title;
			} else {
				return '1' + policy.title;
			}
		}) 
		// .reverse()
		.map(function(policy, key) {
			policy.order = key;
		})
		.value()
	}
	////////////////////////////////////////
	
	$scope.select = function(policy) {
		$scope.model.push(policy);
		$scope.reshuffle();
	}

	$scope.deselect = function(policy) {
		var found = _.find($scope.model, {_id:policy._id});
		_.pull($scope.model, found);
		$scope.reshuffle();
	}

	$scope.toggle = function(policy) {
		var selected = $scope.isSelected(policy);
		if(!selected) {
			$scope.select(policy);
		} else {
			$scope.deselect(policy);
		}
	}


	////////////////////////////////////////

	$scope.isSelected = function(policy) {
		return _.some($scope.model, function(p) {
			return p._id == policy._id;
		})
	}

})