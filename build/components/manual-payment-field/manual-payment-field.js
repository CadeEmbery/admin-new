app.directive('manualPaymentField', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'manual-payment-field/manual-payment-field.html',
        controller: 'ManualPaymentFieldController',
    };
});

app.controller('ManualPaymentFieldController', function($scope) {

    ////////////////////////////////////////////////////

    //Create a model if it doesn't exist
    if (!$scope.model) {
        $scope.model = [];
    }


    $scope.getDate = function() {
        return new Date();
    }

    ////////////////////////////////////////////////////

    $scope.add = function(payment) {
        if (!payment || !payment.description.length) {
            return;
        }

        //Keep track of the date of when we've done it
        payment.date = payment.date || new Date();

        payment.amount = (payment.amount * 100);
        //Push the payment
        $scope.model.push(payment);

        $scope._proposed = {
            description: '',
            amount: 0,
        }
    }

    ////////////////////////////////////////////////////

    $scope.remove = function(entry) {
        _.pull($scope.model, entry);
    }

    ////////////////////////////////////////////////////

    $scope.contains = function(entry) {
        return _.includes($scope.model, entry);
    }


})