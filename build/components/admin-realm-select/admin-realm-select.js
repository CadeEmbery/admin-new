app.directive('realmSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            definedName: '=ngType',
            restrictedRealms: '=ngRestrict',
            popoverDirection: '@',
        },
        templateUrl: 'admin-realm-select/admin-realm-select.html',
        controller: 'RealmSelectController',
    };
});





// app.controller('RealmSelectListController', function($scope) {


// });




app.directive('realmSelectItem', function($compile, $templateCache) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            realm: '=ngModel',
            selection: '=ngSelection',
            searchTerms: '=ngSearchTerms',
        },
        templateUrl: 'admin-realm-select/admin-realm-select-item.html',
        link: function($scope, $element, $attrs) {



            $scope.contains = function(realm) {
                return _.some($scope.selection, function(i) {
                    if (_.isObject(i)) {
                        return i._id == realm._id;
                    } else {
                        return i == realm._id;
                    }
                });
            }

            $scope.toggle = function(realm) {

                var matches = _.filter($scope.selection, function(existingRealmID) {

                    var searchRealmID = realm._id;
                    if (existingRealmID._id) {
                        existingRealmID = existingRealmID._id;
                    }

                    return (searchRealmID == existingRealmID);
                })

                ////////////////////////////////

                if (matches.length) {

                    _.each(matches, function(match) {
                        _.pull($scope.selection, match);
                    })
                } else {

                    if(!$scope.selection) {
                        $scope.selection = [];
                    }
                    $scope.selection.push(realm);
                }

            }

            ///////////////////////////////////////////////////

            function hasActiveChild(realm) {

                var active = $scope.contains(realm);

                if (active) {
                    return true;
                }

                if (realm.children && realm.children.length) {
                    return _.some(realm.children, hasActiveChild);
                }
            }

            ///////////////////////////////////////////////////

            $scope.isExpanded = function(realm) {
                if (realm.expanded) {
                    return realm.expanded;
                }

                return $scope.activeTrail(realm);
            }

            ///////////////////////////////////////////////////

            $scope.activeTrail = function(realm) {
                if (!realm.children) {
                    return;
                }

                return _.some(realm.children, hasActiveChild);
            }



            var template = '<realm-select-item ng-model="subRealm" ng-search-terms="searchTerms" ng-selection="selection" ng-repeat="subRealm in realm.children | filter:searchTerms | orderBy:' + "'title'" + '"></realm-select-item>';

            var newElement = angular.element(template);
            $compile(newElement)($scope);
            $element.find('.realm-select-children').append(newElement);
        }
    };
});



////////////////////////////////////////////////////

function formatRealmTrees(model, scopeObject, realmTree, searchTerms, restrictedRealms, selectFirst, dictionary) {


    scopeObject.trees = {};

    // console.log('REALM TREE', realmTree)
    ///////////////////////////////////////

    mapTree(null, 'tree');
    //TURN THIS ON LATER
    // if()
    mapTree('team', 'groupTree');

    ///////////////////////////////////////

    scopeObject.trees['grouped'] = _.chain(scopeObject.trees['team'])
        .reduce(function(set, realm) {

            ///////////////////////////////////////

            var definitionBlock

            ///////////////////////////////////////

            if(realm.definition) {
                realm._discriminator = realm.definition;
            }
            if (realm._discriminator) {
                definitionBlock = dictionary[realm._discriminator];
            }



            if (!definitionBlock) {
                definitionBlock = {
                    title: 'Group / Team',
                    plural: 'Groups / Teams',
                    singular: 'Group / Team',
                }
            }

            ///////////////////////////////////////

            var existing = set[realm._discriminator];
            if (!existing) {
                existing = set[realm._discriminator] = {
                    title: definitionBlock.title,
                    plural: definitionBlock.plural,
                    realms: []
                }
            }

            existing.realms.push(realm);

            return set;
        }, {})
        .values()
        .value()

    ///////////////////////////////////////

    ///////////////////////////////////////

    function mapTree(discriminatorType, treeKey) {
        // $scope.tree = realmTree;

        // return;

        var isSelectableLookup = {};
        var hasSelectionCriteria;


        //No restrictions
        if (!restrictedRealms || !restrictedRealms.length) {
            hasSelectionCriteria = false;
            // $scope[treeKey] = realmTree;

            if (selectFirst) {
                if (realmTree.length == 1) {

                    var first = _.first(realmTree);
                    if (!first.children || !first.children.length) {
                        model = [first];
                    }
                }
            }
            // return;



        } else {
            hasSelectionCriteria = true;

            isSelectableLookup = _.reduce(restrictedRealms, function(set, realmID) {
                set[realmID] = true;
                return set;
            }, {})

        }

        ///////////////////////////////////////

        function canSelect(realm) {

            var canBeSelected = (hasSelectionCriteria && isSelectableLookup[realm._id]) || !hasSelectionCriteria;
            var isCorrectDiscriminator;

            if (discriminatorType) {
                isCorrectDiscriminator = realm._discriminatorType === discriminatorType;
            } else {
                isCorrectDiscriminator = !realm._discriminatorType;
            }

            // console.log(realm.title, realm._discriminator, realm._discriminatorType, isCorrectDiscriminator);
            return canBeSelected && isCorrectDiscriminator;
        }

        ///////////////////////////////////////

        function filterSelectable(realm, parentArray) {

            var copiedRealm;

            ///////////////////////////////////////

            var matchesSearchCriteria = true;
            if (searchTerms && searchTerms.length) {
                matchesSearchCriteria = _.includes(realm.title.toLowerCase(), String(searchTerms.toLowerCase()))
            }



            //If we can select this realm then 
            //add it into the parent array
            var include = canSelect(realm) && matchesSearchCriteria

            // if(matchesSearchCriteria) {
            //     console.log('MATCHES', realm.title, matchesSearchCriteria, include)
            // }


            // console.log('TREE CAN SELECT', realm.title, realm._discriminatorType, discriminatorType, include)
            if (include) {


                copiedRealm = _.pick(realm, [
                    '_id',
                    'title',
                    'definition',
                    '_discriminator',
                    '_discriminatorType',
                    'color',
                    'bgColor',
                ])

                copiedRealm.children = [];


                // results.push(realm);
                parentArray.push(copiedRealm)
            }

            ///////////////////////////////////////

            //If the realm has branches, recursively walk down the branches
            if (realm.children && realm.children.length) {

                var targetArray = parentArray;
                if (copiedRealm && copiedRealm.children) {
                    targetArray = copiedRealm.children;
                }

                //Loop through the children
                _.each(realm.children, function(child) {
                    filterSelectable(child, targetArray)
                });
            }
        }

        ///////////////////////////////////////

        //Now we need to get fancy
        var filteredTree = [];


        _.each(realmTree, function(realm) {
            filterSelectable(realm, filteredTree);
        });


        // console.log('FILTERED TREE', filteredTree)

        ////////////////////////////////////////////////////////

        if (selectFirst) {
            //If only one realm is available and no realm is selected
            //select it by default
            if (filteredTree.length == 1 && !model.length) {

                var first = _.first(filteredTree);

                //If there are no children select this only realm by default
                if (!first.children || !first.children.length) {
                    model = [first];
                }
            }
        }

        if (!discriminatorType) {
            scopeObject.trees['none'] = filteredTree;
        } else {
            //THIS IS WHERE WE UNCOMMENT IF WE WANT THE OTHER KINDS OF REALMS
            scopeObject.trees[discriminatorType] = filteredTree;
        }

    }

}

app.controller('RealmSelectController', function($scope, $rootScope, TypeService, FluroAccess, FluroSocket) {


    $scope.search = {};
    $scope.loading;



    $scope.dynamicPopover = {
        templateUrl: 'admin-realm-select/admin-realm-popover.html',
    };

    if (!$scope.popoverDirection) {
        $scope.popoverDirection = 'bottom';
    }


    ////////////////////////////////////////////////////

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }


    $scope.previewRealms = function() {
        return _.filter($scope.model, function(realm) {
            return !realm._discriminator;
        })
    }

    //////////////////////////////////////////////////

    FluroSocket.on('content.create', socketUpdate);

    //////////////////////////////////////////////////

    function socketUpdate(eventData) {
        //New realm was created
        console.log('New realm available')
        if (eventData.data._type == 'realm') {
            return reloadAvailableRealms();
        }
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    //////////////////////////////////////////////////

    $scope.$on("$destroy", function() {
        FluroSocket.off('content.create', socketUpdate);
    });

    ////////////////////////////////////////////////////

    $scope.$watch('search.terms', function() {
        updateLists();
    });

    ////////////////////////////////////////////////////


    $scope.activeIndex = 0;

    function updateLists(selectFirst) {
        formatRealmTrees($scope.model, $scope, $scope.realmTree, $scope.search.terms, $scope.restrictedRealms, selectFirst, TypeService.dictionary);
    

        // console.log('TREES?', $scope.trees)
        var topRealms = _.chain($scope.trees)
        .map(function(tree) {
            return tree;
        })
        .flatten()
        .compact()
        .value();

        if(topRealms.length == 1) {
            if(!topRealms[0].children || !topRealms[0].children.length) {
                if(!$scope.model.length) {
                    $scope.model.push(topRealms[0]);
                }
            }
        }

    }



    ////////////////////////////////////////////////////

    function reloadAvailableRealms() {

        $scope.loading = true;

        //We need to wait until all the defined types have been loaded
        TypeService.definedTypes.$promise.then(function() {

            //If the parent definition exists get it otherwise get the primitive type
            var typeDefinition = TypeService.getTypeFromPath($scope.definedName);

            if (!typeDefinition) {
                return;
            }

            ////////////////////////////////////////////////////////

            var parentType = $scope.definedName;
            if (typeDefinition.parentType) {
                parentType = typeDefinition.parentType;
            }

            //Now we get the realm tree from FluroAccess service
            FluroAccess.retrieveSelectableRealms('create', $scope.definedName, parentType)
                .then(function(realmTree) {

                    

                    $scope.realmTree = realmTree;
                    updateLists(true);
                    $scope.loading = false;


                }, function(err) {
                    $scope.loading = false;
                });
        });
    }

    //////////////////////////////////

    //One for good measure
    reloadAvailableRealms();

    //////////////////////////////////

    $scope.isSelectable = function(realm) {

        var realmID = realm;
        if (realmID._id) {
            realmID = realmID._id;
        }

        //////////////////////////////////

        if ($scope.restrictedRealms && $scope.restrictedRealms.length) {

            //Check if the realm we are asking about is in the restricted realms
            return _.some($scope.restrictedRealms, function(restrictedRealmID) {
                if (restrictedRealmID._id) {
                    restrictedRealmID = restrictedRealmID._id;
                }
                return realmID == restrictedRealmID
            });

        } else {
            return true;
        }
    }

    //////////////////////////////////
    //////////////////////////////////
    //////////////////////////////////

    $scope.contains = function(realm) {
        return _.some($scope.model, function(i) {
            if (_.isObject(i)) {
                return i._id == realm._id;
            } else {
                return i == realm._id;
            }
        });
    }

    $scope.toggle = function(realm) {

        var matches = _.filter($scope.model, function(r) {
            var id = r;
            if (_.isObject(r)) {
                id = r._id;
            }

            return (id == realm._id);
        })

        ////////////////////////////////

        if (matches.length) {
            $scope.model = _.reject($scope.model, function(r) {
                var id = r;
                if (_.isObject(r)) {
                    id = r._id;
                }

                return (id == realm._id);
            });
        } else {
            $scope.model.push(realm);
        }

    }



});