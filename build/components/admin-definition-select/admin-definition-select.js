app.directive('definitionSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            filterParents: '=ngParents',
        },
        templateUrl: 'admin-definition-select/admin-definition-select.html',
        controller: 'DefinitionSelectController',
    };
});


app.filter('definitionParents', function() {
    
    return function(definitions, keys) {

        return _.filter(definitions, function(def) {
            return _.contains(keys, def.parentType);
        })
    };
});




app.controller('DefinitionSelectController', function($scope, $filter, TypeService) {

    
    //$scope.types = TypeService.types;
    //$scope.types = TypeService.getAllCreateableTypes();

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }


    //////////////////////////////////

    TypeService.refreshDefinedTypes(true).then(function() {

        $scope.types = TypeService.definedTypes;
        refilter()

        $scope.$watch('filterParents', refilter);

    });


    //////////////////////////////////

    function refilter() {

        var array = $scope.filterParents;
        $scope.filteredTypes = $filter('definitionParents')($scope.types, array);

    }
    //////////////////////////////////

    $scope.contains = function(item) {
        return _.some($scope.model, function(i) {
            if (_.isObject(i)) {
                return i.path == item.definitionName;
            } else {
                return i == item.definitionName;
            }
        });
    }

    $scope.toggle = function(item) {

        var matches = _.filter($scope.model, function(r) {
            var id = r;
            if (_.isObject(r)) {
                id = r.path;
            }

            return (id == item.definitionName);
        })

        ////////////////////////////////

        if (matches.length) {
            $scope.model = _.reject($scope.model, function(r) {
                var id = r;
                if (_.isObject(r)) {
                    id = r.path;
                }

                return (id == item.definitionName);
            });
        } else {
            $scope.model.push(item.definitionName);
        }

    }

});