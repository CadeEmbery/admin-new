app.directive('columnSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            definition: '=',
        },
        templateUrl: 'admin-column-select/admin-column-select.html',
        controller: 'ColumnSelectController',
        link: function(scope, element, attrs) {


            scope.$element = element;

        }
    };
});



app.directive('queryColumnSelect', function(PathfinderService) {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            testData: '=ngTestData',
            definition: '=',
        },
        templateUrl: 'admin-column-select/query-column-select.html',
        controller: 'ColumnSelectController',
        link: function($scope, $element, $attrs) {


            $scope.$element = $element;


            $scope.$watch('testData', function(items) {


                // console.log('ITEMS', items);

                var output = _.chain(items)
                    .reduce(function(set, item) {

                        //We have the column
                        var columns = PathfinderService.extract(item);

                        _.each(columns, function(column) {
                            var existing = set[column.key];
                            if (!existing) {
                                existing = set[column.key] = column;
                                existing.examples = [];
                            }

                            //add the examples
                            if(existing.examples.indexOf(column.value) == -1) {
                                existing.examples.push(column.value)
                            }
                        })

                        return set;
                    }, {})
                    .values()
                    .sortBy(function(entry) {
                        return entry.key
                    })
                    // .map(function(field) {

                    //     switch(field.type) {
                    //         case 'object':
                    //         case 'date':
                    //         case 'array':

                    //         return field;
                    //         break
                    //     }
                    //     if(field.examples && field.examples.length) {
                    //         field.readable = field.key + ' (' + field.examples.join(', ') + ')' 
                    //     }

                    //     return field;
                    // })
                    .value();


                // console.log('OUTPUT', output);


                $scope.rows = output;



            })


        }
    };
});



app.controller('ColumnSelectController', function($scope) {

    ////////////////////////////////////////////////////

    //Create a model if it doesn't exist
    if (!$scope.model) {
        $scope.model = [];
    }



    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////


    // function getFieldPathTrail(array, target, trail, result) {
    //     //return $scope.getTrail($scope.model, field, []);

    //     for (var key in array) {
    //         var field = array[key];

    //         if (field == target) {

    //             if(!field.asObject && field.directive != 'embedded') {
    //                 trail.push(field.key);
    //             } else {

    //                 if (field.minimum == field.maximum == 1) {
    //                     trail.push(field.key);
    //                 } else {
    //                     trail.push(field.key + '[i]');
    //                 }

    //             }


    //             //trail.push(field.key);
    //             result.trail = trail.slice();
    //             return;

    //             //return callback(trail.slice());
    //         }

    //         if (field.fields && field.fields.length) {

    //             if (field.asObject || field.directive == 'embedded') {

    //                 if (field.minimum == field.maximum == 1) {
    //                     trail.push(field.key);
    //                 } else {
    //                     trail.push(field.key + '[i]');
    //                 }

    //             }

    //             getFieldPathTrail(field.fields, target, trail, result);

    //             if (field.asObject || field.directive == 'embedded') {
    //                 trail.pop();
    //             }

    //         }
    //     }
    // }


    $scope.$watch('definition.fields', function(fields) {
        if (fields) {
            $scope.availableFields = extractFieldsFromDefinitionFields(fields, '0');
        }
    })



    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////


    // $scope.$watchCollection('definition.fields', function(fields) {


    //     //Get the available field groups
    //     var availableFields = _.chain(fields)
    //         .reduce(function(result, field, key) {

    //             if (field.type == 'group' || field.directive) {
    //                 result.push(field);
    //             }

    //             if (field.fields && field.fields.length) {
    //                 //Recursive search
    //                 var folders = getFlattenedFields(field.fields);

    //                 if (folders.length) {
    //                     result.push(folders)
    //                 }
    //             }

    //             return result;

    //         }, [])
    //         .flatten()
    //         .filter(function(field) {

    //             switch (field.type) {
    //                 case 'void':
    //                     return;
    //                     break;
    //             }
    //             return field
    //         })
    //         .compact()
    //         .map(function(field) {

    //             var trail = [];
    //             var result = {};

    //             getFieldPathTrail(fields, field, trail, result);

    //             field.trail = 'data.' + result.trail.join('.');

    //             field.indentedLabel = '';

    //             _.times(result.trail.length - 1, function() {
    //                 field.indentedLabel += '--';
    //             })

    //             if (field.indentedLabel.length) {
    //                 field.indentedLabel += ' ' + field.title;
    //             } else {
    //                 field.indentedLabel = field.title;
    //             }

    //             return field;
    //         })

    //     .value();

    //     $scope.availableFields = availableFields;
    // })


    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////



    //Create a new object
    $scope._new = {}


    var renderers = []

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    renderers.push({
        title: 'Date and Time',
        input: 'date',
        key: 'datetime',
        description: "Format a date field as '7:30pm - 1 Jun 2016'",
    });

    renderers.push({
        title: 'Date',
        input: 'date',
        key: 'date',
        description: "Format a date field as '1 Jun 2016'",
    });

    renderers.push({
        title: 'Time',
        input: 'date',
        key: 'time',
        description: "Format a date field as the time with meridian eg. '7:30pm'",
    });

    renderers.push({
        title: 'Date Format - (Year)',
        input: 'date',
        key: 'year',
        description: "Format a date field as the year  eg. '2016'",
    });

    renderers.push({
        title: 'Date Format - (Day)',
        input: 'date',
        key: 'date_day',
        description: "Format a date field as the day of the month  eg. '17'",
    });

    renderers.push({
        title: 'Date Format - (Weekday)',
        input: 'date',
        key: 'date_weekday',
        description: "Format a date field as the weekday name eg. 'Thursday'",
    });

    renderers.push({
        title: 'Date Format - (Month Name)',
        input: 'date',
        key: 'date_monthname',
        description: "Format a date field as the month name eg. 'February'",
    });




    renderers.push({
        title: 'Date Format - (Weekday relative)',
        input: 'number',
        key: 'date_day_relative',
        description: "Format the number of days from now. eg. '0, Today, 1, Tomorrow, 2, Friday'",
    });

    renderers.push({
        title: 'Currency',
        input: 'integer',
        key: 'currency',
        description: "Format an amount of the smallest currency unit eg. (179 cents) as '1.79'",
    });

    renderers.push({
        title: 'Number',
        input: 'integer',
        key: 'number',
        description: "Format a number value",
    });



    renderers.push({
        title: 'Definition Name',
        input: 'string',
        key: 'definitionName',
        description: "Show the human readable title of a definition",
    });






    renderers.push({
        title: 'Age',
        input: 'date',
        key: 'age',
        description: "Format a date field as the number of years since eg. '19'",
    });





    renderers.push({
        title: 'Name (Capitalize)',
        input: 'string',
        key: 'capitalize',
        description: "Capitalize the name of a contact/user",
    });






    if ($scope.definition && $scope.definition.parentType == 'interaction') {
        renderers.push({
            title: 'Contact Names (Capitalize)',
            input: 'string',
            key: 'contacts',
            description: "Show all contact names linked to the interaction",
        });

    }

    renderers.push({
        title: 'Multiple Line Long Text',
        input: 'string',
        key: 'longtext',
        description: "Wraps a large amount of text onto multiple lines",
    });

    renderers.push({
        title: 'Time ago',
        input: 'date',
        key: 'timeago',
        description: "Format a date field as a relative description eg. '10 days ago'",
    });

    renderers.push({
        title: 'Thumbnail',
        input: 'object',
        key: 'thumbnail',
        description: "Format an image reference or an image ID field as a thumbnail image",
    });

    renderers.push({
        title: 'Phone Number',
        input: 'string',
        key: 'phoneNumber',
        description: "Format a show an actionable phone number",
    });

    renderers.push({
        title: 'Email Address',
        input: 'string',
        key: 'email',
        description: "Format and show an actionable email address",
    });

    renderers.push({
        title: 'Persona Avatar',
        input: 'object',
        key: 'personaAvatar',
        description: "Profile photo of a user persona",
    });

    renderers.push({
        title: 'Contact Avatar',
        input: 'object',
        key: 'contactAvatar',
        description: "Profile photo of a contact",
    });

    renderers.push({
        title: 'Realm Dots',
        input: 'object',
        key: 'realmDots',
        description: "Coloured dots for each realm",
    });

    renderers.push({
        title: 'Filesize',
        input: 'integer',
        key: 'filesize',
        description: "Format a integer of kbs to a more readable value eg. 125mb",
    });

    renderers.push({
        title: 'Dimensions',
        input: 'image',
        key: 'dimensions',
        description: "If the item is an image prints simplified dimensions in pixels eg. '1920x1080'",
    });

    renderers.push({
        title: 'Privacy',
        input: 'item',
        key: 'privacy',
        description: "Shows a lock or unlock icon representing the privacy setting of the content",
    });


    renderers.push({
        title: 'Expiry / Renewal Date',
        input: 'item',
        key: 'expiry',
        description: "Show the next expiry/renewal date of a purchase",
    });

    renderers.push({
        title: 'Boolean (True or False)',
        input: 'boolean',
        key: 'boolean',
        description: "Prints a tick if field is 'true'",
    });


    renderers.push({
        title: 'Attendance Timeline',
        input: 'array',
        key: 'attendance',
        description: "Prints columns representing each event attended",
    });



    renderers.push({
        title: 'Subfield: Phone Numbers',
        input: 'array',
        key: 'field.phoneNumbers',
        description: "Renders all phoneNumbers found on items in this column",
    });

    renderers.push({
        title: 'Subfield: Email Addresses',
        input: 'array',
        key: 'field.emails',
        description: "Renders all email addresses found on items in this column",
    });

    renderers.push({
        title: 'Columns',
        input: 'array',
        key: 'columns',
        description: "Renders each key in a specified object as a seperate column",
    });

    renderers.push({
        title: 'Columns: Checkin Dots',
        input: 'array',
        key: 'columns.checkinDots',
        description: "Renders each key of a checkin tally object as a seperate column",
    });

    renderers.push({
        title: 'Columns: Boolean',
        input: 'array',
        key: 'columns.boolean',
        description: "Renders each key of an object as a tick/cross",
    });


    renderers.push({
        title: 'JSON Output',
        // input:'array',
        key: 'json',
        description: "Renders the JSON output of this value",
    });

    ////////////////////////////////////////////////////

    $scope.renderers = _.sortBy(renderers, function(renderer) {
        return renderer.title;
    });

    ////////////////////////////////////////////////////

    $scope.add = function() {
        var insert = angular.copy($scope._new);
        $scope.model.push(insert);
        $scope._new = {};

        $scope.$element.find('.focus-back').focus();
    }

    ////////////////////////////////////////////////////

    $scope.remove = function(entry) {
        _.pull($scope.model, entry);
    }

    ////////////////////////////////////////////////////

    $scope.contains = function(entry) {
        return _.contains($scope.model, entry);
    }

})