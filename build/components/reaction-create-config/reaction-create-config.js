app.directive('reactionCreateConfig', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'reaction-create-config/reaction-create-config.html',
        link: function($scope, $element, $attrs) {

        	if(!$scope.model) {
                $scope.model = [{}];
            }


            $scope.addItem = function() {
                $scope.model.push({})
            }

            $scope.removeItem = function(item) {
                _.pull($scope.model, item);
            }

        }
    };
});


