app.directive('unavailableList', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            lookingAt: '=lookingAt',
        },
        templateUrl: 'admin-unavailable-list/admin-unavailable-list.html',
        controller: 'UnavailableListController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.service('UnavailableAssignmentService', function($filter) {
    var service = {}

    ///////////////////////////////////

    ///////////////////////////////////////

    service.periodSummary = function(contact) {

        if (!contact.periods || !contact.periods.length) {
            return;
        }

        var string = _.chain(contact.periods)
            .map(function(item) {

                var startDate = new Date(item.startDate);
                var endDate = new Date(item.endDate);
                var difference = (endDate - startDate);
                var readable = $filter('readableDate')(item);
                //If it's all day
                if(difference == 86399999) {
                    readable = startDate.format('l j M');
                }



                return (item.description || 'Unavailable') + ' during ' + readable;
            })
            .value()
            .join('<br/>');

        if (!string.length) {
            return;
        }

        return string;
    }

    ///////////////////////////////////////

    service.deniedSummary = function(contact) {

        if (!contact.assignments || !contact.assignments.length) {
            return;
        }

        var string = _.chain(contact.assignments)
            .map(function(conflict) {

                if (conflict.description && conflict.description.length) {
                    return '\'' + conflict.title + '\' (' + conflict.description + ')';
                } else {
                    return 'Unavailable for \'' + conflict.title + '\'';
                }
            })
            .value()
            .join('<br/>');

        if (!string.length) {
            return;
        }

        return string;
    }


    ///////////////////////////////////////

    service.conflictSummary = function(contextEvent, contact) {

        if (!contact.conflicts || !contact.conflicts.length) {
            return;
        }

        var string = _.chain(contact.conflicts)
            .compact()
            .reduce(function(set, assignment) {

                if (!assignment.event) {
                    // console.log('No event for assignment', assignment);
                    return set;
                }
                var eventID = assignment.event._id;

                var existing = _.find(set, {
                    _id: eventID
                });

                if (!existing) {
                    existing = {
                        _id: eventID,
                        title: "'" + assignment.event.title + "'",
                        startDate: new Date(assignment.event.startDate).format('g:ia'),
                        endDate: new Date(assignment.event.endDate).format('g:ia'),
                        rosters: [],
                    }

                    set.push(existing);
                }

                ///////////////////////////////////////////////////////////

                //Get the roster title from the assignment
                var rosterTitle = _.get(assignment, 'roster.title');

                //Use unspecified if no roster is attached
                if (!rosterTitle) {
                    rosterTitle = 'unspecified';
                }

                ///////////////////////////////////////////////////////////

                //Check if the roster exists already
                var existingRosterEntry = _.find(existing.rosters, {
                    title: rosterTitle
                });

                //If it doesnt then push it in
                if (!existingRosterEntry) {
                    existingRosterEntry = {
                        title: rosterTitle,
                        positions: [],
                    }
                    existing.rosters.push(existingRosterEntry);
                }

                //Add the assignment name to the roster entry
                existingRosterEntry.positions.push(assignment.title);

                return set;
            }, [])
            .map(function(event) {


                var eventName = event.title;


                ///////////////////////////////////////

                if(contextEvent) {
                    var currentEventID = contextEvent;
                    if(currentEventID._id) {
                        currentEventID = currentEventID._id;
                    }

                    if(currentEventID == event._id) {
                        eventName = ' at this event';
                    }
                }

                ///////////////////////////////////////

                var names = _.chain(event.rosters)
                .map(function(roster) {
                    var rosterTitle = '';
                    if(roster.title) {
                        rosterTitle = roster.title + ' - ';
                    }

                    return _.map(roster.positions, function(position) {
                        return rosterTitle + position;
                    })

                })
                
                .flatten()
                .value()
                .join('<br/>');

                if (event.startDate == event.endDate) {
                    return '<strong>' + event.title + '</strong><br/>' + names;
                    // return names + ' at ' + event.title;
                } else {
                    return '<strong>' + event.startDate + '-' + event.endDate + ' ' + event.title + '</strong><br/>' + names;
                    // return names + ' at ' + event.startDate + '-' + event.endDate + ' ' + event.title;
                }

            
                
            })
            .value()
            .join('<br/>');

        if (!string.length) {
            return;
        }

        return string;

    }

    ///////////////////////////////////

    return service;
})
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

app.controller('UnavailableListController', function($scope, UnavailableAssignmentService, $filter, FluroContent, $timeout, ModalService) {

    $scope.$watch('model', refresh);
    $scope.$watch('lookingAt', refresh);

    $scope.processing = true;

    ///////////////////////////////////////

    var inflightRequest;


    $scope.getTooltip = function(contact) {

        var conflicts = UnavailableAssignmentService.conflictSummary($scope.model, contact);
        var denied = UnavailableAssignmentService.deniedSummary(contact);
        var periods = UnavailableAssignmentService.periodSummary(contact);

        var string;

        ////////////////////////////

        if (periods) {
            if (!string) {
                string = '';
            }
            string += '<div style="padding: 5px 0; border-top: 2px solid #fff;"><h6 class="text-uppercase">Away</h6><p>' + periods + '</p></div>';
        }

        if (conflicts) {
            if (!string) {
                string = '';
            }
            string += '<div style="padding: 5px 0; border-top: 2px solid #fff;"><h6 class="text-uppercase">Already rostered</h6><p>' + conflicts + '</p></div>';
        }

        if (denied) {
            if (!string) {
                string = '';
            }
            string += '<div style="padding: 5px 0; border-top: 2px solid #fff;"><h6 class="text-uppercase">Marked Unavailable</h6><p>' + denied + '</p></div>';
        }

        if (!string) {
            return;
        }

        string = '<div class="text-left tooltip-block"><h4 class="text-capitalize">' + contact.title + '</h4>' + string + '</div>';

        ////////////////////////////


        return string;



    }


    

    ///////////////////////////////////////

    function refresh() {

        $scope.processing = true;

        var event = $scope.model;

        if (!event) {
            $scope.processing = false;
            $scope.unavailable = [];
            $scope.conflicts = [];
            return;
        }

        var eventID = event;
        if (eventID._id) {
            eventID = eventID._id;
        }


        var lookingAtID = $scope.lookingAt;
        if (lookingAtID && lookingAtID._id) {
            lookingAtID = lookingAtID._id;
        }

        if (_.isObject(lookingAtID)) {
            lookingAtID = null;
        }

        inflightRequest = FluroContent.endpoint('assignments/unavailable/' + eventID, true, true).query({
            context: lookingAtID
        }).$promise;

        inflightRequest.then(function(res) {

            $scope.processing = false;
            // $scope.list = res;

            $scope.unavailable = _.filter(res, function(contact) {
                return contact.periods.length || contact.assignments.length;
            })

            $scope.conflicts = _.filter(res, function(contact) {
                return contact.conflicts.length;
            })
        }, function(err) {
            $scope.processing = false;

            console.log('ERROR LOADING UNAVAILABLE LIST', err);
            // $scope.list = []
            $scope.unavailable = [];
            $scope.conflicts = [];
        })
    }

    ///////////////////////////////////////

    $scope.editAssignment = function(assignment) {

        var itemObject = angular.copy(assignment);
        itemObject._type = 'assignment';
        itemObject.realms = $scope.model.realms;


        // //console.log('MODAL EDIT', itemObject);
        ModalService.edit(itemObject, function(res) {

            console.log('DONE')
            $timeout(function() {
                return refresh();
            })
        });
    }

    ///////////////////////////////////////

});