app.directive('printable', function($compile, $timeout) {
    return {
        restrict: 'A',
        scope: {
            title: '=printable',
            printDisabled: '=printDisabled',
        },
        link: function($scope, $element, $attributes) {

            if (!$scope.printDisabled) {
                var template = ('<div class="clearfix hidden-print hidden-xs"><a class="print-button btn btn-primary" ng-click="print()"><span>Print \'{{title}}\'</span><i class="far fa-print"></i></a></div>');
                var cTemplate = $compile(template)($scope);
                //$element.before(cTemplate);
                $element.prepend(cTemplate);
            }


            $scope.print = function() {


                var cssFiles = $('link');
                var printDivCSS = '';

                _.each(cssFiles, function(link) {
                    printDivCSS += '<link href="' + $(link).attr('href') + '" rel="stylesheet" type="text/css">';
                })


                var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
                WinPrint.document.write(printDivCSS)
                WinPrint.document.write($element[0].innerHTML);
                WinPrint.document.close();
                WinPrint.focus();
                WinPrint.onload = function() {

                    $timeout(function() {
                        WinPrint.print();
                        WinPrint.close();
                    }, 500)


                }

               

            }




        }
    };
});