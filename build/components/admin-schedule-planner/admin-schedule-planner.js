app.directive('schedulePlanner', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-schedule-planner/admin-schedule-planner.html',
        controller: 'SchedulePlanController',
    };
});

app.directive('cellEditable', function($timeout) {

    return {
        restrict: 'C',
        require: '^schedulePlanner',
        scope: true,
        link: function($scope, $element, $attr, tableCtrl) {




            $scope.deselect = function(event) {

                $timeout(function() {


                    event.stopPropagation();
                    event.preventDefault();
                    tableCtrl.selectedCell = null;
                })
            }

            ////////////////////////////////////

            $scope.isSelected = function() {
                return (tableCtrl.selectedCell == $element);
            }

            ///////////////////////////////////////////////

            $element.on('click', function(event) {

                console.log('CLICKED');
                if (tableCtrl.selectedCell != $element) {
                    tableCtrl.selectedCell = $element;

                    $timeout(function() {


                        var input = $element.find('.cell-input').eq(0);
                        input.focus().select();

                    }, 300)
                }
            })



        }
    };
});


app.directive('multiSchedulePlanner', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            event: '=ngEvent',
        },
        templateUrl: 'views/ui/multi-schedule-planner.html',
        controller: 'MultiSchedulePlaner',
    };
});






//////////////////////////////////////////////////////////////////////////

app.controller('MultiSchedulePlaner', function($scope) {

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }


    //Watch for changes to the start date
    $scope.$watch('event.startDate', function() {
        $scope.defaultDate = new Date($scope.event.startDate)
    });


    $scope.removePlanner = function(planner) {
        _.pull($scope.model, planner);
    }

    $scope.addPlanner = function() {

        var newPlanName = 'New Plan';
        if ($scope.model.length) {
            newPlanName += ' ' + $scope.model.length;
        }

        $scope.model.push({
            title: newPlanName,
            startDate: $scope.defaultDate,
        })
    }


});



//////////////////////////////////////////////////////////////////////////

app.controller('SchedulePlanController', function($scope, $q, FluroContent, ModalService) {

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = {};
    }

    if (!$scope.model.schedules) {
        $scope.model.schedules = [];
    }

    $scope.defaults = {}

    this.selectedCell = {};


    console.log('PLAN MODEL', $scope.model);

    $scope.scheduleDragOptions = {
        handle: ' .handle',
        placeholder: function() {

                var count = $scope.model.teams.length;
                count += 3;
                return '<tr class="as-sortable-placeholder"><td colspan="' + (count) + '"></td></tr>';
            }
            // items: ' .panel:not(.panel-heading)'
            // axis: 'y'
    }

    $scope.model.schedules = _.map($scope.model.schedules, function(item) {
        if (!item) {
            return {}
        } else {
            return item;
        }
    })

    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////

    // //Default
    // var startDate = new Date($scope.model.startDate);

    //Watch for the start date
    $scope.$watch('model.startDate + model.event.startDate', function() {


        if ($scope.model.startDate) {
            console.log('USE PLAN START DATE', $scope.model, $scope.model.startDate)
            return $scope.defaults.startDate = new Date($scope.model.startDate);
        }

        if ($scope.model.event && $scope.model.event.startDate) {
            console.log('USE EVENT START DATE', $scope.model.event, $scope.model.event.startDate)
            return $scope.defaults.startDate = new Date($scope.model.event.startDate);
        }

        console.log('Create new start date')
        return $scope.defaults.startDate = new Date();

    });

    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////

    /////////////////////////////////////////////

    $scope.hasCalculatedTime = function(target) {
        return true;
        var schedules = $scope.model.schedules;

        var index = schedules.indexOf(target);
        var previousSchedule = schedules[index-1];

        if(previousSchedule.duration) {
            return true;
        }
    }



    $scope.estimateTime = function(schedules, target) {

        if (target) {
            var total = 0;
            var foundAmount = 0;

            var index = schedules.indexOf(target);

            _.every(schedules, function(item, key) {
                //No item so return running total
                if (!item) {
                    foundAmount = total;
                } else {
                    //Increment the total
                    if (item.duration) {
                        total += parseInt(item.duration);
                        foundAmount = (total - item.duration);
                    } else {
                        foundAmount = total;
                    }
                }

                return (index != key)
            })


            var laterTime = new Date($scope.defaults.startDate);

            var milliseconds = foundAmount * 1000;
            laterTime.setTime($scope.defaults.startDate.getTime() + milliseconds);




            return laterTime;
        } else {
            return;
        }
    }

    $scope.keys = [
            "C",
            "Db",
            "D",
            "Eb",
            "E",
            "F",
            "Gb",
            "G",
            "Ab",
            "A",
            "Bb",
            "B",
            ]

    /////////////////////////////////////////////

    $scope.removeSchedule = function(schedule) {
        _.pull($scope.model.schedules, schedule);
    }

    /////////////////////////////////////////////

    $scope.addSchedule = function() {

        $scope.model.schedules.push({})
    }

    /////////////////////////////////////////////

    $scope.addSongs = function() {


        var selected = {}
        var params = {};

        var request = ModalService.browse('song', selected, params).result;

        request.then(done, done);

        function done() {

            var promises = _.map(selected.items, function(item) {
                return FluroContent.resource('get/'+ item._id).get().$promise;
            });

            $q.all(promises)
            .then(function(results) {

                _.each(results, function(song) {

                    var songDuration = _.get(song, 'data.duration');
                    var songKey = _.get(song, 'data.key');

                    console.log('SONG', song);

                    var songName = song.title;
                    // if(songKey && songKey.length) {
                    //     songName += ' ['+songKey+']';
                    // }

                    $scope.model.schedules.push({
                        type: 'song',
                        title:songName,
                        links:[song],
                        duration:songDuration,
                        key:songKey,
                    })
                 })
            })
        }
    }

    /////////////////////////////////////////////

    $scope.addBreaker = function() {

        $scope.model.schedules.push({
            title:'Break',
            type: 'breaker',
        })
    }

});