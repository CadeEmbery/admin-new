app.directive('definitionDrop', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            parentType:'=type',
        },
        templateUrl: 'admin-definition-dropdown/admin-definition-dropdown.html',
        controller: 'DefinitionDropdownController',
    };
});

/////////////////////////////////////////////

app.controller('DefinitionDropdownController', function($scope, $filter, TypeService) {

    $scope.options = _.map(TypeService.getSubTypes($scope.parentType), function(definition) {
        return {
            label:definition.title,
            value:definition.definitionName,
        }
    });



});