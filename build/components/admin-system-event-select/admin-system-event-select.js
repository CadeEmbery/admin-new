app.directive('systemEventSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            startDate:'=ngStartDate',
            endDate:'=ngEndDate',
        },
        templateUrl: 'admin-system-event-select/admin-system-event-select.html',
        controller: 'SystemEventSelectController',
    };
});





app.controller('SystemEventSelectController', function($scope) {


    if (!$scope.model || !_.isArray($scope.model)) {
        $scope.model = [];
    }

    $scope.reset = function() {
        //Create a new object
        $scope._new = {
            total: 1,
            period: 'day',
            when: 'before',
            point: 'start',
            trigger:'',
        }
    }


    $scope.reset();


    $scope.getPlural = function(number) {
        number = parseInt(number)

        if (number != 1) {
            return 's';
        }
    }
    

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    $scope.getReminderTime = function(reminder) {

        var date;
        if(reminder.point == 'end') {
            date = moment($scope.endDate);
        } else {
            date = moment($scope.startDate);
        }

        var period = reminder.period;
        var total = parseInt(reminder.total);

        ///////////////////////////////

        if(reminder.when == 'after') {
            date = date.add(total, period);//.toDate();
        } else {
            date = date.subtract(total, period);//.toDate();
        }

        ///////////////////////////////

        return date.toDate().format('g:ia l M Y') + ' (' +date.fromNow() + ')';
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    $scope.remove = function(item) {
        _.pull($scope.model, item);
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {

        var insert = angular.copy($scope._new);
        if (!$scope.model) {
            $scope.model = [];
        }

        if (!insert.period) {
            return;
        }

        if (!insert.total) {
            return;
        }

        // if (!insert.trigger || !insert.trigger.length) {
        //     return;
        // }

        if (!insert.reactions || !insert.reactions.length) {
            return;
        }
       
        $scope.model.push(insert);
        $scope.reset();
       
    }

})