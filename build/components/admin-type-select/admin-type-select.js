app.directive('typeSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-type-select/admin-type-select.html',
        controller: 'TypeSelectController',
    };
});


/////////////////////////////////////////////////////////////////////

app.controller('TypeSelectController', function($scope, $filter, TypeService) {





    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }

    /////////////////////////////////////////////////////////////////////

    $scope.search = {
        terms: '',
    }


    $scope.$watch('search.terms', updateList);
    /////////////////////////////////////////////////////////////////////


    $scope.loading = true;
    var promise = TypeService.refreshDefinedTypes(true)

    /////////////////////////////////////////////////////////////////////

    $scope.isExpanded = function(entry) {
        return $scope.search.terms.length || entry.expanded || $scope.contains(entry) || _.some(entry.defined, $scope.contains);
    }

    /////////////////////////////////////////////////////////////////////

    promise.then(function(res) {


        //$scope.definedTypes = TypeService.definedTypes;
        $scope.types = TypeService.types;
        $scope.definedTypes = TypeService.definedTypes;

        createTree();

        $scope.loading = false;

    });

    /////////////////////////////////////////////////////////////////////

    function createTree() {




        var treeLookup = _.chain($scope.types)
            .reduce(function(set, type) {

                var existing = set[type.path];

                /////////////////////////

                if (!existing) {

                    existing =
                        set[type.path] = {
                            singular: type.singular,
                            plural: type.plural,
                            path: type.path,
                            defined: [],
                        }
                }

                /////////////////////////


                /////////////////////////


                return set;
            }, {})
            .value();

        /////////////////////////////////////////////////////////////////////

        _.each($scope.definedTypes, function(definition) {

            var parentType = definition.parentType;

            console.log('PARENT', parentType, definition.title);

            if(treeLookup[parentType] && treeLookup[parentType].defined) {
                treeLookup[parentType].defined.push({
                    singular: definition.title,
                    plural: definition.plural,
                    path: definition.definitionName,
                });
            }

            
        });

        /////////////////////////////////////////////////////////////////////

        $scope.treeSource = _.chain(treeLookup).values()
        .sortBy(function(entry) {
                return entry.singular || entry.title;
            })
            .values()
            .value();

        updateList();

    }


    /////////////////////////////////////////////////////////////////////

    function updateList() {

        if (!$scope.treeSource) {
            return;
        }

        var filtered = $scope.treeSource.slice();

        if ($scope.search.terms && $scope.search.terms.length) {
            filtered = $filter('filter')(filtered, $scope.search.terms);

            _.each(filtered, function(entry) {
                entry.defined = $filter('filter')(entry.defined, $scope.search.terms);
            })
        }

        $scope.tree = filtered;

    }



    /////////////////////////////////////////////////////////////////////


    $scope.contains = function(item) {
        return _.some($scope.model, function(i) {
            if (_.isObject(i)) {
                return i.path == item.path;
            } else {
                return i == item.path;
            }
        });
    }

    $scope.toggle = function(item) {

        var matches = _.filter($scope.model, function(r) {
            var id = r;
            if (_.isObject(r)) {
                id = r.path;
            }

            return (id == item.path);
        })

        ////////////////////////////////

        if (matches.length) {
            $scope.model = _.reject($scope.model, function(r) {
                var id = r;
                if (_.isObject(r)) {
                    id = r.path;
                }

                return (id == item.path);
            });
        } else {
            $scope.model.push(item.path);
        }

    }

});