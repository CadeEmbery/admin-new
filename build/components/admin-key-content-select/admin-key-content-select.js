app.directive('keyContentSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-key-content-select/admin-key-content-select.html',
        controller: 'KeyContentSelectController',
    };
});





app.controller('KeyContentSelectController', function($scope) {


    
    if (!$scope.model || !_.isArray($scope.model)) {
        $scope.model = [];
    }
    
    //Create a new object
    $scope._new = {}

    ////////////////////////////////////////////////////

    $scope.remove = function(item) {
        _.pull($scope.model, item);
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {
        var insert = angular.copy($scope._new);
        if(!$scope.model) {
            $scope.model = [];
        }

        if(insert.key) {
            var keyExists = _.find($scope.model, {key:insert.key});
            
            if(!keyExists) {
                $scope.model.push(insert);
                $scope._new = {}
            }
        }
    }

})
