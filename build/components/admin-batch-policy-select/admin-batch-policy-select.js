app.directive('batchPolicySelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            selection: '=ngModel',
           // type:'=ngType',
           // definition:'=ngDefinition',
        },
        templateUrl: 'admin-batch-policy-select/admin-batch-policy-select.html',
        controller: 'BatchPolicySelectController',
    };
});

app.controller('BatchPolicySelectController', function($scope, $state, Batch, Notifications, $rootScope, CacheManager, Fluro, FluroContent, FluroAccess, $http) {

    $scope.selectedPolicies = [];


    $scope.dynamicPopover = {
        templateUrl: 'admin-batch-policy-select/admin-batch-policy-popover.html',
    };

    var definedName = 'persona';
   

    //Now we get the policies from FluroAccess
    $scope.policies = FluroContent.resource('policy').query();

    /////////////////////////////////////

    $scope.applyPolicies = function() {

        var details = {
            ids: $scope.selection.ids,
            policies: $scope.selectedPolicies,
        }

        Batch.addPolicies(details, done);
    }


    function done(err, data) {

        // console.log('Batch done', data);
        if (err) {
            //console.log('DONE ERR', err)
            return Notifications.error(err);
        }

        ///////////////////////

        //Find all the caches we need to clear
        var caches = ['persona'];

        ///////////////////////

        //Refresh any caches that might be affected outside of this view
        if(data.result) {
            if(data.result.types && data.result.types.length) {
                caches.concat(data.result.types)
            }

            if(data.result.definitions && data.result.definitions.length) {
                caches.concat(data.result.definitions)
            }
        }

        _.each(caches, function(t) {
            CacheManager.clear(t);
        });

        ///////////////////////

        //Reload the state
        $state.reload();

        ///////////////////////

        //Deselect all
        $scope.selection.deselectAll();
        Notifications.status('Updated policies for ' + data.success.length + ' users')



    }

    /////////////////////////////////////

    $scope.removePolicies = function() {
        var details = {
            ids: $scope.selection.ids,
            policies: $scope.selectedPolicies,
        }

        Batch.removePolicies(details, done);
    }

    /////////////////////////////////////

    $scope.select = function(item) {
        $scope.selectedPolicies.push(item);
    }

    /////////////////////////////////////

    $scope.isSelected = function(item) {
        var found = _.find($scope.selectedPolicies, function(r) {
            if(item._id) {
                return item._id == r._id;
            } else {
                return item = r._id;
            }
        });

        if(found) {
            return true;
        }
    }

    /////////////////////////////////////

    $scope.toggle = function(item) {
        if($scope.isSelected(item)) {
            $scope.deselect(item);
        } else {
            $scope.select(item);
        }
    }

    /////////////////////////////////////

    $scope.deselect = function(item) {
        _.pull($scope.selectedPolicies, item);
    }




    /**/



});