app.directive('batchProductSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            selection: '=ngModel',
            // type:'=ngType',
            // definition:'=ngDefinition',
        },
        templateUrl: 'admin-batch-product-select/admin-batch-product-select.html',
        controller: 'BatchProductSelectController',
    };
});

app.controller('BatchProductSelectController', function($scope, $state, ModalService, Batch, Notifications, $rootScope, CacheManager, Fluro, FluroContent, FluroAccess, $http) {

    $scope.popover = {
        open: false,
    }

    if (!$scope.selectedProducts) {
        $scope.selectedProducts = [];
    }

    $scope.dynamicPopover = {
        templateUrl: 'admin-batch-product-select/admin-batch-product-popover.html',
    };

    /////////////////////////////////////
    $scope.browse = function() {


        var params = {};
        return ModalService.browse('product', $scope.selectedProducts, params);

    }

    /////////////////////////////////////

    $scope.proposed = {};

    /////////////////////////////////////

    $scope.add = function(item) {

        $scope.selectedProducts.push(item);
        $scope.proposed = {};
    }


    $scope.remove = function(product) {

        _.pull($scope.selectedProducts, product);
    }

    /////////////////////////////////////

    $scope.getProducts = function(val) {

        var url = Fluro.apiURL + '/content/product/search/' + val;
        return $http.get(url, {
            ignoreLoadingBar: true,
            params: {
                limit: 20,
                allDefinitions:true,
            }
        }).then(function(response) {
            var results = response.data;
            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.selectedProducts, {
                    '_id': item._id
                });

                if (!exists) {
                    filtered.push(item);
                }
                return filtered;
            }, [])
        });

    };

    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////


    $scope.addToProducts = function() {

        $scope.popover = {
        open: false,
    }

        var details = {
            ids: $scope.selection.ids,
            products: $scope.selectedProducts,
        }

        Batch.addToProducts(details, done);
    }

    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////


    $scope.removeFromProducts = function() {

        $scope.popover = {
        open: false,
    }
    
        var details = {
            ids: $scope.selection.ids,
            products: $scope.selectedProducts,
        }

        Batch.removeFromProducts(details, done);
    }

    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    function done(err, data) {
        var selectionLength = $scope.selection.items.length;

        // console.log('Batch done', data);
        if (err) {
            //console.log('DONE ERR', err)
            return Notifications.error(err);
        }

        //////////////////////////////////////////////////////////

        var updatedProducts = _.get(data, 'result.updatedProducts');

        if (!updatedProducts || !updatedProducts.length) {
            return;
        }

        ///////////////////////

        //Find all the caches we need to clear
        var caches = [];

        ///////////////////////

        //Refresh any caches that might be affected outside of this view
        if (data.result) {
            if (data.result.types && data.result.types.length) {
                caches.concat(data.result.types)
            }

            if (data.result.definitions && data.result.definitions.length) {
                caches.concat(data.result.definitions)
            }
        }

        ///////////////////////

        caches = _.uniq(caches);
        caches = _.compact(caches);

        _.each(caches, function(t) {
            CacheManager.clear(t);
        });

        ///////////////////////

        //Reload the state
        $state.reload();


        ///////////////////////

        //Deselect all
        $scope.selection.deselectAll();

        //////////////////////////////////////////////////////////

        if (updatedProducts.length == 1) {
            Notifications.status('Added ' + selectionLength + ' items to ' + "'" + updatedProducts[0].title + "'");
        } else {
            Notifications.status('Added ' + selectionLength + ' items to ' + updatedProducts.length + ' products');
        }
    }


    //////////////////////////////////
    //////////////////////////////////

    //See whether a use is allowed to create new objects
    $scope.createEnabled = FluroAccess.can('create', 'product');

    $scope.create = function() {

         $scope.popover = {
                open:false
            };

        var template = {
            title:$scope.proposed.value,
            items:$scope.selection.items,
        };

        ModalService.create('product', {
            template:template,
        }, function(res) {
            $scope.options.push(res);
            $scope.model.push(res);

           
        })
    }

    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    // function done(err, data) {
    //     if (err) {
    //         Notifications.error(err);
    //     } else {
    //         if(data.results.length == 1) {
    //             Notifications.status('Added '+ $scope.selection.length +' items to ' + "'" + data.results[0].title + "'");
    //         } else {
    //             Notifications.status('Added '+ $scope.selection.length +' items to ' + data.results.length + ' products');
    //         }

    //         _.chain(data.results)
    //             .map(function(result) {
    //                 if (result.definition) {
    //                     return result.definition;
    //                 } else {
    //                     return result._type;
    //                 }
    //             })
    //             .uniq()
    //             .each(function(type) {
    //                 CacheManager.clear(type);
    //             })
    //             .value();


    //         $state.reload();


    //         //Deselect
    //         //$scope.selection.deselectAll();
    //     }
    // }



});