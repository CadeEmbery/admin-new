app.directive('relativeMessageSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            slots: '=?ngSlots',
            startDate: '=ngStartDate',
            endDate: '=ngEndDate',
        },
        templateUrl: 'admin-relative-message-select/admin-relative-message-select.html',
        controller: 'RelativeMessageSelectController',
    };
});





app.controller('RelativeMessageSelectController', function($scope) {


    if (!$scope.slots) {
        $scope.slots = [{
                title: 'Ticket Holders',
                value: 'ticket',
            },
            {
                title: 'Expected Contacts',
                value: 'expected',
            },
            {
                title: 'Parents of expected Contacts',
                value: 'expected parents',
            },
            {
                title: 'Checked In Contacts',
                value: 'checkin',
            },
            {
                title: 'Parents of checked in Contacts',
                value: 'checkin parents',
            },
        ]
    }

    if (!$scope.model || !_.isArray($scope.model)) {
        $scope.model = [];
    }

    $scope.reset = function() {
        //Create a new object
        $scope._new = {
            methodPreference:'all',
            total: 1,
            period: 'day',
            when: 'before',
            point: 'start',
            segments: [],
            methods: ['email'],
        }
    }


    $scope.reset();

    $scope.getPlural = function(number) {
        number = parseInt(number)

        if (number != 1) {
            return 's';
        }
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    $scope.getReminderTime = function(reminder) {

        var date;

        switch (reminder.point) {
            // case 'checkinEnd':
            //     date = moment($scope.endDate);
            // break;
            // case 'checkinStart':
            //     date = moment($scope.endDate);
            // break;
            case 'end':
                date = moment($scope.endDate);
                break;
            default:
                date = moment($scope.startDate);
                break;
        }


        var period = reminder.period;
        var total = parseInt(reminder.total);

        ///////////////////////////////

        if (reminder.when == 'after') {
            date = date.add(total, period); //.toDate();
        } else {
            date = date.subtract(total, period); //.toDate();
        }

        ///////////////////////////////

        return date.toDate().format('g:ia l M Y') + ' (' + date.fromNow() + ')';
    }

    ////////////////////////////////////////////////////

    $scope.segmentSelected = function(reminder, slotName) {
        if (!slotName) {
            return;
        }
        return _.includes(reminder.segments, slotName);
    }


    $scope.toggleSegment = function(reminder, slotName) {
        if (!slotName) {
            return;
        }
        var selected = $scope.segmentSelected(reminder, slotName);
        if (selected) {
            _.pull(reminder.segments, slotName);
        } else {
            if (!reminder.segments) {
                reminder.segments = [];
            }
            reminder.segments.push(slotName);
        }
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////


    ////////////////////////////////////////////////////

    $scope.reminderMethodSelected = function(reminder, channelName) {

        if (!channelName) {
            return;
        }
        var lowerCaseName = channelName.toLowerCase();
        return _.includes(reminder.methods, lowerCaseName);
    }


    $scope.toggleReminderMethod = function(reminder, channelName) {
        if (!channelName) {
            return;
        }
        var selected = $scope.reminderMethodSelected(reminder, channelName);

        var lowerCaseName = channelName.toLowerCase();

        if (selected) {
            _.pull(reminder.methods, lowerCaseName);
        } else {
            if (!reminder.methods) {
                reminder.methods = [];
            }
            reminder.methods.push(lowerCaseName);
        }
    }


    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    $scope.remove = function(item) {
        _.pull($scope.model, item);
    }


    $scope.isValidToCreate = function() {
        var insert = $scope._new;

        if (!insert.period) {
            return;
        }

        if (!insert.total) {
            return;
        }

        if (!insert.segments || !insert.segments.length) {
            return;
        }

        if (!insert.methods || !insert.methods.length) {
            return;
        }

        if (!insert.methodPreference || !insert.methodPreference.length) {
            return;
        }

        if (!insert.title || !insert.title.length) {
            return;
        }

        return true

    }
    ////////////////////////////////////////////////////

    $scope.create = function() {

        if(!$scope.isValidToCreate()) {
            return;
        }

        var insert = angular.copy($scope._new);
        if (!$scope.model) {
            $scope.model = [];
        }

        $scope.model.push(insert);
        $scope.reset();

    }

})