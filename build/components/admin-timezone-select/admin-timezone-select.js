app.directive('timezoneSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            default:'=?'
        },
        templateUrl: 'admin-timezone-select/admin-timezone-select.html',
        // controller: 'TimezoneSelectController',
        link:function($scope, element, attrs) {

            $scope.timezones = moment.tz.names();

            if(!$scope.model) {

                switch($scope.default) {
                    default:
                        var tz = moment.tz.guess();
                        $scope.model = tz;
                    break;
                }
            }

        }
    };
});


