app.directive('paymentModifierForm', function() {

    return {
        restrict: 'E',
        replace:true,
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            closeCallback: '=close',
            availableFields: '=',
            isNew: '=',
        },
        templateUrl: 'payment-modifier-editor/payment-modifier-form.html',
        controller: 'PaymentModifierFormController',
    };
});



app.controller('PaymentModifierFormController', function($rootScope, $uibModal, $scope) {


    var now = new Date();

    $scope.milliseconds = now.getTime();

    $scope.close = $scope.closeCallback;




});