app.directive('paymentModifierEditor', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            modifiers: '=ngModel',
            definition: '=',
        },
        templateUrl: 'payment-modifier-editor/payment-modifier-editor.html',
        controller: 'PaymentModifierEditor',
    };
});



app.controller('PaymentModifierEditor', function($rootScope, $uibModal, $scope) {

    ////////////////////////////////////////////////////

    //Create a modifiers if it doesn't exist
    if (!$scope.modifiers) {
        $scope.modifiers = [];
    }




    ////////////////////////////////////////////////////

    //Create a new object
    $scope._new = {
        // title: '',
        // operation: '',
        // expression: '',
        // condition: '',
    }

    ////////////////////////////////////////////////////

    $scope.getSummary = function(item) {
        var message;
        var action;
        var amount = item.expression || '';


        var condition;

        if (item.condition && item.condition.length) {
            condition = '<div class="text-muted">if <em>' + item.condition + '</em></div>'; //' returns true';
        }

        //If amount is a pure number
        if (!isNaN(amount)) {
            amount = '<strong>$' + (amount / 100) + '</strong>';
        }

        switch (item.operation) {
            case 'set':
                action = 'Set current total to';
                break;
            case 'add':
                action = 'Add';
                amount = amount + ' to current total';
                break;
            case 'multiply':
                action = 'Multiply current total by';
                break;
            case 'subtract':
                action = 'Subtract';
                amount = amount + ' from current total';
                break;
            case 'divide':
                action = 'Divide current total by';
                break;
            default:
                return;
                break;
        }

        message = _.compact([condition, action, amount]).join(' ');

        return message;
    }

    ////////////////////////////////////////////////////

    $scope.addModifier = function() {

        // var submittedKey = $scope._new.key;

        // if (!submittedKey || !submittedKey.length) {
        //     return;
        // }

        // var exists = _.some($scope.states, {
        //     key: submittedKey
        // });

        // if (exists) {
        //     return;
        // }

        // /////////////////////////////////////
        // //Insert entry
        // var insert = angular.copy($scope._new);

        // insert.key = insert.key.toLowerCase();

        var newModifier = {};
        $scope.modifiers.push(newModifier);
        $scope.editModifier(newModifier, true);

        // $scope._new = {};

        // $scope.$broadcast('add');
    }

    ////////////////////////////////////////////////////

    $scope.removeModifier = function(entry) {
        _.pull($scope.modifiers, entry);
    }

    ////////////////////////////////////////////////////

    $scope.duplicateModifier = function(entry) {
        var copy = angular.copy(entry);

        //Get where it currently is
        var getCurrentIndex = _.indexOf($scope.modifiers, entry);

        var insertIndex = getCurrentIndex;

        //Insert it in
        $scope.modifiers.splice(insertIndex, 0, copy);
        $scope.editModifier(copy, true);
    }


    ////////////////////////////////////////////////////

    $scope.editModifier = function(entry, isNew) {

        var parentScope = $scope;
        // var availableFields = $scope.availableFields;
        var availableFields = extractFieldsFromDefinitionFields($scope.definition.fields, '0', true);



            


        var modalInstance = $uibModal.open({
            template: '<payment-modifier-form ng-model="entry" is-new="isNew" available-fields="availableFields" close="closeModal"></payment-modifier-form>',
            size: 'lg',
            backdrop: 'static',
            controller: function($scope) {

                $scope.availableFields = availableFields;
                $scope.entry = entry;
                // $scope.availableFields = availableFields;
                $scope.isNew = isNew;

                // $scope.test = testEvent;
                // $scope.isModal =true;
                $scope.closeModal = function(cancelled) {

                    if (cancelled) {
                        console.log('cancelled')
                        parentScope.removeModifier(entry);
                        $scope.$close();
                    } else {
                        console.log('close modal')
                        $scope.$close();
                    }

                }
            }
        });
    }

    ////////////////////////////////////////////////////

    $scope.contains = function(entry) {
        return _.contains($scope.modifier, entry);
    }

})