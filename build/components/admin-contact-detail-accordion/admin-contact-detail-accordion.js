app.directive('contactDetailAccordion', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            definitions: '=ngDefinitions',
            contact: '=ngContact',
        },
        // Replace the div with our template
        templateUrl: 'admin-contact-detail-accordion/admin-contact-detail-accordion.html',
        controller: 'ContactDetailAccordionController',
    };
});


app.controller('ContactDetailAccordionController', function($scope, ModalService, $rootScope, $q, $timeout, FluroContent, Notifications, FluroAccess, TypeService) {

    //////////////////////////////////////////////////

    $scope.canDefine = FluroAccess.can('create', 'definition');
   

    var params = {
        template:{
           parentType:'contactdetail',
        }
    };


    //////////////////////////////////////////////////


    $scope.defineNew = function() {

        ModalService.create('definition', params, function(res) {

            console.log('Definition', res);
            $scope.definitions.push(res);

            //Update the view
            update();
        });
    }

    //////////////////////////////////////////////////

    $scope.$watch('definitions', update);
    $scope.$watch('model', update);

    //////////////////////////////////////////////////

    $scope.addSheet = function(pair) {

        var definition = pair.definition;

        //Try and figure out the logical realms to place these in
        FluroAccess.retrieveSelectableRealms('create', definition.definitionName, 'contactdetail')
            .then(function(createableRealms) {


                var createInRealms = [];
                var restrictedRealms = definition.restrictRealms;

                if (restrictedRealms && restrictedRealms.length) {
                    createInRealms = _.filter(createableRealms, function(realm) {
                        if (realm._id) {
                            realm = realm._id;
                        }

                        return _.includes(restrictedRealms, realm);
                    })
                } else {

                    //If there is only one createable realm then use it duh.
                    if (createableRealms.length == 1) {
                        createInRealms = createableRealms;
                    } else {

                        //Use the realms on the contact
                        createInRealms = createInRealms.concat(definition.realms);
                        createInRealms = createInRealms.concat($scope.contact.realms);
                        createInRealms = _.chain(createInRealms)
                            .compact()
                            .uniq(function(realm) {
                                if (realm._id) {
                                    return realm._id;
                                } else {
                                    return realm;
                                }
                            })
                            .filter(function(realmID) {
                                //console.log('Filter', realmID.title, createableRealms);
                                if (realmID._id) {
                                    realmID = realmID._id;
                                }

                                //Only use the realms that the contact is already in
                                //or are on the definition itself
                                return _.some(createableRealms, {
                                    _id: realmID
                                });
                            })
                            .value();
                    }
                }

                //Create the sheet with default details
                pair.sheets.push({
                    contact:$scope.contact._id,
                    title: definition.title,
                    _type:'contactdetail',
                    realms: createInRealms,
                    definition: definition.definitionName,
                    data: {},
                    author:$rootScope.user,
                    managedAuthor:$rootScope.user.persona,
                })
            })
    }

    //////////////////////////////////////////////////

    $scope.saveChanges = function(sheet, definition) {

        if(sheet.state == 'processing') {
            return;
        }


        sheet.state = 'processing';

        function saveSuccess(res) {

            sheet.state = 'success';
            sheet._id = res._id;

            $timeout(function() {
                sheet.state = null;
            }, 3000);

            // delete res.realms;

            // _.assign(sheet, res);
            Notifications.status('Updated ' + definition.title + ' for ' + $scope.contact.firstName);
        }

        function saveFail(err) {

            sheet.state = 'failed';

            $timeout(function() {
                sheet.state = null;
            }, 3000);


            Notifications.error('Failed to update ' + definition.title);
        }

        //////////////////////////////////////////////////
        if (sheet._id) {

            //Edit existing content
            FluroContent.resource(sheet.definition).update({
                id: sheet._id,
            }, sheet, saveSuccess, saveFail);
        } else {

            //Save new sheet
            FluroContent.resource(sheet.definition).save(sheet, saveSuccess, saveFail);
        }



    }


    //////////////////////////////////////////////////

    $scope.canEditSheet = function(sheet) {


        if(sheet.realms && sheet.realms.length) {
            var editAccess = FluroAccess.canEditItem(sheet);

            return editAccess
        } else {
            return true;
        }
    }

    //////////////////////////////////////////////////

    //Update the sheets
    function update() {

        //////////////////////////////

        var sheets = $scope.model;
        var definitions = $scope.definitions;

        //////////////////////////////

        $scope.pairs = _.reduce(definitions, function(results, definition) {

            var pair = _.find(results, {
                definitionName: definition.definitionName
            });


            if (!pair) {

                //Find all sheets that exist and match
                var matchedSheets = _.chain(sheets)
                .filter({
                    definition: definition.definitionName
                })
                .map(function(sheet) {
                    if(!sheet.data) {
                        sheet.data = {};
                    }
                    return sheet;
                })
                .value();


                pair = {
                    _id: definition._id,
                    definitionName: definition.definitionName,
                    sheets: matchedSheets,
                    definition: definition,
                }

                //Check if this sheet can be created
                FluroAccess.retrieveSelectableRealms('create', definition.definitionName, 'contactdetail').then(function(createableRealms) {
                    $timeout(function() {
                        if (createableRealms.length) {
                            pair.createable = true;
                        }
                    })
                })

                results.push(pair);
            }


            return results;
        }, [])


        //console.log('THINGS', $scope.pairs);
    }

});