app.directive('quickSearch', function($rootScope) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'quick-search/quick-search.html',
        controller: 'QuickSearchController',
        link: function($scope, $element, $attr) {

            ////////////////////////////////////

            // $scope.$watch(function() {
            //     return $rootScope.quicksearch.open;
            // }, function(open) {
            //     if(open) {
            //         $element.find('#quick-search-input').focus();
            //         $element[0].scrollTop();
            //     }
            // });



        }
    };
});



app.controller('QuickSearchController', function($scope, $rootScope, $state, FluroAccess, FluroContent, TypeService, SearchService) {


    $scope.service = SearchService;

    ////////////////////////////////////////////////
    ////////////////////////////////////////////////

    $scope.$watch('service.criteria.terms', function(terms, oldTerms) {

        refreshMenuDirectory();

        if (terms == oldTerms) {
            return;
        }

        $scope.results = null;
        $scope.apps = null;

        ////////////////////////////////////

        //Now we search
        if (terms && terms.length) {
            SearchService.search(terms); //.then(searchComplete, searchFailed);

            FluroContent.endpoint('apps', true, true).query().$promise.then(function(apps) {
                $scope.apps = apps;
            });
        }
    });

    ////////////////////////////////////


    // $scope.viewItem = function(item, isModal, type) {

    //     // var type = item._type;
    //     // var definition = item.definition;
    //     // var definedName = definition || type;

    //     // $state.go(type + '.view', {
    //     //     id: item._id,
    //     //     definitionName: definedName
    //     // });

    //     $rootScope.viewItem(item, isModal, type);
    //     SearchService.close();
    // }

    /////////////////////////////////////////////

    $scope.editItem = function(item) {

        var type = item._type;
        var definition = item.definition;
        var definedName = definition || type;

        if (definition && definition.length) {
            $state.go(type + '.custom', {
                id: item._id,
                definitionName: definedName
            });
        } else {
            $state.go(type + '.edit', {
                id: item._id,
                definitionName: definedName
            });
        }

       SearchService.close();
    }


    ////////////////////////////////////

    $scope.$watch(function() {
        return TypeService.menuTree;
    }, refreshMenuDirectory);

    ////////////////////////////////////

    $scope.$watch('service.results', function(results) {

        $scope.results = _.chain(results)
            .map(function(result) {
                return result.results;
            })
            .flatten()
            .compact()
            .map(function(item) {

                var matchType = TypeService.getTypeFromPath(item.definition || item._type);
                if (matchType) {
                    item.searchKey = matchType.plural;
                } else {
                    item.searchKey = item._type;
                }

                return item;
            })
            .reduce(function(set, item) {



                var existing = _.find(set, {
                    plural: item.searchKey
                });

                if (!existing) {
                    existing = {
                        plural: item.searchKey,
                        definition: item.definition,
                        _type: item._type,
                        results: [],
                    }

                    set.push(existing);
                }

                existing.results.push(item);

                return set;
            }, [])
            .value();
    })



    ////////////////////////////////////

    function refreshMenuDirectory() {
        // //console.log('REFRESH');

        var types = TypeService.allTypes(); //.types;//allTypes();
        // var definedTypes = TypeService.definedTypes;



        var searchTerms = '';

        //If we are searching
        if (SearchService.criteria && SearchService.criteria.terms && SearchService.criteria.terms.length) {

            //Update the search terms
            searchTerms = String(SearchService.criteria.terms).toLowerCase();

            // //console.log("search terms", searchTerms)
            types = _.filter(types, function(type) {
                var title = String(type.plural).toLowerCase();

                // //console.log('TYPES', type);
                var match = (title.indexOf(searchTerms) != -1);

                if (match) {
                    return true;
                }
                return _.some(type.children, function(sub) {
                    var title = String(sub.plural).toLowerCase();

                    var match = (title.indexOf(searchTerms) != -1);
                    return match;
                })
            })
        }

        //////////////////////////////////////

        types = _.sortBy(types, function(type) {
            return type.plural
        })

        //////////////////////////////////////

        var basicTypes = _.filter(types, function(type) {

            var isBasic = (!type.parentType || !type.parentType.length);
            var canAccess = FluroAccess.canAccess(type.path);

            return isBasic && canAccess;
        })

        var definedTypes = _.filter(types, function(type) {

            var isDefined = (type.parentType && type.parentType.length);
            var canAccess = FluroAccess.canAccess(type.definitionName, type.parentType);

            return isDefined && canAccess;

        })

        // //console.log('TYPES', basicTypes, definedTypes);


        //////////////////////////////////////

        var groupList = [];

        groupList.push({
            title: 'Content',
            _id: 'content',
            icon: 'article',
            items: []
        });

        groupList.push({
            title: 'Events & Planning',
            _id: 'events',
            icon: 'event',
            items: []
        });

        groupList.push({
            title: 'People & Contacts',
            _id: 'contacts',
            icon: 'contact',
            items: []
        });

        groupList.push({
            title: 'Files & Media',
            _id: 'files',
            icon: 'asset',
            items: []
        });

        groupList.push({
            title: 'Interactions',
            _id: 'interactions',
            icon: 'interaction',
            items: []
        });

        groupList.push({
            title: 'Users & Permissions',
            _id: 'permissions',
            icon: 'user',
            items: []
        });

        //////////////////////////////////////

        if (searchTerms.length) {
            groupList = [];
        }

        //////////////////////////////////////


        types = _.chain(basicTypes)
            .reduce(function(results, type) {
                var groupName = type.group;
                var _id = String(groupName).toLowerCase();

                var existing = _.find(results, {
                    _id: _id
                });

                if (!existing) {
                    existing = {
                        title: groupName,
                        _id: _id,
                        items: []
                    }
                    results.push(existing);
                }

                //Gather all the sub children
                type.subItems = _.filter(definedTypes, {
                    parentType: type.path
                });

                //Push the type into the existing group
                existing.items.push(type);


                return results;
            }, groupList)
            .filter(function(group) {
                return group.items.length;
            })
            .value();


        types = _.chunk(types, 4)

        $scope.directory = types;
    }

    ////////////////////////////////////////////////

    $scope.list = function(set) {

        SearchService.searchIn = SearchService.criteria.terms;


        if (set.definition && set.definition.length) {
            $state.go(set._type + '.custom', {
                definition: set.definition,
                // search:SearchService.criteria.terms
            });

        } else {
            $state.go(set._type + '.default');
        }

        SearchService.close();
    }



});


app.service('SearchService', function($q, $rootScope, $state, FluroContent, TypeService) {

    var service = {
        criteria: {},
        random:Math.random() * Math.random() * 1000,
    }

    ///////////////////////////////////////////////

    service.close = function() {
        service.focussed = false;
        service.criteria = {};
    }
    ///////////////////////////////////////////////

    service.clear = function() {
        console.log('CLEAR!')
        service.results = null;
        service.focussed = false;
        service.criteria = {};
    }

    ///////////////////////////////////////////////

    service.select = function(sref) {
        $state.go(sref);
        service.clear();
    }
    ///////////////////////////////////////////////

    service.isOpen = function() {
        var hasTerms = _.get(service, 'criteria.terms.length');
        if (service.focussed || hasTerms) {
            return true;
        }
    }

    // service.toggle = function() {
    //     if(service.isOpen()) {
    //         service.clear()
    //     };
    // }

    ///////////////////////////////////////////////

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
        // service.open = false;
        service.criteria = {}
        service.focussed = false;
    });

    ///////////////////////////////////////////////

    service.search = function(terms) {

        service.processing = true;
        var deferred = $q.defer();


        ///////////////////////////////////////////////

        if (!terms || !terms.length) {
            deferred.resolve([]);
            service.processing = false;
            return deferred.promise;
        }

        ///////////////////////////////////////////////

        // var isID = validator.isMongoId(terms);

        // if (isID) {
        //     var request = FluroContent.endpoint('content/search/' + terms).query().$promise;
        //     request.then(mapResults, clearResults)
        //     return request.then(mapResults, clearResults)
        // }

        ///////////////////////////////////////////////

        return TypeService.definedTypes.$promise.then(function(definedTypes) {

            // var allTypes = [];
            // allTypes = allTypes.concat(TypeService.types, definedTypes);


            // allTypes = _.map(allTypes, function(typeObject) {
            //     return typeObject.path || typeObject.definitionName;
            // })

            var types = [
                'product',
                'purchase',
                'collection',
                'policy',
                'query',
                'family',
                'persona',
                'contact',
                'contactdetail',
                'event',
                'team',
                'realm',
                'article',
                'asset',
                'image',
                'video',
                'audio',
                'interaction',
                'application',
                'definition',
            ]

            var basicFields = 'name realms title definition width height module description color bgColor firstLine firstName lastName gender duration filesize _type startDate endDate status assetType external extension mimetype';

            ///////////////////////////////////////////////

            var criteria = {
                limit: 8,
                simple: true,
                select: basicFields,
                allDefinitions: true,
                populateFields: 'realms',
                populateSelect: 'color bgColor title',
                types: types,
                searchInheritable: true,
                statuses:['active', 'archived', 'draft'],
                // operator: 'or',
                // cascade: true,
                expanded: true,
            }

            // var criteria = {};

            ///////////////////////////////////////////////

            var request = FluroContent.endpoint('content/search/' + terms).query(criteria).$promise;

            return request.then(mapResults, clearResults)

        });
        ///////////////////////////////////////////////

        function mapResults(res) {

            console.log('RESULTS', res);
            service.processing = false;

            service.results = res;
            deferred.resolve(res)
        }

        function clearResults(err) {
            service.processing = false;

            service.results = null;
            deferred.resolve()
        }

        ///////////////////////////////////////////////
        return deferred.promise;

    }

    ///////////////////////////////////////////////

    return service;

})