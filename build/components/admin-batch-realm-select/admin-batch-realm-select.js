app.directive('batchRealmSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            selection: '=ngModel',
            type: '=ngType',
            definition: '=ngDefinition',
            queryMode: '=',
        },
        templateUrl: 'admin-batch-realm-select/admin-batch-realm-select.html',
        controller: 'BatchRealmSelectController',
    };
});

/////////////////////////////////////////////////////////////////////

app.controller('BatchRealmSelectController', function($scope, $state, Batch, Notifications, $rootScope, TypeService, CacheManager, Fluro, FluroAccess, $http) {

    $scope.popover = {
        open: false,
        loading:true,
    };

    $scope.model = [];
    $scope.search = {};

    $scope.dynamicPopover = {
        templateUrl: 'admin-batch-realm-select/admin-batch-realm-popover.html',
    };

    /////////////////////////////////////////////////

    

    /////////////////////////////////////////////////

    var definedName = $scope.type.path;
    var parentType;

    if ($scope.definition) {
        definedName = $scope.definition.definitionName;
        parentType = $scope.definition.parentType;
    }

    /////////////////////////////////////////////////

    if ($scope.queryMode) {
       var firstItem = _.first($scope.selection.items);

       if(firstItem) {
           definedName = firstItem.definition || firstItem._type;
           parentType = firstItem._type;
       }

    }



    ////////////////////////////////////////////////////

    ////////////////////////////////////////////////////

    $scope.$watch('search.terms', function() {
        updateLists();
    });


    function updateLists(selectFirst) {

        var realmTree = $scope.realmTree || [];
        var searchTerms = $scope.search.terms;

        $scope.trees = {};

        $scope.popover.loading = false;
        // console.log('REALM TREE', realmTree)
        ///////////////////////////////////////

        mapTree(null, 'tree');
        //TURN THIS ON LATER
        mapTree('team', 'groupTree');




        ///////////////////////////////////////

        function mapTree(discriminatorType, treeKey) {
            // $scope.tree = realmTree;

            // return;

            var isSelectableLookup = {};
            var hasSelectionCriteria;


            //No restrictions
            if (!$scope.restrictedRealms || !$scope.restrictedRealms.length) {
                hasSelectionCriteria = false;
                // $scope[treeKey] = realmTree;

                if(selectFirst) {
                    if (realmTree.length == 1) {
                        if (!realmTree[0].children || !realmTree[0].children.length) {
                            console.log('SELECT FIRST', realmTree[0]);
                            $scope.model = [realmTree[0]];
                        }
                    }
                }
                // return;

                

            } else {
                hasSelectionCriteria = true;

                isSelectableLookup = _.reduce($scope.restrictedRealms, function(set, realmID) {
                    set[realmID] = true;
                    return set;
                }, {})

            }

            ///////////////////////////////////////

            function canSelect(realm) {

                var canBeSelected = (hasSelectionCriteria && isSelectableLookup[realm._id]) || !hasSelectionCriteria;
                var isCorrectDiscriminator;

                if (discriminatorType) {
                    isCorrectDiscriminator = realm._discriminatorType === discriminatorType;
                } else {
                    isCorrectDiscriminator = !realm._discriminatorType;
                }

                // console.log(realm.title, canBeSelected, isSelectable, isCorrectDiscriminator);
                return canBeSelected && isCorrectDiscriminator;
            }

            ///////////////////////////////////////

            function filterSelectable(realm, parentArray) {

                var copiedRealm;

                ///////////////////////////////////////

                var matchesSearchCriteria = true;
                if(searchTerms && searchTerms.length) {
                    matchesSearchCriteria = _.includes(realm.title.toLowerCase(), searchTerms)
                }

                //If we can select this realm then 
                //add it into the parent array
                var include = canSelect(realm) && matchesSearchCriteria
                    // console.log('TREE CAN SELECT', realm.title, realm._discriminatorType, discriminatorType, include)
                if (include) {


                    copiedRealm = _.pick(realm, [
                        '_id',
                        'title',
                        'definition',
                        '_discriminator',
                        '_discriminatorType',
                        'color',
                        'bgColor',
                    ])

                    copiedRealm.children = [];


                    // results.push(realm);
                    parentArray.push(copiedRealm)
                }

                ///////////////////////////////////////

                //If the realm has branches, recursively walk down the branches
                if (realm.children && realm.children.length) {

                    var targetArray = parentArray;
                    if (copiedRealm && copiedRealm.children) {
                        targetArray = copiedRealm.children;
                    }

                    //Loop through the children
                    _.each(realm.children, function(child) {
                        filterSelectable(child, targetArray)
                    });
                }
            }

            ///////////////////////////////////////

            //Now we need to get fancy
            var filteredTree = [];


            _.each(realmTree, function(realm) {
                filterSelectable(realm, filteredTree);
            });


            // console.log('FILTERED TREE', filteredTree)

            ////////////////////////////////////////////////////////

            if(selectFirst) {
                //If only one realm is available and no realm is selected
                //select it by default
                if (filteredTree.length == 1 && !$scope.model.length) {
                    //If there are no children select this only realm by default
                    if (!filteredTree[0].children || !filteredTree[0].children.length) {
                        $scope.model = [filteredTree[0]];
                        console.log('SELECT FIRST', filteredTree[0]);
                    }
                }
            }

            if (!discriminatorType) {
                $scope.trees['none'] = filteredTree;
            } else {
                $scope.trees[discriminatorType] = filteredTree;
            }

        }



    }
   
   //Now we get the realm tree from FluroAccess service
    FluroAccess.retrieveSelectableRealms('create', definedName, parentType, true)
        .then(function(realmTree) {

            $scope.realmTree = realmTree;
            updateLists(true);


        });


    // /////////////////////////////////////////////////

    // FluroAccess.retrieveSelectableRealms('create', definedName, parentType, true)
    // .then(function(tree) {

    //      $scope.popover.loading = false;
    //     // console.log('Got tree', tree);
    //     $scope.tree = tree;
    // });

    /////////////////////////////////////////////////

    // if (FluroAccess.isFluroAdmin()) {


    // } else {


    //     //Now we get the realms from FluroAccess
    //     var realms = FluroAccess.retrieveSelectableRealms('create', definedName, parentType, true);

    //     $scope.tree = _.chain(realms)
    //         .sortBy(function(realm) {
    //             if (realm.trail && realm.trail.length) {
    //                 return realm.trail.length;
    //             } else {
    //                 return 0;
    //             }
    //         })
    //         // .reverse()
    //         .reduce(function(results, realm) {

    //             realm.children = [];

    //             if (realm.trail && realm.trail.length) {
    //                 var parentID = String(realm.trail[realm.trail.length - 1]);

    //                 var parent = _.find(realms, function(pRealm) {
    //                     return String(pRealm._id) == parentID;
    //                 });

    //                 if (!parent) {
    //                     delete realm.trail;
    //                     results.push(realm);
    //                     return results;
    //                 }

    //                 if (!parent.children) {
    //                     parent.children = []
    //                 }

    //                 delete realm.trail;
    //                 parent.children.push(realm);

    //                 parent.children = _.sortBy(parent.children, 'title');
    //             } else {
    //                 results.push(realm);
    //             }

    //             return results;

    //         }, [])
    //         .value();
    // }
    // $scope.realms = realms;

    /////////////////////////////////////

    $scope.applyRealms = function() {

        $scope.popover = {
            open: false
        }

        var details = {
            ids: $scope.selection.ids,
            realms: $scope.model,
        }

        details.users = ($scope.type.path == 'user');
        if ($scope.type.path == 'persona') {
            details.useModel = 'persona';
        }


        if ($scope.type.path == 'ticket') {
            details.useModel = 'ticket';
        }

        


        Batch.addRealms(details, done);
    }


    //////////////////////////////////////////////////////////

    function done(err, data) {

        console.log('Batch done', data);
        if (err) {
            //console.log('DONE ERR', err)
            return Notifications.error(err);
        }

        ///////////////////////

        //Find all the caches we need to clear
        var caches = [];

        if (definedName) {
            caches.push(definedName);
        }

        if (parentType) {
            caches.push(parentType);
        }

        ///////////////////////

        //Refresh any caches that might be affected outside of this view
        if (data.result) {
            if (data.result.types && data.result.types.length) {
                caches.concat(data.result.types)
            }

            if (data.result.definitions && data.result.definitions.length) {
                caches.concat(data.result.definitions)
            }
        }

        ///////////////////////

        caches = _.uniq(caches);
        caches = _.compact(caches);

        _.each(caches, function(t) {
            console.log('CLEAR', t);
            CacheManager.clear(t);
        });

        ///////////////////////

        //Reload the state
        $state.reload();

        ///////////////////////

        //Deselect all
        $scope.selection.deselectAll();
        Notifications.status('Updated realms on ' + data.success.length + ' items');



    }

    /////////////////////////////////////

    $scope.removeRealms = function() {

        $scope.popover = {
            open: false
        }


        var details = {
            ids: $scope.selection.ids,
            realms: $scope.model,
        }

        details.users = ($scope.type.path == 'user');
        if ($scope.type.path == 'persona') {
            details.useModel = 'persona';
        }

        Batch.removeRealms(details, done);
    }

    /////////////////////////////////////

    $scope.select = function(item) {
        $scope.model.push(item);
    }

    /////////////////////////////////////

    $scope.isSelected = function(item) {
        var found = _.find($scope.model, function(r) {
            if (item._id) {
                return item._id == r._id;
            } else {
                return item = r._id;
            }
        });

        if (found) {
            return true;
        }
    }

    /////////////////////////////////////

    $scope.toggle = function(item) {
        if ($scope.isSelected(item)) {
            $scope.deselect(item);
        } else {
            $scope.select(item);
        }
    }

    /////////////////////////////////////

    $scope.deselect = function(item) {
        _.pull($scope.model, item);
    }




    /**/



});