app.directive('batchState', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            selection: '=ngModel',
            definition: '=ngDefinition',
        },
        controller: 'BatchStateController',
        templateUrl: 'admin-batch-state/admin-batch-state.html',
    };
});

app.controller('BatchStateController', function($scope, $state, Batch, Notifications, $rootScope, CacheManager, FluroAccess, FluroContent, Fluro, $http, ModalService) {

    $scope.popover = {
        open:false
    };

    //////////////////////////////////////////////////////////

    // //////////////////////////////////////////////////////////

    // //Get all the options
    // FluroContent.resource('collection').query({
    //     simple: true,
    //     allDefinitions:true,
    // }, function(res) {
    //     $scope.options = res;
    // });

    //////////////////////////////////////////////////////////

    $scope.dynamicPopover = {
        templateUrl: 'admin-batch-state/popover.html',
    };


    $scope.states = _.chain($scope.definition)
    .get('data.states')
    .value();



    /////////////////////////////////////

    $scope.select = function(state) {

        $scope.popover = {
            open:false
        }
        
        var details = {
            ids: $scope.selection.ids,
            fields:{
                state:state.key,
            },
        }

        Batch.updateFields(details, done);
    }



    //////////////////////////////////////////////////////////

    function done(err, data) {


        var selectionLength = $scope.selection.items.length;

        // console.log('Batch done', data);
        if (err) {
            //console.log('DONE ERR', err)
            return Notifications.error(err);
        }

        // //////////////////////////////////////////////////////////

        var result =  _.get(data, 'result');

        if(!result || !result.length) {
            return;
        }

        ///////////////////////

        //Find all the caches we need to clear
        var caches = [];

        ///////////////////////

        //Refresh any caches that might be affected outside of this view
        if (data.result) {

            console.log('RESULT IS', data.result);
            if (data.result.types && data.result.types.length) {
                caches.concat(data.result.types)
            }

            if (data.result.definitions && data.result.definitions.length) {
                caches.concat(data.result.definitions)
            }
        }

        ///////////////////////

        caches = _.uniq(caches);
        caches = _.compact(caches);

        _.each(caches, function(t) {
            CacheManager.clear(t);
        });

        ///////////////////////

        //Reload the state
        $state.reload();

        ///////////////////////

        //Deselect all
        $scope.selection.deselectAll();

        //////////////////////////////////////////////////////////

        if(data.success.length == 1) {
            Notifications.status('Moved '+ selectionLength +' cards');
        } else {
            Notifications.status('Moved '+ selectionLength +' card');
        }
    }

    //////////////////////////////////
    //////////////////////////////////

    // //See whether a use is allowed to create new objects
    // $scope.createEnabled = FluroAccess.can('create', 'collection');   

    // $scope.create = function() {
    //     $scope.popover = {
    //         open:false
    //     }

    //     var template = {
    //         // title:'My new collection',
    //         items:$scope.selection.items,
    //     }

    //     ModalService.create('collection', {template:template}, function(res) {
    //         $scope.options.push(res);
    //         $scope.model.push(res);
    //     })
    // }

    // //////////////////////////////////
    // //////////////////////////////////

    // $scope.contains = function(item) {
    //     return _.some($scope.collections, function(i) {
    //         if (_.isObject(i)) {
    //             return i._id == item._id;
    //         } else {
    //             return i == item._id;
    //         }
    //     });
    // }
    
    // $scope.toggle = function(realm) {

    //     var existing = _.find($scope.collections, {
    //         _id: realm._id
    //     });

    //     if (existing) {
    //         _.pull($scope.collections, existing);
    //     } else {
    //         $scope.collections.push(realm);
    //     }

    // }

    /////////////////////////////////////



});