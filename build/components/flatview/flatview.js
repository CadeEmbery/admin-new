app.directive('flatview', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            definition: '=',
            private:"=",
        },
        templateUrl: 'flatview/flatview.html',
        controller: 'FlatviewController',
    };
});

//////////////////////////////////////////////////////////////////////

app.service('FlattenService', function() {

    var service = {};

    ///////////////////////////////

    service.flatten =  function(obj) {
        //Resulting object
        var flattened = {}

        //Check if its circular
        var circlular = []
        var circLoc = []

        ////////////////////////////////////////

        //Recurring function that goes down the tree
        function _route(prefix, value) {
            var i, len, type, keys, circularCheck, loc

            if (value == null) {
                if (prefix === "") {
                    return
                }
                flattened[prefix] = null
                return
            }

            if(_.isDate(value)) {
                flattened[prefix] = value
                return
            }

            ////////////////////////////////////

            type = typeof value
            // console.log('TYPE', type)

            

            ////////////////////////////////////

            if (typeof value == "object") {
                circularCheck = circlular.indexOf(value)

                if (circularCheck >= 0) {
                    loc = circLoc[circularCheck] || "this"
                    flattened[prefix] = "[Circular (" + loc + ")]"
                    return
                }
                
                circlular.push(value)
                circLoc.push(prefix)

                ///////////////////////////////////////////////

                if (Array.isArray(value)) {
                    len = value.length
                        // if (len == 0) _route(prefix + "[]", null)
                    _route(prefix);

                    for (i = 0; i < len; i++) {
                        _route(prefix + "[" + i + "]", value[i])
                    }

                    return
                }

                ///////////////////////////////////////////////

                keys = Object.keys(value)
                len = keys.length

                ///////////////////////////////////////////////

                if (prefix) prefix = prefix + "."
                if (len == 0) _route(prefix, null)

                ///////////////////////////////////////////////

                for (i = 0; i < len; i++) {
                    _route(prefix + keys[i], value[keys[i]])
                }

                return
            }

            flattened[prefix] = value
        }

        ////////////////////////////////////////

        //Start with the top object
        _route("", obj)

        ////////////////////////////////////////

        return flattened
    }

    ///////////////////////////////

    return service;
})

//////////////////////////////////////////////////////////////////////
app.controller('FlatviewController', function($rootScope, FlattenService, $uibModal, $scope) {


    var flattened = FlattenService.flatten($scope.model);

    ////////////////////////////////////////////////////

    // function getLabel(key) {

    //     if(!$scope.definition) {
    //         return;
    //     }

    //     var trail = [];
    //     var result = {};

    //     // function callback(actualTrail) {
    //     //     return actualTrail;
    //     // }

    //     getFieldPathTrail($scope.model, target, trail, result);

    //     if(target.parentType == 'interaction') {
    //         return 'interaction.' + result.trail.join('.');
    //     } else {
    //         return 'data.' + result.trail.join('.');
    //     }
    // }
    // //////////////////////////////////////////


    // function getFieldPathTrail(array, target, trail, result) {
    //     //return $scope.getTrail($scope.model, field, []);

    //     for (var key in array) {
    //         var field = array[key];

    //         if (field == target) {

    //             if(!field.asObject && field.directive != 'embedded') {
    //                 trail.push(field.key);
    //             } else {

    //                 if (field.minimum == field.maximum == 1) {
    //                     trail.push(field.key);
    //                 } else {
    //                     trail.push(field.key + '[i]');
    //                 }

    //             }


    //             //trail.push(field.key);
    //             result.trail = trail.slice();
    //             return;

    //             //return callback(trail.slice());
    //         }

    //         if (field.fields && field.fields.length) {

    //             if (field.asObject || field.directive == 'embedded') {

    //                 if (field.minimum == field.maximum == 1) {
    //                     trail.push(field.key);
    //                 } else {
    //                     trail.push(field.key + '[i]');
    //                 }

    //             }

    //             getFieldPathTrail(field.fields, target, trail, result);

    //             if (field.asObject || field.directive == 'embedded') {
    //                 trail.pop();
    //             }

    //         }
    //     }
    // }



    ////////////////////////////////////////////////////

    $scope.flat = _.chain(flattened)
    .map(function(value, key) {

        if(!value) {
            return;
        }
       
        return {
            key:key,
            value:value,
            // label:label || key,
        }
    })
    .compact()
    .value();

    ////////////////////////////////////////////////////



    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

   


})