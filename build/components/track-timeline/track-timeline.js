app.directive('trackTimeline', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            visual: '=',
            color: '=',
        },
        replace: true,
        templateUrl: 'track-timeline/track-timeline.html',
        controller: 'TrackTimelineController',
    };
});

app.controller('TrackTimelineController', function($scope) {


    var today = new Date();
    today.setHours(0, 0, 0, 0);

    ////////////////////////////////////////////////////////

    if (!$scope.visual) {
        $scope.visual = {
            display: 'week',
        };
    }

    ////////////////////////////////////////////////////////

    var dayStringFormat = 'Y-m-d';
    var weekStringFormat = 'Y-W';
    var monthStringFormat = 'Y-m';


    ////////////////////////////////////////////////////////

    $scope.$watch('model', function(model) {

        if (!model) {
            return;
        }

        ////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////

        var days = _.reduce(_.range(365), function(set, i) {

            var dateStringFormat = dayStringFormat;

            //Take 1 day
            var days = 1 * 86400 * 1000;
            var date = new Date(today.getTime() - (i * days))
            var dateString = date.format(dateStringFormat);
            var dateLabel = date.format('l j M Y');
            var dateLegend = date.format('j/M');

            set[dateString] = {
                legend: dateLegend,
                label: dateLabel,
                dateKey: dateString,
                data: [],
                events: [],
                total: 0,
            }

            return set;
        }, {});


        ////////////////////////////////////////////////////////

        var weeks = _.reduce(_.range(52), function(set, i) {

            var dateStringFormat = weekStringFormat;

            //Take 7 days
            var days = 7 * 86400 * 1000;
            var date = new Date(today.getTime() - (i * days))
            var dateString = date.format(dateStringFormat);
            var dateLabel = 'Week ' + date.format('W Y');
            var dateLegend = date.format('W');

            set[dateString] = {
                legend: dateLegend,
                label: dateLabel,
                dateKey: dateString,
                data: [],
                events: [],
                total: 0,
            }

            return set;
        }, {});

        ////////////////////////////////////////////////////////

        var months = _.reduce(_.range(12), function(set, i) {
            var dateStringFormat = monthStringFormat;

            //Take 7 days
            var days = 31 * 86400 * 1000;
            var date = new Date(today.getTime() - (i * days))
            var dateString = date.format(dateStringFormat);
            var dateLabel = date.format('M Y');
            var dateLegend = date.format('M Y');

            set[dateString] = {
                legend: dateLegend,
                label: dateLabel,
                dateKey: dateString,
                data: [],
                events: [],
                total: 0,
            }

            return set;
        }, {});

        ////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////


        var stats = {
                day: {
                    min: 0,
                    max: 0,
                },
                month: {
                    min: 0,
                    max: 0,
                },
                week: {
                    min: 0,
                    max: 0,
                }
            }
            ////////////////////////////////////////////////////////

        _.each(model.events, function(event) {

            var eventDate = new Date(event.startDate);

            if (!event.checkins) {
                return;
            }

            /////////////////////////////////

            var dayKey = eventDate.format(dayStringFormat);
            var monthKey = eventDate.format(monthStringFormat);
            var weekKey = eventDate.format(weekStringFormat);

            /////////////////////////////////

            var matchDay = days[dayKey];
            var matchMonth = months[monthKey];
            var matchWeek = weeks[weekKey];

            /////////////////////////////////

            if (matchDay) {
                matchDay.hasEvents = true;
                matchDay.events.push(event);
                matchDay.total += event.checkins;
                // console.log('day', matchDay.total)

                if (!stats.day.min) {
                    stats.day.min = event.checkins;
                }
                stats.day.max = Math.max(matchDay.total, stats.day.max);
                stats.day.min = Math.min(matchDay.total, stats.day.min);
            }

            /////////////////////////////////

            if (matchMonth) {
                matchMonth.hasEvents = true;
                matchMonth.events.push(event);
                matchMonth.total += event.checkins;
                // console.log('day', matchMonth.total)


                if (!stats.month.min) {
                    stats.month.min = event.checkins;
                }
                stats.month.max = Math.max(matchMonth.total, stats.month.max);
                stats.month.min = Math.min(matchMonth.total, stats.month.min);
            }

            /////////////////////////////////

            if (matchWeek) {
                matchWeek.hasEvents = true;
                matchWeek.events.push(event);
                matchWeek.total += event.checkins;
                // console.log('day', matchWeek.total)


                if (!stats.week.min) {
                    stats.week.min = event.checkins;
                }
                stats.week.max = Math.max(matchWeek.total, stats.week.max);
                stats.week.min = Math.min(matchWeek.total, stats.week.min);
            }
        });

        //////////////////////////////////////

        $scope.days = _.chain(days)
            .values()
            .map(function(entry) {
                entry.opacity = entry.total / stats.day.max;
                entry.percent = (entry.opacity * 100);
                entry.opacity = Math.max(entry.opacity, 0.5);
                entry.width = 1 / 365 * 100;
                return entry;
            })
            .value()
            .slice(0, 90)
            .reverse()


        $scope.weeks = _.chain(weeks)
            .values()
            .map(function(entry) {
                entry.opacity = entry.total / stats.week.max;
                entry.percent = (entry.opacity * 100);
                entry.opacity = Math.max(entry.opacity, 0.5);
                entry.width = 1 / 52 * 100;
                return entry;
            })
            .value()
            .reverse();

        $scope.months = _.chain(months)
            .values()
            .map(function(entry) {
                entry.opacity = entry.total / stats.month.max;
                entry.percent = (entry.opacity * 100);
                entry.opacity = Math.max(entry.opacity, 0.5);
                entry.width = 1 / 12 * 100;
                return entry;
            })
            .value()
            .reverse();


        var daysWithEvents = _.filter(days, 'hasEvents');
        var weeksWithEvents = _.filter(weeks, 'hasEvents');
        var monthsWithEvents = _.filter(months, 'hasEvents');


        $scope.stats = stats;
        $scope.averageDay = Math.round($scope.model.total / daysWithEvents.length);
        $scope.averageWeek =Math.round( $scope.model.total / weeksWithEvents.length);
        $scope.averageMonth = Math.round($scope.model.total / monthsWithEvents.length);




    })

});