app.directive('ticketSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-ticket-select/admin-ticket-select.html',
        controller: 'TicketSelectController',
        link:function($scope, $element) {
            $scope.$element = $element;
        }
    };
});



app.controller('TicketSelectController', function($scope) {

    ////////////////////////////////////////////////////

    //Create a model if it doesn't exist
    if (!$scope.model) {
        $scope.model = [];
    } 


    //Create a new object
    $scope._new = {}

    /////////////////////////////////////////////

    // $scope.$watch('_new', function(proposedOption) {
    //     if (!proposedOption.event || !proposedOption.event.length) {
    //         startWatchingTitle();
    //     } else {
    //         stopWatchingTitle();
    //     }
    // })

    /////////////////////////////////////////////

    // var watchTitle;

    // function stopWatchingTitle() {
    //     if (watchTitle) {
    //         watchTitle();
    //     }
    // }

    // function startWatchingTitle() {

    //     if (watchTitle) {
    //         watchTitle();
    //     }

    //     watchTitle = $scope.$watch('_new.name', function(newValue) {
    //         if (newValue) {
    //             $scope._new.event = newValue;//_.camelCase(newValue); //.toLowerCase();
    //         }
    //     });

    // }

    ////////////////////////////////////////////////////

    $scope.add = function() {
        var insert = angular.copy($scope._new);

        if(!$scope.model) {
            $scope.model = [];
        }
        
        $scope.model.push(insert);
        $scope._new = {};
         // $scope.$broadcast('add');

         console.log('REFOCUS')
          $scope.$element.find('.focus-back').focus();
    }

    ////////////////////////////////////////////////////

    $scope.remove = function(entry) {
        _.pull($scope.model, entry);
    }

    ////////////////////////////////////////////////////

    $scope.contains = function(entry) {
        return _.contains($scope.model, entry);
    }

})


