app.directive('productReminderSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-product-reminder-select/admin-product-reminder-select.html',
        controller: 'ProductReminderSelectController',
    };
});





app.controller('ProductReminderSelectController', function($scope) {

    if (!$scope.model || !_.isArray($scope.model)) {
        $scope.model = [];
    }

    $scope.reset = function() {
        //Create a new object
        $scope._new = {
            total: 30,
            period: 'day',
            when: 'before',
            point: 'renewal',
        }
    }


    $scope.reset();

    $scope.getPlural = function(number) {
        number = parseInt(number)

        if (number != 1) {
            return 's';
        }
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    /**
    $scope.getReminderTime = function(reminder) {

        var date;
        if(reminder.point == 'end') {
            date = moment($scope.endDate);
        } else {
            date = moment($scope.startDate);
        }

        var period = reminder.period;
        var total = parseInt(reminder.total);

        ///////////////////////////////

        if(reminder.when == 'after') {
            date = date.add(total, period);//.toDate();
        } else {
            date = date.subtract(total, period);//.toDate();
        }

        ///////////////////////////////

        return date.toDate().format('g:ia l M Y') + ' (' +date.fromNow() + ')';
    }
    /**/

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    $scope.remove = function(item) {
        _.pull($scope.model, item);
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {

        var insert = angular.copy($scope._new);
        if (!$scope.model) {
            $scope.model = [];
        }

        if (!insert.period) {
            return;
        }

        if (!insert.total) {
            return;
        }
        // if(insert.position) {
        // var keyExists = _.find($scope.model, {position:insert.position});

        // if(!keyExists) {
        $scope.model.push(insert);
        $scope.reset();
        // }
        // }
    }

})