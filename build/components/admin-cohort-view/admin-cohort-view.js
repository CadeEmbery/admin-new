app.directive('cohortWeekView', function() {

    return {
        restrict: 'E',
        replace: true,
        // scope: {
            // model: '=ngModel',
        //     filterParents: '=ngParents',
        // },
        templateUrl: 'admin-cohort-view/admin-cohort-view.html',
        controller: 'AdminCohortWeekViewController',
    };
});

app.directive('cohortMonthView', function() {

    return {
        restrict: 'E',
        replace: true,
        // scope: {
            // model: '=ngModel',
        //     filterParents: '=ngParents',
        // },
        templateUrl: 'admin-cohort-view/admin-cohort-view.html',
        controller: 'AdminCohortMonthViewController',
    };
});


app.service('AdminCohortViewService', function($q, FluroContent) {

    var service = {};

    ////////////////////////////////

    service.tooltip = function(week) {

        var string = '';

        _.each(week, function(checkin) {

            if(!checkin.i) {
                return;
            }

            string += new Date(checkin.i.startDate).format('D j M') + ' - ' + checkin.i.title + '<br/>'; 
        });


        return string;
    }

    ////////////////////////////////

    service.retrieve = function(contacts, periods,  options) {

        if(!options) {
            options = {};
        }

        var deferred = $q.defer();

        ///////////////////////////////////////////////

        var contactIDs = _.chain(contacts)
        .compact()
        .map(function(contact) {
            if(contact._id) {
                return contact._id;
            }

            return contact;
        })
        .uniq()
        .value();

        ///////////////////////////////////////////////

        var postData = {
            contacts:contactIDs,
        }

        if(options.period) {
            postData.period = options.period;
        }


        var weekFormat = 'W Y';
        var monthFormat = 'M Y';
        var yearFormat = 'Y';

        ///////////////////////////////////////////////

        FluroContent.endpoint('contact/cohort').save(postData).$promise.then(statsLoaded, statsFailed);

        ///////////////////////////////////////////////

        function statsLoaded(stats) {

            //Map all the data so we can get the keys

            _.each(stats, function(statGroup) {
                
                //Mapped
                _.each(statGroup, function(checkin) {
                    
                    var date = new Date(checkin.d);

                    switch(options.period) {
                        case 'months':
                            checkin.periodKey = date.format(monthFormat);
                        break;
                        default:
                            checkin.periodKey = date.format(weekFormat);
                        break;
                    }

                });
            });

            /////////////////////////////////////////////////////

            _.each(contacts, function(contact) {

                var contactStats = stats[contact._id];
                
                //Checkins
                var checkins = _.map(periods, function(periodKey) {
                    return _.filter(contactStats, {periodKey:periodKey});
                });

                //Get the total checkins
                var total = _.sum(checkins, function(week) {
                    return week.length;
                })

                contact.cohortData = {
                    periods:checkins,
                    total:total,
                }
            });

            return deferred.resolve(stats);
        }

        function statsFailed(err) {
            return deferred.reject(stats);
        }   

        ///////////////////////////////////////////////

        return deferred.promise;
    }

    ////////////////////////////////

    return service;
});

/////////////////////////////////////////////////////////////

app.controller('AdminCohortMonthViewController', function($scope, AdminCohortViewService, FluroContent) {

    var dateStringFormat ='M Y';
    var today = new Date();
    var limit = 12;


    var periods = _.chain(_.range(limit))
    .map(function(set, i) {
        var days = 31 * 86400 * 1000;
        var date = new Date(today.getTime() - (i * days))
        var dateString = date.format(dateStringFormat);
        return dateString;
    })
    .uniq()
    .compact()
    .value()
    .reverse();

    // var periods = _.map(_.range(limit), function(set, i) {
    //     var days = 31 * 86400 * 1000;
    //     var date = new Date(today.getTime() - (i * days))
    //     var dateString = date.format(dateStringFormat);
    //     return dateString;
    // })
    // .reverse();


    $scope.periodName = function(i, period) {
        return period;
    }


    $scope.periods = periods;
    $scope.cohortSettings = {};
    $scope.cohortTooltip = AdminCohortViewService.tooltip;

    ///////////////////////////////////////////////

    $scope.$watch('pageItems', listChanged);

    ///////////////////////////////////////////////

    function listChanged(contacts) {

        $scope.cohortSettings.loading = true;
        
        ///////////////////////////////////////////////

        var promise = AdminCohortViewService.retrieve(contacts, periods, {period:'months'});

        ///////////////////////////////////////////////

        function resolved(res) {
            $scope.cohortSettings.loading = false;
        }

        ///////////////////////////////////////////////

        return promise.then(resolved, resolved);

    }




});



/////////////////////////////////////////////////////////////

app.controller('AdminCohortWeekViewController', function($scope, AdminCohortViewService, FluroContent) {

    var dateStringFormat ='W Y';
    var today = new Date();
    var limit = 12;

    var periods = _.chain(_.range(limit))
    .map(function(set, i) {
        var days = 7 * 86400 * 1000;
        var date = new Date(today.getTime() - (i * days))
        var dateString = date.format(dateStringFormat);
        return dateString;
    })
    .uniq()
    .compact()
    .value()
    .reverse();


    $scope.periodName = function(i, period) {
        if(i == (limit-1)) {
            return 'This Week';
        } 
        return (limit-i) + ' weeks';
    }


    $scope.periods = periods;
    $scope.cohortSettings = {};
    $scope.cohortTooltip = AdminCohortViewService.tooltip;

    ///////////////////////////////////////////////

    $scope.$watch('pageItems', listChanged);

    ///////////////////////////////////////////////

    ///////////////////////////////////////////////

    function listChanged(contacts) {

        $scope.cohortSettings.loading = true;
        
        ///////////////////////////////////////////////

        var promise = AdminCohortViewService.retrieve(contacts, periods, {period:'weeks'});

        ///////////////////////////////////////////////

        function resolved(res) {
            $scope.cohortSettings.loading = false;
        }

        ///////////////////////////////////////////////

        return promise.then(resolved, resolved);

    }




});