app.directive('permissionSetManager', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-permission-set-manager/admin-permission-set-manager.html',
        controller: 'PermissionSetManager',
    };
});


app.controller('PermissionSetManager', function($scope, $state, ModalService, $rootScope, FluroAccess) {

    if (!$scope.model) {
        $scope.model = [];
    }

    $scope._proposed = {
        roles: [],
        realms: []
    }

    $scope.canEditRole = function(item) {
        
        var access = FluroAccess.canEditItem(item);

        return access;
    }

    $scope.editRole = function(item) {
        ModalService.edit(item, function(result) {
            //$state.reload();
        }, function(result) {});
    }

    ///////////////////////////////////////////

    ///////////////////////////////////////////

    $scope.add = function() {

        var copy = angular.copy($scope._proposed);
        $scope.model.push(copy);

        $scope._proposed = {
            roles: [],
            realms: []
        }


    }

    ///////////////////////////////////////////

    $scope.remove = function(field) {
        _.pull($scope.model, field);
    }

});