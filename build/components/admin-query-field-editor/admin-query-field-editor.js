app.directive('queryFieldEditor', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-query-field-editor/admin-query-field-editor.html',
        controller: 'QueryFieldEditorController',
    };
});


app.controller('QueryFieldEditorController', function($scope) {


	if(!$scope.model) {
		$scope.model = [];
	}
	
    $scope.newField = {}


    this.add = function() {

    	var copy = angular.copy($scope.newField);

    	$scope.model.push(copy);

    	$scope.newField = {}
    }

    this.remove = function(field) {
    	_.pull($scope.model, field);
    }

});