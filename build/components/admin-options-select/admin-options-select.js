app.directive('optionsSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-options-select/admin-options-select.html',
        controller: 'OptionsSelectController',
        link:function($scope, $element) {
            $scope.$element = $element;
        }
    };
});



app.controller('OptionsSelectController', function($scope) {

    ////////////////////////////////////////////////////

    //Create a model if it doesn't exist
    if (!$scope.model) {
        $scope.model = [];
    } 


    //Create a new object
    $scope._new = {}

    /////////////////////////////////////////////

    $scope.$watch('_new', function(proposedOption) {
        if (!proposedOption.value || !proposedOption.value.length) {
            startWatchingTitle();
        } else {
            stopWatchingTitle();
        }
    })

    /////////////////////////////////////////////

    var watchTitle;

    function stopWatchingTitle() {
        if (watchTitle) {
            watchTitle();
        }
    }

    function startWatchingTitle() {

        if (watchTitle) {
            watchTitle();
        }

        watchTitle = $scope.$watch('_new.name', function(newValue) {
            if (newValue) {
                $scope._new.value = newValue;//_.camelCase(newValue); //.toLowerCase();
            }
        });

    }

    ////////////////////////////////////////////////////

    $scope.add = function() {
        var insert = angular.copy($scope._new);
        $scope.model.push(insert);
        $scope._new = {};
         // $scope.$broadcast('add');

         console.log('REFOCUS')
          $scope.$element.find('.focus-back').focus();
    }

    ////////////////////////////////////////////////////

    $scope.remove = function(entry) {
        _.pull($scope.model, entry);
    }

    ////////////////////////////////////////////////////

    $scope.contains = function(entry) {
        return _.contains($scope.model, entry);
    }

})


