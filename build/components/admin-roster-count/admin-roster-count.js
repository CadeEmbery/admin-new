app.directive('rosterCount', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-roster-count/admin-roster-count.html',
        controller: 'RosterCountController',
    };
});

app.controller('RosterCountController', function($scope, $filter) {


    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////

    $scope.tooltip = renderTooltip();

    function renderTooltip() {

        //console.log('ROSTERS', $scope.model.rostered)

        var start = '<div class="text-left tooltip-block">';
        var end = '</div>';


        //////////////////////////////////////////////////

        function renderBlock(slot) {
            var blockStart = '<div class="col-xs-4 inline-column"><strong>' + slot.title + '</strong><br/>';
            var blockEnd = '</div>';

            var blockContent =_.map(slot.assignments, function(assignment) {
                var contactName = _.get(assignment, 'contact.title') || assignment.contactName;

                switch (assignment.confirmationStatus) {
                    case 'confirmed':
                        return ('<div class="brand-success" style="white-space:nowrap"><i class="far success fa-check fa-fw"></i> ' + contactName + '</div>');
                        break;
                    case 'denied':
                        return ('<div class="brand-danger" style="white-space:nowrap"><i class="fas fa-exclamation fa-fw"></i> ' + contactName + '</div>');
                        break;
                    case 'proposed':
                        return ('<div class="brand-primary" style="white-space:nowrap"><i class="fas fa-question fa-fw"></i> ' + contactName + '</div>');
                        break;
                    default:
                        return ('<div style="white-space:nowrap"><i class="text-muted far warning fa-clock fa-fw"></i> ' + contactName + '</div>');
                        break;
                }
            }).join(' ');

            //Return the html
            return blockStart + blockContent + blockEnd;
        }

        //////////////////////////////////////////////////

        function renderRow(roster) {
            var rowStart = '<div class="tooltip-roster"><h5>'+roster.title+'</h5><div class="row row-inline">';
            var rowEnd = '</div></div>';

            var rowContent = _.map(roster.slots, renderBlock).join('');

            //Return the html
            return rowStart + rowContent + rowEnd;
        }

        //////////////////////////////////////////////////

        var content = _.chain($scope.model.rostered)
            .map(renderRow)
            .value()
            .join(' ');

        //////////////////////////////////////////////////

        return start + content + end;

    }


    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////

    $scope.summary = _.chain($scope.model.rostered)
    .compact()
    .map(function(roster) {
        return roster.slots;
    })
    .flatten()
    .compact()
    .map(function(slot) {
        return slot.assignments
    })
    .flatten()
    .reduce(function(results, assignment) {

        var status = assignment.confirmationStatus

        var existing = results[status];

        if(!existing) {
            existing = {
                status:status,
                assignments:[],
            }

            results[status] = existing;
        }

        existing.assignments.push(assignment);
        existing.count = existing.assignments.length;

        return results;
    }, {})
    .value();

    if(!_.keys($scope.summary).length) {
        $scope.summary = null;
    }

    ////////////////////////////////////////////////////////


})