app.directive('keyValueSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            keyLabel:'@keylabel',
            valueLabel:'@valuelabel',

        },
        templateUrl: 'admin-key-value-select/admin-key-value-select.html',
        controller: 'KeyValueSelectController',
    };
});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
 
                event.preventDefault();
            }
        });
    };
});



app.controller('KeyValueSelectController', function($scope) {

    if(!$scope.keyLabel || !$scope.keyLabel.length) {
        $scope.keyLabel = 'Key';
    }

    if(!$scope.valueLabel || !$scope.valueLabel.length) {
        $scope.valueLabel = 'Value';
    }

    if (!$scope.model || (_.isObject($scope.model) && _.isArray($scope.model))) {
        $scope.model = {};
    }


    //Create a new object
    $scope._new = {}

    ////////////////////////////////////////////////////

    $scope.remove = function(key) {
        delete $scope.model[key];
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {
        var insert = angular.copy($scope._new);
        if(!$scope.model) {
            $scope.model = {}
        }
        if(insert.key && !$scope.model[insert.key]) {
            $scope.model[insert.key] = insert.value;
            $scope._new = {}
        }
    }

})


