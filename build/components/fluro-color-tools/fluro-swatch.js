app.directive('swatch', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        replace: true,
        scope: {
            model: '=ngModel',
        },
        template: '<div class="swatch" style="background-color:{{model}}"><i class="far fa-eyedropper" ng-if="!model"></i></div>',
    };
});