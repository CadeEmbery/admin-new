app.controller('GroupModalController', function($scope, Batch, contacts, $filter,  groups) {

    $scope.contacts = contacts;
    $scope.groups = groups;

    $scope.search = {};

    //////////////////////////////////////////////



    $scope.groupTypes = _.reduce(groups, function(results, group) {

        var definition = 'team';
        if (group.definition && group.definition.length) {
            definition = group.definition;
        }

        var existing = _.find(results, {
            definition: definition
        });

        if (!existing) {
            existing = {
                definition: definition,
                title: definition,
                teams: []
            }

            results.push(existing);
        }

        existing.teams.push(group);

        return results;
    }, []);

    //////////////////////////////////////////////

    $scope.$watch('search.terms', updateSearch);


    // <!-- <tr ng-repeat="group in groupType.teams | filter:search.terms | orderBy:'title' track by group._id"> -->
    // <tr ng-repeat="group in filtered[groupType.definition] track by group._id">

    function updateSearch(terms) {

    	if(!terms) {
    		terms ='';
    	}

        $scope.filtered = _.reduce($scope.groupTypes, function(set, typeSet) {

        	var filteredTeams = typeSet.teams;

        	if(terms.length) {
        		filteredTeams = $filter('filter')(filteredTeams, terms);
        	}

            set[typeSet.definition] = filteredTeams;//typeSet.teams;
            return set;
        }, {})


    }

    //////////////////////////////////////////////

    //Add a track of what groups we actually added to
    //TODO
    $scope.addToGroup = function(group) {
        group.processing = true;


        var contactIDs = _.map(contacts, function(contact) {
            return contact._id;
        });


        var details = {
            ids: contactIDs,
            teams: [group],
        }

        //////////////////////////////////////////////

        return Batch.addToTeam(details, function(err, result) {
            group.processing = false;
            console.log('DONE', err, result);

        })
    }


    //////////////////////////////////////////////

    $scope.removeFromGroup = function(group) {
        group.processing = true;


        var contactIDs = _.map(contacts, function(contact) {
            return contact._id;
        });


        var details = {
            ids: contactIDs,
            teams: [group],
        }

        //////////////////////////////////////////////

        return Batch.removeFromTeam(details, function(err, result) {
            group.processing = false;
            console.log('DONE', err, result);

        })
    }

})