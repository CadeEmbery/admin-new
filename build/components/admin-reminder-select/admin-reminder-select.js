app.directive('reminderSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            slots: '=ngSlots',
            startDate:'=ngStartDate',
            endDate:'=ngEndDate',
            readonly:'=ngReadonly',
            title:'=ngTitle',
        },
        templateUrl: 'admin-reminder-select/admin-reminder-select.html',
        controller: 'ReminderSelectController',
    };
});





app.controller('ReminderSelectController', function($scope) {



    // console.log('READONY', $scope.readonly);

    if (!$scope.model || !_.isArray($scope.model)) {
        $scope.model = [];
    }

    $scope.reset = function() {
        //Create a new object
        $scope._new = {
            total: 1,
            period: 'day',
            when: 'before',
            point: 'start',
            assignments: [],
            methods:[],
        }
    }


    $scope.reset();

    $scope.getPlural = function(number) {
        number = parseInt(number)

        if (number != 1) {
            return 's';
        }
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    $scope.getReminderTime = function(reminder) {

        var date;
        if(reminder.point == 'end') {
            date = moment($scope.endDate);
        } else {
            date = moment($scope.startDate);
        }

        var period = reminder.period;
        var total = parseInt(reminder.total);

        ///////////////////////////////

        if(reminder.when == 'after') {
            date = date.add(total, period);//.toDate();
        } else {
            date = date.subtract(total, period);//.toDate();
        }

        ///////////////////////////////

        return date.toDate().format('g:ia l M Y') + ' (' +date.fromNow() + ')';
    }

    ////////////////////////////////////////////////////

    $scope.reminderAssignmentSelected = function(reminder, assignmentName) {

        if (!assignmentName) {
            return;
        }
        var lowerCaseName = assignmentName.toLowerCase();
        return _.includes(reminder.assignments, lowerCaseName);
    }


    $scope.toggleReminderAssignment = function(reminder, assignmentName) {
        if (!assignmentName) {
            return;
        }
        var selected = $scope.reminderAssignmentSelected(reminder, assignmentName);

        var lowerCaseName = assignmentName.toLowerCase();

        if (selected) {
            _.pull(reminder.assignments, lowerCaseName);
        } else {
            if (!reminder.assignments) {
                reminder.assignments = [];
            }
            reminder.assignments.push(lowerCaseName);
        }
    }

    ////////////////////////////////////////////////////

    $scope.reminderMethodSelected = function(reminder, channelName) {

        if (!channelName) {
            return;
        }
        var lowerCaseName = channelName.toLowerCase();
        return _.includes(reminder.methods, lowerCaseName);
    }


    $scope.toggleReminderMethod = function(reminder, channelName) {
        if (!channelName) {
            return;
        }
        var selected = $scope.reminderMethodSelected(reminder, channelName);

        var lowerCaseName = channelName.toLowerCase();

        if (selected) {
            _.pull(reminder.methods, lowerCaseName);
        } else {
            if (!reminder.methods) {
                reminder.methods = [];
            }
            reminder.methods.push(lowerCaseName);
        }
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    $scope.remove = function(item) {
        _.pull($scope.model, item);
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {

        var insert = angular.copy($scope._new);
        if (!$scope.model) {
            $scope.model = [];
        }

        if (!insert.period) {
            return;
        }

        if (!insert.total) {
            return;
        }
        // if(insert.position) {
        // var keyExists = _.find($scope.model, {position:insert.position});

        // if(!keyExists) {
        $scope.model.push(insert);
        $scope.reset();
        // }
        // }
    }

})