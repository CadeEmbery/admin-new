app.directive('sheetManager', function() {

    return {
        restrict: 'E',
        replace: true,
        // scope: {
        //     pairs:'=',
        // },
        // Replace the div with our template
        templateUrl: 'admin-sheet-manager/admin-sheet-manager.html',
        controller: 'SheetManagerController',
    };
});


app.controller('SheetManagerController', function($scope, FluroAccess, ModalService, $timeout) {


    //////////////////////////////////////////////////

    $scope.canDefine = FluroAccess.can('create', 'definition');


    var params = {
        template: {
            parentType: 'contactdetail',
        }
    };

    //////////////////////////////////////////////////


    $scope.defineNew = function() {

        ModalService.create('definition', params, function(res) {

            console.log('Definition', res);
            $scope.definitions.push(res);

            //Update the view
            update();
        });
    }



    //////////////////////////////

    var detailSheets = $scope.extras.contactDetails;
    var sheetDefinitions = $scope.extras.detailDefinitions;

    //Get the definitions and paired contactdetail sheets
    $scope.sheetPairs = _.reduce(sheetDefinitions, function(results, definition) {

        var existingPair = _.find(results, {
            definitionName: definition.definitionName
        });


        if (!existingPair) {

            //Find all sheets that exist and match
            var matchedSheets = _.chain(detailSheets)
                .filter({
                    definition: definition.definitionName
                })
                .map(function(sheet) {
                    if (!sheet.data) {
                        sheet.data = {};
                    }
                    return sheet;
                })
                .value();


            existingPair = {
                _id: definition._id,
                definitionName: definition.definitionName,
                sheets: matchedSheets,
                definition: definition,
            }

            //Check if this sheet can be created
            FluroAccess.retrieveSelectableRealms('create', definition.definitionName, 'contactdetail').then(function(createableRealms) {
                $timeout(function() {
                    if (createableRealms.length) {
                        existingPair.createable = true;
                    }
                })
            })

            results.push(existingPair);
        }


        return results;
    }, [])



});