app.directive('localTable', function($filter) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            items: '=ngModel',
            columns: '=columns',
            search: '=search',
            name:'=',
        },
        templateUrl: 'local-table/local-table.html',
        link: function($scope, $element, $attrs) {

            // if(!$scope.search) {
            //     $scope.search = {
            //         order: 'title',
            //         reverse: false,
            //     }

            //     console.log('Create a new search')

            // }
            

            $scope.filteredItems = $scope.items;


            $scope.pager = {
                limit:25,
            }


            $scope.setOrder = function(key) {
                $scope.search.order = key;
            }

            $scope.setReverse = function(boolean) {
                $scope.search.reverse = boolean;
            }

           



            /////////////////////////////////////////////

            $scope.refilter = function() {

                var filteredItems = $scope.items;

                if ($scope.search.terms && $scope.search.terms.length) {
                    filteredItems = $filter('filter')($scope.items, $scope.search.terms);
                }

                $scope.filteredItems = filteredItems;
                $scope.resort();

            }

            //////////////////////////////////////////////////////

            $scope.resort = function() {

                var filteredItems = $scope.filteredItems;

                //////////////////////////////////////////////////////

                //Order the items
                filteredItems = _.sortBy(filteredItems, function(item) {
                    var val = _.get(item, $scope.search.order);

                    ///////////////////////////////

                    if (val) {

                        if (_.isArray(val) && val.length) {
                            val = val[0];
                        }

                        if (val.title) {
                            val = val.title;
                        } else {
                            if (val.name) {
                                val = val.name;
                            }
                        }
                    }

                    ///////////////////////////////

                    switch ($scope.search.orderRenderer) {
                        case 'boolean':
                            if (!val) {
                                return 0
                            } else {
                                return 1;
                            }
                            break;
                        case 'number':
                            if (!val) {
                                return -999999999999999;
                            } else {
                                return 9999999999999999;
                            }

                            return parseInt(val);
                            break;
                        case 'date':

                            var date = new Date(val);
                            return date.getTime();
                            break;
                        default:
                            return val || '';
                            break;
                    }

                });


                if ($scope.search.reverse) {
                    filteredItems.reverse();
                }

                //////////////////////////////////////////////////////

                $scope.sorted = filteredItems;
                $scope.updatePage();
            }

            /////////////////////////////////////////////

            $scope.updatePage = function() {

                if (!$scope.sorted) {
                    $scope.sorted = $scope.filteredItems;
                }

                if ($scope.sorted.length < ($scope.pager.limit * ($scope.pager.currentPage - 1))) {
                    $scope.pager.currentPage = 1;
                }

                var startIndex = ($scope.pager.currentPage - 1) * $scope.pager.limit;
                if (!startIndex) {
                    startIndex = 0;
                }
                var endIndex = startIndex + $scope.pager.limit;
                var pageItems = $scope.sorted.slice(startIndex, endIndex);

                $scope.pageItems = pageItems;
            }

            /////////////////////////////////////////////

             /////////////////////////////////////////////


            $scope.$watch('items', $scope.refilter);
            $scope.$watch('search.terms', $scope.refilter);
            $scope.$watch('search.order + search.reverse', $scope.resort);

            // $scope.refilter();

            /////////////////////////////////////////////

        }
    };
});