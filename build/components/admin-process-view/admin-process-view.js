app.directive('processView', function($sessionStorage, $timeout) {

    return {
        restrict: 'E',
        replace: true,
        // scope: {
        //     model: '=ngModel',
        //     filterParents: '=ngParents',
        // },
        templateUrl: 'admin-process-view/admin-process-view.html',
        controller: 'ProcessListViewController',
        link: function($scope, $element, $attr) {

            // //console.log('GET persist', $element, $sessionStorage.persist)


            // var container = $element.find('.process-columns');
            // var val = $sessionStorage.persist;

            // $timeout(function() {
            //     container.scrollLeft(val); // = / = scrollPersisterService.GetScrollTop();
            //     // //console.log('SET', $sessionStorage.persist, container.scrollLeft());
            // })


            // $scope.$on('$destroy', function() {
            //     $sessionStorage.persist = container.scrollLeft();
            //     // //console.log('GET', $sessionStorage.persist)
            // });
        }
    };
});



function PROCESS_CARD_PRIORITY_SORT(card) {

    var num = '2';
    var trailer = 0;
    var val;

    ///////////////////////////////////////////

    //If we are archived then add straight to the bottom of the list
    if (card.status == 'archived') {
        num = '4';
        val = parseFloat(num + '.' + trailer);
        return val + '-' + card.title;
    }

    ///////////////////////////////////////////

    //If we are complete then add us to the bottom of the list
    if (card.processStatus == 'complete') {
        num = '3';
        val = parseFloat(num + '.' + trailer);
        return val + '-' + card.title;
    }

    ///////////////////////////////////////////

    if (card.dueDate) {

        if(!card.moment) {
            card.moment = moment(card.dueDate);
        }

        var now = new Date();
        var duetime = card.moment.toDate().getTime()
        trailer = card.moment.toDate().getTime()

        if (duetime < now.getTime()) {
            //If it's overdue then we add it to the very very top
            num = '0';
        } else {
            //Otherwise just add it to the top of the 
            //pending cards
            num = '1';
        }
    }

    ///////////////////////////////////////////

    var val = parseFloat(num + '.' + trailer);

    return val + '-' + card.title;



}


app.controller('ProcessListViewController', function($scope, $rootScope, Fluro, FluroSocket, CacheManager, Batch, $state, FluroContent, Notifications, $uibModal, FluroStorage, ModalService) {


    ////////////////////////////////////////////////////////

    $scope.$watch('filteredItems', function() {
        return renderProgress(true)
    });


    var definition = $scope.definition;


    ////////////////////////////////////////////////////////

    $scope.tasksInColumn = function(column) {


        return _.chain(column.taskLists)
            .map('tasks')
            .flatten()
            .filter({
                required: true
            })
            .value()
    }
    ////////////////////////////////////////////////////////


    $scope.insertColumn = function() {


        var newStates = _.get(definition, 'data.states');
        if (!newStates) {
            newStates = [];
            _.set(definition, 'data.states', newStates);
        }

        //Get the next step
        var number = (newStates.length + 1);

        //Push a new state
        newStates.push({
            title: 'Step ' + number,
            key: 'step' + number,
        });

        ////////////////////////////////////////

        var updateDetails = {
            data: {
                states: newStates,
            }
        }

        ////////////////////////////////////////

        // console.log('UPDATE STATES', definedName, definition._id);
        var definedName = definition.definition || 'definition';

        ////////////////////////////////////////

        var request = FluroContent.resource(definedName).update({
            id: definition._id,
        }, updateDetails, saveComplete, saveFailed);

        ////////////////////////////////////////////

        function saveComplete(res) {
            // console.log('Save Completed', res, modalInstance);
            Notifications.status('Updated  \'' + definition.title + '\'');

            renderProgress();
        }


        function saveFailed(err) {
            Notifications.error(err);
        }

    }

    ////////////////////////////////////////////////////////

    function done(err, data) {

        if (err) {
            return Notifications.error(err);
        }


        ///////////////////////

        //Find all the caches we need to clear
        var caches = _.chain(data.results)
            .map(function(result) {
                return [result.definition, result._type];
            })
            .flatten()
            .value();

        ///////////////////////

        //Make sure we refresh the current view
        if ($scope.definition) {
            caches.push($scope.definition.definitionName)
        }
        caches.push($scope.type.path)

        ///////////////////////

        caches = _.uniq(caches);
        caches = _.compact(caches);

        _.each(caches, function(t) {
            CacheManager.clear(t);
        });

        ///////////////////////

        //Reload the state
        $state.reload();

        ///////////////////////

        //Deselect all
        // $scope.selection.deselectAll();
        /**/
        Notifications.status('Updated  ' + data.results.length + ' ' + $scope.definition.plural);
    }

    ///////////////////////////////////

    $scope.editColumn = function(column) {

        column.processing = true;
        var modalStatus = {};

        var modalInstance = $uibModal.open({
            template: '<process-column-editor ng-model="column" ng-definition="definition" process-definitions="processDefinitions" detail-definitions="detailDefinitions" processing="modalStatus.processing" close="closeModal" cancel="cancelModal"></process-column-editor>',
            size: 'md',
            backdrop: 'static',
            resolve: {
                processDefinitions: function(FluroContent) {
                    var promise = FluroContent.endpoint('defined/types/process').query().$promise;

                    promise.then(function() {

                    }, function() {
                        column.processing = false;
                    });

                    return promise;
                },
                detailDefinitions: function(FluroContent) {
                    var promise = FluroContent.endpoint('defined/types/contactdetail').query().$promise;

                    promise.then(function(res) {
                        //console.log('got em', res)
                    }, function() {
                        column.processing = false;
                    });

                    return promise;
                }
            },
            controller: function($scope, processDefinitions, detailDefinitions) {

                column.processing = false;

                $scope.column = column;
                $scope.definition = definition;
                $scope.modalStatus = modalStatus;
                $scope.processDefinitions = processDefinitions,
                    $scope.detailDefinitions = detailDefinitions,




                    // $scope.test = testEvent;
                    // $scope.isModal =true;

                    $scope.cancelModal = function() {
                        console.log('CLOSE MODAL', modalInstance);
                        modalInstance.close();
                    }



                $scope.closeModal = function() {

                    if (modalStatus.processing) {
                        return;
                    }

                    modalStatus.processing = true;

                    var definedName = 'definition';
                    if (definition.definition) {
                        definedName = definition.definition;
                    }

                    ////////////////////////////////////////////

                    //We only need to update the state information
                    var updateDetails = {
                        data: definition.data,
                    }

                    ////////////////////////////////////////////

                    console.log('Updating Definition', definition._id);

                    var request = FluroContent.resource(definedName).update({
                        id: definition._id,
                    }, updateDetails, saveComplete, saveFailed);

                    ////////////////////////////////////////////

                    function saveComplete(res) {
                        modalStatus.processing = false;
                        // console.log('Save Completed', res, modalInstance);
                        Notifications.status('Updated  \'' + column.title + '\'');

                        modalInstance.close();
                    }


                    function saveFailed(err) {
                        modalStatus.processing = false;
                        console.log('Save Error', err);
                        Notifications.error(err);
                    }
                }
            }
        });
    }



    ///////////////////////////////////

    $scope.archiveItems = function(items) {

        var ids = _.map(items, function(item) {
            if (item._id) {
                return item._id;
            } else {
                return item;
            }
        });

        var details = {
            // ids: $scope.selection.ids,
            ids: ids,
            status: 'archived',
        }

        Batch.setStatus(details, done);
    }

    ///////////////////////////////////

    $scope.click = function(card) {

        console.log('Clicked card', card);
        if (!card) {
            return;
        }
        card._type = 'process';
        card.definition = $scope.definition.definitionName;

        var canEdit = $rootScope.access.canEditItem(card);

        if (canEdit) {
            $rootScope.modal.edit(card);
        } else {
            $rootScope.modal.view(card);
        }

    }


    ///////////////////////////////////

    $scope.selectColumn = function(column) {
        $scope.selection.selectMultiple(column.items);
    }

    ///////////////////////////////////

    $scope.dragControlListeners = {
        // accept: function(sourceItemHandleScope, destSortableScope) {
        //     return true
        // },
        itemMoved: function(event) {


            var card = event.source.itemScope.card;
            card.processing = true;
            var from = event.source.itemScope.sortableScope;
            var to = event.dest.sortableScope;

            //Get the key we are moving to
            var toState = to.column.key;
            // //console.log(card, 'FROM', from, 'TO', to);

            FluroContent.resource($scope.definedType).update({
                    id: card._id,
                }, {
                    state: toState
                })
                .$promise
                .then(function(res) {
                    card.processing = false;
                    // //console.log(res);
                }, function(err) {
                    // ///console.log(err);
                    return Notifications.error(err);
                });

            // return syncItems([card._id], toState);
        }
    }

    ////////////////////////////////////////////////////

    function getWaitMessage(date) {

        var now = new Date();

        if (now.getTime() > date.getTime()) {
            return 'Waiting...'; //moment(date).fromNow(true);
        }

        return 'Reactivate ' + moment(date).fromNow();

        ////////////////////////////////////////////////////////

        var sameDay = (date.format('j M Y') == now.format('j M Y'));

        if (sameDay) {
            return 'Reactivate (' + date.format('g:ia') + ' Today'
        }

        //Check if it's the same week
        var sameWeek = (date.format('W Y') == now.format('W Y'));

        if (sameWeek) {
            return 'Reactivate (' + date.format('g:ia l') + ')';
        }

        //Check if it's the same Month
        var sameMonth = (date.format('M Y') == now.format('M Y'));

        if (sameMonth) {
            return 'Reactivate (' + date.format('D j M') + ')';
        }


        //Check if it's the same Month
        var sameYear = (date.format('Y') == now.format('Y'));

        if (sameYear) {
            return 'Reactivate (' + date.format('j M') + ')';
        }


        return 'Reactivate (' + date.format('j M Y') + ')';

    };


    ////////////////////////////////////////////////////

    function getDueDateTime(date) {



        var now = new Date();

        if (now.getTime() > date.getTime()) {
            return 'Due ' + moment(date).fromNow();
        }

        var sameDay = (date.format('j M Y') == now.format('j M Y'));

        if (sameDay) {
            return 'Due ' + date.format('g:ia') + ' Today'
        }

        //Check if it's the same week
        var sameWeek = (date.format('W Y') == now.format('W Y'));

        if (sameWeek) {
            return 'Due ' + date.format('g:ia l');
        }

        //Check if it's the same Month
        var sameMonth = (date.format('M Y') == now.format('M Y'));

        if (sameMonth) {
            return 'Due ' + date.format('D j M');
        }


        //Check if it's the same Month
        var sameYear = (date.format('Y') == now.format('Y'));

        if (sameYear) {
            return 'Due ' + date.format('j M');
        }


        return date.format('j M Y');

    };



    ///////////////////////////////////

    $scope.getMessage = function(card, state) {

        var now = new Date();

        if (card.status == 'archived') {
            return 'Archived';
        }

        if (!card.dueDate) {
            return 'No date set';
        }

        var date = card.moment;

        ////////////////////////////////////////////

        if (state.style == 'waiting') {
            switch (state.autoComplete) {
                case 'wait':
                    return getWaitMessage(date.toDate());
                    break;
                default:
                    break;
            }
        }

        if (card.processStatus == 'complete') {

            var automation = _.get(state, 'autoComplete');

            switch (automation) {
                case 'manual':
                    return 'Complete';
                    break;
                default:
                    return 'Will progress in ' + date.fromNow(true);
                    break;
            }
        }

        return getDueDateTime(date.toDate());
    }


    $scope.getClass = function(card) {

        var now = new Date();

        if (card.processing) {
            return 'processing';
        }

        if (card.status == 'archived') {
            return 'archived';
        }

        if (card.processStatus == 'complete') {
            return 'complete';
        }

        if (card.processStatus == 'failed') {
            return 'failed';
        }

        if (card.processStatus == 'pending') {
            return 'pending';
        }

        if (!card.dueDate) {
            return 'incomplete';
        }

        var date = card.moment;
        if (date && date.toDate().getTime() < now.getTime()) {
            return 'overdue';
        }

        return 'incomplete';
    }




    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    FluroSocket.on('content.edit', updateBoard);
    FluroSocket.on('content.create', updateBoard);
    FluroSocket.on('content.delete', updateBoard);
    FluroSocket.on('content.restore', updateBoard);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function updateBoard(data) {



        if (Batch.current.length) {
            return console.log('NO REFRESH WHEN BATCH IS IN PROCESS');
        }

        ////////////////////////////////////////

        if (!data.item) {
            return;
        }

        ////////////////////////////////////////

        var itemType = data.item._type;
        var itemDefinition = data.item.definition;
        var itemID = data.item._id;

        console.log('SOCKET', itemID, data.item.title, data.key);

        if (itemType == 'process' && itemDefinition == $scope.definition.definitionName) {
            switch (data.key) {

                case 'content.delete':
                    //Remove the item
                    var match = _.find($scope.items, {
                        _id: itemID
                    });
                    _.pull($scope.items, match);


                    break;
                case 'content.edit':
                case 'content.create':
                case 'content.restore':

                    //Something changed
                    FluroContent.resource(itemDefinition, true, true).get({
                        id: itemID,
                    }).$promise.then(function(updatedItem) {



                        var existingMatch = _.find($scope.items, {
                            _id: itemID
                        });

                        // console.log('GOT UPDATE', itemID, updatedItem.title, existingMatch.title);
                        if (existingMatch) {
                            _.assign(existingMatch, updatedItem);
                            existingMatch.processing = false;

                            return renderProgress();
                        } else {

                            console.log('ADD THE NEW ITEM');
                            $scope.items.push(updatedItem);
                            $scope.filteredItems.push(updatedItem);
                            return renderProgress();
                        }

                    })
                    break;
            }

        }
    }
    //////////////////////////////////////////////////

    $scope.$on("$destroy", function() {
        FluroSocket.off('content.edit', updateBoard);
        FluroSocket.off('content.create', updateBoard);
        FluroSocket.off('content.delete', updateBoard);
        FluroSocket.off('content.restore', updateBoard);
    });





    ///////////////////////////////////
    ///////////////////////////////////

    function renderProgress(sorting) {


        if ($scope.definition && $scope.definition.data) {

            var now = new Date();

            /////////////////////////////////////////////////////

            var processStates = $scope.definition.data.states;

            /////////////////////////////////////////////////////

            var columnStateLookup = _.reduce($scope.definition.data.states, function(set, state) {
                set[state.key] = state;
                state.items = [];

                /////////////////////////////////////

                //Reset all 0
                state.incomplete = 0;
                state.complete = 0;
                state.total = 0;

                /////////////////////////////////////

                return set;
            }, {});

            /////////////////////////////////////////////////////

            //Map the columns
            $scope.columns = _.chain($scope.filteredItems)
                .map(function(card) {

                    //Add the little results circles for each state
                    card.stateResults = _.chain(processStates)
                        .map(function(processState) {

                            if (processState.style == 'waiting') {
                                return;
                            }

                            var output = {
                                title: processState.title,
                                key: processState.key,
                            }

                            if (card.results) {
                                output.result = card.results[processState.key];
                            }

                            switch (output.result) {
                                case 'complete':
                                    output.icon = 'fas fa-check-circle brand-success';
                                    break;
                                case 'failed':
                                    output.icon = 'fas fa-exclamation-circle brand-danger';
                                    break;
                                case 'pending':
                                    output.icon = 'fas fa-minus-circle brand-warning';
                                    break;
                                default:

                                    if (card.state == processState.key) {
                                        output.icon = 'far fa-dot-circle text-muted';
                                    } else {
                                        output.icon = 'far fa-circle text-muted';
                                    }

                                    break;

                            }

                            return output;
                        })
                        .compact()
                        .value()

                    /////////////////////////////////////

                    if (card.dueDate) {
                        card.moment = moment(card.dueDate);
                    }

                    return card;

                })
                .reduce(function(set, card) {

                    //find the matching column
                    var column = set[card.state];

                    /////////////////

                    if (!column) {
                        column = set['_limbo'];
                    }

                    //If there is no limbo column
                    if (!column) {
                        column = set['_limbo'] = {
                            title: 'Issues / Stuck',
                            description: 'All cards that are lost or broken due to the board being changed after creation.',
                            items: [],
                        }
                    }


                    /////////////////////////////////////

                    column.items.push(card);


                    /////////////////////////////////////

                    var taskCount = card.taskCount;
                    if (!taskCount) {
                        return set;
                    }

                    /////////////////////////////////////

                    if (taskCount.incomplete) {
                        column.incomplete += taskCount.incomplete;
                    }
                    if (taskCount.complete) {
                        column.complete += taskCount.complete;
                    }
                    if (taskCount.total) {
                        column.total += taskCount.total;
                    }



                    return set;

                }, columnStateLookup)
                .values()
                .sortBy(function(column) {
                    //Sort the columns
                    return _.indexOf(processStates, column)
                })
                .map(function(column) {

                    ////////////////////////////////////////////////

                    if (!sorting) {
                        return column;
                    }

                    ////////////////////////////////////////////////

                    column.items = _.chain(column.items)
                        .sortBy(PROCESS_CARD_PRIORITY_SORT)

                        .value();

                    ////////////////////////////////////////////////

                    return column;
                })
                .value();

            /////////////////////////////////////////////////////




            // var processStates = $scope.definition.data.states;

            // _.reduce(processStates, function(column) {

            //     //Reset all 0
            //     column.incomplete = 0;
            //     column.complete = 0;
            //     column.total = 0;

            //     ////////////////////////////////////////////////

            //     //Map all items
            //     var columnItems = _.chain($scope.filteredItems)
            //         .filter(function(card) {
            //             return card.state == column.key;
            //         })
            //         .map(function(card) {

            //             //Add the little results circles for each state
            //             card.stateResults = _.chain(processStates)
            //                 .map(function(processState) {

            //                     if (processState.style == 'waiting') {
            //                         return;
            //                     }

            //                     var output = {
            //                         title: processState.title,
            //                         key: processState.key,
            //                     }

            //                     if (card.results) {
            //                         output.result = card.results[processState.key];
            //                     }

            //                     switch (output.result) {
            //                         case 'complete':
            //                             output.icon = 'fas fa-check-circle brand-success';
            //                             break;
            //                         case 'failed':
            //                             output.icon = 'fas fa-exclamation-circle brand-danger';
            //                             break;
            //                         case 'pending':
            //                             output.icon = 'fas fa-minus-circle brand-warning';
            //                             break;
            //                         default:

            //                             if(card.state == processState.key) {
            //                                 output.icon = 'far fa-dot-circle text-muted';
            //                             } else {
            //                                 output.icon = 'far fa-circle text-muted';
            //                             }

            //                             break;

            //                     }

            //                     return output;
            //                 })
            //                 .compact()
            //                 .value()

            //             /////////////////////////////////////

            //             if (card.dueDate) {
            //                 card.moment = moment(card.dueDate);
            //             }

            //             /////////////////////////////////////

            //             var taskCount = card.taskCount;
            //             if (!taskCount) {
            //                 return card;
            //             }

            //             /////////////////////////////////////

            //             if (taskCount.incomplete) {
            //                 column.incomplete += taskCount.incomplete;
            //             }
            //             if (taskCount.complete) {
            //                 column.complete += taskCount.complete;
            //             }
            //             if (taskCount.total) {
            //                 column.total += taskCount.total;
            //             }

            //             /////////////////////////////////////

            //             return card;
            //         })
            //         .value();

            //     ////////////////////////////////////////////////

            //     if (!sorting) {
            //         column.items = columnItems;
            //     }

            //     ////////////////////////////////////////////////

            //     column.items = _.chain(columnItems)
            //         .sortBy(function(card) {

            //             var num = '2';
            //             var trailer = 0;
            //             var val;

            //             ///////////////////////////////////////////

            //             //If we are archived then add straight to the bottom of the list
            //             if (card.status == 'archived') {
            //                 num = '4';
            //                 val = parseFloat(num + '.' + trailer);
            //                 return val + '-' + card.title;
            //             }

            //             ///////////////////////////////////////////

            //             //If we are complete then add us to the bottom of the list
            //             if (card.processStatus == 'complete') {
            //                 num = '3';
            //                 val = parseFloat(num + '.' + trailer);
            //                 return val + '-' + card.title;
            //             }

            //             ///////////////////////////////////////////

            //             if (card.dueDate) {

            //                 var now = new Date();
            //                 var duetime = card.moment.toDate().getTime()
            //                 trailer = card.moment.toDate().getTime()

            //                 if (duetime < now.getTime()) {
            //                     //If it's overdue then we add it to the very very top
            //                     num = '0';
            //                 } else {
            //                     //Otherwise just add it to the top of the 
            //                     //pending cards
            //                     num = '1';
            //                 }
            //             }

            //             ///////////////////////////////////////////

            //             var val = parseFloat(num + '.' + trailer);

            //             return val + '-' + card.title;



            //         })
            //         .value();

            // })

            /**
            _.each($scope.definition.data.states, function(state) {

                state.items = _.chain($scope.filteredItems)
                    .filter({
                        state: state.key
                    })
                    .map(function(card) {

                        if(card.dueDate) {
                            card.moment = moment(card.dueDate);
                        }

                        if (card.criticalTasks && card.criticalTasks.length) {
                            card.complete = _.every(card.criticalTasks, {
                                complete: true
                            });
                        }

                        return card;
                    })
                    .value();

               
                    
            })

            /**/
        }
    }

    ///////////////////////////////////
    ///////////////////////////////////
    ///////////////////////////////////
    ///////////////////////////////////

    /**
    function syncItems(items, key) {

        var processID = $scope.item._id;

        var request = FluroContent.endpoint('process/' + processID + '/progress').save({
                items: items,
                state: key,
            })
            .$promise;


        /////////////////////////////////////////

        function success(res) {

            var results = res.results;

            //Merge the results 
            // var leftovers = _.differenceBy(progressItems, results, function(item) {
            //     return item._id;
            // });

            var leftovers = _.filter(progressItems, function(item) {
                return !_.some(results, {
                    _id: item._id
                });
            });


            leftovers = leftovers.concat(results);
            progressItems = leftovers;

            //Update the board
            return renderProgress();
        }

        /////////////////////////////////////////

        function failed(res) {
            //console.log('Fail', res);
        }

        //Run the request
        request.then(success, failed);
    }

    ///////////////////////////////////
    ///////////////////////////////////
    ///////////////////////////////////
    ///////////////////////////////////

/**
    $scope.addItems = function(column) {



        // if ($scope.params.searchInheritable) {
        //     params.searchInheritable = $scope.params.searchInheritable = true;
        // }

        var params = {};
        var modelSource = {};


        var typeName;

        if ($scope.item.types && $scope.item.types.length) {
            typeName = $scope.item.types[0];
        }


        var modal = ModalService.browse(typeName, modelSource, params);


        /////////////////////////////////////

        function modalClosed() {
            syncItems(modelSource.items, column.key);
        }

        /////////////////////////////////////


        modal.result.then(modalClosed, modalClosed)

    }
/**/

});