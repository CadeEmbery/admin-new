app.directive('redirectSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-redirect-select/admin-redirect-select.html',
        controller: 'RedirectSelectController',
    };
});


app.controller('RedirectSelectController', function($scope) {

	if(!$scope.model) {
		$scope.model = [];
	}

	$scope._new = {}

	///////////////////////////////////////

	$scope.remove = function(item) {
		return _.pull($scope.model, item);
	}

	$scope.create = function() {

		var copy = angular.copy($scope._new);

		if(!copy.from || !copy.from.length) {
			return;
		}

		if(!copy.to || !copy.to.length) {
			return;
		}

		var existing = _.find($scope.model, {from:copy.from});


		$scope.model.push(copy);
		$scope._new = {};

	}
})


app.directive('legacyRedirectSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-redirect-select/legacy-redirect-select.html',
        controller: 'KeyValueSelectController',
    };
});
