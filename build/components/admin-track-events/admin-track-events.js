app.directive('trackEvents', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            browseDate: '=ngDate',
            type: '=ngType',
        },
        templateUrl: 'admin-track-events/admin-track-events.html',
        controller: 'TrackEventsController',
    };
});





app.controller('TrackEventsController', function($scope, $rootScope, FluroStorage, CacheManager, FluroAccess, $state, ModalService) {

    //////////////////////////////////////////
    /**
    function getDatesRangeArray(startDate, endDate, interval, total) {
        var config = {
                interval: interval || 'days',
                total: total || 1
            },
            dateArray = [],
            currentDate = startDate.clone();

        while (currentDate < endDate) {
            dateArray.push(currentDate);
            currentDate = currentDate.clone().add(config.total, config.interval);
        }

        return dateArray;
    };

    /**
    function getRange(startDate, endDate, addFn, interval) {

         addFn = addFn || Date.prototype.addDays;
         interval = interval || 1;

         var retVal = [];
         var current = new Date(startDate);

         while (current <= endDate) {
          retVal.push(new Date(current));
          current = addFn.call(current, interval);
         }

         return retVal;

        }
/**/


    // function isDate(dateArg) {
    //     var t = (dateArg instanceof Date) ? dateArg : (new Date(dateArg));
    //     return !isNaN(t.valueOf());
    // }

    // function isValidRange(minDate, maxDate) {
    //     return (new Date(minDate) <= new Date(maxDate));
    // }

    // function getDates(startDt, endDt) {
    //     var error = ((isDate(endDt)) && (isDate(startDt)) && isValidRange(startDt, endDt)) ? false : true;
    //     var between = [];

    //     if (error) {
    //         console.log('error occured!!!... Please Enter Valid Dates');
    //     } else {

    //         var currentDate = new Date(startDt),
    //             end = new Date(endDt);
    //         while (currentDate <= end) {
    //             between.push(new Date(currentDate));
    //             currentDate.setDate(currentDate.getDate() + 1);
    //         }
    //     }
    //     return between;
    // }


    $scope.getDayEvents = function(day, track) {

        var checkDate = new Date(day);
        checkDate.setHours(0);
        checkDate.setMinutes(0);
        checkDate.setSeconds(0);
        checkDate.setMilliseconds(0);
        var stringCheckDate = checkDate.getTime();

        return _.filter(track, function(event) {

            var itemDate = new Date(event.startDate);
            itemDate.setHours(0);
            itemDate.setMinutes(0);
            itemDate.setSeconds(0);
            itemDate.setMilliseconds(0);

            // console.log('check', itemDate.getTime(), stringCheckDate)
            return itemDate.getTime() == stringCheckDate;
        })
    }

    //////////////////////////////////////////

    $scope.createTrack = function() {

        function createSuccess(res) {
console.log('Created Event Track', res)
        }

        function createCancel(err) {
console.log('Cancelled Event Track', err)
        }

        ModalService.create('eventtrack', {}, createSuccess, createCancel);
    }

    //////////////////////////////////////////

    $scope.$watch('browseDate', function(browseDate) {

        var rangeStart = moment(browseDate).toDate(); //.subtract(90, 'days').toDate();
        var rangeEnd = moment(browseDate).add(90, 'days').toDate();

        $scope.range = moment.range(rangeStart, rangeEnd).toArray('days');
    });

    //////////////////////////////////////////

    $scope.$watch('model', function(data) {
        $scope.tracks = _.chain(data)
        // .sortBy(function(item) {
        //     return item.startDate;
        // })
        .filter(function(item) {
            if (item.track) {
                return true;
            }
        })
            .groupBy(function(item) {

                return item.track._id
            })
            .value();


        // console.log('TRACK DATA', $scope.tracks, data);


    })

});