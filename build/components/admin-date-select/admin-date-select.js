


app.directive('dateselect', function($document, $timeout) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'admin-date-select/admin-date-select.html',
        scope: {
            boundModel: '=ngModel',
            label: '=ngLabel',
            minDate: '=minDate',
            initDate: '=initDate',
            useTime: '=useTime',
            required: '=',
            rounding: '=',
            forceDate: '=',
            direction: '@',
        },
        link: function($scope, element, attr) {

            $scope.settings = {
                open:false,
            }

            // console.log('STARTING', $scope.boundModel, $scope.forceDate, $scope.initDate);
            if($scope.boundModel == 'now') {
                $scope.boundModel = new Date();
            }

            $scope.dateIsSet = false;

            ////////////////////////////////////////////////

            function elementClick(event) {
                //Clicked inside
                event.stopPropagation();
            }

            function documentClick(event) {
                //Clicked outside
               $timeout(function() {
                    $scope.settings.open = false;
                });
            }

            //Listen for when this date select is open
            $scope.$watch('settings.open', function(bol) {
                if (bol) {
                    $scope.dateIsSet = true;
                    element.on('click', elementClick);
                    $document.on('click', documentClick);
                } else {

                    element.off('click', elementClick);
                    $document.off('click', documentClick);
                }
            })

        },
        controller: function($scope, $timeout) {

            // //console.log('INIT BOUND MODEL', $scope.boundModel);

            if(!$scope.settings) {
                $scope.settings = {};
            }

            if($scope.initDate) {
                $scope.settings.dateModel = new Date($scope.settings.dateModel);
            }


            if($scope.forceDate && !$scope.boundModel) {
                $scope.boundModel = new Date();
            }

            if($scope.boundModel) {
                $scope.dateIsSet = true;
            }

            ///////////////////////////////////////

            //Rounding factor
            var coeff = 1000 * 60 * 5;

            // if ($scope.boundModel) {

            //     $scope.settings.model = new Date($scope.boundModel);
            // } else {
            //     $scope.settings.model = new Date();
            // }

            ///////////////////////////////////////

            // $scope.$watch('settings.open', function(open) {
            //     if (open) {
            //         if(!$scope.boundModel) {
            //             $scope.boundModel = new Date($scope.settings.model);
            //         }
            //     }
            // })

            ///////////////////////////////////////

            $scope.removeDate = function() {
                //console.log('Remove DATE');
                $scope.boundModel = null;
                $scope.settings.open = false;
                $scope.dateIsSet = false;
                ////console.log('Remove Date')
            }

            ///////////////////////////////////////

            //round to nearest 5mins
            if ($scope.rounding) {
                // if (_.isDate($scope.settings.model)) {
                    // $scope.settings.model = new Date(Math.round($scope.settings.model.getTime() / coeff) * coeff)
                // }
                if (_.isDate($scope.boundModel)) {
                    $scope.boundModel = new Date(Math.round($scope.boundModel.getTime() / coeff) * coeff)
                }
            }

            ///////////////////////////////////////
            ///////////////////////////////////////

            function updateLabel() {

                var today = new Date();

                // //console.log('RESET Bound Model',$scope.boundModel)
                if ($scope.boundModel) {
                    
                    var date = new Date($scope.boundModel);

                    var showYear = (date.format('Y') != today.format('Y'));


                    if (!$scope.useTime) {
                        if(showYear) {
                            $scope.readable = date.format('D j M Y');
                        } else {
                            $scope.readable = date.format('D j M');
                        }
                    } else {
                        if(showYear) {
                           $scope.readable = date.format('D j M Y - g:i ') + '<span class="meridian">' + date.format('a') + '</span>';
                        } else {
                           $scope.readable = date.format('D j M - g:i ') + '<span class="meridian">' + date.format('a') + '</span>';
                        }
                    }
                    //$scope.readable = date.format('D j F g:i') + '<span class="meridian">' + date.format('a') +'</span>';
                } else {
                    if ($scope.label && $scope.label.length) {
                        $scope.readable = $scope.label;
                    } else {
                        $scope.readable = 'None provided';
                    }
                }
            }

            /**
            var cancelWatch;

            function stopWatchingBoundModel() {
                if(cancelWatch) {
                   cancelWatch();
                }
            }

            function startWatchingBoundModel() {
                cancelWatch = $scope.$watch('boundModel', boundModelUpdated);
            }
            

            function boundModelUpdated() {
                stopWatchingBoundModel();

                //console.log('BOUND MODEL CHANGED', $scope.boundModel);
                $scope.settings.model = angular.copy($scope.boundModel);
                updateLabel();

                startWatchingBoundModel();
            }

            //Start watching to start with
            startWatchingBoundModel();
            /**/


            $scope.$watch('boundModel', boundModelChanged, true);
            $scope.$watch('settings.dateModel', dateModelChanged, true);

            function boundModelChanged() {
                if($scope.settings.dateModel != $scope.boundModel) {
                    if($scope.boundModel) {
                        //console.log('CREATE 1')
                        $scope.settings.dateModel = $scope.boundModel = new Date($scope.boundModel)
                    } else {
                        $scope.settings.dateModel = null;
                    }

                }
                updateLabel();
            }

            function dateModelChanged() {
                if($scope.settings.dateModel && $scope.dateIsSet) {
                    if($scope.boundModel != $scope.settings.dateModel) {


                    $scope.boundModel = $scope.settings.dateModel = new Date($scope.settings.dateModel)
                    }
                    updateLabel();
                }
            }

            /**
            //Watch for changes
            $scope.$watch('settings.dateModel', function() {
                // //console.log('MODEL CHANGE', data);
                // //Link to the bound model
                if ($scope.settings.open) {
                    $scope.boundModel = angular.copy($scope.settings.dateModel);
                }

                updateLabel();

            }, true)
            /**/
        }

    };

});