app.directive('batchAssign', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            selection: '=ngModel',
            // type:'=ngType',
            // definition:'=ngDefinition',
        },
        templateUrl: 'admin-batch-assign/admin-batch-assign-select.html',
        controller: 'BatchAssignController',
    };
});

app.controller('BatchAssignController', function($scope, $state, Notifications, Batch, ModalService) {


    $scope.open = function() {


            var modelSource = {};
            var params = {};

            ////////////////////////////////////////////////////

            ModalService.browse('contact', modelSource, params).result.then(closed, closed);

            ////////////////////////////////////////////////////

            function done(err, data) {

                if (err) {
                    //console.log('DONE ERR', err)
                    return Notifications.error(err);
                }

                ///////////////////////

                //Reload the state
                $state.reload();

                ///////////////////////

                //Deselect all
                $scope.selection.deselectAll();
                Notifications.status('Reassigned ' + data.success.length + ' items');
            }

            ////////////////////////////////////////////////////

            function closed(result) {

                var cardIDs = $scope.selection.ids;
                var assignees = modelSource.items;

                //If we have people to assign to
                if(assignees && assignees.length) {


                        var details = {
                            ids: cardIDs,
                            contacts:assignees, 
                            // track: trackID, //$scope.selectedProcesses,
                        }

                        return Batch.assignToContacts(details, done);
                }
            }

        // var modalInstance = $uibModal.open({
        //     template: '<test-event-configure ng-model="test" close="closeModal" remove="removeTest"></test-event-configure>',
        //     size: 'md',
        //     controller:function($scope) {
        //         $scope.test = testEvent;
        //         $scope.isModal =true;
        //         $scope.closeModal = function() {
        //             console.log('close modal')
        //             $scope.$close();
        //         }

        //         $scope.removeTest = function() {
        //             $scope.$close();
        //             _.pull(tests, testEvent);
        //         }
        //     }
        // });
    }

    /**

    //////////////////////////////////////////////////////////

    //Get all the options
    FluroContent.resource('eventtrack').query({
        simple: true,
    }, function(res) {
        $scope.options = res;
    });

    //////////////////////////////////////////////////////////

    $scope.dynamicPopover = {
        templateUrl: 'admin-batch-assign/admin-batch-assign-popover.html',
    };

    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    

    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    $scope.setEventTrack = function(trackID) {

        var details = {
            ids: $scope.selection.ids,
            track: trackID, //$scope.selectedProcesses,
        }

        Batch.setEventTrack(details, done);
    }
    /**/

});