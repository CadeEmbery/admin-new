app.directive('multiPhoneField', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            placeholder:'@',
            //type: '=ngType',
            //title: '=ngTitle',
            //minimum: '=ngMinimum',
            //maximum: '=ngMaximum',
        },
        templateUrl: 'admin-multi-phone-field/multi-phone-field.html',
        controller: 'MultiPhoneFieldController',
    };
});


app.controller('MultiPhoneFieldController', function($scope) {

    $scope._proposed = {};

    ////////////////////////////////////////////////////

    //Create a model if it doesn't exist
    if (!$scope.model) {
        $scope.model = [];
    } 

    ////////////////////////////////////////////////////

    $scope.add = function() {

        var value = $scope._proposed.value;

        if (value && value.length) {
            if(!$scope.contains(value)) {
                if(!$scope.model) {
                    $scope.model = [];
                }
                $scope.model.push(value);
                $scope._proposed = {};
            }
           
        }
    }

    ////////////////////////////////////////////////////

    $scope.remove = function(entry) {
        _.pull($scope.model, entry);
    }

    ////////////////////////////////////////////////////

    $scope.contains = function(entry) {
        return _.includes($scope.model, entry);
    }

   


})