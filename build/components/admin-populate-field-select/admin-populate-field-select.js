app.directive('populateFieldSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            placeholder:'@',
            //type: '=ngType',
            //title: '=ngTitle',
            //minimum: '=ngMinimum',
            //maximum: '=ngMaximum',
        },
        templateUrl: 'admin-populate-field-select/admin-populate-field-select.html',
        controller: 'AdminPopulateFieldSelectController',
    };
});


app.controller('AdminPopulateFieldSelectController', function($scope) {

    
    ////////////////////////////////////////////////////

    //Create a model if it doesn't exist
    if (!$scope.model) {
        $scope.model = [];
    } 

    ////////////////////////////////////////////////////

    $scope.add = function(value) {

        console.log('Add')
        if(!$scope.model) {
            $scope.model = [];
        }

        if(value.path && value.path.length) {
            $scope.model.push(value);
            $scope._proposed = {}
        }
    }

    ////////////////////////////////////////////////////

    $scope.remove = function(entry) {
        _.pull($scope.model, entry);
    }
})