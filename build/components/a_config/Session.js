app.service('Session', function($rootScope, SearchService, TypeService, $state, FluroSocket, FluroAuthService, CacheManager) {

    var controller = {}


    //////////////////////////////////////////

    controller.refresh = function() {

        console.log('Refresh mang')
        SearchService.clear();
        CacheManager.clearAll();
SearchService.clear();;


        //Refresh User Session
        var req = FluroAuthService.getSession();

        req.then(function(res) {

            if (res && res.data) {
                $rootScope.user = res.data;
            } else {
                console.log('Session Authorisation error', res)
            }


            console.log('Refresh Defined Types!')
            TypeService.refreshDefinedTypes();
            $state.reload();

        }, function(res) {
            console.log('Session Authorisation error', res)
            $state.reload();
        });
    }
    return controller;
})