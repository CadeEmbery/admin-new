// //Help migrate lodash
// if( typeof _.contains === 'undefined' ) {
//     _.contains = _.includes;
//     _.prototype.contains = _.includes;
// }
// if( typeof _.object === 'undefined' ) {
//     _.object = _.zipObject;
// }
// _.uniq = _.uniqBy;


// var d = new Date();
// console.log(d.getTime());

// moment.langData('en')._relativeTime.s = "just now";/
// moment.localeData('en')._relativeTime.s = "just now";


///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

var APPLICATION_SHARE_NAME = 'Fluro';
var ANDROID_SHARE_LINK = 'http://flr.ms/4r';
var IOS_SHARE_LINK = 'http://flr.ms/4q';


///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////


var app = angular.module('fluro', [
    // 'ui.tinymce',

    'mentio',
    'ng-vkThread',
    'angular-json-editor',
    'ng-file-model',
    'ngResource',
    'ang-drag-drop',
    'ui.router',
    'ngTouch',
    'fluro.config',
    'fluro.util',
    'fluro.types',
    'fluro.asset',
    'fluro.ui',
    'fluro.access',
    'fluro.validate',
    'fluro.content',
    'ngStorage',
    'ui.bootstrap',
    'fluro.socket',
    'fluro.auth',
    'fluro.video',
    // 'fluro.interactions',
    'fluro.audio',
    'angular-loading-bar',
    'ui.sortable',
    'ui.ace',
    'ui.tree',
    'mwl.calendar',
    'ngMap',
    'angularFileUpload',
    'akoenig.deckgrid',
    'colorpicker.module',
    'chart.js',
    'angulartics',
    'angulartics.google.analytics',
    'formly',
    'formlyBootstrap',
    'ng-currency',
    'ngAnimate',
    'ngSanitize',
    'ngIntlTelInput',
    // 'jsonFormatter',

])


/////////////////////////////////////////////////////////////////////

function getMetaKey(stringKey) {
    var metas = document.getElementsByTagName('meta');

    for (i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute("property") == stringKey) {
            return metas[i].getAttribute("content");
        }
    }
    return "";
}

/////////////////////////////////////////////////////////////////////

//FIX
/////////////////////////////////////////////////////////////////////


//Bootstrap application
function initializeApplication() {
    console.log('Init')

    //////////////////////////////////
    var initInjector = angular.injector(["ng"]);
    var $http = initInjector.get("$http");

    //////////////////////////////////
    //Get the location of our API
    var apiURL = getMetaKey('fluro_url');
    var token = getMetaKey('fluro_application_key');


    //Fix for now
    // apiURL = apiURL.replace('http://api', 'http://v2.api');

    //////////////////////////////////

    var config = {}
    if (token) {
        config.headers = {
            'Authorization': 'Bearer ' + token
        }
    } else {
        config.withCredentials = true;
    }


    //Request
    var req = $http.get(apiURL + "/session", config);

    req.then(function(res) {

        if (res && res.data) {
            app.constant("$initUser", res.data);

            //Bootstrap the application
            angular.element(document).ready(function() {
                angular.bootstrap(document, ["fluro"]);
            });
        } else {
            //   console.log('Bootstrap error', res)
        }
    }, function(err) {
        console.log('Bootstrap error', err)
    });


}


var massiveQueryLimit = 20000; //30000;

//Start
initializeApplication();

/////////////////////////////////////////////////////////////////////







app.config(function($stateProvider, JSONEditorProvider, $animateProvider, $compileProvider, $controllerProvider, $httpProvider, TypeConfigProvider, FluroProvider, $urlRouterProvider, $locationProvider) {





    //Add ng-animate-disable feature
    $animateProvider.classNameFilter(/^(?:(?!ng-animate-disabled).)*$/);
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|webcal|caldav|chrome-extension):/);


    ///////////////////////////////////////////////////


    // 
    JSONEditorProvider.configure({
        plugins: {
            ace: {
                theme: 'tomorrow_night_eighties'
            }
        },
        defaults: {
            options: {
                iconlib: 'fontawesome4',
                theme: 'bootstrap3',
                // ajax: true
            }
        }
    });

    ///////////////////////////////////////////////////

    //app.fluroController = $controllerProvider.register;
    //app.fluroDirective = $compileProvider.directive;

    ///////////////////////////////////////////////////

    ///////////////////////////////////////////////////

    //Get the access token and the API URL
    var accessToken = getMetaKey('fluro_application_key');
    var apiURL = getMetaKey('fluro_url');


    //Create the initial config setup
    var initialConfig = {
        apiURL: apiURL,
        token: accessToken,
        sessionStorage: true, //Change this if you want to use local storage  or session storage
        backupToken: accessToken,
    }

    ///////////////////////////////////////////

    //Check if we are developing an app locally
    var appDevelopmentURL = getMetaKey('app_dev_url');

    //If an app development url is set then we know where to login etc
    if (appDevelopmentURL && appDevelopmentURL.length) {
        initialConfig.appDevelopmentURL = appDevelopmentURL;
    } else {
        //Set HTML5 mode to true when we deploy
        //otherwise live reloading will break in local dev environment
        $locationProvider.html5Mode(true);
    }

    ///////////////////////////////////////////

    //Set the initial config
    FluroProvider.set(initialConfig);

    ///////////////////////////////////////////

    //Http Intercpetor to check auth failures for xhr requests
    if (!accessToken) {
        $httpProvider.defaults.withCredentials = true;
    }

    $httpProvider.interceptors.push('FluroAuthentication');
    $httpProvider.interceptors.push('FluroSignedOut');

    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////

    $urlRouterProvider.otherwise("/");




    ///////////////////////////////////////////

    var types = TypeConfigProvider.$get().types;


    //Create routes for each content type
    _.each(types, function(object) {
        addRoute(object);
    });

    function addRoute(type) {

        //List page
        $stateProvider.state(type.path, {
            title: type.plural,
            url: '/' + type.path + '?searchInheritable&searchArchived&search&startDate',
            redirectTo: type.path + '.default',
            template: '<div ui-view class="application-panel"></div>',
        });







        ////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////



        ////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////


        /**/
        //List page
        $stateProvider.state(type.path + '.default', {
            title: type.plural,
            url: '',
            template: '<div class="application-panel" ng-include="template"></div>',
            controller: 'ContentListController',
            resolve: {
                type: function() {
                    return type;
                },
                definitions: function(FluroContent) {

                    return FluroContent.endpoint('defined/types/' + type.path, true, true).query().$promise;
                },
                access: function(FluroAccess, definitions) {

                    // console.log('GET DEFINITIONS', definitions)
                    if (definitions.length) {
                        return true;
                    }

                    var canAccess = FluroAccess.canAccess(type.path);
                    return FluroAccess.resolveIf(canAccess);
                },
                actions: function() {
                    // console.log('ACTIONS', type.actions)
                    return type.actions;
                },

                //Do a count first to check if we need to use mass indexing
                items: function(FluroContent, $stateParams, $http, Fluro, $q, ContentFieldService) {


                    switch (type.path) {
                        case 'integration':
                            return FluroContent.resource(type.path).query({
                                sort: '-updated'
                            }).$promise
                        break;
                        case 'process':
                        case 'contactdetail':
                            return [];
                            break;
                    }




                    ////////////////////////////////////////////////////////

                    var queryConfig = {
                        sort: '-updated',
                        appendPostCount: 'all',
                    }


                    ////////////////////////////////////////////////////////

                    //Map what fields we need to retrieve
                    ContentFieldService.getFields(queryConfig, type, null, $stateParams);

                    /////////////////////////////////////////////////

                    var neverCache = false;
                    if (type.path == 'mailout') {
                        neverCache = true;
                        // console.log('DONE CACHE NOW');
                    }



                    /////////////////////////////////////////////////

                    var deferred = $q.defer();

                    /////////////////////////////////////////////////


                    var countConfig = _.clone(queryConfig);
                    countConfig.count = true;

                    //////////////////////////////////////////////////

                    // var serverResource = FluroContent.resource(type.path, false, neverCache);
                    var serverResource = FluroContent.resource(type.path, false, neverCache);


                    //Run a count to see how many results will be returned
                    serverResource.get(countConfig)
                        .$promise
                        .then(function(res) {


                            // console.log('TOTAL', res.total);
                            //If more than 1000 then we want to get it as a meta indexed feed
                            if (res.total > massiveQueryLimit) {
                                // // console.log('LOAD META DATA NOW');
                                // queryConfig.meta = true;
                                // queryConfig.limit = massiveQueryLimit;
                                // // queryConfig.offset = massiveQueryLimit;

                                // var date = moment().subtract(6, 'months').toDate();
                                // queryConfig.createdAfter = date;

                                queryConfig.limit = 5000; //massiveQueryLimit;


                                // console.log('RUN QUERY');

                                //Load the indexed feed
                                return serverResource.query(queryConfig)
                                    .$promise
                                    .then(function(firstItems) {

                                        var indexedData = {
                                            massive: true,
                                            items: firstItems,
                                            total: res.total,
                                        }
                                        // indexedData.total = res.total;
                                        deferred.resolve(indexedData);

                                    }, deferred.reject);

                            }

                            //////////////////////////////////////////

                            switch (type.path) {
                                case 'account':
                                case 'event':
                                    // case 'transaction':
                                    break;
                                default:
                                    //USE FAST QUERY ENDPOINT
                                    console.log('DO FAST QUERY INSTEAD', queryConfig)
                                    return $http.post(Fluro.apiURL + '/content/' + type.path + '/filter', {
                                            select: queryConfig.fields,
                                            searchInheritable: queryConfig.searchInheritable,
                                            includeArchived: !queryConfig.excludeArchived,
                                            allDefinitions: queryConfig.allDefinitions,
                                            // populateAll:true,
                                            legacy: true,
                                        })
                                        .then(function(res) {
                                            console.log('FAST QUERY')
                                            deferred.resolve(_.sortBy(res.data, 'created').reverse());
                                        }, function(err) {
                                            deferred.reject(err);
                                        });
                                    break;
                            }


                            // var serverResource = FluroContent.resource(type.path, false, neverCache);

                            // '/content/filter'
                            // type.path
                            //Query and load all the items
                            return serverResource.query(queryConfig)
                                .$promise
                                .then(function(res) {

                                    // console.log('GOT SOMETHING', res)
                                    deferred.resolve(res);
                                }, function(err) {
                                    // console.log('GOT ERROR', err)
                                    deferred.reject(err);

                                });
                            // deferred.resolve, deferred.reject);


                        }, function(err) {
                            deferred.reject(err);
                        });





                    return deferred.promise;


                },
                definition: function() {
                    return;
                },
                extras: function(FluroContent, $q) {

                    //Create a deferred promise
                    var deferred = $q.defer();

                    //Data Object
                    var data = {};

                    switch (type.path) {
                        default:
                            // console.log('RESOLVED EXTRAS')
                            deferred.resolve(data);
                            break;
                    }



                    return deferred.promise;
                }
            }
        });

        /**/

        //Trash List page
        $stateProvider.state(type.path + '.trash', {
            title: type.plural,
            url: '/trash',
            template: '<div class="application-panel" ng-include="template"></div>',
            controller: 'ContentTrashController',
            resolve: {
                type: function() {
                    return type;
                },
                access: function(FluroAccess) {

                    var canRestore = FluroAccess.can('restore any', type.path);
                    var canDestroy = FluroAccess.can('destroy any', type.path);
                    var canRestoreOwn = FluroAccess.can('restore own', type.path);
                    var canDestroyOwn = FluroAccess.can('destroy own', type.path);
                    return FluroAccess.resolveIf((canRestore || canDestroy || canRestoreOwn || canDestroyOwn));
                },
                actions: function() {
                    return null;
                },
                items: function(FluroContent, FluroAccess) {


                    return FluroContent.resource(type.path).query({
                        status: 'deleted',
                        cache: false,
                        list: true,
                        fields: ['updatedBy', 'updated', 'title']
                    }).$promise
                },
                definitions: function(items, $resource, Fluro) {
                    return;
                },
                definition: function() {
                    return;
                }
            }
        });




        //////////////////////////////////////////////////////////

        //Create a list page
        $stateProvider.state(type.path + '.create', {
            title: 'Create ' + type.singular,
            url: '/create/:definitionName?initDate',
            // params: {
            //     definitionName: null
            // },
            templateUrl: 'fluro-admin-content/types/form.html',
            controller: 'ContentFormController',
            resolve: {
                //Get the definition
                definition: function($stateParams, FluroContent) {

                    if ($stateParams.definitionName) {
                        return FluroContent.endpoint('defined/' + $stateParams.definitionName).get().$promise;
                    } else {
                        return null;
                    }
                },
                type: function() {
                    return type;
                },
                access: function(FluroAccess, definition) {
                    var canCreate;

                    // console.log('USING FLURO ACCESS')
                    if (definition) {
                        canCreate = FluroAccess.can('create', definition.definitionName, definition.parentType);
                    } else {
                        canCreate = FluroAccess.can('create', type.path);
                    }

                    return FluroAccess.resolveIf(canCreate);
                },
                item: function(definition, $q) {

                    // console.log('ITEM')
                    var deferred = $q.defer();

                    if (definition) {
                        //If the definition exists
                        if (definition.definitionName) {
                            //Send back a new item with the definition set
                            return {
                                definition: definition.definitionName,
                            };
                        } else {
                            //Just don't resolve
                            deferred.reject();
                        }
                        return deferred.promise;
                    } else {
                        return {};
                    }
                },
                extras: function(item, $q) {


                    console.log('EXTRAS?', item)

                    var deferredExtras = $q.defer();

                    //Data Object
                    var data = {};

                    //////////////////////////////////////

                    // switch (item._type) {
                    //     case 'process':

                    //     console.log('RETRIEVE THE PROCESS REFERENCE')
                    //         if (item.item) {
                    //             var referenceID = item.item;
                    //             if (referenceID._id) {
                    //                 referenceID = referenceID._id;
                    //             }
                    //         }

                    //         var deferred = $q.defer();

                    //         FluroContent.resource('get').get({
                    //                 id: referenceID,
                    //                 appendDefinition: true,
                    //             })
                    //             .$promise
                    //             .then(function(referencedItem) {
                    //                 data.reference = referencedItem;
                    //                 deferredExtras.resolve(data);
                    //             }, function(err) {
                    //                 //Just use the basics we do know
                    //                 deferredExtras.resolve(data);
                    //             });
                    //         break;
                    //     default:
                    deferredExtras.resolve(data);
                    // break;
                    // }

                    //////////////////////////////////////

                    return deferredExtras.promise;
                }
            }
        });

        //////////////////////////////////////////////////////////

        //Create a list page
        $stateProvider.state(type.path + '.edit', {
            title: 'Edit ' + type.singular,
            url: '/:definitionName/:id/edit',
            // params: {
            //     definitionName: null,
            // },
            templateUrl: 'fluro-admin-content/types/form.html',
            controller: 'ContentFormController',
            resolve: {
                type: function() {
                    return type;
                },
                item: function($stateParams, FluroContent) {

                    var definedName = type.path;
                    if ($stateParams.definitionName) {
                        definedName = $stateParams.definitionName;
                    }

                    var queryConfig = {
                        id: $stateParams.id,
                        context: 'edit',
                        appendPostCount: 'all',
                    }

                    // //Append Teams if its a contact
                    // if(type.path == 'contact') {
                    //     queryConfig.appendTeams = 'all';
                    // }
                    if (type.path == 'event' || type.path == 'roster') {

                        queryConfig.appendAssignments = true;
                        queryConfig.appendUnavailability = true;
                    }


                    // return FluroContent.resource(definedName).get(queryConfig).$promise;
                    if(type.path == 'process') {
                        return FluroContent.resource('get').get(queryConfig).$promise;
                    } 

                    //Not confident that we can do /get for everything just yet
                    //so only do it for process cards atm
                    return FluroContent.resource(definedName).get(queryConfig).$promise;
                },
                definition: function(item, FluroContent) {
                    if (item.definition) {
                        return FluroContent.endpoint('defined/' + item.definition).get().$promise;
                    }
                    return null;
                },
                access: function(FluroAccess, $rootScope, $stateParams, item, definition) {
                    var canEdit = FluroAccess.canEditItem(item, (type.path == 'user'));
                    return FluroAccess.resolveIf(canEdit);
                },
                extras: function(FluroContent, $q, item) {

                    var deferredExtras = $q.defer();

                    //Data Object
                    var data = {};


                    switch (type.path) {
                        case 'contact':

                            /////////////////////////////////////////////////

                            function loadContactDetails() {
                                var deferred = $q.defer();

                                FluroContent.endpoint('contact/details/' + item._id, true, true).query()
                                    .$promise
                                    .then(deferred.resolve, function() {
                                        deferred.resolve([]);
                                    });

                                return deferred.promise;
                            }

                            /////////////////////////////////////////////////

                            function loadContactDetailDefinitions() {
                                var deferred = $q.defer();

                                FluroContent.endpoint('defined/types/contactdetail', true, true).query()
                                    .$promise
                                    .then(deferred.resolve
                                        //     function(sheets) {
                                        //     console.log('SHEETS', sheets)
                                        //     sheets = _.filter(sheets, function(sheet) {

                                        //         console.log('Sheeet', sheet);
                                        //         return sheet.status != 'archived';
                                        //     })

                                        //     deferred.resolve(sheets);
                                        // }

                                        ,
                                        function() {
                                            deferred.resolve([]);
                                        });

                                return deferred.promise;
                            }

                            /////////////////////////////////////////////////

                            function loadPersona() {
                                var deferred = $q.defer();

                                FluroContent.endpoint('contact/' + item._id + '/personas', true, true).query()
                                    .$promise.
                                then(deferred.resolve, function() {
                                    deferred.resolve([]);
                                });

                                return deferred.promise;
                            }

                            /////////////////////////////////////////////////

                            function loadUnsubscribes() {
                                var deferred = $q.defer();

                                FluroContent.endpoint('contact/' + item._id + '/unsubscribes', true, true).query()
                                    .$promise.
                                then(deferred.resolve, function() {
                                    deferred.resolve([]);
                                });

                                return deferred.promise;
                            }

                            /////////////////////////////////////////////////

                            function loadSMSUnsubscribes() {
                                var deferred = $q.defer();

                                FluroContent.endpoint('contact/' + item._id + '/smsunsubscribes', true, true).query()
                                    .$promise.
                                then(deferred.resolve, function() {
                                    deferred.resolve([]);
                                });

                                return deferred.promise;
                            }



                            /////////////////////////////////////////////////

                            function loadContactDevices() {
                                var deferred = $q.defer();

                                FluroContent.endpoint('contact/' + item._id + '/devices', true, true).query()
                                    .$promise.
                                then(deferred.resolve, function() {
                                    deferred.resolve([]);
                                });

                                return deferred.promise;
                            }

                            /////////////////////////////////////////////////

                            function loadContactNotifications() {
                                var deferred = $q.defer();

                                FluroContent.endpoint('contact/' + item._id + '/notifications', true, true).query()
                                    .$promise.
                                then(deferred.resolve, function() {
                                    deferred.resolve([]);
                                });

                                return deferred.promise;
                            }

                            /////////////////////////////////////////////////

                            function loadRelationships() {


                                var deferred = $q.defer();

                                FluroContent.endpoint('contact/' + item._id + '/relationships', true, true).query({}).$promise.then(deferred.resolve, function() {
                                    deferred.resolve([]);
                                });

                                return deferred.promise;
                            }


                            /////////////////////////////////////////////////

                            $q.all([
                                loadContactDetails(),

                                loadPersona(),
                                loadUnsubscribes(),

                                loadContactDetailDefinitions(),

                                loadContactDevices(),
                                loadContactNotifications(),
                                loadSMSUnsubscribes(),
                                // loadRelationships(),

                                // FluroContent.endpoint('contact/details/' + item._id, true, true).query().$promise,
                                // FluroContent.endpoint('contact/relationships/' + item._id, true, true).query().$promise,
                            ]).then(function(values) {
                                if (values[0]) {
                                    data.contactDetails = values[0];
                                }

                                if (values[1]) {
                                    data.personas = values[1];
                                }

                                if (values[2]) {
                                    data.unsubscribes = values[2];
                                }

                                if (values[3]) {
                                    data.detailDefinitions = values[3];
                                }

                                if (values[4]) {
                                    data.devices = values[4];
                                }

                                if (values[5]) {
                                    data.notifications = values[5];
                                }
                                if (values[6]) {
                                    data.smsunsubscribes = values[6];
                                }

                                // if (values[6]) {
                                //     data.relationships = values[6];
                                // }

                                //Resolve the promise
                                deferredExtras.resolve(data);

                            }, deferredExtras.reject);

                            break;



                        case 'process':

                            // console.log('RETRIEVE THE PROCESS REFERENCE')
                            if (item.item) {
                                var referenceID = item.item;
                                if (referenceID._id) {
                                    referenceID = referenceID._id;
                                }

                                FluroContent.resource('get').get({
                                        id: referenceID,
                                        appendDefinition: true,
                                    })
                                    .$promise
                                    .then(function(referencedItem) {
                                        data.reference = referencedItem;
                                        deferredExtras.resolve(data);
                                    }, function(err) {
                                        //Just use the basics we do know
                                        deferredExtras.resolve(data);
                                    });
                            } else {

                                deferredExtras.resolve(data);
                            }





                            break;
                        default:
                            deferredExtras.resolve(data);
                            break;
                    }

                    return deferredExtras.promise;
                }
            }
        });




        //////////////////////////////////////////////////////////

        //Create a list page
        $stateProvider.state(type.path + '.view', {
            title: 'View ' + type.singular,
            url: '/:definitionName/:id/view?backOnClose',
            // params: {
            //     definitionName: null,
            // },
            templateUrl: 'fluro-admin-content/types/view.html',
            controller: 'ContentViewController',
            resolve: {
                type: function() {
                    return type;
                },
                item: function($stateParams, FluroContent) {

                    // console.log('Go get it son!');
                    var definedName = type.path;
                    if ($stateParams.definitionName) {
                        definedName = $stateParams.definitionName;
                    }


                    var queryConfig = {
                        id: $stateParams.id,
                        appendPostCount: 'all'
                    }

                    //Append Teams if its a contact
                    if (type.path == 'contact') {
                        queryConfig.appendTeams = 'all';
                    }

                    if (type.path == 'event' || type.path == 'roster') {
                        queryConfig.appendAssignments = true;
                    }



                    return FluroContent.resource(definedName).get(queryConfig).$promise;
                },
                definition: function(item, $resource, Fluro) {
                    if (item.definition) {


                        var promise = $resource(Fluro.apiURL + '/defined/' + item.definition).get().$promise;



                        return promise;
                    }
                    return null;
                },
                access: function(FluroAccess, $rootScope, $stateParams, item, definition) {

                    var itemAccountID = item.account;
                    var userAccountID = $rootScope.user.account;

                    if (itemAccountID._id) {
                        itemAccountID = itemAccountID._id;
                    }

                    if (userAccountID._id) {
                        userAccountID = userAccountID._id;
                    }

                    if (itemAccountID && (userAccountID != itemAccountID)) {
                        return true;
                    }


                    //Check if the user can view the item
                    var canView = FluroAccess.canViewItem(item, (type.path == 'user'));

                    return FluroAccess.resolveIf(canView);
                },
                extras: function(FluroContent, item, $q) {

                    var deferred = $q.defer();

                    //Data Object
                    var data = {};

                    switch (type.path) {

                        case 'process':

                            // console.log('RETRIEVE THE PROCESS REFERENCE')
                            if (item.item) {
                                var referenceID = item.item;
                                if (referenceID._id) {
                                    referenceID = referenceID._id;
                                }

                                FluroContent.resource('get').get({
                                        id: referenceID,
                                        appendDefinition: true,
                                    })
                                    .$promise
                                    .then(function(referencedItem) {
                                        data.reference = referencedItem;
                                        deferred.resolve(data);
                                    }, function(err) {
                                        //Just use the basics we do know
                                        deferred.resolve(data);
                                    });


                            } else {
                                deferred.resolve(data);
                            }



                            break;


                            // case 'process':
                            //    return FluroContent.endpoint('process/' + item._id + '/progress', null, true).query().$promise;
                            // break;
                        case 'query':

                            // //Run the query and return it
                            // FluroContent.endpoint('content/_query/' + item._id, true, true)
                            //     .query()
                            //     .$promise
                            //     .then(function(res) {
                            //         data.results = res;
                            //         deferred.resolve(data);
                            //     }, deferred.reject);

                            deferred.resolve(data);

                            break;
                        case 'resultset':

                            //Run the query and return it
                            FluroContent.endpoint('results/' + item._id, true, true)
                                .query()
                                .$promise
                                .then(function(res) {
                                    data.results = res;
                                    deferred.resolve(data);
                                }, deferred.reject);

                            break;
                        case 'mailout':

                            if (item.state != 'sent') {


                                //Do a preflight check on the mailout
                                FluroContent.endpoint('mailout/' + item._id + '/preflight', false, true)
                                    .query()
                                    .$promise
                                    .then(function(res) {
                                        data.preflightContacts = res;
                                        deferred.resolve(data);
                                    }, function(err) {
                                        if (err.severity == 'warning') {
                                            return deferred.resolve({});
                                        }
                                        deferred.reject(err);
                                    });
                            } else {
                                deferred.resolve(data);
                            }

                            break;
                        case 'contact':

                            /////////////////////////////////////////////////

                            function loadProcessStates() {
                                var deferred = $q.defer();

                                FluroContent.endpoint('process/states/' + item._id).query().$promise.then(deferred.resolve, function() {
                                    deferred.resolve([]);
                                });

                                return deferred.promise;
                            }

                            /////////////////////////////////////////////////

                            function loadContactDetails() {
                                var deferred = $q.defer();

                                FluroContent.endpoint('contact/details/' + item._id, true, true).query().$promise.then(deferred.resolve, function() {
                                    deferred.resolve([]);
                                });

                                return deferred.promise;
                            }

                            /////////////////////////////////////////////////

                            function loadRelationships() {


                                var deferred = $q.defer();

                                FluroContent.endpoint('contact/' + item._id + '/relationships', true, true).query({}).$promise.then(deferred.resolve, function() {
                                    deferred.resolve([]);
                                });

                                return deferred.promise;
                            }


                            /////////////////////////////////////////////////

                            function loadPersona() {
                                var deferred = $q.defer();

                                FluroContent.endpoint('contact/' + item._id + '/persona', true, true).get().$promise.then(deferred.resolve, function() {
                                    deferred.resolve([]);
                                });

                                return deferred.promise;
                            }

                            /////////////////////////////////////////////////

                            $q.all([
                                    loadProcessStates(),
                                    loadContactDetails(),
                                    loadRelationships(),
                                    loadPersona()
                                    // FluroContent.endpoint('contact/details/' + item._id, true, true).query().$promise,
                                    // FluroContent.endpoint('contact/relationships/' + item._id, true, true).query().$promise,
                                ])
                                .then(function(values) {
                                    data.processStates = values[0];
                                    data.contactDetails = values[1];
                                    data.relationships = values[2];
                                    data.persona = values[4];


                                    //Resolve the promise
                                    deferred.resolve(data);

                                }, deferred.reject);

                            break;

                        case 'plan':

                            if (item.event) {

                                var eventID = item.event;
                                if (eventID._id) {
                                    eventID = eventID._id;
                                }




                                ////////////////////////////////////////////////

                                //Get all assignments for an event
                                var assignmentPromise = FluroContent.endpoint('event/' + eventID + '/assignments', false, true)
                                    .query()
                                    .$promise;

                                assignmentPromise.then(function(res) {
                                    data.assignments = res;
                                });

                                ////////////////////////////////////////////////

                                //Get all assignments for an event
                                var rosterPromise = FluroContent.endpoint('event/' + eventID + '/rosters', false, true)
                                    .query()
                                    .$promise;

                                rosterPromise.then(function(res) {
                                    data.rosters = res;
                                });

                                ////////////////////////////////////////////////

                                $q.all([
                                        assignmentPromise,
                                        rosterPromise,
                                    ])
                                    .then(function() {
                                        deferred.resolve(data);
                                    }, deferred.reject);

                            } else {
                                deferred.resolve(data);
                            }


                            // if (item.event) {

                            //     var eventID = item.event;
                            //     if (eventID._id) {
                            //         eventID = eventID._id;
                            //     }

                            //     //Do a preflight check on the mailout
                            //     FluroContent.endpoint('event/' + eventID + '/assignments', false, true)
                            //         .query()
                            //         .$promise
                            //         .then(function(res) {
                            //             data.assignments = res;
                            //             deferred.resolve(data);
                            //         }, deferred.reject);

                            // } else {
                            //     deferred.resolve(data);
                            // }

                            break;
                        case 'event':


                            var id;
                            if (item.event) {
                                if (item.event._id) {
                                    id = item.event._id;
                                } else {
                                    id = item.event;
                                }
                            } else {
                                id = item._id;
                            }

                            if (id) {


                                $q.all([
                                    function() {
                                        var deferred = $q.defer();
                                        FluroContent.endpoint('confirmations/event/' + id).query().$promise.then(deferred.resolve, function(err) {
                                            deferred.resolve([])
                                        })
                                        return deferred.promise;
                                    }(),

                                    function() {
                                        var deferred = $q.defer();
                                        FluroContent.endpoint('event/' + id + '/attendance').get().$promise.then(deferred.resolve, function(err) {
                                            deferred.resolve([])
                                        })
                                        return deferred.promise;
                                    }(),

                                    function() {
                                        var deferred = $q.defer();
                                        FluroContent.endpoint('checkin/event/' + id).query({
                                            all: true
                                        }).$promise.then(deferred.resolve, function(err) {
                                            deferred.resolve([])
                                        })
                                        return deferred.promise;
                                    }(),
                                    function() {

                                        var deferred = $q.defer();
                                        // deferred.resolve({})
                                        // return deferred.promise;
                                        FluroContent.endpoint('event/' + id + '/guestlist').query({

                                        }).$promise.then(deferred.resolve, function(err) {
                                            deferred.resolve([])
                                        })
                                        return deferred.promise;
                                    }(),
                                ]).then(function(values) {
                                    /**
                                    //Wait til all the promises have been fulfilled
                                    $q.all([
                                        FluroContent.endpoint('confirmations/event/' + id).query().$promise,
                                        FluroContent.endpoint('attendance/' + id).get().$promise,
                                        FluroContent.endpoint('checkin/event/' + id).query({
                                            all: true
                                        }).$promise,
                                    ])
                                    /**/


                                    //Append the data
                                    data.confirmations = values[0];
                                    data.attendance = values[1];
                                    data.checkins = values[2];
                                    data.guests = values[3];

                                    //Resolve the promise
                                    deferred.resolve(data);
                                }, deferred.reject);

                                /**
                                FluroContent.endpoint('confirmations/event/' + id).query().$promise.then(function(res) {
                                    data.confirmations = res;
                                    deferred.resolve(data);
                                }, deferred.reject);
                                /**/
                            } else {
                                deferred.resolve(data);
                            }
                            break;
                        default:
                            deferred.resolve(data);
                            break;
                    }

                    return deferred.promise;
                }
            }
        });

        //////////////////////////////////////////////////////////

        //Create a list page
        $stateProvider.state(type.path + '.delete', {
            title: 'Delete ' + type.plural,
            url: '/:definitionName/:id/delete',
            // params: {
            //     definitionName: null,
            // },
            templateUrl: 'fluro-admin-content/types/delete.html',
            controller: 'ContentDeleteController',
            resolve: {
                type: function() {
                    return type;
                },
                item: function($stateParams, FluroContent) {
                    var definedName = type.path;
                    if ($stateParams.definitionName) {
                        definedName = $stateParams.definitionName;
                    }

                    return FluroContent.resource(definedName).get({
                        id: $stateParams.id
                    }).$promise;
                },
                definition: function(item, $resource, Fluro) {
                    if (item.definition) {
                        return $resource(Fluro.apiURL + '/defined/' + item.definition).get().$promise;
                    }
                    return null;
                },
                deleteCheck: function($resource, Fluro, item) {
                    if (type.path == 'realm') {
                        return $resource(Fluro.apiURL + '/realm/check/' + item._id).get().$promise;
                    }
                    return null;
                },
                access: function(FluroAccess, $rootScope, $stateParams, item, definition) {
                    var canDelete = FluroAccess.canDeleteItem(item, (type.path == 'user'));
                    return FluroAccess.resolveIf(canDelete);
                },
            }
        });


        //////////////////////////////////////////////////////////

        //List page
        $stateProvider.state(type.path + '.custom', {
            title: type.plural,
            url: '/:definition?search',
            // params: {
            //     definition: null,
            // },
            template: '<div class="application-panel" ng-include="template"></div>',
            controller: 'ContentListController',
            resolve: {
                definition: function($stateParams, $q, Fluro, $resource) {
                    if ($stateParams.definition) {

                        //   console.log('tEst definition')
                        return $resource(Fluro.apiURL + '/defined/' + $stateParams.definition).get().$promise;
                    } else {
                        //   console.log('Failed definition')
                        return $q.defer().reject();
                    }
                },
                type: function() {


                    return type;
                },
                access: function(FluroAccess, definition) {

                    var canAccess = FluroAccess.canAccess(definition.definitionName, definition.parentType);
                    // console.log('CAN ACCESS', canAccess);

                    var request = FluroAccess.resolveIf(canAccess);
                    return request;
                },

                actions: function() {
                    return type.actions;
                },
                items: function(definition, FluroContent, $stateParams, $http, Fluro, $q, ContentFieldService) {

                    //   console.log('ITEMS')
                    if ( /*type.path == 'event' || */ type.path == 'integration') {
                        return FluroContent.resource(definition.definitionName).query().$promise
                    }




                    //////////////////////////////////////////////////////

                    var queryConfig = {
                        sort: '-updated',
                        appendPostCount: 'all',
                    }


                    var sortOverride = _.get(definition, 'data.sort');
                    if (sortOverride) {
                        queryConfig.sort = sortOverride;
                    }

                    // console.log('OVERRIDE SORT', queryConfig.sort);

                    ////////////////////////////////////////////////////////

                    //Map what fields we need to retrieve
                    ContentFieldService.getFields(queryConfig, type, definition, $stateParams);

                    ////////////////////////////////////////////////////////


                    /**
                    var fields = [];

                    //////////////////////////////////////////////////////

                    //If the definition has it's own defined columns
                    if (definition.columns && definition.columns.length) {
                        var customFields = _.map(definition.columns, function(col) {
                            return col.key;
                        })
                        fields = fields.concat(customFields);
                    }

                    //   console.log('FILTER 1')

                    //Include filterable fields
                    var filterFields = _.map(type.filters, function(col) {
                        return col.key;
                    })
                    fields = fields.concat(filterFields);

                    //If the definition doesnt disable the basic columns
                    if (!definition.disableColumns) {
                        var columnFields = _.map(type.columns, function(col) {
                            return col.key;
                        })

                        //Add the updated by columns
                        fields = fields.concat(columnFields);
                        //fields.push('updatedBy');
                        //fields.push('updated');

                    }

                    //Still include basic fields
                    fields = fields.concat(basicFields);
                    fields = _.uniq(fields);

                    switch (type.path) {
                        case 'event':
                            fields.push('forms')
                            fields.push('plans')
                            fields.push('assignments')
                            fields.push('volunteers')
                            fields.push('locations')
                            fields.push('rooms')
                            fields.push('track')
                            queryConfig.appendAssignments = true;
                            break;
                        case 'checkin':
                            fields.push('event');
                            break;
                        case 'post':
                            fields.push('parent');
                            fields.push('managedAuthor');
                            break;
                        case 'realm':
                            fields.push('trail');
                            fields.push('bgColor');
                            fields.push('color');
                            break;

                        case 'contact':
                            _.pull(fields, 'updatedBy');
                            fields.push('emails');
                            fields.push('phoneNumbers');
                            break;
                        case 'location':
                            fields.push('latitude')
                            fields.push('longitude')
                            break;
                        case 'location':
                            fields.push('latitude')
                            fields.push('longitude')
                            break;
                        case 'plan':
                            fields.push('event')
                                // queryConfig.appendAssignments = true;
                            break;
                        case 'image':
                            fields.push('width')
                            fields.push('height')
                            break;
                        case 'persona':
                            fields.push('username')
                            fields.push('user')
                            fields.push('collectionEmail')
                            break;
                        case 'audio':
                            fields.push('assetType')
                            break;
                        case 'video':
                            fields.push('assetType');
                            fields.push('width');
                            fields.push('height');
                            break;
                        case 'interaction':
                            fields.push('data._firstName')
                            fields.push('data._lastName')
                            break;
                        case 'process':
                            fields.push('state')
                            fields.push('item')
                            fields.push('assignedTo')
                            fields.push('dueDate')
                            fields.push('taskCount')
                            fields.push('processStatus')
                                // fields.push('optionalTasks')
                            break;
                        case 'mailout':
                            fields.push('subject')
                            break;
                    }

                    //Add these to all types
                    fields.push('author');
                    fields.push('owners');
                    fields.push('managedAuthor');
                    fields.push('managedOwners');
                    fields.push('hashtags');
                    fields.push('keywords');

                    //Attach the fields to the query
                    queryConfig.fields = fields;

                    //////////////////////////////

                    // var queryConfig = {
                    //     fields: fields,
                    //     sort: '-updated',
                    //     appendPostCount:'all',

                    //     //list: true,
                    // }

                    //// console.log('$STATEPARAMS', $stateParams);

                    /**/




                    if ($stateParams.searchInheritable) {
                        queryConfig.searchInheritable = true;
                    }


                    var neverCache = false;
                    if (type.path == 'mailout') {
                        neverCache = true;
                        // console.log('No cache');
                    }


                    /////////////////////////////////////////////////

                    var deferred = $q.defer();

                    /////////////////////////////////////////////////

                    var countConfig = _.clone(queryConfig);
                    countConfig.count = true;


                    //////////////////////////////////////////////////

                    var serverResource = FluroContent.resource(definition.definitionName, false, neverCache);

                    //Run a count to see how many results will be returned
                    serverResource.get(countConfig)
                        .$promise
                        .then(function(res) {



                            // console.log('TOTAL', res.total);
                            //If more than 1000 then we want to get it as a meta indexed feed
                            if (res.total > massiveQueryLimit) {
                                // // console.log('LOAD META DATA NOW');
                                // queryConfig.meta = true;
                                // queryConfig.limit = massiveQueryLimit;
                                // // queryConfig.offset = massiveQueryLimit;

                                // var date = moment().subtract(6, 'months').toDate();
                                // queryConfig.createdAfter = date;

                                queryConfig.limit = 5000; //massiveQueryLimit;

                                //Load the indexed feed
                                return serverResource.query(queryConfig)
                                    .$promise
                                    .then(function(firstItems) {

                                        var indexedData = {
                                            massive: true,
                                            items: firstItems,
                                            total: res.total,
                                        }
                                        // indexedData.total = res.total;
                                        deferred.resolve(indexedData);

                                    }, deferred.reject);

                                // //Load the indexed feed
                                // return serverResource.get(queryConfig)
                                //     .$promise
                                //     .then(function(indexedData) {
                                //         indexedData.total = res.total;
                                //         deferred.resolve(indexedData);

                                //     }, deferred.reject);
                            }




                            // // console.log('TOTAL', res.total);
                            // //If more than 1000 then we want to get it as a meta indexed feed
                            // if (res.total > massiveQueryLimit) {
                            //     // console.log('LOAD META DATA NOW');
                            //     queryConfig.meta = true;
                            //     queryConfig.limit = massiveQueryLimit;
                            //     // queryConfig.offset = massiveQueryLimit;

                            //     var date = moment().subtract(6, 'months').toDate();
                            //     queryConfig.createdAfter = date;


                            //     //Load the indexed feed
                            //     return serverResource.get(queryConfig)
                            //         .$promise
                            //         .then(function(indexedData) {
                            //             indexedData.total = res.total;
                            //             deferred.resolve(indexedData);

                            //         }, deferred.reject);
                            // }

                            //////////////////////////////////////////

                            //////////////////////////////////////////

                            switch (type.path) {
                                case 'account':
                                case 'event':
                                    // case 'transaction':
                                    break;
                                default:
                                    //USE FAST QUERY ENDPOINT
                                    console.log('DO FAST QUERY INSTEAD', queryConfig)
                                    return $http.post(Fluro.apiURL + '/content/' + definition.definitionName + '/filter', {
                                            select: queryConfig.fields,
                                            searchInheritable: queryConfig.searchInheritable,
                                            includeArchived: !queryConfig.excludeArchived,
                                            legacy: true,
                                            // populateAll:true,

                                            // select: queryConfig.fields,
                                            // searchInheritable: queryConfig.searchInheritable,
                                            // includeArchived: !queryConfig.excludeArchived,
                                            // allDefinitions: queryConfig.allDefinitions,
                                            // // populateAll:true,
                                            // legacy: true,


                                        })
                                        .then(function(res) {
                                            console.log('FAST QUERY')
                                            deferred.resolve(_.sortBy(res.data, 'created').reverse());
                                        }, function(err) {
                                            deferred.reject(err);
                                        });
                                    break;
                            }


                            //Query and load al the items
                            return serverResource.query(queryConfig)
                                .$promise
                                .then(deferred.resolve, deferred.reject);


                        }, deferred.reject);





                    return deferred.promise;


                    /**  

                      //   console.log('FILTER 2', definition);
                      var promise = FluroContent.resource(definition.definitionName, false, neverCache).query(queryConfig).$promise

                      //   console.log('FILTER 3')
                      promise.then(function(res) {
                          //   console.log('Got res', res);
                      }, function(err) {
                          //   console.log('ERRORrrrrrr', err);
                      });

                      //   console.log('FILTER 4')
                      return promise;
                      /**/

                },
                definitions: function(FluroContent) {
                    //   console.log('FILTER 5')
                    //   console.log('DEFINITIONS')
                    return FluroContent.endpoint('defined/types/' + type.path, true, true).query().$promise;
                },
                extras: function(FluroContent, $q) {

                    var deferred = $q.defer();

                    //Data Object
                    var data = {};



                    //   console.log('EXTRAS')
                    switch (type.path) {
                        /**
                        case 'event':
                            FluroContent.endpoint('confirmations').query().$promise.then(function(res) {
                                data.eventConfirmations = res;
                                deferred.resolve(data);
                            }, deferred.reject);
                            break;
                            /**/
                        default:
                            deferred.resolve(data);
                            break;
                    }



                    return deferred.promise;
                }
            }

        });


        //Trash List page
        $stateProvider.state(type.path + '.custom_trash', {
            title: type.plural,
            url: '/:definition/trash',
            // params: {
            //     definition: null,
            // },
            template: '<div class="application-panel" ng-include="template"></div>',
            controller: 'ContentTrashController',
            resolve: {
                definition: function($stateParams, $q, FluroContent) {
                    if ($stateParams.definition) {
                        //Get the definition
                        return FluroContent.endpoint('defined/' + $stateParams.definition).get().$promise;
                    } else {
                        //   console.log('No definition name found')
                        return $q.defer().reject();
                    }
                },
                type: function() {
                    return type;
                },
                access: function(FluroAccess, definition) {
                    var canRestore = FluroAccess.can('restore any', definition.definitionName, definition.parentType);
                    var canDestroy = FluroAccess.can('destroy any', definition.definitionName, definition.parentType);
                    var canRestoreOwn = FluroAccess.can('restore own', definition.definitionName, definition.parentType);
                    var canDestroyOwn = FluroAccess.can('destroy own', definition.definitionName, definition.parentType);
                    return FluroAccess.resolveIf((canRestore || canDestroy || canRestoreOwn || canDestroyOwn));
                },
                actions: function() {
                    return null;
                },
                items: function(FluroContent, definition) {

                    return FluroContent.resource(definition.definitionName).query({
                        status: 'deleted',
                        list: true,
                        cache: false,
                        fields: ['title', 'updatedBy', 'updated']
                    }).$promise;
                },
                definitions: function(items, Fluro) {
                    return;
                },
            }
        });



    }

    //////////////////////////////////////////////////////////


    //List page
    $stateProvider.state('pinned', {
        title: 'My Pins',
        url: '/pinned',
        controller: 'PinnedRouteController',
        templateUrl: 'routes/pinned/pinned.html',
    });


    //////////////////////////////////////////////////////////

    //List page
    $stateProvider.state('upload', {
        title: 'Upload',
        url: '/upload/:type',
        // params: {
        //     type: null,
        // },
        templateUrl: 'views/upload.html',
        controller: 'UploadController',
        resolve: {
            definedTypes: function(TypeService) {

                var types = TypeService.definedTypes;
                if (types.length) {
                    return types;
                } else {

                    var req = TypeService.refreshDefinedTypes();

                    return req.$promise;


                    //return TypeService.refreshDefinedTypes().$promise;
                }
            },
            definedType: function(definedTypes, $stateParams, TypeService) {
                //We need to wait for the 'definedTypes' to be resolved before we call this
                return TypeService.getTypeFromPath($stateParams.type);
            },
            access: function(FluroAccess, definedType) {

                var uploadableTypes = ['asset', 'image', 'video', 'audio'];
                var isUploadable;

                if (definedType.parentType) {
                    isUploadable = _.contains(uploadableTypes, definedType.parentType);
                } else {
                    isUploadable = _.contains(uploadableTypes, definedType.path);
                }

                var canCreate = FluroAccess.can('create', definedType.path, definedType.parentType);
                return FluroAccess.resolveIf(canCreate && isUploadable);
            },
            /*
            createables: function(TypeService, definedTypes) {
               
                var createableTypes = TypeService.getAllCreateableTypes();

                return _.filter(createableTypes, function(def) {
                    if(def.parentType) {
                        return _.contains(['asset', 'image', 'video', 'audio'], def.parentType);
                    } else {
                        return _.contains(['asset', 'image', 'video', 'audio'], def.path);
                    }
                });
            },
            access: function(FluroAccess, createables) {
                if (createables.length) {
                    return FluroAccess.resolveIf(true);
                } else {
                    return FluroAccess.resolveIf(false);
                }
            },
            */
        }
    });

    /**/
    //List page
    $stateProvider.state('matrix', {
        title: 'Multi Planner',
        url: '/matrix?events',
        // params: {
        //     id: null,
        // },
        templateUrl: 'routes/matrix/matrix.html',
        controller: MatrixRouteController,
        resolve: MatrixRouteController.resolve,
    });
    /**/


    /**/
    //List page
    $stateProvider.state('mycalendar', {
        title: 'Calendar Sync',
        url: '/my/calendar',
        // params: {
        //     id: null,
        // },
        templateUrl: 'routes/mycalendar/mycalendar.html',
        controller: MyCalendarController,
        resolve: MyCalendarController.resolve,
    });

    $stateProvider.state('myaccount', {
        title: 'My Account',
        url: '/my/account',
        // params: {
        //     id: null,
        // },
        templateUrl: 'routes/myaccount/myaccount.html',
        controller: MyAccountController,
        resolve: MyAccountController.resolve,
    });
    /**/




    /**/
    //List page
    $stateProvider.state('songselect', {
        title: 'Import From SongSelect',
        url: '/import/songselect',
        // params: {
        //     id: null,
        // },
        templateUrl: 'routes/import.songselect/songselect.html',
        controller: SongSelectRouteController,
        resolve: SongSelectRouteController.resolve,
    });
    /**/



    $stateProvider.state('importPodcast', {
        title: 'Import Podcasts',
        url: '/import/podcasts',
        // params: {
        //     id: null,
        // },
        templateUrl: 'routes/import.podcasts/podcasts.html',
        controller: ImportPodcastController,
        resolve: ImportPodcastController.resolve,
    });


    $stateProvider.state('importElvanto', {
        title: 'Import Elvanto',
        url: '/import/elvanto',
        // params: {
        //     id: null,
        // },
        templateUrl: 'routes/import.elvanto/elvanto.html',
        controller: ImportElvantoController,
        resolve: ImportElvantoController.resolve,
    });


    $stateProvider.state('interactionIssues', {
        title: 'Interaction Issues',
        url: '/issues/interactions',
        // params: {
        //     id: null,
        // },
        templateUrl: 'routes/issues.interactions/interactions.html',
        controller: IssuesInteractionsController,
        resolve: IssuesInteractionsController.resolve,
    });



    /**/
    //List page
    $stateProvider.state('channel', {
        url: '/channel/:id',
        // params: {
        //     id: null,
        // },
        templateUrl: 'routes/channel/channel.html',
        controller: 'ChannelController',
        resolve: {
            channel: function($rootScope, $stateParams, FluroContent) {

                var user = $rootScope.user;



                /////////////////////////////////////////////




                function findRealm(haystack) {

                    var results = [haystack];

                    if (haystack.children) {
                        var extras = _.map(haystack.children, findRealm);
                        results = results.concat(extras);
                    }

                    return results;
                }

                /////////////////////////////////////////////

                var flattenedRealms = _.chain(user.permissionSets)
                    .map(findRealm)
                    .flattenDeep()
                    .compact()
                    .uniq()
                    .value();

                // console.log('Flattened Realms', flattenedRealms);

                return _.find(flattenedRealms, {
                    _id: $stateParams.id
                });
            },
            items: function($stateParams, FluroContent) {
                return FluroContent.endpoint('realm/list/' + $stateParams.id).query().$promise;
            },
        }
    });
    /**/


    $stateProvider.state('home', {
        title: 'Home',
        url: '/',
        controller: 'HomeController',
        templateUrl: 'views/home.html',
        resolve: {
            /*
            stats: function(FluroContent) {
                return FluroContent.endpoint('track').query().$promise;
            },
            */
            /**/
            checkins: function(FluroContent) {
                return; // FluroContent.endpoint('info/stats/checkins').query().$promise;
            },
            attendance: function(FluroContent) {
                return; // FluroContent.endpoint('info/stats/attendance').query().$promise;
            },
            interactions: function(FluroContent) {
                return; // FluroContent.endpoint('info/stats/interactions').query().$promise;
            },

            /**/
            /**
            assignments: function(FluroContent) {
                return FluroContent.endpoint('info/stats/assignments').query().$promise;
            }
            /**/

        }
    });


    $stateProvider.state('certificates', {
        title: 'Certificates',
        url: '/certificates',
        controller: 'CertificatesController',
        templateUrl: 'routes/certificates/certificates.html',
        resolve: {
            certificates: function($http, Fluro) {
                return $http.get(Fluro.apiURL + '/system/certificates');
            },

            /**/
            /**
            assignments: function(FluroContent) {
                return FluroContent.endpoint('info/stats/assignments').query().$promise;
            }
            /**/

        }
    });


    $stateProvider.state('servers', {
        title: 'Servers',
        url: '/servers',
        controller: 'ServersController',
        templateUrl: 'routes/servers/servers.html',
        resolve: {
            servers: function($http, Fluro) {
                return $http.get(Fluro.apiURL + '/system/instances');
            },

            /**/
            /**
            assignments: function(FluroContent) {
                return FluroContent.endpoint('info/stats/assignments').query().$promise;
            }
            /**/

        }
    });


    $stateProvider.state('logs', {
        title: 'Logs',
        url: '/logs',
        controller: 'ServerLogsController',
        templateUrl: 'routes/logs/logs.html',
        resolve: {
            logs: function($http, Fluro) {
                return $http.get(Fluro.apiURL + '/log', {
                    params: {
                        limit: 500,
                    }
                });
            },

            /**/
            /**
            assignments: function(FluroContent) {
                return FluroContent.endpoint('info/stats/assignments').query().$promise;
            }
            /**/

        }
    });


    // $stateProvider.state('mailoutPreview', {
    //     title: 'Mailout Preview',
    //     url: '/mailout/preview/:definitionName?mailout',
    //     controller: 'MailoutPreviewController',
    //     templateUrl: 'fluro-admin-content/types/mailout/preview.html',
    //     resolve: {
    //         //Get the definition
    //         definition: function($stateParams, FluroContent) {
    //             // if ($stateParams.definitionName) {
    //                 return FluroContent.endpoint('defined/' + $stateParams.definitionName).get().$promise;
    //             // } else {
    //                 // return null;
    //             // }
    //         },
    //         mailout: function($stateParams, definition, FluroContent) {

    //             if(!$stateParams.mailout) {
    //                 return null;
    //             }


    //             var queryConfig = {
    //                 id: $stateParams.mailout,
    //                 // appendPostCount: 'all', maybe add annotations later on
    //             }

    //             return FluroContent.resource(definition.definitionName).get(queryConfig).$promise;
    //         },
    //     }
    // });



});




/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

//Request the user to sign in again if the cookie expires or is signed out
//due to a server error
app.service('FluroSignedOut', function($q, $injector) {
    return {
        responseError: function(response) {
            console.log('Unauthenticated', response);
            switch (response.status) {
                case 401:
                    // console.log('PROMPT LOGIN')

                    var ModalService = $injector.get('ModalService');
                    var promise = ModalService.promptLogin();


                    promise.then(function(res) {
                        console.log('SUCCESS CALLBACK!!!', res)
                    }, function(err) {
                        console.log('ERROR CALLBACK', err);
                    });


                    break;
            }

            return $q.reject(response);
        },
    };
});


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
//app.run(function($state, $rootScope, FluroAuthService) {




app.run(function($state, $sessionStorage, $q, $templateCache, FluroSocket, AccountService, $rootScope, ServerNotificationService, PinService, AccessPassService, IntercomService, PersonaService, $interval, SearchService, ShareModalService, Batch, FluroStorage, $uibModalStack, FluroContent, $rootScope, $templateCache, Fluro, FluroTokenService, FluroAccess, FluroSEO, ModalService, TypeService, VideoTools, Asset, $initUser, $document) {




    $rootScope.singleLineAce = function(editor) {
        // remove newlines in pasted text
        editor.on("paste", function(e) {
            e.text = e.text.replace(/[\r\n]+/g, " ");
        });

        // make mouse position clipping nicer
        editor.renderer.screenToTextCoordinates = function(x, y) {
            var pos = this.pixelToScreenCoordinates(x, y);
            return this.session.screenToDocumentPosition(
                Math.min(this.session.getScreenLength() - 1, Math.max(pos.row, 0)),
                Math.max(pos.column, 0)
            );
        };
        // disable Enter Shift-Enter keys
        editor.commands.bindKey("Enter|Shift-Enter", "null")
    }

    /////////////////////////////////////////////////////////////////////


    $templateCache.put('mentio-menu.tpl.html', '<ul class=\"dropdown-menu\" style=\"display:block\"> <li mentio-menu-item=\"item\" ng-repeat=\"item in items track by $index\"> <a class="clearfix content-select-option mention-item"> <img class="avatar" ng-src="{{$root.personaAvatarURL(item._id)}}"/> <span ng-bind-html=\"item.label | mentioHighlight:triggerText:\'menu-highlighted\' | unsafe\"></span> </a> </li> </ul>');


    $rootScope.definitionName = function(string, plural) {

        var match = TypeService.dictionary[string];

        // console.log('MATCH', string, match, TypeService.dictionary);

        if (!match) {
            return string;
        }

        if (plural) {
            return match.plural;
        } else {
            return match.singular || match.title;
        }
    }
    //////////////////////////////////////

    $rootScope.accountService = AccountService;
    //////////////////////////////////////

    $rootScope.mentionable = [{}]

    //////////////////////////////////////

    $rootScope.getMentionTextRaw = function(item, $event) {
        return '@' + item.mentionID;
    };

    $rootScope.searchMentionable = function(term) {

        console.log('Search Mentionable', term)
        var peopleList = [];

        /////////////////////////////////////

        if (!term || !term.length) {
            console.log('No Term mentioned')
            return;
        }

        /////////////////////////////////////

        var promise = FluroContent.endpoint('mention/' + term).query({}).$promise;

        promise.then(function(res) {
            // console.log('SEARCH FOR', term, res);
            angular.forEach(res, function(item) {
                if (item.title.toLowerCase().indexOf(term.toLowerCase()) >= 0) {
                    // item.mentionID = item.firstName;
                    peopleList.push({
                        label: item.title,
                        mentionID: item.mentionID,
                    });
                }
            });


            console.log('People match', peopleList);
            $rootScope.mentionable = peopleList;
            return $q.when(peopleList);
        })

        /////////////////////////////////////

        return promise;



        // return $rootScope.people;
        // var deferred = $q.defer();

        // deferred.resolve($rootScope.people);


        //$rootScope.people;
        // var peopleList = [];
        // return $http.get('peopledata.json').then(function (response) {
        //     angular.forEach(response.data, function(item) {
        //         if (item.name.toUpperCase().indexOf(term.toUpperCase()) >= 0) {
        //             peopleList.push(item);
        //         }
        //     });
        //     $scope.people = peopleList;
        //     return $q.when(peopleList);
        // });

        // return deferred.promise;
    };




    $rootScope.hasModal = function() {
        var hasModal = $uibModalStack.getTop();

        if (hasModal) {
            return true;
        }
    }

    //////////////////////////////////////////////////////////////////

    $rootScope.serverNotifications = ServerNotificationService;

    //////////////////////////////////////////////////////////////////

    $rootScope.pinService = PinService
    //////////////////////////////////////////////////////////////////

    $rootScope.intercom = IntercomService;
    $rootScope.accessPassService = AccessPassService;


    //////////////////////////////////////////////////////////////////

    $rootScope.personaService = PersonaService;

    //////////////////////////////////////////////////////////////////

    var browserTimezone = moment.tz.guess();
    $rootScope.currentTimezone = browserTimezone

    //////////////////////////////////////////////////////////////////

    $rootScope.differentTimezone = function(timezone) {

        if (!timezone) {
            // console.log('NO TIMEZONE')
            return;
        }



        if ($rootScope.currentTimezone == timezone) {
            // console.log('TIMEZONE IS SAME')
            return;
        }

        var now = new Date();
        var current = moment.tz(now, browserTimezone).utcOffset();
        var checked = moment.tz(now, timezone).utcOffset();

        // console.log('-----')
        // console.log('CURRENT', current)
        // console.log('CHECKED', checked)
        // console.log('MATCH', current == checked);

        if (current == checked) {
            return;
        }

        return true;

    }

    //////////////////////////////////////////////////////////////////
    /*
    //Disable Backspacing
    $document.on('keydown', keyDown)

    function keyDown(event) {
        var key = event.keyCode || event.which;

       //var contentEditable = (String(event.target.contentEditable) == 'true');
        var contentEditable = (String(event.target.contentEditable) == 'true');
        var input = (event.target.nodeName == "INPUT" || event.target.nodeName == "SELECT");

        switch (key) {
            case 8:
                if (contentEditable || input) {
                   // //console.log('Is allowed backspace')
                } else {
                 //   console.log('BACKSPACE')
                    event.preventDefault();
                }
                break;
        }


     //   console.log('TESTING', event)
         event.preventDefault();
    }
*/
    //////////////////////////////////////////////////////

    /**
    $interval(function() {
        FluroContent.resource('session').save({noCache:true}).$promise.then(function(res) {
            console.log('HIT');
        })
    }, 300)
    /**/

    //////////////////////////////////////////////////////

    // console.log('TESTING GOT USER', $initUser, Fluro.token);
    if (!$rootScope.user) {
        $rootScope.user = $initUser;
    }

    /////////////////////////////////////////////

    if (window.Chatra) {
        window.Chatra('setIntegrationData', {
            /* main properties */
            name: $rootScope.user.firstName + ' ' + $rootScope.user.lastName,
            email: $rootScope.user.email,
            account: $rootScope.user.account.title,
        });

        window.ChatraSetup = {
            clientId: $rootScope.user._id + '-chat',
            buttonSize: 0,
        }

        $rootScope.toggleChat = function() {

            if ($rootScope.chatOpen) {
                $rootScope.chatOpen = false;
                window.Chatra('minimizeWidget')
            } else {
                $rootScope.chatOpen = true;
                window.Chatra('openChat', true)
            }
        }
        console.log('SET INTEGRATION DATA')
    }

    /////////////////////////////////////////////


    $rootScope.shareModal = ShareModalService;
    $rootScope.access = FluroAccess;
    $rootScope.asset = Asset;


    $rootScope.apiURL = Fluro.apiURL;

    //////////////////////////////////////////////////////

    //$rootScope.basicTypes = TypeService.types;
    TypeService.refreshDefinedTypes();


    // $rootScope.realms = $rootScope.user.permissionSets;

    /**
    FluroContent.endpoint('realm/tree').query().$promise.then(function(res) {
        $rootScope.realms = res;
        console.log('Realms', res);
        
    })
    /**/

    //////////////////////////////////////////////////////

    $rootScope.getVideoImageUrl = VideoTools.getVideoThumbnail;
    $rootScope.getImageUrl = Asset.imageUrl;
    $rootScope.getThumbnailUrl = Asset.thumbnailUrl;
    $rootScope.getDownloadUrl = Asset.downloadUrl;
    $rootScope.getUrl = Asset.getUrl;

    //Add search to rootscope
    $rootScope.quicksearch = SearchService;

    //////////////////////////////////////////////////////

    //Batch
    $rootScope.batch = Batch;
    //////////////////////////////////////////////////////

    //Modal
    $rootScope.modal = ModalService;

    //////////////////////////////////////////////////////


    //Handy function for opening a full page with a back ability
    $rootScope.viewItem = function(item, isModal, type) {

        // console.log('Get VIEW ITEM', item, isModal, type);
        // return;

        // console.log('VU')

        if (isModal) {
            //Were in a modal
            ModalService.view(item);
            // ui-sref="contact.view({id:contact._id, backOnClose:true})"
        } else {
            if (!type || !type.length) {
                type = item._type;
            }
            $state.go(type + '.view', {
                id: item._id,
                definitionName: item.definition,
                backOnClose: true
            })
        }
    }


    //////////////////////////////////////////////////////

    //Handy function for opening a full page with a back ability
    $rootScope.editItem = function(item, isModal, type) {



        console.log('Get EDIT ITEM', item, isModal, type);
        // return;

        // console.log('VU')

        if (isModal) {
            //Were in a modal
            ModalService.edit(item);
            // ui-sref="contact.view({id:contact._id, backOnClose:true})"
        } else {
            if (!type || !type.length) {
                type = item._type;
            }
            $state.go(type + '.edit', {
                id: item._id,
                definitionName: item.definition,
                backOnClose: true
            })
        }
    }

    //////////////////////////////////////////////////////

    $rootScope.userAvatarURL = function(userID) {
        var url = Fluro.apiURL + '/get/avatar/user/' + userID;
        return url;
    }

    $rootScope.personaAvatarURL = function(personaID) {
        var url = Fluro.apiURL + '/get/avatar/persona/' + personaID;
        return url;
    }

    $rootScope.contactAvatarURL = function(personaID) {
        var url = Fluro.apiURL + '/get/avatar/contact/' + personaID;
        return url;
    }

    $rootScope.checkinAvatarURL = function(personaID) {
        var url = Fluro.apiURL + '/get/avatar/checkin/' + personaID;
        return url;
    }


    //////////////////////////////////////////////////////

    $rootScope.clearSessionTokens = function() {
        //   console.log('Clear session tokens');
        FluroTokenService.deleteSession();
        location.reload();
    }

    //////////////////////////////////////////////////////

    // console.log('TESTING', $rootScope.user);

    $rootScope.isSessioned = function() {
        if ($rootScope.user.refreshToken) {
            return true;
        }
    }

    //////////////////////////////////////////////////////



    $rootScope.canAccess = function(path) {

        var canAccessType = FluroAccess.canAccess(path);

        if (canAccessType) {
            return true;
        }

        //Lookup the other types to see if we need access
        var basicTypes = TypeService.requiredBasicTypes();
        var canAccessDefinition = _.contains(basicTypes, path);

        //Return 
        return (canAccessDefinition)
    }


    $rootScope.cleanupPastedHTML = function(html) {

        return strip_tags(html, "<br><p><img><a><h1><h2><h3><h4><h5><h6><ol><ul><li>");
    }

    //////////////////////////////////////////////////////////////////

    $rootScope.seo = FluroSEO;

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeStart', function(evt, to, params) {
        if (to.redirectTo) {
            evt.preventDefault();
            $state.go(to.redirectTo, params)
        }

        SearchService.criteria = {};
        SearchService.open = false;
    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
        console.log('ERROR', event, error)
        // throw error;
    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams) {

        //Hide the overlay menu
        // FluroStorage.sessionStorage('application').showNewMenu = false;



        if (toParams.definition) {
            $rootScope.currentDefPath = toParams.definition;
        } else {
            delete $rootScope.currentDefPath;
        }

        //// console.log('STATE', $state.current.resolve.definition());

        FluroSEO.headTitle = $state.$current.title + ' | Admin';
    });


    //Make touch devices more responsive
    FastClick.attach(document.body);

});