app.directive('phoneVerification', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: true,
        //     model: '=ngModel',
        //     single:'='
        // },
        templateUrl: 'admin-phone-verification/admin-phone-verification.html',
        controller: 'PhoneVerificationController',
    };
});

app.controller('PhoneVerificationController', function($scope, $http, $rootScope, Notifications, Fluro, FluroContent) {

    $scope.settings = {
        state:'ready',
    };

    //////////////////////////////////////////////////

    $scope.submission = {};

    //////////////////////////////////////////////////

    $scope.verify = function() {

        if(!$scope.settings.code || !$scope.settings.code.length) {
            return;
        }

        ////////////////////////////////////////////////////

        function verifyComplete(res) {
            $scope.settings.state = 'verified';
            $rootScope.user.phoneNumber = res.number;
            $rootScope.user.countryCode = res.countryCode;
        }

        function verifyFailed(err) {
            Notifications.warning('Phone Number Verification Failed');
            $scope.settings.state = 'error';
        }

        ////////////////////////////////////////////////////

        $scope.settings.state = 'verifying';

        ////////////////////////////////////////////////////

        var verifyData = {
            code:$scope.settings.code,
            number:$scope.settings.requestedNumber,
        }


        FluroContent.endpoint('sms/verify')
        .save(verifyData)
        .$promise
        .then(verifyComplete, verifyFailed);

    }

    //////////////////////////////////////////////////

    $scope.request = function() {

        if(!$scope.submission.number || !$scope.submission.number.length) {
            return;
        }

        ////////////////////////////////////////////////////

        function requestComplete(res) {
            $scope.settings.state = 'requested';
            $scope.settings.requestedNumber = res.number;
        }

        function requestFailed(err) {
            Notifications.warning('Verification request failed');
            $scope.settings.state = 'error';
        }

        ////////////////////////////////////////////////////

        $scope.settings.state = 'requesting';

        ////////////////////////////////////////////////////

        FluroContent.endpoint('sms/request')
        .save($scope.submission)
        .$promise
        .then(requestComplete, requestFailed);

    }

});