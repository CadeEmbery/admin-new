app.directive('actionsDropdown', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            definition: '=ngDefinition',
        },
        templateUrl: 'actions-dropdown/actions-dropdown.html',
        controller: 'ActionsDropdownController',
    };
});

// template: '<a popover-trigger="outsideClick" uib-popover-template="dynamicPopover.templateUrl" popover-is-open="settings.open" popover-append-to-body="true" popover-placement="bottom" popover-title="{{dynamicPopover.title}}" class="btn btn-default"><i class="far fa-cog"></i></a>',


/////////////////////////////////////////////////////

app.controller('ActionsDropdownController', function(FluroAccess, $state, Notifications, FluroAccess, CacheManager, Batch, TypeService, $scope) {

    $scope.settings = {
        open: false,
    }

    //////////////////////////////////////////////////////////

    if (!$scope.model) {
        return;
    }



    var allDefinitions =

        //////////////////////////////////////////////////////////

        $scope.dynamicPopover = {
            templateUrl: 'actions-dropdown/actions-dropdown-popover.html',
        };

    //////////////////////////////////////////////////////////

    $scope.actions = [];
    // $scope.actions = [{
    //     title:'Test',
    //     action:function() {

    //     }
    // }];

    //////////////////////////////////////////////////////////



    /////////////////////////////////////

    //Move to status
    // $scope.moveTo = function(status) {

    //     $scope.settings.open = false;

    //     var details = {
    //         ids: $scope.selection.ids,
    //         status: status,
    //     }


    //     if ($scope.type.path == 'persona') {
    //         details.useModel = 'persona';
    //     }

    //     if ($scope.type.path == 'ticket') {
    //         details.useModel = 'ticket';
    //     }


    //     Batch.setStatus(details, done);
    // }

    //////////////////////////////////////////////////////////

    $scope.act = function(action) {
        // console.log('Action', action.action)
        $scope.settings.open = false;

        if(action.check) {
            var confirmed = window.confirm("Are you sure you want to " + action.title + '?');

            if (!confirmed) {
                return;
            }
        }


        if (action.action) {
            action.action();
        }
    }

    //////////////////////////////////////////////////////////

    function done(err, data) {
        // console.log(err, results);


        if (err) {
            //console.log('DONE ERR', err)
            return Notifications.error(err);
        }


        // console.log('Batch done', data);




        ///////////////////////

        //Find all the caches we need to clear
        var caches = [];

        ///////////////////////

        //Make sure we refresh the current view
        if ($scope.model.definition) {
            caches.push($scope.model.definition)
        }
        caches.push($scope.model._type)

        ///////////////////////

        //Refresh any caches that might be affected outside of this view
        if (data.result) {
            if (data.result.types && data.result.types.length) {
                caches.concat(data.result.types)
            }

            if (data.result.definitions && data.result.definitions.length) {
                caches.concat(data.result.definitions)
            }
        }

        ///////////////////////

        caches = _.uniq(caches);
        caches = _.compact(caches);

        _.each(caches, function(t) {
            CacheManager.clear(t);
        });

        ///////////////////////


        var type = $scope.model._type;


        var actionTaken = data.action;
        var currentStateName = $state.current.name;



        switch (actionTaken) {
            case "batch remove definition":

                ////////////////////////////////////////////

                //Find out what it is now
                console.log('CURRENT STATE', $state);

                console.log('GO TO', currentStateName, { id: $scope.model._id, definitionName: '' })
                return $state.go(currentStateName, { id: $scope.model._id, definitionName: '' });

                break;
            case 'batch redefine':


                var toDefinition = data.data.definition;
                ////////////////////////////////////////////

                console.log('Get current State', $state);
                return $state.go(currentStateName, { id: $scope.model._id, definitionName: toDefinition });


                break;
        }

        ///////////////////////


        //Reload the state
        $state.reload();

        ///////////////////////

        //Deselect all
        // $scope.selection.deselectAll();
        // Notifications.status('Updated  ' + data.success.length + ' ' + $scope.type.plural);



    }




    //////////////////////////////////////////////////////////


    var sourceType = $scope.model._type;

    //////////////////////////////////////////////////////////

    //Get all the Capability Definitions
    var definitions = TypeService.getSubTypes(sourceType)
    var canEdit = FluroAccess.canEditItem($scope.model);
    var canDelete = FluroAccess.canEditItem($scope.model);

    //////////////////////////////////////////////////////////

    switch ($scope.model._type) {
        case 'contact':

            //Check if can edit
            if (canEdit) {

                if ($scope.model.status != 'archived') {
                    $scope.actions.push({
                        title: 'Archive ' + $scope.model.title,
                        action: function() {
                            console.log('ACT');
                            Batch.setStatus({
                                ids: [$scope.model._id],
                                status: 'archived',
                            }, done);
                        },
                        check: true,
                    })
                }


                // if ($scope.model.status != 'archived') {
                //     $scope.actions.push({
                //         title: 'Archive ' + $scope.model.title + ' and family members',
                //         action: function() {
                //             console.log('ACT');
                //             Batch.setStatus({
                //                 ids: [$scope.model._id],
                //                 status: 'archived',
                //                 entireFamily:true,
                //             }, done);
                //         },
                //         check: true,
                //     })
                // }

                if ($scope.model.status != 'active') {
                    $scope.actions.push({
                        title: 'Mark ' + $scope.model.title + ' as active',
                        action: function() {
                            console.log('ACT');
                            Batch.setStatus({
                                ids: [$scope.model._id],
                                status: 'active',
                            }, done);
                        }
                    })
                }


                //Remove the existing definition
                if ($scope.definition && $scope.model.definition && $scope.model.definition.length) {
                    $scope.actions.push({
                        title: "Remove '" + $scope.definition.title + "' definition",
                        action: function() {
                            Batch.removeDefinition({
                                ids: [$scope.model._id],
                            }, done);
                        },
                        check: true,
                    })
                }

                /////////////////////////////////////////////////////

                _.each(definitions, function(definition) {


                    if (definition.definitionName == $scope.model.definition) {
                        return;
                    }

                    /////////////////////////////////////

                    $scope.actions.push({
                        title: "Redefine as '" + definition.title + "'",
                        action: function() {


                            // $scope.removeDefinition = function() {
                            //     $scope.settings.open = false;
                            //     var details = {
                            //         ids: $scope.selection.ids,
                            //     }

                            //     Batch.removeDefinition(details, done);
                            // }

                            // /////////////////////////////////////

                            // $scope.defineAs = function(def) {


                            Batch.redefine({
                                ids: [$scope.model._id],
                                definition: definition.definitionName,
                            }, done);


                        },
                        check: true,
                    })

                })

            }


            ////////////////////////////////////////////////////////

            if (canDelete) {
                $scope.actions.push({
                    title: 'Delete ' + $scope.model.title,
                    action: function() {
                        $state.go($scope.model._type + '.delete', { id: $scope.model._id, definitionName: $scope.model.definition });
                    },
                    // check: true,
                })

            }


            // //Remove the existing definition
            // if ($scope.definition && $scope.model.definition && $scope.model.definition.length) {
            //     $scope.actions.push({
            //         title: "Remove '" + $scope.definition.title + "' definition",
            //         action: function() {
            //             Batch.removeDefinition({
            //                 ids: [$scope.model._id],
            //             }, done);
            //         }
            //     })
            // }
            break;
    }



});