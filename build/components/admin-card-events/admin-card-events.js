app.directive('cardEvents', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            browseDate: '=ngDate',
            type: '=ngType',
        },
        templateUrl: 'admin-card-events/admin-card-events.html',
        controller: 'CardEventsController',
    };
});



app.directive('datemark', function() {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="datemark"><span class="weekday">{{weekday}}</span><h2>{{dateNumber}}</h2><h3>{{month}}</h3></div>',
        scope: {
            model: '=ngModel',
        },
        controller: function($scope, DateTools) {

            var d = new Date($scope.model); //new Date();

            if (!DateTools.isValidDate(d)) {
                d = new Date();
                d.setTime($scope.model)
            }




            /*
             var integer = parseInt($scope.model);

            if(integer) {
                d.setTime($scope.model)
            } else {
                d = new Date($scope.model);
            }
            console.log('Datemark', $scope.model, integer, d);
            */

            $scope.weekday = d.format('l');
            $scope.dateNumber = d.format('j');
            $scope.month = d.format('F');
        }
    }
});



app.controller('CardEventsController', function($scope, Fluro, $rootScope, $uibModal, CacheManager, FluroAccess, $state, ModalService) {

    // $scope.listController= $scope.$parent.$parent.$parent.$parent;
    //$scope.canCreate = $scope.listController.canCreate;

    //console.log('LIST CONTROLLER', $scope.listController,$scope, $scope.type);

    $scope.canCreate = function() {
        // console.log('GET DEFINITION', $scope.definition);
        return FluroAccess.can('create', $scope.type);
    }

    //////////////////////////////////////////////////

    $scope.summary = getFirstLine;

    // function(event) {
    //     return _.chain([
    //         getTimes(event),
    //         getFirstLine(event),
    //     ])
    //     .compact()
    //     .value()
    //     .join(' • ')
    // }

    //////////////////////////////////////////////////

    function getFirstLine(event) {
        if(event.firstLine && event.firstLine.length) {
            return String(event.firstLine).slice(100);
        }
    }

    //////////////////////////////////////////////////


    function getRooms(event) {
        if(event.rooms && event.rooms.length) {
            return  _.map(event.rooms, function(room) {
                return room.title;
            }).join(', ');
        }
    }

    $scope.rooms = getRooms;
    $scope.times = getTimes;

    //////////////////////////////////////////////////
    function getTimes(event) {

        var startDate;
        var endDate;

        if(Fluro.timezoneOffset) {
            startDate = DateTools.localDate(event.startDate);
        } else {
            startDate = new Date(event.startDate);
        }

        ////////////////////////////////////////

        if(Fluro.timezoneOffset) {
            endDate = DateTools.localDate(event.endDate);
        } else {
            endDate = new Date(event.endDate);
        }

         var formattedStartDate = startDate.format('g:ia');
         var formattedEndDate = endDate.format('g:ia');


        if(startDate.format('j/m/Y') == endDate.format('j/m/Y')) {

           
            if(formattedStartDate == formattedEndDate) {
            return formattedStartDate;
            }

            
            return formattedStartDate + ' - ' + formattedEndDate;
        } else {
            return formattedStartDate;
        }


    }

    $scope.canCreatePlans = function() {
        return FluroAccess.can('create', 'plan');
    }


    $scope.canViewPlans = function() {
        return FluroAccess.can('view', 'plan');
    }

    ///////////////////////////////////////////////

    $scope.recurEvent = function(event) {
        var modalInstance = $uibModal.open({
            templateUrl: 'event-recur-tools/event-recur-tools.html',
            controller: 'EventRecurToolsController',
            //size: 'lg',
            //backdrop: 'static',
            resolve: {
                event: function() {
                    return event;
                }
            }
        });

        function successCallback(dates) {
            console.log('Success', dates);
        }

        modalInstance.result.then(successCallback);
    }


    $scope.openInMatrix = function(group) {

        // console.log('Events', array);

        var eventIDs = _.chain(group.items)
        .compact()
        .map(function(eventID) {
            if(eventID._id) {
                return eventID._id;
            }

            return eventID;
        })
        .value();

        $state.go('matrix', {events:eventIDs});
    }


    ///////////////////////////////////////////////

    $scope.canCopy = function(item) {

        var typeName = $scope.type.path;
        var parentType;

        if (item._type) {
            var typeName = item._type;
        }

        if (item.definition) {
            typeName = item.definition;
            parentType = item._type;
        }

        var valid = FluroAccess.can('create', typeName, parentType);

        //console.log('Can copy', valid, typeName, item)

        return valid;
    }

    ///////////////////////////////////////////////


    $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item, ($scope.type == 'user'));
    }

    ///////////////////////////////////////////////


    $scope.canDelete = function(item) {
        return FluroAccess.canDeleteItem(item, ($scope.type == 'user'));
    }



    ///////////////////////////////////////////////

    $scope.create = function(initDate) {
        console.log('CREATE')
            // console.log('Create at', initDate)
        $state.go('event.create', {
            initDate: initDate
        });
    }


    ///////////////////////////////////////////////

    $scope.viewInModal = function(item) {

        // console.log('View in modal')
        ModalService.view(item);
    }

    ///////////////////////////////////////////////

    $scope.editInModal = function(item) {
        ModalService.edit(item, function(result) {
            $state.reload();
        }, function(result) {});
    }

    ///////////////////////////////////////////////

    $scope.createInModal = function() {
        ModalService.create($scope.type, {}, function(result) {
            $state.reload();
        }, function(result) {
            //console.log('Failed to create in modal', result)
        });
    }



    ///////////////////////////////////////////////

    $scope.createPlanFromTemplate = function(event) {

        var selection = {};
        var modal = ModalService.browse('plan', selection);
        modal.result.then(addTemplatePlan,
            addTemplatePlan)

        function addTemplatePlan() {
            if (selection.items && selection.items.length) {

                //Create an array if there isnt one already
                if (!event.plans || !event.plans.length) {
                    event.plans = [];
                }

                //Get the template
                var template = selection.items[0];

                //Link it to this event
                template.event = event;
                template.startDate = null;




                console.log('Add Plan from template', template)
                ModalService.edit(template, function(copyResult) {
                    if (copyResult) {
                        event.plans.push(copyResult)
                    }
                    //console.log('After', $scope.item)
                }, function(res) {
                    //console.log('Failed to copy', res)
                }, true, template, $scope);
            }
        }

    }

    ///////////////////////////////////////////////

    $scope.addPlan = function(event) {


        ModalService.create('plan', {
            template: {
                event: event,
                realms: event.realms,
                startDate: event.startDate,
            }
        }, function(result) {
            console.log('RELOAD')


            CacheManager.clear($scope.type);
            $state.reload();
        }, function(result) {
            //console.log('Failed to create in modal', result)
        });
    }

    ///////////////////////////////////////////////

    $scope.copyItem = function(item) {

        ModalService.edit(item, function(result) {
            //console.log('Copied in modal', result)
            $state.reload();
        }, function(result) {
            //console.log('Failed to copy in modal', result)
        }, true);


    }






    //////////////////////////////////////////

    $scope.resetToToday = function() {
        $scope.browseDate = new Date();
    }

    $scope.$watch('model', function(data) {

        /////////////////////////////////////////

        $scope.dateList = _.chain(data)
        .reduce(function(results, item) {
                var itemDate = new Date(item.startDate);
                itemDate.setHours(0, 0, 0, 0);

                //Get the dateKey
                var dateKey = itemDate.getTime();

                if(!results[dateKey]) {
                    results[dateKey] = {
                        date:dateKey,
                        items:[],
                    }
                }

                results[dateKey].items.push(item);

                return results;
            }, {})
            .values()
            .sortBy(function(item) {
                return item.date;
            })
            .value()
            .slice(0,30);



    })

});