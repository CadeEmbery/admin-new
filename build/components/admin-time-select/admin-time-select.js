app.directive('adminTimeSelect', function($timeout) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            initDate: '=',
        },
        templateUrl: 'admin-time-select/admin-time-select.html',
        // controller: 'AdminTimeSelectController',
        link: function($scope, $element) {

            // $scope.$watch('model', function() {

            /**/
            if (!$scope.model) {
                if ($scope.initDate) {
                    if (_.isDate($scope.initDate)) {
                        $scope.model = new Date($scope.initDate.getTime());
                    } else {
                        $scope.model = new Date($scope.initDate);
                    }
                } else {
                    $scope.model = new Date();
                }
            }


            if (_.isString($scope.model)) {
                $timeout(function() {
                    $scope.model = new Date($scope.model);
                })
            }
            // console.log('MODEL INIT', $scope.model);

            /**/



            $scope.setHours = function(value) {

                // console.log('SET HOURS', value, $scope.model);
                var meridian = $scope.getMeridian();


                switch (meridian) {
                    case 'AM':
                        $scope.model.setHours(value);
                        break;
                    case 'PM':


                        if (value < 12) {
                            var newVal = value + 12;
                            $scope.model.setHours(newVal);
                        } else {
                            $scope.model.setHours(value);
                        }
                        break;
                }
            }

            $scope.setMinutes = function(value) {
                console.log('Set minutes', value)

                $scope.model.setMinutes(value);

            }

            $scope.setMeridian = function(value) {

                var meridian = $scope.getMeridian();

                if (meridian == value) {
                    return;
                }

                var hours = $scope.model.getHours();

                switch (value) {
                    case 'PM':
                        if (hours < 12) {
                            $scope.setHours(12 + hours);
                        }
                        break;
                    case 'AM':

                        if (hours == 12) {
                            value = 0;
                        }

                        if (hours > 12) {
                            var newVal = hours - 12;

                            if (newVal < 0) {
                                value = (newVal * -1);
                            } else {
                                value = newVal;
                            }
                        }
                        $scope.model.setHours(value);
                        break;
                }

            }

            /////////////////////////////////////////////////

            $scope.activeHours = function(value) {

                if(!$scope.model) {
                    return;
                }
                
                //Hours String
                var currentHours = $scope.model.getHours();
                var hoursString = String(currentHours);
                var meridian = $scope.getMeridian();

                if (hoursString == '0') {
                    return (value == 12);
                }

                if (hoursString == String(value)) {
                    return true;
                }

                switch (meridian) {
                    case 'AM':
                        return hoursString == String(value);
                        break;
                    case 'PM':
                        return hoursString == String(value + 12);
                        break;
                }

            }

            $scope.activeMinutes = function(value) {
                if ($scope.model) {

                    var stringVal = String(value);
                    var stringMinutes = String($scope.model.getMinutes());

                    if(stringMinutes.length < 2) {
                        stringMinutes  = '0' + stringMinutes;
                    }
                    // console.log('stringval', stringVal, '|', 'stringMins', stringMinutes);
                    // if(stringVal == '00' && stringMinutes == '0') {
                    //     return true;
                    // }

                    return stringVal == stringMinutes;
                }
            }

            $scope.getMeridian = function() {
                if ($scope.model) {
                    return ($scope.model.getHours() < 12) ? 'AM' : 'PM';
                }
            }

            $scope.activeMeridian = function(value) {
                return value == $scope.getMeridian();
            }

            /**/
        }
    }
})