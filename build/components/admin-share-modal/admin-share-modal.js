function copyToClipboard(string) {

    //Create a new input invisible on the screen
    var element = angular.element('<input id="clipper">');
    element.css({
        position: 'fixed',
        top: 999999999,
        left: 99999999999,
        'z-index': -999,
    });

    //Set the value to what we want to copy
    element[0].value = string;

    //Add it to the DOM and select it
    angular.element('body').append(element);
    element[0].select();

    ////////////////////////////////////////////

    //Now try and copy to clipboard
    var copied;

    try {
        copied = document.execCommand('copy');

        if (!copied) {
            console.error("Cannot copy text");
        } else {
            console.log("Copied text", copied);
        }

        //Remove the element
        element.remove();
    } catch (err) {
        console.log('Unable to copy', err);

        //Remove the element
        element.remove();
    }
}

///////////////////////////////////////////////


app.service('ShareModalService', function($uibModal, FluroContent) {

    var service = {};

    /////////////////////////////////////

    service.copy = function() {
        copyToClipboard(service.link);

        service.copied = true;
    }

    /////////////////////////////////////

    service.share = function(urlString) {

        service.link = null;
        service.copied = null;

        if (!urlString || !urlString.length) {
            return;
        }

        //Set processing
        service.processing = true;

        //Get a shortcode url for it
        FluroContent.endpoint('url/shorten')
            .save({
                url: urlString
            })
            .$promise
            .then(function(res) {
                service.processing = false
                service.link = 'http://' + res.url;


                var modalInstance = $uibModal.open({
                    templateUrl: 'admin-share-modal/admin-share-modal.html',
                    controller: function($scope) {
                        $scope.service = service;
                    },
                    size: 'sm',
                    // backdrop: 'static',
                    // resolve: {
                    //     template: function() {
                    //         return template
                    //     }
                    // }
                });

                return  modalInstance.result;

            }, function(err) {
                 service.processing = false
                console.log('ERROR', err);
            });
    }


    /////////////////////////////////////

    return service;
})



///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////

