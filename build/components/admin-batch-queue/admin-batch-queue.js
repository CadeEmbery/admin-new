app.directive('adminBatchQueue', function() {

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'admin-batch-queue/admin-batch-queue.html',
        controller: 'BatchQueueController',
    };
});

//////////////////////////////

app.controller('BatchQueueController', function($scope, Batch) {

    $scope.batch = Batch;

    //////////////////////////////

    $scope.secondsRemaining = function(task) {

        var t1 = new Date(task.eta);
        var t2 = new Date();
        var dif = t1.getTime() - t2.getTime();
        var seconds = Math.round(dif / 1000);

        if(isNaN(seconds)) {
            return 'Calculating...';
        }

        if(seconds < 2) {
            return 'Wrapping up...';
        }

        return 'About ' + seconds + ' seconds remaining';
    }

    //////////////////////////////
    
    $scope.getProgress = function(task) {
        return Math.floor((task.success.length / task.total) * 100)
    }

});