app.directive('keyContactSelect', function() {

    return {
        restrict: 'E',
        replace:true,
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-key-contact-select/admin-key-contact-select.html',
        controller: 'KeyContactSelectController',
    };
});





app.controller('KeyContactSelectController', function($scope) {

    if (!$scope.model || !_.isArray($scope.model)) {
        $scope.model = [];
    }
    
    //Create a new object
    $scope._new = {}

    ////////////////////////////////////////////////////

    $scope.remove = function(item) {
        _.pull($scope.model, item);
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {

        console.log('CREATE NEW')
        var insert = angular.copy($scope._new);
        if(!$scope.model) {
            $scope.model = [];
        }

        if(insert.position) {
            var keyExists = _.find($scope.model, {position:insert.position});
            
            if(!keyExists) {
                $scope.model.push(insert);
                $scope._new = {}
            }
        }
    }

})
