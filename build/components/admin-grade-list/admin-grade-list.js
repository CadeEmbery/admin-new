app.directive('gradeList', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            // options: '=ngOptions',
        },
        templateUrl: 'admin-grade-list/admin-grade-list.html',
        controller: 'GradeListController',
    };
});


//////////////////////////////////////////////////////////////////////////

app.controller('GradeListController', function($scope) {

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }

    //////////////////////////////////////////////////////////

    $scope.proposed = {}

    //////////////////////////////////////////////////////////

    $scope.create = function() {

        if(!$scope.proposed.title || !$scope.proposed.title.length) {
            return;
        }

        if (!_.some($scope.model, {
            title: $scope.proposed.title
        })) {
            var insert = angular.copy($scope.proposed);

            $scope.model.push(insert)
            $scope.proposed = {}
        }
    }


    //////////////////////////////////////////////////////////

    $scope.remove = function(room) {
        _.pull($scope.model, room);
    }
});