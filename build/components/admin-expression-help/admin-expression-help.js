app.directive('expressionHelp', function() {

    return {
        restrict: 'E',
        replace:true,
        // Replace the div with our template
        scope: {
            editor: '=',
        },
        templateUrl: 'admin-expression-help/admin-expression-help.html',
        controller: 'ExpressionHelpController',
    };
});



app.controller('ExpressionHelpController', function($scope) {

    console.log('Editor mang!', $scope.editor);
    $scope.$watch('editor.definition.fields', function(fields) {
        $scope.availableFields = extractFieldsFromDefinitionFields(fields, '0', true);
    }, true);

});

