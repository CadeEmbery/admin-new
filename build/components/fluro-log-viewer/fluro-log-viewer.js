app.directive('logViewer', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            itemID: '=ngModel'
        },
        templateUrl: 'fluro-log-viewer/fluro-log-viewer.html',
        controller: 'FluroLogViewerController',
    };
});

// app.controller('FluroLogViewerController', function($scope, $timeout, FluroContent, FluroSocket, $filter) {


//     $scope.logs = [];

// if($scope.itemID) {
//     FluroContent.endpoint('log').query({
//         item: $scope.itemID,
//     }).$promise.then(function(res) {
//         $scope.logs = res;
//     }, function(res) {
//         console.log('Error loading logs');
//     });

//     //////////////////////////////////////////////////////////

//     FluroSocket.on('content.edit', socketUpdate);

//     function socketUpdate(socketEvent) {

//         //Same id content
//         var sameId = (socketEvent.item == $scope.itemID);
//         $timeout(function() {
//             $scope.logs.unshift(socketEvent);
//         })

//     }
// }




// });




app.controller('FluroLogViewerController', function($scope, $timeout, FluroContent, Fluro, FluroSocket, $filter) {


    $scope.logs = [];


    if ($scope.itemID) {

        FluroContent.endpoint('log', true, true).query({
            item: $scope.itemID,
            limit: 100,
        }).$promise.then(function(res) {
            $scope.logs = res;
        }, function(res) {
            console.log('Error loading logs');
        });

        //////////////////////////////////////////////////////////

        FluroSocket.on('content.edit', socketUpdate);
        FluroSocket.on('reaction.error', socketUpdate);

        function socketUpdate(socketEvent) {

            //Same id content
            var sameId = (socketEvent.item == $scope.itemID);
            $timeout(function() {
                $scope.logs.unshift(socketEvent);
            })

        }
    }

    //////////////////////////////////////////////////////////

    $scope.getRollbackURL = function(id) {
        return Fluro.apiURL + '/content/rollback/' + id;
    }

    //////////////////////////////////////////////////////////

    $scope.$watch('logs', function(logs) {


        $scope.dated = _.chain(logs)
            .reduce(function(results, log) {


                var logDate = new Date(log.created);
                logDate.setHours(0, 0, 0);

                var monthKey = logDate.format('M Y');
                var dayKey = logDate.format('j M Y');

                //////////////////////////////////////////

                var existingMonth = results[monthKey]; //_.find(results, {monthKey:monthKey});

                if (!existingMonth) {
                    existingMonth = {
                        title: logDate.format('M Y'),
                        monthKey: monthKey,
                        days: []
                    }

                    results[monthKey] = existingMonth;
                    // results.push(existingMonth);
                }

                //////////////////////////////////////////

                var existingDay = _.find(existingMonth.days, {
                    dayKey: dayKey
                });

                if (!existingDay) {
                    existingDay = {
                        date: logDate,
                        dayKey: dayKey,
                        logs: []
                    }

                    existingMonth.days.push(existingDay);
                }

                existingDay.logs.push(log);

                return results;

            }, {})
            .values()
            .value();
    })




});