

console.log('MCE WRAPPER INIT')

//Setup the directive
app.directive('fluroMce', function($timeout, Fluro) {
    return {
        restrict: 'A',
        require: 'ngModel',
        // controller: function($scope, $uibModal) {
        //     $scope.modal = $uibModal;

        // },
        link: function($scope, $element, $attrs, ngModel) {

        	// var computed = window.getComputedStyle($element[0]);
        	// var styles = "body { font-family: !important;}"

			$scope.defaultMCE = {
				// content_style:,
				// content_style:'css/style.css',
			    height:300,
			    init_instance_callback: function(editor) {
			        if(!$scope) {
			            return console.log('No scope for editor options');
			        }

			        console.log('SET FRAME', editor.iframeElement);
			        $scope.mceFrame = editor.iframeElement;
			    }
			}


        }
	}
});