app.directive('gridList', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-grid-list/admin-grid-list.html',
        controller: 'GridListController',
    };
});



app.controller('GridListController', function($scope, $rootScope, $state, ModalService) {


    $scope.listController= $scope.$parent.$parent.$parent.$parent;
    $scope.canCreate = $scope.listController.canCreate;
    $scope.canEdit = $scope.listController.canEdit;

    $scope.selection = $scope.listController.selection;


    $scope.previewImage  = null;

    ///////////////////////////////////////////////

    $scope.viewInModal = function(item) {

        console.log('View in modal')
        ModalService.view(item);
    }


    ///////////////////////////////////////////////

    $scope.selectImage = function(item) {
        $scope.previewImage = item;
    }

    $scope.deselectImage = function() {
        $scope.previewImage = null;
    }

    ///////////////////////////////////////////////

    $scope.editInModal = function(item) {

        console.log('Edit in modal')
        ModalService.edit(item);
    }

});