app.directive('search', function() {

    return {
        restrict: 'E',
        replace:true,
        scope:{
        	model:'=ngModel',
            modelOptions:"=?",
            placeholder:'@',
        },
        templateUrl: 'admin-search/search.html',
        controller: 'SearchFormController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('SearchFormController', function($scope) {

    if(!$scope.modelOptions) {
        $scope.modelOptions = {debounce:600}
    }

    // console.log('TESTING', $scope)
    if(!$scope.placeholder || !$scope.placeholder.length) {
        $scope.placeholder = 'Search';
    }

	$scope.clear = function() {
		$scope.model = '';
	}

});
