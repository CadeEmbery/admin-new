app.directive('adminPersonaSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            single:'='
        },
        templateUrl: 'admin-persona-select/admin-persona-select.html',
        controller: 'AdminPersonaSelectController',
    };
});




app.controller('AdminPersonaSelectController', function($scope, $http, Fluro, FluroContent) {


    if (!$scope.model && !$scope.single) {
        $scope.model = [];
    }

    $scope.proposed = {};

    $scope.canAdd = true;

    $scope.$watch('single + model', function() {
        if($scope.single) {
            if($scope.model) {
                $scope.canAdd = false;
            } else {
                $scope.canAdd = true;
            }
        } else {
            $scope.canAdd = true;
        }
    })

    /////////////////////////////////////

    $scope.add = function(item) {

        if($scope.single) {
            $scope.model = item;
        } else {
            $scope.model.push(item);
        }

        $scope.proposed = {};
    }

    /////////////////////////////////////

    $scope.removePersona = function(persona) {
        if($scope.single) {
            $scope.model = null;
        } else {
           _.pull($scope.model, persona);
       }
    }

    /////////////////////////////////////

    $scope.getOptions = function(val) {


        // var url = Fluro.apiURL + '/user/search/' + val;
        // return $http.get(url, {
        //     ignoreLoadingBar: true,
        //     params: {
        //         limit: 20,
        //     }
        // })
        return FluroContent.endpoint('content/persona/search/' + val, true, true).query({
                limit: 20,
        })
        .$promise
        .then(function(results) {

            console.log('RESULTS', results);

           // var results = response.data;
            return _.reduce(results, function(filtered, item) {

                //if(item) {
                    var exists;

                    if($scope.single) {
                        if($scope.model) {
                            exists = ($scope.model && ($scope.model._id == item._id));
                        }
                    } else {
                        exists = _.some($scope.model, {
                            '_id': item._id
                        });
                    }

                    if (!exists) {
                        filtered.push(item);
                    }
                //}

                // console.log('Filtered', filtered);

                return filtered;
            }, [])
        });
    };

});
