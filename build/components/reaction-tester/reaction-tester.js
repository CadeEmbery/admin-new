app.directive('reactionTester', function(Fluro, $http, $rootScope, $compile) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            reaction: '=ngReaction',
            interaction: '=ngInteraction',
        },
        templateUrl: 'reaction-tester/reaction-tester.html',
        link: function($scope, $element, $attrs) {

            // var container = $element.find('#reaction-test-iframe');
            var iframe = $element.find('#reaction-test-iframe')[0];
            console.log('IFrame', iframe);
            var iframeContent = iframe.contentDocument.body;

            $scope.testSend = {

            };


            if ($rootScope.user && $rootScope.user.email) {
                $scope.testSend.email = $rootScope.user.email;
            }

            ///////////////////////////////////////////////

            $scope.send = function() {


                if (!$scope.reaction) {
                    return;
                }

                if (!$scope.interaction) {
                    return;
                }



                /////////////////////////////

                //Endpoint
                var request = $http.post(Fluro.apiURL + '/reaction/email/send/' + $scope.reaction + '/' + $scope.interaction);

                request.then(function(res) {


                    $scope.testSend.result = res;
                }, function(res) {

                    $scope.testSend.result = res;

                });


            }

            ///////////////////////////////////////////////

            $scope.reload = function() {

                // container.contents().empty();
                iframeContent.innerHTML = '';


                // container.empty();

                if (!$scope.reaction) {
                    return;
                }

                if (!$scope.interaction) {
                    return;
                }

                /////////////////////////////

                //Data
                var data = {}
                data.interaction = $scope.interaction;

                /////////////////////////////

                //Endpoint
                var request = $http.post(Fluro.apiURL + '/reaction/email/render/' + $scope.reaction + '/' + $scope.interaction, data);

                request.then(function(res) {
                    //console.log('Request Example', res);
                    // container.html(res.data);
                    iframeContent.innerHTML = (res.data);

                    // container.contents().append(res.data);

                    $compile(iframeContent)($scope);
                }, function(res) {
                    //console.log('Request Example FAILED', res);
                    // container.contents().append('<h5 style="color: #ff0000;">ERROR!</h5><pre>' + res.data + '</pre>');
                    // container.html('<h5 style="color: #ff0000;">ERROR!</h5><pre>' + res.data + '</pre>');
                    // $compile(container.contents())($scope);

                    iframeContent.innerHTML = ('<h5 style="color: #ff0000;">ERROR!</h5><pre>' + res.data + '</pre>');
                    $compile(iframeContent)($scope);
                });

            }



            ///////////////////////////////////////////////

            $scope.$watch('reaction + interaction', $scope.reload);

        }
    };
});