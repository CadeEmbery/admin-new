app.directive('conditionSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-condition-select/admin-condition-select.html',
        controller: 'ConditionSelectController',
    };
});


app.controller('ConditionSelectController', function($scope) {


    if (!$scope.model) {
        $scope.model = [];
    }


    //Create a new object
    $scope._new = {}

    ////////////////////////////////////////////////////

    $scope.remove = function(key) {
        delete $scope.model[key];
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {
        var insert = angular.copy($scope._new);
        if(!$scope.model) {
            $scope.model = {}
        }
        if(insert.key && !$scope.model[insert.key]) {
            $scope.model[insert.key] = insert.value;
            $scope._new = {}
        }
    }

})


