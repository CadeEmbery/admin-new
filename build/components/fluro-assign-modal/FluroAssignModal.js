

app.controller('FluroAssignModalController', function($scope, cancel, Batch, teams) {

	// $scope.reactions = reactions;
	$scope.teams = teams;


	$scope.selected = {};
	$scope.options = {};

	//////////////////////////////////////

	var ids = _.chain(teams)
	.compact()
	.map(function(team) {
		if(team._id) {
			return team._id
		} else {
			return team;
		}
	})
	.value();

	//////////////////////////////////////

	$scope.assign = function() {
		Batch.assignPosition({
			ids:ids,
			title:$scope.selected.title,
			contacts:$scope.selected.contacts,
			// options:$scope.options,
		}, function() {

			//Close the window
			$scope.cancel();
		})
	}
	//////////////////////////////////////




	$scope.cancel = cancel

});
