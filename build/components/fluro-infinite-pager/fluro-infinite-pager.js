/**
 * sample usage 
<any-tag infinite-pager items="items" per-page="18" ng-params="{nextPageOnScroll:true}">
    <any-tag ng-repeat="page in pages"></any-tag>

    <div ng-if="pages.length <= totalPages">
        <i class="fa fa-spin fa-spinner"></i>
        <button ng-click="nextPage()">next page</button>
    </div>

</any-tag>
*/


var getClosest = function ( elem, selector ) {

    // Element.matches() polyfill
    if (!Element.prototype.matches) {
        Element.prototype.matches =
        Element.prototype.matchesSelector ||
        Element.prototype.mozMatchesSelector ||
        Element.prototype.msMatchesSelector ||
        Element.prototype.oMatchesSelector ||
        Element.prototype.webkitMatchesSelector ||
        function(s) {
            var matches = (this.document || this.ownerDocument).querySelectorAll(s),
            i = matches.length;
            while (--i >= 0 && matches.item(i) !== this) {}
                return i > -1;
        };
    }

    // Get closest match
    for ( ; elem && elem !== document; elem = elem.parentNode ) {
        if ( elem.matches( selector ) ) return elem;
    }

    return null;

};



app.directive('infinitePager', function($timeout, $sessionStorage, $state, $window, $document) {
    return {
        restrict: 'A',
        scope:true,
        link: function($scope, $element, $attr) {

            //Get the scroll parent
            var scrollParent = angular.element($window);
            var foundParent = getClosest($element[0], '.scroll-parent');

            //////////////////////////////////////////////////

            if(foundParent) {
                scrollParent = angular.element(foundParent);
            } else {
                foundParent = angular.element('html');
            }

            //////////////////////////////////////////////////

            //16 per page by default
            var perPage = 16;
            
            if($attr.perPage) {
                perPage = parseInt($attr.perPage);
            }

            if($attr.ngParams) {
                var ngParams = $scope.$eval($attr.ngParams);
                if(ngParams.nextPageOnScroll) {
                    bindNextPageOnScroll();
                }
            }

            if($attr.infinitePager) {
                console.log('SET PAGER ID', $attr.infinitePager);
                $scope.pagerID = $attr.infinitePager;
            }

            //////////////////////////////////////////////////

            $scope.pager = {
                currentPage: 0,
                limit: perPage,
            };

            $scope.pages = [];

            //////////////////////////////////////////////////

            function getPagerID() {
                return $state.current.name + '-' + $scope.pagerID;
            }

            //////////////////////////////////////////////////

            // session storage to identify previous page
            if ($sessionStorage.currentPage) {

                //Get the previous position
                if($sessionStorage.currentPage[getPagerID()]) {
                    // make sure pages is created after back button is pressed, before scrolling down
                    $scope.$on('back-resume-scroll', function(event, args) {
                        // load pages
                        var start = $scope.pager.limit;
                        var end = ($sessionStorage.currentPage[getPagerID()] + 1) * $scope.pager.limit;
                        // console.log('on-before-scroll', start, end);
                        
                        //Slice into seperate chunks
                        var sliced = _.slice($scope.allItems, start, end);
                        $scope.pages.push(sliced);

                        // only scroll and assign page when element is ready
                        $element.ready(function(){
                            document.body.scrollTop = args.previousScrollPosition;
                            $scope.pager.currentPage = $sessionStorage.currentPage[getPagerID()];
                            // console.log('scrolled and currentPage updated');
                        })
                    });
                }
            } else {
                $sessionStorage.currentPage = {};
            }

            /////////////////////////////////////////////////////
            
            $scope.$watch($attr.items, function(items) {
                $scope.allItems = items;
                
                if($scope.allItems) {
                    $scope.pages.length = 0;
                    $scope.pager.currentPage = 0;

                    $scope.totalPages = Math.ceil($scope.allItems.length / $scope.pager.limit) - 1;

                    $scope.updateCurrentPage();                    
                }
            });

            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////

            //Update the current page
            $scope.updateCurrentPage = function() {

                if ($scope.allItems.length < ($scope.pager.limit * ($scope.pager.currentPage - 1))) {
                    $scope.pager.currentPage = 0;
                    if(scrollParent) {
                        console.log('Reset scroll')
                        scrollParent.scrollTop(0);
                    }
                }
                
                var start = ($scope.pager.currentPage * $scope.pager.limit);
                var end = start + $scope.pager.limit;

                // console.log('page updated', start, end);

                //Slice into seperate chunks
                var sliced = _.slice($scope.allItems, start, end);
                $scope.pages.push(sliced);
            }

            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////


            var timer;
            
            $scope.nextPage = function() {
                if ($scope.pager.currentPage < $scope.totalPages) {
                    if(timer) {
                        $timeout.cancel(timer);
                    }

                    timer = $timeout(function() {
                        $scope.pager.currentPage = ($scope.pager.currentPage + 1);
                        $sessionStorage.currentPage[getPagerID()] = $scope.pager.currentPage;
                        $scope.updateCurrentPage();
                    });
                } else {
                    // $scope.updateCurrentPage();
                }
            }
            
            /////////////////////////////////////////////////////

            function bindNextPageOnScroll() {



                scrollParent.on("scroll", scrollChange)

                function scrollChange() {



                    var windowElement = scrollParent[0];
                    var contentElement = $element[0];





                    var windowHeight = windowElement.clientHeight;
                    var contentHeight = contentElement.clientHeight;

                    var scrollTop = windowElement.scrollTop;
                    var scrollLimit = windowElement.scrollHeight - windowHeight;

                    var buffer = windowHeight/2;

                    // console.log('scroll change', scrollLimit - scrollTop, buffer);
                    if((scrollLimit - scrollTop) < buffer) {
                       $scope.nextPage();
                   }
                    // var windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
                    // var body = document.body, html = document.documentElement;
                    // var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
                    // windowBottom = windowHeight + window.pageYOffset;
                    // if (windowBottom >= docHeight) {
                    //     $scope.nextPage();
                    // }
                };


                $scope.$on('$destroy', function() {
                    scrollParent.off('scroll', scrollChange);
                })
            }





        }
    };
    
})