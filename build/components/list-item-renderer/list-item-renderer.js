app.directive('listItemRenderer', function($state, SearchService) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            item: '=ngModel',
        },
        templateUrl: 'list-item-renderer/list-item-renderer.html',
        link: function($scope, $element, $attrs) {

            $scope.view = function(item) {

                var type = item._type;
                var definition = item.definition;
                var definedName = definition || type;

                $state.go(type + '.view', {
                    id: item._id,
                    definitionName: definedName
                });

                SearchService.close();
            }

            /////////////////////////////////////////////

            $scope.edit = function(item) {

                console.log('Edit', item);
                var type = item._type;
                var definition = item.definition;
                var definedName = definition || type;

                // if(definition && definition.length) {
                    // $state.go(type + '.custom', {
                        // id: item._id,
                        // definitionName: definedName
                    // });
                // } else {

                    // console.log('Go to basic edit');
                    var promise = $state.go(type + '.edit', {
                        id: item._id,
                        definitionName: definition
                    });

                    promise.then(function() {
                        console.log('Promise resolved!')
                    SearchService.close();
                    })
                // }

               
                
            }


            /////////////////////////////////////////////

            $scope.colorStyle = {};



            var color = _.find($scope.item.realms, function(realm) {
                return realm.bgColor || realm.color;
            });


            if($scope.item._type == 'realm') {
                color = $scope.item;
            }

            if (color) {
                $scope.colorStyle['backgroundColor'] = color.bgColor;
                $scope.colorStyle['color'] = color.color;
                $scope.colorStyle['boxShadow'] = 'none';
            }



            // $scope.showActions = function($event) {
            //     console.log('Stop propagation!!');
            //     $event.preventDefault();
            //     $event.stopPropagation();
            //     ActionService.show($scope.item);
            // }
        }
    };
});