app.directive('processCircles', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'process-circles/process-circles.html',
        link: function($scope, $element, $attrs) {

            $scope.circles = _.chain($scope.model.states)
                .map(function(state) {

                    if (state.style == 'waiting') {
                        return;
                    }

                    switch (state.result) {
                        case 'complete':
                            state.icon = 'fas fa-check-circle brand-success';
                            break;
                        case 'failed':
                            state.icon = 'fas fa-exclamation-circle brand-danger';
                            break;
                        case 'pending':
                            state.icon = 'fas fa-minus-circle brand-warning';
                            break;
                        default:
                            //If its the current state then add it in a circle
                            if ($scope.model.state == state.key) {
                                state.icon = 'far fa-dot-circle text-muted';
                            } else {
                                state.icon = 'far fa-circle text-muted';
                            }
                            break;
                    }
                    return state;
                })
                .compact()
                .value()


        }
    };
});