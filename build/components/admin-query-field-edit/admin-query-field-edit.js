app.directive('queryFieldEdit', function() {

    return {
        restrict: 'E',
        require:'^queryFieldEditor',
        replace: true,
        scope: {
            model: '=ngModel',
            mode: '@ngMode',
        },
        templateUrl: 'admin-query-field-edit/admin-query-field-edit.html',
        controller: 'QueryFieldEditController',
        link:function($scope, $element, $attrs, ctrl) {
            $scope.manager = ctrl;

        }
    };
});


app.controller('QueryFieldEditController', function($scope) {


    /*
    $scope.listController= $scope.$parent.$parent.$parent.$parent;
    $scope.canCreate = $scope.listController.canCreate;

    ///////////////////////////////////////////////

    $scope.create = function(initDate) {
        console.log('Create at', initDate)
        $state.go('event.create', {initDate:initDate});
    }


    

    //////////////////////////////////////////

    $scope.resetToToday = function() {
        $scope.browseDate = new Date();
    }

    $scope.$watch('model', function(data) {
        $scope.dateList = _.groupBy(data, function(item) {
            var itemDate = new Date(item.startDate);
            itemDate.setHours(0);
            itemDate.setMinutes(0);
            itemDate.setSeconds(0);
            itemDate.setMilliseconds(0);
            return itemDate.getTime();
        });
    })
*/

});