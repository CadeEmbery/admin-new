app.directive('mailoutHelp', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-mailout-help/admin-mailout-help.html',
        controller: 'MailoutHelpController',
    };
});


app.controller('MailoutHelpController', function($scope) {

    ///////////////////////////////////////////////////////


    function getFlattenedFields(array, trail) {

        return _.chain(array).map(function(field, key) {
                if (field.type == 'group') {

                    if (field.asObject) {

                        //Push the field key
                        if (field.maximum != 1) {
                            trail.push(field.key + '[i]');
                        } else {
                            trail.push(field.key);
                        }
                    }

                    // console.log('Go down', field.key);
                    var fields = getFlattenedFields(field.fields, trail);

                    trail.pop();
                    //console.log('Go back up')
                    return fields;
                } else {

                    //Push the field key
                    if (field.maximum != 1) {
                        trail.push(field.key + '[i]');
                    } else {
                        trail.push(field.key);
                    }

                    field.trail = trail.slice();
                    trail.pop();

                    // console.log(field.title, trail);
                    return field;
                }
            })
            .flatten()
            .value();
    }



    $scope.$watch('model.fields', function(fields) {

        console.log('fields changed');
        //Get all the flattened fields
        var flattened = getFlattenedFields(fields, []);


        $scope.availableFields = _.map(flattened, function(field) {

            var isAsset;
            var isImage;

            if (field.type == 'reference') {
                if (field.params.restrictType && field.params.restrictType) {
                    switch (field.params.restrictType) {
                        case 'image':
                            isImage = true;
                        case 'asset':
                        case 'audio':
                        case 'video':
                            isAsset = true;
                            break;
                    }
                }
            }

            ///////////////////////////////////////

            return {
                title: field.title,
                path: field.trail.join('.'),
                type: field.type,
                array: (field.maximum != 1),
                isAsset: isAsset,
                isImage: isImage,
            }
        });
    }, true)

})