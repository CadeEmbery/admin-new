app.directive('assignmentSummary', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            // model: '=ngModel',
            event: '=ngEvent',
        },
        templateUrl: 'admin-assignment-summary/admin-assignment-summary.html',
        controller: 'AdminAssignmentSummaryController',
    };
});

app.controller('AdminAssignmentSummaryController', function($scope, Notifications, $rootScope, $timeout, $q, ModalService, FluroContent) {


    $scope.$watch('event', function() {
        if (!$scope.event.assignmentSlots || !$scope.event.assignmentSlots.length) {
            $scope.event.assignmentSlots = [];
        }

        ////////////////////////////////////////////////////

        if (!$scope.event.assigned) {
            $scope.event.assigned = [];
        }

        if (!$scope.event.temporaryAssignments) {
            $scope.event.temporaryAssignments = [];
        }
    });

    $scope.settings = {};

    ////////////////////////////////////////////////////

    $scope.proposed = {}

    ////////////////////////////////////////////////////

    $scope.matchPeriod = function(assignment) {

    }

    ////////////////////////////////////////////////////

    $scope.toggle = function(group) {

        if ($scope.settings.expanded == group.title) {
            $scope.settings.expanded = null;
        } else {
            $scope.settings.expanded = group.title
        }
    }

    ////////////////////////////////////////////////////

    $scope.getSlotAssignments = function(name) {
        // console.log('Get assignments', name)
        return _.filter($scope.assignments, function(assignment) {
            var correctAssignment = assignment.title == name;
            var active = (assignment.status == 'active');


            if($scope.event.status == 'archived') {
                active = true;
            }


            return correctAssignment && active && assignment.contact;

        });
    }

    ////////////////////////////////////////////////////

    $scope.nudging = false;
    $scope.nudgeAll = function() {
        $scope.nudging = true;


        var ids = _.map($scope.assignments, function(assignment) {
            return assignment._id;
        })


        if (!ids.length) {
            $scope.nudging = false;
        }

        FluroContent.endpoint('assignments/nudge').save({
                assignments: ids
            })
            .$promise
            .then(function(res) {
                $scope.nudging = false;

                console.log('Nudged everyone!', res);

                if (res.success.length) {
                    Notifications.status(res.success.length + ' nudge notifications sent');
                } else {
                    Notifications.warning('No nudges were sent');
                }

            }, function(err) {
                $scope.nudging = false;
                console.log('Error', err);

                Notifications.error(err.message);
            })
    }

    ////////////////////////////////////////////////////

    $scope.editAssignment = function(assignment) {

        if (assignment.startDate) {
            return;
        }


        if (assignment.confirmationStatus == 'temporary') {
            return _.pull($scope.event.temporaryAssignments, assignment);
        }

        var itemObject = angular.copy(assignment);


        itemObject._type = 'assignment';
        itemObject.realms = $scope.event.realms;


        console.log('EDIT ASSIGNMENT', itemObject)

        // //console.log('MODAL EDIT', itemObject);
        ModalService.edit(itemObject, function(res) {

            var found = _.find($scope.event.assigned, {
                _id: res._id
            });
            //Update the status of the assignment so it removes from the list
            _.assign(found, res);

            $timeout(function() {

                refreshList();
            });
        });
    }


    ////////////////////////////////////////////////////

    $scope.removeAssignment = function(assignment) {

        console.log('Remove assignment!', assignment);

        if (assignment.confirmationStatus == 'temporary') {
            console.log('Pull Assignment');
            _.pull($scope.event.temporaryAssignments, assignment);
            return refreshList();
        }

        //Mark as unavailable
        if (assignment.confirmationStatus == 'denied') {
            console.log('Hide the assignment')
            return $scope.hideAssignment(assignment);
        }

        var itemObject = angular.copy(assignment);
        itemObject._type = 'assignment';
        itemObject.realms = $scope.event.realms;

        //console.log('MODAL REMOVe', itemObject);
        ModalService.remove(itemObject, function(res) {
            //console.log('WOOOOT, refresh the assignment', $scope.event.assigned, assignment);

            var found = _.find($scope.event.assigned, {
                _id: assignment._id
            });

            //console.log('PULL FOUND', found);
            _.pull($scope.event.assigned, found);
            $timeout(function() {

                refreshList();
            });
        })

    }

    ////////////////////////////////////////////////////

    $scope.canEditAssignment = function(assignment) {
        assignment._type = 'assignment';
        return $rootScope.access.canEditItem(assignment);
    }

    $scope.canDeleteAssignment = function(assignment) {
        assignment._type = 'assignment';
        return $rootScope.access.canDeleteItem(assignment);
    }

    ////////////////////////////////////////////////////

    $scope.hideAssignment = function(assignment) {

        console.log('Hide the assignment please', assignment);

        FluroContent.resource('assignment/' + assignment._id).update({
                status: 'archived',
            })
            .$promise
            .then(function(res) {

                //console.log('Mark status of assignment');
                var found = _.find($scope.event.assigned, {
                    _id: res._id
                });
                //Update the status of the assignment so it removes from the list
                found.status = 'archived';

                $timeout(function() {

                    refreshList();
                });


            }, function(err) {
                console.log('ERROR hiding assignment', err)
            })
    }

    ////////////////////////////////////////////////////

    $scope.addProposedAssignments = function() {

        //Add the assignments
        $scope.addAssignments($scope.proposed.title, $scope.proposed.contacts);

        //And reset the proposed
        $scope.proposed = {};
    }

    ////////////////////////////////////////////////////

    $scope.addAssignments = function(assignmentName, contacts) {
        console.log('Add assignments to temporary')

        //Map the requests
        var assignments = _.map(contacts, function(contact) {

            var data = {
                title: assignmentName,
                contact: contact,
                confirmationStatus: 'temporary',
                status: 'active',
            }

            return data;
        });

        // console.log('Adding', assignments.length, 'to temporary', $scope.event.temporaryAssignments);

        var concatenated = $scope.event.temporaryAssignments.concat(assignments);

        $scope.event.temporaryAssignments = _.chain(concatenated).filter(function(assignment) {

                var contactID = assignment.contact;
                if (contactID._id) {
                    contactID = contactID._id;
                }

                //Check in existing assignments
                var match = _.find($scope.event.assigned, function(ass) {

                    var assignedContactID = ass.contact;
                    if (assignedContactID._id) {
                        assignedContactID = assignedContactID._id;
                    }

                    var correctContact = (assignedContactID == contactID);
                    var correctTitle = (ass.title == assignment.title);

                    //console.log('GOT IT?', correctContact, correctTitle, '---', assignedContactID, contactID)

                    return (correctContact && correctTitle);
                });

                if (match) {
                    //console.log('MATCH')
                    return false;
                } else {
                    return true;
                }


            })
            .uniq(function(assignment) {
                var contactID = assignment.contact;
                if (contactID._id) {
                    contactID = contactID._id;
                }

                return assignment.title + ' ' + contactID;
            })
            .value();


        console.log('UPDATE TEMPORARY')
    };

    ////////////////////////////////////////////////////

    // $scope.saveAssignments = function() {

    //     //console.log('Add assignments')

    //     //Map the requests
    //     var requests = _.map($scope.event.temporaryAssignments, function(data) {
    //         return FluroContent.endpoint('assignments/' + $scope.event._id).save(data).$promise;
    //     });

    //     //////////////////////////////

    //     $q.all(requests)
    //     .then(function(res) {
    //         //console.log('RESULT', res);
    //         $scope.proposed = {};
    //     }, function(err) {
    //         //console.log('ERROR', err);
    //     });
    // }

    ////////////////////////////////////////////////////

    $scope.selectTeam = function() {

        var selection = {};
        var modal = ModalService.browse('team', selection);
        modal.result.then(addTeams, addTeams)

        function addTeams() {

            if (selection.items && selection.items.length) {


                // var slots = _.chain(selection.items)
                // .map(function(team) {
                //     return team.assignments;
                // })
                // .flatten()
                // .value();

                ////////////////////////////////////////////////////



                ////////////////////////////////////////////////////

                var assignments = _.chain(selection.items)
                    .map(function(team) {
                        return team.assignments;
                    })
                    .flatten()
                    .map(function(assignment) {

                        ////////////////////////////////////////////

                        var assignmentTitle = assignment.title;

                        var alreadySlotted = _.find($scope.event.assignmentSlots, {
                            title: assignmentTitle
                        });

                        if (!alreadySlotted) {
                            $scope.event.assignmentSlots.push({
                                title: assignmentTitle
                            })
                        }

                        ////////////////////////////////////////////

                        return _.map(assignment.contacts, function(contact) {
                            return {
                                contact: contact,
                                assignment: assignment
                            }
                        });
                    })
                    .flatten()
                    .map(function(item) {


                        var obj = {
                            title: item.assignment.title,
                            contact: item.contact,
                            confirmationStatus: 'temporary',
                            status: 'active',
                        }

                        return obj;
                    })
                    .value();

                ////////////////////////////////////////////////////

                var concatenated = $scope.event.temporaryAssignments.concat(assignments);

                ////////////////////////////////////////////////////

                $scope.event.temporaryAssignments = _.chain(concatenated)
                    .filter(function(assignment) {

                        var contactID = assignment.contact;
                        if (contactID._id) {
                            contactID = contactID._id;
                        }

                        //Check in existing assignments
                        var match = _.find($scope.event.assigned, function(ass) {

                            var assignedContactID = ass.contact;
                            if (assignedContactID._id) {
                                assignedContactID = assignedContactID._id;
                            }

                            var correctContact = (assignedContactID == contactID);
                            var correctTitle = (ass.title == assignment.title);

                            // //console.log('GOT IT?', correctContact, correctTitle, '---', assignedContactID, contactID)

                            return (correctContact && correctTitle);
                        });

                        if (match) {
                            //console.log('MATCH')
                            return false;
                        } else {
                            return true;
                        }
                    })
                    .uniq(function(assignment) {
                        var contactID = assignment.contact;
                        if (contactID._id) {
                            contactID = contactID._id;
                        }

                        return assignment.title + ' ' + contactID;
                    })
                    .value();


            }
        }
    }

    ////////////////////////////////////////////////////

    $scope.removeSlot = function(slot) {
        _.pull($scope.event.assignmentSlots, slot);
    }

    ////////////////////////////////////////////////////

    $scope.getClass = function(assignment) {

        switch (assignment.confirmationStatus) {
            case 'confirmed':
                return 'confirmed';
                break;
            case 'denied':
                return 'danger';
                break;
            case 'temporary':
                return 'warning';
                break;
        }


        var matchPeriod = _.find($scope.event.unavailable, function(period) {
            var contactID = period.contact;
            if (contactID._id) {
                contactID = contactID._id;
            }

            return contactID == assignment.contact._id;

        })

        if (matchPeriod) {
            return 'danger';
        }
    }

    ////////////////////////////////////////////////////

    $scope.addToSlot = function(slot) {
        if (!slot || !slot.title) {
            return console.log('Invalid slot');
        }

        var selection = {};
        var modal = ModalService.browse('contact', selection, {});

        //Close
        function closed() {
            console.log('Selected contacts', selection.items);
            if (selection.items.length) {
                $scope.addAssignments(slot.title, selection.items);
            }
        }

        //Close of modal
        modal.result.then(closed, closed)
    }

    ////////////////////////////////////////////////////

    $scope.$watch('event.assigned', refreshList);
    $scope.$watch('event.temporaryAssignments', refreshList);

    ////////////////////////////////////////////////////

    function refreshList() {

        console.log('REFRESH LIST')
        var assignments = [];
        assignments = assignments.concat($scope.event.assigned);
        assignments = assignments.concat($scope.event.temporaryAssignments);

        // console.log('EVENT ASSIGNMENTS', assignments, $scope.event.assigned, $scope.event.temporaryAssignments)
        //////////////////////////////////////////////////////

        $scope.groupedByAssignment = _.chain(assignments)
            .filter(function(assignment) {

                if($scope.event.status == 'archived') {
                    return true;
                } else {
                    return (assignment.status == 'active');
                }
            })
            // .sortBy('title')
            .reduce(function(results, assignment) {

                //Get existing
                var existing = _.find(results, {
                    title: assignment.title
                });

                var alreadySlotted = _.find($scope.event.assignmentSlots, {
                    title: assignment.title
                });

                if (!alreadySlotted) {
                    $scope.event.assignmentSlots.push({
                        title: assignment.title
                    })
                }

                if (!existing) {
                    var object = {
                        title: assignment.title,
                        status: 'unknown',
                        assignments: [],
                    }

                    existing = object;
                    // results[assignment.title] = existing;
                    results.push(existing);
                }

                //Add the assignment
                existing.assignments.push(assignment);



                return results;
            }, [])
            .map(function(group) {

                group.summary = _.groupBy(group.assignments, 'confirmationStatus');


                ///////////////////////////////////////////////////

                group.status = 'unknown';



                if (group.summary.confirmed && (group.summary.confirmed.length == group.assignments.length)) {
                    group.status = 'confirmed';
                }

                if (group.summary.denied && group.summary.denied.length) {
                    group.status = 'denied';
                }


                return group;
            })
            .value();

        $scope.groupedByStatus = _.groupBy(assignments, function(assignment) {
            if (assignment.confirmationStatus) {
                return assignment.confirmationStatus;
            } else {
                return 'unknown';
            }
        })

        $scope.unavailable = [];

        var cancelledAssignments = _.chain(assignments)
            .filter(function(assignment) {
                return (assignment.confirmationStatus == 'denied');
            })
            .reduce(function(results, assignment) {

                var contactID = assignment.contact;
                if (contactID._id) {
                    contactID = contactID._id;
                }

                var existing = _.find(results, {
                    _id: contactID
                });

                if (!existing) {
                    existing = {
                        _id: contactID,
                        title: assignment.contact.title,
                        // title:assignment.title,
                        reasons: [],
                        assignments: [],
                    }

                    results.push(existing);
                }


                // if(_.some(existing.reasons, {title:assignment.title})) {
                //Add the reason

                var description = 'unavailable';
                if (assignment.description) {
                    description = assignment.description;
                }

                assignment.readable = description;
                existing.assignments.push(assignment);
                // existing.reasons.push(assignment.title +', ' + description);

                return results;
            }, $scope.unavailable)
            .sortBy(function(contact) {
                return contact.title;
            })
            .value();

        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////


        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////

        //Check any unavailable people 
        var absentContacts = _.reduce($scope.event.unavailable, function(results, period) {

            var contactID = period.contact;
            if (contactID._id) {
                contactID = contactID._id;
            }

            var existing = _.find(results, {
                _id: contactID
            });

            if (!existing) {
                existing = {
                    _id: contactID,
                    title: period.contact.title,
                    // title:assignment.title,
                    reasons: [],
                    assignments: [],
                }

                results.push(existing);
            }

            var description = 'unavailable';
            if (period.description) {

                var startDate = new Date(period.startDate);
                var endDate = new Date(period.endDate);
                description = period.description + ' - ' + startDate.format('j M') + ' - ' + endDate.format('j M');
            }
            period.readable = description;
            existing.assignments.push(period);

            // existing.reasons.push(assignment.title +', ' + description);


            // return {
            //     title:period.contact.title,
            //     reasons:[period.description],
            // };

            return results;

        }, $scope.unavailable);

        //Include the unavailable contacts in the list
        // $scope.unavailable = [].concat(cancelledAssignments, absentContacts);
        $scope.assignments = assignments;




    }

})