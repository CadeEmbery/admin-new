app.directive('duplicateManager', function() {

    return {
        restrict: 'E',
        replace: true,
        // scope: {
        //     model: '=ngModel',
        //     filterParents: '=ngParents',
        // },
        templateUrl: 'admin-duplicate-manager/admin-duplicate-manager.html',
        controller: 'AdminDuplicateManagerController',
    };
});

/////////////////////////////////////////////////////////////

app.controller('AdminDuplicateManagerController', function($scope, $stateParams, FluroStorage, CacheManager, $filter, $state, Notifications, FluroContent) {


    $scope.$stateParams = $stateParams;

    //////////////////////////////

    //Get Local and session storage
    var local;
    var session;

    if ($scope.definition) {
        local = FluroStorage.localStorage($scope.definition.definitionName)
        session = FluroStorage.sessionStorage($scope.definition.definitionName)
    } else {
        local = FluroStorage.localStorage($scope.type.path)
        session = FluroStorage.sessionStorage($scope.type.path)
    }

    //////////////////////////////

    //Setup Pager    
    if (!session.duplicatePager) {
        $scope.duplicatePager =
            session.duplicatePager = {
                limit: 20,
                maxSize: 8,
            }
    } else {
        $scope.duplicatePager = session.duplicatePager;
    }



    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////

    //Move to status
    function divide(ids) {
        var details = {
            ids: ids,
        }

        function divideComplete(res) {

            CacheManager.clear('contact');

            ///////////////////////

            //Reload the state
            $state.reload();

            ///////////////////////

            Notifications.status('Divided ' + details.ids.length + ' contacts into individuals');

            //Deselect all
            $scope.selection.deselectAll();
        }

        function divideFailed(res) {
            return Notifications.error(res.data);
        }

        var request = FluroContent.endpoint('contact/divide').save(details).$promise.then(divideComplete, divideFailed);
    }


    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////

    //Move to status
    function divideFamilies(ids) {
        var details = {
            ids: ids,
        }

        function divideComplete(res) {


            CacheManager.clear('family');

            ///////////////////////

            //Reload the state
            $state.reload();

            ///////////////////////

            Notifications.status('Divided ' + details.ids.length + ' families into individuals');

            //Deselect all
            $scope.selection.deselectAll();

        }

        function divideFailed(res) {
            return Notifications.error(res.data);
        }

        var request = FluroContent.endpoint('family/divide').save(details).$promise.then(divideComplete, divideFailed);
    }

    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////

    //Move to status
    function merge(ids, callback) {
        var details = {
            ids: ids,
        }

        if (!callback) {
            callback = function() {

            }
        }

        function mergeComplete(res) {

            if ($scope.merging) {
                return callback(null, res);
            }

            ///////////////////////

            CacheManager.clear('contact');

            ///////////////////////

            //Reload the state
            $state.reload();

            ///////////////////////

            Notifications.status('Merged ' + details.ids.length + ' contacts for ' + res.title);

            //Deselect all
            $scope.selection.deselectAll();

            return callback(null, res);

        }

        function mergeFailed(err) {
            if (!$scope.merging) {
                Notifications.error(err.data);
            }
            return callback(err);
        }

        var request = FluroContent.endpoint('contact/merge').save(details).$promise.then(mergeComplete, mergeFailed);
    }


    /////////////////////////////////////

    //Move to status
    function mergeFamilies(ids, callback) {
        var details = {
            ids: ids,
        }

        if (!callback) {
            callback = function() {

            }
        }

        function mergeComplete(res) {

            if ($scope.merging) {
                return callback(null, res);
            }

            ///////////////////////

            CacheManager.clear('family');

            ///////////////////////

            //Reload the state
            $state.reload();

            ///////////////////////

            Notifications.status('Merged ' + details.ids.length + ' families into ' + res.title);

            //Deselect all
            $scope.selection.deselectAll();
            return callback(null, res);

        }

        function mergeFailed(err) {

            if (!$scope.merging) {
                Notifications.error(err.data);
            }
            return callback(err);
        }

        var request = FluroContent.endpoint('family/merge').save(details).$promise.then(mergeComplete, mergeFailed);
    }

    ////////////////////////////////////////
    ////////////////////////////////////////
    ////////////////////////////////////////


    $scope.toggleSelection = function(item, group) {

        var selectedIds = $scope.selection.ids;

        //Check if theres any items selected outside of the same group as this item
        var outsideOfGroupSelected = _.any(selectedIds, function(id) {
            return !_.some(group, {
                _id: id
            });
        })

        //If there are then clear the selection
        if (outsideOfGroupSelected) {
            $scope.selection.deselectAll();
        }

        //Toggle item
        $scope.selection.toggle(item);

    }

    ////////////////////////////////////////

    $scope.addressKey = function(item) {
        return _.compact([
            _.get(item, 'address.addressLine1'),
            _.get(item, 'postalAddress.addressLine1')
        ]).join('');
    }


    ////////////////////////////////////////

    $scope.summary = function(item) {

        if ($scope.type.path == 'family') {
            var addressDetails = [
                _.get(item, 'address.addressLine1'),
                _.get(item, 'address.suburb'),
                _.get(item, 'address.state'),
                _.get(item, 'address.postalCode'),
                _.get(item, 'address.country'),
            ];

            var postalAddressDetails = [
                _.get(item, 'postalAddress.addressLine1'),
                _.get(item, 'postalAddress.suburb'),
                _.get(item, 'postalAddress.state'),
                _.get(item, 'postalAddress.postalCode'),
                _.get(item, 'postalAddress.country'),
            ];


            ////////////////////////////////////////////

            var addressString = _.chain(addressDetails)
                .flattenDeep()
                .compact()
                .value()
                .join(', ');

            ////////////////////////////////////////////

            var postalString = _.chain(postalAddressDetails)
                .flattenDeep()
                .compact()
                .value()
                .join(', ');


            ////////////////////////////////////

            var phoneNumbers = _.chain(item.phoneNumbers)
                .map(function(number) {
                    return 'ph:' + number;
                })
                .compact()
                .value()
                .join(', ');

            ////////////////////////////////////

            var emails = _.chain(item.emails)
                .map(function(number) {
                    return 'email:' + number;
                })
                .compact()
                .value()
                .join(', ');

            //////////////////////////////////////////////////////

            var useAddressString;

            if (postalString.length) {
                useAddressString = postalString
            } else {
                useAddressString = addressString;
            }

            //////////////////////////////////////////////////////

            if (useAddressString && useAddressString.length) {
                return useAddressString;
            }

            //////////////////////////////////////////////////////

            var pieces = [];
            pieces.push(phoneNumbers);
            pieces.push(emails);

            //////////////////////////////////////////////////////

            return _.compact(pieces).join(', ').trim();

        } else {

            var pieces = [];

            var age;
            if (item.dob) {
                var age = $filter('age')(item.dob) + 'yrs';
            }

            pieces.push(_.startCase(item.definition) || _.startCase(item._type));
            pieces.push(item.gender);

            pieces.push(age);
            pieces.push(_.get(item, 'phoneNumbers'));
            pieces.push(_.get(item, 'emails'));
            pieces.push(_.get(item, 'family.address.addressLine1'));
            pieces.push(item.timezone);

            // pieces = pieces.concat(item.emails);



            return _.chain(pieces)
                .flatten()
                .compact()
                .join(', ').trim();
        }
    }
    ////////////////////////////////////////
    ////////////////////////////////////////
    ////////////////////////////////////////

    $scope.selectGroup = function(group) {
        $scope.selection.deselectAll();
        $scope.selection.selectMultiple(group);
    }

    $scope.notSelected = function(group) {
        return !$scope.selectedInGroup(group); //$scope.selection.containsAny(group);
    }


    $scope.selectedInGroup = function(group) {
        // var hasSelected = $scope.selection.containsAny(group);
        return $scope.selection.containsAny(group) && $scope.selection.length > 1;

    }

    ////////////////////////////////////////

    $scope.divideSelected = function(group) {
        $scope.divideGroup($scope.selection.items);
    }

    $scope.divideGroup = function(group) {

        /////////////////////////////////////

        var ids = _.map(group, function(item) {
            return item._id;
        })

        /////////////////////////////////////

        if ($scope.type.path == 'family') {
            divideFamilies(ids);
        } else {
            divide(ids);
        }
        //$scope.selection.selectMultiple(group);
    }

    ////////////////////////////////////////

    $scope.mergeSelected = function(group) {
        $scope.mergeGroup($scope.selection.items);
    }

    $scope.mergeGroup = function(group) {

        /////////////////////////////////////

        var ids = _.map(group, function(item) {
            return item._id;
        })

        /////////////////////////////////////

        if ($scope.type.path == 'family') {
            mergeFamilies(ids);
        } else {
            merge(ids);
        }
        //$scope.selection.selectMultiple(group);
    }

    ////////////////////////////////////////

    $scope.toggleMergeAll = function() {
        if ($scope.merging) {
            $scope.cancelMergeAll();
        } else {
            $scope.mergeAll();
        }
    }
    ////////////////////////////////////////

    $scope.cancelMergeAll = function() {
        console.log('Cancel the merge')
        $scope.merging = false;
        $scope.updateDuplicatePage();
    }


    ////////////////////////////////////////

    function getDifferences(array, key) {
        return _.chain(array)
            .map(function(item) {
                var value = _.get(item, key);

                if (!value) {
                    return;
                }

                return String(value);
            })
            .compact()
            .uniq()
            .value();
    }

    ////////////////////////////////////////

    function isSame(array, key) {
        return (getDifferences(array, key).length <= 1);
    }

    ////////////////////////////////////////

    function isSameGender(array) {
        var differences = getDifferences(array, 'gender');


        var validDifferences = _.chain(differences)
            .compact()
            .filter(function(string) {
                return string != 'unknown';
            })
            .value();

        return validDifferences.length <= 1;
    }


    ////////////////////////////////////////

    function isSameDOB(array) {

        //Get all the different verified birthdays
        var differences = _.chain(array)
            .map(function(contact) {

                if (!contact.dob) {
                    return;
                }

                return moment(contact.dob).format('DD/MM/YYYY');
            })
            .compact()
            .uniq()
            .value();

        return differences.length <= 1;
    }

    ////////////////////////////////////////

    function isDifferent(array, key) {
        return (getDifferences(array, key).length > 1);
    }

    ////////////////////////////////////////
    ////////////////////////////////////////
    ////////////////////////////////////////

    function isSimilar(array, key) {
        var differences = getDifferences(array, key);

        var compareTo = differences.pop();
        var scores = _.map(differences, function(string) {
            return [string, similarity(string, compareTo)]
        });
    }

    ////////////////////////////////////////
    ////////////////////////////////////////

    function splitStringInHalf(str) {
        var middle = Math.ceil(str.length / 2);
        var s1 = str.slice(0, middle);
        var s2 = str.slice(middle);
        return [s1, s2];
    };


    function isEven(num) {
        return num % 2 === 0;
    }


    ////////////////////////////////////////

    function isLikelyFamiliesAreTheSame(families) {

        var summaries = _.chain(families)
            .map(function(family) {
                return family.summary.toLowerCase();
            })
            .compact()
            .uniq()
            .value()

        ///////////////////////////////////// 

        var mappedAddressKeys = _.chain(families)
            .map(function(family) {
                return family.addressKey;
            })
            .uniq()
            .value();

        if (mappedAddressKeys.length == 1) {
            //This is a good one
            console.log('Should merge address', mappedAddressKeys);
            return true;
        }

        ///////////////////////////////////// 

        var mappedSummaryKeys = _.chain(families)
            .map(function(family) {
                return family.summaryKey;
            })
            .uniq()
            .value();

        if (mappedSummaryKeys.length == 1) {
            //This is a good one
            console.log('Should merge summary', mappedSummaryKeys);
            return true;
        }

        ///////////////////////////////////// 

        // if (isEven(summaries.length)) {

        //     //Join the strings together
        //     var concatString = summaries.join('');

        //     //Split them in half
        //     var halves = splitStringInHalf(concatString);

        //     //Get a score from the levenschtein algorithm
        //     var score = levDist(halves[0], halves[1]);

        //     //Because the more summaries jammed together the more chance of summaries being similar
        //     //we want to make it harder for large groups of duplicates to be judged as the same,
        //     //eg. 15 items with Sydney NSW in them but a couple of short suburbs different might be considered
        //     //the same with the algorithm above
        //     var measureScore = (concatString.length / summaries.length) / score;

        //     //Any score greater than two
        //     if(measureScore > 2) {
        //         //It's most likely the same address

        //         console.log('MERGING', measureScore, summaries);
        //         return true;
        //     }
        // }

        // return false;

        // ///////////////////////////////////// 

        if (summaries.length == 2) {

            var first = summaries[0];
            var second = summaries[1];

            var score = levDist(first, second);
            var longest = Math.max(first.length, second.length);
            var percentage = Math.ceil(100 - ((score / longest) * 100));

            if (percentage > 72) {
                return true;
            }
        }

        /////////////////////////////////////////

        var scores = LevArray(summaries, summaries[0]);
        if (scores.length == 1 && scores[0].l == 0) {
            return true;
        }

        ///////////////////////////////////// 

        //Check if there are big differences
        var sameSuburb = isSame(families, 'address.suburb');
        var sameState = isSame(families, 'address.state');
        var sameCountry = isSame(families, 'address.country');
        var samePostalCode = isSame(families, 'address.postalCode');
        var sameFirstLine = isSame(families, 'firstLine');

        // var similarAddress = isSimilar(families, 'address.addressLine1');
        //Don't merge if there are differences

        ///////////////////////////////////// 

        var array = [
            sameFirstLine,
            sameSuburb,
            sameState,
            sameCountry,
            samePostalCode,
        ]

        ///////////////////////////////////// 

        var isLikely = _.every(array);
        return isLikely;

    }

    function isLikelyContactsAreTheSame(contacts) {

        //Check if there are big differences
        var sameDob = isSameDOB(contacts);
        var sameGender = isSameGender(contacts, 'gender');
        var sameTimezone = isSame(contacts, 'timezone');


        // var similarAddress = isSimilar(families, 'address.addressLine1');
        //Don't merge if there are differences

        var array = [
            sameDob,
            sameGender,
            sameTimezone,
        ]

        var isLikely = _.every(array);

        //                 console.log(contacts[0].title, array, isLikely);

        // return;
        return isLikely;



    }

    ////////////////////////////////////////

    $scope.mergeAll = function() {

        if ($scope.merging) {
            console.log('Already merging all')
            return;
        }

        ////////////////////////////////////////////

        $scope.merging = true;

        ////////////////////////////////////////////

        var segmented = _.chain($scope.duplicates)
            .reduce(function(results, duplicateSet) {

                var isLikely;
                switch ($scope.type.path) {
                    case 'family':
                        isLikely = isLikelyFamiliesAreTheSame(duplicateSet.items)
                        break;
                    case 'contact':
                        isLikely = isLikelyContactsAreTheSame(duplicateSet.items)
                        break;
                }


                if (isLikely) {
                    duplicateSet.unlikely = false;
                    results.likely.push(duplicateSet);
                } else {
                    duplicateSet.unlikely = true;
                    results.unlikely.push(duplicateSet);
                }

                return results;
            }, {
                likely: [],
                unlikely: []
            })
            .value();


        ////////////////////////////////////////////

        var count = segmented.likely.length;

        ////////////////////////////////////////////

        async.eachSeries(segmented.likely, function(duplicateSet, next) {

            if (!$scope.merging) {
                return next('Merging stopped mid process');
            }

            /////////////////////////////////////

            //Mark as merging
            duplicateSet.merging = true;

            /////////////////////////////////////

            var ids = _.map(duplicateSet.items, function(item) {
                return item._id;
            })

            /////////////////////////////////////

            function callback(err, result) {
                console.log('Merged item');
                if (err) {
                    duplicateSet.merging = false;
                } else {
                    duplicateSet.merged = true;
                }

                $scope.updateDuplicatePage();

                return next(null, result);
            }

            /////////////////////////////////////

            if ($scope.type.path == 'family') {
                return mergeFamilies(ids, callback);
            } else {
                return merge(ids, callback);
            }
        }, allMergeComplete);

        ////////////////////////////////////////////

        function allMergeComplete(err, results) {

            console.log('ALL Merges are complete')
            $scope.merging = false;
            if (err) {
                return Notifications.error(err);
            }

            ///////////////////////


            Notifications.status('Merged ' + count + ' duplicates');

            ///////////////////////

            CacheManager.clear($scope.type.path);

            ///////////////////////

            //Reload the state
            $state.reload();

            //Deselect all
            $scope.selection.deselectAll();
        }


    }

    ////////////////////////////////////////

    $scope.$watch('filteredItems', function(items) {




        $scope.duplicates = _.chain(items)
            .map(function(item) {

                ////////////////////////////////////////////

                //MAPPING WHERE THE SUMMARY IS THE SAME
                var summary = $scope.summary(item);
                item.summary = summary;

                if ($scope.type.path == 'family') {
                    item.addressKey = $scope.addressKey(item);
                }

                return item;
            })
            .reduce(function(set, item) {


                var titles = [item.title];

                ////////////////////////////////
             
                if(item.firstName && item.firstName.length) {
                    titles.push(item.firstName + ' ' + item.lastName);
                }

                if(item.preferredName && item.preferredName.length) {
                    titles.push(item.preferredName + ' ' + item.lastName);
                }

                if(item.ethnicName && item.ethnicName.length) {
                    titles.push(item.ethnicName + ' ' + item.lastName);
                }

                ////////////////////////////////

                titles = _.chain(titles)
                .compact()
                .map(function(string) {
                    return string.toLowerCase();
                })
                .uniq()
                .value();


                // if(_.startsWith(item.title, 'Ed')) {
                // console.log('TITLES', titles)
                // }

                ////////////////////////////////

                _.each(titles, function(title) {



                    if(!title || !title.length) {
                        return;
                    }

                    ////////////////////////////////////////////

                    //MAPPING THE TITLES ARE THE SAME
                    var mappingKey = title;

                    //Create an entry for mapping key
                    if (!set[mappingKey]) {
                        set[mappingKey] = {
                            title: title,
                            items: [],
                        }
                    }

                    /////////////////////////////

                    set[mappingKey].items.push(item);

                    ////////////////////////////////////////////

                    //MAPPING WHERE THE SUMMARY IS THE SAME
                    var summary = item.summary;

                    if (!summary || !summary.length) {
                        return
                    }

                    var summaryKey = _.kebabCase(title + '-' + summary);
                    item.summaryKey = summaryKey;
                    //Create an entry for mapping key
                    if (!set[summaryKey]) {
                        set[summaryKey] = {
                            title: summaryKey,
                            items: []
                        };
                    }

                    /////////////////////////////

                    set[summaryKey].items.push(item);

                    ////////////////////////////////////////////
                    ////////////////////////////////////////////
                    ////////////////////////////////////////////
                    ////////////////////////////////////////////

                    //MAPPING WHERE THE SUMMARY IS THE SAME
                    var addressKey = item.addressKey

                    if (!addressKey || !addressKey.length) {
                        return
                    }

                    addressKey = _.kebabCase(title + '-' + addressKey);

                    item.addressKey = addressKey;
                    //Create an entry for mapping key
                    if (!set[addressKey]) {
                        set[addressKey] = {
                            title: addressKey,
                            items: []
                        };
                    }

                    /////////////////////////////

                    set[addressKey].items.push(item);
                })

                ///////////////////////////////

                return set;






            }, {})
            .values()
            .filter(function(group) {


                //If theres no duplicates by name return false here
                if (!group.items || group.items.length < 2) {
                    return;
                }

                ///////////////////////////////////

                // console.log('Group', group);

                ///////////////////////////////////


                // var distinctions = _.chain(group.items)
                // .reduce(function(set, item) {
                //     _.each(item.distinctFrom, function(code) {
                //         if(!set[code]) {
                //             set[code] = 0;
                //         }
                //         set[code]++;
                //     })

                //     return set;
                // }, {})
                // .values()
                // .value();


                // var multiple = _.some(distinctions, function(pair) {
                //     pair.length >= group.items.length
                // });

                //Check if any are marked as distinct
                var markedAsDistinct = _.chain(group.items)
                    .map(function(item) {
                        return item.distinctFrom;
                    })
                    .flatten()
                    .compact()
                    .countBy(function(id) {
                        return id;
                    })
                    .some(function(val) {
                        // console.log('VAL', val)
                        return val >= group.items.length;
                    })
                    .value();

                ///////////////////////////////////


                //We have multiple matches on name but we need to see if
                //they have been marked as distinct

                // console.log('DISTINCT', markedAsDistinct, group.items)

                return !markedAsDistinct;
            })

            /*
            .reduce(function(set, group) {

                var mappingKey = _.startCase(group.title);
                console.log('Grouping', mappingKey, group);

                ///////////////////////////////////

                //Push into the basic list
                if (!set[mappingKey]) {
                    set[mappingKey] = {
                        title: mappingKey,
                        items: []
                    }

                    // console.log('Mapping key', item._id, mappingKey)
                    set[mappingKey].items.push(item);
                }



                ///////////////////////////////////

                var summaryKey = item.summaryKey;

                if (summaryKey && summaryKey.length) {

                    if (!set[summaryKey]) {
                        set[summaryKey] = {
                            title: summaryKey,
                            items: []
                        }

                        // console.log('Summary key', item._id, summaryKey)
                    }

                    set[summaryKey].items.push(item);
                }


                ///////////////////////////////////

                var addressKey = item.addressKey;

                if (addressKey && addressKey.length) {

                    if (!set[addressKey]) {
                        set[addressKey] = {
                            title: addressKey,
                            items: []
                        }

                        // console.log('Address key', item._id, addressKey)
                    }

                    set[addressKey].items.push(item);
                }

                ///////////////////////////////////

                return set;

            }, {})
            .values()
            /**/
            .map(function(group) {


                //////////////////////////

                // console.log('Map key', group.title, group.items.length);
                group.items = _.chain(group.items)
                    .compact()
                    .sortBy(function(item) {
                        return item.summary;
                    })
                    .uniq(function(item) {
                        return item._id;
                    })
                    .value();




                //////////////////////////

                if (group.items.length < 2) {
                    console.log('Not enough', group)
                    return;
                }


                group.uniqueKey = _.chain(group.items)
                    .map('_id')
                    .sortBy(function(id) {
                        return id;
                    })
                    .value()
                    .join('.');


                console.log('GROUP', group);
                return group;
            })
            .compact()
            .uniq(function(group) {
                return group.uniqueKey;
            })
            // .uniq(function(group) {
            //     return _.map(group.items, '_id').join(',');
            // })
            // .sortBy(function(group) {

            //     return group.title;
            // })
            .value();


        // console.log('DUPLICATES', items, $scope.duplicates);
        // var first = $scope.duplicates[0];
        // first.merging = true;

        //Update the duplicate page
        $scope.updateDuplicatePage();
    });

    ///////////////////////////////////////////////

    //Update the current page
    $scope.updateDuplicatePage = function() {

        var actualDuplicates = _.filter($scope.duplicates, function(set) {
            if (set.merged) {
                return;
            }

            if ($scope.merging) {
                if (set.unlikely) {
                    return;
                }
            }

            return true;
        })

        ///////////////////////////////////////////////

        $scope.actualDuplicateCount = _.chain(actualDuplicates)
            .map('items')
            .flatten()
            .uniq(function(item) {
                return item._id;
            })
            .value()
            .length;

        ///////////////////////////////////////////////

        if (actualDuplicates.length < ($scope.duplicatePager.limit * ($scope.duplicatePager.currentPage - 1))) {
            $scope.duplicatePager.currentPage = 1;
        }

        //Now break it up into pages
        var duplicatePageItems = $filter('startFrom')(actualDuplicates, ($scope.duplicatePager.currentPage - 1) * $scope.duplicatePager.limit);
        duplicatePageItems = $filter('limitTo')(duplicatePageItems, $scope.duplicatePager.limit);

        $scope.duplicatePageItems = duplicatePageItems;


    }


});