/**/


//FIX FOR MOMENT RECUR
moment.fn.dateOnly = function() {
    if (this.tz && typeof(moment.tz) == 'function' ) {
        // return moment.tz(this.format('YYYY/MM/DD'), 'UTC');V
        var d = this.toDate();
        return moment.tz(d, 'UTC');
    } else {
        return this.hours(0).minutes(0).seconds(0).milliseconds(0).add(this.utcOffset(), "minute").utcOffset(0);
    }
};



//////////////////////////////////////////////////////////////////////////

app.controller('EventRecurToolsController', function($scope, $rootScope, $state, event, DateTools, CacheManager, FluroContent, Notifications) {


    $scope.details = {
        count: 1,
        measure: 'days',
        occurrences: 1,
        weekday: 'Sunday',
        nth: 'date',
    };

    ///////////////////////////

    $scope.event = event;

    ///////////////////////////

    // var firstDate = DateTools.localDate(event.startDate, $scope.event.timezone);
    var firstDate = new Date(event.startDate);
    //new Date(event.startDate);

    // new Date(event.startDate);

    var hours = firstDate.getHours();
    var minutes = firstDate.getMinutes();

    ///////////////////////////

    $scope.plural = function(measurement) {
        if (Number($scope.details.count) != 1) {
            return measurement + 's';
        } else {
            return measurement;
        }
    }

    ///////////////////////////

    $scope.create = function() {

        var details = {
            event: event._id,
            dates: $scope.nextDates
        }

        ////////////////////////////

        function success(res) {
            Notifications.status('Created ' + res.results.length + ' events');
            console.log('success', res);

            var definedType = event._type;
            if (event.definition) {
                definedType = event.definition;
            }

            CacheManager.clear(definedType);
            $scope.$close();

            //Reload the state
            $state.reload();
        }

        ////////////////////////////

        function failed(res) {

            console.log('failed', res);
            Notifications.error('Error creating recurring events');
        }

        ////////////////////////////

        FluroContent.endpoint('batch/event').save(details, success, failed);

    }

    ///////////////////////////

    $scope.$watch('details.count + details.nth + details.occurrences + details.measure + details.weekday', update);

    ///////////////////////////


    function update(change) {


        var details = $scope.details;

        /////////////////////////////////////////////////////

        var count = details.count;
        var nth = details.nth;
        var occurrences = details.occurrences;
        var measure = details.measure;
        var weekday = details.weekday;

        /////////////////////////////////////////////////////

        if (!count || count < 1) {
            count = 1;
        }

        if (!occurrences || occurrences < 1) {
            occurrences = 1;
        }

        if (!nth) {
            nth = 'date';
        }

        if (!measure) {
            measure = 'weeks';
        }


        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////

        // var momentDate = new Date($scope.event.startDate);
        // console.log('MOMENT DATe', $scope.event.startDate);



        //Create a recurrence
        // .tz($scope.event.timezone || $root.currentTimezone)







        // // get the next 3 occurrences
        // const nextThreeOccurrences = recur.next(3);
        // console.log('nextThreeOccurrences', nextThreeOccurrences);



        // var dateString = new Date(firstDate.getTime());
        // console.log('STRINGIS', dateString);

        ////////////////////////////////////////////////////

        // var getOffset = firstDate.getTimezoneOffset();
        // console.log('GET OFFSET', getOffset);
        // // console.log('MOMENT TIMEZONE 2', moment.tz(date, Fluro.timezone).utcOffset());
        //     timezoneOffset = window.moment.tz(date, specifiedTimezone).utcOffset();
        //     browserOffset = window.moment(date).utcOffset();

        //     var difference = (timezoneOffset - browserOffset);
        //     var offsetDifference = difference * 60 * 1000;

        //     //Create a new date from what was passed through
        //     var prevDate = new Date(date);

            
        //     //Now adjust the time of our date object to match the difference in timezone
        //     date.setTime(date.getTime() + offsetDifference);




        var startMoment = moment(firstDate);//moment.tz(firstDate, 'UTC');
        $scope.firstDate = startMoment.toDate();

        ////////////////////////////////////////////////////

        var MomentRecur = startMoment.recur();

        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////

        switch (measure) {
            case 'months':
                if (nth == 'date' || !weekday) {

                    //Repeat on the date of the month
                    MomentRecur.every(count, 'months');

                } else {

                    //Repeat on that weekday
                    MomentRecur.every(weekday).daysOfWeek();
                    MomentRecur.every([nth - 1]).weeksOfMonthByDay();
                }
                break;
                case 'weeks':

                    // MomentRecur.every(count, 'weeks');
                    // var weekday = moment(firstDate).format('dddd');
                    // console.log('WEEKDAY', weekday);
                    // MomentRecur.every(weekday).daysOfWeek();
                    var amount = count*7;

                     // console.log('RECUR WEEKS', count, measure, amount, 'days');

                    MomentRecur.every(amount, 'days');
                break;
            default:

                /**
                    //Recur depending on the details
                    var recur = new Recur({
                        units: count,
                        measure: measure,
                        start: firstDate,
                    });

                    var dates = recur.next(occurrences);
                    console.log('GOT EM', occurrences, dates);

                    $scope.nextDates = _.map(dates, function(mom) {
                        var d = mom.toDate();
                        d.setHours(hours, minutes);
                        return d; //mom.format('DD MM YYYY');
                    });

                    return;
                    /**/

                MomentRecur.every(count, measure);
                break;
        }

        ////////////////////////////////////////////////////

        var recurringMoments = MomentRecur.next(occurrences);


        ////////////////////////////////////////////////////

        $scope.nextDates = _.map(recurringMoments, function(mom) {
            var d = mom.toDate();
            d.setHours(hours, minutes);

            // d = DateTools.localDate(d, $scope.event.timezone);


            return d; //mom.format('DD MM YYYY');
        })
    }


});