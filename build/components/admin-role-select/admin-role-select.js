app.directive('adminRoleSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-role-select/admin-role-select.html',
        controller: 'AdminRoleSelectController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('AdminRoleSelectController', function($scope, $rootScope, FluroAccess, Selection, ModalService, FluroContent, $controller) {

    $scope.search = {};

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }

    $scope.createText = 'Create new role';

    /////////////////////////////////////

    //See whether a use is allowed to create new objects
    $scope.createEnabled = FluroAccess.can('create', 'role');   

    /////////////////////////////////////


    $scope.create = function() {
        ModalService.create('role', {}, function(res) {

            res.account = $rootScope.user.account;
            $scope.roles.push(res);
            $scope.model.push(res);
            $scope.updateTree();
        })
    }

    //////////////////////////////////

    $scope.toggle = function(item) {
        if ($scope.contains(item)) {
            $scope.deselect(item)
        } else {
            $scope.select(item);
        }
    }

    //////////////////////////////////

    $scope.select = function(item) {
        if (!$scope.contains(item)) {
            $scope.model.push(item)
        }
    }

    //////////////////////////////////

    $scope.deselect = function(item) {

        var i = _.find($scope.model, function(e) {
            if (_.isObject(e)) {
                return e._id == item._id;
            } else {
                return e == item._id;
            }

        });
        if (i) {
            _.pull($scope.model, i)
        }
    }

    //////////////////////////////////

    $scope.contains = function(item) {
        return _.some($scope.model, function(i) {
            if (_.isObject(i)) {
                return i._id == item._id;
            } else {
                return i == item._id;
            }
        });
    }



    /*
    $scope.create = function() {
        var createInstance = ModalController.open({
            path: 'role',
            singular: 'Role',
            plural: 'Roles',
        }, null, function(result) {

            $scope.options.push(result);
            $scope.model.push(result);
        });

    }
    */

    //////////////////////////////////////////////////////////

    $scope.expanded = [];

    $scope.isExpanded = function(set) {
        if($scope.search.keywords && $scope.search.keywords.length) {
            return true;
        }
        return _.includes($scope.expanded, set._id);
    }


    $scope.hasSelected = function(set) {
        return _.some(set.roles, function(role) {
            return $scope.contains(role);
        });
    }

    $scope.toggleExpanded = function(set) {
        var expanded = $scope.isExpanded(set);

        if(expanded) {
            _.pull($scope.expanded, set._id);
        } else {
            // $scope.expanded = [set._id];
            $scope.expanded.push(set._id);
        }
    }

    //////////////////////////////////////////////////////////

    //Get all the options
    FluroContent.resource('role').query({
        // simple: true,
        sort:'title',
        searchInheritable:true,
        fields:['account', 'title']
    }, function(res) {

        $scope.roles = res;


        // //If there is only one option stick it in the options straight away
        if (res.length == 1) {
            $scope.model.push(res[0]);
        }

        $scope.updateTree();
    })

    //////////////////////////////////////////////////////////

    $scope.updateTree = function() {

        ////////////////////////////////////////////////

        $scope.tree = _.chain($scope.roles)
        .reduce(function(results, role) {

            var existing = _.find(results, {_id:role.account._id});
            
            if(!existing) {

                existing = {
                    _id:role.account._id,
                    title:role.account.title,
                    roles:[],
                }

                if(role.account._id == $rootScope.user.account._id) {
                    $scope.expanded.push(role.account._id);
                }

                results.push(existing);
            }

            existing.roles.push(role);

            existing.roles = _.sortBy(existing.roles, 'title');

            return results;
        }, [])
        .sortBy('title')
        .value();

        ////////////////////////////////////////////////

        // $scope.options = res;

        
    }
});