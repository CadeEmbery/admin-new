app.directive('capabilityFilterBlock', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            contacts:'=ngSource',
            model: '=ngModel',
        },
        templateUrl: 'admin-capability-filter-block/admin-capability-filter-block.html',
        controller: 'CapabilityFilterBlock',
    };
});

///////////////////////////////////////////////////////

app.controller('CapabilityFilterBlock', function($scope, TypeService) {

    //Get all the Capability Definitions
    var definitions = _.reduce(TypeService.getSubTypes('capability'), function(set, definition) {
        set[definition.definitionName] = definition;
        return set;
    }, {});


    ///////////////////////////////


    $scope.isVisible = function() {
        return true;
    }
    ///////////////////////////////


    $scope.$watch('contacts', function(contacts) {

        //Get all the capabilities from our users
        $scope.definitions = _.chain(contacts)
        .map('capabilities')
        .flatten()
        .compact()
        .uniq(function(capability) {
            return capability._id
        })
        .reduce(function(set, capability) {

            var capabilityDefinition = capability.definition || 'capability';
            var existing = set[capabilityDefinition];

            if(!existing) {

                //Get or create a set
                var matchingSet = definitions[capabilityDefinition];

                if(!matchingSet) {
                    matchingSet = {title:'Capability', plural:'Capabilities', definition:'capability'};
                }

                // console.log('MATCHED SET', capability.title, capabilityDefinition, matchingSet);

                set[capabilityDefinition] =
                existing = {
                    title:matchingSet.title,
                    plural:matchingSet.plural,
                    definition:matchingSet.definitionName,
                    items:[],
                }
            }

            //Add the option into the definition set
            capability.key = capability._id;
            existing.items.push(capability);

            return set;
        }, {})
        .values()
        .value()




    })

});
