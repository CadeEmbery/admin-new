app.controller('FluroEmailController', function($rootScope, $scope, contacts, Notifications, FluroContent) {

    $scope.message = {};
    

    console.log('CONTACTS', contacts);
    ///////////////////////////////////////

    $scope.withoutEmail = _.filter(contacts, function(contact) {
        return !contact.emails || !contact.emails.length;
    })

    ///////////////////////////////////////

    $scope.contacts = _.filter(contacts, function(contact) {
        return contact.emails && contact.emails.length;
    })

    ///////////////////////////////////////
    ///////////////////////////////////////

    $scope.cancel = function() {
        $scope.$dismiss();
    }

    ///////////////////////////////////////

    $scope.remove = function(contact) {
        _.pull($scope.contacts, contact);
    }

    ///////////////////////////////////////

    $scope.send = function() {

        var ids = _.map($scope.contacts, function(contact) {
            return contact._id;
        });

        var msg = $scope.message;
        msg.ids = ids;

        // //////////////////////////////////////////////

        // switch(msg.sendFromSelect) {
        //     case 'user':
        //         msg.fromUser = true;
        //     break;
        //     case 'name':
        //         msg.fromName = $scope.message.name;
        //     break;
        //     default:
        //     break;
        // }

        //////////////////////////////////////////////

        var request = FluroContent.endpoint('email/send').save(msg)

        function success(res) {
            console.log('Message Request Success', res);

            ////////////////////////////////////////////

            var successful = _.filter(res.results, function(result) {
                return result.state == 'ready' || result.state == 'sent';
            });

            ////////////////////////////////////////////

            var failed = _.filter(res.results, function(result) {
                switch (result.state) {
                    case 'ready':
                    case 'sent':
                        return false;
                        break;
                    default:
                        return true;
                        break;
                }
            });

            ////////////////////////////////////////////

            if (!failed.length) {
                Notifications.status('Email Message sent to ' + res.results.length + ' contacts')
            } else {
                if (successful.length) {
                    Notifications.warning('Email Message sent to ' + successful.length + ' of ' + res.results.length + ' contacts')
                } else {
                    Notifications.error('Failed to send to ' + res.results.length + ' contacts')
                }
            }

            ////////////////////////////////////////////

            $scope.$dismiss();
        }

        function fail(err) {
            console.log('Email Message Fail', err);
            Notifications.warning(err.toString())
        }

        //////////////

        request.$promise.then(success, fail);


    }


});