

app.controller('FluroMailoutController', function($scope, TypeService, FluroAccess, template,  ModalService) {

   $scope.options = _.chain(TypeService.definedTypes)
    .filter(function(mailoutType) {
        var correctParentType = mailoutType.parentType == 'mailout';
        var canCreate = FluroAccess.can('create', mailoutType.definitionName, 'mailout');

        var systemOnly = mailoutType.systemOnly;
            if(systemOnly) {
                // console.log('FILTER OUT', mailoutType)
                return false;
            }

        return correctParentType && canCreate;
    })
    .value();


     
    

    $scope.select = function(definition) {

        $scope.$dismiss();
        return ModalService.create(definition.definitionName, {template:template});
    }


});