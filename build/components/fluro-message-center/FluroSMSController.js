app.filter('senderName', function() {

    return function(input) {
        return input.replace(/ /g, '').substring(0, 11);
    }
})

app.controller('FluroSMSController', function($rootScope, $scope, $filter, contacts, Notifications, FluroContent) {

    $scope.message = {};


    console.log('CONTACTS', contacts);
    ///////////////////////////////////////

    $scope.withoutPhoneNumber = _.filter(contacts, function(contact) {
        return !contact.phoneNumbers || !contact.phoneNumbers.length;
    })


    $scope.contacts = _.chain(contacts)
        .filter(function(contact) {
            return contact.phoneNumbers && contact.phoneNumbers.length;
        })
        .value();


    ///////////////////////////////////////

    $scope.smsCount = function() {
        return Math.max(Math.ceil(( $scope.message && $scope.message.message ? $scope.message.message.length : 0 ) / 160), 1)
    }
    ///////////////////////////////////////

    $scope.cancel = function() {
        $scope.$dismiss();
    }

    ///////////////////////////////////////

    $scope.remove = function(contact) {
        _.pull($scope.contacts, contact);

        updateEstimate();
    }



    //////////////////////////////////////////////

    $scope.resetNumber = function() {
        var request = FluroContent.endpoint('sms/reset').get().$promise.then(function(res) {
            $rootScope.user.countryCode = null;
            $rootScope.user.phoneNumber = null;
        })
    }


    // $scope.accountBalance = "Calculating account balance...";
    ///////////////////////////////////////

    var creditAmount = 0;

    $scope.updateAccountBalance = function() {

        //console.log("User:", $rootScope.user)
       
        FluroContent.endpoint('account/sms')
            .get()
            .$promise
            .then(function(res) {
                creditAmount = _.get(res, 'creditAmount') || 0;
                updateEstimate();
            });
    }

    ///////////////////////////////////////

    $scope.$watch('message.message.length', updateEstimate);

    ///////////////////////////////////////

    var threshold = 0;

    ///////////////////////////////////////

    function updateEstimate() {

        //Get the cost
        var cost = _.sum($scope.contacts, function(contact) {
            return parseInt(contact.estimatedCost);
        });


        console.log('ESTIMATED', cost)
        cost = (cost * $scope.smsCount());
        console.log('ADJUSTED COST')

        // This is currently a float and not an int so add the /100 after it changes
        $scope.accountBalance = parseInt(creditAmount);
        $scope.pendingBalance = parseInt(creditAmount) - parseInt(cost);


        $scope.displayAccountBalance = $filter('currency')($scope.accountBalance / 100);
        $scope.displayPendingBalance = $filter('currency')($scope.pendingBalance / 100);
        $scope.displayCost = $filter('currency')(cost / 100);
    }

    ///////////////////////////////////////

    $scope.disabled = function() {
        return !$scope.message.message || !$scope.message.message.length || $scope.pendingBalance < threshold;
    }

    $scope.updateAccountBalance()

    ///////////////////////////////////////


    $scope.send = function() {

        var ids = _.map($scope.contacts, function(contact) {
            return contact._id;
        });

        var msg = $scope.message;
        msg.ids = ids;

        //////////////////////////////////////////////

        switch (msg.sendFromSelect) {
            case 'user':
                msg.fromUser = true;
                break;
            case 'name':
                msg.fromName = $scope.message.name;
                break;
            case 'number':
                msg.fromName = 'GENERIC';
                break;
            default:
                break;
        }



        //////////////////////////////////////////////

        var request = FluroContent.endpoint('sms/send').save(msg)

        function success(res) {
            console.log('Message Request Success', res);

            ////////////////////////////////////////////

            var successful = _.filter(res.results, function(result) {
                return result.state == 'ready' || result.state == 'sent';
            });



            ////////////////////////////////////////////

            var failed = _.filter(res.results, function(result) {
                switch (result.state) {
                    case 'ready':
                    case 'sent':
                        return false;
                        break;
                    default:
                        return true;
                        break;
                }
            });

            ////////////////////////////////////////////

            if (!failed.length) {
                Notifications.status('SMS Message sent to ' + res.results.length + ' contacts')
            } else {
                if (successful.length) {
                    Notifications.warning('SMS Message sent to ' + successful.length + ' of ' + res.results.length + ' contacts')
                } else {
                    Notifications.error('Failed to send to ' + res.results.length + ' contacts')
                }
            }

            ////////////////////////////////////////////

            $scope.$dismiss();
        }

        function fail(err) {
            console.log('SMS Message Fail', err);
            Notifications.warning(err.data.toString())
        }

        //////////////

        request.$promise.then(success, fail);


    }


});