app.directive('reactionHelp', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-reaction-help/admin-reaction-help.html',
    };
});
