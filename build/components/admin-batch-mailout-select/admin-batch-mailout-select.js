app.directive('batchMailoutSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            selection: '=ngModel',
            type: '=ngType',
            definition: '=ngDefinition',
            queryMode: '=',
        },
        templateUrl: 'admin-batch-mailout-select/admin-batch-mailout-select.html',
        controller: 'BatchMailoutSelectController',
    };
});

app.controller('BatchMailoutSelectController', function($scope, TypeService, $state, Batch, Notifications, $rootScope, CacheManager, Fluro, ModalService, FluroContent, FluroAccess, $http) {


    $scope.createMailout = function() {


        

        //Create a template
        var template = {};


        ////////////////////////////////////////////

        //We need to check each item and see
        if ($scope.queryMode) {

            ////////////////////////////////////////////

            function addItemToList(entry) {

                var type = entry._type;

                ///////////////////////////

                if (!type || !type.length) {
                    return;
                }

                ///////////////////////////

                //Just use the type based on the path we are currently viewing
                switch (type) {
                    case 'contact':
                        if (!template.contacts) {
                            template.contacts = [];
                        }
                        template.contacts.push(entry);
                        break;
                    case 'contactdetail':
                        if (!template.details) {
                            template.details = [];
                        }
                        template.details.push(entry);
                        break;
                    case 'checkin':
                        if (!template.checkins) {
                            template.checkins = [];
                        }
                        template.checkins.push(entry);
                        break;
                    case 'team':
                        if (!template.teams) {
                            template.teams = [];
                        }
                        template.teams.push(entry);
                        break;
                    case 'interaction':
                        if (!template.interactions) {
                            template.interactions = [];
                        }
                        template.interactions.push(entry);
                        break;
                    case 'persona':
                        if (!template.personas) {
                            template.personas = [];
                        }
                        template.personas.push(entry);
                        break;
                    case 'process':
                        if (!template.cards) {
                            template.cards = [];
                        }
                        template.cards.push(entry);
                        break;
                    case 'ticket':
                        if (!template.tickets) {
                            template.tickets = [];
                        }
                        template.tickets.push(entry);
                        break;
                    case 'event':
                        if (!template.event) {
                            template.event = [];
                        }
                        template.events.push(entry);
                        break;
                    case 'collection':
                        if (!template.collection) {
                            template.collection = [];
                        }
                        template.collections.push(entry);
                        break;
                }
            }

            _.each($scope.selection.items, addItemToList);
            return ModalService.mailout(template);
        }

        ////////////////////////////////////////////

        //We know the kind of thing we are looking at
        var type = $scope.type.path;

        ////////////////////////////////////////////

        if(type == 'team') {

            //Find all the positions first
            return ModalService.selectPositions($scope.selection.items, function(contacts) {
               

                // console.log('GOT RESULT', $scope.selection.items, contacts);
                if(contacts == 'all' || !contacts || !contacts.length) {
                    
                    template.teams = $scope.selection.items;
                    return showModal();
                }

                ////////////////////////////////////////////
                //Now we need to contact the server and get all the people from
                //those selected positions so we can email them

                ////////////////////////////////////////////

                template.contacts = contacts

                ////////////////////////////////////////////

                return showModal();
            });

        } else {

            //Just use the type based on the path we are currently viewing
            switch (type) {
                case 'contact':
                    template.contacts = $scope.selection.items;
                    break;
                case 'checkin':
                    template.checkins = $scope.selection.items;
                    break;
                case 'team':
                    template.teams = $scope.selection.items;
                    break;
                case 'interaction':
                    template.interactions = $scope.selection.items;
                    break;
                case 'persona':
                    template.personas = $scope.selection.items;
                    break;
                case 'process':
                    template.cards = $scope.selection.items;
                    break;
                case 'ticket':
                    template.tickets = $scope.selection.items;
                    break;
                case 'event':
                    template.events = $scope.selection.items;
                    break;
                case 'collection':
                    template.collections = $scope.selection.items;
                    break;
                case 'query':
                    template.query = $scope.selection.items[0];
                    break;
            }


            //Show the modal window to select the mailout
            return showModal();
        }

        ////////////////////////////////////////////

        ////////////////////////////////////////////

        function showModal() {

            



            ModalService.mailout(template)
        }
    }

    // controller.mailout = function(template, successCallback, cancelCallback) {

    // $scope.popover = {
    //     open:false
    // };

    // refreshDefinedTypes
    //Get all the options
    // FluroContent.endpoint('defined/types/mailout').query().$promise.then(function(res) {
    //     $scope.options = res;
    // }, function(err) {
    //     console.log('Error loading mailout types', err)
    // });




    // $scope.options = _.chain(TypeService.definedTypes)
    // .filter(function(mailoutType) {
    //     var correctParentType = mailoutType.parentType == 'mailout';
    //     var canCreate = FluroAccess.can('create', mailoutType.definitionName, 'mailout');

    //     return correctParentType && canCreate;
    // })
    // .value();





    // $scope.dynamicPopover = {
    //     templateUrl: 'admin-batch-mailout-select/admin-batch-mailout-popover.html',
    // };

    /////////////////////////////////////


    // $scope.createMailout = function(mailoutDefinitionName) {

    //     // console.log('Create MAilout', mailoutDefinitionName, $scope.type.path, $scope.selection.ids.length)

    //     $scope.popover = {
    //         open:false,
    //     }

    //     ////////////////////////////////////////////////////////
    //     // console.log('TESTING WHAT KIND OF PAGE WE ARE ON');

    //     // var mailoutConfig = {};
    //     var template = {};


    //     switch($scope.type.path) {
    //         case 'contact':
    //             template.contacts = $scope.selection.items;
    //         break;
    //         case 'checkin':
    //             template.checkins = $scope.selection.items;
    //         break;
    //         case 'team':
    //             template.teams = $scope.selection.items;
    //         break;
    //         case 'interaction':
    //             template.interactions = $scope.selection.items;
    //         break;
    //         case 'persona':
    //             template.personas = $scope.selection.items;
    //         break;
    //         case 'event':
    //             template.events = $scope.selection.items;
    //         break;
    //         case 'collection':
    //             template.collections = $scope.selection.items;
    //         break;
    //         case 'query':
    //             template.query = $scope.selection.items[0];
    //         break;
    //     }

    //     return ModalService.create(mailoutDefinitionName, {template:template});

    //     // var contactIDs = $scope.selection.ids;

    //     //NOW WE LOAD THE CONTACTS

    //     //Post the contact ids to the server and get back all of the contacts details
    //     // var promise = FluroContent.endpoint('mailout/get/contacts').save({contacts:contactIDs}).$promise;
    //     var promise = $http.post(Fluro.apiURL + '/mailout/get/contacts', mailoutConfig);
    //     // {contacts:contactIDs});


    //     promise.then(function(res) {
    //         var params = {
    //             extras:{
    //                 contacts:res.data,
    //             }
    //         };

    //         //Create the mailout popup
    //         ModalService.create(mailoutDefinitionName, params);




    //     }, function(err) {
    //         console.log('Error getting contacts', err);
    //     })


    // }

});