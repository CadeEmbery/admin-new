app.controller('HomeController', function($scope, FluroStorage, $filter, DateTools, FluroContent, checkins, attendance, interactions) {

    $scope.limit = 5;


    //////////////////////////////

    //Setup Local storage
    var local = FluroStorage.localStorage('dashboard');
    var session = FluroStorage.sessionStorage('dashboard');


    //////////////////////////////

    //Setup Search    
    if (!session.search) {
        $scope.search = session.search = {
            filters: {},
        }
    } else {
        $scope.search = session.search;
    }


    if (!$scope.search.startDate) {

        var startDate = new Date();
        startDate.setHours(0, 0, 0, 0);
        startDate.setDate(startDate.getDate() - 30);
        $scope.search.startDate = startDate;
    }

    if (!$scope.search.endDate) {
        $scope.search.endDate = new Date();
    }

    //////////////////////////////////////////////


    $scope.settings = {
        activePanel:'feed'
    };

    //////////////////////////////////////////////

    //Listen for changes to the search parameters
    $scope.$watch('search', updateFilters, true);

    //////////////////////////////////////////////


    $scope.setSearch = function(str) {
        var startDate = new Date();
        startDate.setHours(0, 0, 0, 0);
        switch (str) {
            case 'week':
                startDate.setDate(startDate.getDate() - 7);
                break;
            case 'month':
                startDate.setDate(startDate.getDate() - 30);
                break;
            case 'year':
                startDate.setDate(startDate.getDate() - 365);
                break;
        }

        $scope.search.startDate = startDate;
        $scope.search.endDate = new Date();

    }

    //$scope.$watch('startCropDate + endCropDate', updateFilters)



    /*


    //Start Crop Dates
    $scope.startCropDate = startDate;
    $scope.endCropDate = new Date();
    /**/

    //////////////////////////////////////////////

    $scope.readableDateRange = function(startDate, endDate) {
        return DateTools.readableDateRange(startDate, endDate);
    }

    //////////////////////////////////////////////

    //Where the magic happens
    function updateFilters() {

        //Start with all the results
        //            filteredItems = $filter('orderBy')(filteredItems, $scope.search.order, $scope.search.reverse);

        var filteredCheckins = $filter('orderBy')($scope.checkins, 'created');
        var filteredInteractions = $filter('orderBy')($scope.interactions, 'created');//$scope.interactions;
        var filteredAttendance = $filter('orderBy')($scope.attendance, 'created');//$scope.attendance;

        ///////////////////////////////////////////////////

        //Filter between the crop dates
        filteredAttendance = _.filter(filteredAttendance, function(report) {
            var reportTime = new Date(report.event.startDate).getTime();

            var start = new Date($scope.search.startDate).getTime();
            var end = new Date($scope.search.endDate).getTime();

            return ((reportTime > start) && (reportTime < end));
        });

        ///////////////////////////////////////////////////

        //Filter between the crop dates
        filteredCheckins = _.filter(filteredCheckins, function(checkin) {
            var checkinTime = new Date(checkin.created).getTime();

            var start = new Date($scope.search.startDate).getTime();
            var end = new Date($scope.search.endDate).getTime();

            return ((checkinTime > start) && (checkinTime < end));
        });



        ///////////////////////////////////////////////////

        //Filter between the crop dates
        filteredInteractions = _.filter(filteredInteractions, function(interaction) {
            var createdTime = new Date(interaction.created).getTime();

            var start = new Date($scope.search.startDate).getTime();
            var end = new Date($scope.search.endDate).getTime();

            return ((createdTime > start) && (createdTime < end));
        });


        //Filter search terms
        //filteredCheckins = $filter('filter')(filteredCheckins, $scope.search.terms);

        //Order the items
        //filteredCheckins = $filter('orderBy')(filteredCheckins, $scope.search.order, $scope.search.reverse);

        if ($scope.search.filters) {
            _.forOwn($scope.search.filters, function(value, key) {
                if (_.isArray(value)) {
                    _.forOwn(value, function(value) {
                        filteredAttendance = $filter('reference')(filteredAttendance, key, value);
                    })
                } else {
                    filteredAttendance = $filter('reference')(filteredAttendance, key, value);
                }
            });
        }

        if ($scope.search.filters) {
            _.forOwn($scope.search.filters, function(value, key) {
                if (_.isArray(value)) {
                    _.forOwn(value, function(value) {
                        filteredCheckins = $filter('reference')(filteredCheckins, key, value);
                    })
                } else {
                    filteredCheckins = $filter('reference')(filteredCheckins, key, value);
                }
            });
        }

        

        if ($scope.search.filters) {
            _.forOwn($scope.search.filters, function(value, key) {
                if (_.isArray(value)) {
                    _.forOwn(value, function(value) {
                        filteredInteractions = $filter('reference')(filteredInteractions, key, value);
                    })
                } else {
                    filteredInteractions = $filter('reference')(filteredInteractions, key, value);
                }
            });
        }

        ////////////////////////////////////////////


        //Update the items with our search
        $scope.filteredAttendance = filteredAttendance;
        $scope.filteredCheckins = filteredCheckins;
        $scope.filteredInteractions = filteredInteractions;
    }



    //////////////////////////////////////////////
    //////////////////////////////////////////////

    $scope.interactions = interactions;
    // $scope.checkins = checkins;
    $scope.tracks = _.sortBy(checkins, function(track) {
        return -track.total;
    });;

    $scope.settings.max = _.max(checkins, function(track) {
        return track.max
    });
    
    $scope.attendance = attendance;

    //////////////////////////////////////////////


    //////////////////////////////////////////////




    //////////////////////////////////////////////
    //////////////////////////////////////////////
    //////////////////////////////////////////////




    //////////////////////////////////////////////
    //////////////////////////////////////////////
    //////////////////////////////////////////////
    //////////////////////////////////////////////    
    //////////////////////////////////////////////

    /**

    var interactions = _.groupBy(interactions, function(interaction) {
        if (interaction.definition) {
            return interaction.definition._id;
        } else {
            return '';
        }
    });



    $scope.iLabels = _.map(interactions, function(group) {
        return group[0].title
    })

    $scope.iData = _.map(interactions, function(group) {
        return group.length
    })


    //////////////////////////////////////////////

    var events = _.groupBy(checkins, function(checkin) {
        if (checkin.event) {
            return checkin.event._id;
        } else {
            return '';
        }
    });

    $scope.pieLabels = _.map(events, function(group) {
        return group[0].event.title;
    })

    $scope.pieData = _.map(events, function(event) {
        return event.length;
    })

    //////////////////////////////////////////////

    $scope.dated = _.groupBy(checkins, function(checkin) {
        var date = new Date(checkin.event.startDate);
        return date.format('l j M'); //date.getTime();
    })


    $scope.barLabels = _.keys($scope.dated);
    $scope.barData = _.map($scope.dated, function(group) {
        return group.length
    })


    $scope.barSeries = _.chain(checkins)
        .map(function(checkin) {
            return checkin.realms;
        })
        .flatten()
        .compact()
        .map(function(realm) {
            return realm.title;
        })
        .uniq()
        .value()

        /**/

    ////////////////////////////////////////////////////////////

    //Retrieve all checkins for a specified realm on a specified date

    function getRealmCheckinsForDay(contacts, realmId, timestamp) {
        return _.filter(contacts, function(checkin) {

            var correctRealm = _.some(checkin.realms, {
                _id: realmId
            });

            //Get the event
            var event = checkin.event;
            var datestamp = new Date(event.startDate);
            datestamp.setHours(0, 0, 0, 0);

            var correctDate = (datestamp.getTime() == timestamp);
            return correctRealm && correctDate;
        })
    }

    ////////////////////////////////////////////////////////////

    function getRealmAttendanceForDay(reports, realmId, timestamp) {
        return _.filter(reports, function(report) {

            var correctRealm = _.some(report.realms, {
                _id: realmId
            });

            

            //Get the event
            var event = report.event;
            var datestamp = new Date(event.startDate);
            datestamp.setHours(0, 0, 0, 0);

            var correctDate = (datestamp.getTime() == timestamp);

            return correctRealm && correctDate;
        })
    }

    ////////////////////////////////////////////////////////////

    function getAttendanceForDay(reports, timestamp) {
        return _.filter(reports, function(report) {

            //Get the event
            var event = report.event;
            var datestamp = new Date(event.startDate);
            datestamp.setHours(0, 0, 0, 0);

            var correctDate = (datestamp.getTime() == timestamp);

            return correctDate;
        })
    }

    ////////////////////////////////////////////////////////////

    function getAttendanceForEvent(reports, eventId) {
        return _.filter(reports, function(report) {
            return report.event._id == eventId;
        })
    }

    ////////////////////////////////////////////////////////////

    //Retrieve all checkins for a specified realm on a specified date

    function getEventCheckinsForDay(contacts, eventId, timestamp) {
        return _.filter(contacts, function(checkin) {

            //Get the event
            var event = checkin.event;
            var datestamp = new Date(event.startDate);
            datestamp.setHours(0, 0, 0, 0);

            var correctDate = (datestamp.getTime() == timestamp);
            var correctEvent = event._id == eventId;
            return correctEvent && correctDate;
        })
    }

    ////////////////////////////////////////////////////////////

    //Retrieve all checkins for a specified realm on a specified date

    function getEventAttendanceForDay(reports, eventId, timestamp) {
        return _.filter(reports, function(report) {

            //Get the event
            var event = report.event;
            var datestamp = new Date(event.startDate);
            datestamp.setHours(0, 0, 0, 0);

            var correctDate = (datestamp.getTime() == timestamp);
            var correctEvent = event._id == eventId;
            return correctEvent && correctDate;
        })
    }

    ////////////////////////////////////////////////////////////


    $scope.$watch('filteredInteractions', function(items) {
        //////////////////////////////////////////////

        var iData = [];
        var iLabels = [];

        
        var groups = _.groupBy(items, function(interaction) {
            return interaction.definition;
        })

        _.each(groups, function(group) {
            iData.push(group.length);
            iLabels.push(group[0].definition);
        });

        $scope.iData = iData;
        $scope.iLabels = iLabels;
    });

    ////////////////////////////////////////////////////////////


    $scope.$watch('filteredCheckins + filteredAttendance', function() {

        var filteredCheckins = $scope.filteredCheckins;
        var filteredAttendance = $scope.filteredAttendance;

        /////////////////////////////////////////////

        var uniqueCheckins = _.uniq(filteredCheckins, function(checkin) {
            if(checkin.contact) {
                return checkin.contact._id;
            }
        });

        $scope.uniqueCheckins = uniqueCheckins;

        /////////////////////////////////////////////



        //Realms
        var realms = _.chain(filteredCheckins)
            .map(function(checkin) {
                return checkin.realms;
            })
            .flatten()
            .compact()
            .uniq(function(realm) {
                return realm._id;
            })
            .value();

        /////////////////////////////////////////////

        var genders = _.countBy(uniqueCheckins, function(checkin) {

            var val ='unknown';

            if(checkin.contact) {
                if(checkin.contact.gender) {
                    val = checkin.contact.gender;
                }
            }

            return val;
            
        });



        $scope.genderData = _.values(genders);
        $scope.genderLabels = _.keys(genders);


        $scope.genderColors = _.map($scope.genderLabels, function(gender) {
            
            //['#34c7f1', '#f746a6', '#eeeeee'];


            switch(gender) {
                case 'male':
                    return '#34c7f1';
                break;
                case 'female':
                    return '#f746a6';
                break;
                default:
                    return '#eeeeee';
                break;
            }
        });

       
        $scope.genders = genders;

        /////////////////////////////////////////////

       

        var checkinDates = _.chain(filteredCheckins)
            .map(function(checkin) {
                var event = checkin.event;
                var datestamp = new Date(event.startDate);
                datestamp.setHours(0, 0, 0, 0);
                return datestamp.getTime();
            })
            .uniq()
            .value();


        var attendanceDates = _.chain(filteredAttendance)
            .map(function(report) {
                var event = report.event;
                var datestamp = new Date(event.startDate);
                datestamp.setHours(0, 0, 0, 0);
                return datestamp.getTime();
            })
            .uniq()
            .value();


        /////////////////////////////////////////////


        var checkinEvents = _.chain(filteredCheckins)
            .map(function(item) {
                return item.event;
            })
            .uniq(function(event) {
                return event._id;
            })
            .value();

        var attendanceEvents = _.chain(filteredAttendance)
            .map(function(item) {
                return item.event;
            })
            .uniq(function(event) {
                return event._id;
            })
            .value();

        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////

        var datedCheckinData = [];

        _.each(realms, function(realm) {
            var realmData = [];

            _.each(checkinDates, function(date) {
                var checkinCount = getRealmCheckinsForDay(filteredCheckins, realm._id, date);
                //var reports = getRealmAttendanceForDay(filteredAttendance, realm._id, date);
                

                // var totalAttendance = 0;
                
                // _.each(reports, function(report) {
                //     totalAttendance += report.count;
                // });

                // var avgAttendance = (totalAttendance / reports.length);

                // if(totalAttendance && totalAttendance > checkinCount) {
                //     realmData.push(totalAttendance);
                // } else {
                //     realmData.push(checkinCount.length);
                // }
                
                realmData.push(checkinCount.length);
            });

            datedCheckinData.push(realmData);
        });

        $scope.dateData = datedCheckinData;

        //Labels
        $scope.dateLabels = _.map(checkinDates, function(date) {
            return new Date(date).format('l j M');
        });

        //Series
        $scope.dateSeries = _.map(realms, function(realm) {
            return realm.title;
        });

        //Series
        $scope.dateColors = _.map(realms, function(realm) {

            if (realm.bgColor) {
                return realm.bgColor;
            } else {
                return '#cccccc';
            }
        });


        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////

        
        var average = [];
        var total =[];

        _.each(attendanceEvents, function(event) {

            var reports = getAttendanceForEvent(filteredAttendance, event._id);
            var totalAttendance = 0;
        
            _.each(reports, function(report) {
                totalAttendance += report.count;
            });

            var avgAttendance = Math.ceil(totalAttendance / reports.length);
            total.push(totalAttendance);
            average.push(avgAttendance);

        });

        //Combine
        $scope.attendanceData = [average, total];

        //Labels
        $scope.attendanceLabels = _.map(attendanceEvents, function(event) {
            //return event.title;
            return new Date(event.startDate).format('l j M');
        });

        //Series
        $scope.attendanceSeries = ['Averaged', 'Combined'];

        //Series
        $scope.attendanceColors = ['#000000', '#aaaaaa'];


        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////


        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////

        var ageData = [];
        var ageLabels = [];


        var ages = _.countBy(filteredCheckins, function(checkin) {
            var val ='Unknown';
            if(checkin.contact) {
                if(checkin.contact.dob) {
                    var years = moment().diff(checkin.contact.dob, 'years');
                    if(years > 0) {
                        val = years
                    }
                }
            }
            return val;
        });

        /////////////////////////////////////////////////////
        
        //Remove the unknown ages from the graph


        /////////////////////////////////////////////////////

        ageData = _.values(ages);
        ageLabels = _.keys(ages);

        $scope.ageData = [ageData];
        $scope.ageLabels = ageLabels;

        /////////////////////////////////////////////////////

        var knownAges = _.chain(uniqueCheckins)
        .map(function(checkin) {
            if(checkin.contact) {
                if(checkin.contact.dob) {
                    var years = moment().diff(checkin.contact.dob, 'years');
                    if(years > 0) {
                        return years;
                    }
                }
            }
        })
        .compact()
        .value();


        var averageAge = Math.round(_.sum(knownAges) / knownAges.length);

        if(!averageAge) {
            averageAge = 'Unknown';
        }

        $scope.averageAge = averageAge;


        //////////////////////////////////////////////
        //////////////////////////////////////////////
        //////////////////////////////////////////////
        //////////////////////////////////////////////
        //////////////////////////////////////////////

        var pieData = [];
        var pieLabels = [];
        var pieList = [];
        var pieColors = [];


        _.each(checkinEvents, function(event) {
            var object = {};
            object.tally = 0;
            object.title = event.title;
            object._type = event._type;
            object.definition = event.definition;
            object._id = event._id;
            object.color = '#cccccc';

            _.each(filteredCheckins, function(checkin) {
                var inEvent = (checkin.event._id == event._id);
                if (inEvent) {
                    object.tally++;
                    if (checkin.realms && checkin.realms.length) {
                        if (checkin.realms[0].bgColor) {
                            object.color = checkin.realms[0].bgColor;
                        }
                    }
                }
            })
            pieData.push(object.tally);
            pieLabels.push(object.title);
            pieList.push(object)
            pieColors.push(object.color)
        })

        $scope.pieData = pieData;
        $scope.pieList = pieList;
        $scope.pieLabels = pieLabels;
        $scope.pieColors = pieColors;



        



      


    })

    /*
    $scope.lineData = [
        [65, 59, 80, 81, 56, 55, 40],
        [28, 48, 40, 19, 86, 27, 90],
        [28, 48, 20, 19, 86, 27, 90]
    ];
    $scope.lineLabels = ["January", "February", "March", "April", "May", "June", "July"];
    $scope.lineSeries = ['Series A', 'Series B', 'Series C'];



    $scope.onClick = function(points, evt) {
        console.log(points, evt);
    }




    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////





    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////




    /*
    $scope.barLabels = _.chain(events)
    .map(function(group) {
    	return group[0].event.realms;
    })
    .flatten()
    .compact()
    .map(function(realm) {
        return realm.title;
    })
    .uniq()
    .value();


    $scope.barData = _.map(events, function(event) {
    	return event.length;
    })
*/


    /*
    $scope.series = _.map(group) {
    	return group[0].event.title;
    }//['Series A', 'Series B'];




    /*

    var stats = {
    	total:0,
    	events:{}
    };

    var weeklyLabels = [];


    _.each(stats, function(checkin) {

        var label = '';
        if (checkin.event) {
            label = checkin.event.title;
        } else {
            label = 'Unknown';
        } 

        if (!weeklyStats.events[label]) {
            weeklyStats.events[label] = {
            	title:label,
            	event:checkin.event,
            	count:0,
            	contents:[]
            }

            weeklyLabels.push(label);
        }

        //Increment the count
        var obj = weeklyStats.events[label];
        obj.count++;
        obj.contents.push(checkin);

        weeklyStats.total++;
    });


    $scope.weeklyStats = weeklyStats;
    $scope.weeklyLabels = weeklyLabels;
    */

    //////////////////////////////////////////////
    //////////////////////////////////////////////
    //////////////////////////////////////////////



    /*
    $scope.pieLabels = _.map(stats, function(stat))["Download Sales", "In-Store Sales", "Mail-Order Sales"];
    $scope.pieData = [300, 500, 100];




    //$scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
    $scope.series = ['Series A', 'Series B'];
    $scope.data = [
        [65, 59, 80, 81, 56, 55, 40],
        [28, 48, 40, 19, 86, 27, 90]
    ];
    $scope.onClick = function(points, evt) {
        console.log(points, evt);
    };




    /**
$scope.stats = stats;


$scope.$watch('stats', function(stats) {

	$scope.segments = _.groupBy(stats, function(stat) {
		return stat.event;
	})
})

console.log($scope.stats);
/*




console.log(stats);


$scope.trackEvent = function() {
	

	var tracker = {
		event:$scope.eventName
	}

	FluroContent.endpoint('track').save(tracker).$promise.then(function(res) {
		console.log('success', res)
	},
	function(res) {
		console.log('Failed', res)	
	})

}
/**/

    /**/



    window.dispatchEvent(new Event('resize'));

});