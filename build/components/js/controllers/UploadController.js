app.controller('UploadController', function($scope, $window, UploadService, definedType, TypeService, ModalService, Notifications) {

    $scope.uploadService = UploadService;


    //Get the defined type
    $scope.definedType = definedType;

    //Create a default data object
    $scope.defaultData = {}

    if(definedType.parentType) {
        $scope.defaultData.type = definedType.parentType;
        $scope.defaultData.definition = definedType.path;
    } else {
        $scope.defaultData.type = definedType.path;
    }



    $scope.startUpload = function() {
        if ($scope.defaultData.realms.length) {

            //Add our default data
            $scope.uploadService.startUpload($scope.defaultData)
            //$scope.uploadService.uploader.uploadAll()
        } else {
            Notifications.error('Please select at least one realm before uploading')
        }
    }

   

    $scope.isImage = function(item) {
        var type = '|' + item._file.type.slice(item._file.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }



    $scope.removeTag = function(parent, tag) {
        _.pull(parent.tags, tag);
    }

    //////////////////////////////////////////////////////////

    $scope.editUploadedItem = function(item) {
        ModalService.edit(item);
    }

    /*
    $scope.removeTags = function(tag) {
        _.each($scope.selection.items, function(item) {
            if (item.formData && item.formData.tags && item.formData.tags.length) {
                item.formData.tags = _.filter(item.formData.tags, function(rTag) {
                    return (rTag != tag);
                })
            }
        });
    }

    //////////////////////////////////////////////////////////

    $scope.addTag = function(tag) {
        _.each($scope.selection.items, function(item) {
            if (!item.formData) {
                item.formData = {}
            }
            if (!item.formData.tags) {
                item.formData.tags = [];
            }

            if (!_.contains(item.formData.tags, tag)) {
                item.formData.tags.push(tag)
            }


        });

        //Clear
        delete $scope.options.newTagName;
    }

    //////////////////////////////////////////////////////////

    $scope.getIncludedTags = function() {
        var tags = _.chain($scope.selection.items)
            .filter(function(item) {
                return (item.formData && item.formData.tags && item.formData.tags.length);
            })
            .map(function(item) {
                return item.formData.tags;
            })
            .flatten()
            .uniq()
            .value();

        return tags;
    }

    //////////////////////////////////////////////////////////

    $scope.addTagsToSelection = function(tags) {

        //Loop through each queue item
        _.each($scope.selection.items, function(item) {
            if (!item.formData) {
                item.formData = {}
            }
            if (!item.formData.tags) {
                item.formData.tags = [];
            }
            _.each(tags, function(tag) {
                item.formData.tags.push(tag)
            });
        })

        $scope.options.selectedTags.length = 0;
    }


    $scope.removeTagsFromSelection = function(tags) {

        //Loop through each queue item
        _.each($scope.selection.items, function(item) {

            item.formData.tags = _.reduce(item.formData.tags, function(result, tag, key) {
                if (!_.contains(tags, tag)) {
                    result.push(tag);
                }

                return result;
            }, [])

        })

        $scope.options.selectedTags.length = 0;


    }
    */



})