app.filter('readableTypeName', function(TypeService) {
    return function(type, plural) {

        var matchType = TypeService.getTypeFromPath(type);
        if(matchType) {

            if(plural) {
                return matchType.plural;
            }
            return matchType.singular;
        }

        return type;



    }

});