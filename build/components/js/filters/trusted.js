app.filter('trusted', ['$sce',
    function($sce) {
        return function(text) {
            if(!text) {
                return $sce.trustAsHtml('');
            }
            return $sce.trustAsHtml(String(text));
        };
    }
]);



app.filter('trustedResource', ['$sce',
    function($sce) {
        return function(url) {
            return $sce.trustAsResourceUrl(url);
        };
    }
]);