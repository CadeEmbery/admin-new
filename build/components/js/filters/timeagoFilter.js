app.filter('timeago', function() {
    return function(date, boolean, seconds) {


        if(seconds) {
            return moment(date).fromNow(boolean, 'ss');
        } else {
            return moment(date).fromNow(boolean);
        }
    };
});





app.filter('timefrom', function() {
    return function(date1, date2, boolean) {
        return moment(date1).from(date2, boolean);
    };
});

app.filter('timedifference', function() {
    return function(date1, date2) {
    	var measure = moment(date1).from(date2, true);
    	if(date1 < date2) {
    		return measure + ' before this event';
    	} else {
    		return measure + ' after this event';
    	}
    };
});




app.filter('countdown', function() {
    return function(date) {

    	var now = moment();
		var input = moment(date);
		return now.diff(input);
    };
});

