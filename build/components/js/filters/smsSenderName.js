app.filter('smsName', function() {
    return function(string) {
        string = string.replace(/ /g, '');
        string = string.replace(/[^a-zA-Z0-9]+/g, "");

        return string.substring(0, 11);
    }
});