


app.filter('contactsummary', function() {
        return function(contact) {
            


            var household = _.get(contact, 'family.title');

            if(household && household.length) {
            	household += ' household';
            }


            /////////////////////////////////////

        	var pieces =_.compact([
        		_.startCase(_.get(contact, 'gender')),
        		_.get(contact, 'householdRole'),
        		household,
        	]);

           

			return pieces.join(', ');

        };
    });
