app.filter('plural', function() {


     /*function calculateAge(birthday) { // birthday is a date
         var ageDifMs = Date.now() - birthday.getTime();
         var ageDate = new Date(ageDifMs); // miliseconds from epoch
         return Math.abs(ageDate.getUTCFullYear() - 1970);
     }
     */

     return function(string) { 

        if(_.endsWith(string, 's')) {
            return string + "es";
        } else {
            return string + 's';
        }
     }; 
     
});