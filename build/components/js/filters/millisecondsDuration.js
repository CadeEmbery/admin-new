app.filter('readableMilliseconds', function() {
  return function(milliseconds) {

    
    var oneSecond = 1000;
    var oneMinute = oneSecond * 60;
    var oneHour = oneMinute * 60;
    var oneDay = oneHour * 24;

    var seconds = (milliseconds % oneMinute) / oneSecond;
    var minutes = Math.floor((milliseconds % oneHour) / oneMinute);
    var hours = Math.floor((milliseconds % oneDay) / oneHour);
    var days = Math.floor(milliseconds / oneDay);

    var timeString = '';
    if (days !== 0) {
        timeString += (days !== 1) ? (days + ' days ') : (days + ' day ');
    }
    if (hours !== 0) {
        timeString += (hours !== 1) ? (hours + ' hrs ') : (hours + 'hr ');
    }
    if (minutes !== 0) {
        timeString += (minutes !== 1) ? (minutes + ' mins ') : (minutes + 'min ');
    }
    if (seconds !== 0 || milliseconds < 1000) {
        timeString += (seconds !== 1) ? (seconds.toFixed(0) + 's ') : (seconds.toFixed(0) + 's ');
    }

    return timeString;
};
});



app.filter('readableSeconds', function() {
  return function(seconds) {

    var milliseconds = seconds * 1000;
    var oneSecond = 1000;
    var oneMinute = oneSecond * 60;
    var oneHour = oneMinute * 60;
    var oneDay = oneHour * 24;

    var seconds = (milliseconds % oneMinute) / oneSecond;
    var minutes = Math.floor((milliseconds % oneHour) / oneMinute);
    var hours = Math.floor((milliseconds % oneDay) / oneHour);
    var days = Math.floor(milliseconds / oneDay);

    var timeString = '';
    if (days !== 0) {
        timeString += (days !== 1) ? (days + ' days ') : (days + ' day ');
    }
    if (hours !== 0) {
        timeString += (hours !== 1) ? (hours + ' hrs ') : (hours + 'hr ');
    }
    if (minutes !== 0) {
        timeString += (minutes !== 1) ? (minutes + ' mins ') : (minutes + 'min ');
    }
    if (seconds !== 0 || milliseconds < 1000) {
        timeString += (seconds !== 1) ? (seconds.toFixed(1) + 's ') : (seconds.toFixed(1) + 's ');
    }

    return timeString;
};
});

app.filter('hhmmss', function() {
  return function(secs) {



    function pad(str) {
        return ("0"+str).slice(-2);
    }

    // function hhmmss(secs) {
      var minutes = Math.floor(secs / 60);
      secs = secs%60;
      var hours = Math.floor(minutes/60)
      minutes = minutes%60;
      return pad(hours)+":"+pad(minutes)+":"+pad(secs);
      // return pad(minutes)+":"+pad(secs);
    // }
    // return hhmmss(seconds);
};
});





app.filter('readableDuration', function() {


    // return function convertTime(seconds) {


    //     var time = seconds;
    //     // time = parseInt(time / 60);

    //     var minutes = time % 60;
    //     time = parseInt(time / 60);

    //     var hours = time % 24;
    //     var out = "";

    //     if (hours && hours > 0) out += hours + " " + ((hours == 1) ? "hr" : "hrs") + " ";
    //     if (minutes && minutes > 0) out += minutes + " " + ((minutes == 1) ? "min" : "mins") + " ";
    //     if (seconds && seconds > 0) out += seconds + " " + ((seconds == 1) ? "sec" : "secs") + " ";

    //     return out.trim();
    // }


    return  function(duration) {
        var hour = 0;
        var min = 0;
        var sec = 0;

        if (duration) {
            if (duration >= 60) {
                min = Math.floor(duration / 60);
                sec = duration % 60;
            } else {
                sec = duration;
            }

            if (min >= 60) {
                hour = Math.floor(min / 60);
                min = min - hour * 60;
            }

            // if (hour < 10) {
            //     hour = '0' + hour;
            // }
            // if (min < 10) {
            //     min = '0' + min;
            // }
            // if (sec < 10) {
            //     sec = '0' + sec;
            // }
        }

        var out = '';

        if(hour) {
            out += hour + ' hr';
            if(hour > 1) {
                out += 's ';
            } else {
                out += ' ';
            }
        }

        if(min) {
            out += min + ' min';
            if(min > 1) {
                out += 's ';
            } else {
                out += ' ';
            }
        }

        if(sec && !hour) {
            out += sec + ' sec';
            if(sec > 1) {
                out += 's ';
            } else {
                out += ' ';
            }
        }

        return out.trim();
    }





});