app.filter('completedTasks', function() {
        return function(array) {
            var completed = _.filter(array, {complete:true});

            return (completed.length + '/' + array.length);
        };
    });
