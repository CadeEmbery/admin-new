app.filter('capitalizename', function() {



    function formatName(name) {

        if (!name) {
            return;
        }

        if (!_.isString(name)) {
            return;
        }

        //Start Case
        name = name.toLowerCase().trim();

        name = name.replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        name = name.replace(/'(S)/g, function(letter) {
            return letter.toLowerCase();
        });


        // name = _.startCase(name);

        //Add capitalized Mc
        name = name.replace(/Mc[a-z]/, function(k) {
            return 'Mc' + k[2].toUpperCase();
        });

        // name = name.replace(/Mac[a-z]/, function(k) {
        //     return 'Mac' + k[3].toUpperCase();
        // });

        return name;

        // title.replace(/\w\S*/g, function(txt) {
        //     var name = txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        //     //Fix Mc Names
        //     name = name.replace(/Mc[a-z]/, function(k) {
        //         return 'Mc' + k[2].toUpperCase();
        //     });

        //     return name;
        // });
    }




    return function(text) {

        console.log('Fluro: capitalizename is deprecated and should be removed');
        if (text && text.length) {
            return formatName(text);
        } else {
            return text;
        }
    };
});