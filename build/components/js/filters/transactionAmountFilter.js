app.filter('transactionAmount', function() {
    return function(num, currency) {

        var prefix = '$';
       
        return prefix + (num / 100).toFixed(2);;
    };
});