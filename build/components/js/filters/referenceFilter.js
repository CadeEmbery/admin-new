app.filter("reference", function() {
    return function(items, fieldName, id) {

        //Find where item author is the id specified
        var results = _.filter(items, function(item) {

            var key = _.get(item, fieldName);

            if (key) {
                if(_.isArray(key)) {
                    var array = key;
                    return _.some(array, function(e) {
                        if(_.isObject(e)) {
                            if(e._id) {
                                return e._id == id;
                            }
                        } else {
                            return e == id;
                        }
                    })
                } else {
                    return (key._id == id || key == id);
                }
            }
        });

        return results;



    };
});


