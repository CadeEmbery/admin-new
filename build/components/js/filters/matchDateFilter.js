


app.filter("matchDate", function() {

    return function(items, dateString, style) {

        var date = new Date(dateString)

        return _.reduce(items, function(results, item) {

            var startDate;
            var endDate;

            //Get the start of the day
            if (item.startDate) {
                startDate = new Date(item.startDate);
                startDate.setHours(0, 0, 0, 0);
            }

            //////////////////////////////////////
            if(!startDate) {
                results.push(item);
                return results;
            }

            //////////////////////////////////////

            //Get the start of the day
            if (item.endDate) {
                endDate = new Date(item.endDate);
            } else {
                endDate = new Date(item.startDate);
            }

            endDate.setHours(23, 59, 59, 999);
            //////////////////////////////////////

            

            //Turn into integers
            var checkTimestamp = date.getTime();
            var startTimestamp = startDate.getTime();
            var endTimestamp = endDate.getTime();
            
            switch (style) {
                case 'upcoming':
                    if(checkTimestamp <= startTimestamp || checkTimestamp <= endTimestamp) {
                        results.push(item);
                    }
                    break;
                default:
                    if(checkTimestamp >= startTimestamp && checkTimestamp <= endTimestamp) {
                        results.push(item)
                    }
                    break;
            }


            return results;
        }, []);

    };
});