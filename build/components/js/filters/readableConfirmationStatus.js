


app.filter('readableConfirmationStatus', function() {
        return function(status) {
            switch(status) {
            	case 'denied':
            		return 'declined';
            	break;
            	case 'unknown':
            		return 'unconfirmed';
            	break;
            }

            return status;
        };
    });
