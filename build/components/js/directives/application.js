app.directive('application', function() {

    return {
        restrict: 'E',
        replace: true,
        // Replace the div with our template
        templateUrl: 'views/application.html',
        controller: 'ApplicationController',
        // link:function($scope, $element) {


        //     angular.element(document).on('click', function(event) {


        //         var parent = angular.element(event.target).closest('.popover');
        //         if(parent.length) {

        //         }


        //     })
        // }
    };
});

//////////////////////////////////////////////////////////////////////////

// console.log('IM HERE');


app.controller('ApplicationController', function($rootScope, FluroSocket, UserSessionService, FluroContent, FluroTokenService, TypeService, Notifications, FluroAccess, $scope, $state, ModalService, FluroStorage, $rootScope, TypeService, CacheManager, Session, $state, FluroAuthService, FluroSocket, FluroAuthService, Notifications, $window) {


    //////////////////////////////
    
    FluroContent.endpoint('user/personas', false, true).query()
    .$promise
    .then(function(res) {
        $scope.userAccounts = res;
    });

    var maxCollapseSize = 2200;

    // console.log('Random Search')


    FluroSocket.on('session', UserSessionService.refresh)

    //////////////////////////////

    //Setup Local storage
    $scope.settings = FluroStorage.sessionStorage('application');
    $scope.search = {}; //FluroStorage.sessionStorage('application');

    //////////////////////////////

    $scope.sidebar = {};

    $scope.toggleSidebarState = function(str) {
        if($scope.sidebar.state == str) {
            $scope.sidebar.state = null;
        } else {
            $scope.sidebar.state = str;
        }
    }

    //////////////////////////////

    // $scope.getSidebarStyle = function() {
    //     switch($scope.settings.sidebarStyle) {
    //         case 'lg':
    //             return 'sidebar-lg';
    //         break;
    //         case 'md':
    //             return 'sidebar-md';
    //         break;
    //         default:
    //             return 'sidebar-xs';
    //         break;
    //     }
    // }


    $scope.sidebarExpanded = function() {
        if(window.innerWidth > maxCollapseSize) {
            return false;
        }

        return !$scope.settings.sidebarHidden;
    }


    $scope.hamburgerClass = function() {

        //If your on a large screen then just leave it
        if(window.innerWidth > maxCollapseSize) {
            return;
        }

        //If your on a phone and the menu is open
        //then show the close
        if(window.innerWidth <= 768) {
            if(!$scope.settings.sidebarHidden) {
                return 'open';
            } else {
                return;
            }
        }

        if(!$scope.settings.sidebarHidden) {
            return 'minimal';//'arrow-left';
        } else {
            return 'arrow-right';
        }


        

    }

    $scope.toggleMenu = function() {
        if(window.innerWidth > maxCollapseSize) {
            return;
        }
        $scope.settings.sidebarHidden = !$scope.settings.sidebarHidden;

        // if (window.innerWidth <= 768) {
        //     if($scope.settings.sidebarStyle == 'lg') {
        //         $scope.settings.sidebarStyle = 'xs';
        //     } else {
        //         $scope.settings.sidebarStyle = 'lg';
        //     }
        //     return;
        // }

        // switch($scope.settings.sidebarStyle) {
        //     case 'xs':
        //         $scope.settings.sidebarStyle = 'md';
        //     break;
        //     case 'md':
        //         $scope.settings.sidebarStyle = 'lg';
        //     break;
        //     default:
        //         $scope.settings.sidebarStyle = 'xs';
        //     break;
        // }
        // $scope.hideSidebarMenu = !$scope.hideSidebarMenu;
        // $scope.settings.hideMenu = !$scope.settings.hideMenu;
        // if (window.innerWidth <= 768) {
        //     //console.log('Toggle mobile menu')

        //     if (!$rootScope.quicksearch.open) {
        //         $scope.settings.hideMenu = !$scope.settings.hideMenu
        //     }

        //     $rootScope.quicksearch.open = false;
        // } else {
        //     $rootScope.quicksearch.open = !$rootScope.quicksearch.open;
        //     // $scope.settings.showNewMenu = !$scope.settings.showNewMenu
        // }
    }


    // //console.log('APPP')



    $rootScope.$on('$stateChangeStart', function(evt, to, params) {

        if (window.innerWidth <= 768) {
            $scope.settings.sidebarHidden  = true;
        }
        // $scope.settings.hideMenu = true;
        // $scope.hideSidebarMenu = true;
        //console.log('HIDE MENU')
    });


    // $scope.$watch(function() {
    //     return ($scope.settings.hideMenu && window.innerWidth <= 768);
    // }, function(hideMenu) {
    //     $scope.hideSidebarMenu = hideMenu;
    // })

   


    //////////////////////////////

    //Return the tree
    $scope.types = TypeService;

    //////////////////////////////

    $scope.createMenuSettings = {}

    //////////////////////////////

    $rootScope.quickCreate = function(type) {
        ModalService.create(type.path);
    }

    //////////////////////////////

    $scope.toggleMenuExpand = function($event, item) {

        if (item.children) {
            if (item.children.length == 1) {
                $scope.selectMenuItem(item.children[0]);
            } else {
                $event.preventDefault();
                $event.stopPropagation();
                item.expanded = !item.expanded;
            }
        } else {
            $scope.selectMenuItem(item);
        }
    }

    //////////////////////////////

    $scope.getSref = function(type) {


        var selectType = type;

        if (!selectType) {
            // //console.log('FAILED', type);
            return 'failed';
        }


        //If the top level is clicked select the first child
        if (selectType.children && selectType.children.length) {
            selectType = type.children[0];
        }

        // //console.log('select type', selectType);
        //If there is a parent type
        if (selectType.parentType) {

            return selectType.parentType + ".custom({definition:'" + selectType.definitionName + "'})";
            /*
            $state.go(selectType.parentType + '.custom', {
                definition: selectType.definitionName
            });
*/
        } else {


            //Return the default
            return selectType.path + '.default';
            // $state.go(selectType.path + '.default');
        }

    }

    $scope.selectMenuItem = function(type) {


        var selectType = type;

        if (type.children) {
            selectType = type.children[0];
        }

        if (selectType.parentType) {
            $state.go(selectType.parentType + '.custom', {
                definition: selectType.definitionName
            });
        } else {
            $state.go(selectType.path + '.default');
        }
    }

    //////////////////////////////

    $scope.isActive = function(type) {
        if (type.parentType) {
            return $rootScope.currentDefPath == type.definitionName;
        } else {
            return $state.current.name == type.path + '.default';
        }
    }

    //////////////////////////////

    $scope.isActiveParent = function(type) {
        return $state.$current.includes[type.path];
    }




    //////////////////////////////////////
    //////////////////////////////////////
    //////////////////////////////////////
    //////////////////////////////////////


    function divide(array, chunkSize) {

        if (!array) {
            array = [];
        }

        var div = Math.floor(array.length / chunkSize);

        if (!div) {
            div = 1;
        }

        var chunked = _.chunk(array, div);
        return chunked
    }

    //////////////////////////////////////
    //////////////////////////////////////
    //////////////////////////////////////
    //////////////////////////////////////

    


    // $scope.$watch(function() {

    //     return TypeService.menuTree;

    // }, function(tree) {

    //console.log(tree);


    //     $scope.menuGroups = _.chain(tree)
    //     .reduce(function(results, type) {

    //         var groupName = type.group;
    //         var _id = String(groupName).toLowerCase();

    //         var existing = _.find(results, {_id:_id});

    //         if(!existing) {
    //             existing = {
    //                 title:groupName,
    //                 _id:_id,
    //                 items:[]
    //             }
    //             results.push(existing);
    //         }

    //         //Push the type into the existing group
    //         existing.items.push(type);


    //         return results;
    //     },[])
    //     .value();

    //     $scope.groupedMenuTree = _.chain(tree)
    //         .groupBy(function(item) {
    //             return item.group;
    //         })
    //         .reduce(function(results, group, key) {
    //             var section = {
    //                 title: key,
    //                 items: group,
    //             }
    //             results.push(section);

    //             return results;

    //         }, [])
    //         .sortBy(function(section) {
    //             return section.title;
    //         })
    //         .value();


    // })



    $scope.logout = function() {
        var req = FluroAuthService.logout();


        req.then(function(res) {
            Notifications.warning('You are now signed out')
            $window.location.reload();

        }, function(res) {
            Notifications.warning('There was an issue trying to sign out')
        })



    }



});