app.directive('handle', function() {

    return {
        restrict: 'E',
        replace:true,
        template:'<div class="btn btn-handle"><i class="far fa-arrows"></i></div>'
    };
});
