/*

app.directive('contentSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            ngParams: '&',
        },
        templateUrl: 'views/ui/content-select.html',
        controller: 'ContentSelectController',
    };
});


//////////////////////////////////////////////////////////////////////////

app.controller('ContentSelectController', function($scope, $rootScope, Fluro, ModalService, TypeService, FluroAccess, $http) {

    /////////////////////////////////////

    if (!$scope.selectedItems) {
        $scope.selectedItems = {
            items:[]
        }
    }

    /////////////////////////////////////


    if (!$scope.params) {
        $scope.params = {};
    }


    /////////////////////////////////////

    $scope.$watch(function() {
        return $scope.ngParams();
    }, function(p) {

        if (!p) {
            p = {}
        }
        $scope.params = p;


        if ($scope.params.canCreate) {
            $scope.canCreate = $scope.params.canCreate;
        }

        if ($scope.params.type) {
            $scope.type = TypeService.getTypeFromPath($scope.params.type)
            //$scope.type = $scope.params.type;
        }

        if ($scope.params.minimum) {
            $scope.minimum = $scope.params.minimum;
        }
        if ($scope.params.maximum) {
            $scope.maximum = $scope.params.maximum;
        }

        if ($scope.params.allowedValues) {
            $scope.allowedValues = $scope.params.allowedValues;
        }

        if ($scope.params.defaultValues) {
            $scope.defaultValues = $scope.params.defaultValues;

            //If theres nothing in the list yet
            if (!$scope.selectedItems.items.length) {
                //Loop through each default value and include it
                _.each($scope.defaultValues, function(item) {
                    $scope.selectedItems.items.push(item);
                })
            }
        }
    }, true)

    ///////////////////////////////////////////////

    $scope.$watch('model', function() {

        console.log('Update model');
        if ($scope.model) {
            if (_.isArray($scope.model)) {
                if ($scope.model.length) {
                    if ($scope.maximum) {
                        //take the maximum first items from the existing model
                        $scope.selectedItems.items = _.take($scope.model, $scope.maximum);
                    } else {
                        $scope.selectedItems.items = angular.copy($scope.model);
                    }
                } else {
                    $scope.selectedItems.items = _.union($scope.model, $scope.defaultValues);
                }
            } else {
                $scope.selectedItems.items = [$scope.model];
            }
        }
    })

    ////////////////////////////////////////////////////

    $scope.$watch('selectedItems.items', function(items) {

        console.log('Selected Items changed', items)
        if ($scope.maximum == 1) {
            $scope.model = _.first(items);
        } else {
            $scope.model = items;
        }

    }, true)


    /////////////////////////////////////

    $scope.proposed = {}

    /////////////////////////////////////

    $scope.dynamicPopover = {
        templateUrl: 'views/ui/create-popover.html',
    };

    $scope.searchPlaceholder = 'Search for existing content';

    if ($scope.type) {
        $scope.searchPlaceholder = 'Search for ' + $scope.type.singular + ' items';
    }

    /////////////////////////////////////

    $scope.createEnabled = function() {
        if ($scope.type) {
            return FluroAccess.can('create', $scope.type.path);
        } else {
            return true;
        }
    }

    /////////////////////////////////////

    $scope.isSortable = function() {

        if ($scope.maximum) {
            return $scope.maximum > 1;
        }
        return true;
    }

    /////////////////////////////////////

    $scope.addEnabled = function() {
        if ($scope.maximum) {
            return ($scope.selectedItems.items.length < $scope.maximum)
        } else {
            return true;
        }
    }

    /////////////////////////////////////

    $scope.add = function(item) {
        $scope.selectedItems.items.push(item);
        $scope.proposed = {};
    }

    /////////////////////////////////////

    $scope.remove = function(item) {
        if (_.contains($scope.selectedItems.items, item)) {
            _.pull($scope.selectedItems.items, item);
        }
    }

    /////////////////////////////////////

    $scope.edit = function(item) {
        ModalService.edit(item);
    }

    /////////////////////////////////////

    $scope.canEdit = FluroAccess.canEditItem;

    /////////////////////////////////////

    $scope.browse = function() {
        if ($scope.type) {
            ModalService.browse($scope.type.path, $scope.selectedItems);
        } else {
            ModalService.browse(null, $scope.selectedItems);
        }
    }

    /////////////////////////////////////

    $scope.createableTypes = TypeService.getAllCreateableTypes;

    /////////////////////////////////////

    $scope.create = function(typeToCreate) {
        if (!typeToCreate) {
            ModalService.create($scope.type.path, {}, $scope.add)
        } else {
            ModalService.create(typeToCreate, {}, $scope.add)
        }
    }

    /////////////////////////////////////

    $scope.getOptions = function(val) {

        if ($scope.allowedValues && $scope.allowedValues.length) {
            return _.reduce($scope.allowedValues, function(filtered, item) {
                var exists = _.some($scope.selectedItems.items, {
                    '_id': item._id
                });

                if (!exists) {
                    filtered.push(item);
                }
                return $filter('filter')(filtered, val);
            }, [])
        } else {

            var typePath;
            if ($scope.type) {
                typePath = $scope.type.path;
            }

            return $http.get(Fluro.apiURL + '/search', {
                ignoreLoadingBar: true,
                params: {
                    keys: val,
                    type: typePath,
                    limit: 10,
                }
            }).then(function(response) {
                var results = response.data;
                return _.reduce(results, function(filtered, item) {
                    var exists = _.some($scope.selectedItems.items, {
                        '_id': item._id
                    });
                    if (!exists) {
                        filtered.push(item);
                    }
                    return filtered;
                }, [])
            });
        }
    };

    /////////////////////////////////////
    /*
    $scope.editItem = function(item) {
        //console.log('MODAL', item._id)
        ModalController.open({path:item._type}, {
            id: item._id
        }, function(result) {
            // console.log('COPY COMPLETED', result)
        })
    }

    */

    /////////////////////////////////////
    /*
    $scope.create = function() {
        var definitionName;

        var types = _.map(ContentTypes, function(type) {
            return type.path;
        })

        if (!_.contains(types, $scope.restrictType)) {
            definitionName = $scope.restrictType;
            var definition = $resource(Fluro.apiURL + '/defined/' + definitionName).get({}, function(res) {
                var createInstance = ModalController.open({
                    singular: res.title,
                    plural: res.plural,
                    path: res.parentType,
                }, {
                    definitionName: definitionName
                }, function(result) {
                    $scope.add(result);
                })
            })
        } else {
            if ($scope.restrictType) {
                var createInstance = ModalController.open({
                    path: $scope.restrictType,
                }, {}, function(result) {
                    $scope.add(result);
                })
            } else {
                console.log('Cant create', $scope.restrictType)
            }
        }
    }

});
*/