app.directive('batchTagSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            selection: '=ngModel',
        },
        templateUrl: 'admin-tag-select/admin-tag-select.html',
        controller: 'BatchTagSelectController',
    };
});

app.controller('BatchTagSelectController', function($scope, $state, Batch, Notifications, $rootScope, CacheManager, Fluro, $http, FluroContent) {

    $scope.popover = {
        open:false
    };

    $scope.tags = [];


    $scope.dynamicPopover = {
        templateUrl: 'views/ui/batch-tag-popover.html',
    };

    $scope.proposed = {}


    /////////////////////////////////////

    $scope.applyTags = function() {

        $scope.popover = {
            open:false
        }

        var details = {
            ids: $scope.selection.ids,
            tags: $scope.tags,
        }

        Batch.addTags(details, done);
    }


    //////////////////////////////////////////////////////////

    function done(err, data) {

        // console.log('Batch done', data);
        if (err) {
            //console.log('DONE ERR', err)
            return Notifications.error(err);
        }

        ///////////////////////

        //Find all the caches we need to clear
        var caches = [];

        ///////////////////////

        //Refresh any caches that might be affected outside of this view
        if (data.result) {
            if (data.result.types && data.result.types.length) {
                caches.concat(data.result.types)
            }

            if (data.result.definitions && data.result.definitions.length) {
                caches.concat(data.result.definitions)
            }
        }

        ///////////////////////

        caches = _.uniq(caches);
        caches = _.compact(caches);

        console.log('Clear caches', caches);

        _.each(caches, function(t) {
            CacheManager.clear(t);
        });

        ///////////////////////

        //Reload the state
        $state.reload();
        console.log('Reload state');

        ///////////////////////

        //Deselect all
        $scope.selection.deselectAll();
        Notifications.status('Updated tags on ' + data.success.length + ' items');



    }

    /////////////////////////////////////

    $scope.removeTags = function() {
        var details = {
            ids: $scope.selection.ids,
            tags: $scope.tags,
        }

        Batch.removeTags(details, done);
    }

    /////////////////////////////////////

    $scope.add = function(item) {
        $scope.tags.push(item);
        $scope.proposed = {};
    }

    /////////////////////////////////////

    $scope.deselect = function(item) {
        _.pull($scope.tags, item);
    }

    /////////////////////////////////////

    $scope.getTags = function(val) {

        var url = Fluro.apiURL + '/content/tag/search/' + val;
        return $http.get(url, {
            ignoreLoadingBar: true,
            params: {
                limit: 20,
            }
        }).then(function(response) {
            var results = response.data;
            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.model, {
                    '_id': item._id
                });
                if (!exists) {
                    filtered.push(item);
                }
                return filtered;
            }, [])
        });

    };

    /////////////////////////////////////

    $scope.create = function() {
        var details = {
            title: $scope.proposed.value,
        }

        //Immediately create a tag   
        FluroContent.resource('tag').save(details, function(tag) {

            //$scope.tags.push(tag);
            $scope.tags.push(tag);

            //We need to clear the tag cache now
            CacheManager.clear('tag');

            //Clear
            $scope.proposed = {}
        }, function(data) {
            Notifications.error('Failed to create tag');
        });
    }


    /**/



});