
app.directive('ngThumb', ['$window',
    function($window) {
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function(item) {
                return angular.isObject(item) && item instanceof $window.File;
            },
            isImage: function(file) {
                var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        };

        return {
            restrict: 'A',
            template: '<div class="canvas-preview"><i class="far fa-spinner fa-spin"></i><canvas/></div>',
            link: function(scope, element, attributes) {
                if (!helper.support) return;

                var params = scope.$eval(attributes.ngThumb);

                if (!helper.isFile(params.file)) return;
                if (!helper.isImage(params.file)) return;

                var canvas = element.find('canvas').hide();
                var loader = element.find('i').show();
                var reader = new FileReader();

                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);

                function onLoadFile(event) {
                    var img = new Image();
                    img.onload = onLoadImage;
                    img.src = event.target.result;
                }

                function onLoadImage() {

                    var width = params.width || this.width / this.height * params.height;
                    var height = params.height || this.height / this.width * params.width;
                    canvas.attr({
                        width: width,
                        height: height
                    });
                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);

                     loader.hide();
                    canvas.show();
                }
            }
        };
    }
]);



/*app.directive('ngThumb', ['$window', function($window) {
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function(item) {
                return angular.isObject(item) && item instanceof $window.File;
            },
            isImage: function(file) {
                var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        };

        return {
            restrict: 'A',
            template: '<canvas/>',
            scope:{
                file:'=ngThumb',
            },
            link: function(scope, element, attributes) {

                scope.$watch('file', function() {

                    if (!helper.support) return;

                    if (!helper.isFile(scope.file)) return;
                    if (!helper.isImage(scope.file)) return;


                    var thumbWidth = 100;
                    var thumbHeight;

                    var canvas = element.find('canvas');
                    var reader = new FileReader();

                    reader.onload = onLoadImage;
                    reader.readAsDataURL(scope.file);

                    
                    function onLoadImage(event) {
                        var img = new Image();
                        //img.onload = onLoadImage;
                        img.src = event.target.result;
                        
                        
                        //Append the image
                        element.empty()
                        .append(img);
                        
                    }

                    

                    function onLoadImage() {
                       
                        
                        var width = thumbWidth || this.width / this.height * thumbHeight;
                        var height = thumbHeight || this.height / this.width * thumbWidth;

                        canvas.attr({ width: width, height: height });
                        canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                        
                    }
                    

                })
                
            }
        };
    }]);

    */