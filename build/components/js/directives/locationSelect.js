app.directive('locationSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            locations: '=ngLocations',
            rooms: '=ngRooms',
        },
        templateUrl: 'views/ui/location-select.html',
        controller: 'LocationSelectController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('LocationSelectController', function($scope, $controller) {

    //Create a model if none exists
    if (!$scope.rooms) {
        $scope.rooms = [];
    }

    if (!$scope.locations) {
        $scope.locations = [];
    }


    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////

    $scope.$watch('locations', function(newData, oldData) {
        var roomOptions = [];

        if (newData) {
            //Loop through each location and add the room name to the options list
            _.each(newData, function(loc) {
                _.each(loc.rooms, function(room) {
                    room.locationName = loc.title;
                    roomOptions.push(room);
                })
            })
        }

        //Update the options that are available
        $scope.roomOptions = roomOptions;



    }, true);


    $scope.$watch('roomOptions', function(data) {

        if (data) {
            //Loop through each selected room
            var filtered = _.filter($scope.rooms, function(room) {
                return _.any($scope.roomOptions, function(existingRoom) {
                    return (existingRoom._id == room._id);
                });
            });

            $scope.rooms.length = 0;

            angular.extend($scope.rooms, filtered);
        }
    }, true)


    /////////////////////////////////////


});