
app.directive('trashSelectToolbar', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            plural: '=ngPlural',
            definition: '=ngDefinition',
            type: '=ngType',
            done:'=ngDone',
        },
        // Replace the div with our template
        templateUrl: 'views/ui/trash-select-toolbar.html',
        controller: 'TrashSelectToolbarController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('TrashSelectToolbarController', function($scope, $state, Notifications, FluroAccess, Batch) {


    var destroyAny = FluroAccess.can('destroy any', $scope.type.path)
    var destroyOwn = FluroAccess.can('destroy own', $scope.type.path)
    var restoreAny = FluroAccess.can('restore any', $scope.type.path)
    var restoreOwn = FluroAccess.can('restore own', $scope.type.path)

    if ($scope.definition) {
        restoreAny = FluroAccess.can('restore any', $scope.definition.definitionName, $scope.definition.parentType)
        restoreOwn = FluroAccess.can('restore own', $scope.definition.definitionName, $scope.definition.parentType)
        destroyAny = FluroAccess.can('destroy any', $scope.definition.definitionName, $scope.definition.parentType)
        destroyOwn = FluroAccess.can('destroy own', $scope.definition.definitionName, $scope.definition.parentType)
    }

    //Check if the user can batch destroy or batch restore
    $scope.canBatchDestroy = (destroyAny || destroyOwn);
    $scope.canBatchRestore = (restoreAny || restoreOwn);


    




    ////////////////////////////////////////////

    $scope.restoreSelected = function() {
        console.log('Restore selected')

        Batch.restore({
            ids: $scope.model.ids,
            useModel:$scope.type.path,
        }, function(err, res) {

            if (err) {
                console.log('DONE ERR', err)
                return Notifications.error(err);
            }

            console.log('trash restore res', res);

            if(res && res.success) {
                Notifications.status('Restored ' + res.success.length + ' ' + $scope.type.plural);
            }

            $scope.model.deselectAll();

            $scope.done(res);
        });
    }

    

    ////////////////////////////////////////////

    $scope.destroySelected = function() {
        Batch.destroy({
            ids: $scope.model.ids,
            useModel:$scope.type.path,
        }, function(err, res) {
            if (err) {
                console.log('DONE ERR', err)
                return Notifications.error(err);
            }

            console.log('trash destroy res', res);

            if(res && res.success) {
                Notifications.status('Destroyed ' + res.success.length + ' ' + $scope.type.plural);
            }

            $scope.model.deselectAll();
            
            //Refresh
             $scope.done(res);
        });

    }

    






});