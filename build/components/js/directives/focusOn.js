app.directive('focusOn', function() {

	 return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            // console.log('FOCUS ON', element);

        	// element[0].focus();
		      scope.$on(attrs.focusOn, function(e) {
                if(element && element.length) {
		          element[0].focus();
                }
		      });
		   }
		}
});


app.directive('focusMe', ['$timeout', '$parse', function ($timeout, $parse) {
    return {
        //scope: true,   // optionally create a child scope
        link: function (scope, element, attrs) {

            console.log('FOCUS ME', element);

            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                console.log('value=', value);
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
            // to address @blesh's comment, set attribute value to 'false'
            // on blur event:
            element.bind('blur', function () {
                console.log('blur');
                scope.$apply(model.assign(scope, false));
            });
        }
    };
}]);