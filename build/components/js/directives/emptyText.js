app.directive('emptyText', function() {

    return {
        restrict: 'E',
        replace: true,
        // Replace the div with our template
        templateUrl: 'views/ui/empty-text.html',
    };
});
