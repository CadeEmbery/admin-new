app.directive('contentListSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            type: '@ngType',
        },
        templateUrl: 'views/ui/object-list-select.html',
        controller: 'ContentListSelectController',
    };
});


//////////////////////////////////////////////////////////////////////////

app.controller('ContentListSelectController', function($scope, ModalService, $filter,  FluroAccess, FluroContent, $controller) {


    //Get all the availableOptions
    FluroContent.resource($scope.type).query({
        // simple: true,
        allDefinitions:true,
    })
    .$promise.then(function(res) {
        $scope.availableOptions = _.sortBy(res, function(item) {
            return item.title;
        });
    }, function(err) {
        $scope.availableOptions = [];
    });
    $scope.search = {};


    /////////////////////////////////////

    $scope.$watch('availableOptions', filterOptions);
    $scope.$watch('search.terms', filterOptions);
    function filterOptions() {

        var options = $scope.availableOptions;

        if($scope.search && $scope.search.terms) {
            options = $filter('filter')(options, {
                title: $scope.search.terms
            });

            console.log('FILTER AVAILABLE OPTIONS')
        }
        $scope.options = options;
    }

    //See whether a use is allowed to create new objects
    $scope.createEnabled = FluroAccess.can('create', $scope.type);


    console.log('Checking Create', $scope.type, $scope.createEnabled);

    /////////////////////////////////////

    $scope.create = function(typeToCreate) {
        if (!typeToCreate) {
            ModalService.create($scope.type, {}, newItemCreated)
        } else {
            ModalService.create(typeToCreate, {}, newItemCreated)
        }
    }

    if ($scope.type) {
        $scope.createText = 'Create new ' + $scope.type;
    } else {
        $scope.createText = 'Create new';
    }

    /////////////////////////////////////

    function newItemCreated(item) {
        if (item) {
            $scope.availableOptions.push(item);
            $scope.model.push(item);
        }
    }

    /////////////////////////////////

    $scope.toggle = function(item) {
        if ($scope.contains(item)) {
            $scope.deselect(item)
        } else {
            $scope.select(item);
        }
    }

    //////////////////////////////////

    $scope.select = function(item) {
        if (!$scope.contains(item)) {
            $scope.model.push(item)
        }
    }

    //////////////////////////////////

    $scope.deselect = function(item) {

        var i = _.find($scope.model, function(e) {
            if (_.isObject(e)) {
                return e._id == item._id;
            } else {
                return e == item._id;
            }

        });
        if (i) {
            _.pull($scope.model, i)
        }
    }

    //////////////////////////////////

    $scope.contains = function(item) {
        return _.some($scope.model, function(i) {
            if (_.isObject(i)) {
                return i._id == item._id;
            } else {
                return i == item._id;
            }
        });
    }

});