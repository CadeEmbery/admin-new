
app.filter('toArray', function() { return function(obj) {
    if (!(obj instanceof Object)) return obj;
    return _.map(obj, function(val, key) {
        return Object.defineProperty(val, '$key', {__proto__: null, value: key});
    });
}});



app.directive('permissionSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        replace: true,
        scope: {
            model: '=ngModel',
            options: '=ngPermissionOptions',
            title: '@title',
        },
        templateUrl: 'views/ui/permission-select.html',
        controller: 'PermissionSelectController',
    };
});


app.controller('PermissionSelectController', function($scope, $filter) {


    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }

    $scope.search = {}


    $scope.filtered = function(column) {
       return $filter('filter')(column, $scope.search.terms);
    }

    //////////////////////////////////////////////////////////

    $scope.add = function(item) {
        if (item) {
            $scope.model.push(item);
        }
        $scope.model = _.uniq($scope.model);
    }

    //////////////////////////////////////////////////////////

    $scope.selectAll = function(key) {
        var items = $scope.options[key].permissions;
        _.each(items, function(item) {
            $scope.add(item.value);
        })
    }

    //////////////////////////////////////////////////////////

    $scope.toggleAll = function(key) {
        if($scope.allSelected(key)) {
            $scope.deselectAll(key);
        } else {
            $scope.selectAll(key);
        }
    }

    //////////////////////////////////////////////////////////

    $scope.deselectAll = function(key) {

        var items = $scope.options[key].permissions;

        _.remove($scope.model, function(item) {
           return _.some(items, {value:item})
        })
    }


    

    //////////////////////////////////////////////////////////

    $scope.allSelected = function(key) {
        var items = $scope.options[key].permissions;
        return _.every(items, function(item) {
            return _.contains($scope.model, item.value);
        })
    }


    //////////////////////////////////////////////////////////

    $scope.countSelected = function(key) {
        var items = $scope.options[key].permissions;

        var count = _.filter(items, function(item) {
             return _.includes($scope.model, item.value);
        }).length;

        return count;
        /**
        return '('+count+'/'+items.length+')';
        // return _.count(items, function(item) {
           
        // })
        /**/
    }



    //////////////////////////////////////////////////////////

    $scope.remove = function(item) {
        _.pull($scope.model, item);
    }

    //////////////////////////////////////////////////////////

    $scope.contains = function(item) {
        return _.contains($scope.model, item);
    }

    //////////////////////////////////////////////////////////

    $scope.toggle = function(item) {
        if($scope.contains(item)) {
            $scope.remove(item);
        } else {
            $scope.add(item);
        }
    }
});
