

app.directive('avatar', function() {
    return {
        restrict: 'E',
        replace:true,
        scope: {
            id: '=avId', //Field information -- this is what the field looks like
            style: '@avStyle', //Host object we look for the data on (the data object) -- this is the data in the field
            cacheBuster:'=?'
        },
        template:'<div class="avatar"><img ng-src="{{$root.asset.avatarUrl(id, style, {w:200,h:200, cb:cacheBuster})}}"/></div>',
        link: function(scope, element, attrs) {
            if(!scope.style) {
                scope.style = 'contact';
            }
        }
    };
});