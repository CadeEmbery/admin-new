

app.directive('machineName', function($parse) {
    return {
        restrict: 'A',
        require: '^ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            ngModelCtrl.$viewChangeListeners.push(function() {
                /*Set model value differently based on the viewvalue entered*/
                var regexp = /[^a-zA-Z0-9-_]+/g;

                var val = ngModelCtrl.$viewValue.replace(regexp, '-');
                var cameled = _.camelCase(val);


                //$parse(attrs.ngModel).assign(scope, ngModelCtrl.$viewValue.replace(regexp, ''));
                $parse(attrs.ngModel).assign(scope, cameled);
                

            });
        }
    };
});



app.directive('triggerName', function($parse) {
    return {
        restrict: 'A',
        require: '^ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            ngModelCtrl.$viewChangeListeners.push(function() {
                var regexp = /[^a-zA-Z0-9-_]+/g;

                var val = ngModelCtrl.$viewValue.replace(regexp, '_');
                var cameled = val.toLowerCase().split('_').join('.');





                //$parse(attrs.ngModel).assign(scope, ngModelCtrl.$viewValue.replace(regexp, ''));
                $parse(attrs.ngModel).assign(scope, cameled);
                

            });
        }
    };
});