app.directive('includeColumnManager', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'views/ui/include-column-manager.html',
        controller: 'IncludeColumnManagerController',
    };
});

app.controller('IncludeColumnManagerController', function($scope) {

    if (!$scope.model) {
        $scope.model = [];
    }

    $scope.sortableOptions = {
        handle: ' .handle'
        // items: ' .panel:not(.panel-heading)'
        // axis: 'y'
    }

    $scope._proposed = {
        key:'Column ' + ($scope.model.length+1),
    }

    $scope.addColumn = function() {


        var column = angular.copy($scope._proposed);
        $scope.model.push(column);

        $scope._proposed = {
            key:'Column ' + ($scope.model.length + 1),
        }



        /*var exists = _.filter($scope.model, {key:column.key})

        if (!exists.length && column.key.length) {
            $scope.model.push(column);
            $scope._proposed = {}
        } else {
            $scope.model.push({});
            //console.log(column.key, 'Is already defined')
        }
        */

    }


    $scope.removeColumn = function(column) {
        _.pull($scope.model, column)
    }


})