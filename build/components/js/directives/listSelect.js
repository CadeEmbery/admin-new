app.directive('listSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            options: '=ngOptions',
        },
        templateUrl: 'views/ui/list-select.html',
        controller: 'ListSelectController',
    };
});



app.directive('valueListSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            options: '=',
        },
        templateUrl: 'views/ui/value-select.html',
        controller: 'ValueListSelectController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('ValueListSelectController', function($scope, $controller) {

    if(!$scope.options) {
        $scope.options = [];
    }

    if(!$scope.model) {
        $scope.model = [];
    }

    $scope.search = {};
    
    /////////////////////////////////

    $scope.toggle = function(item) {
        if ($scope.contains(item)) {
            $scope.deselect(item)
        } else {
            $scope.select(item);
        }
    }

    //////////////////////////////////

    $scope.select = function(option) {
        if (!$scope.contains(option)) {
            $scope.model.push(option.value)
        }
    }

    //////////////////////////////////

    $scope.deselect = function(option) {
        if($scope.contains(option)) {
            $scope.model = _.filter($scope.model, function(v) {
                return v != option.value;
            })
        }
    }

    //////////////////////////////////

    $scope.contains = function(option) {
        return _.includes($scope.model, option.value)
    }

});



//////////////////////////////////////////////////////////////////////////

app.controller('ListSelectController', function($scope, $controller) {

    if(!$scope.options) {
        $scope.options = [];
    }

    /////////////////////////////////

    $scope.toggle = function(item) {
        if ($scope.contains(item)) {
            $scope.deselect(item)
        } else {
            $scope.select(item);
        }
    }

    //////////////////////////////////

    $scope.select = function(item) {
        if (!$scope.contains(item)) {
            $scope.model.push(item)
        }
    }

    //////////////////////////////////

    $scope.deselect = function(item) {

        var i = _.find($scope.model, function(e) {
            if (_.isObject(e)) {
                return e._id == item._id;
            } else {
                return e == item._id;
            }

        });
        if (i) {
            _.pull($scope.model, i)
        }
    }

    //////////////////////////////////

    $scope.contains = function(item) {
        return _.some($scope.model, function(i) {
            if (_.isObject(i)) {
                return i._id == item._id;
            } else {
                return i == item._id;
            }
        });
    }

});

