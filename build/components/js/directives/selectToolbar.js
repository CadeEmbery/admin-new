app.directive('selectToolbar', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            plural: '=ngPlural',
            definitions: '=ngDefinitions',
            definition: '=ngDefinition',
            filtered: '=ngFiltered',
            type: '=ngType',
            queryMode: '=?',
            queryItem: '=?'
        },
        // Replace the div with our template
        templateUrl: 'views/ui/select-toolbar.html',
        controller: 'SelectToolbarController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('SelectToolbarController', function($scope, $uibModal, AccessPassService, $state, ModalService, TypeService, FluroContent, FluroAccess) {

    var definedName = $scope.type.path;

    var editAny = FluroAccess.can('edit any', $scope.type.path)
    var editOwn = FluroAccess.can('edit own', $scope.type.path)

    if ($scope.definition) {
        editAny = FluroAccess.can('edit any', $scope.definition.definitionName, $scope.definition.parentType)
        editOwn = FluroAccess.can('edit own', $scope.definition.definitionName, $scope.definition.parentType)
        definedName = $scope.definition.definitionName;
    }




    ///////////////////////////////////////////////


    $scope.sendMessage = function(model) {


        ////////////////////////////////////////////

        function messageSuccess(res) {
            console.log('Message Send Success', res);
        }

        ////////////////////////////////////////////

        function messageFail(res) {
            console.log('Message Send Fail', res)
        }

        ////////////////////////////////////////////

        var ids = _.map(model.items, function(item) {
            return item._id;
        })

        ModalService.message(ids, messageSuccess, messageFail)
    }

    ///////////////////////////////////////////////

    $scope.canUpdateFamilies = ((editAny || editOwn) && $scope.type.path == 'contact');

    ///////////////////////////////////////////////



    

    $scope.canSharePolicy = function() {

        if($scope.type.path != 'contact') {
            return;
        }


        var canGrantPolicy = FluroAccess.can('grant', 'policy');
        var canGrantOwnPolicy = FluroAccess.can('grant held', 'policy');



        return (canGrantPolicy || canGrantOwnPolicy);

    }

    ///////////////////////////////////////////////

    $scope.sharePolicy = function() {


        var contacts = $scope.model.items;

        ////////////////////////////////////////////////////

        contacts = _.chain(contacts)
        .compact()
        // .map(function(contact) {
        //     if(contact._id) {
        //         return contact._id;
        //     }

        //     return contact;
        // })
        .uniq(function(contact) {
            return contact._id;
        })
        .value();

        ////////////////////////////////////////////////////

        if(!contacts || !contacts.length) {
            console.log('No contacts selected');
            return;
        }

        ////////////////////////////////////////////////////

        return AccessPassService.shareToContacts(contacts);

        ////////////////////////////////////////////////////


        // ////////////////////////////////////////////////////

        // FluroContent.resource('contact').get({
        //     id: contact._id
        // }).$promise.then(function(item) {

        //     //Create new template
        //     var template = {
        //         //name: item.firstName + ' ' + item.lastName,
        //         firstName: item.firstName,
        //         lastName: item.lastName,
        //         collectionEmail: [item.emails[0]],
        //         realms: realms
        //     }

        //     //Popup and create
        //     ModalService.create('persona', {
        //         template: template
        //     }, function(result) {
        //         console.log('Success', result);
        //         $state.reload();
        //         $scope.model.deselectAll();

        //     }, function(result) {
        //         console.log('Fail', result)
        //     });
        // })
    




    }

    ///////////////////////////////////////////////


    $scope.canTag = ((editAny || editOwn) && definedName != 'tag' && definedName != 'realm' && definedName != 'user');

    ///////////////////////////////////////////////


    //Collect Permissions
    var createCollection = FluroAccess.can('create', 'collection');
    var editAnyCollection = FluroAccess.can('edit any', 'collection');
    var editOwnCollection = FluroAccess.can('edit own', 'collection');

    $scope.canCollect = ((createCollection || editAnyCollection || editOwnCollection) && definedName != 'user');

    ///////////////////////////////////////////////

    //Get all sub types for a particular parent type
    function getChildTypes(parentType) {
        return _.filter(TypeService.definedTypes, function(type) {
            return type.parentType == parentType;
        });
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////


    ///////////////////////////////////////////////

    //CHECK IF WE CAN ADD TO PROCESSES
    // var productTypes = getChildTypes('product');

    // var canEditProcesses = _.chain(productTypes)
    // .some(function(type) {
    //     //Edit Product Permissions
    //     var canCreate = FluroAccess.can('create', type.definitionName);
    //     var canEdit = FluroAccess.can('edit any', type.definitionName);
    //     var canEditOwn = FluroAccess.can('edit own', type.definitionName);
    //     return (canCreate || canEdit || canEditOwn);
    // })
    // .value();

    $scope.canAddToProcesses = (FluroAccess.canAccess('process') && definedName != 'user') && ($scope.type.path != 'process');
    
    $scope.canManageGroups = function() {
        var firstItem = _.first($scope.model.items);
        if($scope.type.path == 'contact' || firstItem._type == 'contact') {
            return true;
        }
    }




    ///////////////////////////////////////////////

    //Get all defined products
    var productTypes = getChildTypes('product');

    //Check if we can create any kind of product
    var canEditProductTypes = _.chain(productTypes)
        .some(function(type) {
            //Edit Product Permissions
            var canCreate = FluroAccess.can('create', type.definitionName); //type.parentType //if we add product derivative permissions
            var canEdit = FluroAccess.can('edit any', type.definitionName); //type.parentType //if we add product derivative permissions
            var canEditOwn = FluroAccess.can('edit own', type.definitionName); //type.parentType //if we add product derivative permissions
            return (canCreate || canEdit || canEditOwn);
        })
        .value();

    ///////////////////////////////////////////////

    switch ($scope.type.path) {
        case 'article':
        case 'asset':
        case 'audio':
        case 'video':
        case 'image':
        case 'code':
        case 'collection':
        case 'event':
        case 'package':
        case 'query':
            $scope.canEditProducts = canEditProductTypes;
            break;
    }

    ///////////////////////////////////////////////

    //Check if user can create mailouts
    // var canCreateMailout = FluroAccess.can('create', 'mailout');

    switch ($scope.type.path) {
        case 'contact':
        case 'team':
        case 'interaction':
        case 'checkin':
        case 'event':
        case 'collection':
        case 'query':
        case 'persona':
        case 'process':
        case 'ticket':



            // if ($scope.queryMode && $scope.filtered.length) {
            //     $scope.canMailout = false;

            // } else {
                var createableMailouts = _.chain(TypeService.definedTypes)
                    .filter(function(mailoutType) {
                        var correctParentType = mailoutType.parentType == 'mailout';
                        var canCreate = FluroAccess.can('create', mailoutType.definitionName, 'mailout');

                        return correctParentType && canCreate;
                    })
                    .value();



                // console.log('Can Create Mailout?', )
                if (createableMailouts && createableMailouts.length) {
                    $scope.canMailout = true;
                }
            // }


            break;
    }



    $scope.openMatrix = function() {

        // console.log('IDS', $scope.model.ids);

        $state.go('matrix', {events:$scope.model.ids});
    }

    ///////////////////////////////////////////////

    // //Edit Product Permissions
    // var createProduct = FluroAccess.can('create', 'product');
    // var editAnyProduct = FluroAccess.can('edit any', 'product');
    // var editOwnProduct = FluroAccess.can('edit own', 'product');


    ///////////////////////////////////////////////

    $scope.selectFiltered = function() {
        $scope.model.selectMultiple($scope.filtered);
    }

    ///////////////////////////////////////////////

    // console.log('TESTING')
    $scope.$watch('filtered.length', checkTotalSelection);
    $scope.$watch('model.items.length', checkTotalSelection);

    function checkTotalSelection() {
        // console.log('check total selection', $scope.filtered.length);

        var filteredIds = _.map($scope.filtered, function(item) {
            return item._id;
        });

        var difference = _.difference(filteredIds, $scope.model.ids);

        // console.log('Filtered', $scope.filtered.length, difference.length);

        $scope.notAllSelected = (difference.length > 0);

    }

    ///////////////////////////////////////////////


    $scope.createUser = function() {
        var contact = $scope.model.items[0];

        var realms = angular.copy(contact.realms);


        FluroContent.resource('contact').get({
            id: contact._id
        }).$promise.then(function(item) {

            //Create new template
            var template = {
                //name: item.firstName + ' ' + item.lastName,
                firstName: item.firstName,
                lastName: item.lastName,
                collectionEmail: [item.emails[0]],
                realms: realms
            }

            //Popup and create
            ModalService.create('persona', {
                template: template
            }, function(result) {
                console.log('Success', result);
                $state.reload();
                $scope.model.deselectAll();

            }, function(result) {
                console.log('Fail', result)
            });
        })
    }

    ///////////////////////////////////////////////

    $scope.canCreateUser = function() {
        if ($scope.model.length == 1 && $scope.type.path == 'contact') {
            return FluroAccess.can('create', 'persona');
        }
    }

    ///////////////////////////////////////////////

    $scope.duplicate = function() {

        var item = $scope.model.items[0];

        ModalService.edit(item, function(result) {
            console.log('Copied in modal reload state');
            $state.reload();
            $scope.model.deselectAll();
        }, function(result) {
            //console.log('Failed to copy in modal', result)
        }, true);


    }

    ///////////////////////////////////////////////

    $scope.canDuplicate = function() {

        if ($scope.model.length == 1) {

            var typeName = $scope.type.path;
            var item = $scope.model.items[0];

            if (item.definition) {
                typeName = item.definition;
            }

            return FluroAccess.can('create', typeName, $scope.type.path);


        }
    }



    ///////////////////////////////////////////////

    $scope.openGroupManager = function() {

        var contacts = $scope.model.items;

        var modalInstance = $uibModal.open({
            templateUrl: 'admin-group-modal/admin-group-modal.html',
            controller: 'GroupModalController',
            size:'lg',
            resolve: {
                contacts: function() {
                    return contacts;
                },
                groups: function() {
                    return FluroContent.resource('team').query({
                        fields: ['title', 'firstLine', 'realms', 'definition', '_type'],
                        allDefinitions: true
                    }).$promise;
                }
            }
        });



        // modalInstance.result.then(successCallback);

        /**
        ModalService.edit(item, function(result) {
            //console.log('Copied in modal', result)
            $state.reload();
            $scope.model.deselectAll();
        }, function(result) {
            //console.log('Failed to copy in modal', result)
        }, true);

        /**/


    }




    ///////////////////////////////////////////////

    $scope.canRecur = function() {
        if ($scope.model.length == 1) {
            var typeName = $scope.type.path;
            var item = $scope.model.items[0];

            if (item.definition) {
                typeName = item.definition;
            }

            return item._type == 'event' && FluroAccess.can('create', typeName, 'event');
        }
    }





    ///////////////////////////////////////////////

    $scope.recurEvent = function() {

        console.log('Recur Event')

        var eventItem = $scope.model.items[0];


        var modalInstance = $uibModal.open({
            templateUrl: 'event-recur-tools/event-recur-tools.html',
            controller: 'EventRecurToolsController',
            //size: 'lg',
            //backdrop: 'static',
            resolve: {
                event: function() {
                    return eventItem;
                }
            }
        });

        function successCallback(dates) {
            console.log('Success', dates);
        }

        modalInstance.result.then(successCallback);

        /**
        ModalService.edit(item, function(result) {
            //console.log('Copied in modal', result)
            $state.reload();
            $scope.model.deselectAll();
        }, function(result) {
            //console.log('Failed to copy in modal', result)
        }, true);

        /**/


    }



    /*
    //Enable Batch Tags, Realms, Collect
    $scope.enableBatchTags = true;
    $scope.enableBatchRealms = true;
    $scope.enableBatchCollect = true;


   
    switch($scope.type.path) {
        case 'realm':
            $scope.enableBatchRealms = false;
        case 'tag':
            $scope.enableBatchTags = false;
            $scope.enableBatchCollect = false;
        break;
        case 'collection':
            $scope.enableBatchCollect = false;
        break;
        default:
        break;
       
    }



    */

        console.log('TEST', $scope.type);


});