

'use strict';

app

//////////////////////////////////////////////////////////////////////////

.directive('valueSelect', function() {

    return {
        restrict: 'E',
        replace:true,
        scope: {
            model: '=ngModel',
            params: '=ngParams',
           // options: '=ngOptions',
            //maximum: '=ngMaximum',
            //minimum: '=ngMinimum',
        },
        templateUrl: 'views/ui/value-select.html',
        controller: 'ValueSelectController',
    };
})


//////////////////////////////////////////////////////////////////////////

.controller('ValueSelectController', function($scope, FluroCollection) {

    //Create a model if none exists
    if (!$scope.selectedItems) {
        $scope.selectedItems = [];
    }

     $scope.$watch('params', function() {

        if ($scope.params) {
            if ($scope.params.minimum) {
                $scope.collection.minimum = 
                $scope.minimum = $scope.params.minimum;
            }
            if ($scope.params.maximum) {
                $scope.collection.maximum = 
                $scope.maximum = $scope.params.maximum;
            }
            if ($scope.params.allowedValues) {
                $scope.allowedValues = $scope.params.allowedValues;
            }
            if ($scope.params.defaultValues) {
                $scope.defaultValues = $scope.params.defaultValues;
            }
        }
    }, true)

    /////////////////////////////////////

    if($scope.model) {
        if(_.isArray($scope.model)) {
            if($scope.maximum) {
                //take the maximum first items from the existing model
                $scope.selectedItems = _.take($scope.model, $scope.maximum);
            } else {
                $scope.selectedItems = angular.copy($scope.model);
            }
        } else {
            $scope.selectedItems = [$scope.model];
        }
    }

    /////////////////////////////////////

    //Switch for whether more items can be added
    $scope.addEnabled = function() {
        return ($scope.selectedItems.length < $scope.maximum);
    }

    /////////////////////////////////////

    //Search terms
    $scope.search = {}

    /////////////////////////////////////

    //Update the model when the selection changes
    $scope.$watch('selectedItems', function(items) {

        if(items.length < $scope.maximum) {
            $scope.addEnabled = true;
        } else {
            $scope.addEnabled = false;
        }

        if($scope.maximum == 1) {
            $scope.model = _.first(items);
        } else {
            $scope.model = items;
        }
    }, true);

    /////////////////////////////////////

    //Create a collection
    $scope.collection = new FluroCollection();
    $scope.collection.set($scope.selectedItems);

    ////////////////////////////////////////////////////

    //Item or ID
    $scope.toggle = $scope.collection.toggle; 
    $scope.contains = $scope.collection.contains;
    $scope.add = $scope.collection.add;
    $scope.only = $scope.collection.only;
    $scope.remove = $scope.collection.remove;

})
