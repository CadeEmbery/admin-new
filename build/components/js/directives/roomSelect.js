app.directive('roomSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            options: '=ngRoomOptions',
        },
        templateUrl: 'views/ui/room-select.html',
        controller: 'ListSelectController',
    };
});
