app.directive('defaultListView', function() {

    return {
        restrict: 'E',
        replace:true,
        // Replace the div with our template
        templateUrl: 'views/modes/default.html',
    };
});


app.directive('trashListView', function() {

    return {
        restrict: 'E',
        replace:true,
        // Replace the div with our template
        templateUrl: 'views/modes/trash.html',
    };
});


app.directive('eventListView', function() {

    return {
        restrict: 'E',
        replace:true,
        // Replace the div with our template
        templateUrl: 'views/modes/event-list.html',
    };
});


app.directive('accountListView', function() {

    return {
        restrict: 'E',
        replace:true,
        // Replace the div with our template
        templateUrl: 'views/modes/account-list.html',
    };
});
