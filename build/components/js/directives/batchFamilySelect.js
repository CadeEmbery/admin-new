app.directive('batchFamilySelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            selection: '=ngModel',
        },
        templateUrl: 'admin-family-select/admin-family-select.html',
        controller: 'BatchFamilySelectController',
    };
});

app.controller('BatchFamilySelectController', function($scope, $state, Batch, Notifications, $rootScope, CacheManager, Fluro, $http, FluroContent) {

    $scope.families = [];


    $scope.dynamicPopover = {
        templateUrl: 'views/ui/batch-family-popover.html',
    };

    $scope.proposed = {}


    /////////////////////////////////////

    $scope.applyFamilies = function() {

        var details = {
            ids: $scope.selection.ids,
            families: $scope.families,
        }

        Batch.addFamilies(details, done);
    }


    //////////////////////////////////////////////////////////

    function done(err, data) {

        // console.log('Batch done', data);
        if (err) {
            //console.log('DONE ERR', err)
            return Notifications.error(err);
        }

        ///////////////////////

        //Find all the caches we need to clear
        var caches = [];

        ///////////////////////

        //Refresh any caches that might be affected outside of this view
        if(data.result) {
            if(data.result.types && data.result.types.length) {
                caches.concat(data.result.types)
            }

            if(data.result.definitions && data.result.definitions.length) {
                caches.concat(data.result.definitions)
            }
        }

        ///////////////////////

        caches = _.uniq(caches);
        caches = _.compact(caches);

        _.each(caches, function(t) {
            CacheManager.clear(t);
        });

        ///////////////////////

        //Reload the state
        $state.reload();

        ///////////////////////

        //Deselect all
        $scope.selection.deselectAll();
        Notifications.status('Updated family of ' + data.success.length + ' contacts');

    }

    /**
    function done(err, data) {
        if (err) {
            Notifications.error(err);
        } else {



            Notifications.status('Families updated on ' + data.results.length + ' items');

            _.chain(data.results)
                .map(function(result) {
                    if (result.definition) {
                        return result.definition;
                    } else {
                        return result._type;
                    }
                })
                .uniq()
                .each(function(type) {
                    console.log('Clear Cache', type)
                    CacheManager.clear(type);
                })
                .value();


            $state.reload();


            //Deselect
            //$scope.selection.deselectAll();
        }
    }
    /**/

    /////////////////////////////////////

    $scope.removeFamilies = function() {
        var details = {
            ids: $scope.selection.ids,
            families: $scope.families,
        }

        Batch.removeFamilies(details, done);
    }

    /////////////////////////////////////

    $scope.add = function(item) {
        $scope.families.push(item);
        $scope.proposed = {};
    }

    /////////////////////////////////////

    $scope.deselect = function(item) {
        _.pull($scope.families, item);
    }

    /////////////////////////////////////

   $scope.getFamilies = function(val) {

        var url = Fluro.apiURL + '/content/family/search/' + val;
        return $http.get(url, {
            ignoreLoadingBar: true,
            params: {
                limit: 20,
            }
        }).then(function(response) {
            var results = response.data;
            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.model, {
                    '_id': item._id
                });
                if (!exists) {
                    filtered.push(item);
                }
                return filtered;
            }, [])
        });

    };

    /////////////////////////////////////

    $scope.create = function() {

       
        var selectedRealms = _.chain($scope.selection.items)
        .map(function(contact) {
            return contact.realms;
        })
        .flatten()
        .map(function(realmID) {
            if(realmID._id) {
                return realmID._id;
            }

            return realmID
        })
        .compact()
        .uniq()
        .value();
        
        /////////////////////////////////////////

        var details = {
            title: $scope.proposed.value,
            realms:selectedRealms,
        }

        //Immediately create a family   
        FluroContent.resource('family').save(details, function(family) {

            //$scope.families.push(family);
            $scope.families.push(family);

            //We need to clear the family cache now
            CacheManager.clear('family');

            //Clear
            $scope.proposed = {}
        }, function(data) {
            Notifications.error(data);
        });
    }


    /**/



});