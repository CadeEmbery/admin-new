app.directive('includeSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            type: '=ngType',
        },
        templateUrl: 'views/ui/include-select.html',
        controller: 'includeSelectController',
    };
});



app.directive('includeEdit', function($compile) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'views/ui/include-edit.html',
        controller: 'includeEditController',
        /*
        
        link: function($scope, $element, $attrs) {
            //if (angular.isArray($scope.member.children)) {

            //Create the template
            var cTemplate = $compile('<include-select ng-model="model.includes">')($scope);
            $element.find('.nested').append(cTemplate);
        }

        */
    
    };
});



/*
app.directive('includeWrapperEdit', function($compile) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'views/ui/include-wrapper-edit.html',
        controller: 'includeWrapperEditController',
        link: function($scope, $element, $attrs) {
            //if (angular.isArray($scope.member.children)) {

            //Create the template
            var cTemplate = $compile('<include-select ng-model="model.includes">')($scope);
            $element.find('.nested').append(cTemplate);
        }
    
    };
});



//////////////////////////////////////////////////////////////////////////

app.controller('includeWrapperEditController', function($scope) {

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = {};
    }


    if(!$scope.model.includes) {
        $scope.model.includes = [];
    }

});

*/


//////////////////////////////////////////////////////////////////////////

app.controller('includeEditController', function($scope, $http) {

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = {};
    }

/*
    if(!$scope.model.includes) {
        $scope.model.includes = [];
    }
    */


    $scope.sortableOptions = {
        columnKey:'content',
        // items: ' .panel:not(.panel-heading)'
        // axis: 'y'
    }

});


//////////////////////////////////////////////////////////////////////////

app.controller('includeSelectController', function($scope, $http) {

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }


    /*
    $scope.dragFinish = function($index) {

        console.log('DRAG FINISH', $index);
        $scope.model.splice($index, 1)
    }
    */
   
   $scope.addColumn = function(include) {
    if(!include.columns) {
        include.columns = []
    }

    include.columns.push({})
   }


    $scope.sortableOptions = {
        columnKey:'include',
        accept: function (sourceItemHandleScope, destSortableScope) {
            //console.log(sourceItemHandleScope.$parent.sortableOptions, destSortableScope);

            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;;
          //return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
        }
    }


    $scope.removeInclude = function(include) {
        _.pull($scope.model, include);
    }

    $scope.addInclude = function() {
        $scope.model.push({})
    }
});