app.directive('locationMap', function($window, $timeout) {
    return {
        restrict: "E",
        replace: true,
        scope: {
            item: '=ngLocation',
        },
        //templateUrl: 'views/ui/location-map.html',
        template: '<div class="map-outer"><div class="map-inner"></div></div>',
        link: function($scope, $element, $attrs) {

            //Map Variables
            var mapOptions;
            var map;
            var markers = [];
            var geocoder;


            ////////////////////////////////////

            //Initialize Map
            var initialize = function() {
                mapOptions = {
                    zoom: 15,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    //disableDefaultUI: true,
                    maxZoom: 15,
                    //scrollwheel: false,
                    //draggable: false,
                };

                //Create the Geocoder
                geocoder = new google.maps.Geocoder();

                //Create the map
                map = new google.maps.Map($element.find('.map-inner').get(0), mapOptions);

                //If there is an item defined
                if ($scope.item) {
                    //Listen for click
                    google.maps.event.addListener(map, 'click', function(event) {
                        map.panTo(event.latLng);
                        setLatLong(event.latLng);
                    });
                }

                //Listen for window resize
                angular.element($window).bind('resize', fitMap);
            };




            ////////////////////////////////////

            function setLatLong(pos) {

                //Get the keys
                var keys = _.keys(pos);

                $timeout(function() {
                    clearMarkers();



                    //Get the lat and longitude
                    $scope.item.latitude = pos.lat();//[keys[0]];
                    $scope.item.longitude = pos.lng();//pos[keys[1]];
                    /*
				    
				    $scope.model[0].latitude = location.
				    var marker = new google.maps.Marker({
				        position: location, 
				        map: map
				    });
*/
                })
            }

            ////////////////////////////////////

            //Create Marker


            function createMarker(location) {

                if (location.latitude && location.longitude) {
                    var position = new google.maps.LatLng(location.latitude, location.longitude);
                    map.setCenter(position);

                    var marker = new google.maps.Marker({
                        map: map,
                        position: position,
                        title: location.title
                    });

                    markers.push(marker);
                }
            }

            ////////////////////////////////////

            function clearMarkers() {
                _.each(markers, function(marker) {
                    marker.setMap(null);
                })

            }

            ////////////////////////////////////

            $scope.$watch("item.latitude + item.longitude", function() {

                if (!$scope.item) {
                    return;
                }



                if ($scope.item.latitude && $scope.item.longitude) {
                    //Clear markers
                    clearMarkers();

                    //Create marker
                    createMarker($scope.item);
                    fitMap();

                } else {

                	//Find the lat and long by searching the address
                	
                    //Clear markers
                    clearMarkers();

                    //////////////////////////////

                    var stringAddress = '';

                    if ($scope.item.addressLine1) {
                        stringAddress += $scope.item.addressLine1 + ', ';
                    }

                    if ($scope.item.suburb) {
                        stringAddress += $scope.item.suburb + ', ';
                    }

                    if ($scope.item.state) {
                        stringAddress += $scope.item.state + ' ';
                    }

                    if ($scope.item.postalCode) {
                        stringAddress += $scope.item.postalCode + ' ';
                    }

                    if ($scope.item.country) {
                        stringAddress += $scope.item.country;
                    }


                    console.log('Find by address', stringAddress);

                    //////////////////////////////


                    geocoder.geocode({
                            'address': stringAddress,
                        },
                        function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var found = results[0];

                                if (found) {
                                    //Set the location according to the address
                                    setLatLong(found.geometry.location);
                                }
                            }
                        })

                }



            });

            ////////////////////////////////////


            var fitMap = function() {
                var bounds = new google.maps.LatLngBounds();

                if ($scope.item) {
                    var myLatLng = new google.maps.LatLng($scope.item.latitude, $scope.item.longitude);
                    bounds.extend(myLatLng);
                } else {
                    _.each(markers, function(marker) {
                        bounds.extend(marker.position)
                        /*
                        if (location.latitude && location.longitude) {
                            var myLatLng = new google.maps.LatLng(location.latitude, location.longitude);
                            bounds.extend(myLatLng);
                        }
                        */
                    });
                }

                map.fitBounds(bounds);

            }

            //Initialize the map
            initialize();

        },
    };
});

/*

app.directive('addressMap', function() {
    return {
        restrict: "A",
        template: "<div id='addressMap'></div>",
        scope: {
            address: "=",
        },
        controller: function($scope) {
            var geocoder;
            var latlng;
            var map;
            var marker;
            var initialize = function() {
                geocoder = new google.maps.Geocoder();
                latlng = new google.maps.LatLng(-34.397, 150.644);
                var mapOptions = {
                    zoom: 15,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById('addressMap'), mapOptions);
            };
            var markAdressToMap = function() {
                geocoder.geocode({
                        'address': $scope.address
                    },
                    function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            map.setCenter(results[0].geometry.location);
                            marker = new google.maps.Marker({
                                map: map,
                                position: results[0].geometry.location
                            });
                        }
                    });
            };
            $scope.$watch("address", function() {
                if ($scope.address != undefined) {
                    markAdressToMap();
                }
            });
            initialize();
        },
    };
});

*/





/*

app.controller('LocationMapController', function($scope) {


   
    var map;


    $scope.$on('mapInitialized', function(event, evtMap) {


        






        /*
        console.log('WOOT')
        map = evtMap;


        $scope.addMarker = function(event) {
            var ll = event.latLng;
            $scope.positions.push({
                lat: ll.lat(),
                lng: ll.lng()
            });
        }

        $scope.deleteMarkers = function() {
            $scope.positions = [];
        };

        $scope.mapClick = function(e) {



            markers = {};

            var marker = new google.maps.Marker({
                position: e.latLng,
                map: map
            });


            map.panTo(e.latLng);
        }
        

});
});

*/