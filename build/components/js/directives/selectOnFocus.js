app.directive('selectOnFocus', function($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var focusedElement = null;


            function selectElementContents(el) {
                var range = document.createRange();
                range.selectNodeContents(el);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            }


            element.on('focus', function() {


                var self = this;
                if (focusedElement != self) {
                    focusedElement = self;
                    $timeout(function() {

                        if(self.select) {
                            self.select();
                        } else {
                            selectElementContents(self);
                        }
                    }, 10);
                }
            });

            element.on('blur', function() {
                focusedElement = null;
            });
        }
    }
});