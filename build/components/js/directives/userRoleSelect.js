app.directive('userRoleSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'views/ui/list-select.html',
        controller: 'UserRoleSelectController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('UserRoleSelectController', function($scope, FluroAccess, Selection, ModalService, FluroContent, $controller) {

    $scope.search = {};

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }

    $scope.createText = 'Create new role';

    /////////////////////////////////////

    //See whether a use is allowed to create new objects
    $scope.createEnabled = FluroAccess.can('create', 'role');   

    /////////////////////////////////////


    $scope.create = function() {
        ModalService.create('role', {}, function(res) {
            $scope.options.push(res);
            $scope.model.push(res);
        })
    }

    //////////////////////////////////

    $scope.toggle = function(item) {
        if ($scope.contains(item)) {
            $scope.deselect(item)
        } else {
            $scope.select(item);
        }
    }

    //////////////////////////////////

    $scope.select = function(item) {
        if (!$scope.contains(item)) {
            $scope.model.push(item)
        }
    }

    //////////////////////////////////

    $scope.deselect = function(item) {

        var i = _.find($scope.model, function(e) {
            if (_.isObject(e)) {
                return e._id == item._id;
            } else {
                return e == item._id;
            }

        });
        if (i) {
            _.pull($scope.model, i)
        }
    }

    //////////////////////////////////

    $scope.contains = function(item) {
        return _.some($scope.model, function(i) {
            if (_.isObject(i)) {
                return i._id == item._id;
            } else {
                return i == item._id;
            }
        });
    }



    /*
    $scope.create = function() {
        var createInstance = ModalController.open({
            path: 'role',
            singular: 'Role',
            plural: 'Roles',
        }, null, function(result) {

            $scope.options.push(result);
            $scope.model.push(result);
        });

    }
    */

    //////////////////////////////////////////////////////////

    //Get all the options
    FluroContent.resource('role').query({
        simple: true,
        sort:'title',
        searchInheritable:true,
    }, function(res) {
        $scope.options = res;

        //If there is only one option stick it in the options straight away
        if ($scope.options.length == 1) {
            $scope.model.push($scope.options[0]);
        }
    });
});