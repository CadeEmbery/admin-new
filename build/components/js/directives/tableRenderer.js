app.directive('tableRenderer', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            column: '<ngColumn',
            item: '<ngItem',
            definition: '<ngDefinition',
        },
        templateUrl: 'views/ui/renderer.html',
        controller: 'TableRendererController',
    };
});


/////////////////////////////////////////////////////////////////

app.controller('TableRendererController', function($scope, $rootScope, FluroContent, Notifications, $filter, Tools) {


    /////////////////////////////////////////////////////////////////

    var value = _.get($scope.item, $scope.column.key); //Tools.getDeepProperty($scope.item, $scope.column.key);

    // console.log('INITIAL VALUE', $scope.item, value)
    /////////////////////////////////////////////////////////////////

    if (!$scope.column.renderer || !$scope.column.renderer.length) {


        var isArray = _.isArray(value);
        var isObject = _.isObject(value);

        if (isArray) {

            // var string = '';

            var string = _.chain(value)
                .compact()
                .map(function(v) {
                    if (_.isObject(v)) {

                        if (v.title || v.name) {

                            if (v.color && v.bgColor) {
                                return '<span class="inline-tag" style="color:' + v.color + '; background-color:' + v.bgColor + '">' + (v.title || v.name) + '</span>';
                            } else {
                                // console.log('VALUE', $scope.column.key, value, $scope.column.renderer);
                                return '<span class="inline-tag">' + (v.title || v.name) + '</span>';
                            }
                        }
                    } else {
                        //Filter
                        if ($scope.column.filter) {
                            v = $filter($scope.column.filter)(v);
                        }

                        if (v && v.color && v.bgColor) {
                            return '<span class="inline-tag" style="color:' + v.color + '; background-color:' + v.bgColor + '">' + v + '</span>';
                        } else {
                            return '<span class="inline-tag">' + v + '</span>';
                        }
                    }
                })
                .value().join('');

            // console.log('VALUE', $scope.column.key, string);
            $scope.value = string;

        } else if (isObject) {
            if (value._type == 'event') {

                return $scope.value = '<div class="table-link" ng-click="$root.modal.view({_id:\'' + value._id + '\', _type:\'' + (value._type || '') + '\', definition:\'' + (value.definition || '') + '\'})">' + value.title + '<span class="text-muted"> // ' + $filter('date')(value.startDate) + '</span></div>';
            }


            var readableText = value.title || value.name;

            if (value._id) {
                return $scope.value = '<div class="table-link" ng-click="$root.modal.view({_id:\'' + value._id + '\', _type:\'' + (value._type || '') + '\', definition:\'' + (value.definition || '') + '\'})">' + readableText + '</div>';
            } else {
                return $scope.value = readableText
            }

        } else {
            $scope.value = value;
        }


    } else {
        if ($scope.column.filter) {
            $scope.value = $filter($scope.column.filter)(value);
        } else {


            //////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////


            var isSubField = _.startsWith($scope.column.renderer, 'field.');


            //If its a sub field
            if (isSubField) {


                var string = $scope.column.renderer;
                var pathKey = string.substring(string.indexOf(".") + 1);
                //////////////////////////////////////////////


                var printValue;

                //////////////////////////////////////////////

                //If the source object is a single item
                if (_.isObject(value)) {
                    printValue = _.get(value, pathKey);
                }


                //If the source object is a single item
                if (_.isArray(value)) {
                    printValue = _.map(value, function(valueItem) {
                        return _.get(valueItem, pathKey);
                    });
                }

                //////////////////////////////////////////////

                if (!printValue) {
                    return;
                }

                //Now we have the actual values, time to figure out how to render them

                var renderedPrintValue = _.chain(printValue)
                    .flattenDeep()
                    .uniq()
                    .value()
                    .join(', ');

                //////////////////////////////////////////////

                return $scope.value = renderedPrintValue;
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////

            switch ($scope.column.renderer) {

                // case 'columns':


                //     $scope.renderableColumns = _.chain($scope.item.columns)
                //     .map(function(column) {
                //         switch(column.renderer) {
                //             case 'columns':
                //                 var columns = unwindColumn(column.key)
                //                 console.log('FOUND COLUMNS', columns);
                //                 return columns;
                //             break;
                //         }

                //         return column;
                //     })
                //     .flatten()
                //     .value();



                //     return $scope.value = '<span class="small">' + _.map(value, function(contact) {
                //         return $filter('capitalizename')(contact.title);
                //     }).join(', ') + '</span>';


                // break;

                case 'contacts':
                    // return $scope.value = '<span class="small">' + _.map(value, function(contact) {
                    //     return $filter('capitalizename')(contact.title);
                    // }).join(', ') + '</span>';

                    return $scope.value = '<span class="small">' + _.map(value, function(contact) {
                        return contact.title;
                    }).join(', ') + '</span>';

                    break;
                case 'json':
                    return $scope.value = '<pre>' + JSON.stringify(value, null, 2) + '</pre>';

                    break;

                case 'permissionSet':
                    if (value && value.length) {

                        // var summary = '';
                        // _.each(value, function(v) {
                        //     var roleNames = [];
                        //     var realmNames = [];

                        //     _.each(v.roles, function(role) {
                        //         roleNames.push(role.title);
                        //     })

                        //     _.each(v.realms, function(realm) {
                        //         realmNames.push(realm.title);
                        //     });


                        //     ////////////////////////////////////////

                        //     if(!roleNames.length && !realmNames.length) {
                        //         return;
                        //     }

                        //     ////////////////////////////////////////

                        //     var roleSummary = '';
                        //     if(roleNames.length <= 2) {
                        //         roleSummary += roleNames.join(', ')
                        //     } else {
                        //         roleSummary += (roleNames.length) + ' roles';
                        //     }

                        //     var realmSummary = '';
                        //     if(realmNames.length <= 2) {
                        //         realmSummary += realmNames.join(', ')
                        //     } else {
                        //         realmSummary += (realmNames.length) + ' realms <br/>';
                        //     }
                        //     ////////////////////////////////////////

                        //     summary +=  '<div class="text-left">' + roleSummary + ' in ' + realmSummary + '</div>';

                        // });

                        return $scope.value = '<span class="inline-tag">' + value.length + ' additional</span>';
                    }

                    return;

                    break;
                case 'time':

                    //console.log('vALUE', value)
                    if (value) {
                        return $scope.value = $filter('formatDate')(value, 'g:ia');
                    }
                    break;
                case 'processStatus':


                    return $scope.value = '<div class="process-status-pill ' + value + '">' + value + '</div>';

                    break;
                case 'definitionName':
                    return $scope.value = $rootScope.definitionName(value);
                    break;
                case 'amountDue':


                    var amount = $scope.item.amount;

                    if (!amount) {
                        return $scope.value = '';
                    }


                    var amountDue = $scope.item.amountDue;
                    var readableAmount = (parseInt(amount) / 100).toFixed(2);
                    var readableAmountDue = (parseInt(amountDue) / 100).toFixed(2);

                    if (amountDue) {
                        return $scope.value = '<div uib-tooltip="$' + readableAmountDue + ' yet to be paid" class="text-center brand-danger">-$' + readableAmountDue + '</div>'
                    } else {
                        return $scope.value = '<div  class="text-center brand-success">$' + readableAmount + '</div>'
                    }

                    break;
                case 'processProgress':


                    //Add the little results circles for each state

                    var states = _.get($scope.definition, 'data.states');
                    var item = $scope.item;

                    var mode = _.get($scope.definition, 'data.mode');
                    if (mode == 'lanes') {

                        var currentState = _.find(states, { key: item.state });
                        var currentResult = item.results[item.state];

                        var className = 'default';

                        switch (currentResult) {
                            case 'complete':
                                //className = 'brand-success';
                                break;
                            case 'failed':
                                className = 'brand-danger';
                                break;
                            case 'pending':
                                className = 'brand-warning';
                                break;
                        }
                        return $scope.value = '<div class="' + className + '">' + _.get(currentState, 'title') + '</div>';
                    }


                    var circles = _.chain(states)
                        .map(function(state) {

                            if (state.style == 'waiting') {
                                return;
                            }

                            var output = {
                                title: state.title,
                                key: state.key,
                            }

                            if (item.results) {
                                output.result = item.results[state.key];
                            }

                            switch (output.result) {
                                case 'complete':
                                    output.icon = 'fas fa-check-circle brand-success';
                                    break;
                                case 'failed':
                                    output.icon = 'fas fa-exclamation-circle brand-danger';
                                    break;
                                case 'pending':
                                    output.icon = 'fas fa-minus-circle brand-warning';
                                    break;
                                default:

                                    //If its the current state then add it in a circle
                                    if (item.state == state.key) {
                                        output.icon = 'far fa-dot-circle text-muted';
                                    } else {
                                        output.icon = 'far fa-circle text-muted';
                                    }
                                    break;
                            }
                            return output;
                        })
                        .compact()
                        .map(function(dot) {
                            return '<i class="' + dot.icon + '"></i>';
                        })
                        .value()



                    return $scope.value = '<div class="circles small">' + circles.join('') + '</div>';
                    // return '<div class="circles small"><i class="{{processState.icon}}" ng-repeat="processState in item.stateResults track by processState.key"></i></div>';


                    break;
                case 'duration':

                    //console.log('vALUE', value)
                    if (value) {
                        return $scope.value = '<span class="small text-muted">' + $filter('videoDuration')(value) + '</span>';
                    }
                    break;
                case 'userType':
                    // //console.log('USER TYPE', $scope.item);
                    if ($scope.item.username && $scope.item.username.length) {
                        return $scope.value = 'managed'
                    } else {
                        if ($scope.item.user || $scope.item.userEmail) {
                            return $scope.value = 'connected';
                        } else {
                            if ($scope.item.collectionEmail && $scope.item.collectionEmail.length) {
                                return $scope.value = 'awaiting collection';
                            }
                        }
                    }
                    break;
                case 'transactionUser':
                    // //console.log('USER TYPE', $scope.item);
                    if ($scope.item.user && $scope.item.user.name) {
                        return $scope.value = '<div class="no-wrap">' + $scope.item.user.name + '</div>';
                    }

                    if ($scope.item.persona && $scope.item.persona.title) {
                        return $scope.value = '<div class="no-wrap">' + $scope.item.persona.title + '</div>';
                    }

                    if ($scope.item.contact && $scope.item.contact.title) {
                        return $scope.value = '<div class="no-wrap">' + $scope.item.contact.title + '</div>';
                    }
                    break;

                case 'interactionFirstName':
                    if ($scope.item.contact) {
                        return $scope.value = $filter('capitalizename')($scope.item.contact.firstName);
                    } else {
                        if ($scope.item.data) {
                            return $scope.value = $filter('capitalizename')($scope.item.data._firstName);
                        }
                    }
                    break;
                case 'interactionLastName':
                    if ($scope.item.contact) {
                        return $scope.value = $filter('capitalizename')($scope.item.contact.lastName);
                    } else {
                        if ($scope.item.data) {
                            return $scope.value = $filter('capitalizename')($scope.item.data._lastName);
                        }
                    }
                    break;
                case 'transactionEmail':
                    // //console.log('USER TYPE', $scope.item);
                    if ($scope.item.user && $scope.item.user.email) {
                        return $scope.value = '<div class="no-wrap">' + $scope.item.user.email + '</div>';
                    }

                    if ($scope.item.persona && $scope.item.persona.collectionEmail) {
                        return $scope.value = '<div class="no-wrap">' + $scope.item.persona.collectionEmail + '</div>';
                    }

                    if ($scope.item.persona && $scope.item.persona.username) {
                        return $scope.value = '<div class="no-wrap">' + $scope.item.persona.username + '</div>';
                    }
                    break;
                case 'license':
                    if (value) {

                        if (value.length) {
                            if (value.length > 1) {
                                return $scope.value = value.length;
                            } else {
                                return $scope.value = value[0].name;
                            }
                        }
                    }
                    break;
                case 'managedLicense':
                    if (value) {
                        if (value.length) {
                            if (value.length > 1) {
                                return $scope.value = value.length;
                            } else {
                                return $scope.value = value[0].title;
                            }
                        }

                    }
                    break;

                case 'collectButton':
                    // if (value) {

                    $scope.collect = function(item) {
                        if (!item || item.collected) {
                            return;
                        }
                        // console.log('COLLECT', item)
                        FluroContent.endpoint('tickets/collect/' + item._id).get().$promise.then(function(res) {
                            item.collected = true;
                            Notifications.status(item.title + ' was collected');
                        })
                    }

                    return $scope.value = '<div class="toggle-switch collection-switch" ng-click="collect(item)" ng-class="{\'on\':item.collected}"></div>';

                    // <div>{{:item.collected}}</div>';
                    // }
                    break;
                case 'realmDots':

                    if (value) {
                        $scope.realms = value;
                        return $scope.value = '<div class="small text-center dots"><i ng-repeat="realm in realms track by realm._id" ng-show="!realm._discriminatorType" tooltip-append-to-body="true" uib-tooltip="{{realm.title}}" class="fas fa-circle " style="color: {{realm.bgColor}};"></i></div>';
                    }



                    break;
                case 'checkinDots':

                    if (value) {

                        if (_.isNumber(value)) {
                            var array = [];
                            array.length = parseInt(value);
                            $scope.dots = _.fill(array, value);
                        }

                        if (_.isArray(value)) {
                            $scope.dots = value;
                        }




                        return $scope.value = '<div class="small text-center cohort-dots"><span ng-repeat="dot in dots track by $index" tooltip-append-to-body="true" uib-tooltip="{{dot.title}}" class="cohort-dot" style="background-color: {{dot.bgColor || \'#000\'}};"></span></div>';
                        // return $scope.value = '<pre>{{dots | json}}</pre>';
                    }



                    break;

                case 'attendance':

                    if (value) {
                        var renderValue = '';

                        if (!value.length) {
                            return $scope.value = renderValue;
                        }


                        renderValue += '<div class="attendance-timeline">';

                        var strings = _.map(value, function(entry) {
                            if (entry) {
                                return '<span uib-tooltip="' + entry.title + '"><i class="far fa-check"></i></span>'
                            } else {
                                return '<span class="text-muted">-</span>'; //<i class="far fa-circle text-muted"></i></span>'
                            }
                        })

                        //Join all the string together
                        renderValue += strings.join('');

                        renderValue += '</div<';

                        $scope.value = renderValue;
                        return $scope.value = renderValue;
                        // <div class="small text-center circles"><span ng-repeat="event in item['+"'"+ $scope.column.key +"'" +']" tooltip-append-to-body="true" uib-tooltip="{{event.title}}" ><i class="far fa-check"></i></span></div>';
                    }





                    break;
                case 'notifications':

                    var notificationDescriptions = _.map(value, function(notification) {

                        var triggerName = notification.trigger;

                        /////////////////////////////////////

                        switch (triggerName) {
                            case 'team.join':
                                triggerName = 'A new member joins the group';
                                break;
                            case 'team.leave':
                                triggerName = 'A member leaves the group';
                                break;
                            case 'content.edit':
                                triggerName = 'Group information is updated';
                                break;
                            case 'confirmation.confirmed':
                                triggerName = 'A member confirms an assignment';
                                break;
                            case 'confirmation.unavailable':
                                triggerName = 'A member declines an assignment';
                                break;
                            case 'contact.unavailability':
                                triggerName = 'A member updates their availability';
                                break;
                        }

                        /////////////////////////////////////

                        return '<p>Notify ' + _.map(notification.assignments, _.startCase).join(', ') + ' when: <br/>' + triggerName + '<p>';

                        return str;

                    })


                    $scope.tooltip = '<div class="text-left">' + notificationDescriptions.join('') + '</div>';



                    if (value && value.length) {
                        return $scope.value = '<div class="text-center small text-muted" uib-tooltip-html="tooltip  | trusted">' + value.length + '</div>'
                    }

                    break;

                case 'age':
                    if ($scope.item.age) {
                        return $scope.value = '<div class="text-center">' + $scope.item.age + '</div>';
                    }

                    ////////////////////////////////////////

                    if (!value) {
                        return $scope.value = '<div class="text-center small text-muted">Unknown</div>';
                    }

                    ////////////////////////////////////////

                    var renderValue = $filter('age')(value);
                    if (renderValue) {
                        return $scope.value = '<div class="text-center">' + renderValue + '</div>';
                    } else {
                        return $scope.value = '<div class="text-center small text-muted">Unknown</div>';
                    }

                    break;
                    // case 'capitalize':
                    //     if (value) {
                    //         var renderValue = $filter('capitalizename')(value);

                    //         return $scope.value = renderValue;

                    //     } else {
                    //         return '';
                    //     }
                    //     break;
                case 'thumbnail':
                    var itemID; // = $scope.item._id;

                    // if($scope.item._type == 'image')

                    if (value) {
                        if (_.isString(value)) {
                            itemID = value;
                        }

                        if (_.isObject(value)) {
                            itemID = value._id;
                        }

                        if (_.isArray(value)) {
                            itemID = value[0];
                        }
                    }

                    ///////////////////////////

                    if (itemID && itemID._id) {
                        itemID = itemID._id;
                    }





                    var thumbnailUrl = $rootScope.asset.imageUrl(itemID, 100);
                    var fullUrl = $rootScope.asset.imageUrl(itemID, 768);

                    if (itemID) {
                        // console.log('THUMBNAIL ID', itemID)
                        return $scope.value = '<div ng-mouseover="item.hover = true" ng-mouseout="item.hover = false"><div class="hover-large" ng-if="item.hover"><img preload-image aspect="{{(item.height / item.width) * 100}}" src="' + fullUrl + '"/></div><img preload-image class="img-responsive" src="' + thumbnailUrl + '"/></div>';
                    } else {
                        return;
                    }
                    //   return $scope.value = '<img ng-src="{{$root.asset.imageUrl(' + "'" + $scope.value + "'" + ')}}"/>';

                    break;
                case 'filesize':
                    if (value) {
                        return $scope.value = '<span class="small text-muted">' + $filter('filesize')(value) + '</span>';
                    }
                    break;
                case 'dimensions':
                    if (value) {
                        return $scope.value = '<span class="small text-muted">' + $scope.item.width + 'x' + $scope.item.height + '</span>';
                    }
                    break;
                case 'privacy':


                    switch (value) {
                        case 'public':
                            return $scope.value = '<div class="text-center"><i class="far fa-lock-open" tooltip-placement="bottom" tooltip-append-to-body="true" uib-tooltip="Public (Anyone can view)"></i></div>';
                            break;
                        default:
                            return $scope.value = '<div class="text-center"><i class="fas fa-lock text-muted" tooltip-placement="bottom" tooltip-append-to-body="true" uib-tooltip="Only users with access can view"></i></div>';
                            break;
                    }
                    break;
                case 'datetime':


                    if (value) {

                        var renderValue = $filter('formatDate')(value, 'g:ia - D j M Y');

                        var timeago = $filter('timeago')(value);
                        return $scope.value = '<div class="inline-date" tooltip-placement="bottom" tooltip-append-to-body="true" uib-tooltip="' + timeago + '"><i class="far fa-clock-o"></i><span>' + renderValue + '</span></div>';

                    }
                    break;
                case 'date':
                    if (value) {

                        var renderValue = $filter('formatDate')(value, 'j M Y');

                        var timeago = $filter('timeago')(value);
                        return $scope.value = '<div class="inline-date" tooltip-placement="bottom" tooltip-append-to-body="true" uib-tooltip="' + timeago + '"><i class="far fa-clock-o"></i><span>' + renderValue + '</span></div>';

                    }
                    // if (value) {
                    //     return $scope.value = $filter('formatDate')(value, 'j M Y');
                    // }
                    break;
                case 'year':
                    if (value) {
                        return $scope.value = $filter('formatDate')(value, 'Y');
                    }
                    break;
                case 'date_day':
                    if (value) {
                        return $scope.value = $filter('formatDate')(value, 'j');
                    }
                    break;
                case 'date_weekday':
                    if (value) {
                        return $scope.value = $filter('formatDate')(value, 'l');
                    }
                    break;
                case 'date_monthname':
                    if (value) {
                        return $scope.value = $filter('formatDate')(value, 'F');
                    }
                    break;
                case 'currency':
                    // if (value) {
                    return $scope.value = '<div class="text-center">' + (parseInt(value) / 100).toFixed(2); + '</div>'

                    break;
                case 'generateInviteCode':


                    return $scope.value = '<get-invite-code></get-invite-code>'


                    break;
                    // case 'formShare':

                    //     if($scope.item._type == 'definition' && $scope.item.privacy == 'public' && $scope.item.parentType == 'interaction') {
                    //     return $scope.value = '<div class="form-share"><a class="btn btn-block btn-default btn-sm" ng-click="$root.formShareService.share(item)">Share</a></div>';
                    //     }
                    //     <a class="btn btn-block btn-default btn-sm" ng-click="$root.formShareService.share(item)">
                    //         Share
                    //         <!-- <i class="far fa-share"></i> -->
                    //     </a>
                    // </div>'


                    break;
                case 'number':
                    if (value) {
                        return $scope.value = '<div class="text-center small">' + value + '</div>'
                    } else {
                        return $scope.value = '<div class="text-center small"></div>';
                    }
                    break;
                case 'phoneNumber':
                    if (value) {
                        if (_.isArray(value)) {
                            return $scope.value = _.map(value, function(v) { return '<a class="inline-tag" href="tel:' + v + '"><i class="far fa-phone"></i><span>' + v + '</span></a>'; });
                        } else {
                            return $scope.value = '<a href="tel:' + value + '">' + value + '</a>';
                        }


                    }

                    break;
                case 'email':
                    if (value) {
                        if (_.isArray(value)) {
                            return $scope.value = _.map(value, function(v) { return '<a class="inline-tag" href="mailto:' + v + '"><i class="far fa-envelope"></i><span>' + v + '</span></a>'; });
                        } else {
                            return $scope.value = '<a href="mailto:' + value + '">' + value + '</a>';
                        }



                    }

                    break;
                case 'timeago':
                    if (value) {
                        var renderValue = $filter('timeago')(value);
                        return $scope.value = '<span class="single-line small">' + renderValue + '</span>';
                    }
                    break;
                case 'expiry':
                    if (value) {

                        if ($scope.item._type != 'purchase') {

                            var date = new Date(value);
                            var now = new Date();

                            if (now > date) {
                                return $scope.value = '<div class="small text-muted"><span>Expired</span></div>';
                            } else {
                                var renderValue = $filter('formatDate')(value, 'g:ia - j M Y');
                                return $scope.value = renderValue;
                            }

                        } else {
                            if (!$scope.item.expires) {
                                return $scope.value = ''; //'<span class="text-muted">forever</span>';
                            }

                            if ($scope.item.renew) {
                                var renderValue = $filter('formatDate')(value, 'g:ia - j M Y');
                                return $scope.value = '<div class="inline-date"><i class="far fa-clock-o"></i><span>' + renderValue + '</span></div>';
                            }

                            if ($scope.item.status == 'expired') {
                                return $scope.value = '<div class="small text-muted"><span>expired</span></div>';
                            }

                            //Text lead in
                            if (value) {
                                var renderValue = $filter('timeago')(value);
                                return $scope.value = '<div class="small text-muted"><span>' + renderValue + '</span></div>';
                            }
                        }


                    }
                    break;
                case 'boolean':


                    if (value) {
                        switch (value) {
                            case 'no':
                            case 'false':
                            case '0':
                                return $scope.value = '<div class="text-center text-muted"><i class="fal fa-times text-muted"></i></div>';
                                break;
                        }
                        return $scope.value = '<div class="text-center"><i class="fas fa-check-circle brand-success"></i></div>';
                    } else {
                        return $scope.value = '<div class="text-center text-muted"><i class="fal fa-times text-muted"></i></div>';
                    }
                    //  else {
                    //     return $scope.value = '<i class="far fa-times text-muted"></i>';
                    // }
                    break;
                case 'paymentStatus':
                    if (value) {
                        switch (value) {
                            case 'success':
                                return $scope.value = '<div class="text-center"><i class="far fa-check brand-success"></i></div>';
                                break;
                            case 'partial_refund':
                                return $scope.value = '<div class="text-center"><strong class="small text-muted">Partial Refund</strong></div>';
                                break;
                            case 'refund':
                                return $scope.value = '<div class="text-center"><span class="small text-muted">Refunded</span></div>';
                                break;
                            case 'failed':
                                return $scope.value = '<div class="text-center"><i class="far fa-warning brand-danger"></i></div>';
                                break;
                        }
                    }
                    //  else {
                    //     return $scope.value = '<i class="far fa-times text-muted"></i>';
                    // }
                    break;
                case 'paymentTime':
                    if (value) {

                        var renderValue = $filter('formatDate')(value, 'g:ia');
                        var dateValue = $filter('formatDate')(value, 'g:ia j M Y');

                        var timeago = $filter('timeago')(value);
                        return $scope.value = '<div class="inline-date" tooltip-placement="bottom" tooltip-append-to-body="true" uib-tooltip="' + dateValue + '"><i class="far fa-clock-o"></i><span>' + renderValue + '</span></div>';

                    }
                    break;
                case 'paymentMode':
                    if (value == 'sandbox') {
                        return $scope.value = '<div class="text-center"><span class="text-muted small">sandbox</span></div>';
                    }

                    if (value == 'live') {
                        return $scope.value = '<div class="text-center"><strong class="small">live</strong></div>';
                    }

                    break;


                case 'personaAvatar':

                    if (value) {

                        var outputString = '';
                        var names = [];

                        //Multiple avatars
                        if (_.isArray(value)) {
                            outputString += _.chain(value)
                                .compact()
                                .map(function(persona) {
                                    var personaID = persona;
                                    if (persona._id) {
                                        personaID = persona._id;
                                    }

                                    names.push(_.startCase(persona.title));

                                    return '<img class="face" ng-src="' + $rootScope.asset.avatarUrl(personaID, 'persona', {
                                        w: 30,
                                        h: 30
                                    }) + '"/>';
                                })
                                .value()
                                .join('');
                        } else {

                            //Single face
                            var personaID = value;
                            if (personaID._id) {
                                personaID = personaID._id;
                            }

                            names.push(_.startCase(value.firstName + ' ' + value.lastName));

                            outputString += '<img class="face" ng-src="' + $rootScope.asset.avatarUrl(personaID, 'persona', {
                                w: 30,
                                h: 30
                            }) + '"/>';
                        }



                        return $scope.value = '<div class="face-stack" uib-tooltip="' + names.join(', ') + '">' + outputString + '</div>';
                    }
                    break;
                case 'guestAttendanceIcon':

                    if (value) {

                        var iconString;
                            if (value.checkin) {
                                iconString = 'brand-success far fa-checkin';
                            }

                            if (value.guestConfirmed) {
                                iconString = 'brand-success  far fa-check-circle';
                            }

                            if (value.guestDeclined) {
                                iconString = 'brand-danger far fa-times';
                            }

                            if (value.ticket) {
                                iconString = 'brand-primary  far fa-ticket';
                            }
                        

                        if(iconString) {
                            return $scope.value = '<i class="'+iconString+'"/>';
                        }
                    }
                    break;
                case 'contactAvatar':

                    if (value) {


                        var outputString = '';
                        var names = [];

                        //Multiple avatars
                        if (_.isArray(value)) {
                            outputString += _.chain(value)
                                .compact()
                                .map(function(contact) {
                                    var contactID = contact;
                                    if (contact._id) {
                                        contactID = contact._id;
                                    }

                                    names.push(_.startCase(contact.title));

                                    return '<img class="face" ng-src="' + $rootScope.asset.avatarUrl(contactID, 'contact', {
                                        w: 30,
                                        h: 30
                                    }) + '"/>';
                                })
                                .value()
                                .join('');
                        } else {

                            //Single face
                            var contactID = value;
                            if (contactID._id) {
                                contactID = contactID._id;
                            }

                            names.push(_.startCase(value.title));

                            outputString += '<img class="face" ng-src="' + $rootScope.asset.avatarUrl(contactID, 'contact', {
                                w: 30,
                                h: 30
                            }) + '"/>';
                        }



                        return $scope.value = '<div class="face-stack" uib-tooltip="' + names.join(', ') + '">' + outputString + '</div>';
                    }
                    break;


                default:


                    if (_.isObject(value)) {

                        if (value._type == 'event') {
                            return $scope.value = value.title + '<span class="text-muted"> // ' + $filter('date')(value.startDate) + '</span>';
                        }

                        if (value.title) {
                            return $scope.value = value.title;
                        }

                        if (value.name) {
                            return $scope.value = value.name;
                        }
                    }

                    break;
            }

            $scope.value = value;
        }
    }


})


/*
//////////////////////////////

    //Setup Search    
    $scope.retrieveValue = function(item, column) {
        var value = Tools.getDeepProperty(item, column.key);

        
        /*
        var template = '<'+column.renderer+'></'+column.renderer+'>';

         var cTemplate = $compile(template)();
        $element.append(cTemplate);
        
        if(column.filterId) {           
            return $filter(column.filterId)(value);
        } else {
            return value;
        }
        
    }
    */