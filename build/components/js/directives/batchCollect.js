app.directive('batchCollect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            selection: '=ngModel',
        },
        template: '<a popover-trigger="outsideClick" uib-popover-template="dynamicPopover.templateUrl" popover-is-open="popover.open" popover-placement="bottom" popover-append-to-body="true" popover-title="{{dynamicPopover.title}}" class="btn btn-default"><i class="far fa-collection"></i></a>',
        controller: 'BatchCollectController',
    };
});

app.controller('BatchCollectController', function($scope, $state, Batch, Notifications, $rootScope, CacheManager, FluroAccess, FluroContent, Fluro, $http, ModalService) {

    $scope.popover = {
        open:false
    };

    //////////////////////////////////////////////////////////

    $scope.options = [];
    $scope.collections = [];
    $scope.search = {};

    //////////////////////////////////////////////////////////

    //Get all the options
    FluroContent.resource('collection').query({
        simple: true,
        allDefinitions:true,
    }, function(res) {
        $scope.options = res;
    });

    //////////////////////////////////////////////////////////

    $scope.dynamicPopover = {
        templateUrl: 'views/ui/batch-collect-popover.html',
    };

    /////////////////////////////////////

    $scope.addToCollections = function() {

        $scope.popover = {
            open:false
        }
        
        var details = {
            ids: $scope.selection.ids,
            collections: $scope.collections,
        }

        Batch.addToCollections(details, done);
    }



    //////////////////////////////////////////////////////////

    function done(err, data) {


        var selectionLength = $scope.selection.items.length;

        // console.log('Batch done', data);
        if (err) {
            //console.log('DONE ERR', err)
            return Notifications.error(err);
        }

        //////////////////////////////////////////////////////////

        var updatedCollections =  _.get(data, 'result.updatedCollections');

        if(!updatedCollections || !updatedCollections.length) {
            return;
        }

        ///////////////////////

        //Find all the caches we need to clear
        var caches = [];

        ///////////////////////

        //Refresh any caches that might be affected outside of this view
        if (data.result) {
            if (data.result.types && data.result.types.length) {
                caches.concat(data.result.types)
            }

            if (data.result.definitions && data.result.definitions.length) {
                caches.concat(data.result.definitions)
            }
        }

        ///////////////////////

        caches = _.uniq(caches);
        caches = _.compact(caches);

        _.each(caches, function(t) {
            CacheManager.clear(t);
        });

        ///////////////////////

        //Reload the state
        $state.reload();


        ///////////////////////

        //Deselect all
        $scope.selection.deselectAll();

        //////////////////////////////////////////////////////////

        if(updatedCollections.length == 1) {
            Notifications.status('Added '+ selectionLength +' items to ' + "'" + updatedCollections[0].title + "'");
        } else {
            Notifications.status('Added '+ selectionLength +' items to ' + updatedCollections.length + ' collections');
        }
    }

    //////////////////////////////////
    //////////////////////////////////

    //See whether a use is allowed to create new objects
    $scope.createEnabled = FluroAccess.can('create', 'collection');   

    $scope.create = function() {
        $scope.popover = {
            open:false
        }

        var template = {
            // title:'My new collection',
            items:$scope.selection.items,
        }

        ModalService.create('collection', {template:template}, function(res) {
            $scope.options.push(res);
            $scope.model.push(res);
        })
    }

    //////////////////////////////////
    //////////////////////////////////

    $scope.contains = function(item) {
        return _.some($scope.collections, function(i) {
            if (_.isObject(i)) {
                return i._id == item._id;
            } else {
                return i == item._id;
            }
        });
    }
    
    $scope.toggle = function(realm) {

        var existing = _.find($scope.collections, {
            _id: realm._id
        });

        if (existing) {
            _.pull($scope.collections, existing);
        } else {
            $scope.collections.push(realm);
        }

    }

    /////////////////////////////////////



});