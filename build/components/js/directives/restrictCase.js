
app.directive('restrictCase', function($parse) {
    return {
        restrict: 'A',
        require: '^ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            ngModelCtrl.$viewChangeListeners.push(function() {
                /*Set model value differently based on the viewvalue entered*/
                var regexp = /[^a-zA-Z0-9-_]+/g;

                var val = ngModelCtrl.$viewValue.replace(regexp, '');
                varl = val.toLowerCase();


                //$parse(attrs.ngModel).assign(scope, ngModelCtrl.$viewValue.replace(regexp, ''));
                $parse(attrs.ngModel).assign(scope, val);
                

            });
        }
    };
});