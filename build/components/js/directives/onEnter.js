app.directive('ngEnter', function() {
    return function(scope, element, attrs) {

     

        /////////////////////////////////////////

        function enterPressed(event) {

            if (event.which === 13) {

                console.log('EVENT', event);
                scope.$apply(function() {
                    console.log('apply enter');
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        }


        /////////////////////////////////////////
        
        scope.$on('$destroy', function() {
            element.off("keydown keypress", enterPressed);
        });


        element.on("keydown keypress", enterPressed);
    };
});