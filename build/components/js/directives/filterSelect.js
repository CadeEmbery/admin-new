app.directive('filterSelect', function(PathfinderService) {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            testData: '=ngTestData',
            definition: '=definition',
        },
        templateUrl: 'views/ui/filter-select.html',
        // controller: 'FilterSelectController',
        link: function($scope, $element, $attrs) {

            //Create a model if it doesn't exist
            if (!$scope.model) {
                $scope.model = [];
            }


            //Create a new object
            $scope._new = {}

            ////////////////////////////////////////////////////

            $scope.$watch('testData', function(items) {
                $scope.rows = PathfinderService.extract(_.first(items));
            })

            ////////////////////////////////////////////////////

            $scope.add = function() {
                var insert = angular.copy($scope._new);
                $scope.model.push(insert);
                $scope._new = {};

                console.log('FOCUS BACK')
                $element.find('.focus-back').focus();

            }




            $scope.$watch('definition.fields', function(fields) {
                if(!fields) {
                    return;
                }

                $scope.availableFields = extractFieldsFromDefinitionFields(fields, '0');

            })

            ////////////////////////////////////////////////////

            $scope.remove = function(entry) {
                _.pull($scope.model, entry);
            }

            ////////////////////////////////////////////////////

            $scope.contains = function(entry) {
                return _.contains($scope.model, entry);
            }
        }
    };
});