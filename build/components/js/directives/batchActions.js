app.directive('batchActions', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            selection: '=ngModel',
            type: '=ngType',
            definitions: '=ngDefinitions',
            definition: '=ngDefinition',
            queryItem: '=',
            queryMode: '=',
        },
        template: '<a popover-trigger="outsideClick" uib-popover-template="dynamicPopover.templateUrl" popover-is-open="settings.open" popover-append-to-body="true" popover-placement="bottom" popover-title="{{dynamicPopover.title}}" class="btn btn-default"><i class="far fa-cog"></i></a>',
        controller: 'BatchActionsController',
    };
});

/////////////////////////////////////////////////////////////////////

app.controller('BatchActionsController', function($scope, $state, $q, $timeout, ContentListService, TypeService, $uibModal, FluroContent, ModalService, FluroAccess, Batch, Notifications, $rootScope, CacheManager, Fluro, $http) {

    $scope.settings = {
        open: false,
    }

    //////////////////////////////////////////////////////////

    $scope.$watch('definitions', function(definitions) {

        $scope.filteredDefinitions = _.filter(definitions, function(def) {

            if ($scope.definition) {
                return (def.definitionName != $scope.definition.definitionName);
            } else {
                return true;
            }
        });
    })








    //////////////////////////////////////////////////////////

    $scope.isSecuredType = function() {
        switch ($scope.type.path) {
            case 'asset':
            case 'image':
            case 'audio':
            case 'video':
                return true;
                break;
        }
    }




    //////////////////////////////////////////////////////////

    $scope.allMatchType = function(type) {

        var types = [type];
        if (_.isArray(type)) {
            types = type;
        }

        return _.every($scope.selection.items, function(item) {
            return _.includes(types, item._type);
        });
    }

    //////////////////////////////////////////////////////////

    //Edit
    var editAny = FluroAccess.can('edit any', $scope.type.path);
    var editOwn = FluroAccess.can('edit own', $scope.type.path);

    //Delete
    var deleteAny = FluroAccess.can('delete any', $scope.type.path);
    var deleteOwn = FluroAccess.can('delete own', $scope.type.path);

    if ($scope.definition) {
        editAny = FluroAccess.can('edit any', $scope.definition.definitionName, $scope.definition.parentType)
        editOwn = FluroAccess.can('edit own', $scope.definition.definitionName, $scope.definition.parentType)
        //Delete
        deleteAny = FluroAccess.can('delete any', $scope.definition.definitionName, $scope.definition.parentType)
        deleteOwn = FluroAccess.can('delete own', $scope.definition.definitionName, $scope.definition.parentType)
    }

    $scope.editAccess = (editAny || editOwn)
    $scope.deleteAccess = (deleteAny || deleteOwn)


    //////////////////////////////////////////////////////////

    switch ($scope.type.path) {
        case 'plan':
            $scope.templateable = true;
            break;
    }



    //////////////////////////////////////////////////////////

    $scope.canTransformSelection = false;

    switch ($scope.type.path) {
        case 'contact':
        case 'contactdetail':
        case 'process':
        case 'team':
        case 'checkin':
        case 'interaction':
        case 'ticket':
        case 'family':
        case 'roster':
        case 'assignment':
        case 'event':
            $scope.canTransformSelection = true;
            break;
    }

    //////////////////////////////////////////////////////////

    $scope.transformSelection = function(style, action) {

        var details = {
            ids: $scope.selection.ids,
        }

        //////////////////////////////////////////////////////////

        switch (style) {
            case 'roster':
                details.options = {
                    eventAssignments: true,
                }
                break;
            case 'ticket':
                details.options = {
                    eventTickets: true,
                }
                break;
            case 'checkin':
                details.options = {
                    eventCheckins: true,
                }
                break;
            case 'checkin parents':
                details.options = {
                    eventCheckins: true,
                    parents: true,
                }
                break;
            case 'parents':
                details.options = {
                    parents: true,
                }
                break;
            case 'expected':
                details.options = {
                    eventExpected: true,

                }
                break;
            case 'expected parents':
                details.options = {
                    eventExpected: true,
                    parents: true,
                }
                break;

        }

        //////////////////////////////////////////////////////////

        //Find the matching contact ids from the fluro server
        $http.post(Fluro.apiURL + '/contact/select', details)
            .then(function(res) {

                //this should be the people returned
                var contactIDs = res.data;


                if (!contactIDs.length) {
                    return Notifications.warning('No contacts were found');
                }


                switch (action) {
                    case 'sms':
                        ModalService.sms(contactIDs); //, success, failure);
                        break;
                    default:

                        console.log('Transform the selection!', contactIDs)
                        //Set the selection and change page

                        var newSelection = _.reduce(contactIDs, function(set, contactID, key) {
                            set[contactID] = true;
                            return set;
                        }, {});

                        // $state.go('contact.default');
                        ContentListService.selectContacts(newSelection);
                        // transformSelection = newSelection;



                        break;
                }


            });
    }


    //////////////////////////////////////////////////////////


    $scope.canMergeRealms = ($scope.type.path == 'realm' && $rootScope.user.accountType == 'administrator');

    //////////////////////////////////////////////////////////

    if (FluroAccess.can('create', $scope.type.path) && $scope.definition) {
        //console.log('CAN REMOVE', FluroAccess.can('create', $scope.type.path), $scope.definition);
        $scope.canRemoveDefinition = true;
    }
    //////////////////////////////////////////////////////////

    $scope.search = {};

    //////////////////////////////////////////////////////////

    //Move to status
    $scope.changePrivacy = function(status) {

        $scope.settings.open = false;

        if (status == 'public' || status == 'secure') {
            console.log('SET STATUS', status);

            var details = {
                ids: $scope.selection.ids,
                privacy: status,
            }

            Batch.setPrivacy(details, done);
        }
    }

    //////////////////////////////////////////////////////////

    $scope.adjustTimes = function() {

        $scope.settings.open = false;
        var details = {
            ids: $scope.selection.ids,
        }

        ///////////////////////////////

        var selection = $scope.selection;

        ///////////////////////////////

        var modalInstance = $uibModal.open({
            templateUrl: 'admin-batch-timeadjust/admin-batch-timeadjust.html',
            controller: 'AdminBatchTimeAdjustController',
            backdrop: 'static',
            resolve: {
                details: function() {
                    return details;
                },

            },
            size: 'sm',
        });

        modalInstance.result.then(finish);

        /////////////////////////////////////////////

        function finish(proposedData) {

            proposedData.ids = details.ids;
            Batch.adjustTime(proposedData, done);

        }

        function done() {



            if ($scope.definition) {
                CacheManager.clear($scope.definition.definitionName);
            }
            CacheManager.clear($scope.type.path)

            //Clear Selection
            $scope.selection.deselectAll();

            ///////////////////////

            //Reload the state
            $state.reload();

            ///////////////////////

            Notifications.status('Adjusted times for ' + details.ids.length + ' events');

            //Deselect all
            $scope.selection.deselectAll();

        }
    }

    /////////////////////////////////////

    $scope.isType = function(type) {

        // console.log('IS TYPE?', type)
        return $scope.allMatchType([type]);
    }
    /////////////////////////////////////

    $scope.canReprocess = function() {


        switch ($scope.type.path) {
            case 'image':
            case 'video':
                return true; //allMatchType($scope.type.path);
                break;
        }

        return $scope.allMatchType(['image', 'video']);

        // type.path == 'image' || allMatchType('image')
    }
    /////////////////////////////////////

    //Move to status
    $scope.reprocess = function() {

        $scope.settings.open = false;

        var details = {
            ids: $scope.selection.ids,
        }

        Batch.reprocess(details, done);

    }

    /////////////////////////////////////

    $scope.batchAddOwners = function() {


        $scope.settings.open = false;


        var modelSource = {};
        var params = {};

        /////////////////////////////////////////////////////////

        function ownersSelected(result) {

            var itemIDs = $scope.selection.ids;
            var owners = modelSource.items;

            //If we have people to assign to
            if (owners && owners.length) {
                var details = {
                    ids: itemIDs,
                    personas: owners,
                }

                return Batch.addOwner(details, done);
            }
        }

        /////////////////////////////////////////////////////////

        //find owners to select
        ModalService.browse('persona', modelSource, params)
            .result.then(ownersSelected, ownersSelected);
    }





    /////////////////////////////////////

    $scope.batchMergeRealms = function() {

        $scope.settings.open = false;

        var confirmed = window.confirm("Are you sure you want to merge realms? This can not be undone!");

        if (!confirmed) {
            return;
        }


        //////////////////////////////////////////////////

        var details = {
            ids: $scope.selection.ids,
        }

        //////////////////////////////////////////////////

        return Batch.mergeRealms(details, done);


        // FluroContent.endpoint('realm/merge').update(params).$promise.then(success, failed);


        // function success(res) {
        //     console.log('SUCCESS', res);
        // }


        // function failed(err) {
        //     console.log('ERROR', err);
        // }

    }

    /////////////////////////////////////

    $scope.batchRemoveOwners = function() {

        $scope.settings.open = false;

        var modelSource = {};
        var params = {};

        /////////////////////////////////////////////////////////

        function ownersSelected(result) {

            var itemIDs = $scope.selection.ids;
            var owners = modelSource.items;

            //If we have people to assign to
            if (owners && owners.length) {
                var details = {
                    ids: itemIDs,
                    personas: owners,
                }

                return Batch.removeOwner(details, done);
            }
        }

        /////////////////////////////////////////////////////////

        //find owners to select
        ModalService.browse('persona', modelSource, params)
            .result.then(ownersSelected, ownersSelected);
    }

    /////////////////////////////////////

    //Move to status
    $scope.mergeTags = function() {

        $scope.settings.open = false;

        var details = {
            ids: $scope.selection.ids,
        }

        var selection = $scope.selection;


        ///////////////////////////////

        var modalInstance = $uibModal.open({
            templateUrl: 'views/ui/merge-tag-modal.html',
            controller: function($scope) {
                $scope.details = details;
                $scope.selection = selection;
            },
            size: 'sm',
            // backdrop: 'static',
            // resolve: {
            //     tags: function($q) {
            //         return FluroContent.endpoint('message/contacts').query({
            //             ids: ids,
            //         }).$promise;
            //     }
            // }
        });

        modalInstance.result.then(finish);

        /////////////////////////////////////////////

        function finish() {
            if (details.title && details.title.length) {

                var request = FluroContent.endpoint('tags/merge').save(details).$promise.then(mergeComplete, mergeFailed);
            } else {
                Notifications.status('A new tag name is required');
            }
        }

        /////////////////////////////////////////////

        function mergeComplete(res) {


            if ($scope.definition) {
                CacheManager.clear($scope.definition.definitionName);
            }
            CacheManager.clear($scope.type.path)

            ///////////////////////

            $scope.selection.deselectAll();
            //Reload the state
            $state.reload();

            ///////////////////////

            Notifications.status('Merged ' + details.ids.length + ' tags into ' + res.title);

            //Deselect all
            $scope.selection.deselectAll();

        }

        function mergeFailed(res) {
            return Notifications.error(res.data);
        }

    }

    /////////////////////////////////////

    //Move to status
    $scope.merge = function() {


        /////////////////////////////////////


        $scope.settings.open = false;
        var details = {
            ids: $scope.selection.ids,
        }

        function mergeComplete(res) {


            if ($scope.definition) {
                CacheManager.clear($scope.definition.definitionName);
            }
            CacheManager.clear($scope.type.path)

            ///////////////////////

            //Reload the state

            $state.reload();

            ///////////////////////

            Notifications.status('Merged ' + details.ids.length + ' contacts for ' + res.title);

            //Deselect all
            $scope.selection.deselectAll();

        }

        function mergeFailed(res) {
            return Notifications.error(res.data);
        }

        var request = FluroContent.endpoint('contact/merge').save(details).$promise.then(mergeComplete, mergeFailed);
    }


    /////////////////////////////////////

    //Move to status
    $scope.mergeFamilies = function() {

        var confirmed = window.confirm("Are you sure you want to merge these families?");

        if (!confirmed) {
            return;
        }



        $scope.settings.open = false;


        var details = {
            ids: $scope.selection.ids,
        }

        function mergeComplete(res) {


            if ($scope.definition) {
                CacheManager.clear($scope.definition.definitionName);
            }
            CacheManager.clear($scope.type.path)

            ///////////////////////

            //Reload the state
            $state.reload();

            ///////////////////////

            Notifications.status('Merged ' + details.ids.length + ' families into ' + res.title);

            //Deselect all
            $scope.selection.deselectAll();

        }

        function mergeFailed(res) {
            return Notifications.error(res.data);
        }

        var request = FluroContent.endpoint('family/merge').save(details).$promise.then(mergeComplete, mergeFailed);
    }


    /////////////////////////////////////

    //Move to status
    $scope.mergeTeams = function() {


        var confirmed = window.confirm("Are you sure you want to merge these teams?");

        if (!confirmed) {
            return;
        }


        $scope.settings.open = false;
        var details = {
            ids: $scope.selection.ids,
        }

        function mergeComplete(res) {


            if ($scope.definition) {
                CacheManager.clear($scope.definition.definitionName);
            }
            CacheManager.clear($scope.type.path)

            ///////////////////////

            //Reload the state
            $state.reload();

            ///////////////////////

            Notifications.status('Merged ' + details.ids.length + ' items into ' + res.title);

            //Deselect all
            $scope.selection.deselectAll();

            ModalService.view(res);

        }

        function mergeFailed(res) {
            return Notifications.error(res.data);
        }

        var request = FluroContent.endpoint('teams/merge').save(details).$promise.then(mergeComplete, mergeFailed);
    }


    /////////////////////////////////////

    $scope.batchResave = function() {

        var details = {
            ids: $scope.selection.ids,
            status: status,
        }
        
        Batch.resave(details, done);
    }

    /////////////////////////////////////

    //Move to status
    $scope.moveTo = function(status) {

        $scope.settings.open = false;

        var details = {
            ids: $scope.selection.ids,
            status: status,
        }


        if ($scope.type.path == 'persona') {
            details.useModel = 'persona';
        }

        if ($scope.type.path == 'ticket') {
            details.useModel = 'ticket';
        }

        if ($scope.type.path == 'account') {
            details.useModel = 'account';
        }


        Batch.setStatus(details, done);
    }

    /////////////////////////////////////

    $scope.batchDelete = function() {

        $scope.settings.open = false;

        var confirmed = window.confirm('Are you sure you want to delete ' + $scope.selection.ids.length + ' selected ' + $scope.type.plural + '?');

        if (!confirmed) {
            return;
        }


        var details = {
            ids: $scope.selection.ids,
        }

        /////////////////////////////////////

        if ($scope.type.path == 'persona') {
            details.useModel = 'persona';
        }

        if ($scope.type.path == 'ticket') {
            details.useModel = 'ticket';
        }

        if ($scope.type.path == 'account') {
            details.useModel = 'account';
        }


        /////////////////////////////////////


        Batch.deleteItems(details, done);
    }

    /////////////////////////////////////
    /*
    //Add batch editing for defined items
    if($scope.definitions && $scope.definitions.length) {
        $scope.actions.push({
            key:'edit',
            label: 'Edit',
            icon: 'edit',
            run: runEdit
        });
    }
    */

    //////////////////////////////////////////////

    function getTypeAndDefinitionFromFirstItem() {

        //Create a promise
        var deferred = $q.defer();

        ///////////////////////////////////////////////

        var selectionItems = $scope.selection.items;
        var firstItem = _.first(selectionItems);

        ///////////////////////////////////////////////

        var definitionName = firstItem.definition;
        var typeName = firstItem._type;

        ///////////////////////////////////////////////

        var useType = TypeService.getTypeFromPath(typeName);

        ///////////////////////////////////////////////

        //Get the type of the thing
        var results = {
            type: useType,
        }

        ///////////////////////////////////////////////

        //If a definition is specified
        if (definitionName && definitionName.length) {

            TypeService.getDefinition(definitionName).then(function(definition) {

                //Add the definition to the results
                results.definition = definition;

                //Resolve the promise
                deferred.resolve(results);

                //Reject the promise if it fails
            }, deferred.reject);
        } else {
            deferred.resolve(results);
        }

        ///////////////////////////////////////////////

        return deferred.promise;
    }

    //////////////////////////////////////////////


    $scope.batchEdit = function() {


        ///////////////////////////////////////


        if ($scope.queryMode) {
            return getTypeAndDefinitionFromFirstItem()
                .then(function(match) {
                    openModal(match.type, match.definition);
                }, Notifications.warning);
        } else {
            return openModal($scope.type, $scope.definition);
        }

        ////////////////////////////////////////////////

        function openModal(useType, useDefinition) {
            if (!useType) {
                Notifications.warning('Please update your query to include the _type or definition columns to use the batch editing feature');
                return;
            }

            ///////////////////////////////////////

            $scope.settings.open = false;

            ModalService.batch(
                useType,
                useDefinition,
                $scope.selection.ids,
                function(result) {
                    console.log('Woot Done Batch Edit', result)
                    $scope.selection.deselectAll();
                })
        }
    }


    /////////////////////////////////////


    $scope.customExport = function() {

        ///////////////////////////////////////


        if ($scope.queryMode) {
            return getTypeAndDefinitionFromFirstItem()
                .then(function(match) {
                    openModal(match.type, match.definition);
                }, Notifications.warning);
        } else {
            return openModal($scope.type, $scope.definition);
        }

        ////////////////////////////////////////////////

        function openModal(useType, useDefinition) {
            if (!useType) {
                Notifications.warning('Please update your query to include the _type or definition columns to use the batch export feature');
                return;
            }

            ///////////////////////////////////////

            $scope.settings.open = false;

            ModalService.export(
                useType,
                useDefinition,
                $scope.selection.ids,
                function(result) {
                    console.log('Woot Done Batch Export', result)
                })

        }
    }

    /////////////////////////////////////


    $scope.batchExport = function(columnsOnly) {



        $scope.settings.open = false;

        var details = {
            ids: $scope.selection.ids,
            type: $scope.type.path,
            columnsOnly: columnsOnly,
        }
        Batch.exportItems(details, done);
    }


    $scope.batchExportSeperate = function(columnsOnly) {
        $scope.settings.open = false;
        var details = {
            ids: $scope.selection.ids,
            type: $scope.type.path,
            columnsOnly: columnsOnly,
            seperate: true,
        }
        Batch.exportItems(details, done);
    }

    /////////////////////////////////////

    $scope.batchDownload = function() {
        $scope.settings.open = false;
        var details = {
            ids: $scope.selection.ids,
        }

        Batch.downloadItems(details, function(err, result) {
            console.log('Batch download', err, result);
        });
    }

    /////////////////////////////////////

    $scope.removeDefinition = function() {
        $scope.settings.open = false;
        var details = {
            ids: $scope.selection.ids,
        }

        Batch.removeDefinition(details, done);
    }

    /////////////////////////////////////

    $scope.defineAs = function(def) {
        $scope.settings.open = false;
        var details = {
            ids: $scope.selection.ids,
            definition: def.definitionName,
        }

        Batch.redefine(details, done);
    }

    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////


    $scope.resendConfirmations = function() {
        $scope.settings.open = false;
        var details = {
            ids: $scope.selection.ids,
        }

        Batch.resendConfirmations(details, done);
    }
    //////////////////////////////////////////////////////////

    $scope.sendSMS = function() {



        function success(res) {
            console.log('Message Success', res)
        }

        function failure(err) {
            console.log('Message Error', err)
        }

        ////////////////////////////////////////////

        var typeName = $scope.type.path;

        ////////////////////////////////////////////

        if (typeName == 'team') {

            //Prompt the user to select positions before we send an sms
            return ModalService.selectPositions($scope.selection.ids, function(contacts) {


                if (contacts == 'all' || !contacts || !contacts.length) {
                    console.log('Send SMS to all ids', $scope.selection.ids)
                    return smsModal($scope.selection.ids);
                }

                console.log('Send sms to contacts', contacts);
                return smsModal(contacts);
            });

        } else {
            return smsModal($scope.selection.ids);
        }

        //////////////////////////////////////////////////////////

        function smsModal(contacts) {
            //Open a new modal to send SMS to everyone
            ModalService.sms(contacts, success, failure);

        }

        // console.log('SEND SMS', $scope.selection.ids);
        // ModalService.sms($scope.selection.ids);
        // $scope.settings.open = false;

        // console.log('Close settings');
        // 
    }

    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    $scope.sparkReaction = function() {
        console.log('Spark Reaction', $scope.selection.ids);
        ModalService.react($scope.selection.ids);
        $scope.settings.open = false;

        console.log('Close settings');
    }

    $scope.assignPosition = function() {
        console.log('Add Position', $scope.selection.ids);
        ModalService.assignPosition($scope.selection.items);
        $scope.settings.open = false;

        console.log('Close settings');
    }

    //////////////////////////////////////////////////////////

    $scope.dynamicPopover = {
        templateUrl: 'views/ui/batch-actions-popover.html',
    };

    //////////////////////////////////////////////////////////

    function done(err, data) {

        // console.log('Batch done', data);
        if (err) {
            //console.log('DONE ERR', err)
            return Notifications.error(err);
        }

        ///////////////////////

        //Find all the caches we need to clear
        var caches = [];

        ///////////////////////

        //Make sure we refresh the current view
        if ($scope.definition) {
            caches.push($scope.definition.definitionName)
        }
        caches.push($scope.type.path)

        ///////////////////////

        //Refresh any caches that might be affected outside of this view
        if (data.result) {
            if (data.result.types && data.result.types.length) {
                caches.concat(data.result.types)
            }

            if (data.result.definitions && data.result.definitions.length) {
                caches.concat(data.result.definitions)
            }
        }

        ///////////////////////

        caches = _.uniq(caches);
        caches = _.compact(caches);

        _.each(caches, function(t) {
            CacheManager.clear(t);
        });

        ///////////////////////

        //Reload the state
        $state.reload();

        ///////////////////////

        //Deselect all
        $scope.selection.deselectAll();
        Notifications.status('Updated  ' + data.success.length + ' ' + $scope.type.plural);



    }


    /////////////////////////////////////



});