app.directive('route', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },

        templateUrl: 'views/ui/route.html',
        controller: 'RouteController',
    };
});

app.controller('RouteController', function($scope) {

    if (!$scope.model) {
        $scope.model = {
            title: '',
            url: '',
            includes: [],
        }
    }





    $scope.removeRoute = $scope.$parent.removeRoute;


    $scope.addInclude = function() {
        $scope.model.includes.push({});
    }


    /////////////////////////////////////
    /////////////////////////////////////
})


