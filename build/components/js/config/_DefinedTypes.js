/*//Define all of our Content Types
var DefinedTypes = [{
    singular: 'Role',
    plural: 'Roles',
    path: 'role',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
}, {
    singular: 'Code',
    plural: 'Code',
    path: 'code',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    filters: [{
        title: 'Syntax',
        key: 'syntax',
    }, {
        title: 'Author',
        key: 'author',
    }, {
        title: 'Status',
        key: 'status',
    }, {
        title: 'Realm',
        key: 'realm',
    }]
}, {
    singular: 'Package',
    plural: 'Packages',
    path: 'package',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    actions: ['delete', 'export', 'define'],
}, {
    singular: 'Contact',
    plural: 'Contacts',
    path: 'contact',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    actions: ['export', 'delete'],
}, {
    singular: 'Purchase',
    plural: 'Purchases',
    path: 'purchase',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    actions: ['export', 'delete'],
}, {
    singular: 'Product',
    plural: 'Products',
    path: 'product',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    filters: [{
        title: 'License',
        key: 'license'
    }, {
        title: 'Currency',
        key: 'currency'
    }, {
        title: 'Status',
        key: 'status'
    }, {
        title: 'Author',
        key: 'author'
    }],
    actions: ['export', 'delete'],
}, {
    singular: 'Transaction',
    plural: 'Transactions',
    path: 'transaction',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    filters: [{
        title: 'Module',
        key: 'module'
    }, {
        title: 'Status',
        key: 'paymentStatus'
    }, {
        title: 'Mode',
        key: 'mode'
    }, {
        title: 'Currency',
        key: 'currency'
    }, {
        title: 'Integration',
        key: 'integration'
    }],
    actions: ['export'],
}, {
    singular: 'Application',
    plural: 'Applications',
    path: 'application',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    actions: ['define'],
   
}, {
    singular: 'Integration',
    plural: 'Integrations',
    path: 'integration',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    filters: [{
        title: 'Module',
        key: 'module'
    }, {
        title: 'Realms',
        key: 'realms'
    }],
    actions: [
        'archive',
        'delete',
    ],
}, {
    singular: 'Interaction',
    plural: 'Interactions',
    path: 'interaction',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    filters: [{
        title: 'Contact',
        key: 'contact'
    }, {
        title: 'Status',
        key: 'status',
        forceSingle: true,
    }, {
        title: 'Realms',
        key: 'realms'
    }],
    actions: [
        'archive',
        'export',
        'delete',
    ],
}, {
    singular: 'Definition',
    plural: 'Definitions',
    path: 'definition',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    filters: [{
        title: 'Type',
        key: 'parentType',
    }, {
        title: 'Realms',
        key: 'realms',
    }, {
        title: 'Status',
        key: 'status',
    }]
}, {
    singular: 'Realm',
    plural: 'Realms',
    path: 'realm',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
}, {
    singular: 'User',
    plural: 'Users',
    path: 'user',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    filters: [{
        title: 'Realms',
        key: 'realms'
    }, {
        title: 'Account',
        key: 'availableAccounts'
    }, {
        title: 'Status',
        key: 'status'
    }],
}, {
    singular: 'Event',
    plural: 'Events',
    path: 'event',
    autosave: true,
    viewTypes: [{
        type: 'list',
        path: 'list'
    }, {
        type: 'cards',
        path: 'default',
        default: true,
    }],
    filters: [{
        title: 'Realms',
        key: 'realms'
    }, {
        title: 'Tags',
        key: 'tags'
    }, {
        title: 'Locations',
        key: 'locations'
    }, {
        title: 'Rooms',
        key: 'rooms'
    }],
    actions: [
        'export',
        'delete',
    ],
}, {
    singular: 'Location',
    plural: 'Locations',
    path: 'location',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }, {
        type: 'map',
        path: 'map',
    }],
}, {
    singular: 'Endpoint',
    plural: 'Endpoints',
    path: 'endpoint',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
}, {
    singular: 'Account',
    plural: 'Accounts',
    path: 'account',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
}, {
    singular: 'Collection',
    plural: 'Collections',
    path: 'collection',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    actions: [
        'delete',
    ],
}, {
    singular: 'Article',
    plural: 'Articles',
    path: 'article',
    autosave: true,
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    actions: [
        'archive',
        'activate',
        'draft',
        'delete',
    ],
}, {
    singular: 'Asset',
    plural: 'Assets',
    path: 'asset',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    filters: [{
        title: 'Realms',
        key: 'realms'
    }, {
        title: 'Filetype',
        key: 'extension'
    }, {
        title: 'Definition',
        key: 'definition'
    }, {
        title: 'Tags',
        key: 'tags'
    }, {
        title: 'Author',
        key: 'author'
    }, {
        title: 'Status',
        key: 'status',
        forceSingle: true,
    }],
    actions: [
        'delete',
        'download',
    ],


}, {
    singular: 'Tag',
    plural: 'Tags',
    path: 'tag',
    viewTypes: [{
        type: 'list',
        default: true,
        path: 'default',
    }],
    filters: [{
        title: 'Realms',
        key: 'realms'
    }, {
        title: 'Author',
        key: 'author'
    }, {
        title: 'Types',
        key: 'restrictTypes'
    }],
    actions: [
        'delete',
    ],
}, {
    singular: 'Image',
    plural: 'Images',
    path: 'image',
    viewTypes: [{
        type: 'list',
        path: 'default',
        default: true,
    }, {
        type: 'grid',
        path: 'grid',
    }],
    filters: [{
        title: 'Realms',
        key: 'realms'
    }, {
        title: 'Filetype',
        key: 'extension'
    }, {
        title: 'Definition',
        key: 'definition'
    }, {
        title: 'Tags',
        key: 'tags'
    }, {
        title: 'Author',
        key: 'author'
    }, {
        title: 'Status',
        key: 'status',
        forceSingle: true,
    }],
    actions: [
        'delete',
        'download',
        'archive',
        'activate',
    ],
}, {
    singular: 'Audio',
    plural: 'Audio',
    path: 'audio',
    viewTypes: [{
        type: 'list',
        default: true
    }],
    filters: [{
        title: 'Realms',
        key: 'realms'
    }, {
        title: 'Filetype',
        key: 'extension'
    }, {
        title: 'Type',
        key: 'definition'
    }, {
        title: 'Tags',
        key: 'tags'
    }, {
        title: 'Author',
        key: 'author'
    }, {
        title: 'Status',
        key: 'status',
        forceSingle: true,
    }],
    actions: [
        'delete',
        'download',
        'archive',
        'activate',
    ],
}, {
    singular: 'Video',
    plural: 'Videos',
    path: 'video',
    viewTypes: [{
        type: 'list',
        path: 'default',
        default: true
    }, {
        type: 'grid',
        path: 'grid',
    }],
    filters: [{
        title: 'Realms',
        key: 'realms'
    }, {
        title: 'Filetype',
        key: 'extension'
    }, {
        title: 'Definition',
        key: 'definition'
    }, {
        title: 'Type',
        key: 'assetType'
    }, {
        title: 'Tags',
        key: 'tags'
    }, {
        title: 'Author',
        key: 'author'
    }, {
        title: 'Status',
        key: 'status',
        forceSingle: true,
    }],
    actions: [
        'delete',
        'download',
        'archive',
        'activate',
    ],
}, {
    singular: 'Site',
    plural: 'Sites',
    path: 'site',
    viewTypes: [{
        type: 'list',
        default: true
    }],
    actions: [
        'delete',
    ],
}];

*/