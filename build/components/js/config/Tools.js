/*
function strip_tags(input, allowed) {
    //  discuss at: http://phpjs.org/functions/strip_tags/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Luke Godfrey
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    //    input by: Pul
    //    input by: Alex
    //    input by: Marc Palau
    //    input by: Brett Zamir (http://brett-zamir.me)
    //    input by: Bobby Drake
    //    input by: Evertjan Garretsen
    // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // bugfixed by: Onno Marsman
    // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // bugfixed by: Eric Nagel
    // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // bugfixed by: Tomasz Wesolowski
    //  revised by: Rafał Kukawski (http://blog.kukawski.pl/)
    //   example 1: strip_tags('<p>Kevin</p> <br /><b>van</b> <i>Zonneveld</i>', '<i><b>');
    //   returns 1: 'Kevin <b>van</b> <i>Zonneveld</i>'
    //   example 2: strip_tags('<p>Kevin <img src="someimage.png" onmouseover="someFunction()">van <i>Zonneveld</i></p>', '<p>');
    //   returns 2: '<p>Kevin van Zonneveld</p>'
    //   example 3: strip_tags("<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>", "<a>");
    //   returns 3: "<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>"
    //   example 4: strip_tags('1 < 5 5 > 1');
    //   returns 4: '1 < 5 5 > 1'
    //   example 5: strip_tags('1 <br/> 1');
    //   returns 5: '1  1'
    //   example 6: strip_tags('1 <br/> 1', '<br>');
    //   returns 6: '1 <br/> 1'
    //   example 7: strip_tags('1 <br/> 1', '<br><br/>');
    //   returns 7: '1 <br/> 1'






    allowed = (((allowed || '') + '')
        .toLowerCase()
        .match(/<[a-z][a-z0-9]*>/g) || [])
        .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
        commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    var result = input.replace(commentsAndPhpTags, '')
        .replace(/\u00A0/g, '')
        .replace(tags, function($0, $1) {
            return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
        });


    var element = $('<div>' + result + '</div>');

    element.find('*')
        .removeAttr('style');
    //.removeAttr('class');

    //Get the string
    var htmlString = element.eq(0).html();

    return htmlString;

}



var imgOnSelectAction = function(event, $element, editorScope) {

    // setup the editor toolbar
    // Credit to the work at http://hackerwins.github.io/summernote/ for this editbar logic/display
    var finishEdit = function() {
        editorScope.updateTaBindtaTextElement();
        editorScope.hidePopover();

    };

    event.preventDefault();

    //Add Popover css
    editorScope.displayElements.popover.css('width', 'auto');

    //Remove previous popovers
    var container = editorScope.displayElements.popoverContainer;
    container.empty();


    var buttonGroup = angular.element('<div class="btn-group">');
    var fullButton = angular.element('<button type="button" class="btn small" unselectable="on" tabindex="-1">100% </button>');
    fullButton.on('click', function(event) {
        event.preventDefault();
        $element.css({
            'width': '100%',
            'height': ''
        });
        finishEdit();
    });
    var halfButton = angular.element('<button type="button" class="btn small" unselectable="on" tabindex="-1">50% </button>');
    halfButton.on('click', function(event) {
        event.preventDefault();
        $element.css({
            'width': '50%',
            'height': ''
        });
        finishEdit();
    });
    var quartButton = angular.element('<button type="button" class="btn small" unselectable="on" tabindex="-1">25% </button>');
    quartButton.on('click', function(event) {
        event.preventDefault();
        $element.css({
            'width': '25%',
            'height': ''
        });
        finishEdit();
    });
    var resetButton = angular.element('<button type="button" class="btn small" unselectable="on" tabindex="-1">Reset</button>');
    resetButton.on('click', function(event) {
        event.preventDefault();
        $element.css({
            width: '',
            height: ''
        });
        finishEdit();
    });
    buttonGroup.append(fullButton);
    buttonGroup.append(halfButton);
    buttonGroup.append(quartButton);
    buttonGroup.append(resetButton);
    container.append(buttonGroup);

    buttonGroup = angular.element('<div class="btn-group">');
    var floatLeft = angular.element('<button type="button" class="btn small" unselectable="on" tabindex="-1"><i class="far fa-align-left"></i></button>');
    floatLeft.on('click', function(event) {
        event.preventDefault();
        // webkit
        $element.css('float', 'left');
        // firefox
        $element.css('cssFloat', 'left');
        // IE < 8
        $element.css('styleFloat', 'left');
        finishEdit();
    });
    var floatRight = angular.element('<button type="button" class="btn small" unselectable="on" tabindex="-1"><i class="far fa-align-right"></i></button>');
    floatRight.on('click', function(event) {
        event.preventDefault();
        // webkit
        $element.css('float', 'right');
        // firefox
        $element.css('cssFloat', 'right');
        // IE < 8
        $element.css('styleFloat', 'right');
        finishEdit();
    });
    var floatNone = angular.element('<button type="button" class="btn small" unselectable="on" tabindex="-1"><i class="far fa-align-justify"></i></button>');
    floatNone.on('click', function(event) {
        event.preventDefault();
        // webkit
        $element.css('float', '');
        // firefox
        $element.css('cssFloat', '');
        // IE < 8
        $element.css('styleFloat', '');
        finishEdit();
    });

    buttonGroup.append(floatLeft);
    buttonGroup.append(floatNone);
    buttonGroup.append(floatRight);
    container.append(buttonGroup);

    buttonGroup = angular.element('<div class="btn-group">');

    var remove = angular.element('<button type="button" class="btn small" unselectable="on" tabindex="-1"><i class="far fa-trash-o"></i></button>');

    remove.on('click', function(event) {
        event.preventDefault();
        $element.remove();
        finishEdit();
    });

    //Add the buttons
    buttonGroup.append(remove);
    container.append(buttonGroup);

    //Show popover
    editorScope.showPopover($element);
    editorScope.showResizeOverlay($element);
};


app.config(['$provide',
    function($provide) {

        $provide.decorator('taOptions', ['$rootScope', 'ModalService', 'taRegisterTool', 'taSelection', '$uibModal', '$delegate', 'Fluro',
            function($rootScope, ModalService, taRegisterTool, taSelection, $uibModal, taOptions, Fluro) {

                taOptions.toolbar = [
                    ['h1', 'h2', 'h3', 'h4', 'h5', 'quote'],
                    ['bold', 'italics', 'underline'],
                    ['sizeDown', 'sizeLead'],
                    ['ol', 'ul'],
                    ['customInsertImage', 'insertLink'],
                    //['justifyLeft', 'justifyCenter', 'justifyRight'],
                    ['undo', 'redo', 'clear'],
                    ['indent', 'outdent'],
                    ['html']
                ];

                taOptions.disableSanitizer = true;

                //////////////////////////////////////////////////////////////////

                taRegisterTool('sizeDown', {
                    iconclass: "fa fa-angle-down",
                    tooltiptext: 'Reduce text to small',
                    action: function(deferred, restoreSelection) {
                        var selection = angular.element(taSelection.getSelectionElement());
                        if(selection.hasClass('small')) {
                            selection.removeClass('small');
                        } else {
                            selection.addClass('small');
                        }
                    },
                    activeState: function(){
                        var selection = angular.element(taSelection.getSelectionElement());
                        return selection.hasClass('small');
                        //return this.$editor().queryCommandState('.lead');
                    },
                });

                //////////////////////////////////////////////////////////////////

                taRegisterTool('sizeLead', {
                    iconclass: "fa fa-angle-up",
                    tooltiptext: 'Increase text to lead',
                    action: function(deferred, restoreSelection) {
                        var selection = angular.element(taSelection.getSelectionElement());
                        if(selection.hasClass('lead')) {
                            selection.removeClass('lead');
                        } else {
                            selection.addClass('lead');
                        }
                    },
                    activeState: function(){
                        var selection = angular.element(taSelection.getSelectionElement());
                        return selection.hasClass('lead');
                        //return this.$editor().queryCommandState('.lead');
                    },
                });

                


                //////////////////////////////////////////////////////////////////

                // Create our own insertImage button
                taRegisterTool('customInsertImage', {
                    iconclass: "fa fa-image",

                    onElementSelect: {
                        element: 'img',
                        action: imgOnSelectAction
                    },

                    action: function() {
                        var textAngular = this;
                        //var savedSelection = rangy.saveSelection();
                        //var savedSelection = window.getSelection();
                        var selection = angular.element(taSelection.getSelectionElement());

                        //console.log('Get selection', selection)

                        var modalInstance = $uibModal.open({
                            template: '<content-browser ng-model="images" ng-done="submit" ng-type="' + "'image'" + '"></content-browser>',
                            controller: function($uibModalInstance, $scope) {



                                $scope.images = [];

                                $scope.submit = function() {
                                    //console.log('Got Images', $scope.images)
                                    $uibModalInstance.close($scope.images);
                                }
                            }
                        });

                        modalInstance.result.then(function(images) {
                            if (images.length) {
                                //var image = array[0];
                                var string = '';

                                _.each(images, function(image) {
                                    string += '<img src="' + $rootScope.getImageUrl(image._id) + '" ng-src="{{$root.asset.imageUrl(' + "'"+ image._id +"'"+ ')}}"/>';
                                })

                                //var embed = '<img src="'+ image.url+'" fluro-image="' + "'" + image._id + "'" + '" />';
                                //var embed = '<fluro-image="' + image._id + '"></fluro-image>';


                                //textAngular.$editor().wrapSelection('insertHTML', embed);
                                selection.before(string);

                                //console.log('Selection', selection);

                                //rangy.removeMarkers(savedSelection)
                            }

                            return;
                        });

                        return false;
                    },
                });


                return taOptions;
            }
        ]);
    }
]);

/**/