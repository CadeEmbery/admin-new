app.service('AccountService', function($state, Fluro, Notifications, CacheManager, SearchService, TypeService, Notifications, FluroTokenService, FluroContent, FluroSocket, $location) {

    //////////////////////////////////////////////////

    var service = {}

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    service.loginToAccount = function(id, asSession) {

        if(id._id) {
            id = id._id;
        }

        console.log('Login to account', asSession);

        if (asSession) {
            Fluro.sessionStorage = true;

            function success(res) {
                console.log('success now clear caches')
                CacheManager.clearAll();
                SearchService.clear();
                TypeService.refreshDefinedTypes();
                Notifications.status('Switched session to ' + res.data.account.title);
            }

            function fail(res) {
                Notifications.error('Error creating token session');
            }

            var request = FluroTokenService.getTokenForAccount(id);

            //Callback when the promise is complete
            request.then(success, fail);

        } else {

            // //Update the user
            // FluroContent.resource('user').update({
            //     id: $rootScope.user._id,
            // }, {
            //     account: id
            // }, loginSuccess, loginFailed);


            FluroSocket.off('session', UserSessionService.refresh)
                //Update the user
            FluroContent.endpoint('auth/switch/' + id).save({}).$promise.then(loginSuccess, loginFailed);
        }
    }
    

    //////////////////////////////////////////////////

    return service;
});
