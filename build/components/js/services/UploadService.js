app.service('UploadService', function(Fluro, Notifications, CacheManager, FileUploader) {


    var controller = {}

    /////////////////////////////////

    var settings = {
        url: Fluro.apiURL + '/file/upload',
        // url: Fluro.apiURL + '/file/upload/s3/591ba216c758ed4016e69bb6',
        //removeAfterUpload: true,
        withCredentials: true,
        formData: {},
    };


    //Upload with token instead
    if (Fluro.token) {
        settings.withCredentials = false;
        settings.url += '?access_token=' + Fluro.token;
    }

    controller.uploader = new FileUploader(settings);

    /////////////////////////////////



    /////////////////////////////////

    var _defaultData = {}

    /////////////////////////////////

    controller.startUpload = function(defaultData) {
        _defaultData = angular.copy(defaultData);

        console.log('DEFAULT DATA', _defaultData);

        controller.uploader.uploadAll();
    }

    /////////////////////////////////

    controller.uploader.onBeforeUploadItem = function(item) {

        //Get the metadata
        var metaData = _defaultData;

        if (!metaData) {
            metaData = {};
        }

        ///////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////

        //Extend the basic data with the item specific data

        if (!metaData.realms || !metaData.realms.length) {
            metaData.realms = [];
        }

        if (item.formData.realms && item.formData.realms.length) {
            metaData.realms = metaData.realms.concat(item.formData.realms);
        }

        //Unique
        metaData.realms = _.chain(metaData.realms)
            .map(function(realm) {
                if (realm._id) {
                    return realm._id;
                } else {
                    return realm;
                }
            })
            .uniq()
            .value();


        ///////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////

        if (!metaData.tags || !metaData.tags.length) {
            metaData.tags = [];
        }

        if (item.formData.tags && item.formData.tags.length) {
            metaData.tags = metaData.tags.concat(item.formData.tags)
        }

        //Unique
        metaData.tags = _.chain(metaData.tags)
            .map(function(tag) {
                if (tag._id) {
                    return tag._id;
                } else {
                    return tag;
                }
            })
            .uniq()
            .value();


        ///////////////////////////////////////////////////////////

        //metaData = _.assign(metaData, item.formData);

        //Serialize to JSON
        var json = angular.toJson(metaData);

        item.formData = [{
            json: json
        }];

        console.log(item);
    };




    controller.uploader.onErrorItem = function(fileItem, response, status, headers) {

        _.each(response.errors, function(err) {
            if (err.message) {
                Notifications.error(err.message);
            } else {
                Notifications.error(err);
            }
        })

        //Reset File Item and add it back into the queue
        fileItem.isError = false;
        fileItem.isUploaded = false;
        //controller.uploader.queue.push(fileItem);
    };

    /////////////////////////////////

    controller.uploader.onCompleteItem = function(fileItem, response, status, headers) {



        switch (status) {
            case 200:
                fileItem.serverItem = response;


                //Clear Cache
                CacheManager.clear(response._type);



                Notifications.status(response.title + ' was uploaded successfully');
                break;
            default:
                Notifications.error(response);
                break;
        }
    };


    return controller;


})