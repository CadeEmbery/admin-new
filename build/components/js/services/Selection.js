app.factory('Selection', function() {

	var Selection = function(selectedItems, sourceItems, canSelect) {

		var controller = {}


		//////////////////////////////////

		controller.items = [];
		controller.sourceItems = sourceItems;

		if(selectedItems) {
			controller.items = selectedItems;
		}

		//////////////////////////////////
		/*
		controller.ids = function() {
			
			return _.map(controller.items, function(item) {
				return item._id;
			})
		}
		*/
		//////////////////////////////////

        Object.defineProperty(controller, 'ids', {
            get: function() {
                return _.map(controller.items, function(item) {
                	if(_.isObject(item)) {
                    	return item._id;
                	} else {
                    	return item;
                	}
                })
            }
        });


        //////////////////////////////////

        Object.defineProperty(controller, 'length', {

            get: function() {
                return controller.items.length;
            }
        });




		//////////////////////////////////

		controller.toggle = function(item) {

			console.log('Toggle item', item);
			
			if(controller.contains(item)) {
				controller.deselect(item)
			} else {
				controller.select(item);
			}
		}

		//////////////////////////////////

		controller.select = function(item) {
			if(canSelect(item) && !controller.contains(item)) {
				controller.items.push(item)
			} 
		}

		//////////////////////////////////

		controller.deselect = function(item) {

			var i =_.find(controller.items, {_id:item._id});
			if(i) {
				_.pull(controller.items, i)
			}
		}

		//////////////////////////////////

		controller.selectMultiple = function(items) {
			//Loop through each item and select it if allowed
			_.each(items, function(item) {
				if(canSelect(item) && !controller.contains(item)) {
					controller.items.push(item)
				} 
			})
		}

		//////////////////////////////////

		controller.deselectMultiple = function(items) {
			//Loop through each item and select it if allowed
			_.each(items, function(item) {
				controller.deselect(item);
			})
		}

		//////////////////////////////////

		controller.containsAll = function(items) {
			var outcasts = _.filter(items, function(i) {
				return !controller.contains(i);
			})

			if(!outcasts.length) {
				return true;
			}
		}

		//////////////////////////////////

		controller.containsAny = function(items) {

			var ids = _.map(items, function(item) {
				return item._id;
			})

			var both = _.intersection(controller.ids, ids);

			return both.length;
		}

		//////////////////////////////////

		controller.selectAll = function() {
			controller.selectMultiple(controller.sourceItems);
		}

		//////////////////////////////////

		controller.deselectAll = function() {
			controller.items.length = 0;
		}

		//////////////////////////////////

		controller.contains = function(item) {
			if(item) {
				return _.some(controller.items, {_id:item._id});
			}
		}

		//////////////////////////////////
		//////////////////////////////////

		return controller;
	}

	return Selection;

});