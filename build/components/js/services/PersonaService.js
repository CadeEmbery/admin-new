app.service('PersonaService', function (FluroContent) {

	var service = {};

	///////////////////////////////////
	
	service.refresh = function() {

		if(service.request) {
			return service.request;
		}
		
		service.request = FluroContent.endpoint('user/personas', true, true).query().$promise;

		service.request.then(function(res) {
			service.personas = res;

			delete service.request;
		}, function(err) {
			delete service.request;
		})
	}

	///////////////////////////////////

	service.refresh();

	///////////////////////////////////

	return service;

	   
	
	
})