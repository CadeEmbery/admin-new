app.service('Tools', function() {

    var controller = {}

    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    /**/
    controller.getDeepProperty = function(obj, prop) {
        
        return _.get(obj, prop)

        /**
        var parts = prop.split('.'),
            last = parts.pop(),
            l = parts.length,
            i = 1,
            current = parts[0];

         if (!parts.length) {
            return obj[prop];
        }

        while ((obj = obj[current]) && i < l) {
            current = parts[i];
            i++;
        }

        if (obj) {
            if(_.isArray(obj)) {
                if(obj[last]) {
                    //things like .length
                    return obj[last]
                }
                
                if(obj[0]) {
                    return obj[0][last];
                }
            } else {
                return obj[last];
            }
        }
        /**/
    }
    /**/

    /*
    controller.getDeepProperty = function(o, s) {
       // console.log('Get property', s);

        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, ''); // strip a leading dot
        var a = s.split('.');
        for (var i = 0, n = a.length; i < n; ++i) {
            var k = a[i];
            if (k in o) {
                o = o[k];
            } else {
                return;
            }
        }

       // console.log('TEST PROPERTY', o, s, o)

        return o;
    }
*/


    /////////////////////////////////////////////////////

    return controller;
});