app.service('PermissionService', function(TypeService) {

    var controller = {};


    //////////////////////////////////////////////////

    //All available permissions
    controller.permissions = {};
    controller.permissions.batch = [];



    //////////////////////////////////////////////////

    var types = [];

    function addType(singular, plural) {

        var typeObject = {
            singular: singular,
            plural: plural,
        };

        types.push(typeObject);

        /**

        var typeObject = TypeService.getTypeFromPath(singular);

        if(typeObject) {
            console.log('SINGULAR', singular, typeObject);

            typeObject.singular = singular;
            typeObject.plural = plural;

            types.push(typeObject);

        }

        /**{
            singular:singular, 
            plural:plural,

        });
        /**/

    }



    ///////////////////////////////////////////////////


    //Standalone and allow a user to administrate an account
    controller.permissions.account = {
        type: 'account',
        plural: 'Accounts',
        source: {
            singular: 'Account',
            plural: 'Accounts',
            type: 'account',
        },
        permissions: [{
                shortTitle: 'Administrate Account Information',
                title: 'Administrate Account Information',
                value: 'administrate account',
                description: 'Update billing, invoices and Fluro Account Information',
            },

        ]

    }


    ///////////////////////////////////////////////////

    // addType('anything', 'anything');
    addType('article', 'articles');
    addType('assignment', 'assignments');
    addType('persona', 'Personas');
    //addType('user', 'users');
    addType('role', 'roles');
    addType('image', 'images');
    addType('video', 'videos');
    addType('audio', 'audio');
    addType('asset', 'assets');
    addType('tag', 'tags');
    addType('event', 'events');
    addType('location', 'locations');
    addType('collection', 'collections');
    // addType('endpoint', 'endpoints');
    addType('realm', 'realms');
    addType('site', 'sites');
    addType('contact', 'contacts');
    addType('family', 'families');
    addType('eventtrack', 'event tracks');

    addType('contactdetail', 'contactdetails');
    addType('interaction', 'interactions');
    addType('mailout', 'mailouts');
    addType('post', 'posts');

    addType('product', 'products');
    addType('purchase', 'purchases');
    addType('definition', 'definitions');
    addType('integration', 'integrations');
    addType('application', 'applications');
    addType('transaction', 'transactions');
    addType('package', 'packages');
    addType('code', 'code');
    addType('component', 'components');
    addType('query', 'queries');
    addType('plan', 'plans');
    addType('team', 'teams');
    addType('policy', 'access passes');
    addType('checkin', 'checkins');
    addType('reaction', 'reactions');
    addType('attendance', 'attendance headcount');
    addType('process', 'process cards');
    addType('roster', 'roster');
    addType('deployment', 'deployments');
    addType('capability', 'capabilities');
    addType('timetrigger', 'timetriggers');
    addType('resultset', 'resultsets');
    addType('ticket', 'tickets');
    addType('academic', 'academics');

    ////////////////////////////////////////////////



    ////////////////////////////////////////////////

    _.chain(types)
        .sortBy('singular')
        .each(function(type) {

            var source = TypeService.getTypeFromPath(type.singular);

            // console.log('source', source);
            addCrudPermission(type.singular, type.plural, source);
        })
        .value()
    ///////////////////////////////////////////////////

    //Global Applications
    // controller.permissions.application.permissions.push({
    //     title: 'Access Fluro admin panel',
    //     value: 'access admin panel',
    // });

    ///////////////////////////////////////////////////

    // //Integration Permissions
    // controller.permissions.integration.permissions.push({
    //     title: 'Access Secret Keys',
    //     value: 'access secret integration keys',
    // });

    ///////////////////////////////////////////////////

    // //Assign roles permission
    // controller.permissions.role.push({
    //     title: 'Assign roles to users',
    //     value: 'assign role',
    // });

    // //Assign roles permission
    // controller.permissions.user.push({
    //     title: 'Assign realms to users',
    //     value: 'assign realm',
    // });




    function addCrudPermission(type, plural, source) {


        var permissionsObject = {
            type: type,
            plural: plural,
            source: source,
            permissions: []
        }

        // console.log('Permissions Object', permissionsObject);


        if (!controller.permissions[type]) {
            controller.permissions[type] = permissionsObject; //[];
        }

        // controller.permissions[type].push({
        //     title: 'Access ' + plural + ' page',
        //     value: 'access ' + type + ' page',
        // });

        permissionsObject.permissions.push({
            shortTitle: 'Create',
            title: 'Create ' + plural,
            value: 'create ' + type,
            description: 'Can create new ' + plural,
        });


        permissionsObject.permissions.push({
            shortTitle: 'View any',
            title: 'View any ' + plural,
            value: 'view any ' + type,
            description: 'Can view any ' + type,
        });

        permissionsObject.permissions.push({
            shortTitle: 'View own',
            title: 'View own ' + plural,
            value: 'view own ' + type,
            description: 'Can view ' + plural + ' that were created by or are owned by the user',
        });



        permissionsObject.permissions.push({
            shortTitle: 'Edit any',
            title: 'Edit any ' + plural,
            value: 'edit any ' + type,
            description: 'Can edit any ' + type,
        });

        permissionsObject.permissions.push({
            shortTitle: 'Edit own',
            title: 'Edit own ' + plural,
            value: 'edit own ' + type,
            description: 'Can edit ' + plural + ' that were created by or are owned by the user',
        });

        ///////////////////////////////////////////

        if (type == 'transaction') {

            permissionsObject.permissions.push({
                shortTitle: 'Refund',
                title: 'Refund transactions',
                value: 'refund transaction',
                description: 'Can refund transactions',
            });
            return;
        }

        ///////////////////////////////////////////

        permissionsObject.permissions.push({
            shortTitle: 'Delete any',
            title: 'Delete any ' + plural,
            value: 'delete any ' + type,
            description: 'Can delete any ' + type,
        });


        ///////////////////////////////////////////////////////////

        //Dont allow the user to delete their own account
        if (type != 'user') {
            permissionsObject.permissions.push({
                shortTitle: 'Delete own',
                title: 'Delete own ' + plural,
                value: 'delete own ' + type,
                description: 'Can delete ' + plural + ' that were created by or are owned by the user',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Destroy any',
                title: 'Destroy any ' + plural,
                value: 'destroy any ' + type,
                description: 'Can destroy any ' + type + ' that is currently in the trash',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Destroy own',
                title: 'Destroy own ' + plural,
                value: 'destroy own ' + type,
                description: 'Can destroy ' + plural + ' that were created by or are owned by the user that are currently in the trash',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Restore any',
                title: 'Restore any ' + plural,
                value: 'restore any ' + type,
                description: 'Can restore any ' + type + ' from trash',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Restore own',
                title: 'Restore own ' + plural,
                value: 'restore own ' + type,
                description: 'Can restore ' + plural + ' from trash that were created by or are owned by the user',
            });
        }


        //Add submission permission for interaction and post
        if (type == 'interaction' || type == 'post') {
            permissionsObject.permissions.push({
                title: 'Submit ' + plural,
                value: 'submit ' + type,
                description: 'Can submit ' + plural + ' through the interact endpoint',
            });
        }



        if (type == 'team') {

            permissionsObject.permissions.push({
                shortTitle: 'Join',
                title: 'Join Groups / Teams',
                value: 'join team',
                description: 'Can join groups or teams that allow provisional membership',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Leave',
                title: 'Leave Groups / Teams',
                value: 'leave team',
                description: 'Can leave groups or teams that allow provisional membership',
            });
        }


        if (type == 'team' || type == 'event') {

            permissionsObject.permissions.push({
                shortTitle: 'View Chat Feed',
                title: 'View chat on ' + plural,
                value: 'view chat ' + type,
                description: 'Can view ' + plural + ' chats that user is not a member of',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Post Chat Feed',
                title: 'Post chats to ' + plural,
                value: 'chat ' + type,
                description: 'Can post ' + plural + ' chats that user is not a member of',
            });



        }




        switch (type) {
            case 'checkin':
                permissionsObject.permissions.push({
                    shortTitle: 'Leader Override Checkout',
                    title: 'Leader Override Checkout',
                    value: 'leader checkout ' + type,
                    description: 'Can manually override and force a checkout',
                });
                break;
            case 'ticket':
                permissionsObject.permissions.push({
                    shortTitle: 'Collect Tickets',
                    title: 'Collect Tickets',
                    value: 'collect ' + type,
                    description: 'Can scan a ticket and mark it as "collected"',
                });
                break;
            case 'assignment':

            case 'family':
            case 'method':
            case 'purchase':
            case 'tag':
                break;
                ////////////////////////////////////////////
            case 'role':
                permissionsObject.permissions.push({
                    shortTitle: 'Assign individual roles',
                    title: 'Assign individual roles',
                    value: 'assign role',
                    description: 'Can assign individual permission sets to personas that the user is allowed to edit',
                });
                break;
            case 'policy':
                // permissionsObject.permissions.push({
                //     shortTitle: 'Assign access passes',
                //     title: 'Assign access passes',
                //     value: 'assign policy',
                //     description:'Can assign access passes that the user can view to personas that the user is allowed to edit',
                // });


                // permissionsObject.permissions.push({
                //     shortTitle: 'Modify persona access passes',
                //     title: 'Modify persona access passes',
                //     value: 'modify persona policy',
                //     description:'Can modify access passes on any persona that the user is allowed to edit',
                // });


                permissionsObject.permissions.push({
                    shortTitle: 'Grant access pass',
                    title: 'Grant access pass',
                    value: 'grant policy',
                    description: 'Can allocate any access passes to other users',
                });


                permissionsObject.permissions.push({
                    shortTitle: 'Grant held access pass',
                    title: 'Grant held access pass',
                    value: 'grant held policy',
                    description: 'Can allocate access passes that are held by the current user to other users',
                });

                permissionsObject.permissions.push({
                    shortTitle: 'Revoke access pass',
                    title: 'Revoke access pass',
                    value: 'revoke policy',
                    description: 'Can revoke access passes from personas',
                });

                break;
            case 'contact':
                permissionsObject.permissions.push({
                    shortTitle: 'SMS',
                    title: 'Send SMS to contacts',
                    value: 'sms',
                    description: 'Can send SMS messages to contacts that the user is allowed to view',
                });

                permissionsObject.permissions.push({
                    shortTitle: 'Email',
                    title: 'Send Basic Email to contacts',
                    value: 'email',
                    description: 'Can send email messages via fluro to contacts that the user is allowed to view',
                });

               
                break;
            case 'persona':
                permissionsObject.permissions.push({
                    shortTitle: 'Impersonate',
                    title: 'Impersonate other users',
                    value: 'impersonate',
                    description: 'Can impersonate and pretend to personas that the user is allowed to edit',
                });


                // permissionsObject.permissions.push({
                //     shortTitle: 'Assign access passes',
                //     title: 'Assign access passes',
                //     value: 'assign policy',
                //     description:'Can assign access passes that the user can view to personas that the user is allowed to edit',
                // });

                permissionsObject.permissions.push({
                    shortTitle: 'Assign individual roles',
                    title: 'Assign individual roles',
                    value: 'assign role',
                    description: 'Can assign individual permission sets to personas that the user is allowed to edit',
                });

                break;
            default:
                // permissionsObject.permissions.push({
                //     shortTitle: 'Match derivatives',
                //     title: 'Include defined ' + plural,
                //     value: 'include defined ' + type,
                //     description: 'Any permissions held for this type will also flow down to any extended definitions',
                // });
                break;
        }



         permissionsObject.permissions.push({
                    shortTitle: 'Match derivatives',
                    title: 'Include defined ' + plural,
                    value: 'include defined ' + type,
                    description: 'Any permissions held for this type will also flow down to any extended definitions',
                });



        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////

        /**
        //Dont allow the user to delete their own account
        if (type != 'user') {

            //Derivatives
            permissionsObject.permissions.push({
                shortTitle: 'Create',
                title: 'Create ' + type + ' derivatives',
                value: 'create ' + type + ' derivatives',
            });

            permissionsObject.permissions.push({
                shortTitle: 'View any',
                title: 'View any ' + type + ' derivatives',
                value: 'view any ' + type + ' derivatives',
            });

            permissionsObject.permissions.push({
                shortTitle: 'View own',
                title: 'View own ' + type + ' derivatives',
                value: 'view own ' + type + ' derivatives',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Edit any',
                title: 'Edit any ' + type + ' derivatives',
                value: 'edit any ' + type + ' derivatives',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Edit own',
                title: 'Edit own ' + type + ' derivatives',
                value: 'edit own ' + type + ' derivatives',
            });

            ///////////////////////////////////////////

            permissionsObject.permissions.push({
                shortTitle: 'Delete any',
                title: 'Delete any ' + type + ' derivatives',
                value: 'delete any ' + type + ' derivatives',
            });


            permissionsObject.permissions.push({
                shortTitle: 'Delete own',
                title: 'Delete own ' + type + ' derivatives',
                value: 'delete own ' + type + ' derivatives',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Destroy any',
                title: 'Destroy any ' + type + ' derivatives',
                value: 'destroy any ' + type + ' derivatives',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Destroy own',
                title: 'Destroy own ' + type + ' derivatives',
                value: 'destroy own ' + type + ' derivatives',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Restore any',
                title: 'Restore any ' + type + ' derivatives',
                value: 'restore any ' + type + ' derivatives',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Restore own',
                title: 'Restore own ' + type + ' derivatives',
                value: 'restore own ' + type + ' derivatives',
            });
        }

        /**/

        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////


        switch (type) {
            case 'tag':
            case 'role':
            case 'user':
            case 'endpoint':
            case 'realm':
            case 'location':
                //Disable these for the moment
                break;
            default:
                /* controller.permissions[type].push({
                    title: 'Manage tags on ' + plural,
                    value: 'tag ' + type
                });
*/
                break;

        }
    }


    return controller;

});