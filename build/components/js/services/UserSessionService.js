app.service('UserSessionService', function($rootScope, TypeService, Notifications, SearchService, FluroContent, FluroTokenService, CacheManager) {


    var service = {};

    ///////////////////////////////////////

    service.refresh = function() {

    	if($rootScope.user.refreshToken) {
    		//Dont change if we are already in a session
    		return;
    	}

        //Load the user session from the server
        FluroContent.endpoint('session')
            .get()
            .$promise
            .then(function(res) {
                console.log('session refreshed', res);

                FluroTokenService.deleteSession();
                CacheManager.clearAll();
                SearchService.clear();;
                $rootScope.user = res;
                TypeService.refreshDefinedTypes();
                Notifications.warning('Your user session has been updated');
            })
    }

    ///////////////////////////////////////

    return service;
})