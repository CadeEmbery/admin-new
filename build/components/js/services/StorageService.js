app.factory('StorageService', function($q, $rootScope, FluroContent) {

    // url is after api.fluro.io/
    // refreshEventName is the event that will be triggered when the service gets new content
    var StorageService = function(url, refreshEventName, queryParams) {

        var service = {
            // items: [], //Dont start with this so we can check if it ever loaded content
        }

        /////////////////////////////////

        //Keeps track of any inflight requests
        //Which stops multiple simultaneous requests
        //being made to the server
        var promise;

        /////////////////////////////////

        //Function that reloads the content from the server
        service.reload = function() {

            //console.log('RELOAD')
            if (promise) {
                return promise;
            }

            ///////////////////////////////////////

            //Create a promise
            var deferred = $q.defer();

            //Keep track of the request
            promise = deferred.promise;
            service.processing = true;

            /////////////////////////////////////////

            if (!queryParams) {
                queryParams = {};
            }

            /////////////////////////////////////////

            //Request an update of all available events today
            var promise = FluroContent.endpoint(url, true, true)
                .query(queryParams)
                .$promise;

            //Listen
            promise.then(function(res) {
                service.processing = false;
                service.items = res;
                deferred.resolve(res);

                //Clear the promise so the service can be refresh again
                promise = null;

                //Broadcast refresh event
                $rootScope.$broadcast(refreshEventName);

            }, function(err) {
                service.processing = false;
                var res = [];
                service.items = res
                deferred.resolve(res);

                //Clear the promise so the service can be refresh again
                promise = null;

                //Broadcast refresh event
                $rootScope.$broadcast(refreshEventName);

            })

            /////////////////////////////////////////

            return deferred.promise;


        }

        /////////////////////////////////////////

        service.clear = function() {
            if (service.items) {
                service.items = [];
            }
            $rootScope.$broadcast(refreshEventName);
        }

        /////////////////////////////////////////

        return service;

    }

    /////////////////////////////////////

    return StorageService;
})
