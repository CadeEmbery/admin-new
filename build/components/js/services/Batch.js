app.service('Batch', function(FluroContent, Fluro, $http, $uibModal, CacheManager, $interval, $rootScope, $window, Fluro, Notifications) {

    var controller = {};


    //////////////////////////////////////////////////  

    controller.current = [];
    controller.callbacks = {};

    //////////////////////////////////////////////////

    controller.BATCH_COMPLETE = 'batch.complete';
    controller.BATCH_INIT = 'batch.init';

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    //Check the status of existing background tasks
    var timer = $interval(pollStatus, 2000);

    //////////////////////////////////////////////////

    controller.closeModal = function() {
        if(controller.modalInstance) {
            controller.modalInstance.close();
        }
    }

    //////////////////////////////////////////////////

    $rootScope.$watch(function() {
        return controller.current.length;
    }, function(num) {
        if(!num) {
            controller.closeModal();
        }

    })

    //////////////////////////////////////////////////

    controller.showModal = function() {

        //Close any existing modal
        controller.closeModal();

        controller.modalInstance = $uibModal.open({
            template: '<admin-batch-queue></admin-batch-queue>',
            size: 'md',
            backdrop: 'static',
        });
    }

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    controller.callbackTask = function(task) {


        ///////////////////////


        //Find all the caches we need to clear
        var caches = [];

        ///////////////////////

        //Refresh any caches that might be affected outside of this view
        if (task.result) {
            if (task.result.types && task.result.types.length) {
                caches = caches.concat(task.result.types)
            }

            if (task.result.definitions && task.result.definitions.length) {
                caches = caches.concat(task.result.definitions)
            }
        }

        ///////////////////////

        caches = _.uniq(caches);
        caches = _.compact(caches);

        console.log('!!!!!!!!! CLEAR TASK CACHES', caches)

        _.each(caches, function(t) {
            CacheManager.clear(t);
        });

        ///////////////////////
        ///////////////////////
        ///////////////////////
        ///////////////////////
        

        //Remove this task from the current list
        _.pull(controller.current, task);

        //find any callback listed for this task
        var taskID = task._id;

        //Get the registered callback
        var callback = controller.callbacks[taskID];

        //If there is a callback
        if(callback) {

            console.log('Calling back!!');
            //Call it now
            callback(null, task);

            //And unregister the callback
            controller.callbacks[taskID] = null;
        }
    }

    //////////////////////////////////////////////////

    controller.completeTask = function(task) {


        
        switch(task.status) {
            case 'prompt':
            case 'complete':
                console.log('COMPLETE TASK', task);
            break;
            default:
                return;
            break;
        }
        
        ///////////////////////

        if (task.result && task.result.downloadPath && task.result.downloadPath.length) {
            var url = Fluro.apiURL + task.result.downloadPath;
            console.log('Opening', url)
            Notifications.post('Your popup blocker may stop this file from downloading', 'warning');

            if (Fluro.token) {
                $window.open(url + '?access_token=' + Fluro.token);
            } else {
                $window.open(url);
            }

            //Set task to archived
            task.status = 'complete';

            //Remove from the list
            controller.callbackTask(task);

        } else {
            console.log('NO DOWNLOAD PATH', task.result);
        }
    }

    //////////////////////////////////////////////////

    controller.abortTask = function(task) {
        // console.log('Abort Task!!!')

        var request = FluroContent.endpoint('tasks/' + task._id, false, true).delete().$promise;

        request.then(function(res) {
            Notifications.warning(task.title + ' was cancelled');
            task.status = 'aborted';
        }, function(err) {
            Notifications.error(err);
        })
        return request;
    }

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    var polling;

    function pollStatus() {

        ///////////////////////////////////////////////////

        var processing = _.filter(controller.current, function(item) {

            var complete = item.status == 'complete';
            var prompt = item.status == 'prompt';

            return (!complete && !prompt);
        })

        ///////////////////////////////////////////////////

        if (processing.length && !polling) {

            //Mark polling as true
            polling = true;

            //////////////////////////////////////////////////////

            var IDs = _.map(processing, function(task) {
                return task._id;
            })

            //////////////////////////////////////////////////////

            var request = $http.post(Fluro.apiURL + '/tasks', {ids:IDs});
            // FluroContent.endpoint('tasks', false, true).save({
            //     ids:IDs
            // }).$promise;

            request.then(function(res) {
                polling = false;

                //Update the current list
                controller.current = res.data;

                // var item = controller.current = _.map(controller.current, {_id: res._id})
                var stillProcessing = _.filter(res.data, function(item) {
                    var complete = item.status == 'complete';
                    var prompt = item.status == 'prompt';

                    return (!complete && !prompt);
                })

                // if(!stillProcessing.length) {
                //     console.log('Batches complete')
                // }
                // console.log(stillProcessing.length, 'batches still in process');
            
            }, function(err) {
                polling = false;
                console.log('ERR', err.data);
            })
        }
    }

    ////////////////////////////////
    ////////////////////////////////
    ////////////////////////////////
    ////////////////////////////////
    ////////////////////////////////
    ////////////////////////////////
    ////////////////////////////////

    function runBatch(details, callback) {

        //console.log('Run Batch Operation', details);

        if(details.ids && details.ids.length < 10) {
            console.log('Set Immediate')
            details.immediate = true;
        }

        var request = FluroContent.endpoint('batch', false, true).batch(details).$promise;


        ////////////////////////////////

        function batchCreateSuccess(task) {

            $rootScope.$broadcast(controller.BATCH_INIT);

            var taskID = task._id;
            if(callback && taskID) {
                console.log('Register callback');
                controller.callbacks[taskID] = callback;
            }

            //Push this batch to our current list
            controller.current.push(task);

            controller.showModal();



            // if (callback) {
            //     //Dispatch event to the rest of the application
            //     $rootScope.$broadcast(controller.BATCH_INIT);

            //     //Show modal
            //     controller.showModal();

            //     return callback(null, res);
            // }
        }

        ////////////////////////////////

        function batchCreateFail(res) {
            if (callback) {
                return callback(res.data);
            }
        }

        ////////////////////////////////

        request.then(batchCreateSuccess, batchCreateFail);
    }


    ////////////////////////////////

    controller.exportItems = function(details, callback) {
        details.operation = 'export';
        runBatch(details, function(err, result) {
            if (err) {
                console.log(err);
                return err;
            }

            /**/
            if (result.download) {
                console.log('Opening', Fluro.apiURL + result.download)
                Notifications.post('Your popup blocker may stop this file from downloading', 'warning');

                if (Fluro.token) {
                    $window.open(Fluro.apiURL + result.download + '?access_token=' + Fluro.token);
                } else {
                    $window.open(Fluro.apiURL + result.download);
                }
            }
            /**/
        });
    }

    ////////////////////////////////

    controller.downloadItems = function(details, callback) {
        details.operation = 'download';
        runBatch(details, function(err, result) {
            if (err) {
                console.log(err);
                return err;
            }

            if (result.download) {
                console.log('Opening', Fluro.apiURL + result.download)
                Notifications.post('Your popup blocker may stop this file from downloading', 'warning');


                if (Fluro.token) {
                    $window.open(Fluro.apiURL + result.download + '?access_token=' + Fluro.token);
                } else {
                    $window.open(Fluro.apiURL + result.download);
                }
                // $window.open(Fluro.apiURL + result.download);
            }
        });
    }

    ////////////////////////////////

    controller.setPrivacy = function(details, callback) {
        details.operation = 'privacy';
         runBatch(details, callback);
    }


    ////////////////////////////////

    controller.adjustTime = function(details, callback) {
        details.operation = 'adjust time';
        runBatch(details, callback);
    }


    ////////////////////////////////

    controller.assignToContacts = function(details, callback) {
        details.operation = 'assign to contact';
        runBatch(details, callback);
    }

    controller.assignToTeams = function(details, callback) {
        details.operation = 'assign to team';
        runBatch(details, callback);
    }


    ////////////////////////////////

    controller.assignPosition = function(details, callback) {
        details.operation = 'assign position';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.addOwner = function(details, callback) {
        details.operation = 'add owner';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.mergeRealms = function(details, callback) {
        details.operation = 'merge realms';
        runBatch(details, callback);
    }


    

    ////////////////////////////////

    controller.removeOwner = function(details, callback) {
        details.operation = 'remove owner';
        runBatch(details, callback);
    }


    ////////////////////////////////

    controller.reprocess = function(details, callback) {
        details.operation = 'reprocess';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.setStatus = function(details, callback) {
        details.operation = 'status';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.resave = function(details, callback) {
        details.operation = 'resave';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.resendConfirmations = function(details, callback) {
        details.operation = 'resend confirmation';
        runBatch(details, callback);
    }


    ////////////////////////////////

    controller.redefine = function(details, callback) {
        details.operation = 'redefine';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.removeDefinition = function(details, callback) {
        details.operation = 'remove definition';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.deleteItems = function(details, callback) {
        details.operation = 'delete';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.restore = function(details, callback) {
        details.operation = 'restore';
        runBatch(details, callback);
    }


    ////////////////////////////////

    controller.updateFields = function(details, callback) {
        details.operation = 'update';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.destroy = function(details, callback) {
        details.operation = 'destroy';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.spark = function(details, callback) {
        details.operation = 'spark';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.addToCollections = function(details, callback) {
        details.operation = 'collect';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.addTags = function(details, callback) {
        details.operation = 'add tag';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.removeTags = function(details, callback) {
        details.operation = 'remove tag';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.addFamilies = function(details, callback) {
        details.operation = 'add family';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.removeFamilies = function(details, callback) {
        details.operation = 'remove family';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.addRealms = function(details, callback) {
        details.operation = 'add realm';
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.removeRealms = function(details, callback) {
        details.operation = 'remove realm';
        runBatch(details, callback);
    }


    ////////////////////////////////

    controller.addPolicies = function(details, callback) {
        details.operation = 'add policy';
        details.useModel = 'persona';
        // details.users = true;
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.removePolicies = function(details, callback) {
        details.operation = 'remove policy';
        details.useModel = 'persona';
        // details.users = true;
        runBatch(details, callback);
    }

    ////////////////////////////////

    controller.setEventTrack = function(details, callback) {
        details.operation = 'set track';
        runBatch(details, callback);
    }


    controller.addToProducts = function(details, callback) {
        details.operation = 'add to product';
        runBatch(details, callback);
    }

    controller.addToTeam = function(details, callback) {
        details.operation = 'add to team';
        runBatch(details, callback);
    }

    controller.removeFromTeam = function(details, callback) {
        details.operation = 'remove from team';
        runBatch(details, callback);
    }



    ////////////////////////////////

    controller.removeFromProducts = function(details, callback) {
        details.operation = 'remove from product';
        runBatch(details, callback);
    }


    ////////////////////////////////

    controller.addToProcess = function(details, callback) {
        details.operation = 'add to process';
        runBatch(details, callback);
    }

    ////////////////////////////////
    ////////////////////////////////


    return controller;
});