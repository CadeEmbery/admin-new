(function() {
    var w = window;
    var ic = w.Intercom;
    if (typeof ic === "function") {
        ic('reattach_activator');
        ic('update', intercomSettings);
    } else {
        var d = document;
        var i = function() {
            i.c(arguments)
        };
        i.q = [];
        i.c = function(args) {
            i.q.push(args)
        };
        w.Intercom = i;

        function l() {
            var s = d.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = 'https://widget.intercom.io/widget/s455b1lz';
            var x = d.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        }
        if (w.attachEvent) {
            w.attachEvent('onload', l);
        } else {
            w.addEventListener('load', l, false);
        }
    }
})();



app.service('IntercomService', function($rootScope) {



    ////////////////////////////

    var service = {

    };

    service.toggle = function() {
        if (!service.visible) {
            service.show();
        } else {
            service.hide();
        }
    }

    ////////////////////////////

    service.show = function() {
        service.visible = true;
        Intercom('show');
    }

    ////////////////////////////

    service.hide = function() {
        service.visible = false;
        Intercom('hide');
    }

    ////////////////////////////

    $rootScope.$watch('user._id + user.account._id', function() {

        var user = $rootScope.user;

        if (user) {

            if (!service.booted) {
                window.Intercom("boot", {
                    app_id: "s455b1lz",
                    // custom_launcher_selector: '.intercom_launch',
                    hide_default_launcher: true

                });
                service.booted = true;
            }


            var userName = user.firstName + ' ' + user.lastName;
            Intercom('update', {
                "name": userName,
                "account": user.account.title,
                "id": user._id,
                "persona": user.persona,
                "email": user.email,
                "company": {
                    "id": user.account._id,
                    "name": user.account.title,
                },
                
            });
        } else {
            service.booted = false;
            Intercom('shutdown');
        }

    });


    ////////////////////////////

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams) {
        window.Intercom("update");
    });

    ////////////////////////////

    return service;


})