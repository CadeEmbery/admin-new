app.service('GenericPermissionService', function(FluroContent) {

    var controller = {}
    controller.permissions = {};

    //////////////////////////////

    addCrudPermission = function(type, plural, source) {
        // if (!controller.permissions[type]) {
        //     controller.permissions[type] = [];
        // }


        source.singular  = source.title;

        var permissionsObject = {
            type:type,
            plural:plural,
            source:source,
            permissions:[]
        }

        //Add the object
        controller.permissions[type] = permissionsObject;

        permissionsObject.permissions.push({
            title: 'Create ' + plural,
            value: 'create ' + type,
        });


        permissionsObject.permissions.push({
            title: 'View any ' + plural,
            value: 'view any ' + type,
        });

        permissionsObject.permissions.push({
            title: 'View own ' + plural,
            value: 'view own ' + type,
        });

        permissionsObject.permissions.push({
            title: 'Edit any ' + plural,
            value: 'edit any ' + type,
        });

        permissionsObject.permissions.push({
            title: 'Edit own ' + plural,
            value: 'edit own ' + type,
        });

        permissionsObject.permissions.push({
            title: 'Delete any ' + plural,
            value: 'delete any ' + type,
        });

        permissionsObject.permissions.push({
            title: 'Delete own ' + plural,
            value: 'delete own ' + type,
        });

        permissionsObject.permissions.push({
            title: 'Destroy any ' + plural,
            value: 'destroy any ' + type,
        });

        permissionsObject.permissions.push({
            title: 'Destroy own ' + plural,
            value: 'destroy own ' + type,
        });

        permissionsObject.permissions.push({
            title: 'Restore any ' + plural,
            value: 'restore any ' + type,
        });

        permissionsObject.permissions.push({
            title: 'Restore own ' + plural,
            value: 'restore own ' + type,
        });

        ///////////////////////////////////////////////////////////////////

        var checkType = source.parentType;

        //Add submission permission for interaction and post
        if (checkType == 'interaction' || checkType == 'post') {
            permissionsObject.permissions.push({
                title: 'Submit ' + plural,
                value: 'submit ' + type,
                description:'Can submit ' + plural + ' through the post/interact endpoints',
            });
        }



        if (checkType == 'team') {

            permissionsObject.permissions.push({
                shortTitle: 'Join',
                title: 'Join ' + plural,
                value: 'join ' + type,
                description:'Can join '+plural+' that allow provisional membership',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Leave',
                title: 'Leave ' + plural,
                value: 'leave ' + type,
                description:'Can leave '+ plural +' that allow provisional membership',
            });
        }


        if (checkType == 'team' || checkType == 'event') {

            permissionsObject.permissions.push({
                shortTitle: 'View Chat Feed',
                title: 'View chat on ' + plural,
                value: 'view chat ' + type,
                description:'Can view '+plural+' chats that user is not a member of',
            });

            permissionsObject.permissions.push({
                shortTitle: 'Post Chat Feed',
                title: 'Post chats to ' + plural,
                value: 'chat ' + type,
                description:'Can post '+plural+' chats that user is not a member of',
            });



        }
    }

    //////////////////////////////

    controller.getPermissions = function() {

        controller.permissions = {};

        //Get all the options
        FluroContent.endpoint('defined', true, true).query({
           allDefinitions:true,
        })
        .$promise
        .then(function(data) {


             _.chain(data)
            .sortBy('title')
            .each(function(definition) {
                addCrudPermission(definition.definitionName, definition.plural, definition);
            })
             .value();
        });

        return controller.permissions;
    }

    //////////////////////////////
    //////////////////////////////
    //////////////////////////////
    //////////////////////////////

    return controller;

});