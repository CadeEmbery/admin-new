app.service('MyProcessService', function($rootScope, $timeout, ModalService, FluroContent, StorageService) {


    var service = new StorageService('my/tasks', 'processes.refreshed', {
        // all:true
    });

    ////////////////////////////////////////

    $rootScope.$on('processes.refreshed', function() {
        //Update the total count
        service.total = service.items.length;
    });

    ////////////////////////////////////////

    service.initialAssign = function(card, callback) {

        var promptTitle = 'Assign ' + _.startCase(card.title) + ' to';

        // var promptSubtitle = 'Assign ' + _.startCase(card.title) + ' ';

        //First prompt the user
        var actions = {
            title: promptTitle,
            // subtitle: $scope.event.title, //'What would you like to do',
            androidEnableCancelButton: true, // default false
            winphoneEnableCancelButton: true, // default false
            addCancelButtonWithLabel: 'Cancel',
            buttonLabels: [
                'Assign to me',
                'Select contact(s)',
            ]
        }

        window.plugins.actionsheet.show(actions, function(buttonIndex) {

            $timeout(function() {


                switch(buttonIndex) {
                    case 1:
                    //Take the card
                    console.log('OPTION 1');
                        return service.assignSelf(card, callback)
                    break;                
                    case 2:
                    console.log('OPTION 2');
                        return service.reassign(card, callback)
                    break;
                    default:
                    console.log('OPTION 3');
                        return callback();
                    break;

                }

            })
        })
    }

    ////////////////////////////////////////

    service.assignSelf = function(card, callback) {

        
        /////////////////////////////////////

        console.log('ASSIGN SELF NOW')
        FluroContent.endpoint('process/reassign/' + card._id + '/self')
        .update({})
        .$promise
        .then(reassignSuccess, reassignFail);

        /////////////////////////////////////

        function reassignSuccess(res) {
            console.log('Assign Self', res);
            return callback(null, card);
        }

        function reassignFail(err) {
            console.log('Assign Self Error', err);
            return callback(err);
        }
    }

    ////////////////////////////////////////

    service.reassign = function(card, callback) {


        console.log('Reassign to someone else!');
        var collection = [];

        var promise = ModalService.browse({
            title: 'Contact',
            path: 'contact',
            plural: 'Contacts',
        }, collection);

        //Listen for the modal to have a selection
        promise.then(function(items) {
            if (!items || !items.length) {
                console.log('No matches')
                return;
            }

            NotificationService.indicate('Reassigning', 'far fa-fw fa-spinner-third fa-spin', 8000);

            /////////////////////////////////////

            //Get the contact ids
            var contactIDs = _.map(items, '_id');

            /////////////////////////////////////

            FluroContent.endpoint('process/reassign/' + card._id).update({
                contacts: contactIDs
            }).$promise.then(reassignSuccess, reassignFail);

            /////////////////////////////////////

            function reassignSuccess(res) {
                console.log('Reassigned!', res);

                card.assignedTo = res.assignedTo;

                //Reload the processes list as things have changed
                service.reload();
                NotificationService.indicate('Reassigned!', 'far fa-check');

                return callback(null, card);
            }

            function reassignFail(err) {
                console.log('Reassign Failed!', err);
                NotificationService.indicate('Unable to reassign', 'far fa-times');
                return callback(err);
            }
        });
    }

    ////////////////////////////////////////

    service.archive = function(card, callback) {

        FluroContent.endpoint('process/archive/' + card._id)
            .update({})
            .$promise
            .then(archiveSuccess, archiveFail);

        /////////////////////////////////////

        function archiveSuccess(res) {
            card.status = res.archived;

            //Reload the processes list as things have changed
            service.reload();

            NotificationService.indicate('Archived!', 'far fa-check');

            return callback(null, card);
        }

        function archiveFail(err) {
            console.log('Reassign Failed!', err);
            NotificationService.indicate('Failed', 'far fa-times');
            return callback(err);
        }
    }

    ////////////////////////////////////////

    service.getIcon = function(status, current) {
        var icon = 'fal fa-circle';
        var style = 'text-muted';

        //Update it if we need to
        switch (status) {
            case 'complete':
                style = 'brand-success';
                icon = 'fas fa-check-circle';
                break;
            case 'failed':
                style = 'brand-danger';
                icon = 'fas fa-exclamation-circle';
                break;
            case 'pending':
                style = 'brand-warning';
                icon = 'fas fa-minus-circle';
                break;
        }


        if (current && status != 'complete') {
            icon = 'far fa-dot-circle';
        }

        return icon + ' ' + style;
    }


    ////////////////////////////////////////

    return service;

});


app.service('AvailableProcessCardsService', function($rootScope, StorageService) {
    return new StorageService('my/tasks/available', 'process.available.refreshed');

});
