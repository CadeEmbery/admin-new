app.service('PinService', function(FluroContent, $timeout, Fluro, $q, $http, Notifications) {


    var controller = {
        pinned: [],
    }

    ///////////////////////////////////////////

    controller.isPinned = function(itemID) {

        if (!itemID) {
            return;
        }

        if (itemID._id) {
            itemID = itemID._id;
        }

        return _.some(controller.pinned, {
            _id: itemID
        });
    }

    ///////////////////////////////////////////

    controller.pin = function(item) {
        if (item.pinning) {
            return;
        }


        // NotificationService.indicate('Pinning', 'fa fa-spinner-third fa-spin', 20000);

        item.pinning = true;

        /////////////////////////////////////////


        var request = FluroContent.endpoint('stat/' + item._id + '/pinned?unique=true').save({}).$promise;

        /////////////////////////////////////////

        request.then(function(res) {
            $timeout(function() {


                item.pinning = false;
                controller.pinned.push(item);
            })

            Notifications.status('\'' + item.title + '\' was pinned')

            // NotificationService.indicate('Pinned to home', 'ti ti-pin-alt');
            // NotificationService.notify('Pinned ' + "'" +item.title + "'");
        }, function() {
            Notifications.warning('Failed to pin \'' + item.title +'\'')
            // NotificationService.indicate('Failed to Pin', 'ti ti-face-sad');
            item.pinning = false;
        })

        return request;
    }

    ///////////////////////////////////////////

    controller.unpin = function(item) {




        if (item.pinning) {
            return console.log('ITEM IS ALREADY PINNING');
        }

        // NotificationService.indicate('Unpinning', 'fa fa-spinner-third fa-spin', 20000);
        item.pinning = true;

        /////////////////////////////////////////

        var request = FluroContent.endpoint('stat/' + item._id + '/pinned?unique=true').delete({}).$promise;

        /////////////////////////////////////////

        request.then(function(res) {
            $timeout(function() {
                //Remove all that match
                controller.pinned = _.reject(controller.pinned, function(e) {
                    return (e._id == item._id) || (e == item._id);
                });

                item.pinning = false;

                Notifications.status('\'' + item.title + '\' was unpinned');
                // NotificationService.indicate('Unpinned from home', 'ti ti-close');
            });

            

        }, function() {
            Notifications.warning('Failed to unpin \'' + item.title + '\'')
            item.pinning = false;
        })

        return request;
    }

    ///////////////////////////////////////////

    controller.toggle = function(item) {

        var deferred = $q.defer();

        if (item.pinning) {
            deferred.reject();
            return deferred;
        }

        if (controller.isPinned(item)) {
            console.log('UNPIN')
            return controller.unpin(item);
        } else {
            console.log('PIN')
            return controller.pin(item);
        }


        return deferred.promise;
        // item.pinning = true;

        // var request = FluroContent.endpoint('stat/' + item._id + '/pinned').update({}).$promise;

        // request.then(function() {
        //     item.pinning = false;
        //     item.pinned = true;

        //     controller.refreshPins();
        // }, function() {
        //     item.pinning = false;
        // })

        // return request;
    }

    ///////////////////////////////////////////

    controller.refreshPins = function() {

        var deferred = $q.defer();

        $http.post(Fluro.apiURL + '/stat/my/pinned?unique=true', {})
            .then(function(res) {
                $timeout(function() {
                    controller.pinned = res.data;
                    return deferred.resolve(controller.pinned);
                });
            }, function(err) {

                return deferred.resolve(controller.pinned);
            });


        return deferred.promise;
    }



    ///////////////////////////////////////////

    //Refresh the pins for good measure
    controller.refreshPins();

    ///////////////////////////////////////////

    return controller;
})