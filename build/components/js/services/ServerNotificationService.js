var contactIDs = [];

/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////

app.service('ServerNotificationService', function($rootScope, $interval, FluroSocket, $timeout, StorageService, FluroContent) {


    console.log('Listening for NOTIFICATION')



    $rootScope.$watch('user.contacts', function(newContactIDs) {

        if (contactIDs && contactIDs.length) {
            _.each(contactIDs, function(id) {

                FluroSocket.leave(id);
            })
        }

        ////////////////////////////////////////

        contactIDs = newContactIDs

        ////////////////////////////////////////

        if (contactIDs && contactIDs.length) {
            _.each(contactIDs, function(id) {
                console.log('Subscribe contact', id);
                FluroSocket.join(id);
            })
        }

        service.reload();
    })






    /////////////////////////////////////////////
    /////////////////////////////////////////////


    // Initialize Firebase
    // TODO: Replace with your project's customized code snippet
    // Initialize Firebase

    // const messaging;

    // if(window.firebase) {
    //     messaging = firebase.messaging();

    //     messaging.onMessage(function(payload) {
    //       console.log('Message received. ', payload);
    //       // ...
    //     });
    // }




    /////////////////////////////////////////////

    var service = new StorageService('my/notifications?limit=120', 'notifications.refreshed');

    /////////////////////////////////////////////


    /////////////////////////////////////////////

    FluroSocket.on('notification', function(rawPacket) {

        var notification = rawPacket.notification;
        
        console.log('NOTIFICATION RECEIVED', notification);
        if (!service.items) {
            service.collect();
        } else {

            var match = _.find(service.items, { _id: notification._id });
            if (!match) {
                console.log('unshift in')
                service.items.unshift(notification);
                $rootScope.$broadcast('notifications.refreshed');
            }


        }
    })


    /////////////////////////////////////////////

    var inflightRequest;

    service.collect = function() {

        if (inflightRequest) {
            console.log('Already collecting')
            return inflightRequest;
        }

        //Tell the server we are collecting notifications
        inflightRequest = FluroContent.endpoint('my/notifications/collect', true, true).get().$promise;

        //Once thats done update all of the items in the list and mark them as collected
        inflightRequest.then(function() {
            _.each(service.items, function(notification) {
                notification.collected = true;
            });

            $timeout(function() {
                $rootScope.$broadcast('notifications.collected');
            })
            inflightRequest = null;
        }, function(err) {
            inflightRequest = null;
        });

        return inflightRequest;
    }

    /////////////////////////////////////////////

    service.collectChat = function(id) {

        //Tell the server we are collecting notifications
        var promise = FluroContent.endpoint('my/notifications/read/' + id, true, true)
            .get()
            .$promise;

        //Once thats done update all of the items in the list and mark them as collected
        promise.then(function() {

            //////////////////////////////

            _.chain(service.items)
                .filter(function(notification) {
                    var matchType = notification.type == 'chat';
                    var matchConversation = _.includes(notification.references, id);

                    return (matchType && matchConversation);

                })
                .map(function(notification) {
                    notification.read = true;
                    notification.collected = true;
                    return notification;
                })
                .value();

            //////////////////////////////

            $timeout(function() {
                $rootScope.$broadcast('notifications.collected');
            });
        });

        return promise;


    }

    /////////////////////////////////////////////
    /////////////////////////////////////////////

    /**
    //Add a notification to the list
    service.addNotification = function(notification) {

        // console.log('Add to list');
        var contentID = _.get(notification, 'additionalData.content');


        //Refresh information if a notification is
        //an alert about content being added or removed
        switch (notification.type) {
            case 'assignment':
                //Reload the user's assignments
                AssignmentService.reload();
                break;
            case 'process':
                //Reload the user's assignments
                ProcessService.reload();
                break;
            case 'chat':


                // var chat = _.get(notification, 'additionalData.chat');
                $rootScope.$broadcast('conversation.chat', {
                    conversation: contentID
                });

                // $rootScope.$broadcast('chat.message', {conversation:contentID, chat:chat});
                break;
        }

        /////////////////////////////////////////////

        if (!service.items) {
            service.items = []
        }

        /////////////////////////////////////////////

        var alreadyListed = _.some(service.items, {
            _id: notification._id
        });

        if (!alreadyListed) {
            service.items.push(notification);
            //Tell all active controllers that a new notification came in
            // $timeout(function() {
            $rootScope.$broadcast('notifications.refreshed', notification);
            // })
        }
    }
/**/
    /////////////////////////////////////////////

    service.getUncollected = function() {
        return _.filter(service.items, function(notification) {
            return !notification.collected
        }).length;
    }

    /////////////////////////////////////////////

    service.getUnread = function() {
        return _.filter(service.items, function(notification) {
            return !notification.read
        }).length;
    }

    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////

    /**
    //Handle the actions to take for the notification
    service.handleNotification = function(notification) {


        notification.collected = true;

        //Add it to the list if it isn't already
        service.addNotification(notification);

        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////

        //If the notification hasn't been read yet
        if (!notification.read) {

            //Mark as read
            notification.read = true;


            //And tell the server it has been read
            FluroContent.endpoint('notifications/read/' + notification._id).get().$promise.then(function(res) {
                console.log('Read');
                $rootScope.$broadcast('notifications.refreshed', notification);
            }, function(err) {
                console.log('Error reading notification', err);
            })
        }

        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////

        //First try and close the in app browser
        //In case the user had it opened when we handle the notification
        try {
            //Close in app browser
            BrowserService.close();
        } catch (e) {}

        ////////////////////////////////////////

        //Check to see if we specified a content item
        var currentState = $state.current;
        var contentID = _.get(notification, 'additionalData.content');

        // console.log('CONTENT ID', contentID)
        ////////////////////////////////////////

        //Decide where to redirect based on what kind of notification 
        //was triggered
        switch (notification.type) {
            case 'assignment':
                //Go to the home page
                return $state.go('app.assignments');
                break;
                // case 'process':

                //     return $state.go('app.content', {
                //         id: contentID
                //     });
                //     break;
            case 'chat':

                // if($stateParams.id == contentID) {

                // } else {
                // console.log('GO TO FEED BABY', $state.current, notification.type, contentID);
                return $state.go('feed', {
                    id: contentID
                }, {
                    reload: true
                });
                // }



                break;
        }

        ////////////////////////////////////////

        //If a content id was specified 
        if (contentID) {
            //Redirect to the content
            return $state.go('app.content', {
                id: contentID
            });
        }
    }

    /**/

    /////////////////////////////////////////////

    service.reload();


    $interval(function() {
        service.reload();
    }, 20000)

    /////////////////////////////////////////////

    return service;
})