app.service('ContentFieldService', function() {


    var controller = {};



    var hideArchivedFor = [
        'contact',
        'contactdetail',
        'family',
        // 'event',
        'team',
        'article',
    ]

    //////////////////////////////////////

    var basicFields = [
        'title',
        'updatedBy',
        'updated',
        'created',
        'timezone',
        'name',
        'definition',
        'description',
        'startDate',
        'endDate',
        'firstLine',
        'status',
        'tags',
        'realms',
        'assetType',
        'external',
        '_type',
        'mainImage',
        'stats',
        'headcount',
        'tickets',
        'account'
    ];

    //////////////////////////////////////

    controller.filterDebounce = 0;//5;

    //////////////////////////////////////

    controller.getFields = function(queryConfig, type, definition, $stateParams) {

        if(!$stateParams) {
            $stateParams = {};
        }


        console.log('GET FIELDS', queryConfig, type, definition)
        /////////////////////////////////////////////////


        if ($stateParams.searchInheritable) {
            queryConfig.searchInheritable = true;
        }

        if(_.includes(hideArchivedFor, type.path)) {
            if (!$stateParams.searchArchived) {
                // queryConfig.searchArchived = true;
            // } else {
                // console.log('EXCLUDE ARCHIVED')
                queryConfig.excludeArchived = true;
            } else {
                // console.log('NOT SEARCHING ARCHIVED')
            }
        } else {
            // console.log('NOT INCLUDED');
        }


        if(type.path == 'event') {
            if($stateParams.startDate) {
                queryConfig.startDate = $stateParams.startDate;
                // console.log('SET START DATE');
            } else {
                var pastBuffer =new Date();
                pastBuffer.setTime(pastBuffer.getTime() - (86400 * 1));
                queryConfig.startDate = pastBuffer;
            }
        }

        ////////////////////////////////////////////////////////

        // console.log('tEST', $stateParams.searchArchived, type.path)


        ////////////////////////////////////////////////////////

        //Get all the fields usually set for this type
        var columnFields = _.map(type.columns, function(col) {
            return col.key;
        })

        ////////////////////////////////////////////////////////

        //If a definition has set to disable the basic columns
        //then just remove all the usual columns
        if (definition && definition.disableColumns) {
            columnFields = [];
        }

        ////////////////////////////////////////////////////////

        if(_.get(definition, 'definitionName') == 'report') {
            queryConfig.searchInheritable = true;
        }

        ////////////////////////////////////////////////////////

        //Make sure we load all the fields that we want to filter by
        var filterFields = _.map(type.filters, function(col) {
            return col.key;
        })

        ////////////////////////////////////////////////////////

        //Combine all fields
        var fields = [];
        fields = fields.concat(columnFields)
        fields = fields.concat(filterFields);
        fields = fields.concat(basicFields);

        //////////////////////////////////////////////////////

        if (definition) {
            if(definition.columns && definition.columns.length) {
                var customFields = _.map(definition.columns, function(col) {
                    return col.key;
                })
                fields = fields.concat(customFields);
            }

            //////////////////////////////////////////////////////

            if (definition.filters && definition.filters.length) {
                var definedFilterFields = _.map(definition.filters, function(col) {
                    return col.key;
                })
                fields = fields.concat(definedFilterFields);
            }
        }


        /////////////////////////////////////////////////

        switch (type.path) {
            
            case 'event':
                fields.push('forms')
                fields.push('plans')
                fields.push('assignments')
                fields.push('volunteers')
                fields.push('locations')
                fields.push('rooms')
                fields.push('track')
                queryConfig.appendAssignments = true;
                break;
            case 'account':
                fields.push('prefix')
                fields.push('data')
                fields.push('primaryContacts')
                break;
            case 'policy':
                fields.push('inviteCode');
                fields.push('inviteCodeEnabled');
                break;
            case 'contactdetail':
                fields.push('contact');

                // if(!queryConfig.searchArchived) {
                    
                // }

            break;
            case 'definition':
                fields.push('parentType');
                fields.push('privacy');
            break;
            case 'contact':
                fields.push('age')
                fields.push('dob');
                fields.push('distinctFrom');
                fields.push('emails')
                fields.push('phoneNumbers')
                fields.push('capabilities')
                fields.push('locations')
                _.pull(fields, 'updatedBy')
                queryConfig.appendTeams = 'all';

                // queryConfig.status = ['active', '']
                // queryConfig.appendContactDetail = 'all';
                break;
            case 'checkin':
                fields.push('event');
                break;
            case 'attendance':
                fields.push('event');
                break;
            case 'post':
                fields.push('parent');
                fields.push('managedAuthor');
                break;
            case 'realm':
                fields.push('trail');
                fields.push('bgColor');
                fields.push('color');
                break;
            case 'team':
                fields.push('memberCount');
            break;
            case 'purchase':
                fields.push('expires');
                fields.push('renew');
                break;
            case 'family':
                fields.push('address');
                fields.push('phoneNumbers');
                fields.push('emails');
                fields.push('postalAddress');
                fields.push('distinctFrom');
                break;
            case 'realm':
                fields.push('color')
                fields.push('bgColor')
                fields.push('trail')
                fields.push('inheritable');
                break;
            case 'image':
                fields.push('width')
                fields.push('height')
                break;
            case 'audio':
                fields.push('assetType')
                break;
            case 'video':
                fields.push('assetType');
                fields.push('width');
                fields.push('height');
                break;
            case 'location':
                fields.push('latitude')
                fields.push('longitude')
                break;
            case 'plan':
                fields.push('event')
                    // queryConfig.appendAssignments = true;
                break;
            case 'interaction':
                fields.push('data._firstName')
                fields.push('data._lastName')
                fields.push('amountDue')
                fields.push('outstanding')
                fields.push('amount')
                break;
            case 'transaction':
                fields.push('transactionData')
                queryConfig.excludeArchived = false;
                break;
            case 'persona':
                fields.push('username')
                fields.push('user')
                fields.push('userEmail')
                fields.push('collectionEmail')
                break;
            case 'process':
                fields.push('contacts')
                fields.push('state')
                fields.push('item')
                fields.push('assignedTo')
                fields.push('assignedToTeams')
                fields.push('dueDate')
                fields.push('taskCount')
                fields.push('processStatus')
                fields.push('results')
                    // fields.push('optionalTasks')
                break;
            case 'mailout':
                fields.push('subject')
                break;
        }

        /////////////////////////////////////

        // fields.push('account');
        fields.push('author');
        fields.push('owners');
        fields.push('managedAuthor');
        fields.push('managedOwners');
        fields.push('hashtags');
        fields.push('keywords');

        /////////////////////////////////////

        queryConfig.fields = _.uniq(fields);

        if(definition && definition.deepPopulate && definition.deepPopulate.length) {
            console.log('Deep population is specified', definition);
            // queryConfig.fields = null;
        }

        if(!definition) {
	        switch (type.path) {
	            // case 'process':
                case 'team':
                case 'event':
                case 'contact':
                case 'location':
                case 'tag':
                case 'realm':
                case 'post':
                case 'capability':
	            case 'ticketpurchase':
	            case 'mailout':
	            case 'contactdetail':
	            case 'interaction':
                case 'roster':
                case 'process':
                case 'asset':
                case 'audio':
                case 'video':
                case 'product':
	                queryConfig.allDefinitions = true;
	                break;
	        }
	    }



    }

    //////////////////////////////////////
    //////////////////////////////////////
    //////////////////////////////////////

    return controller;
})