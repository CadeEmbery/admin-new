app.service('ModalService', function($uibModal, $rootScope, $state, Fluro, $http, FluroContent, FluroAccess, TypeService) {

    var controller = {}


    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////


    controller.actions = function(options, title, description) {

        console.log('ACTIONS', options);

        //Create the modal
        var modalInstance = $uibModal.open({
            templateUrl: 'fluro-actions-modal/modal.html',
            controller: 'FluroActionsModalController',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                title: function() {
                    return title;
                },
                description: function() {
                    return description;
                },
                options: function() {
                    return options;
                }
            }
        });

        return modalInstance.result;
    }


    /////////////////////////////////////////////////////////////////////




    controller.promptLogin = function() {

        //Ensure we only popup one modal
        if (controller.loginModal) {
            return controller.loginModal.result;
        }

        //Create the modal
        var modalInstance = $uibModal.open({
            templateUrl: 'admin-login-modal/admin-login-modal.html',
            controller: 'TimeoutLoginController',
            size: 'sm',
            backdrop: 'static',
            resolve: {}
        });

        ///////////////////////////////

        controller.loginModal = modalInstance;

        ///////////////////////////////

        var promise = modalInstance.result;

        ///////////////////////////////

        //Listen for a result for the modal
        promise.then(removeInstance)

        //Cleanup the instance
        function removeInstance() {
            controller.loginModal = null;
        }


        ///////////////////////////////

        return promise;

    }


    /////////////////////////////////////////////////////////////////////

    controller.selectPositions = function(teams, successCallback, cancelCallback) {

        // return successCallback();


        var modalInstance;



        ///////////////////////////////

        var teamIDs = _.chain(teams)
            .compact()
            .map(function(team) {
                if (team._id) {
                    return team._id;
                } else {
                    return team;
                }
            })
            .uniq()
            .value();

        ///////////////////////////////
        modalInstance = $uibModal.open({
            templateUrl: 'fluro-positions-modal/positions-modal.html',
            controller: 'FluroPositionsModalController',
            size: 'md',
            backdrop: 'static',
            resolve: {

                positions: function() {
                    return $http.post(Fluro.apiURL + '/teams/positions', {
                        teams: teamIDs,
                    });
                },
                teams: function() {
                    return teamIDs
                },
            }
        });

        var promise = modalInstance.result;
        promise.then(successCallback, cancelCallback);

    }


    /////////////////////////////////////////////////////////////////////

    controller.assignPosition = function(teams, successCallback, cancelCallback) {

        ///////////////////////////////

        var modalInstance;

        ///////////////////////////////

        function closeModal() {
            if (modalInstance) {
                modalInstance.close({});
            }
        }

        ///////////////////////////////
        modalInstance = $uibModal.open({
            templateUrl: 'fluro-assign-modal/assign-modal.html',
            controller: 'FluroAssignModalController',
            size: 'md',
            backdrop: 'static',
            resolve: {
                teams: function() {
                    return teams
                },
                cancel: function() {
                    return closeModal;
                }
            }
        });

        var promise = modalInstance.result;
        promise.then(successCallback, cancelCallback);
    }


    /////////////////////////////////////////////////////////////////////

    controller.react = function(ids, successCallback, cancelCallback) {

        ///////////////////////////////

        var modalInstance;

        ///////////////////////////////

        function closeModal() {
            if (modalInstance) {
                modalInstance.close({});
            }
        }

        ///////////////////////////////
        modalInstance = $uibModal.open({
            templateUrl: 'fluro-react-modal/react.html',
            controller: 'FluroReactModalController',
            size: 'md',
            backdrop: 'static',
            resolve: {
                reactions: function($q, FluroContent) {
                    return FluroContent.resource('reaction')
                        .query()
                        .$promise;
                },
                ids: function() {
                    return ids
                },
                cancel: function() {
                    return closeModal;
                }
            }
        });

        var promise = modalInstance.result;
        promise.then(successCallback, cancelCallback);
    }

    /////////////////////////////////////////////////////////////////////

    controller.sms = function(ids, successCallback, cancelCallback) {

        ///////////////////////////////

        var modalInstance = $uibModal.open({
            templateUrl: 'fluro-message-center/sms.html',
            controller: 'FluroSMSController',
            size: 'md',
            backdrop: 'static',
            resolve: {
                contacts: function($q) {

                    var deferred = $q.defer();

                    console.log('GET SMS CONTACTS', ids)
                    $http.post(Fluro.apiURL + '/sms/contacts', {
                        ids: ids,
                    }).then(function(res) {
                        return deferred.resolve(res.data);
                    }, deferred.reject);


                    return deferred.promise;
                }
            }
        });

        var promise = modalInstance.result;
        promise.then(successCallback, cancelCallback);
    }


    /////////////////////////////////////////////////////////////////////

    controller.email = function(ids, successCallback, cancelCallback) {

        ///////////////////////////////

        var modalInstance = $uibModal.open({
            templateUrl: 'fluro-message-center/email.html',
            controller: 'FluroEmailController',
            size: 'md',
            backdrop: 'static',
            resolve: {
                contacts: function($q) {

                    var deferred = $q.defer();

                    // console.log('GET SMS CONTACTS')b
                    $http.post(Fluro.apiURL + '/email/contacts', {
                        ids: ids,
                    }).then(function(res) {
                        return deferred.resolve(res.data);
                    }, deferred.reject);


                    return deferred.promise;
                }
            }
        });

        var promise = modalInstance.result;
        promise.then(successCallback, cancelCallback);
    }




    /**
    controller.shareForm = function(form) {

        var formID = form;
        if(formID._id) {
            formID = formID._id;
        }

        var url = 'https://forms.fluro.io/form/' + formID;


         $scope.service = ShareModalService;
            console.log('Service', $scope.service)

            $scope.processing = true;



            var longURL = $location.absUrl();

            FluroContent.endpoint('url/shorten').save({url:longURL}).$promise.then(function(res) {

                $scope.processing = false;

                $scope.url = 'http://' + res.url;

            })

            $scope.close = function() {

                angular.element('#clipper').remove();
                ShareModalService.remove()
            }

            $scope.copyUrl = function() {
                console.log('starting to copy');
                clipboard($scope.url);

                

                ShareModalService.copied = true;
               
                
            }

    }
    /**/


    /////////////////////////////////////////////////////////////////////

    controller.mailout = function(template, successCallback, cancelCallback) {

        if (!successCallback) {
            successCallback = function() {}
        }

        if (!cancelCallback) {
            cancelCallback = function() {}
        }
        //////////////////////////////////////////////////////////

        //Check if the user can create any mailouts
        var createableMailouts = _.chain(TypeService.definedTypes)
            .filter(function(mailoutType) {
                var correctParentType = mailoutType.parentType == 'mailout';
                var canCreate = FluroAccess.can('create', mailoutType.definitionName, 'mailout');


                var systemOnly = mailoutType.systemOnly;
                if (systemOnly) {
                    console.log('FILTER OUT', mailoutType)
                    return false;
                }

                return (correctParentType && canCreate);
            })
            .value();


        //If only one template exists then just skip straight to creating the mailout
        if (createableMailouts.length == 1) {
            return controller.create(createableMailouts[0].definitionName, {
                template: template
            });
        }

        //////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////

        var modalInstance = $uibModal.open({
            templateUrl: 'fluro-message-center/mailout.html',
            controller: 'FluroMailoutController',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                template: function() {
                    return template
                }
            }
        });

        var promise = modalInstance.result;
        promise.then(successCallback, cancelCallback);

        return promise;
    }

    /////////////////////////////////////////////////////////////////////

    // controller.message = function(ids, successCallback, cancelCallback) {

    //     ///////////////////////////////

    //     var modalInstance = $uibModal.open({
    //         templateUrl: 'fluro-message-center/main.html',
    //         controller: 'FluroMessageCenterController',
    //         size: 'lg',
    //         backdrop: 'static',
    //         resolve: {
    //             contacts: function($q) {
    //                 return FluroContent.endpoint('message/contacts').query({
    //                     ids: ids,
    //                 }).$promise;
    //             }
    //         }
    //     });

    //     modalInstance.result.then(successCallback, cancelCallback);
    // }



    // /////////////////////////////////////////////////////////////////////

    // controller.mailout = function(ids, successCallback, cancelCallback) {

    //     ///////////////////////////////

    //     var modalInstance = $uibModal.open({
    //         templateUrl: 'fluro-mailout-center/main.html',
    //         controller: 'FluroMailoutCenterController',
    //         size: 'lg',
    //         backdrop: 'static',
    //         resolve: {
    //             contacts: function($q) {
    //                 return FluroContent.endpoint('message/contacts').query({
    //                     ids: ids,
    //                 }).$promise;
    //             }
    //         }
    //     });

    //     modalInstance.result.then(successCallback, cancelCallback);
    // }

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////


    controller.createFamily = function() {

        var dialogSize = 'lg';

        ///////////////////////////////

        var modalInstance = $uibModal.open({
            templateUrl: 'modals/family/family-modal.html',
            controller: 'FamilyModalController',
            size: dialogSize,
            backdrop: 'static',
            resolve: {}
        })


        return modalInstance.result;

    }

    /////////////////////////////////////////////////////////////////////

    controller.createType = function(type, definitionName) {

        console.log('Create Type', type.path, definitionName);

        if (definitionName && definitionName.length) {
            return $state.go(type.path + '.create', { definitionName: definitionName });
        }


        //////////////////////////////////////////////////////////

        //Check if the user can create any mailouts
        var createable = _.chain(TypeService.definedTypes)
            .filter(function(definition) {
                var correctParentType = definition.parentType == type.path;
                var canCreate = FluroAccess.can('create', definition.definitionName, type.path);
                return (correctParentType && canCreate);
            })
            .value();

        //There are no sub types
        if (!createable || !createable.length) {
            return $state.go(type.path + '.create', { definitionName: definitionName });
        }

        ///////////////////////////

        var types = [];

        switch (type.path) {
            case 'mailout':
            case 'post':
            case 'interaction':
            case 'contactdetail':
            case 'process':
            case 'product':
                break
            default:
                types.push({
                    title: 'Basic ' + type.singular,
                    plural: type.plural,
                    _type: type.path,
                })
                break;
        }


        //Add the sub types
        types = types.concat(_.map(createable, function(subType) {
            return {
                title: subType.title,
                plural: subType.plural,
                _type: type.path,
                definitionName: subType.definitionName,
            }
        }))

        ///////////////////////////

        ///Popup a modal for us to select the content type
        var modalInstance = $uibModal.open({
            templateUrl: 'fluro-admin-content/modal/subtype-modal.html',
            size: 'sm',
            controller: function($scope) {
                $scope.mainType = type;
                $scope.types = types;

                $scope.select = function(selectedType) {
                    console.log('Close')
                    $state.go(type.path + '.create', { definitionName: selectedType.definitionName });
                    // console.log('MODAL', $uibModal);
                    modalInstance.close();
                }


            }
        });

        return modalInstance.result;

        // ui-sref="{{type.path}}.create({definitionName:definition.definitionName})"
    }

    /////////////////////////////////////////////////////////////////////

    controller.create = function(typeName, params, successCallback, cancelCallback) {

        //console.log('Create In Modal', params);
        //////////////////////*/

        if (!params) {
            params = {};
        }

        var type = TypeService.getTypeFromPath(typeName);


        if (type.parentType) {

            //Resource
            FluroContent.endpoint('defined/' + type.path).get({}, createModal);

        } else {
            createModal();
        }

        function createModal(definition) {


            var dialogSize = 'lg';

            switch (type.path) {
                case 'realm':
                    dialogSize = 'md';
                    break;
            }



            ///////////////////////////////

            var modalInstance = $uibModal.open({
                templateUrl: 'fluro-admin-content/types/form.html',
                controller: 'ContentFormController',
                size: dialogSize,
                backdrop: 'static',
                resolve: {
                    //Get the definition
                    definition: function() {
                        return definition;
                    },
                    type: function() {
                        if (definition) {
                            return TypeService.getTypeFromPath(definition.parentType);
                        } else {
                            return type;
                        }
                    },
                    access: function(FluroAccess) {
                        var canCreate;
                        if (definition) {
                            canCreate = FluroAccess.can('create', definition.definitionName, definition.parentType);
                        } else {
                            canCreate = FluroAccess.can('create', type.path);
                        }

                        return FluroAccess.resolveIf(canCreate);
                    },
                    item: function($q) {
                        var deferred = $q.defer();

                        var newItem = {};

                        //Check if we received any starting data
                        if (params.template) {
                            newItem = angular.copy(params.template);
                        }

                        //console.log('Create new item', newItem);

                        if (definition) {
                            //If the definition exists
                            if (definition.definitionName) {
                                //Send back a new item with the definition set
                                newItem.definition = definition.definitionName;
                                deferred.resolve(newItem);
                            } else {
                                //Just don't resolve
                                deferred.reject();
                            }

                            //return deferred.promise;
                        } else {

                            deferred.resolve(newItem);
                            //return newItem;
                        }

                        return deferred.promise;
                    },
                    extras: function() {

                        if (!params || !params.extras) {
                            return {}
                        } else {
                            return params.extras;
                        }
                    }
                }
            });

            modalInstance.result.then(function(res) {
                if (successCallback) {
                    if (params.returnWithDefinition) {
                        return successCallback({ item: res, definition: definition })
                    } else {
                        return successCallback(res);
                    }
                }
            }, cancelCallback);
        }
    }



    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////


    controller.remove = function(itemObject, successCallback, cancelCallback) {


        var definedName = itemObject._type;
        if (itemObject.definition) {
            definedName = itemObject.definition;
        }

        var getQueryConfig = {
            id: itemObject._id,
        }

        //Load the items
        FluroContent.resource(definedName).get(getQueryConfig, function(item) {
            var type = TypeService.getTypeFromPath(item._type);

            if (item.definition) {
                FluroContent.endpoint('defined/' + item.definition).get({}, createModal);
            } else {
                createModal();
            }

            //////////////////////

            function createModal(definition) {

                var type = TypeService.getTypeFromPath(item._type);

                var modalInstance = $uibModal.open({
                    templateUrl: 'fluro-admin-content/types/delete.html',
                    controller: 'ContentDeleteController',
                    size: 'md',
                    backdrop: 'static',
                    resolve: {
                        type: function() {
                            return type;
                        },
                        item: function() {
                            return item;
                        },
                        definition: function() {
                            return definition;
                        },
                        deleteCheck: function($resource, Fluro) {
                            if (type.path == 'realm') {
                                return $resource(Fluro.apiURL + '/realm/check/' + item._id).get().$promise;
                            }
                            return null;
                        },
                        access: function(FluroAccess) {
                            var definitionName = type.path;
                            if (definition) {
                                definitionName = definition.definitionName;
                            }
                            var canDelete = FluroAccess.canDeleteItem(item, (definitionName == 'user'));
                            return FluroAccess.resolveIf(canDelete);
                        },
                    }
                });

                modalInstance.result.then(successCallback, cancelCallback);
            }
        });

    }

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////


    controller.view = function(itemObject, successCallback, cancelCallback) {


        var definedName = itemObject._type;
        if (itemObject.definition) {
            definedName = itemObject.definition;
        }


        var getQueryConfig = {
            id: itemObject._id,
            appendPostCount: 'all',
        }

        if (itemObject._type == 'contact') {
            getQueryConfig.appendTeams = 'all';
        }

        if (itemObject._type == 'event') {
            getQueryConfig.appendAssignments = true;
        }

        //Load the items
        FluroContent.resource(definedName).get(getQueryConfig, function(item) {
            var type = TypeService.getTypeFromPath(item._type);

            if (item.definition) {
                FluroContent.endpoint('defined/' + item.definition).get({}, createModal);
            } else {
                createModal();
            }




            //////////////////////

            function createModal(definition) {

                var dialogSize = 'lg';

                switch (item._type) {
                    case 'audio':
                    case 'video':
                    case 'image':
                    case 'policy':
                    case 'checkin':
                    case 'collection':
                    case 'contactdetail':

                    case 'post':
                    case 'product':
                    case 'persona':

                        dialogSize = 'md';
                        break;
                }




                var modalInstance = $uibModal.open({
                    templateUrl: 'fluro-admin-content/types/view.html',
                    controller: 'ContentViewController',
                    size: dialogSize,
                    backdrop: 'static',
                    resolve: {
                        type: function() {
                            return type;
                        },
                        item: function() {
                            return item;
                        },
                        definition: function() {
                            return definition;
                        },
                        access: function(FluroAccess) {
                            var definitionName = type.path;
                            if (definition) {
                                definitionName = definition.definitionName;
                            }
                            var canView = FluroAccess.canViewItem(item, (definitionName == 'user'));
                            return FluroAccess.resolveIf(canView);
                        },
                        extras: function(FluroContent, $q) {

                            var deferred = $q.defer();

                            //Data Object
                            var data = {};

                            switch (type.path) {

                                case 'process':

                                    // console.log('RETRIEVE THE PROCESS REFERENCE')
                                    if (item.item) {
                                        var referenceID = item.item;
                                        if (referenceID._id) {
                                            referenceID = referenceID._id;
                                        }

                                        FluroContent.resource('get').get({
                                                id: referenceID,
                                                appendDefinition: true,
                                            })
                                            .$promise
                                            .then(function(referencedItem) {
                                                data.reference = referencedItem;
                                                deferred.resolve(data);
                                            }, function(err) {
                                                //Just use the basics we do know
                                                deferred.resolve(data);
                                            });
                                    } else {
                                        deferred.resolve(data);
                                    }





                                    break;

                                    // case 'process':
                                    //     return FluroContent.endpoint('process/' + item._id + '/progress', null, true).query().$promise;
                                    //     break;
                                case 'query':

                                    //Run the query and return it
                                    FluroContent.endpoint('content/_query/' + item._id)
                                        .query()
                                        .$promise
                                        .then(function(res) {
                                            data.results = res;
                                            deferred.resolve(data);
                                        }, deferred.reject);

                                    break;
                                case 'mailout':

                                    if (item.state != 'sent') {


                                        //Do a preflight check on the mailout
                                        FluroContent.endpoint('mailout/' + item._id + '/preflight')
                                            .query()
                                            .$promise
                                            .then(function(res) {
                                                data.preflightContacts = res;
                                                deferred.resolve(data);
                                            }, deferred.reject);
                                    } else {
                                        deferred.resolve(data);
                                    }

                                    break;
                                case 'contact':

                                    console.log('Load extras');

                                    $q.all([
                                            function() {
                                                var deferred = $q.defer();
                                                FluroContent.endpoint('process/states/' + item._id, true, true).query().$promise.then(deferred.resolve, function(err) {
                                                    deferred.resolve([]);
                                                })

                                                return deferred.promise;
                                            }(),
                                            function() {
                                                var deferred = $q.defer();
                                                FluroContent.endpoint('contact/details/' + item._id, true, true).query().$promise.then(deferred.resolve, function(err) {
                                                    deferred.resolve([]);
                                                })

                                                return deferred.promise;
                                            }(),
                                            function() {
                                                var deferred = $q.defer();
                                                FluroContent.endpoint('contact/' + item._id + '/relationships', true, true).query().$promise.then(deferred.resolve, function(err) {
                                                    deferred.resolve([]);
                                                })

                                                return deferred.promise;
                                            }(),
                                        ])
                                        .then(function(values) {
                                            data.processStates = values[0];
                                            data.contactDetails = values[1];
                                            data.relationships = values[2];

                                            //Resolve the promise
                                            deferred.resolve(data);

                                        }, deferred.reject);

                                    break;

                                case 'plan':

                                    if (item.event) {

                                        var eventID = item.event;
                                        if (eventID._id) {
                                            eventID = eventID._id;
                                        }




                                        ////////////////////////////////////////////////

                                        //Get all assignments for an event
                                        var assignmentPromise = FluroContent.endpoint('event/' + eventID + '/assignments', false, true)
                                            .query()
                                            .$promise;

                                        assignmentPromise.then(function(res) {
                                            data.assignments = res;
                                        });

                                        ////////////////////////////////////////////////

                                        //Get all assignments for an event
                                        var rosterPromise = FluroContent.endpoint('event/' + eventID + '/rosters', false, true)
                                            .query()
                                            .$promise;

                                        rosterPromise.then(function(res) {
                                            data.rosters = res;
                                        });

                                        ////////////////////////////////////////////////

                                        $q.all([
                                                assignmentPromise,
                                                rosterPromise,
                                            ])
                                            .then(function() {
                                                deferred.resolve(data);
                                            }, deferred.reject);

                                    } else {
                                        deferred.resolve(data);
                                    }

                                    break;

                                case 'event':

                                    var id;
                                    if (item.event) {
                                        if (item.event._id) {
                                            id = item.event._id;
                                        } else {
                                            id = item.event;
                                        }
                                    } else {
                                        id = item._id;
                                    }

                                    if (id) {
                                        // FluroContent.endpoint('confirmations/event/' + id).query().$promise.then(function(res) {
                                        //     data.confirmations = res;
                                        //     //console.log('GOT CONFIRMATIONS', res);
                                        //     deferred.resolve(data);
                                        // });

                                        //Wait til all the promises have been fulfilled
                                        $q.all([
                                            function() {
                                                var deferred = $q.defer();
                                                FluroContent.endpoint('confirmations/event/' + id).query().$promise.then(deferred.resolve, function(err) {
                                                    deferred.resolve([])
                                                })
                                                return deferred.promise;
                                            }(),

                                            // function() {
                                            //     var deferred = $q.defer();
                                            //     FluroContent.endpoint('attendance/' + id).get().$promise.then(deferred.resolve, function(err) {
                                            //         deferred.resolve([])
                                            //     })
                                            //     return deferred.promise;
                                            // }(),

                                            function() {
                                                var deferred = $q.defer();
                                                FluroContent.endpoint('event/' + id + '/attendance').get().$promise.then(deferred.resolve, function(err) {
                                                    deferred.resolve([])
                                                })
                                                return deferred.promise;
                                            }(),


                                            function() {
                                                var deferred = $q.defer();
                                                FluroContent.endpoint('checkin/event/' + id).query({
                                                    all: true
                                                }).$promise.then(deferred.resolve, function(err) {
                                                    deferred.resolve([])
                                                })
                                                return deferred.promise;
                                            }(),

                                            function() {

                                                var deferred = $q.defer();
                                        // deferred.resolve({})
                                        // return deferred.promise;
                                        FluroContent.endpoint('event/' + id + '/guestlist').query({
                                            
                                        }).$promise.then(deferred.resolve, function(err) {
                                            deferred.resolve({})
                                        })
                                        return deferred.promise;
                                            }(),
                                        ]).then(function(values) {

                                            //Append the data
                                            data.confirmations = values[0];
                                            data.attendance = values[1];
                                            data.checkins = values[2];
                                            data.guests = values[3];

                                            //Resolve the promise
                                            deferred.resolve(data);
                                        }, deferred.reject);


                                    } else {
                                        //console.log('NO ID')
                                        deferred.resolve(data);
                                    }
                                    break;
                                default:
                                    deferred.resolve(data);
                                    break;
                            }

                            return deferred.promise;
                        }
                    }
                });

                modalInstance.result.then(successCallback, cancelCallback);
            }
        });
    }

    controller.edit = function(itemObject, successCallback, cancelCallback, createCopy, createFromTemplate, $scope) {


        var definedName = itemObject._type;
        if (itemObject.definition) {
            definedName = itemObject.definition;
        }

        ////////////////////////////////////////////////

        var getQueryConfig = {
            id: itemObject._id,

        }

        if (!createCopy) {
            getQueryConfig.context = 'edit';
        }

        if (itemObject._type == 'event') {
            getQueryConfig.appendAssignments = true;
            getQueryConfig.appendUnavailability = true;
        }

        switch (itemObject._type) {
            case 'product':
            case 'contactdetail':
            case 'roster':
                getQueryConfig.allDefinitions = true;
                break;
        }

        ////////////////////////////////////////////////

        //Load the items
        FluroContent.resource(definedName).get(getQueryConfig, function(item) {
            var type = TypeService.getTypeFromPath(item._type);

            // console.log('TYPES', type);

            if (item.definition) {
                FluroContent.endpoint('defined/' + item.definition).get({}, createModal);
            } else {
                createModal();
            }

            //////////////////////

            function createModal(definition) {


                var dialogSize = 'lg';

                switch (item._type) {
                    case 'process':
                        dialogSize = 'lg';
                        break;
                    case 'assignment':
                        dialogSize = 'md';
                        break;
                }




                var modalInstance = $uibModal.open({
                    templateUrl: 'fluro-admin-content/types/form.html',
                    controller: 'ContentFormController',
                    size: dialogSize,
                    backdrop: 'static',
                    resolve: {
                        type: function() {
                            return type;
                        },
                        item: function() {

                            var newItem = item;

                            if (createCopy || createFromTemplate) {
                                newItem = angular.copy(item);

                                ////////////////////////////////////////////


                                if (createFromTemplate) {
                                    console.log('Creating from template', createFromTemplate)
                                    //If there is a template provided then use that
                                    //and override existing fields
                                    _.assign(newItem, createFromTemplate)
                                }

                                ////////////////////////////////////////////

                                delete newItem._id;
                                delete newItem.slug;
                                delete newItem.author;
                                delete newItem.managedAuthor;
                                delete newItem.__v;
                                delete newItem.created;
                                delete newItem.updated;
                                delete newItem.updatedBy;
                                delete newItem.stats;
                                delete newItem.privateDetails;
                                delete newItem._external;




                                ////////////////////////////////////////////



                                var accountID = item.account;
                                var userAccountID = $rootScope.user.account;

                                if (accountID) {

                                    if (accountID._id) {
                                        accountID = accountID._id;
                                    }

                                    if (userAccountID && userAccountID._id) {
                                        userAccountID = userAccountID._id;
                                    }

                                    if (userAccountID != accountID) {
                                        newItem.realms = [];
                                    }
                                }

                                //Remove the account reference to avoid super user
                                //bug when customising inherited objects
                                delete newItem.account;

                                ////////////////////////////////////////////

                                //Delete API Key
                                delete newItem.apikey;

                                //Clear plan details
                                if (newItem._type == 'event') {
                                    newItem.plans = [];
                                    newItem.assignments = [];
                                }

                                if (newItem._type == 'event') {

                                    var now = new Date()
                                    var initDate = new Date(item.startDate);
                                    initDate.setMonth(now.getMonth(), now.getDate());

                                    // console.log('After set date', initDate);

                                    if ($scope) {
                                        if ($scope.stringBrowseDate) {
                                            initDate = $scope.stringBrowseDate();
                                        }

                                        if ($scope.item && $scope.item.startDate) {
                                            initDate = $scope.item.startDate;
                                            //console.log('Use start date of item we are creating for');
                                        }
                                    }

                                    //Get the length of the event
                                    var oldStartDate = new Date(item.startDate).getTime();
                                    var oldEndDate = new Date(item.endDate).getTime();
                                    var timeDiff = oldEndDate - oldStartDate;

                                    //Create the new dates
                                    var newStartDate = new Date(initDate);
                                    var newEndDate = new Date(newStartDate.getTime() + timeDiff);

                                    //Match the start dates
                                    newItem.startDate = newStartDate;
                                    newItem.endDate = newEndDate;
                                }

                                //Clear the start date of a new plan
                                if (newItem._type == 'plan') {
                                    newItem.startDate = null;
                                }


                                // //Link a new plan to an event if none provided
                                // if (newItem._type == 'plan') {
                                //     // if (newItem._type == 'plan' && !newItem.event) {
                                //     if ($scope.item && $scope.item._id) {
                                //         newItem.event = $scope.item;

                                //         // console.log('Link plan to', $scope.item);
                                //     }
                                // }


                                switch (newItem._type) {
                                    case 'persona':

                                        console.log('NEW ITEM SO CHECKIT');
                                        newItem.user = null;
                                        newItem.collectionEmail = '';
                                        newItem.username = '';
                                        newItem.firstName = '';
                                        newItem.lastName = '';
                                        break;
                                    case 'mailout':
                                        newItem.state = 'ready';
                                        delete newItem.publishDate;
                                        break;
                                }
                            }

                            ////////////////////////////////////////////////

                            //Create Copy
                            if (createCopy) {

                                if (item._type == 'mailout') {
                                    newItem.subject = newItem.title;

                                    console.log('NEW SUBJECT')
                                }

                                newItem.title = newItem.title + ' Copy';

                            }

                            ////////////////////////////////////////////////

                            if (createFromTemplate) {
                                //Update the status
                                newItem.status = 'active';
                            }

                            ////////////////////////////////////////////////

                            // console.log('NEW ITEM', newItem)
                            ////////////////////////////////////////////////



                            return newItem;
                        },
                        access: function(FluroAccess, $rootScope) {
                            var definitionName = type.path;
                            var parentType;

                            if (definition) {
                                definitionName = definition.definitionName;
                                parentType = definition.parentType;
                            }

                            //Allow the user to modify themselves
                            var author = false;

                            //If wanting to edit a user
                            if (type.path == 'user') {
                                author = ($rootScope.user._id == item._id);
                                if (author) {
                                    return true;
                                }
                            } else {
                                //Only allow if author of the content
                                if (_.isObject(item.author)) {
                                    author = (item.author._id == $rootScope.user._id);
                                } else {
                                    author = (item.author == $rootScope.user._id);
                                }
                            }

                            /////////////////////////////////////

                            //Check if we can edit
                            var canEdit = FluroAccess.canEditItem(item, (definitionName == 'user'));
                            var canView = FluroAccess.canViewItem(item, (definitionName == 'user'));
                            var canCreate = FluroAccess.can('create', definitionName, parentType);

                            // console.log('EDIT', canEdit, canCreate, $rootScope.user);

                            if (createCopy) {
                                // return FluroAccess.resolveIf(canCreate && canView);
                                return FluroAccess.resolveIf(canCreate);
                            } else {
                                return FluroAccess.resolveIf(canEdit);
                            }
                        },
                        definition: function() {
                            return definition;
                        },


                        extras: function(FluroContent, $q) {


                            var deferredExtras = $q.defer();

                            //Data Object
                            var data = {};


                            switch (type.path) {
                                case 'process':

                                    // console.log('RETRIEVE THE PROCESS REFERENCE')
                                    if (item.item) {
                                        var referenceID = item.item;
                                        if (referenceID._id) {
                                            referenceID = referenceID._id;
                                        }

                                        FluroContent.resource('get').get({
                                                id: referenceID,
                                                appendDefinition: true,
                                            })
                                            .$promise
                                            .then(function(referencedItem) {
                                                data.reference = referencedItem;
                                                deferredExtras.resolve(data);
                                            }, function(err) {
                                                //Just use the basics we do know
                                                deferredExtras.resolve(data);
                                            });
                                    } else {
                                        deferredExtras.resolve(data);
                                    }




                                    break;
                                case 'contact':

                                    /////////////////////////////////////////////////

                                    function loadContactDetails() {
                                        var deferred = $q.defer();

                                        FluroContent.endpoint('contact/details/' + item._id, true, true).query()
                                            .$promise
                                            .then(deferred.resolve, function() {
                                                deferred.resolve([]);
                                            });

                                        return deferred.promise;
                                    }

                                    /////////////////////////////////////////////////

                                    function loadContactDetailDefinitions() {
                                        var deferred = $q.defer();

                                        FluroContent.endpoint('defined/types/contactdetail', true, true).query()
                                            .$promise
                                            .then(deferred.resolve, function() {
                                                deferred.resolve([]);
                                            });

                                        return deferred.promise;
                                    }

                                    /////////////////////////////////////////////////

                                    function loadPersona() {
                                        var deferred = $q.defer();

                                        FluroContent.endpoint('contact/' + item._id + '/personas', true, true).query()
                                            .$promise.
                                        then(deferred.resolve, function() {
                                            deferred.resolve([]);
                                        });

                                        return deferred.promise;
                                    }

                                    /////////////////////////////////////////////////

                                    function loadUnsubscribes() {
                                        var deferred = $q.defer();

                                        FluroContent.endpoint('contact/' + item._id + '/unsubscribes', true, true).query()
                                            .$promise.
                                        then(deferred.resolve, function() {
                                            deferred.resolve([]);
                                        });

                                        return deferred.promise;
                                    }

                                    /////////////////////////////////////////////////

                                    function loadContactDevices() {
                                        var deferred = $q.defer();

                                        FluroContent.endpoint('contact/' + item._id + '/devices', true, true).query()
                                            .$promise.
                                        then(deferred.resolve, function() {
                                            deferred.resolve([]);
                                        });

                                        return deferred.promise;
                                    }

                                    /////////////////////////////////////////////////

                                    function loadContactNotifications() {
                                        var deferred = $q.defer();

                                        FluroContent.endpoint('contact/' + item._id + '/notifications', true, true).query()
                                            .$promise.
                                        then(deferred.resolve, function() {
                                            deferred.resolve([]);
                                        });

                                        return deferred.promise;
                                    }

                                    /////////////////////////////////////////////////

                                    $q.all([
                                        loadContactDetails(),

                                        loadPersona(),
                                        loadUnsubscribes(),
                                        loadContactDetailDefinitions(),

                                        loadContactDevices(),
                                        loadContactNotifications(),

                                        // FluroContent.endpoint('contact/details/' + item._id, true, true).query().$promise,
                                        // FluroContent.endpoint('contact/relationships/' + item._id, true, true).query().$promise,
                                    ]).then(function(values) {
                                        if (values[0]) {
                                            data.contactDetails = values[0];
                                        }

                                        if (values[1]) {
                                            data.personas = values[1];
                                        }

                                        if (values[2]) {
                                            data.unsubscribes = values[2];
                                        }

                                        if (values[3]) {
                                            data.detailDefinitions = values[3];
                                        }

                                        if (values[4]) {
                                            data.devices = values[4];
                                        }

                                        if (values[5]) {
                                            data.notifications = values[5];
                                        }

                                        //Resolve the promise
                                        deferredExtras.resolve(data);

                                    }, deferredExtras.reject);

                                    break;

                                default:
                                    deferredExtras.resolve(data);
                                    break;
                            }

                            return deferredExtras.promise;
                        }
                    }
                });

                modalInstance.result.then(successCallback, cancelCallback);
            }
        });
    }


    /////////////////////////////////////////////////////////////////////////

    controller.browse = function(type, modelSource, params) {

        if (!params) {
            params = {}
        }

        /////////////////////////////////////////

        //We need an object for our source
        if (!modelSource) {
            modelSource = {};
        }

        /////////////////////////////////////////

        //We need an array to append the items to
        if (!modelSource.items) {
            modelSource.items = [];
        }

        /////////////////////////////////////////

        //Launch the modal
        var modalDetails = {
            template: '<content-browser ng-model="model.items" ng-done="$dismiss" ng-type="type" params="params"></content-browser>',
            controller: function($scope) {
                $scope.type = type;
                $scope.model = modelSource;
                $scope.params = params;
            },
            size: 'lg',
        };
        /*
        if(params.scope) {
            //Isolate scope
            console.log('Isolate Scope');
            modalDetails.scope = params.scope;
        }
        */

        var modalInstance = $uibModal.open(modalDetails);

        return modalInstance;

        //modalInstance.result.then(successCallback, cancelCallback);

    }

    controller.batch = function(type, definition, ids, callback) {

        var basicTypeName = type.path;

        ///////////////////////////////////////////

        var modalInstance;

        ///////////////////////////////////////////

        //Load the basic model
        FluroContent.endpoint('defined/models/' + basicTypeName)
            .query()
            .$promise
            .then(function(basicFields) {

                //var $scope = $rootScope.$new();

                modalInstance = $uibModal.open({
                    template: '<batch-editor></batch-editor>',
                    backdrop: 'static',
                    controller: function($scope) {
                        $scope.type = type;
                        $scope.definition = definition;
                        $scope.baseFields = basicFields,
                            $scope.ids = ids;
                    },
                    size: 'lg',
                });


                modalInstance.result.then(callback);

            }, callback)

        ///////////////////////////////////////////





    }


    controller.export = function(type, definition, ids, callback) {

        // //Get the definition
        // var definitionName = _.get(definition, 'definitionName') || type.path;

        // //Load the basic model
        // FluroContent.endpoint('export/'+definitionName + '/keys')
        // .save()
        // .$promise
        // .then(function(basicFields) {

        //var $scope = $rootScope.$new();

        var modalInstance = $uibModal.open({
            template: '<batch-export></batch-export>',
            backdrop: 'static',
            controller: function($scope) {
                $scope.type = type;
                $scope.definition = definition;
                $scope.ids = ids;
            },
            size: 'lg',
        });


        // }, callback)


        //modalInstance.result.then(successCallback, cancelCallback);

    }

    ///////////////////////

    return controller;
});