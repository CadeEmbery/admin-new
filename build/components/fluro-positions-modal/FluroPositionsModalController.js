

app.controller('FluroPositionsModalController', function($scope, teams, positions, Fluro, $http) {


	$scope.search = {
		terms:'',
	};

	//////////////////////////////////////



	//////////////////////////////////////

	// // $scope.reactions = reactions;
	// $scope.teams = teams;


	// $scope.selected = {};
	// $scope.options = {};

	// //////////////////////////////////////

	// var ids = _.chain(teams)
	// .compact()
	// .map(function(team) {
	// 	if(team._id) {
	// 		return team._id
	// 	} else {
	// 		return team;
	// 	}
	// })
	// .value();

	// //////////////////////////////////////

	// $scope.assign = function() {
	// 	Batch.assignPosition({
	// 		ids:ids,
	// 		title:$scope.selected.title,
	// 		contacts:$scope.selected.contacts,
	// 		// options:$scope.options,
	// 	}, function() {

	// 		//Close the window
	// 		$scope.cancel();
	// 	})
	// }

	//////////////////////////////////////
	//////////////////////////////////////


	$scope.$watch('search.terms', function() {

		var filtered = positions.data;

		if($scope.search.terms && $scope.search.terms.length) {

			var lowercaseTerms = String($scope.search.terms).toLowerCase();

			filtered = _.filter(filtered, function(position) {

				var lowercaseTitle = String(position.title).toLowerCase();

				// console.log(lowercaseTitle, lowercaseTerms)
				return lowercaseTitle.indexOf(lowercaseTerms) >- 1;
			})
		}

		/////////////////////////////

		$scope.positions = filtered;



	});

	//////////////////////////////////////
	//////////////////////////////////////
	//////////////////////////////////////
	//////////////////////////////////////
	//////////////////////////////////////


	// $scope.cancel = cancel

	$scope.cancel = function() {
		console.log('CANCEL', $scope);
		$scope.$dismiss();
	}

	$scope.complete = function() {
		console.log('COMPLETE', $scope);

		var serverData = {
			teams:teams,
		};

		////////////////////////////////////

		if($scope.submission._all)  {
			//Load all contacts
			//(don't send any criteria)
			// console.log('ALL CONTACTS');
			return $scope.$close();


		} else {

			if($scope.submission._assignments) {
				serverData.includeAssignments = true;
			}

			if($scope.submission._members) {
				serverData.includeProvisional = true;
			}

			if($scope.submission.positions) {

				var positions = [];

				for(var key in $scope.submission.positions) {
					if($scope.submission.positions[key]) {
						positions.push(key);
					}
				}

				serverData.includePositions = positions;
				console.log('Selected positions', positions);
			}
		}

		////////////////////////////////////

		// console.log('SERVER DATA', serverData,  $scope.submission);

		////////////////////////////////////

		$http.post(Fluro.apiURL + '/teams/contacts', serverData).then(function(res) {

			// console.log('CONTACTS ARE', res.data);

			$scope.$close(res.data);
		}, function(err) {
			console.log('Error', err);
		})

		//We need to load the contacts
		// $scope.$close($scope.submission)
	}


	//////////////////////////////////////
	//////////////////////////////////////
	//////////////////////////////////////
	//////////////////////////////////////
	//////////////////////////////////////
	//////////////////////////////////////

	$scope.submission = {
		_all:true,
	};


	//////////////////////////////////////

	$scope.isEmpty = function() {
		return !_.keys($scope.submission).length;
	}

	$scope.setEmpty = function() {
		$scope.submission = {};
	}

	//////////////////////////////////////

	$scope.set = function(string) {
		$scope.submission[string] = !$scope.submission[string];
	}

	$scope.isSet = function(string, or1, or2) {

		if($scope.submission[string]) {
			return true;
		}

		if(or1 && $scope.submission[or1]) {
			return true;
		}
		

		if(or2 && $scope.submission[or2]) {
			return true;
		}
		
	}

	//////////////////////////////////////

	$scope.setAssignment = function(string) {
		
		var positionKey = "positions['" + string + "']";
		var currentValue = _.get($scope.submission, positionKey);
		_.set($scope.submission, positionKey, !currentValue);

		// console.log('SET ASSIGNMENT', positionKey, $scope.submission)
	}

	$scope.isAssignmentSet = function(string) {

		if($scope.submission._all) {
			return true;
		}

		if($scope.submission._assignments) {
			return true;
		}


		// if($scope.submission[string]) {
		// 	return true;
		// }

		var positionKey = "positions['" + string + "']";
		return _.get($scope.submission, positionKey);

		
		
	}
});
