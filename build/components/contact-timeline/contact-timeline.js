app.directive('contactTimeline', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            details: '=ngDetails'
        },
        templateUrl: 'contact-timeline/contact-timeline.html',
        controller: 'ContactTimelineController',
    };
});

app.controller('ContactTimelineController', function($scope, $q, ModalService, TypeService, FluroAccess, FluroContent, $filter) {

    $scope.assignmentSummary = function(assignments) {
        return _.map(assignments, function(ass) {
            return ass.title;
        }).join(', ');
    }

    $scope.status = {};

    $scope.stats = {
        upcoming: []
    };



    ////////////////////////////////////////////////////////

    var contactID = $scope.model._id;

    ////////////////////////////////////////////////////////

    var postTypes = _.chain(TypeService.getSubTypes('post'))
        .filter(function(post) {
            return FluroAccess.can('create', post.definitionName, 'post');

        })
        .sortBy(function(type) {
            return type.title;
        })
        .sortBy(function(type) {
            return type.weight || 0
        })
        .value();


    $scope.editPost = function(post) {
        ModalService.edit(post, function(result) {
            _.assign(post.data, result.data)
        })
    }

    $scope.deletePost = function(post) {
        ModalService.remove(post, function() {
            _.pull($scope.posts, post);
        })
    }

    $scope.createPost = function(definition) {

        var params = {
            template: {
                title: definition.title,
                parent: $scope.model,
            },
            returnWithDefinition: true,
        };

        ModalService.create(definition.definitionName, params, function(results) {
            var post = results.item;
            post.fullDefinition = results.definition;
            console.log('RES', post);
            $scope.posts.unshift(post);
        })
    }




    console.log('Wooot', postTypes)
    $scope.postTypes = _.filter(postTypes, function(def) {

        // console.log('POST TYPES', def);
        // if (!def.data || !def.data.postParentTypes || !def.data.postParentTypes.length) {
        //     return true;
        // }

        var parentRestrictions = _.get(def, 'data.postParentTypes');
        if (!parentRestrictions || !parentRestrictions.length) {
            return true;
        }


        console.log('POST TYPE', $scope.model._type, $scope.model.definition);
        return _.includes(parentRestrictions, $scope.model._type);
    });



    ///////////////////////////////////////////////

    $scope.formatRosterTimeline = function() {

        var assignments = $scope.assignments;

        if ($scope.search && $scope.search.terms) {

        }

        $scope.rosterTimeline = _.chain($scope.assignments)
            .sortBy(function(assignment) {
                return assignment.title
            })
            .reduce(function(set, assignment) {

                //Get the roster id

                var event = assignment.event;

                //Create or find the correct roster entry
                var existing = set[event._id];

                //If there isn't one
                if (!existing) {

                    var eventBits = _.pick(event, [
                        'title',
                        'startDate',
                        'endDate',
                        '_type',
                        'definition',
                        'realms',
                        '_id',
                        'firstLine',

                    ])

                    //Add bits
                    eventBits.rosters = [];
                    eventBits.assignments = [];

                    existing =
                        set[event._id] = eventBits;
                }

                //Combine the assignments
                assignment._type = 'assignment';
                existing.assignments.push(assignment);

                return set;
            }, {})
            .values()
            .sortBy(function(event) {
                return new Date(event.startDate) * -1;
            })
            .reduce(function(set, event) {


                //////////////////////////////

                var date = new Date(event.startDate);
                var dateTitle = date.format('J');
                var dateKey = date.format('j M Y');
                // console.log('DATE KEY', dateKey);

                //////////////////////////////

                var existing = set[dateKey];

                //////////////////////////////

                if (!existing) {

                    var day = {
                        title: dateTitle,
                        date: date,
                        events: [],
                    }

                    //Set the existing
                    existing =
                        set[dateKey] = day
                }

                // console.log('EVENT DATE KEY IS', dateKey, event.startDate);
                //Add the event
                existing.events.push(event);



                return set;
            }, {})
            .values()
            .sortBy(function(entry) {
                return new Date(entry.date) * -1;
            })
            .reduce(function(set, entry) {

                var date = new Date(entry.date);
                var dateTitle = date.format('M');
                var dateKey = date.format('M Y');

                var existing = set[dateKey];
                if (!existing) {
                    existing =
                        set[dateKey] = {
                            title: dateTitle,
                            date: date,
                            days: [],
                        }
                }

                existing.days.push(entry);

                return set;
            }, {})
            .values()
            .sortBy(function(entry) {
                return new Date(entry.date) * -1;
            })
            .reduce(function(set, entry) {

                var date = new Date(entry.date);
                var dateTitle = date.format('Y');
                var dateKey = date.format('Y');

                var existing = set[dateKey];
                if (!existing) {
                    existing =
                        set[dateKey] = {
                            title: dateTitle,
                            date: date,
                            months: [],
                        }
                }

                existing.months.push(entry);

                return set;
            }, {})
            .values()
            .sortBy(function(entry) {
                return new Date(entry.date) * -1;
            })
            .value();

    }

    ////////////////////////////////////////////////////////

    $scope.$watch('search.terms', function(searchTerms) {
        $scope.formatRosterTimeline();
    })
    ////////////////////////////////////////////////////////

    $q.all([
        function() {
            var deferred = $q.defer();

            //Checkins
            FluroContent.endpoint('info/checkins').query({
                contact: $scope.model._id
            }).$promise.then(deferred.resolve, function(err) {
                deferred.resolve([]);
            })

            return deferred.promise;
        }(),

        function() {
            var deferred = $q.defer();

            //Interactions
            FluroContent.endpoint('info/interactions').query({
                contact: $scope.model._id
            }).$promise.then(deferred.resolve, function(err) {
                deferred.resolve([]);
            })

            return deferred.promise;
        }(),



        function() {
            var deferred = $q.defer();

            //Assignments
            FluroContent.endpoint('info/newassignments').query({
                contact: $scope.model._id
            }).$promise.then(deferred.resolve, function(err) {
                deferred.resolve([]);
            })

            return deferred.promise;
        }(),

        function() {
            var deferred = $q.defer();

            //Posts
            FluroContent.endpoint('info/posts').query({
                contact: $scope.model._id
            }).$promise.then(deferred.resolve, function(err) {
                deferred.resolve([]);
            })

            return deferred.promise;
        }(),

        function() {
            var deferred = $q.defer();

            //Interactions
            FluroContent.endpoint('info/correspondence').query({
                contact: $scope.model._id
            }).$promise.then(deferred.resolve, function(err) {
                deferred.resolve([]);
            })

            return deferred.promise;
        }(),







        function() {
            var deferred = $q.defer();

            //Interactions
            FluroContent.endpoint('contact/' + contactID + '/sms').query().$promise.then(deferred.resolve, function(err) {
                deferred.resolve([]);
            })

            return deferred.promise;
        }(),


        function() {
            var deferred = $q.defer();

            //Interactions
            FluroContent.endpoint('info/transactions').query({
                contact: $scope.model._id
            }).$promise.then(deferred.resolve, function(err) {
                deferred.resolve([]);
            })

            return deferred.promise;
        }(),

        function() {
            var deferred = $q.defer();

            //Interactions
            FluroContent.endpoint('contact/' + contactID + '/email').query().$promise.then(deferred.resolve, function(err) {
                deferred.resolve([]);
            })

            return deferred.promise;
        }(),

    ]).then(function(values) {


        $scope.checkins = _.chain(values[0])
            .filter(function(checkin) {
                return _.get(checkin, 'event.startDate');
            })
            .sortBy(function(checkin) {
                return new Date(checkin.event.startDate);
            })
            .reverse()
            .value();

        $scope.interactions = values[1];
        $scope.assignments = values[2];
        $scope.posts = values[3];
        $scope.correspondences = values[4];
        $scope.smsMessages = values[5];
        $scope.transactions = values[6];
        $scope.emails = values[7];

        ///////////////////////////////////////////////

        var now = new Date();

        ///////////////////////////////////////////////

        $scope.messages = _.chain([].concat(values[4], values[5], values[7]))
            .sortBy(function(entry) {
                return entry.date;
            })
            .reverse()
            .compact()
            .value();

        ///////////////////////////////////////////////

        $scope.timeline = _.chain(values)
            // .filter(function(card) {
            //     return card.timeline.future;
            // })
            .map(function(group, key) {

                switch (key) {
                    case 0:
                        _.each(group, function(item) {

                            if (!item.event) {
                                return;
                            }

                            item.timeline = {
                                type: 'checkin',
                                date: new Date(item.event.startDate),
                            }


                            if (item.timeline.date > now) {
                                item.timeline.future = true;
                            }

                            // item.timeline.color = '#000';
                            // item.timeline.bgColor = '#000';


                        })
                        break;
                    case 1:
                        _.each(group, function(item) {
                            item.timeline = {
                                type: 'interaction',
                                date: new Date(item.created)
                            }


                            if (item.timeline.date > now) {
                                item.timeline.future = true;
                            }

                            // item.timeline.color = '#000';
                            // item.timeline.bgColor = '#000';
                        })
                        break;
                    case 2:
                        group = _.chain(group)
                            .reduce(function(results, assignment) {


                                // //console.log('ITEM', item)
                                if (!assignment.event) {
                                    return results;
                                }

                                ////////////////////////////////////////

                                var eventID = assignment.event;
                                if (eventID._id) {
                                    eventID = eventID._id;
                                }

                                //Find the event
                                var event = _.find(results, {
                                    _id: eventID
                                });

                                //If theres no event in the list already
                                if (!event) {

                                    //Date
                                    var date = new Date(assignment.event.startDate);

                                    //Create the existing event
                                    event = assignment.event;

                                    //Add assignments
                                    event.assignments = [];
                                    event.realms = [];


                                    //Add timeline object
                                    event.timeline = {
                                        type: 'assignment',
                                        title: event.title,
                                        date: date,
                                        future: (date > now),
                                    }

                                    //Push the event
                                    results.push(event);
                                }

                                ///////////////////////////////////////

                                //Append the realms
                                event.realms = _.uniq(event.realms.concat(assignment.realms), function(realm) {
                                    return realm._id;
                                });


                                event.assignments.push(assignment)

                                return results;

                            }, [])
                            .value();

                        // //console.log('Group', group);


                        break;
                    case 3:

                        // //console.log('GOTHCA', group, group.length)
                        _.each(group, function(item) {


                            item.timeline = {
                                type: 'post',
                                date: new Date(item.created)
                            }

                            if (item.timeline.date > now) {
                                item.timeline.future = true;
                            }



                            // //Realm
                            // var realm = item.realms[0];

                            // item.timeline.color = realm.color;
                            // item.timeline.bgColor = realm.bgColor;
                        })
                        break;
                    case 4:

                        // //console.log('GOTHCA', group, group.length)
                        _.each(group, function(item) {


                            item.timeline = {
                                type: 'correspondence',
                                date: new Date(item.date)
                            }

                            if (item.timeline.date > now) {
                                item.timeline.future = true;
                            }

                            if (item.mailout && item.mailout.realms) {
                                item.realms = item.mailout.realms;
                            }
                            // //Realm
                            // var realm = item.realms[0];

                            // item.timeline.color = realm.color;
                            // item.timeline.bgColor = realm.bgColor;
                        })
                        break;
                    case 5:

                        _.each(group, function(item) {


                            var name = _.get(item, 'managedAuthor.title');
                            if (!name || !name.length) {
                                name = _.get(item, 'author.name');
                            }

                            item.sentBy = name;

                            item.timeline = {
                                type: 'sms',
                                date: new Date(item.date),
                                message: item.message,
                            }

                            if (item.timeline.date > now) {
                                item.timeline.future = true;
                            }
                        })
                        break;
                    case 6:

                        _.each(group, function(item) {


                            item.timeline = {
                                type: 'transaction',
                                date: new Date(item.created),
                                message: item.title,
                            }

                            if (item.timeline.date > now) {
                                item.timeline.future = true;
                            }
                        })
                        break;
                    case 7:

                        _.each(group, function(item) {
                            item.timeline = {
                                type: 'email',
                                date: new Date(item.date),
                                body: item.body,
                                subject: item.subject,
                                toEmail: item.toEmail,
                                fromEmail: item.fromEmail,
                            }

                            if (item.timeline.date > now) {
                                item.timeline.future = true;
                            }
                        })
                        break;
                }

                return group;
            })


            .flatten()
            .compact()
            .filter(function(item) {
                return item.timeline;
            })
            // .filter(function(item) {
            //     return !item.timeline.future;
            // })

            .sortBy(function(card) {
                return card.timeline.date;
            })

            .reverse()
            .reduce(function(results, card) {

                //If its in the future then seperate it here
                if (card.timeline.future) {
                    $scope.stats.upcoming.push(card);
                    return results;
                }



                var cardDate = card.timeline.date;

                if (!$scope.stats.lastSeen) {
                    $scope.stats.lastSeen = cardDate;
                }

                /////////////////////////////////////////

                //Create the keys for the years and months
                var year = new Date(cardDate);
                year.setHours(0);
                year.setMinutes(0);
                year.setSeconds(0);
                year.setMilliseconds(0);

                var yearKey = year.format('Y')

                /////////////////////////////////////////

                var month = new Date(cardDate);
                month.setHours(0);
                month.setMinutes(0);
                month.setSeconds(0);
                month.setMilliseconds(0);

                var monthKey = month.format('M')

                /////////////////////////////////////////

                var day = new Date(cardDate);
                day.setHours(0);
                day.setMinutes(0);
                day.setSeconds(0);
                day.setMilliseconds(0);

                var dayKey = day.format('j')

                /////////////////////////////////////////

                var existingYear = _.find(results, {
                    key: yearKey
                });

                if (!existingYear) {
                    existingYear = {
                        date: year,
                        key: yearKey,
                        months: [],
                    }
                    results.push(existingYear);
                }

                /////////////////////////////////////////

                var existingMonth = _.find(existingYear.months, {
                    key: monthKey
                });

                if (!existingMonth) {
                    existingMonth = {
                        date: month,
                        key: monthKey,
                        days: []
                    };

                    existingYear.months.push(existingMonth);
                }

                /////////////////////////////////////////

                var existingDay = _.find(existingMonth.days, {
                    key: dayKey
                });

                if (!existingDay) {
                    existingDay = {
                        date: day,
                        key: dayKey,
                        items: []
                    };

                    existingMonth.days.push(existingDay);
                }

                //Add the card
                existingDay.items.push(card);

                // /results[yearKey].months[monthKey].items.push(card);

                return results;

            }, [])

            .value();

        /////////////////////////////////////////////////

        var total = _.chain($scope.timeline)
            .map(function(year) {

                return _.chain(year.months)
                    .map(function(m) {

                        var month = {};
                        month.items = _.chain(m.days)
                            .map(function(day) {
                                return day.items;
                            })
                            .flatten()
                            .value();

                        return month;
                    })
                    .value();
            })
            .flatten()
            .slice(0, 12)
            .sum(function(month) {
                return month.items.length
            })
            .value();

        /////////////////////////////////////////////////

        var frequency = 'monthly';

        var mean = Math.round(total / 12); //48);//12);

        switch (mean) {
            case 0:
                $scope.stats.average = 'less than once a month';
                break;
            case 1:
                $scope.stats.average = 'Monthly';
                break;
            case 2:
                $scope.stats.average = 'Fortnightly';
                break;
            default:
                $scope.stats.average = 'Weekly';
                break;
        }

        /////////////////////////////////////////////////

        // var grouped = _.groupBy(data, function(item) {
        //     return item.date;
        // });

        // var groupedByYear = _.groupBy(data, function(item) {
        //     return item.date.substring(0,4);
        // });

        // var groupedByMonth = _.groupBy(data, function(item) {
        //     return item.date.substring(0,7);
        // });


        $scope.formatRosterTimeline();


        // .groupBy(function(card) {
        //     return $filter('formatDate')(card.timeline.date, 'M Y')
        // })
        // .value();
        // 
        // 
        $scope.status.loaded = true;
    })



    ////////////////////////////////////////////////////////////////////////

    $scope.editSheet = function(sheet) {
        ModalService.edit(sheet, function(res) {
            _.assign(sheet, res);
        })
    }


    $scope.addSheet = function(definition) {
        var params = {
            template: {
                realms: $scope.model.realms,
                contact: $scope.model,
            }
        };

        ModalService.create(definition.definitionName, params, function(res) {

            // console.log('CREATED!', res);
            res.fullDefinition = definition;

            console.log('FULL DEFINITION IS', definition);

            $scope.details.push(res);

            $scope.updateCreatableSheets();
            $scope.updateViewableSheets();
        })
    }


    $scope.$watch('details', $scope.updateViewableSheets);

    $scope.updateViewableSheets = function() {
        $scope.viewableSheets = _.chain($scope.details)
            .sortBy(function(sheet) {
                return sheet.title;
            })
            .sortBy(function(sheet) {

                if (sheet.status == 'archived') {
                    return 999999999999999999;
                } else {
                    return 0;
                }
            })

            .value();

    }

    $scope.updateViewableSheets();


    $scope.updateCreatableSheets = function() {

        FluroContent.endpoint('defined/types/contactdetail', true, true).query()
            .$promise
            .then(function(contactDetailDefinitions) {


                $scope.createableSheets = _.filter(contactDetailDefinitions, function(def) {

                    var alreadyExist = _.some($scope.details, {
                        definition: def.definitionName
                    });
                    var canCreate = FluroAccess.can('create', def.definitionName, 'contactdetail');
                    return !alreadyExist && canCreate;
                })




            });





    }




    $scope.updateCreatableSheets();




});