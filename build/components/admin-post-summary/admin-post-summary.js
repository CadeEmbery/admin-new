app.directive('adminPostSummary', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-post-summary/admin-post-summary.html',
        controller: 'AdminPostSummaryController',
    };
});

app.controller('AdminPostSummaryController', function($scope, FluroAccess, $uibModalStack, $rootScope, $state, FluroContent, $q, TypeService) {


    var postTypes = TypeService.getSubTypes('post');

    console.log('POST TYPES', postTypes);


    // console.log('Wooot', postTypes)
    $scope.postTypes = _.filter(postTypes, function(def) {

        
        // if (!def.data || !def.data.postParentTypes || !def.data.postParentTypes.length) {
        //     return true;
        // }

        var parentRestrictions = _.get(def, 'data.postParentTypes');
        if(!parentRestrictions || !parentRestrictions.length) {
        return true; 
        }
        
        
        return _.includes(parentRestrictions, $scope.model._type);
    });




// console.log('TEST DATA', $scope.postTypes);

    $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item);
    }

    ///////////////////////////////////////////////


    $scope.canDelete = function(item) {
        return FluroAccess.canDeleteItem(item);
    }


    $scope.editPost = function(post) {
        $rootScope.modal.edit(post)
    }

    $scope.deletePost = function(post) {
        $state.go('post.delete', {
            id: post._id,
            definitionName: $scope.selected.definition.definitionName
        })
        $uibModalStack.dismissAll();
    }


    //Settings
    $scope.selected = {
        definition: $scope.postTypes[0],
    }


    $scope.getCount = function(definitionName) {

        if ($scope.model && $scope.model.posts) {
            var postItem = $scope.model.posts[definitionName];

            if (postItem) {
                return postItem.count;
            }
        }

        return '';
    }


    $scope.$watch(function() {
            return $scope.selected.definition;
        }, function(postType) {

            if (!postType) {
                $scope.posts = [];
                $scope.definition = null;
                return;
            }

            var hostID = $scope.model._id;
            var definitionName = postType.definitionName;

            //////////////////////////////////////////////

            //Get posts

            function getPosts() {

                var deferred = $q.defer();

                var promise = FluroContent.endpoint('post/' + hostID + '/' + definitionName)
                    .query({
                        additionalFields:[
                        'realms'
                        ]
                    })
                    .$promise
                    .then(deferred.resolve, function(err) {
                        deferred.resolve([]);
                    });

                return deferred.promise;
            }

            //////////////////////////////////////////////

            function getDefinition() {
                return FluroContent.endpoint('defined/' + definitionName)
                    .get()
                    .$promise;
            }

            //////////////////////////////////////////////

            $q.all([
                getPosts(),
                getDefinition(),
            ])

            .then(function(results) {

                $scope.posts = results[0].reverse();
                $scope.definition = results[1];


            })



        })
        /**
    
                    /**/
});