app.directive('relationshipSelect', function() {

    return {
        restrict: 'E',
        replace:true,
        scope: {
            model: '=ngModel',
            contact: '=ngContact',
            extras: '=ngExtras',
        },
        templateUrl: 'admin-relationship-select/admin-relationship-select.html',
        controller: 'RelationshipSelectController',
    };

});


app.controller('RelationshipSelectController', function($scope, FluroContent) {

    if (!$scope.model) {
        $scope.model = [];
    }

    $scope.settings = {};

    //Create a new object


    $scope.resetProposed = function() {
        $scope._new = {
            checkinAuthorised:true,
        }
    }


    $scope.resetProposed();

    ////////////////////////////////////////////////////

    $scope.remove = function(item) {
       _.pull($scope.model, item);
    }


    ////////////////////////////////////////////////////
    /**
    function updateDisplay() {


        var modelRelationships = $scope.model;
        var backlinkRelationships = $scope.backlinks;
        var combined = modelRelationships.concat(backlinkRelationships);

        $scope.display = _.chain(combined)
        .compact()
        .reduce(function(results, row) {

            var relationship = row.relationship.toLowerCase();
            var existingRelationship = _.find(results, function(r) {
                return r.relationship.toLowerCase() == relationship;
            });

            if(!existingRelationship) {
                existingRelationship = row;
                results.push(row)
            } else {
                 //Add all of the contacts in the row and then uniquify them
                existingRelationship.contacts = existingRelationship.contacts.concat(row.contacts);
                existingRelationship.contacts = _.uniq(existingRelationship.contacts, function(contact) {
                    return contact._id;
                })
            }

            return results;
        },[])
        .value()


        console.log('COMBINED', $scope.display);
    }
    /**/

    ////////////////////////////////////////////////////

    FluroContent.endpoint('contact/' + $scope.contact._id + '/relationships', true, true)
    .query({
        backlinks:true
    })
    .$promise
    .then(function(res) {
        $scope.backlinks = res;
        // _.filter(res, function(relationshipRow) {
        //     return !_.some($scope.model,function(r) {
        //         return r.relationship.toLowerCase() == relationshipRow.relationship.toLowerCase()
        //     })
        // });


        // updateDisplay();
    });

    ////////////////////////////////////////////////////

    $scope.getContacts = function(row) {

        var relationship = String(row.relationship).toLowerCase();
        
        var matchingRow = _.find($scope.backlinks, function(r) {
            return r.relationship.toLowerCase() == relationship;
        })

        if(!matchingRow) {
            return row.contacts;
        }
        var all = [].concat(row.contacts, matchingRow.contacts);
        return _.uniq(all, function(contact) {
            return contact._id;
        })
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {

        var insert = angular.copy($scope._new);

        if(!$scope.model) {
            $scope.model = [];
        }

        if(insert.relationshipType != 'other') {
        	insert.relationship = insert.relationshipType;
        	delete insert.relationshipType;
        }

        if(!insert.relationship || !insert.relationship.length) {
        	return;
        }

        if(!insert.contacts || !insert.contacts.length) {
        	return;
        }


        var relationship = insert.relationship;

        var existing = _.find($scope.model, {relationship:relationship});

        if(existing) {
        	existing.contacts = existing.contacts.concat(insert.contacts);
        	existing.contacts = _.uniq(existing.contacts, function(contact) {
        		return contact._id;
        	})

        	return $scope.resetProposed();
        } else {
        	
        	 $scope.model.push(insert);;
            $scope.resetProposed();
        }

        // updateDisplay();

        // if(insert.key && !$scope.model[insert.key]) {
           
        // }
    }

    ////////////////////////////////////////////////////

    // updateDisplay();

})
