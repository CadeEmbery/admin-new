app.directive('externalLinkSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-external-link-select/admin-external-link-select.html',
        controller: 'ExternalLinkSelectController',
        link:function($scope, $element) {
            $scope.$element = $element;
        }
    };
});



app.controller('ExternalLinkSelectController', function($scope) {

    ////////////////////////////////////////////////////

    //Create a model if it doesn't exist
    if (!$scope.model) {
        $scope.model = [];
    } 


    //Create a new object
    $scope._new = {}

    ////////////////////////////////////////////////////

    $scope.add = function() {
        var insert = angular.copy($scope._new);
        $scope.model.push(insert);
        $scope._new = {};

         console.log('REFOCUS')
          $scope.$element.find('.focus-back').focus();
    }

    ////////////////////////////////////////////////////

    $scope.remove = function(entry) {
        _.pull($scope.model, entry);
    }

    ////////////////////////////////////////////////////

    $scope.contains = function(entry) {
        return _.contains($scope.model, entry);
    }

})


