app.directive('componentPreview', function(Fluro, $http, $compile, $timeout, $injector, $ocLazyLoad) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-component-preview/admin-component-preview.html',
        //controller: 'ComponentPreviewController',
        link: function($scope, $element, $attr) {

            $scope.$watch('model', function(model) {
                if (model) {

                    /**/
                    var url = Fluro.apiURL + '/get/scripts/component/' + $scope.model._id;

                    /*
                    $element.append('<script src="'+url+'" type="text/javascript"></script>');
                    
                    
*/
                    console.log('Lazy load', url)
                    $ocLazyLoad.load('js!'+url).then(function() {
                        console.log('OC LAZY LOADED')
                        $compile($element)($scope);
                        console.log('Compiled New Element')
                    });

                    /*
                    $http.get(url)
                        .then(function(res) {
                            var output = '<script type="text/javascript">' + res.data + '</script>';
                            
                            var compiled = $compile(output)($scope);
                            $element.append(compiled);

                            ///////////////////////////////////

                            $timeout(function() {
                                var exist = $injector.has('testComponentDirective');
                                if(exist) {
                                    $element.append('<test-component ng-model="{}" ng-nodes="{}"></test-component>');
                                } else {
                                    console.log('Nope');
                                }
                            })

                            //

                        })
*/


                }
            })
        }
    };
});

/*

app.controller('ComponentPreviewController', function($scope, Fluro, $http, $compile) {



    $scope.$watch('model', function(model) {
        if (model) {
            $http.get(Fluro.apiURL + '/get/scripts/component/' + $scope.model._id)
                .then(function(res) {
                    var output = '<script type="text/javascript">' + res.data + '</script>';



                })
        }
    })

})

*/