app.directive('myProcessPanel', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        replace:true,
        // scope: {
        //     model: '=ngModel',
        //     placeholder:'@',
        //     //type: '=ngType',
        //     //title: '=ngTitle',
        //     //minimum: '=ngMinimum',
        //     //maximum: '=ngMaximum',
        // },
        templateUrl: 'admin-my-process-panel/admin-my-process-panel.html',
        controller: 'MyProcessPanelController',
    };
});

app.controller('MyProcessPanelController', function($scope, $state, $rootScope, $interval, MyProcessService, AvailableProcessCardsService) {

    $scope.availableService = AvailableProcessCardsService;
    $scope.processService = MyProcessService;


    $scope.click = function(card) {
        console.log('CARD', card);
        // $rootScope.editItem(card);

         // $rootScope.editItem = function(item, isModal, type) {

        

        // console.log('Get EDIT ITEM', item, isModal, type);
        // return;

        // console.log('VU')

        // if (isModal) {
            //Were in a modal
            // ModalService.edit(item);
            // ui-sref="contact.view({id:contact._id, backOnClose:true})"
        // } else {
            // if (!type || !type.length) {
                // type = item._type;
            // }
            $state.go('process.edit', {
                id: card._id,
                definitionName: card.definition,
                backOnClose: true
            })
        // }
    



    }

    ///////////////////////////////////////////

    $scope.$watch('processService.items', function(items) {
        $scope.list =_.sortBy($scope.processService.items, PROCESS_CARD_PRIORITY_SORT);

       
    
    })

    ///////////////////////////////////////////

    function refresh() {
        MyProcessService.reload();
        AvailableProcessCardsService.reload();
    }

    ///////////////////////////////////////////
    
    $interval(refresh, 10000);
    refresh();
    

});
