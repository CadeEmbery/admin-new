app.directive('batchProcessSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            selection: '=ngModel',
            type:'=ngType',
            // definition:'=ngDefinition',
        },
        templateUrl: 'admin-batch-process-select/admin-batch-process-select.html',
        controller: 'BatchProcessSelectController',
    };
});

app.controller('BatchProcessSelectController', function($scope, TypeService, $state, ModalService, Batch, Notifications, $rootScope, CacheManager, Fluro, FluroContent, FluroAccess, $http) {


    $scope.popover = {
            open:false
        }

    // refreshDefinedTypes
    //Get all the options
    // FluroContent.endpoint('defined/types/process').query().$promise.then(function(res) {
    //     $scope.options = res;
    // }, function(err) {
    //     console.log('Error loading process types', err)
    // });

    $scope.$watch(function() {
        return TypeService.definedTypes;
    }, function(types) {
        $scope.options = _.chain(types)
        .filter(function(type) {
            return type.parentType == 'process';
        })
        .sortBy(function(type) {
            return type.title;
        })
        .value()
    })


    $scope.dynamicPopover = {
        templateUrl: 'admin-batch-process-select/admin-batch-process-popover.html',
    };


    $scope.createEnabled = FluroAccess.can('create', 'process');   

    $scope.create = function() {
        $scope.popover = {
            open:false
        }

        ///////////////////////////////

        var template = {
            parentType:'process',
            data: {
                processTypes: [
                    $scope.type.path,
                ]
            },
        }

        ///////////////////////////////

        ModalService.create('definition', {template:template}, function(res) {
            $scope.options.push(res);
            $scope.model.push(res);
        })
    }


    /**

    if (!$scope.selectedProcesses) {
        $scope.selectedProcesses = [];
    }


    $scope.selected = function(item) {
        return _.includes($scope.selectedProcesses, item);
    }


    $scope.toggle = function(item) {
        if($scope.selected(item)) {
            $scope.deselect(item);
        } else {
            $scope.select(item);
        }
    }

    $scope.select = function(item) {
        if(!$scope.selected(item)) {
            $scope.selectedProcesses.push(item);
        }
    }

    $scope.deselect = function(item) {
        
        _.pull($scope.selectedProcesses, item);
        
    }

    /**/

    /////////////////////////////////////



    /////////////////////////////////////


    $scope.addToProcess = function(processName) {

        $scope.popover = {
            open:false
        }

        // var processNames =  _.map($scope.selectedProcesses, function(def) {
        //     return def.definition
        // })
        var details = {
            ids: $scope.selection.ids,
            process: processName, //$scope.selectedProcesses,
        }

        Batch.addToProcess(details, function(err, data) {
            if (err) {
                Notifications.error(err);
            } else {

                if(!data.success || !data.success.length) {
                    return Notifications.warning('These ' + $scope.selection.length + ' items were unable to be added to ' + processName);
                }
                
                Notifications.status('Added '+ data.success.length +' items to process');
                
                //Clear the cache for the process
                CacheManager.clear(processName);

                ///////////////////////

                //Reload the state
                $state.reload();

                $state.go('process.custom', {
                    definition: processName
                })
            }
        });



    }



    /**






    /////////////////////////////////////


    /////////////////////////////////////

    $scope.proposed = {};

    /////////////////////////////////////

    $scope.add = function(item) {
        $scope.selectedProcesses.push(item);
        $scope.proposed = {};
    }


    $scope.remove = function(product) {
       _.pull($scope.selectedProcesses, product);
    }

    /////////////////////////////////////

    $scope.getProcesess = function(val) {

        var url = Fluro.apiURL + '/content/process/search/' + val;
        return $http.get(url, {
            ignoreLoadingBar: true,
            params: {
                limit: 20,
            }
        }).then(function(response) {
            var results = response.data;
            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.selectedProcesses, {
                    '_id': item._id
                });
                
                if (!exists) {
                    filtered.push(item);
                }
                return filtered;
            }, [])
        });

    };

    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////


    $scope.addToProcesess = function() {

        var details = {
            ids: $scope.selection.ids,
            products: $scope.selectedProcesses,
        }

        Batch.addToProcesess(details, done);
    }

    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////


    $scope.removeFromProcesess = function() {

        var details = {
            ids: $scope.selection.ids,
            products: $scope.selectedProcesses,
        }

        Batch.removeFromProcesess(details, done);
    }

    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////

    function done(err, data) {
        if (err) {
            Notifications.error(err);
        } else {
            if(data.results.length == 1) {
                Notifications.status('Added '+ $scope.selection.length +' items to ' + "'" + data.results[0].title + "'");
            } else {
                Notifications.status('Added '+ $scope.selection.length +' items to ' + data.results.length + ' products');
            }

            _.chain(data.results)
                .map(function(result) {
                    if (result.definition) {
                        return result.definition;
                    } else {
                        return result._type;
                    }
                })
                .uniq()
                .each(function(type) {
                    CacheManager.clear(type);
                })
                .value();


            $state.reload();


            //Deselect
            //$scope.selection.deselectAll();
        }
    }
    /**/


});