app.directive('batchExport', function() {

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'admin-batch-export/admin-batch-export.html',
        controller: 'BatchExporterController',
    };
});



// app.directive('batchExtendedFields', function($compile) {

//     return {
//         restrict: 'A',
//         link: function($scope, $element, $attrs) {

//             if ($scope.definition) {
//                 //Flatten all the fields that are defined
//                 $scope.flattenedFields = getFlattenedFields($scope.definition.enabledFields);
//             }
//             var template = '<div class="clearfix" ng-repeat="field in flattenedFields"><div class="col-md-3"><label>Modify</label><input type="checkbox" ng-model="batchConfig.modifyFields[field.key]"/></div><div class="col-md-9"><field-edit-render ng-model="batchConfig.data[field.key]" ng-field="field"></field-edit-render></div></div>';

//             //Compile the template and replace
//             var cTemplate = $compile(template)($scope);
//             $element.append(cTemplate);

//         }
//     };
// });


app.service('BatchExportService', function(Fluro, $http, ModalService, TypeService, FluroContent, Notifications, $window, Batch) {

    var service = {};

    /////////////////////////////////////////

    service.requestKeys = function(definitionName, details) {


        return $http.post(Fluro.apiURL + '/export/' + definitionName + '/keys', details)
            // FluroContent.endpoint('export/' + definitionName + '/keys')
            // .save(details)
            // .$promise;
    }

    /////////////////////////////////////////

    service.run = function(definitionName, details) {
        return FluroContent.endpoint('export/' + definitionName)
            .save(details)
            .$promise
            .then(function(res) {

                Notifications.post('Your popup blocker may stop this file from downloading', 'warning');

                var url = Fluro.apiURL + res.download;

                if (Fluro.token) {
                    $window.open(url + '?access_token=' + Fluro.token);
                } else {
                    $window.open(url);
                }

            }, function(err) {
                console.log('Error', err);
            });

    }


    /////////////////////////////////////////

    service.save =  function(definitionName, details) {

        //Get the fields and the ids
        var fields = details.fields;
        var unwinds = details.unwind;
       
        ///////////////////////////////////
        ///////////////////////////////////

        //Check if it's a basic type
       var isBasic = TypeService.isBasicType(definitionName);
       var criteria;




        ///////////////////////////////////

       if(isBasic) {
            criteria = {
                _type:definitionName,
                status:{
                    $ne:'deleted',
                }
            }
        } else {
            criteria = {
                definition:definitionName,
                status:{
                    $ne:'deleted',
                }
            }
        }

        ///////////////////////////////////
        ///////////////////////////////////
        ///////////////////////////////////



        var template = {
            title:details.title,
            query:JSON.stringify(criteria),
            unwind:unwinds,

        };


        ///////////////////////////////////
        ///////////////////////////////////
        ///////////////////////////////////
        ///////////////////////////////////


        var fieldBreakdown = _.reduce(fields, function(set, field) {

            //If its got a longer trail then we should populate it
             var keyPieces = field.key.split(/[\.\[]/);
             if(keyPieces.length > 1) {
                set.populates.push(keyPieces[0]);
                set.populateSelects.push(keyPieces[1]);
             }

             set.selects.push(keyPieces[0]);

             return set;
        }, {populates:[], populateSelects:[], selects:[]});
        
        ////////////////////////

        template.populateFields = _.chain(fieldBreakdown.populates)
        .compact()
        .uniq()
        .value()
        .join(' ');

        ////////////////////////

        template.populateSelect = _.chain(fieldBreakdown.populateSelects)
        .compact()
        .uniq()
        .value()
        .join(' ');

        var selectFields = _.chain(fieldBreakdown.selects)
        .compact()
        .uniq()
        .value();

        ////////////////////////

        selectFields = selectFields.concat([
            '_type',
            'definition',
            'realms'
        ])
        template.select = selectFields.join(' ');

        ////////////////////////

        template.columns =  _.map(fields, function(field) {
            return {
                title:field.label,
                key:field.key,
            }
        })

        ///////////////////////////////////
        ///////////////////////////////////
        ///////////////////////////////////
        ///////////////////////////////////

         var params = {
            template:template,
         }

        ///////////////////////////////////

        return ModalService.create('query', params);

    }   

    // service.save = function(definitionName, details) {

        // return FluroContent.endpoint('export/' + definitionName + '/save')
        //     .save(details)
        //     .$promise
        //     .then(function(res) {
        //         console.log('RESPONSE', res);
        //     }, function(err) {
        //         console.log('Error', err);
        //     });

    // }

    /////////////////////////////////////////

    return service;
})


app.controller('BatchExporterController', function($scope, FluroStorage, FluroContent, BatchExportService, Notifications, $state, CacheManager, Batch) {

    $scope.cancel = $scope.$dismiss;

    var definitionName = _.get($scope.definition, 'definitionName') || $scope.type.path;


    //Get the local storage for this batch editing
    $scope.local = FluroStorage.localStorage(definitionName);

    /////////////////////////////////////////

    if ($scope.local.export) {
        $scope.config = $scope.local.export;
    } else {
        $scope.local.export = $scope.config = {
            selectedFields: [],
            unwinds: [],
        }
    }

    /////////////////////////////////////////

    $scope.config.selectedColumns = _.chain($scope.config.selectedColumns)
        .map(function(field) {
            field.uuid = guid();
            return field;
        })
        .compact()
        .value();




    /////////////////////////////////////////
    
    $scope.run = function() {

        var details = {
            ids: $scope.ids,
        }

        //Attach the fields
        details.fields = $scope.config.selectedColumns;

        //If there are unwinds 
        details.unwind = $scope.config.unwinds;

        BatchExportService.run(definitionName, details);

    }

    /////////////////////////////////////////
    
    $scope.save = function() {

        var details = {
            ids: $scope.ids,
        }

        //Attach the fields
        details.fields = $scope.config.selectedColumns;

        //If there are unwinds 
        details.unwind = $scope.config.unwinds;

        details.title = '';

        if($scope.definition) {
            details.title = 'New Export - ' +$scope.definition.plural;
        } else {
            details.title = 'New Export - ' +$scope.type.plural;
        }

        BatchExportService.save(definitionName, details);

    }

    /////////////////////////////////////////

    $scope._proposed = {};

    /////////////////////////////////////////

    function enableColumn(key) {
        var match = _.find($scope.columns, {
            key: key
        });

        if(!match) {
            return;
        }

        if (!$scope.config.selectedColumns) {
            $scope.config.selectedColumns = []
        }

        match = angular.copy(match);
        match.uuid = guid();
        $scope.config.selectedColumns.push(match);

    }

    /////////////////////////////////////////

    $scope.$watch('_proposed.field', function(key) {
        if (!key) {
            return;
        }

        //////////////////////////////////////

        if (key == '_form') {

            var formFields = _.chain($scope.columns)
                .filter(function(column) {
                    return _.startsWith(column.key, 'rawData');
                })
                .map(function(col) {
                    return enableColumn(col.key);
                })
                .value();

            //Delete and finish
            delete $scope._proposed.field;
            return;


        }

        //////////////////////////////////////

        if (key == '_defined') {

            var formFields = _.chain($scope.columns)
                .filter(function(column) {
                    return _.startsWith(column.key, 'data');
                })
                .map(function(col) {
                    return enableColumn(col.key);
                })
                .value();

            //Delete and finish
            delete $scope._proposed.field;
            return;


        }

        //////////////////////////////////////

        if (key == '_all') {

            var formFields = _.chain($scope.columns)
            .filter(function(item) {
                return !_.includes(item.key, '[');
            })
            .map(function(col) {
                return enableColumn(col.key);
            })
            .value();

            //Delete and finish
            delete $scope._proposed.field;
            return;
        }

        //////////////////////////////////////

        //End here by adding the selected column
        enableColumn(key);
        delete $scope._proposed.field;
        return;

    })

    /////////////////////////////////////////

    $scope.$watch('_proposed.unwind', function(key) {
        if (!key) {
            return;
        }

        if (!$scope.config.unwinds) {
            $scope.config.unwinds = []
        }

        if ($scope.config.unwinds.indexOf(key) != -1) {
            delete $scope._proposed.unwind;
            return;
        }

        $scope.config.unwinds.push(key);

        delete $scope._proposed.unwind;
    })

    /////////////////////////////////////////



    /////////////////////////////////////////

    $scope.$watch('config.unwinds', reloadKeys, true);
    $scope.$watch('config.selectedColumns', rerender, true);
    $scope.$watch('columns', rerender);

    function rerender() {

        var tallest = 0;

        var renderColumns = _.chain($scope.config.selectedColumns)

        .map(function(c) {

                var key = c.key;

                var match = _.find($scope.columns, {
                    key: key
                });

                if (!match) {
                    match = {
                        label: c.label,
                        key: key,
                    }
                }

                var clone = _.clone(match);
                clone.uuid = guid();

                if (clone.rows) {
                    tallest = Math.max(tallest, clone.rows.length);
                }

                return clone;
            })
            // .compact()
            .value();


        // , 'key');

        // /////////////////////////////////////////

        // var renderColumns = _.chain($scope.columns)
        // .filter(function(column) {
        //     return _.includes(selectedColumnKeys, column.key);
        // })
        // .slice()
        // .map(function(column) {
        //     column.uuid = guid();
        //     return column;

        // })
        // .value()

        $scope.range = _.range(0, Math.min(10, tallest));

        /////////////////////////////////////////

        $scope.renderColumns = renderColumns;
    }

    /////////////////////////////////////////

    $scope.deselectField = function(item) {
        _.pull($scope.config.selectedColumns, item)
    }

    $scope.deselectUnwind = function(item) {
        _.pull($scope.config.unwinds, item)
    }

    /////////////////////////////////////////
    /////////////////////////////////////////


    function reloadKeys() {

        var request = {
            ids: $scope.ids,
            unwind: $scope.config.unwinds,
        }

        /////////////////////////////////////////////////
        BatchExportService
            .requestKeys(definitionName, request)
            .then(keysLoaded, keysFailed);

        /////////////////////////////////////////////////

        function keysLoaded(res) {

            var columns = [];
            var unwindableColumns = [];

            ///////////////////////////////////

            _.each(res.data, function(column) {
                var labelString = column.key;
                var labelPieces = labelString.split(/[\.\[]/);
                var labelStart = labelPieces[0];
                var selectLabelString;

                if(labelStart == 'rawData') {
                    // console.log('PIECES', labelPieces);
                    labelPieces.shift();

                    labelStart = labelPieces[0];
                    
                        labelString = _.startCase(labelPieces.join(' '));
                        selectLabelString = _.startCase('Submitted ' + labelPieces.join(' '));
                } else {
                    labelString = _.startCase(labelString);
                }

                //Render the column with a nicer label
                column.label = labelString;
                column.selectLabel =  selectLabelString || labelString;
                

                columns.push(column);

                if (column.multi) {
                    unwindableColumns.push(column);
                }

                column.examples = _.chain(column.rows)
                    .compact()
                    .uniq()
                    .slice(0, 3)
                    .join(', ')
                    .value()

            });

            ///////////////////////////////////

            //Add the columns to the scope
            $scope.columns = columns;
            $scope.unwindable = unwindableColumns;
        }

        function keysFailed(err) {
            console.log('Error', err);
        }

    }



});