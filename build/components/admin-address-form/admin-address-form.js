app.directive('addressForm', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-address-form/admin-address-form.html',
        //controller: 'CardEventsController',
    };
});


app.directive('addressView', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-address-form/admin-address-view.html',
        //controller: 'CardEventsController',
    };
});

