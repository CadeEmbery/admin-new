app.directive('batchEditor', function() {

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'admin-batch-editor/admin-batch-editor.html',
        controller: 'BatchEditorController',
    };
});



// app.directive('batchExtendedFields', function($compile) {

//     return {
//         restrict: 'A',
//         link: function($scope, $element, $attrs) {

//             if ($scope.definition) {
//                 //Flatten all the fields that are defined
//                 $scope.flattenedFields = getFlattenedFields($scope.definition.fields);
//             }
//             var template = '<div class="clearfix" ng-repeat="field in flattenedFields"><div class="col-md-3"><label>Modify</label><input type="checkbox" ng-model="batchConfig.modifyFields[field.key]"/></div><div class="col-md-9"><field-edit-render ng-model="batchConfig.data[field.key]" ng-field="field"></field-edit-render></div></div>';

//             //Compile the template and replace
//             var cTemplate = $compile(template)($scope);
//             $element.append(cTemplate);

//         }
//     };
// });




app.controller('BatchEditorController', function($scope, Notifications, FlattenService, $state, CacheManager, Batch) {

    console.log('BATCH EDITOR')


    function getFlattenedFieldTrails(array, trail) {

        return _.chain(array).map(function(field, key) {
                if (field.type == 'group') {

                    if (field.asObject) {

                        if(field.maximum != 1) {
                            return;
                        }
                        // return;
                        //Push the field key
                        trail.push(field.key);
                    }
                    var fields = getFlattenedFieldTrails(field.fields, trail);

                    trail.pop();
                    return fields;
                } else {

                    //Push the field key
                    trail.push(field.key);

                    field.trail = trail.slice().join('.');
                    trail.pop();

                    // console.log(field.title, trail);
                    return field;
                }
            })
            .compact()
            .flatten()
            .value();
    }



    /////////////////////////////////////////
    /////////////////////////////////////////
    /////////////////////////////////////////
    /////////////////////////////////////////
    /////////////////////////////////////////
    /////////////////////////////////////////
    /////////////////////////////////////////

    $scope.batchConfig = {
        modify: {
            basic: {},
            defined: {},
        },
        behaviour: {}
    };

    /////////////////////////////////////////
    /////////////////////////////////////////
    /////////////////////////////////////////

    ///This is the object we are actually editing when we change fields
    $scope.updateData = {}

    /////////////////////////////////////////
    /////////////////////////////////////////
    /////////////////////////////////////////

    $scope.extras = {};

    /////////////////////////////////////////

    //Find all the batchEditable fields on the basic item schema
    var editableFields = _.filter($scope.baseFields, function(field) {
        return field.batchEditable;
    })


    // console.log('BASIC FIELDs', editableFields);

    /////////////////////////////////////////

    switch ($scope.type.path) {
        case 'event':
            editableFields.push({
                title: 'Checkin Opens (mins)',
                description: 'How many minutes earlier can users checkin',
                directive: 'input',
                type: 'integer',
                key: 'checkinData.checkinStartOffset',
                // trail:'checkinData.checkinStartOffset',
                maximum: 1,
                minimum: 1,
            })

            editableFields.push({
                title: 'Checkin Closes (mins)',
                description: 'How many minutes after this events end time should this event show in the checkin app',
                directive: 'input',
                type: 'integer',
                key: 'checkinData.checkinEndOffset',
                // trail:'checkinData.checkinStartOffset',
                maximum: 1,
                minimum: 1,
            })

            editableFields.push({
                title: 'Require PIN when checking in',
                description: 'Should this specific event require a PIN number when checking in, if the app doesn\'t already require it?',
                directive: 'input',
                type: 'boolean',
                key: 'checkinData.requirePin',
                // trail:'checkinData.checkinStartOffset',
                maximum: 1,
                minimum: 0,
            })

            editableFields.push({
                title: 'Disable Checkin',
                description: 'Should checkin be disabled for this event?',
                directive: 'input',
                type: 'boolean',
                key: 'checkinData.disabled',
                // trail:'checkinData.checkinStartOffset',
                maximum: 1,
                minimum: 0,
            })
            break;
    }




    editableFields.push({
        title: 'Inheritable',
        description: 'Allow usage and access to child accounts',
        directive: 'input',
        type: 'boolean',
        key: 'inheritable',
        // trail:'checkinData.checkinStartOffset',
        maximum: 1,
        minimum: 1,
    })





    $scope.basicList = getFlattenedFieldTrails(editableFields, []);


    // console.log('bASIC LIST', $scope.basicList);
    $scope.basicDefinition = {
        fields: editableFields,
    }

    /////////////////////////////////////////

    if ($scope.definition) {
        $scope.definedList = getFlattenedFieldTrails($scope.definition.fields, []);
    }


    console.log('DEFINED', $scope.definedList);


    /////////////////////////////////////////

    $scope.$watch('batchConfig.modify', updateList, true)
        // $scope.$watch('basicList', updateBasicList, true)

    /////////////////////////////////////////

    function updateList(list) {

        // console.log('DEFINED LIST', $scope.definedList);

        /////////////////////////////////////////

        var arrayFields = [];

        /////////////////////////////////////////

        if ($scope.definition) {
            $scope.definedFields = _.filter($scope.definedList, function(field) {
                field.keyTrail = field.trail.replace(/\./g, "_")
                var activated = _.get($scope.batchConfig, 'modify.defined[' + field.keyTrail + ']');
                // var activated = (_.get($scope.batchConfig, 'modify.defined') || {})[field.trail];
                return activated;
            })
        } else {
            $scope.definedFields = [];
        }

        /////////////////////////////////////////

        //Loop through each field and match
        $scope.basicFields = _.filter(editableFields, function(field) {

            var visible = $scope.batchConfig.modify.basic[field.key];

            if (visible) {
                console.log('IS VISIBLE', field.key, $scope.batchConfig)
            }

            return visible;

        })

        /////////////////////////////////////////


        _.each($scope.basicFields, function(field) {
            if (field.maximum != 1) {
                arrayFields.push(field);
            }
        });

        _.each($scope.definedFields, function(field) {
            if (field.maximum != 1) {
                field.defined = true;
                arrayFields.push(field);
            }
        });

        // _.chain([
        //     $scope.basicFields, 
        //     $scope.definedFields
        // ])
        // .flattenDeep()
        // .filter(function(field) {
        //     return field.maximum != 1;
        // })
        // .value();


        $scope.arrayFields = arrayFields;


    }

    /////////////////////////////////////////

    //Template
    // $scope.template = 'fluro-admin-content/types/' + $scope.type.path + '/form.html';

    /////////////////////////////////////////

    // $scope.$watch('batchConfig.modifyFields', function() {
    //     console.log($scope.batchConfig.modifyFields);
    // }, true)


    /////////////////////////////////////////

    $scope.behaviourActive = function(field, value) {
        var trail = field.trail;

        if (field.defined) {
            trail = 'behaviour.data.' + trail;
        } else {
            trail = 'behaviour.' + trail;
        }

        var currentValue = _.get($scope.batchConfig, trail);

        if (!currentValue) {
            currentValue = '';
        }

        return currentValue == value;

    }

    $scope.toggleArrayBehaviour = function(field, value) {


        var trail = field.trail;

        if (field.defined) {
            trail = 'behaviour.data.' + trail;
        } else {
            trail = 'behaviour.' + trail;
        }

        _.set($scope.batchConfig, trail, value);

    }

    /////////////////////////////////////////

    $scope.cancel = $scope.$dismiss;

    /////////////////////////////////////////

    //Save
    $scope.run = function(status) {

        // return console.log('Fail on purpose for now', $scope.batchConfig)
        var details = {}
        details.ids = $scope.ids;
        // details.fields = {
        //     data:{}
        // };




        /////////////////////////////////////////

        //Gather all the fields we want to edit on the basic type

        var list = _.get($scope.batchConfig, 'modify.basic');
        var basicSet = _.chain(list)
            .map(function(value, key) {
                if (!value) {
                    return;
                }

                return key;
            })
            .compact()
            .value();

        /////////////////////////////////////////

        //Gather all the fields we want to edit that are defined on the object
        var list = _.get($scope.batchConfig, 'modify.defined');
        var definedSet = _.chain(list)
            .map(function(value, key) {
                // console.log('MATCH', value, key);
                if (!value) {
                    return;
                }

                return key;
                // console.log('KEY IS ACTIVE', key);
                // return key;

            })
            .compact()
            .value();

        /////////////////////////////////////////

        var trailKeys = [];
        trailKeys = trailKeys.concat(basicSet, definedSet);

        /////////////////////////////////////////

        console.log('UPDATE DATA', trailKeys, $scope.updateData);
        // return;

        var fieldLookup = {};

        _.each(basicSet, function(key) {
            console.log('SET KEY', key);
            mapFields(fieldLookup, key);
        });

        _.each(definedSet, function(key) {
            console.log('SET KEY', key);
            mapFields(fieldLookup, key, 'data.');
        });



        /////////////////////////////////////////

        function referenceIDs(array) {
            return _.chain(array)
            .compact()
            .map(function(value) {
                if(value._id) {
                    return value._id;
                }

                return value;
            })
            .compact()
            .value();
        }
        /////////////////////////////////////////

        details.fields = fieldLookup;

        console.log('FIELD LOOKUP', fieldLookup, $scope.updateData);

        function mapFields(results, key, appendKey) {

            if(!appendKey || !appendKey.length) {
                appendKey = '';
            }

            var dotPath = appendKey + String(key.replace(/\_/g, "."));
            var newValue = _.get($scope.updateData, appendKey + key);

            if(newValue._id) {
                newValue = newValue._id;
            }

            if(_.isArray(newValue)) {
                newValue = referenceIDs(newValue);
            }

            if (!newValue) {
                console.log('NO VALUE FOR', dotPath);
                _.set(results, dotPath, null);
                return results;
            }

            console.log('SET NEW VALUE', dotPath, newValue);
            //Set the data on the results object
            _.set(results, dotPath, newValue);



            return results;
        }

        //Update behaviour is
        details.behaviour = $scope.batchConfig.behaviour;

        /////////////////////////////////////////

        // details.modifiedData = $scope.batchConfig.data;

        /*
        _.each($scope.batchConfig.modifyFields, function(val, fieldPath) {
            if(val) {
                //console.log('TESTING', 'val', val, 'fieldName', fieldName);

                var newValue = _.get($scope.batchConfig.data, fieldPath);
                _.set(details.fields.data, fieldPath, newValue);
                //details.fields.data[fieldName] = $scope.batchConfig.data[fieldName];
            }
        })
        */
       
       if($scope.type.path == 'ticket') {
           details.useModel = 'ticket';
       }

       if($scope.type.path == 'persona') {
           details.useModel = 'persona';
       }

        /////////////////////////////////////////
        /////////////////////////////////////////
        /////////////////////////////////////////


        Batch.updateFields(details, function(err, resource) {
            if (err) {
                Notifications.error('error')
            } else {
                $scope.$close(resource.results);

                if ($scope.definition) {
                    CacheManager.clear($scope.definition.definitionName);
                } else {
                    CacheManager.clear($scope.type.path);
                }

                $state.reload();

                var length = _.get(resource, 'results.length');

                // $scope.successCallback(resource.results)
                Notifications.status(length + ' ' + $scope.type.plural + ' were updated')
            }
        });

        //Here is where the magic happens
    }



});