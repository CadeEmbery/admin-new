app.directive('applicationSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            hostType: '@',
        },
        templateUrl: 'admin-application-select/admin-application-select.html',
        controller: 'ApplicationSelectController',
    };
});

/////////////////////////////////////////////////////

app.controller('ApplicationSelectController', function($scope, FluroAccess, $filter, FluroAccess, ModalService, FluroContent) {

	if($scope.hostType) {
		$scope.hostType = 'role';
	}

	if(!$scope.model) {
		$scope.model = [];
	}

	var queryDetails = {
		searchInheritable:true,
		allDefinitions:true,
	}

	////////////////////////////////////////

	if(!$scope.settings) {
		$scope.settings = {}
	}

	if(!$scope.model.length) {
		$scope.settings.showList = true;
	}

	$scope.canEdit = FluroAccess.canEditItem;

	////////////////////////////////////////

	FluroContent.resource('application', false, true)
        .query(queryDetails)
        .$promise.then(function(results) {
            //console.log('Got content results', results)
            $scope.applications = _.sortBy(results, function(app) {
            	return app.title;
            });
        });

	////////////////////////////////////////
	
	$scope.select = function(app) {
		$scope.model.push(app);
	}

	$scope.deselect = function(app) {
		var found = _.find($scope.model, {_id:app._id});
		_.pull($scope.model, found);
	}

	$scope.toggle = function(app) {
		var selected = $scope.isSelected(app);
		if(!selected) {
			$scope.select(app);
		} else {
			$scope.deselect(app);
		}
	}


	////////////////////////////////////////

	$scope.isSelected = function(app) {
		return _.some($scope.model, function(p) {
			return p._id == app._id;
		})
	}

})