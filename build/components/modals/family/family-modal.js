



app.directive('familyModalContact', function(){
    // Runs during compile
    return {
        // name: '',
        // priority: 1,
        // terminal: true,
        replace:true,
        scope: {
            family:'=',
            item:'=ngModel',
        },
        controller: 'FamilyModalContactController',
        // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
        // restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
        // template: '',
        templateUrl: 'modals/family/contact.html',
        // replace: true,
        // transclude: true,
        // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
        // link: function($scope, iElm, iAttrs, controller) {
            
        // }
    };
});


app.controller('FamilyModalContactController', function($scope, TypeService) {

    $scope.settings = {};


     $scope.contactDefinitions = _.filter(TypeService.definedTypes, {parentType:'contact'});



});


app.controller('FamilyModalController', function($scope, $uibModal, FluroContent, CacheManager, Notifications) {



    $scope.item = {
        samePostal:true,
        items:[{}],
    }


    $scope.addContact = function() {
        $scope.item.items.push({
            lastName:$scope.item.title,
        })
    }

    $scope.removeContact = function(contact) {
        _.pull($scope.item.items, contact);
    }






    $scope.save = function() {


        //Check everything
        if(!$scope.item.realms || !$scope.item.realms.length) {
            return Notifications.error('Please select at least one realm for the family household');
        }

        if(!$scope.item.title || !$scope.item.title.length) {
            return Notifications.error('Household title is required');
        }
        //////////////////////////////////////////////

        var errorMessages = _.chain($scope.item.items)
        .map(function(contact, index){

            if(!contact.lastName || !contact.lastName.length) {
                contact.lastName = $scope.item.title;
            }

            if(!contact.firstName || !contact.firstName.length) {
                return 'Family member ' + (index+1) + ' does not have a first name';
            }
        })
        .compact()
        .flatten()
        .value();


        if(errorMessages.length) {
            return Notifications.error(errorMessages[0]);
        }

        //////////////////////////////////////////////

        FluroContent.endpoint('family').save($scope.item).$promise.then(function(res) {

             CacheManager.clear('family');
             CacheManager.clear('contact');

            Notifications.status('Family was saved successfully!');

            $scope.$close(res);
        }, function(err) {

            var errorMessage = err;


            if(errorMessage.data) {
                errorMessage = errorMessage.data;
            }

            if(errorMessage.errors && errorMessage.errors.length) {
                errorMessage = errorMessage.errors[0];
            }

            if(errorMessage.error && errorMessage.error) {
                errorMessage = errorMessage.error;
            }

            if(errorMessage.message && errorMessage.message.length) {
                errorMessage = errorMessage.message;
            }

            

            return Notifications.error(errorMessage);
        })


    }
})