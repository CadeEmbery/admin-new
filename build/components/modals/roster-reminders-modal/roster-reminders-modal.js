app.directive('rosterRemindersModal', function(){
	// Runs during compile
	return {
		// name: '',
		// priority: 1,
		// terminal: true,
        replace:true,
		scope: {
            title:'=',
            defaultReminders:'=',
			model:'=ngModel',
			close: '=',
            dismiss:'=',
			processing:'=',
		},
		controller: 'RosterRemindersModalController',
		// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
		// restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		// template: '',
		templateUrl: 'modals/roster-reminders-modal/roster-reminders-modal.html',
		// replace: true,
		// transclude: true,
		// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
		// link: function($scope, iElm, iAttrs, controller) {
			
		// }
	};
});


app.controller('RosterRemindersModalController', function($scope, $uibModal) {


$scope.processing = false;


    console.log('SCOPE',$scope);

	////////////////////////////////////////////////////

   	function removeSlot(slot) {
    	return _.pull($scope.model.data.slots, slot);
    }

        ////////////////////////////////////////////////////

    $scope.editSlot = function(slot) {

        var modalInstance = $uibModal.open({
            template: '<default-roster-slot></default-roster-slot>',
            size: 'md',
            backdrop:'static',
            controller: function($scope) {
                $scope.slot = slot;
                $scope.remove = function(slot) {
                	removeSlot(slot);
                	$scope.$close();
                }
            }
        });

    }

    

    ////////////////////////////////////////////////////

    $scope.createSlot = function() {

        //Check if a slots array is created
        var slots = _.get($scope.model, 'data.slots');
        if (!slots) {

            if(!$scope.model.data) {
                $scope.model.data = {}
            }
            $scope.model.data.slots = [];
        }

        ////////////////////////////////////////////////////

        var slot = {
            title: '',
            minimum: 1,
            maximum: 0,
            requireCapabilities: [],
            preferCapabilities: [],
        }

        ////////////////////////////////////////////////////

        $scope.model.data.slots.push(slot);

        ////////////////////////////////////////////////////

        $scope.editSlot(slot);

    }


})