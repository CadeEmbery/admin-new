app.directive('progressPills', function() {

    return {
        restrict: 'E',
        replace:true,
        scope: {
            completed: '=',
            total: '=',
        },
        templateUrl: 'progress-pills/progress-pills.html',
        link: function($scope, $element, $attrs) {

            ///////////////////////////////////////

            $scope.$watch('completed + total', function() {

                $scope.pills = [];
                
                _.times(parseInt($scope.total), function(n) {
                    if (n <= parseInt($scope.completed)) {
                        $scope.pills[n] = 'complete';
                    } else {
                        $scope.pills[n] = 'incomplete';
                    }
                });

            });
            
        }
    };
});