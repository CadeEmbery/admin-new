app.directive('groupNotificationSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-group-notification-select/admin-group-notification-select.html',
        controller: 'GroupNotificationSelectController',
    };
});





app.controller('GroupNotificationSelectController', function($scope) {




    if (!$scope.model.notifications || !_.isArray($scope.model.notifications)) {
        $scope.model.notifications = [];
    }

    ////////////////////////////////////////////////////

    $scope.reset = function() {
        //Create a new object
        $scope._new = {
            trigger: 'team.join',
            assignments: [],
        }
    }


    $scope.reset();

    ////////////////////////////////////////////////////

    $scope.getPlural = function(number) {
        number = parseInt(number)

        if (number != 1) {
            return 's';
        }
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    // $scope.getReminderTime = function(reminder) {

    //     var date;
    //     if(reminder.point == 'end') {
    //         date = moment($scope.endDate);
    //     } else {
    //         date = moment($scope.startDate);
    //     }

    //     var period = reminder.period;
    //     var total = parseInt(reminder.total);

    //     ///////////////////////////////

    //     if(reminder.when == 'after') {
    //         date = date.add(total, period);//.toDate();
    //     } else {
    //         date = date.subtract(total, period);//.toDate();
    //     }

    //     ///////////////////////////////

    //     return date.toDate().format('g:ia l M Y') + ' (' +date.fromNow() + ')';
    // }

    ////////////////////////////////////////////////////

    $scope.isSelected = function(notification, assignmentName) {

        if (!assignmentName) {
            return;
        }
        var lowerCaseName = assignmentName.toLowerCase();
        return _.includes(notification.assignments, lowerCaseName);
    }


    $scope.toggleSlot = function(notification, assignmentName) {
        if (!assignmentName) {
            return;
        }
        var selected = $scope.isSelected(notification, assignmentName);

        var lowerCaseName = assignmentName.toLowerCase();

        if (selected) {
            _.pull(notification.assignments, lowerCaseName);
        } else {
            if (!notification.assignments) {
                notification.assignments = [];
            }
            notification.assignments.push(lowerCaseName);
        }
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    $scope.remove = function(entry) {
        _.pull($scope.model.notifications, entry);
    }

    $scope.options = [];

    $scope.options.push({
        trigger:'team.join',
        title:'A new contact joins this group',
    })

    $scope.options.push({
        trigger:'team.leave',
        title:'A contact leaves this group',
    })

    $scope.options.push({
        trigger:'content.edit',
        title:'This group\'s information is updated',
    })

    $scope.options.push({
        trigger:'contact.unavailability',
        title:'A group member updates their availability',
    });

    $scope.options.push({
        trigger:'confirmation.confirmed',
        title:'A group member confirms an assignment',
    });

    $scope.options.push({
        trigger:'confirmation.unavailable',
        title:'A group member declines an assignment',
    });

    $scope.options.push({
        trigger:'contact.birthday',
        title:'Group members birthday',
    });

    // $scope.options.push({
    //     trigger:'contact.birthday',
    //     title:'A new post is added',
    // });


    $scope.getReadable = function(key) {
        return _.find($scope.options, {trigger:key}).title;
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {

        console.log('CREATE NEW NOTIFICATION', $scope._new);
        var insert = angular.copy($scope._new);
        if (!$scope.model.notifications) {
            $scope.model.notifications = [];
        }

        if (!$scope._new.assignments || !$scope._new.assignments.length) {
            return;
        }

        if(!$scope._new.trigger || !$scope._new.trigger.length) {
            return;
        }

        // if(insert.position) {
        // var keyExists = _.find($scope.model, {position:insert.position});

        // if(!keyExists) {
        $scope.model.notifications.push(insert);
        $scope.reset();
        // }
        // }
    }

})