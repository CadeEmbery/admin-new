app.directive('confirmButton', function($timeout) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            ask: '@',
            confirm: '@',
        },
        templateUrl: 'confirm-button/confirm-button.html',
        link: function($scope, $element, $attrs) {

            var callback;

            if ($attrs.callback) {
                callback = function() {
                    $scope.$parent.$eval($attrs.callback);
                }
            }

            ///////////////////////////////////////

            $scope.settings = {};

            ///////////////////////////////////////

            $element.on('click', function(event) {
                if ($scope.settings.confirming) {
                    if(callback) {
                        return callback();
                    }
                } else {
                    $scope.settings.confirming = true;

                    $timeout(function() {
                        $scope.settings.confirming = false;
                    }, 3000)
                }
            });

            ///////////////////////////////////////


        }

    }
});