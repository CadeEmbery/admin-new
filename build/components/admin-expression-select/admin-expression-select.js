app.directive('expressionSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            editor:'=editor',
        },
        templateUrl: 'admin-expression-select/admin-expression-select.html',
        controller: 'ExpressionSelectController',
    };
});




app.controller('ExpressionSelectController', function($scope, $rootScope) {


    if (!$scope.model || (_.isObject($scope.model) && _.isArray($scope.model))) {
        $scope.model = {};
    }


    //Create a new object
    $scope._new = {}

    ////////////////////////////////////////////////////

    $scope.remove = function(key) {
        delete $scope.model[key];
    }


    ////////////////////////////////////////////////////
    
    $scope.selectExpression = function(path) {

    	delete $rootScope.expressionSelectFunction;

    	if(!$scope._new.value) {
    		$scope._new.value = '';
    	} else {
    		$scope._new.value += ' ';
    	}

    	$scope._new.value += path;


    }

    $scope.picker = function() {
    	$rootScope.expressionSelectFunction = $scope.selectExpression;
    }

    ////////////////////////////////////////////////////


    ////////////////////////////////////////////////////

    $scope.create = function() {
        var insert = angular.copy($scope._new);
        if(!$scope.model) {
            $scope.model = {}
        }
        if(insert.key && !$scope.model[insert.key]) {
            $scope.model[insert.key] = insert.value;
            $scope._new = {}
        }
    }

})
