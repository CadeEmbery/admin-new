app.directive('metadataManager', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'metadata-manager/metadata-manager.html',
        controller: 'MetadataManager',
    };
});



app.controller('MetadataManager', function($rootScope, $uibModal, $scope) {

    ////////////////////////////////////////////////////

    //Create an object if it doesn't exist
    if (!$scope.model) {
        $scope.model = {};
    }

    
    $scope.settings = {};

    $scope._new = {}

    ////////////////////////////////////////////////////

    $scope.removeKey = function(key) {
        console.log('Remove', key)

        delete $scope.model[key];
        // _.set($scope.model, key, null);
        // delete $scope.model.key;
    }

    ////////////////////////////////////////////////////

    $scope.add = function() {

        var pair = $scope._new;

        if(!pair.key || !pair.value) {
            return;
        }

        //Set the value
        _.set($scope.model, pair.key, pair.value);

        //Reset the proposed value
        $scope._new = {};
    }

})