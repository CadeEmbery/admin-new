app.directive('secretInput', function() {
    return {
        restrict: 'E',
        replace:true,
        templateUrl:"secret-input/secret-input.html",
        scope:{
            model:'=ngModel',
        },
        link: function($scope, $element, $attrs) {
          

            $scope.settings = {};


            $scope.$watch('model', function(string) {
                if(!string || !string.length) {
                    $scope.settings.encoded = '';
                } 

                var redacted = _.map(string, function() {
                    return '*'
                }).join('');

                $scope.settings.encoded = redacted;

            })
        }
    };
});

