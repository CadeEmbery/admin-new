app.directive('realmNav', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-realm-nav/admin-realm-nav.html',
        controller: 'RealmNavigationController',
    };
});



app.directive('realmNavItem', function($compile, $templateCache) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            realm: '=ngModel',
        },
        templateUrl: 'admin-realm-nav/admin-realm-nav-item.html',
        link:function($scope, $element, $attrs) {

            var template = '<realm-nav-item ng-model="subRealm" ng-repeat="subRealm in realm.children | orderBy:' +"'title'" + ' track by subRealm._id"></realm-nav-item>';

            var newElement = angular.element(template);
            $compile(newElement)($scope);
            $element.find('.realm-nav-children').append(newElement);
        }
    };
});

////////////////////////////////////////////////////

app.controller('RealmNavigationController', function($scope, $sessionStorage) {

    $scope.$watch('model', function(model) {
        $scope.realms = _.map(model, function(realm) {
            return realm;
        })
    });
    if($sessionStorage['realmNav']) {
        $scope.settings = $sessionStorage['realmNav'];
    } else {
        $scope.settings = $sessionStorage['realmNav'] = {};
    }

    // //////////////////////////////////////////

    // function findParent(parent, realm) {

    //     var exists = _.includes(parent.children, realm);

    //     if(exists) {
    //         return parent;
    //     } else {
    //         findParent(parent.children);
    //     }
    // }

    // //////////////////////////////////////////

    // $scope.back = function() {
    //     var parent = _.chain($scope.realms)
    //     .flattenDeep()
    //     .find({

    //     })
    // }


    // $scope.select = function(realm) {

    //     $scope.settings.selectedRealm = realm;
    // }

});