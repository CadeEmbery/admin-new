

app.controller('FluroReactModalController', function($scope, cancel, Batch, reactions, ids) {

	$scope.reactions = reactions;
	$scope.ids = ids;


	$scope.selected = {};
	$scope.options = {};

	//////////////////////////////////////

	$scope.spark = function() {
		Batch.spark({
			ids:ids,
			reaction:$scope.selected.reaction,
			options:$scope.options,
		}, function() {

			//Close the window
			$scope.cancel();
		})
	}
	//////////////////////////////////////




	$scope.cancel = cancel

});
