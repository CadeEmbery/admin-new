app.directive('notificationList', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {

        },
        templateUrl: 'admin-notification-list/admin-notification-list.html',
        controller: 'NotificationListController',
    };
});


app.controller('NotificationListController', function($scope, $state,$timeout,  $rootScope, FluroAccess, FluroContent, ServerNotificationService) {



    $rootScope.$on('notifications.refreshed', function() {

        $timeout(function() {

            console.log('notification list - Notifications refreshed was fired')
        $scope.notifications = ServerNotificationService.items;

  })
    })

    /////////////////////////////////////////////////////

    $scope.collect = function() {
        _.each(ServerNotificationService.items, function(notification) {
            notification.collected = true;
        });

        //Tell the server to update
        ServerNotificationService.collect();
    }

    /////////////////////////////////////////////////////
    $scope.getIcon = function(notification) {
        switch (notification.type) {
            case 'assignment':
                return 'far fa-event';
                break;
            default:
                return 'far fa-' + notification.type;
                break;
        }
    }
    /////////////////////////////////////////////////////

    $scope.clicked = function(notification) {

        console.log('CLICKED', notification)

        /////////////////////////////////////////////////////

        //If the notification hasn't been read yet
        if (!notification.read) {

            //Mark as read
            notification.read = true;
            notification.collected = true;

            //And tell the server it has been read
            FluroContent.endpoint('notifications/read/' + notification._id)
                .get()
                .$promise
                .then(function(res) {
                    console.log('Read');
                    $rootScope.$broadcast('notifications.refreshed', notification);
                }, function(err) {
                    console.log('Error reading notification', err);
                })
        }

        /////////////////////////////////////////////////////

        var contentID = _.get(notification, 'additionalData.content');

        /////////////////////////////////////////////////////

        switch (notification.type) {
            default:
                FluroContent.resource('get/' + contentID).get().$promise.then(function(item) {
                    var type = item._type;
                    var definition = item.definition;
                    var definedName = definition || type;

                    switch (type) {
                        case 'process':

                           
                            if (FluroAccess.canEditItem(item)) {
                                 console.log('CAN EDIT ITEM')
                                $state.go(type + '.edit', {
                                    id: item._id,
                                    definitionName: definedName
                                });
                            } else {
                                console.log('CAN ONLY ITEM')
                                $state.go(type + '.view', {
                                    id: item._id,
                                    definitionName: definedName
                                });
                            }

                            break;
                        default:
                            $state.go(type + '.view', {
                                id: item._id,
                                definitionName: definedName
                            });
                            break;
                    }

                })
                break;
        }

    }
})