app.directive('adminPlanView', function(ModalService) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            item: '=ngModel',
            event: '=ngEvent',
            confirmations:'=ngConfirmations',
        },
        templateUrl: 'fluro-admin-content/types/plan/view.html',
        link:function($scope, $element, $attributes) {
            $scope.canEditPlan = true;

            $scope.editPlan = function(plan) {
                console.log('Plan', plan)
                ModalService.edit($scope.item);
            }
        }


        //controller: 'PlanViewController',
    };
});



app.controller('AdminPlanViewController', function($scope, ModalService) {

    if($scope.item.event) {
        $scope.event = $scope.item.event;

        if(!$scope.item.startDate) {
            $scope.item.startDate = $scope.item.event.startDate;
        }
    }



    $scope.item.showTeamOnSide = true;
    
    /////////////////////////////////////////////

    $scope.contexts = []

    /////////////////////////////////////////////

    $scope.enablePrinting = function(assignmentGroup) {        
        var assignments = _.filter(assignmentGroup.assignments, function(assignment) {
            return (assignment.confirmationStatus == 'unknown' || assignment.confirmationStatus == 'confirmed');
        });

        return assignments.length;
    }

    /////////////////////////////////////////////
    /////////////////////////////////////////////


    var rosterSource;

    /////////////////////////////////////////////

    if($scope.extras && $scope.extras.rosters) {
        rosterSource = $scope.extras.rosters;
    } else {
        rosterSource = _.get($scope, 'event.rostered'); 
    }

    /////////////////////////////////////////////

    $scope.rosters = _.chain(rosterSource)
    .map(function(roster) {

        roster.slots = _.filter(roster.slots, function(slot) {
            return slot.assignments && slot.assignments.length;
        });
        
        return roster;
    
    })
    .filter(function(roster) {
        return roster.slots && roster.slots.length;
    })
    .value();

    // .filter(function(roster) {
    //     return _.some(roster.slots, function(slot) {
    //         return slot.assignments && slot.assignments.length;
    //     })
    // });


    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////

    /**/
    //DEPRECATED ASSIGNMENTS
    var assignmentSource;


    if($scope.extras && $scope.extras.assignments) {
        assignmentSource = $scope.extras.assignments;
    } else {
        assignmentSource = _.get($scope, 'event.assigned');
    }


    $scope.assignmentGroups = _.chain(assignmentSource)
    .reduce(function(results, assignment) {

        var _id = assignment.title.toLowerCase();
        //Check if the group exists
        var existing = _.find(results, {_id:_id});

        //Create the group if it doesn't exist
        if(!existing) {
            existing = {
                _id:_id,
                title:assignment.title,
                assignments:[],
            }

            results.push(existing);
        }

        //Add this assignment to the group
        existing.assignments.push(assignment);

        existing.assignments = _.sortBy(existing.assignments, function(assignment) {
            return assignment.contact.title;
        })

        return results;
    },[])
    .value();

    /**/

    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////

    $scope.clicked = function(scheduleItem) {

        if(scheduleItem.links && scheduleItem.links.length) {
            if(scheduleItem.links.length == 1) {
                ModalService.view(scheduleItem.links[0]);
            }
        }

    }

    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////

    /**
    $scope.assignments = _.chain($scope.event.assigned)
    .reduce(function(results, assignment) {
        
        // console.log('Assignment', assignment);

        var existing = _.find(results, {title:assignment.title});

        if(!existing) {
            existing = {
                title:assignment.title,
                assignments:[]
            }

            results.push(existing);
        }

        existing.assignments.push(assignment);

        return results;

    }, [])
    .sortBy(function(group) {
        return group.title;
    })
    .value();

    /**/


    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////

    $scope.toggleContext = function(val) {

        if (_.contains($scope.contexts, val)) {
            _.pull($scope.contexts, val);
        } else {
            $scope.contexts.push(val)
        }
    }

    $scope.contextIsActive = function(val) {


        return _.contains($scope.contexts, val);
    }


    /////////////////////////////////////////////

    $scope.isConfirmed = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = (contact._id == confirmation.contact._id);
            return correctAssignment && correctContact && confirmation.status == 'confirmed';
        })
    }

    /////////////////////////////////////////////

    $scope.isUnavailable = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = (contact._id == confirmation.contact._id);
            return correctAssignment && correctContact && confirmation.status == 'denied';
        })
    }

    /////////////////////////////////////////////

    $scope.isUnknown = function(contact, assignment) {
        return !$scope.isConfirmed(contact, assignment) && !$scope.isUnavailable(contact, assignment);
    }

    /////////////////////////////////////////////

    $scope.hasCalculatedTime = function(target) {
        return true;
        
        var startDate = $scope.item.startDate;
        var schedules = $scope.item.schedules;

        var index = schedules.indexOf(target);
        var previousSchedule = schedules[index-1];

        if(previousSchedule.duration) {
            return true;
        }
    }

    $scope.estimateTime = function(startDate, schedules, target) {

        if (target) {
            var total = 0;
            var foundAmount = 0;

            var index = schedules.indexOf(target);

            _.every(schedules, function(item, key) {
                //No item so return running total
                if (!item) {
                    foundAmount = total;
                } else {
                    //Increment the total
                    if (item.duration) {
                        total += parseInt(item.duration);
                        foundAmount = (total - item.duration);
                    } else {
                        foundAmount = total;
                    }
                }

                return (index != key)
            })


            var laterTime = new Date(startDate);

            var milliseconds = foundAmount * 1000;
            laterTime.setTime(laterTime.getTime() + milliseconds);

            return laterTime;
        } else {
            return;
        }
    }



    $scope.isBlankColumn = function(team) {


        var hasContent = _.chain($scope.item.schedules)
            .any(function(schedule) {
                if(!schedule) {
                    return false;
                }

                if (!schedule.notes) {
                    return false;
                }
                if (schedule.notes[team] && schedule.notes[team].length) {
                    return true;
                } else {
                    return false;
                }
            })
            .value();

        return !hasContent;
    }
});