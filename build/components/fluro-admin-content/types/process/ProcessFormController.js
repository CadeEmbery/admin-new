app.controller('ProcessFormController', function($scope, $rootScope, $filter, Notifications, TypeService, Fluro, FluroContent) {

    /**
	$scope.definedTypes = TypeService.definedTypes;
    $scope.types = TypeService.types;
	

	$scope.$watch('item', function(item) {
		if(!$scope.item.types) {
			$scope.item.types = [];
		}
	})

		/**/


    if($scope.extras.reference) {
        $scope.item.item = $scope.extras.reference;
    }
    /////////////////////////////////////////////////////////////

    var states = _.get($scope.definition, 'data.states');

    if(!$scope.item._id) {
        $scope.item.state = states[0].key;
    }

    /////////////////////////////////////////////////////////////
    // $scope.getReference = function() {

    
    
    /////////////////////////////////////////////////////////////

    $scope.$watch('definition.data.processTypes', function(types) {

        // if(definition.data && definition.data.processTypes) {
        $scope.selectable = {
                types: types
            }
            // }
    });


    /////////////////////////////////////////////////////////////

    $scope.resendForm = function(formRow) {
        formRow.processing = true;


        console.log('FORM', formRow);
        var cardID = $scope.item._id;
        var formID = _.get(formRow, 'form._id')  || _.get(formRow, 'form');

        FluroContent.endpoint('process/resend/' + cardID + '/' + formID).update().$promise.then(function(res) {
            console.log('Results!')
            Notifications.status('Form has been resent');
            formRow.processing = false;
        }, function(err) {
            console.log('Error', err);

            Notifications.error('There was an error resending the form');
            formRow.processing = false;
        });
        // router.put('/resend/:cardID/:formID', processEditMiddleware, function(req, res) {
    }

    /////////////////////////////////////////////////////////////

    $scope.awaitingForms = _.filter($scope.item.forms, function(entry) {
        return !entry.interaction;
    })

    $scope.receivedForms = _.filter($scope.item.forms, function(entry) {
        return entry.interaction;
    })

    /////////////////////////////////////////////////////////////

    $scope.$watch('item.item', function(reference) {
        if (reference) {
            var name = $filter('capitalizename')(reference.title);

            //Update the title
            // var string = name + ' - ' + $scope.definition.title;
            $scope.item.title = name;;
        }

    })

    //////////////////////////////////////////////////////////

    $scope.$watch('item.taskLists.length', refreshTaskLists);

    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    function refreshTaskLists() {

        console.log('Get task Lists');



        //////////////////////////////////////////////////////////


        $scope.groupedTasks = _.chain(states)
            .reduce(function(results, state) {
                var key = state.key;

                //Get all task lists for this state
                var taskLists = _.filter($scope.item.taskLists, {
                    state: key
                });

                if (!taskLists || !taskLists.length) {
                    return results;
                }

                ////////////////////////////

                //Add the task lists
                results.push({
                    state: state,
                    lists: taskLists,
                });

                ////////////////////////////

                return results;
            }, [])
            .value();
    }


    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    $scope.addTaskList = function() {
        // console.log('Add Task List');
        if (!$scope.item.taskLists) {
            $scope.item.taskLists = [];
        }

        ///////////////////////////////////////

        var startingStateName = $scope.item.state;

        
        ///////////////////////////////////////

        if(!startingStateName) {
            startingStateName = states[0].key;
        }
        
        var actualState = _.find(states, function(state) {
            return state.key == startingStateName;
        })

        ///////////////////////////////////////

        var name = actualState.title + ' tasks';
        var existingStateList = _.find($scope.item.taskLists, function(list) {
            return list.title == name && list.state == startingStateName;
        })

        //If there isnt a list for this state yet
        //then add one now
        if (!existingStateList) {
            $scope.item.taskLists.push({
                title: name,
                adding: true,
                state: startingStateName,
                tasks: [],
            });
            return;
        }


        //Create a list for the individual
        name = $rootScope.user.firstName + "'s tasks";
        var existingNameList = _.find($scope.item.taskLists, function(list) {
            return list.title == name;
        })

        ///////////////////////////////////////

        //Focus on the existing list
        if (existingNameList) {
            existingNameList.adding = true;
            return;
        }

        //Create the new list

        $scope.item.taskLists.push({
            title: name,
            adding: true,
            tasks: [],
        })




    }

    $scope.addTask = function(list) {
        if (!list.tasks) {
            list.tasks = [];
        }

        list.tasks.push({});
    }

});