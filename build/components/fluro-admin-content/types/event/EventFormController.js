app.controller('EventFormController', function($scope, $state, $q, $timeout, FluroContent, TypeService, FluroAccess, Notifications, $stateParams, ModalService, DateTools) {

    $scope.ts = {}


    //////////////////////////////////////////

    $scope.showPastWarning = function() {

        if ($scope.item._id) {
            return;
        }

        var endDate = new Date($scope.item.endDate);
        var now = new Date();

        if (now > endDate) {
            return true;
        }

    }

    //////////////////////////////////////////////////

    $scope.unlinkPlan = function(plan) {
        console.log('Unlink plan', plan)
        FluroContent.endpoint('event/' + $scope.item._id + '/plans/' + plan._id).delete().$promise.then(function(res) {
            console.log('Plan Unlinked!')

            _.pull($scope.item.plans, plan);
        }, function(err) {
            console.log('ERROR UNLINKING PLAN', err);
        });
    }

    //////////////////////////////////////////////////

    $scope.editPlan = function(plan) {
        ModalService.edit(plan, done);

        /////////////////////////////

        function done(result) {
            _.assign(plan, result);
        }
    }

    //////////////////////////////////////////////////

    $scope.currentTimezone = moment.tz.guess();


    //////////////////////////////////////////////////

    $scope.canDefineRoster = FluroAccess.can('create', 'definition');
    $scope.defineNewRoster = function() {


        var params = {
            template: {
                parentType: 'roster',
            }
        }


        ModalService.create('definition', params, function(res) {


            //Update the view
            // update();
        });
    }



    //////////////////////////////////////////


    $scope.canAssign = function() {
        return FluroAccess.canAccess('assignment');
    }

    $scope.canRoster = function() {
        return FluroAccess.canAccess('roster');
    }


    ////////////////////////////////////////

    $scope.canEditRoster = function(roster) {

        var definitionName = roster.definition;
        var canAny = FluroAccess.can('edit any', definitionName, 'roster');
        // var canOwn =  FluroAccess.can('edit own', definitionName, 'roster');

        // console.log('CHECK', can);
        return canAny;
    }

    ////////////////////////////////////////

    $scope.canDeleteRoster = function(roster) {
        return FluroAccess.canDeleteItem(roster);
    }

    ////////////////////////////////////////

    $scope.deleteRoster = function(roster) {

        var itemObject = {
            _id: roster._id,
            _type: 'roster',
            definition: roster.definition,
            realms: $scope.item.realms,
        }

        /////////////////////////////

        ModalService.remove(itemObject, done);

        /////////////////////////////

        function done(result) {
            _.pull($scope.item.rostered, result);
        }
    }

    ////////////////////////////////////////

    $scope.editRoster = function(roster) {

        console.log('ROSTER', roster);

        var itemObject = {
            _id: roster._id,
            _type: 'roster',
            definition: roster.definition,
            realms: $scope.item.realms,
        }

        /////////////////////////////

        ModalService.edit(itemObject, done);

        /////////////////////////////

        function done(result) {

            _.assign(roster, result);

            // var isModal = $scope.$eval('$close');

            //  if(isModal) {
            //      return;
            //  }
        }
    }

    ////////////////////////////////////////

    $scope.$watch('item.rosters', refreshRosters, true);
    $scope.$watch('item.rostered', refreshRosters, true);

    //////////////////////////////////////////

    $scope.$watch(function() {
        return TypeService.definedTypes;
    }, function() {
        $scope.rosterTypes = _.chain(TypeService.definedTypes)
            .filter(function(definition) {
                return definition.parentType == 'roster';
            })
            .value();
    }, true);

    //////////////////////////////////////////////////////

    $scope.canCreateRoster = function(rosterType) {
        var alreadyCreated = _.some($scope.allRosters, {
            definition: rosterType.definitionName
        });

        if (alreadyCreated) {
            return false;
        }

        var definitionName = rosterType.definitionName;

        return FluroAccess.can('create', definitionName, 'roster');
    }

    //////////////////////////////////////////////////////

    function refreshRosters() {

        var rostered = $scope.item.rostered;
        var rosters = $scope.item.rosters;

        if (!rostered) {
            rostered = [];
        }

        if (!rosters) {
            rosters = [];
        }

        ///////////////////////////////////////

        var allRosters = _.chain([].concat(rosters, rostered))
            .compact()
            .reduce(function(results, roster) {

                var existing = _.find(results, {
                    _id: roster._id
                });

                if (!existing) {
                    existing = {
                        ref: roster,
                        _id: roster._id,
                        title: roster.title,
                        realms: roster.realms,
                        _type: roster._type,
                        definition: roster.definition,
                        slots: [],
                    }

                    results.push(existing);
                }


                //Add the slots in
                existing.slots = _.compact(existing.slots.concat(roster.slots));

                return results;




            }, [])
            .value();

        ///////////////////////////////////////

        $scope.allRosters = allRosters;

        ///////////////////////////////////////

        $scope.allUnconfirmed = _.chain(allRosters)
            .map('slots')
            .flatten()
            .map('assignments')
            .flatten()
            .filter({
                confirmationStatus: 'unknown'
            })
            .value()

        ///////////////////////////////////////

        $scope.reminderSlots = _.chain(allRosters)
        .map('slots')
        .flatten()
        .compact()
        .uniq(function(slot) {
            return slot.title;
        })
        .value();

    }

    //////////////////////////////////////////

    $scope.nudging = false;

    $scope.nudge = function() {

            $scope.nudging = true;

            //////////////////////////////////////////

            FluroContent.endpoint('assignments/nudge/event/' + $scope.item._id).save()
                .$promise
                .then(function(res) {
                    $scope.nudging = false;

                    //console.log('Nudged everyone!', res);
                    if (res.success.length) {
                        Notifications.status(res.success.length + ' nudge notifications sent');
                    } else {
                        Notifications.warning('No nudges were sent');
                    }

                }, function(err) {
                    $scope.nudging = false;
                    //console.log('Error', err);

                    Notifications.error(err.message);
                })



        }
        //////////////////////////////////////////

    /**
    
	//////////////////////////////////////////

    //Check if the user provided an init date upon creation
    //var initDate = $stateParams.initDate;
    if (!$scope.item._id) {
        if ($scope.definition && $scope.definition.data) {
            if ($scope.definition.data.defaultAssignments) {
                $scope.item.assignments = angular.copy($scope.definition.data.defaultAssignments);
            }
        }
    }

    


    //////////////////////////////////////////

    /**/


    // $scope._proposed = {};
    // $scope.ticketOptions = [];

    // //////////////////////////////////////////
    // //////////////////////////////////////////

    // $scope.addTicketOption = function() {

    //     var ticketOption = angular.copy($scope._proposed.ticket);

    //     if(!$scope.item.ticketing) {
    //         $scope.item.ticketing = [];
    //     }

    //     $scope.item.ticketing.push(ticketOption);

    //     $scope._proposed.ticket = {};
    // }

    // //////////////////////////////////////////

    // $scope.removeTicketOption = function(ticketOption) {

    //     _.pull($scope.item.ticketing, ticketOption);
    // }


    //////////////////////////////////////////



    var initDate;

    /**/
    //If there are modal params
    if ($stateParams.initDate) {
        initDate = $stateParams.initDate;
    }

    if (initDate) {

        if (_.isString(initDate)) {
            initDate = new Date(parseInt(initDate)); //Number(initDate);
        }

        var initStart = new Date(initDate);
        var initEnd = new Date(initDate);

        initStart.setHours(13);
        initStart.setMinutes(initStart.getMinutes() + 30);
        initStart.setMinutes(0);

        initEnd.setHours(13);
        initEnd.setMinutes(initEnd.getMinutes() + 30);
        initEnd.setMinutes(0);




        if (!$scope.item.startDate) {
            $scope.item.startDate = initStart;
            $scope.item.endDate = initEnd;
        }

        // console.log('INIT START DATE', initStart);
    }
    /**/




    //////////////////////////////////////////

    $scope.confirmations = $scope.extras.confirmations;

    //////////////////////////////////////////

    var currentDate1;
    var currentDate2;

    if ($scope.item.startDate) {
        //Use end date if the dates are different when we open an event
        currentDate1 = new Date($scope.item.startDate);
        currentDate2 = new Date($scope.item.endDate);
    }


    if (currentDate1 && currentDate2 && (currentDate1.getTime() != currentDate2.getTime())) {
        $scope.ts.useEndDate = true;
    }


    //Make sure the end date is never earlier than the start date
    $scope.$watch('item.startDate + ts.useEndDate', function(data) {

        //Compare the dates
        var date1 = new Date($scope.item.startDate);
        var date2 = new Date($scope.item.endDate);

        //If the user does not want to use an end date set it the same as the start date
        if (!$scope.ts.useEndDate) {
            $scope.item.endDate = $scope.item.startDate;
        }

        //Update the end time if its earlier than the start time
        if (date2.getTime() < date1.getTime()) {
            $scope.item.endDate = new Date(date1)
        }

    });

    //////////////////////////////////////////
    //////////////////////////////////////////

    $scope.$watch('item.startDate +  item.endDate + item.checkinData.checkinStartOffset + item.checkinData.checkinEndOffset', function() {

            var startDate = $scope.item.startDate;
            var endDate = $scope.item.endDate;

            if (!$scope.item.checkinData) {
                $scope.item.checkinData = {
                    checkinStartOffset: 90,
                    checkinEndOffset: 90,
                }
            }

            ///////////////////////////////////////
            ///////////////////////////////////////
            ///////////////////////////////////////

            var startOffset;
            if($scope.item.checkinData.checkinStartOffset=== undefined || $scope.item.checkinData.checkinStartOffset === null || isNaN($scope.item.checkinData.checkinStartOffset)) {
                startOffset = 90;
            } else {
                console.log('This is it?', $scope.item.checkinData.checkinStartOffset);

                startOffset = parseInt($scope.item.checkinData.checkinStartOffset);
            }

            ///////////////////////////////////////

            var endOffset;
            if($scope.item.checkinData.checkinEndOffset=== undefined || $scope.item.checkinData.checkinEndOffset === null || isNaN($scope.item.checkinData.checkinEndOffset)) {
                endOffset = 90;
            } else {
                endOffset = parseInt($scope.item.checkinData.checkinEndOffset);
            }

            ///////////////////////////////////////
            ///////////////////////////////////////
            ///////////////////////////////////////

            $scope.checkinStartDate = moment(startDate).subtract(startOffset, 'minutes');
            $scope.checkinEndDate = moment(endDate).add(endOffset, 'minutes');
        })
        //////////////////////////////////////////


    /*

    //////////////////////////////////////////

    var today = new Date();

    var currentDate1 = new Date(today.getTime());
    var currentDate2 = new Date(today.getTime());

    if ($scope.item.startDate) {
        //Use end date if the dates are different when we open an event
        currentDate1 = new Date($scope.item.startDate);
        currentDate2 = new Date($scope.item.endDate);
    }

    if (currentDate1.getTime() != currentDate2.getTime()) {
        $scope.ts.useEndDate = true;
    }


	

    //////////////////////////////////////////

    //Include the readable date range function
    $scope.readableDateRange = DateTools.readableDateRange;

/**/


    $scope.createPlanFromTemplate = function() {

        var selection = {};
        var modal = ModalService.browse('plan', selection);
        modal.result.then(addPlan, addPlan)

        function addPlan() {
            // console.log('Select Plans', $scope.item);
            if (selection.items && selection.items.length) {

                //Create an array if there isnt one already
                if (!$scope.item.plans || !$scope.item.plans.length) {
                    $scope.item.plans = [];
                }

                /////////////////////////////////////////////////////////////////

                //Get the template
                var template = selection.items[0];
                template.event = $scope.item;

                console.log('Add Plan from template', template)

                var createCopy = true;
                // var createFromTemplate = true;


                ////////////////////////////////////////

                function planCreateSuccess(copyResult) {
                    console.log('Copy Result!', copyResult);
                    if (copyResult) {
                        $scope.item.plans.push(copyResult)
                    }
                }

                function planCreateFail(err) {
                    console.log('Plan Failed', err);
                }

                ////////////////////////////////////////

                ModalService.edit(template, planCreateSuccess, planCreateFail, createCopy, template, $scope);

            }
        }

    }


    $scope.createPlanFromBlank = function() {
        console.log('Woooot');


        var params = {
            template: {
                event: $scope.item,
                startDate: $scope.item.startDate,
                realms: $scope.item.realms,
            }
        }

        ///////////////////////////////////////////////

        function planCreateSuccess(copyResult) {
            console.log('Copy Result!', copyResult);
            if (copyResult) {
                $scope.item.plans.push(copyResult)
            }
        }

        function planCreateFail(err) {
            console.log('Plan Failed', err);
        }

        ///////////////////////////////////////////////

        ModalService.create('plan', params, planCreateSuccess, planCreateFail)

    }



    //////////////////////////////////////////////////

    $scope.createDefinedRoster = function(rosterType) {


        var params = {
            template: {
                title: rosterType.title,
                event: $scope.item,
                // startDate:$scope.item.startDate,
                // realms:$scope.item.realms,
            }
        }


        function rosterCreateSuccess(copyResult) {
            console.log('NEW ROSTER Result!', copyResult);
            if (copyResult) {
                if (!$scope.item.rosters) {
                    $scope.item.rosters = [];
                }
                $scope.item.rosters.push(copyResult)
            }
        }

        /**
        ///////////////////////////////////////////////

        function rosterCreateSuccess(copyResult) {
            // console.log('NEW ROSTER Result!', copyResult);
            if (copyResult) {

                if (!$scope.item.rosters) {
                    $scope.item.rosters = [];
                }
                $scope.item.rosters.push(copyResult)

                ///////////////////////////////////////

                var typeName = 'roster';
                if(copyResult.definition) {
                    typeName = copyResult.definition;
                }

                copyResult.processing = true;

                $timeout(function() {

                    console.log('LOADING');
                    FluroContent.resource(typeName, true, true).get({
                        id:copyResult._id
                    }).$promise.then(function(roster) {

                        console.log('LOADED');
                        copyResult.processing = false;
                        copyResult.slots = roster.slots;
                    })

                }, 1000)
                
            }
        }
        /**/

        function rosterCreateFail(err) {
            console.log('Roster Failed', err);
        }

        ///////////////////////////////////////////////

        ModalService.create(rosterType.definitionName, params, rosterCreateSuccess, rosterCreateFail)

    }

    $scope.createRosterFromTemplate = function() {

        var selection = {};
        var modal = ModalService.browse('roster', selection);
        modal.result.then(addRoster, addRoster)

        function addRoster() {
            // console.log('Select Plans', $scope.item);
            if (selection.items && selection.items.length) {

                //Create an array if there isnt one already
                if (!$scope.item.rosters || !$scope.item.rosters.length) {
                    $scope.item.rosters = [];
                }

                /////////////////////////////////////////////////////////////////

                //Get the template
                var template = selection.items[0];
                template.event = $scope.item;

                console.log('Add Roster from template', template)

                var createCopy = true;
                // var createFromTemplate = true;


                ////////////////////////////////////////

                function rosterCreateSuccess(copyResult) {
                    console.log('Copy Result ADD!', copyResult);
                    if (copyResult) {
                        if (!$scope.item.rosters) {
                            $scope.item.rosters = [];
                        }
                        $scope.item.rosters.push(copyResult)
                    }
                }

                function rosterCreateFail(err) {
                    console.log('Plan Failed', err);
                }

                ////////////////////////////////////////

                ModalService.edit(template, rosterCreateSuccess, rosterCreateFail, createCopy, template, $scope);

            }
        }

    }


    $scope.createRosterFromBlank = function() {


        var params = {
            template: {
                event: $scope.item,
                // startDate:$scope.item.startDate,
                // realms:$scope.item.realms,
            }
        }

        ///////////////////////////////////////////////

        function rosterCreateSuccess(copyResult) {
            console.log('Copy Result!', copyResult);
            if (copyResult) {
                if (!$scope.item.rosters) {
                    $scope.item.rosters = [];
                }
                $scope.item.rosters.push(copyResult)
            }
        }

        function rosterCreateFail(err) {
            console.log('Roster Failed', err);
        }

        ///////////////////////////////////////////////

        ModalService.create('roster', params, rosterCreateSuccess, rosterCreateFail)

    }


    //////////////////////////////////////////////////


    function saveTemporaryAssignments(eventID, callback) {

        // console.log('SAVE TEMPORARY ASSIGNMENTS FIRST', eventID);

        var array = _.get($scope.item, 'temporaryAssignments');

        if (!array) {
            //No assignments
            return callback();
        }

        //////////////////////////////////////////////////


        var tempAssignments = array.slice(0);

        //////////////////////////////////////////////////

        //Map the requests
        var requests = _.map(tempAssignments, function(data) {
            var copy = angular.copy(data);
            copy.confirmationStatus = 'unknown';

            var promise = FluroContent.endpoint('assignments/' + eventID).save(copy).$promise;

            //Listen for when its done
            promise.then(function(res) {


                data._id = res._id;
                data.confirmationStatus = res.confirmationStatus;
                data.realms = res.realms;
                data.owners = res.owners;
                data.managedOwners = res.managedOwners;
                data.author = res.author;
                data.managedAuthor = res.managedAuthor;
                data._type = res._type;
                data.definition = res.definition;

                //Pull from temporary
                _.pull($scope.item.temporaryAssignments, data);

                //Add to the assigned list
                if (!$scope.item.assigned) {
                    $scope.item.assigned = [];
                }

                $scope.item.assigned.push(data)
            })


            return promise;
        });

        //////////////////////////////

        $q.all(requests)
            .then(function(res) {
                // console.log('RESULT', res);

                return callback();
            }, function(err) {

                if (err.data && err.data.errors && err.data.errors.contact) {
                    return Notifications.error(err.data.errors.contact.message);
                }
                // if(err.data) {
                //     if(err.data.message) {
                //         Notifications.error(err.data.message);
                //     } else {
                //         Notifications.error(err.data);
                //     }
                // } else {
                Notifications.error(err);
                // }


            });
    }

    //////////////////////////////////////////////////

    //Override the function that is usually fired on save success
    $scope.saveSuccess = function(res) {

        console.log('SAVE SUCCESS', res);

        if ($scope.item._id) {
            return $scope.contentSaveSuccess(res);
        }

        ////////////////////////////////////////////

        //Save the temporary assignments
        saveTemporaryAssignments(res._id, function() {

            console.log('Temporary assignments saved')
                //Once all are saved then run the usual contentSave function
            $scope.contentSaveSuccess(res);
        });

    };

    //////////////////////////////////////////////////

    $scope.save = function(options) {

            if (!$scope.item._id) {
                console.log('No id so save as normal')
                return $scope.contentSave(options);
            }

            if (!$scope.item.temporaryAssignments || !$scope.item.temporaryAssignments.length) {
                console.log('no assignments so save as normal')
                return $scope.contentSave(options);
            }

            ////////////////////////////////////////////

            console.log('Save temp assignments')
                //Save the temporary assignments
            saveTemporaryAssignments($scope.item._id, function() {

                //Once all are saved then run the usual contentSave function
                $scope.contentSave(options);
            });
        }
        //////////////////////////////////////////////////
        //////////////////////////////////////////////////
        //////////////////////////////////////////////////
        //////////////////////////////////////////////////
        //////////////////////////////////////////////////
        //////////////////////////////////////////////////



    /*
    //////////////////////////////////////////////////


    $scope.saveFinished = function(result) {

        if (!$scope.item._id) {
            if (result._id) {

                _.assign($scope.item, result);

                //$scope.item._id = result._id;

                $scope.saveText = 'Save';
                $scope.cancelText = 'Close';
            }

            $scope.closeOnSave = true;
            return true;
        } else {

            console.log('Close like normal')
            if ($scope.$close) {
                return $scope.$close(result)
            } else {
                return false;
            }
        }
    }

    /*
    //Save
    $scope.save = function(options) {

        //TODO Disable Socket     

        if (!options) {
            options = {}
        }
        //////////////////////////////////////////

        //Use save options

        if (options.status) {
            status = options.status;
        }

        if (options.exitOnSave) {
            saveSuccessCallback = exitAfterSave;
        }

        //////////////////////////////////////////

        if ($scope.item._id) {
            //Edit existing user
            FluroContent.resource($scope.definedType).update({
                id: $scope.item._id,
            }, $scope.item, $scope.saveSuccess, $scope.saveFail);
        } else {

            console.log('SAVE SUCCESS STAY');
            //Saving a new event so stay and dont disappear
            FluroContent.resource($scope.definedType).save($scope.item, $scope.saveSuccessStay, $scope.saveFail);
        }


    }


    //////////////////////////////////////////////////

    $scope.saveSuccessStay = function(result) {
        console.log('Stay please')
        //Clear cache
        CacheManager.clear($scope.definedType);

        if (result._id) {
            $scope.item.__v = result.__v;
            $scope.item._id = result._id;
        }


        //TODO Reenable Socket
        Notifications.status(result.title + ' saved successfully')
        
        return false;

       
        if ($scope.$close) {
            return $scope.$close(result)
        } else {
            console.log('Return false modal')
            return false;
        }
        
    }

    */

    //////////////////////////////////////////


})