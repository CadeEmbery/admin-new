app.controller('EventViewController', function($scope, $state, $parse, FluroAccess, Fluro, $filter, $window, Notifications, DateTools, ModalService, FluroContent, ModalService) {




    // FluroContent.endpoint('checkin/event/' + $scope.item._id).query({all:true}).$promise.then(function(res) {
    //     $scope.checkins = res;
    // });

    ////////////////////////////////////////

    $scope.canEditRoster = function() {
        return FluroAccess.can('edit', 'roster');
    }

    ////////////////////////////////////////

    $scope.readableDate = DateTools.readableDateRange;

    ////////////////////////////////////////

    $scope.editRoster = function(roster) {

        var itemObject = {
            _id: roster._id,
            _type: 'roster',
            realms: $scope.item.realms,
        }

        /////////////////////////////

        ModalService.edit(itemObject, done);

        /////////////////////////////

        function done(result) {

            _.assign(roster, result);

            // var isModal = $scope.$eval('$close');

            //  if(isModal) {
            //      return;
            //  }
            //  $state.reload();
        }
    }

    ////////////////////////////////////////



    ////////////////////////////////////////

    $scope.groupedAssignments = _.chain($scope.item.assigned)
        .filter(function(assignment) {
            return assignment.status == 'active';
        })
        .sortBy('title')
        .groupBy(function(assignment) {
            return assignment.title;
        })
        .map(function(group, key) {

            return {
                title: key,
                assignments: group,
            }
        })
        .value();


    $scope.unavailable = _.chain($scope.item.assigned)
        .filter(function(assignment) {
            return (assignment.confirmationStatus == 'denied');
        })
        .reduce(function(results, assignment) {

            var contactID = assignment.contact;
            if (contactID._id) {
                contactID = contactID._id;
            }

            var existing = _.find(results, {
                _id: contactID
            });

            if (!existing) {
                existing = {
                    _id: contactID,
                    title: assignment.contact.title,
                    // title:assignment.title,
                    reasons: [],
                }

                results.push(existing);
            }


            // if(_.some(existing.reasons, {title:assignment.title})) {
            //Add the reason

            var description = 'unavailable';
            if (assignment.description) {
                description = assignment.description;
            }
            existing.reasons.push({
                title: assignment.title,
                description: description
            });
            // }




            return results;
        }, [])
        .value();


    $scope.viewEvent = function() {
        ModalService.view(item.event);
    }

    /////////////////////////////////////////////

    $scope.contexts = [];

    /////////////////////////////////////////////


    if ($scope.extras) {

        $scope.guests = _.map($scope.extras.guests, function(contact) {
            if (contact.attendance.checkin) {
                contact.class = 'success';
                return contact;
            }

            if (contact.attendance.guestConfirmed) {
                contact.class = 'success';
                return contact;
            }

            if (contact.attendance.guestDeclined) {
                contact.class = 'danger';
                return contact;
            }

            if (contact.attendance.ticket) {
                contact.class = 'warning';
                return contact;
            }

            return contact;
        });


        /////////////////////////////////////////////


        $scope.checkins = $scope.extras.checkins;
        $scope.confirmations = $scope.extras.confirmations;
        $scope.attendance = $scope.extras.attendance;

        //Confirmations
        $scope.unavailableList = _.filter($scope.confirmations, function(con) {
            return con.status == 'denied';
        })

        $scope.confirmedList = _.filter($scope.confirmations, function(con) {
            return con.status == 'confirmed';
        })

        if ($scope.checkins && $scope.checkins.length) {
            $scope.attendanceTabLabel = $scope.checkins.length + ' checkins';
        } else {
            $scope.attendanceTabLabel = 'Attendance';
        }
    }





    /////////////////////////////////////////////

    //Load all registrations
    FluroContent.endpoint('event/' + $scope.item._id + '/interactions').query().$promise.then(function(res) {
        $scope.registrations = res;
    });

    FluroContent.endpoint('tickets/event/' + $scope.item._id).query().$promise.then(function(res) {
        console.log('TICKETS WOO')
        $scope.tickets = res;
    });

    /////////////////////////////////////////////

    $scope.exportTickets = function() {

        $scope.ticketsExporting = true;
        FluroContent.endpoint('tickets/event/' + $scope.item._id + '/csv').get().$promise.then(function(res) {
            console.log('TICKETS EXPORT', res)
            $scope.ticketsExporting = false;

            Notifications.post('Your popup blocker may stop this file from downloading', 'warning');

            var downloadURL = Fluro.apiURL + res.download;

            if (Fluro.token) {
                $window.open(downloadURL + '?access_token=' + Fluro.token);
            } else {
                $window.open(downloadURL);
            }

        }, function(err) {
            console.log('TICKETS EXPORT Error', err)
            $scope.ticketsExporting = false;
        });
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    $scope.guestColumns = [{
            title: 'First Name',
            key: 'firstName',
        },
        {
            title: 'Last Name',
            key: 'lastName',
        },
        {
            title: 'Gender',
            key: 'gender',
        },

        {
            title: 'Age',
            key: 'age',
            orderRenderer: 'number',
        },

        {
            title: '',
            key: 'attendance',
            renderer: 'guestAttendanceIcon'
        },
    ]

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    function refilterGuests() {

        var filtered = $filter('filter')($scope.extras.guests, $scope.search.terms);



        $scope.guestCollections = _.reduce(filtered, function(set, contact) {

            _.each(contact.attendance, function(item, key) {
                if (set[key]) {
                    set[key].contacts.push(contact);
                }
            });

            
            if (!contact.attendance.checkin) {
                set['absent'].contacts.push(contact);
            }

            //If they have rsvpd somehow
            if (!contact.attendance.guestConfirmed && !contact.attendance.guestDeclined) {
                set['maybe'].contacts.push(contact);
            }
        

            return set;
        }, {
            guestConfirmed: {
                title: 'Confirmed RSVP',
                contacts: [],
            },
            maybe: {
                title: 'No RSVP',
                contacts: [],
            },
            guestDeclined: {
                title: 'Declined RSVP',
                contacts: [],
            },
            ticket: {
                title: 'Ticket',
                contacts: [],
            },
            checkin: {
                title: 'Checked In',
                contacts: [],
            },
            absent: {
                title: 'Not Checked In',
                contacts: [],
            },
        })


        //If there are no RSVPS
        if(!$scope.guestCollections['guestConfirmed'].contacts.length && !$scope.guestCollections['guestDeclined'].contacts.length) {
            //Remove all the RSVP columns
            delete $scope.guestCollections['maybe'];
            delete $scope.guestCollections['guestConfirmed'];
            delete $scope.guestCollections['guestDeclined'];
        }



    }




    $scope.$watch('search.terms', refilterGuests);
    refilterGuests();


    /////////////////////////////////////////////

    $scope.toggleContext = function(val) {

        if (_.contains($scope.contexts, val)) {
            _.pull($scope.contexts, val);
        } else {
            $scope.contexts.push(val)
        }
    }

    $scope.contextIsActive = function(val) {


        return _.contains($scope.contexts, val);
    }

    /////////////////////////////////////////////

    $scope.isConfirmed = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = contact._id == confirmation.contact._id;
            return correctAssignment && correctContact && confirmation.status == 'confirmed';
        })
    }

    /////////////////////////////////////////////

    $scope.isUnavailable = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = contact._id == confirmation.contact._id;
            return correctAssignment && correctContact && confirmation.status == 'denied';
        })
    }

    /////////////////////////////////////////////

    $scope.isUnknown = function(contact, assignment) {
        return !$scope.isConfirmed(contact, assignment) && !$scope.isUnavailable(contact, assignment);
    }

    /////////////////////////////////////////////

    $scope.estimateTime = function(startDate, schedules, target) {

        if (target) {
            var total = 0;
            var foundAmount = 0;

            var index = schedules.indexOf(target);

            _.every(schedules, function(item, key) {
                //No item so return running total
                if (!item) {
                    foundAmount = total;
                } else {
                    //Increment the total
                    if (item.duration) {
                        total += parseInt(item.duration);
                        foundAmount = (total - item.duration);
                    } else {
                        foundAmount = total;
                    }
                }

                return (index != key)
            })


            var laterTime = new Date(startDate);



            var milliseconds = foundAmount * 60 * 1000;
            laterTime.setTime(laterTime.getTime() + milliseconds);

            return laterTime;
        } else {
            return;
        }
    }


})