app.controller('EventListController', function($scope, $timeout, $sce, $filter, $state, $stateParams, ModalService, FluroStorage, DateTools) {


    var weekday = [];
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";


    ///////////////////////////////////////////////

    _.each($scope.items, function(item) {
        item.weekday = weekday[new Date(item.startDate).getDay()];
    });

    ///////////////////////////////////////////////

    // console.log('EVENT LIST')
    //Setup Local storage
    var local;
    var session;

    if ($scope.definition) {
        local = FluroStorage.localStorage($scope.definition.definitionName)
        session = FluroStorage.sessionStorage($scope.definition.definitionName)
    } else {
        local = FluroStorage.localStorage($scope.type.path)
        session = FluroStorage.sessionStorage($scope.type.path)
    }


    ///////////////////////////////////////////////

    $scope.resetToday = function() {
        var newDate = startOfDay();
        session.search.browseDate = newDate;
        return $state.go('.', { startDate: newDate }, {
            reload: true
        })
    }

    ///////////////////////////////////////////////


    function isValidDate(d) {
      return d instanceof Date && !isNaN(d);
    }



    ///////////////////////////////////////////////

    $scope.changeDate = function() {

        var d = session.search.browseDate = startOfDay(session.search.browseDate);

        // $scope.updateFilters();
        if ($stateParams.startDate != d) {
            return $state.go('.', { startDate: d }, {
                reload: true
            })
        }
    }
    ///////////////////////////////////////////////

    delete $scope.search.filters.status;

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    $scope.getMarker = function(date, mode) {


        if (mode === 'day') {

            //Get the timestamp of our check date
            var checkTimestamp = new Date(date);
            checkTimestamp.setHours(0, 0, 0, 0);
            checkTimestamp = checkTimestamp.getTime();

            //Search through all items to see if they match any dates
            var match = _.some($scope.items, function(event) {

                var startDate;
                var endDate;

                //Get the start of the day
                if (event.startDate) {
                    startDate = new Date(event.startDate);
                    startDate.setHours(0, 0, 0, 0);
                }

                //Get the start of the day
                if (event.endDate) {
                    endDate = new Date(event.endDate);
                }

                //Get the end of the day
                endDate.setHours(23, 59, 59, 999);

                //Turn into integers
                var checkTimestamp = date.getTime();
                var startTimestamp = startDate.getTime();
                var endTimestamp = endDate.getTime();

                if (checkTimestamp >= startTimestamp && checkTimestamp <= endTimestamp) {
                    return true; //
                }
            })

            if (match) {
                return 'has-event';
            }
        }

        return '';

    };





    ///////////////////////////////////////////////

    //Set to today if not set previously
    if (!session.search.order) {
        session.search.order = 'startDate';

    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    $scope.getGrouped = function(item) {
        var object = _.chain(item.assigned)
            .filter(function(assignment) {
                return assignment.status == 'active';
            })
            .groupBy(function(assignment) {
                return assignment.confirmationStatus;
            })
            .value();

        // console.log('GOT', object, item.assigned);
        return object;
    }

    $scope.getConfirmed = function(item) {
        return $scope.getAssigned(item, 'confirmed');
    }

    $scope.getDenied = function(item) {
        return $scope.getAssigned(item, 'denied');
    }

    $scope.getAssignedContacts = function(item) {
        var results = [];

        _.each(item.assignments, function(ass) {
            _.each(ass.contacts, function(contact) {
                results.push({
                    assignment: ass._id,
                    contact: contact._id,
                })
            });
        });

        return results;
    }

    $scope.getUnknown = function(item) {

        var contacts = $scope.getAssignedContacts(item);
        var results = [];

        if ($scope.extras && $scope.extras.eventConfirmations && $scope.extras.eventConfirmations.length) {

            //Get the confirmation list
            var eventConfirmationObject = _.find($scope.extras.eventConfirmations, {
                _id: item._id
            });

            if (eventConfirmationObject && eventConfirmationObject.confirmations) {

                //Get the confirmations
                var confirmations = eventConfirmationObject.confirmations;

                results = _.filter(contacts, function(contactAss) {
                    return !_.some(confirmations, function(conf) {
                        return conf.contact._id == contactAss.contact;
                    });
                });

            }
        }

        return results;

    }

    ///////////////////////////////////////////////

    $scope.getAssigned = function(item, status) {
        var results = [];

        if ($scope.extras && $scope.extras.eventConfirmations && $scope.extras.eventConfirmations.length) {
            //Get the confirmation list
            var eventConfirmationObject = _.find($scope.extras.eventConfirmations, {
                _id: item._id
            });

            if (eventConfirmationObject && eventConfirmationObject.confirmations) {
                if (status) {
                    results = _.filter(eventConfirmationObject.confirmations, function(conf) {
                        var statusMatch = (conf.status == status);

                        if (!statusMatch) {
                            return false;
                        }

                        var assignment = _.find(item.assignments, function(ass) {
                            var matchId = (ass._id == conf.assignment || ass.title == conf.title);
                            return matchId;
                        });

                        if (!assignment) {
                            return false;
                        }

                        return _.some(assignment.contacts, {
                            _id: conf.contact._id
                        });


                    });
                } else {
                    results = eventConfirmationObject.confirmations;
                }
            }
        }

        return results;
    }

    ///////////////////////////////////////////////

    $scope.getAssignmentTitle = function(item, assignmentId) {
        var ass = _.find(item.assignments, {
            _id: assignmentId
        });
        if (ass) {
            return ass.title;
        }
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    $scope.pagerEnabled = function() {

        var morePages = ($scope.filteredItems && $scope.filteredItems.length && ($scope.filteredItems.length > $scope.pager.limit));

        return morePages && ($scope.settings.viewMode != 'calendar' && $scope.settings.viewMode != 'tracks' && $scope.settings.viewMode != 'cards');
    }

    ///////////////////////////////////////////////

    $scope.planTooltip = function(item) {

        var string = '';

        _.each(item.plans, function(plan) {
            string += plan.title + '<br/>';
        })


        string += '';

        return string;

    }

    $scope.viewPlans = function(event) {

        if (event.plans.length == 1) {
            $scope.viewInModal(event.plans[0])
        } else {
            $scope.viewInModal(event);
        }
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////


    $scope.assignmentTooltip = function(item) {

        var string = '<div class="text-left tooltip-block"><div class="row">';




        _.each(item.assignments, function(assignment) {
            if (assignment.contacts.length) {
                string += '<div class="col-xs-6 inline-column"><strong>' + assignment.title + '</strong><br/>';

                _.each(assignment.contacts, function(contact) {
                    if (contact) {
                        string += '<div style="white-space: nowrap">' + contact.title + '</div>';
                    }
                });

                string += '</div>';
            }
        })
        /*
        if(item.assignments.length) {
            return 'By ' + item.updatedBy + ' ' + $filter('timeago')(item.updated);
        } else {
            return $filter('timeago')(item.updated);
        }
        */

        string += '</div></div>';

        return string;

        //return $sce.trustAsHtml('<span ng-if="item.updatedBy">By ' +item.updatedBy+ '</span> {{item.updated | timeago}}');
    }

    ////////////////////////////////////////////////////////////////

    $scope.newTooltip = function(item) {
        var string = '<div class="text-left tooltip-block"><div class="row row-inline">';

        var groupedAssignments = _.chain(item.assigned)
            .filter(function(assignment) {
                return assignment.status == 'active';
            })
            .sortBy('title')
            .groupBy(function(assignment) {
                return assignment.title;
            })

            .map(function(group, key) {

                var str = '<div class="col-xs-4 inline-column"><strong>' + key + '</strong><br/>';


                _.chain(group)
                    .filter(function(assignment) {
                        return assignment.contact;
                    })
                    .map(function(assignment) {

                        // console.log('NO ASSIGNMENT', assignment);

                        var contactName = assignment.contact.title;

                        switch (assignment.confirmationStatus) {
                            case 'confirmed':
                                str += ('<div class="brand-success" style="white-space:nowrap"><i class="far success fa-check"></i> ' + contactName + '</div>');
                                break;
                            case 'denied':
                                str += ('<div class="brand-danger" style="white-space:nowrap"><i class="fas fa-exclamation"></i> ' + contactName + '</div>');
                                break;
                            default:
                                str += ('<div style="white-space:nowrap"><i class="text-muted fas warning fa-question"></i> ' + contactName + '</div>');
                                break;
                        }

                    })
                    .value();

                str += '</div>';
                return str;
            })
            .value();

        string += groupedAssignments.join('');
        string += '</div></div>';

        return string;
    }

    ///////////////////////////////////////////////

    /**
    $scope.assignedTooltip = function(item) {
        

        //Get all the confirmations we know about
        var confirmations = $scope.getAssigned(item);

        //Create the HTML String
        var string = '<div class="text-left tooltip-block"><div class="row">';

        //Loop through each assignment
        _.each(item.assignments, function(assignment) {

            if (assignment.contacts.length) {

                string += '<div class="col-xs-6 inline-column"><strong>' + assignment.title + '</strong><br/>';

                _.each(assignment.contacts, function(contact) {

                    var findConfirmation = _.find(confirmations, function(conf) {
                        var correctContact = (conf.contact._id == contact._id);
                        var correctAssignment = (conf.assignment == assignment._id || conf.title == assignment.title);
                        return (correctContact && correctAssignment);
                    })
                    

                    var contactName = $filter('capitalizename')(contact.title);

                    if (findConfirmation) {
                        switch (findConfirmation.status) {
                            case 'confirmed':
                                string += ('<span class="brand-success"><i class="far success fa-check"></i> ' + contactName + '</span><br/>');
                                break;
                            case 'denied':
                                string += ('<span class="brand-danger"><i class="far fa-exclamation"></i> ' + contactName + '</span><br/>');
                                break;
                            default:
                                string += ('<span><i class="text-muted fa warning fa-question"></i> ' + contactName + '</span><br/>');
                                break;
                        }

                    } else {
                        string += ('<span><i class="text-muted fa warning fa-question"></i> ' + contactName + '</span><br/>');
                    }

                });

                string += '</div>';
            }
        })


        //Loop through each assignment
        _.each(item.volunteers, function(slot) {

            //if (slot.contacts.length) {
                string += '<div class="col-xs-6 inline-column"><strong>' + slot.title + '</strong><br/>';

                if(slot.contacts && slot.contacts.length) {
                    _.each(slot.contacts, function(contact) {
                        // var contactName = $filter('capitalizename')(contact.title);
                        string += ('<span class="brand-success text-capitalize"><i class="far success fa-check"></i> ' + contact.title + '</span><br/>');
                    });

                    if(slot.minimum && slot.contacts.length < slot.minimum){
                        string += ('<span class="brand-danger"><i class="far fa-exclamation"></i> ' + (slot.minimum - slot.contacts.length) + ' more required</span><br/>');
                    }
                } else {
                    if(slot.minimum){
                        string += ('<span class="brand-danger"><i class="far fa-exclamation"></i> ' + slot.minimum + ' required</span><br/>');
                    }
                }

                string += '</div>';
            //} 
        })

        string += '</div></div>';

        return string;




    }
    /**/


    // function getVolunteerNumbers(slot) {
    //     if(!slot.minimum) {
    //         return '';
    //     }

    //     if(!slot.contacts || !slot.contacts.length) {
    //         return '<span>'+slot.minimum+' required</span>'
    //     }

    //     if(slot.contacts.length >= slot.minimum) {
    //         return '<i></i>';
    //     }

    //     var num = slot.minimum - slot.contacts.length;
    //     if(num > 0) {
    //         return '<span>('+slot.contacts.length +'/'+ slot.minimum+')</span>';
    //     } else {

    //     }



    // }


    ///////////////////////////////////////////////

    $scope.stringBrowseDate = function() {

        var itemDate = new Date(session.search.browseDate);
        itemDate.setHours(0);
        itemDate.setMinutes(0);
        itemDate.setSeconds(0);
        itemDate.setMilliseconds(0);
        return itemDate.getTime();
    }


    $scope.canCreatePlans = function() {
        return FluroAccess.can('create', 'plan');
    }


    ///////////////////////////////////////////////

    $scope.createPlanFromTemplate = function(event) {

        var selection = {};
        var modal = ModalService.browse('plan', selection);
        modal.result.then(addTemplatePlan,
            addTemplatePlan)

        function addTemplatePlan() {


            if (selection.items && selection.items.length) {

                //Create an array if there isnt one already
                if (!event.plans || !event.plans.length) {
                    event.plans = [];
                }

                //Get the template
                var template = selection.items[0];



                //Link it to this event
                template.event = event;
                template.startDate = null;

                // console.log('ORIGINAL', template);

                // console.log('TEMPLATE', template)

                // console.log('Add Plan from template', template)
                ModalService.edit(template, function(copyResult) {
                    if (copyResult) {
                        event.plans.push(copyResult)
                    }
                    //console.log('After', $scope.item)
                }, function(res) {
                    //console.log('Failed to copy', res)
                }, true, template, $scope);
            }
        }

    }

    ///////////////////////////////////////////////

    $scope.addPlan = function(event) {

        ModalService.create('plan', {
            template: {
                event: event,
                realms: event.realms,
                startDate: event.startDate,
            }
        }, function(result) {
            if (result.definition) {
                CacheManager.clear(result.definition);
            } else {
                CacheManager.clear(result._type);
            }

            $state.reload();

        }, function(result) {
            //console.log('Failed to create in modal', result)
        });
    }

    ///////////////////////////////////////////////

    if (!session.search.calendarMode) {
        session.search.calendarMode = 'month';
    }

    $scope.calendarOpen = function(item) {
        $scope.viewInModal(item.event)
    }


    $scope.calendar = {}

    ///////////////////////////////////////////////


    $scope.$watch('filteredItems', function(data) {

        $scope.dateGroups = _.groupBy(data, function(item) {
            var itemDate = new Date(item.startDate);
            itemDate.setHours(0);
            itemDate.setMinutes(0);
            itemDate.setSeconds(0);
            itemDate.setMilliseconds(0);
            return itemDate.getTime();
        });


        $scope.calendar.events = _.map(data, function(item) {
            return {
                event: item,
                title: item.title, // The title of the event
                type: 'primary', // The type of the event (determines its color). Can be important, warning, info, inverse, success or special
                startsAt: new Date(item.startDate), // A javascript date object for when the event starts
                endsAt: new Date(item.endDate), // Optional - a javascript date object for when the event ends
                editable: false, // If edit-event-html is set and this field is explicitly set to false then dont make it editable. 
                //If set to false will also prevent the event from being dragged and dropped.
                deletable: false, // If delete-event-html is set and this field is explicitly set to false then dont make it deleteable
                incrementsBadgeTotal: true, //If set to false then will not count towards the badge total amount on the month and year view
                //recursOn: 'year', // If set the event will recur on the given period. Valid values are year or month
                //cssClass: 'a-css-class-name' //A CSS class (or more, just separate with spaces) that will be added to the event when it is displayed on each view. Useful for marking an event as selected / active etc
            }
        })
    }, true);

    ///////////////////////////////////////////////

    $scope.currentTimezone = moment.tz.guess();

    $scope.readableDate = DateTools.readableDateRange;

    $scope.setShowAll = function() {
        session.search.restrictToDate = false;
        $scope.updateFilters();
    }

    $scope.setShowDate = function() {
        session.search.restrictToDate = true;
        $scope.updateFilters();
    }


    ///////////////////////////////////////////////

    //Filter the items by all of our facets
    $scope.updateFilters = function() {

        if ($scope.items) {
            //Start with all the results
            var filteredItems = $scope.items;


            //Old Search functionality
            filteredItems = $filter('filter')(filteredItems, {
                searchString: $scope.search.terms
            });

            // //Filter search terms
            // filteredItems = $filter('filter')(filteredItems, $scope.search.terms);

            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////

            if (session.search.browseDate) {



                ////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////

                var now = new Date();
                now.setHours(0, 0, 0, 0);
                var then = new Date(session.search.browseDate);


                var newStateParams = {};
                var needsReload;


                if (then < now) {
                    // console.log('In the past reload')
                    // if (!$stateParams.searchArchived) {
                    //     newStateParams.searchArchived = true;
                    //     needsReload = true;
                    // }

                    if ($stateParams.startDate != then) {
                        newStateParams.startDate = then;
                        needsReload = true;
                    }

                }

                if (needsReload) {
                    $state.go('.', newStateParams, {
                        reload: true
                    })
                    return;
                }

                ////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////





                // console.log('Have browse date')
                var browseType = 'upcoming';
                if (session.search.restrictToDate) {
                    browseType = 'specific';
                }

                // console.log('Filter all', browseType);
                filteredItems = $filter('matchDate')(filteredItems, session.search.browseDate, browseType);
            }

            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////

            //Order the items
            filteredItems = _.sortBy(filteredItems, function(item) {
                var val = _.get(item, $scope.search.order);

                ///////////////////////////////

                if (!val) {
                    if ($scope.search.reverse) {
                        return 0;
                    } else {
                        return 99999999999999999999999;
                    }
                }

                ///////////////////////////////

                if (_.isArray(val) && val.length) {
                    val = val[0];
                }

                if (val.title) {
                    return val.title;
                }

                return val;

            });


            if ($scope.search.reverse) {
                filteredItems.reverse();
            }


            //Order the items
            // filteredItems = $filter('orderBy')(filteredItems, $scope.search.order, $scope.search.reverse);

            if ($scope.search.filters) {
                _.forOwn($scope.search.filters, function(value, key) {

                    if (_.isArray(value)) {
                        _.forOwn(value, function(value) {
                            filteredItems = $filter('reference')(filteredItems, key, value);
                        })
                    } else {
                        filteredItems = $filter('reference')(filteredItems, key, value);
                    }
                });
            }

            ////////////////////////////////////////////

            //Update the items with our search
            $scope.filteredItems = filteredItems;

            // console.log('UPDATE PAGE')
            //Update the current page
            $scope.updateSort();

        }
    }


    function startOfDay(input) {


        var date = new Date(input);

        if(!isValidDate(date)) {
           console.log('Not a valid date');
           date = new Date();
        }

        
        date.setHours(0, 0, 0, 0);

        


        return date;
    }

    ///////////////////////////////////////////////

    //Set to today if not set previously
    if (!session.search.browseDate) {
        console.log('NO BROWSE DATE', session.search.browseDate)
        return $scope.resetToday();
    } else {

        //What is the previous date we were looking at?
        var currentSessionDate = startOfDay(session.search.browseDate);


        if (!$stateParams.startDate) {

            if (!currentSessionDate) {
                //Reset to today
                return $scope.resetToday();
            } else {
                console.log('GOTTA RELOAD THAT SUCKA', currentSessionDate)
            }

            // return; $timeout(function() {


            // $state.go($state.current, { startDate: startOfDay(currentSessionDate) }, {reload:true})
            // }, 300);
            // $state.reload();



        }

    }


});