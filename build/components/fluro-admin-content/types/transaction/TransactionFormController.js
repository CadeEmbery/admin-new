app.controller('TransactionFormController', function($scope, $state,  Notifications, CacheManager, FluroContent) {


	//Create a refund object
	$scope._refund = {};


		// console.log('REFUND THIS AMOUNT', $scope);
	$scope.createFullRefund = function() {

		// console.log('Create full refund')

		//Copy the refund object
		var refundData = angular.copy($scope._refund);




		FluroContent.endpoint('payment/refund/' + $scope.item._id).save({
			amount:$scope.item.total,
		}).$promise.then(function(res) {

			//Reload the state
			$state.reload();
			CacheManager.clear('transaction');
			if($scope.item.definition) {
				CacheManager.clear($scope.item.definition);
			}
			Notifications.post('A full refund was made to ' + $scope.item.title, 'status');
		}, function(err) {

			if(err.message) {
				Notifications.post(err.message, 'error');
			} else {
				Notifications.post(err, 'error');
			}
		});

		//Setup for another refund
		$scope._refund = {};

		if($scope.$dismiss) {
			$scope.$dismiss();
		}

	}

	/////////////////////////////////////////////////////////////////////////


	$scope.createRefund = function() {

		console.log('Create refund')

		//Copy the refund object
		var refundData = angular.copy($scope._refund);

		FluroContent.endpoint('payment/refund/' + $scope.item._id).save(refundData).$promise.then(function(res) {

			//Reload the state
			$state.reload();

			CacheManager.clear('transaction');
			if($scope.item.definition) {
				CacheManager.clear($scope.item.definition);
			}

			
			Notifications.post('A refund was made to ' + $scope.item.title, 'status');
		}, function(err) {

			if(err.message) {
				Notifications.post(err.message, 'error');
			} else {
				Notifications.post(err, 'error');
			}
		});

		//Setup for another refund
		$scope._refund = {};

	}

});