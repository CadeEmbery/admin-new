app.controller('EventTrackFormController', function($scope, TypeService) {


    $scope.plural = function(measurement) {
        if (Number($scope.item.recurCount) != 1) {
            return measurement + 's';
        } else {
            return measurement;
        }
    }

    ////////////////////////////////////////////

    //Wait til all the definitions have been loaded
    var promise = TypeService.refreshDefinedTypes(true);

    promise.then(function() {
        $scope.eventDefinitions = TypeService.getSubTypes('event');
        $scope.rosterDefinitions = TypeService.getSubTypes('roster');



         $scope.allSlots = _.chain($scope.rosterDefinitions)
         .map('data.slots')
         .flatten()
        .compact()
        .uniq(function(slot) {
            return slot.title;
        })
        .value()

        console.log('Roster Data', $scope.rosterDefinitions);



    });

    ////////////////////////////////////////////
    ////////////////////////////////////////////


    $scope.defaultRosterData = _.reduce($scope.item.defaultRosters, function(rosterSet, rosterEntry, rosterKey) {

        /////////////////////////

        var mappedSlots = _.reduce(rosterEntry.slots, function(slotSet, slot) {

            /////////////////////////

            //Map the slot
            slotSet[_.startCase(slot.title)] = {
                confirmationStatus:slot.confirmationStatus,
                contacts:slot.contacts,
            }

            /////////////////////////

            return slotSet;

        }, {});

        /////////////////////////

        rosterSet[rosterEntry.definition] = {
            create:rosterEntry.create,
            slots:mappedSlots,
            managedOwner:rosterEntry.managedOwner,
        }

        /////////////////////////

        return rosterSet;

    }, {});




    // console.log('STARTING ROSTER DATA', $scope.defaultRosterData);


    startWatchingRosterData();

    ////////////////////////////////////////////

    var watchStopper;

    function startWatchingRosterData() {
        watchStopper = $scope.$watch('defaultRosterData', updateRosters, true);
    }

    function stopWatchingRosterData() {
        if (watchStopper) {
            watchStopper();
        }
    }

    ////////////////////////////////////////////

    


    function updateRosters(rosterData) {




        ////////////////////////////////////////////



        $scope.item.defaultRosters = _.reduce(rosterData, function(rosterSet, rosterEntry, rosterKey) {


            ////////////////////////////////////////////

            if (!rosterEntry.create) {
                return rosterSet;
            }

            ////////////////////////////////////////////

            //Map each slot
            var mappedRosterSlots = _.reduce(rosterEntry.slots, function(set, slot, slotKey) {

                

                //Create a mapped slot
                var mappedSlot = {
                    title: _.startCase(slotKey),
                    contacts: _.map(slot.contacts, '_id'),
                    confirmationStatus: slot.confirmationStatus,
                }

                if(_.isArray(slot.contacts)) {
                    mappedSlot.contacts = _.map(slot.contacts, '_id');
                } else {
                    mappedSlot.contacts = [slot.contacts];
                }

                mappedSlot.contacts = _.chain(mappedSlot.contacts)
                .compact()
                .uniq()
                .value();

                //Push it into the list
                set.push(mappedSlot);

                return set;
            }, [])

            ////////////////////////////////////////////

            //Map the roster object
            var mappedRoster = {
                definition: rosterKey,
                create: rosterEntry.create,
                slots: mappedRosterSlots,
                managedOwner:_.map(rosterEntry.managedOwner, '_id'),
            }

            ////////////////////////////////////////////

            //Push it into the set
            rosterSet.push(mappedRoster);


            

            ////////////////////////////////////////////

            return rosterSet;

        }, []);

        ////////////////////////////////////////////

        


        ////////////////////////////////////////////

        // console.log('ROSTERS UPDATED', $scope.item.defaultRosters);

        ////////////////////////////////////////////


    }



});