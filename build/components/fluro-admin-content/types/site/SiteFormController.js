app.controller('SiteFormController', function($scope, $http, FluroAccess, ObjectSelection, ContentVersionService) {


/**
    if (!$scope.item.routes) {
        $scope.item.routes = [];
    }

    $scope.selected = {}


    if (!$scope.item.menus) {
        $scope.item.menus = [];
    }

    $scope.addRoute = function() {
        $scope.item.routes.push({})
    }

    $scope.removeRoute = function(route) {
        console.log('Remove route', route)
        _.remove($scope.item.routes, route);
    }


    $scope.addMenu = function() {
        $scope.item.menus.push({title:'Menu'})
    }

    $scope.removeMenu = function() {
        _.remove($scope.item.menus, $scope.selected.menu);
        $scope.selected.menu = null;

    }

    $scope.context = {
        showAll: true
    }

    $scope.isActive = function(route) {
        return $scope.context.activeRoute == route;
    }

    /**/




    $scope.selection = new ObjectSelection();
    $scope.selection.multiple = false;


    $scope.$watch('item.routes', function(routes) {
        $scope.flattenedRoutes = getFlattenedRoutes(routes);
    })


    $scope.$watch('item._id', function(id) {
        if(id) {
            $scope.versions = ContentVersionService.list($scope.item);
        }
    })


    //////////////////////////////////

    $scope.hasSlug = function(route) {
        return _.contains(route.url, ':slug');
    }


    $scope.defaultSaveOptions.forceVersioning = true;


    //Now we have nested routes we need to flatten
    function getFlattenedRoutes(array) {
        return _.chain(array).map(function(route) {
                if (route.type == 'folder') {
                    return getFlattenedRoutes(route.routes);
                } else {
                    return route;
                }
            })
            .flatten()
            .compact()
            .value();
    }


    /*
    if (!$scope.item.branches || !$scope.item.branches.length) {
        $scope.item.branches = [{
            title: 'Home',
            _type: 'container',
        }]
    }


    $scope.search = {}

    //////////////////////////////////////////////////////////

    $scope.$watch('search.terms', function(data) {

        console.log('Searching', data)

        $http.get('/api/search', {
            ignoreLoadingBar: true,
            params: {
                keys: data,
                limit: 10,
            }
        }).then(function(response) {
            $scope.contentOptions = response.data;
        });

        



    })

    //////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////

    $scope.toolbox = [{
        title: 'Container',
        _type:'container',
    }, ]
*/



});