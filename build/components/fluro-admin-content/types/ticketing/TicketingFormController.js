app.controller('TicketingFormController', function($scope) {
	$scope._proposed = {
        addon:{
            variations:[]
        }
    };

    //////////////////////////////////////////
    //////////////////////////////////////////

    $scope.addAttendeeType = function() {

        var attendee = angular.copy($scope._proposed.attendee);

        if(!$scope.item.attendeeTypes) {
            $scope.item.attendeeTypes = [];
        }
        
        $scope.item.attendeeTypes.push(attendee);

        $scope._proposed.attendee = {};
    }

    //////////////////////////////////////////

    $scope.removeAttendeeType = function(attendee) {

        _.pull($scope.item.attendeeTypes, attendee);
    }


//////////////////////////////////////////

    $scope.addTicketOption = function() {

        var ticketOption = angular.copy($scope._proposed.ticket);

        if(!$scope.item.ticketOptions) {
            $scope.item.ticketOptions = [];
        }
        
        $scope.item.ticketOptions.push(ticketOption);

        $scope._proposed.ticket = {};
    }

    //////////////////////////////////////////

    $scope.removeTicketOption = function(ticketOption) {

        _.pull($scope.item.ticketOptions, ticketOption);
    }


//////////////////////////////////////////

    $scope.addTicketRow = function(ticket) {

        if(!ticket.rows) {
            ticket.rows = [];
        }

        ticket.rows.push({editable:true});
    }

    //////////////////////////////////////////

    $scope.removeTicketRow = function(ticket, row) {
        _.pull(ticket.rows, row);
    }





    //////////////////////////////////////////
    //////////////////////////////////////////
    //////////////////////////////////////////
    //////////////////////////////////////////

    $scope.addAddon = function() {

        var addon = angular.copy($scope._proposed.addon);

        if(!$scope.item.addons) {
            $scope.item.addons = [];
        }
        
        $scope.item.addons.push(addon);

        $scope._proposed.addon = {
            variations:[]
        };
    }
    //////////////////////////////////////////


    $scope.addVariation = function(addon) {

        if(!addon.variations) {
            addon.variations = [];
        }

        addon.variations.push({title:'', amount:0});
    }

    $scope.removeVariation = function(addon, variation) {

       _.pull(addon.variations, variation);
    }

    //////////////////////////////////////////

    $scope.removeAddon = function(addon) {

        _.pull($scope.item.addons, addon);
    }



})