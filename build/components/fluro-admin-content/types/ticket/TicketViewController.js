
app.controller('TicketViewController', function($scope, Fluro) {



	var ticketID = $scope.item._id;
	var interactionID = _.get($scope.item, 'interaction._id') || _.get($scope.item, 'interaction');


	$scope.ticketCodeURL = Fluro.apiURL + '/system/qr?input=http://tickets.fluro.io/ticket/' + ticketID;
	$scope.interactionCodeURL = Fluro.apiURL + '/system/qr?input=http://tickets.fluro.io/interaction/' + interactionID;


});