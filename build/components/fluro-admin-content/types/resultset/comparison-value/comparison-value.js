app.directive('compare', function() {
    return {
        restrict: 'E',
        replace:true,
        scope:{
        	then:'=previous',
        	now:'=current',
        },
        templateUrl: 'fluro-admin-content/types/resultset/comparison-value/comparison-value.html',
        link: function($scope, $element, $attr) {

            var now = parseFloat($scope.now);
            var then = parseFloat($scope.then);

            //////////////////////////////////

            var movement = _.round(Math.abs(100 - (now / then) * 100), 2)

            if(isFinite(movement)) {
            	$scope.movement = movement.toString().concat("%");;
            } else {
            	$scope.moment = '';
            }
            //////////////////////////////////

            if (now > then) {
                $scope.positive = true;
            } else if (now < then) {
                $scope.negative = true;

            } else {
                $scope.equal = true;
                $scope.movement = "";
            }

            //////////////////////////////////

            // console.log('$SCOPE', now, then);
        }


    }
});