app.controller('ResultSetViewController', function($scope, Fluro, $state) {


    $scope.link = function(entry) {

        // console.log('Entry', entry)
        var filename = _.kebabCase($scope.item.title + ' ' + entry.date);
        return Fluro.apiURL + '/results/' + $scope.item._id + '/' + entry._id + '/' + filename + '.csv';

        
    }

    // $scope.jsonLink = function(entry) {

    //     // console.log('Entry', entry)
    //     var filename = _.kebabCase($scope.item.title + ' ' + entry.date);
    //     return Fluro.apiURL + '/results/' + $scope.item._id + '/' + entry._id + '/' + filename + '.csv';
    // }

    /////////////////////////////////////////////////

    $scope.viewSource = function() {
        $state.go('query.view', {
            definitionName: $scope.item.parent.definition,
            id: $scope.item.parent._id
        })
    }


    /////////////////////////////////////////////////

    var sheets = $scope.extras.results;
    $scope.sheets = sheets;

    /////////////////////////////////////////////////

    $scope.selected = {};

    /////////////////////////////////////////////////

    //Get all the dates we have data for
    $scope.labels = _.map(sheets, function(sheet) {
    	return new Date(sheet.date).format('j M');
    });

    

    /////////////////////////////////////////////////

    var allOptions = _.chain(sheets)
    .map(function(sheet) {
    	//For each sheet add 
    	return _.map(sheet.data, function(obj, key) {
    		return _.map(_.keys(obj), function(subKey) {
    			return '["' + key + '"].' + subKey;
    		})
    	})
    })
    .flattenDeep()
    .uniq()
    .value();

    $scope.getCell = function(sheet, path) {



    	var value = _.get(sheet, 'data.'+ path.value);
    	if(typeof value == 'number'){
    		return _.round(value, 2);
    	} else {
    		return value;
    	}

    }

    /////////////////////////

    /////////////////////////

    $scope.trackingPaths = _.chain(allOptions)
    .reduce(function(set, string) {

        

    	var pieces = string.split('.');
        var lastKey = pieces.pop();//pieces[pieces.length-1];

        //Check if there is a '.' in the object key
        var multiLevelKey = pieces.length > 1;
    	var groupName = pieces.join('.');//pieces.shift();
    	
    	var title = _.startCase(lastKey);
    	var groupTitle = '\'' + _.startCase(groupName) + '\'';


    	//////////////////////////////////////////

    	switch(lastKey) {
    		case 'length':
    		case 'values':
    		
    			return set;
    		break;
    		case 'total':
    			title = 'Sum of all ' + groupTitle + ' cells';
    		break;
    		case 'numValues':
    			title = 'Count of '+groupTitle+ ' cell values';
    		break;
    		case 'unique':
    			title = 'Count of unique ' + groupTitle+' cell values';
    		break;
    		case 'smallest':
    			title = 'Least '+ groupTitle+' cell value';
    		break;
    		case 'largest':
    			title = 'Greatest '+groupTitle+' cell value';
    		break;
    		case 'average':
    			title = 'Average '+ groupTitle +' cell value';
    		break;
    	}

    	//////////////////////////////////////////

    	var option = {
    		title:title,
    		value:string,
    	}

    	if(!set[groupName]) {
    		set[groupName] = {
    			title:_.startCase(groupName),
    			paths:[]
    		}
    	}

        console.log('OPTION', option);
    	set[groupName].paths.push(option);

        // console.log('CHECK SET', groupName, set[groupName]);

    	return set;
    }, {})
    .values()
    .value();

    /////////////////////////////////////////////////

    $scope.$watch('selected', updateTracks, true);

    /////////////////////////////////////////////////

    function updateTracks() {
	   
	    var seriesNames = _.chain(allOptions)
	    .filter(function(path) {
	    	return $scope.selected[path];
	    })
	    .value();


	    /////////////////////////

	    console.log('SERIES CHANGE', seriesNames);

	    $scope.series = _.map(seriesNames, _.startCase);
	    $scope.data = _.chain(seriesNames)
	    .map(function(path) {

	    	// data.attendance.average
	    	return _.map(sheets, function(sheet) {
	    		return _.get(sheet.data, path);
	    	});
	    })
	    .value();

	}

    /////////////////////////////////////////////////
    // .value();

    // $scope.data = _.chain($scope.series)
    // .map(function(series) {
    // 	return _.map(, function(path) {
    // 		// console.log('GET', path, sheet);
    // 		return _.get(sheet.data, path) || 0;
    // 	})
    // })
    // .value();


    // console.log('STUFF', $scope.labels, $scope.series, $scope.data);
    // console.log('STUFF', seriesNames);

 

    // ["January", "February", "March", "April", "May", "June", "July"];

    // $scope.series = ['Series A', 'Series B'];
    // $scope.data = [
    //     [65, 59, 80, 81, 56, 55, 40],
    //     [28, 48, 40, 19, 86, 27, 90]
    // ];


    // $scope.onClick = function(points, evt) {
    //     console.log(points, evt);
    // };
    // $scope.datasetOverride = [{
    //     yAxisID: 'y-axis-1'
    // }, {
    //     yAxisID: 'y-axis-2'
    // }];
    // $scope.options = {
    //     scales: {
    //         yAxes: [{
    //             id: 'y-axis-1',
    //             type: 'linear',
    //             display: true,
    //             position: 'left'
    //         }, {
    //             id: 'y-axis-2',
    //             type: 'linear',
    //             display: true,
    //             position: 'right'
    //         }]
    //     }
    // };

    /////////////////////////////////////////////////

    $scope.years = _.chain(sheets)
    	.sortBy(function(sheet) {
    		return (new Date(sheet.date)).getTime();
    	})
        .reduce(function(set, entry) {

            var date = new Date(entry.date);
            var dateKey = date.format('j M Y');

            var existing = set[dateKey];
            if (!existing) {
                existing =
                    set[dateKey] = {
                        title: date.format('l j M'),
                        date: date,
                        items: [],
                    }
            }

            existing.items.unshift(entry);

            return set;
        }, {})
        .values()
        .reduce(function(set, day) {

            var date = new Date(day.date);
            var monthKey = date.format('M Y');

            var existing = set[monthKey];
            if (!existing) {
                existing =
                    set[monthKey] = {
                        title: date.format('M'),
                        date: date,
                        days: [],
                    }
            }

            existing.days.unshift(day);

            return set;
        }, {})
        .values()
        .reduce(function(set, month) {

            var date = new Date(month.date);
            var yearKey = date.format('Y');

            var existing = set[yearKey];
            if (!existing) {
                existing =
                    set[yearKey] = {
                        title: date.format('Y'),
                        date: date,
                        months: [],
                    }
            }

            existing.months.unshift(month);

            return set;
        }, {})
        .values()
        .reverse()
        .value();

});