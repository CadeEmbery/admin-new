app.controller('TeamFormController', function($scope, $rootScope, FluroContent) {
    
	$scope.tracks = [];


    



    if(!$scope.item._id) {
        $scope.item.visibleAssigned = true;
        $scope.item.visibleMembers = true;
        $scope.item.shareContactDetails = true;


        $scope.item.assignments = _.get($scope.definition, 'data.defaultPositions') || [];


    } else {
    	FluroContent.endpoint('teams/' + $scope.item._id + '/tracks').query()
    	.$promise
    	.then(function(tracks) {

    		$scope.tracks = _.filter(tracks, {status:'active'});
    	}, function(err) {
    		console.log('Error loading tracks', err);
    	})
    }



    if($scope.definition && $scope.definition.data && $scope.definition.data.defaultPositions) {

        if(!$scope.item.assignments) {
            $scope.item.assignments = [];
        }
        

        _.each($scope.definition.data.defaultPositions, function(position) {

            var positionTitle = String(position.title).toLowerCase();
           
            var match = _.find($scope.item.assignments, function(existingSlot) {
                return String(existingSlot.title).toLowerCase() == positionTitle;
            });


            if(!match) {
                // console.log('CREATE DEFAULT ROLE');
                $scope.item.assignments.push(position);
            } else {

                if(!match.roles) {
                    match.roles = [];
                }

                match.roles = _.chain(match.roles.concat(position.roles))
                .compact()
                .uniq(function(role) {
                    return role._id || role;
                })
                .value();

                // console.log('AUGMENT WITH ROLES')
            }
        })
    }







    $scope.createEventTrack = function() {


        var defaultRealms = $scope.item.realms || [];
        defaultRealms.push($scope.item._realm);

        defaultRealms = _.uniq(defaultRealms, function(realm) {
            return realm._id|| realm;
        })

        var params = {
            template:{
                title:$scope.item.title,
                defaultExpectTeams:[$scope.item],
                realms:defaultRealms,
            }
        };


        $rootScope.modal.create('eventtrack', params, function(track) {
               $scope.tracks.push(track);
        });
    }

    $scope.saveTrack = function(track) {

        console.log('Save Track', track)
        if(track.save) {
        track.save();
        }
    }

    $scope.trackSummary = function(track) {

    	string = '';


    	if(track.autoRecur) {

    		if(track.recurDefinition) {
    			string += 'Regular '+track.recurDefinition+' events ';
    		} else {
    			string += 'Regular events ';
    		}
    	} else {
            return 'No regular meetings'
        }


    	// switch(track.recurMeasure) {
    		// case 'days':

    		// break;
    		
    	// }

    	if(track.recurNth == 'date') {
    		string += 'Every ' + track.recurCount +' ' + track.recurMeasure + ' ';
    		string += 'on ' + track.recurWeekday + ' ';
    	}

    	// if(track.recurNth == 'date') {
    		// string += 'Every ' + track.recurCount +' ' + track.recurMeasure + ' ';
    		string += track.defaultStartTime+'hrs ' + track.defaultTimezone + ' timezone';
    	// }



    	return string;
    }
});