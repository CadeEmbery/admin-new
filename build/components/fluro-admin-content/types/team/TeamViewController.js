app.controller('TeamViewController', function($scope, $rootScope, $filter, $state, TypeService, FluroAccess, ModalService) {
    // ProcessPermissionService

    $scope.viewContact = $rootScope.viewItem;


    //////////////////////////////////////////////////////////

    $scope.$watch('search.terms', function(terms){

        var filteredAssignments = $scope.item.assignments;
        var filteredMembers = $scope.item.provisionalMembers;

        ///////////////////////////////////////////////

        if(terms && terms.length) {
            filteredAssignments = $filter('filter')(filteredAssignments, terms);
            filteredMembers = $filter('filter')(filteredMembers, terms);
        }

        ///////////////////////////////////////////////

        filteredAssignments = _.chain(filteredAssignments)
        .filter(function(assignmentSlot) {
            return _.get(assignmentSlot, 'contacts.length');
        })
        .map(function(slot) {

            var output = {
                title:slot.title,
            }

            //Map the contacts
            output.contacts = _.chain($filter('filter')(slot.contacts, terms))
            .sortBy(function(contact) {
                return contact.firstName;
            })
            .value();

            return output;
        })
        .value();

        ///////////////////////////////////////////////

        filteredMembers = _.chain(filteredMembers)
        .sortBy(function(member) {
            return member.firstName;
        })
        .value();

        ///////////////////////////////////////////////

        $scope.filteredAssignments = filteredAssignments;
        $scope.filteredMembers = filteredMembers;

        ///////////////////////////////////////////////

        


       



        
    }) 


    //////////////////////////////////////////////////////////

    TypeService.refreshDefinedTypes(true).then(function() {

        //Check if the user can create any mailouts
        var createableMailouts = _.chain(TypeService.definedTypes)
            .filter(function(mailoutType) {
                var correctParentType = mailoutType.parentType == 'mailout';
                var canCreate = FluroAccess.can('create', mailoutType.definitionName, 'mailout');

                return correctParentType && canCreate;
            })
            .value();

         if (createableMailouts && createableMailouts.length) {
        $scope.canMailout = true;
    }
    })

    

    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

   


    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    $scope.sendMailout = function() {

        function success(res) {
            console.log('Mailout Select Success', res)
        }

        function failure(err) {
            console.log('Mailout Select Error', err)
        }


        var teams = [$scope.item];

        var template = {
            realms:$scope.item.realms,
        }
        return ModalService.selectPositions(teams, function(contacts) {
                          
            //If all or nothing was selected then just send to the teams
            //and be done here
            if(!contacts || !contacts.length) {
                template.teams = teams;
            } else {
                template.contacts = contacts
            }

           return ModalService.mailout(template, success, failure);

        });



    }


    //////////////////////////////////////////////////////////

    $scope.sendSMS = function() {


        var teams = [$scope.item];

        var template = {
            realms:$scope.item.realms,
        }

        //////////////////////////////////////////////////////////

        return ModalService.selectPositions(teams, function(contacts) {
                          


            function success(res) {
                console.log('Message Success', res)
            }

            function failure(err) {
                console.log('Message Error', err)
            }

            //////////////////////////////////////////////////////////

            var NoContactsSelected = (!contacts || !contacts.length);
            var AllPositionsSelected = (contacts == 'all');

            //////////////////////////////////////////////////////////

            if(NoContactsSelected || AllPositionsSelected) {
                
                var allAssigned = _.chain($scope.item.assignments)
                .map(function(assignment) {
                	return assignment.contacts;
                })
                .flatten()
                .value();


                var allMembers = $scope.item.provisionalMembers;


                contacts = [].concat(allAssigned, allMembers);
                contacts = _.chain(contacts)
                .map(function(contact) {
                	return contact._id;
                })
                .compact()
                .uniq()
                .value();
            }



            //////////////////////////////////////////////////////////

            //Open a new modal to send SMS to everyone
            ModalService.sms(contacts, success, failure);

        });

    }



    //////////////////////////////////////////////////////////

});