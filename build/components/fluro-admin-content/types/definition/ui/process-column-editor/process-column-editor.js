app.directive('processColumnEditor', function(){
	// Runs during compile
	return {
		// name: '',
		// priority: 1,
		// terminal: true,
		scope: {
			model:'=ngModel',
			definition:'=ngDefinition',
			processDefinitions:'=processDefinitions',
			detailDefinitions:'=detailDefinitions',
			closeCallback: '=close',
			cancelCallback: '=cancel',
			processing:'=',
		},
		controller: 'ProcessColumnEditorController',
		// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
		// restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		// template: '',
		templateUrl: 'fluro-admin-content/types/definition/ui/process-column-editor/process-column-editor.html',
		// replace: true,
		// transclude: true,
		// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
		// link: function($scope, iElm, iAttrs, controller) {
			
		// }
	};
});


app.controller('ProcessColumnEditorController', function($scope, TypeService) {


	$scope.addTaskList = function() {

		var state = $scope.model;

        console.log('Add Task List');
        if(!state.taskLists) {
            state.taskLists = [];
        }

        state.taskLists.push({
            title: state.title +" tasks",
            adding:true,
            tasks:[],
        })
    }


    $scope.taskCount = function() {
    	return _.chain($scope.model.taskLists)
    	.map(function(list) {
    		return list.tasks.length;
    	})
    	.sum()
    	.value();
    }



    // $scope.detailDefinitions = detailDefinitions;//TypeService.getSubTypes('contactdetail');

    console.log('DETAIL DEFINITIONS', $scope.detailDefinitions);

    $scope.contactDefinitions = TypeService.getSubTypes('contact');

    $scope.close = $scope.closeCallback;

    console.log('cANCEL', $scope.cancelCallback)
    $scope.cancel = $scope.cancelCallback;




});