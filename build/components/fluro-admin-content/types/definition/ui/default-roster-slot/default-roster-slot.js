app.directive('defaultRosterSlot', [function () {
	return {
        restrict: 'E',
        replace: true,
        templateUrl: 'fluro-admin-content/types/definition/ui/default-roster-slot/default-roster-slot.html',
        link: function($scope, $element, $attrs, $ctrl) {


        	$scope.finish = function() {
        		var hasTitle = _.get($scope.slot, 'title');

        		if(!hasTitle || !hasTitle.length) {
        			return;
        		}

        		return $scope.$close();
        	}

        }
    }
}])