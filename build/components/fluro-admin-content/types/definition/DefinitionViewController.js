app.controller('DefinitionViewController', function($scope) {


	if($scope.item.parentType == 'interaction' && $scope.item.privacy == 'public') {
		$scope.formURL = 'https://forms.fluro.io/form/' + $scope.item._id;
	}
})