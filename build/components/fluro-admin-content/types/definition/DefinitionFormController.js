function extractFieldsFromDefinitionFields(fields, indexIterator, includeExamples) {

    if (!indexIterator) {
        indexIterator = 'i'
    };

    function getFlattenedFields(array, trail, titles) {

        return _.chain(array)
            .map(function(field, key) {

                var returnValue = [];

                // console.log('FIELD WOOT', field);
                /////////////////////////////////////////

                //If there are sub fields
                if (field.fields && field.fields.length) {


                    if (field.asObject || field.directive == 'embedded') {
                        //Push the field itself
                        trail.push(field.key);
                        titles.push(field.title)

                        field.trail = trail.slice();
                        field.titles = titles.slice();

                        trail.pop();
                        titles.pop();
                        returnValue.push(field);


                        ///////////////////////////////

                        //Prepend the key to all lowed fields




                        if (field.maximum != 1) {
                            trail.push(field.key + '[' + indexIterator + ']');
                            titles.push(field.title);
                        } else {
                            trail.push(field.key);
                            titles.push(field.title);
                        }
                    }

                    // console.log('Go down', field.key);
                    var fields = getFlattenedFields(field.fields, trail, titles);

                    if (field.asObject || field.directive == 'embedded') {
                        trail.pop()
                        titles.pop();
                    }
                    //console.log('Go back up')
                    returnValue.push(fields);


                } else {
                    /////////////////////////////////////////

                    //Push the field key
                    trail.push(field.key);
                    titles.push(field.title);

                    field.trail = trail.slice();
                    field.titles = titles.slice();
                    trail.pop();
                    titles.pop();
                    returnValue.push(field);
                }



                /////////////////////////////////////////

                return returnValue;

            })
            .flattenDeep()
            .value();
    }

    // //Get all the flattened fields
    var flattened = getFlattenedFields(fields, [], []);


    var mapped = _.chain(flattened)
        .map(formatField)
        .compact()
        .value();

    /////////////////////////////////////////////////

    if (includeExamples) {
        return _.map(mapped, addExamples);
    } else {
        return mapped;
    }

    /////////////////////////////////////////////////

    var cutOff = 20;

    /////////////////////////////////////////////////

    function formatField(field, index) {

        if (field.type == 'void') {
            return;
        }


        var isArray = field.maximum != 1;
        var firstTen = [];
        var fieldPath = field.trail.join('.');

        /////////////////////////////////////////////////

        //Create the indents
        var indents = [];
        _.times(field.trail.length - 1, function(n) {
            indents.push('--');
        });

        //Indent the title
        var indentedTitle = indents.join('-') + ' ' + field.title;

        if (isArray) {
            //Add (Multiple to the end of the title)
            indentedTitle = indentedTitle + ' (Multiple)';
        }

        /////////////////////////////////////////////////

        //And it has options
        if (field.options && field.options.length) {
            firstTen = field.options.slice(0, cutOff);
        } else if (field.allowedValues && field.allowedValues.length) {
            firstTen = _.map(field.allowedValues.slice(0, cutOff), function(entry) {
                if (entry) {
                    return {
                        name: entry,
                        value: entry
                    }
                }
            });
        } else if (field.defaultValues && field.defaultValues.length) {
            firstTen = _.map(field.defaultValues.slice(0, cutOff), function(entry) {
                if (entry) {
                    return {
                        name: entry,
                        value: entry
                    }
                }
            });
        }


        var delimiter = '[' + indexIterator + ']';
        var delimiterIndex = fieldPath.lastIndexOf(delimiter);

        // console.log('FIELD PATH', fieldPath, delimiter, delimiterIndex);
        //Check if this field is a child of a lower level multi group
        if (delimiterIndex != -1) {
            var parentPath = fieldPath.slice(0, delimiterIndex);
            var childPath = fieldPath.slice(delimiterIndex + delimiter.length + 1);

            // console.log('NESTED', parentPath, childPath);
        }

        /////////////////////////////////////////////////

        return {
            title: field.title,
            indentedTitle: indentedTitle,
            key: field.key,
            parentTitle: field.titles[field.titles.length - 2],
            parentPath: parentPath,
            childPath: childPath,
            path: fieldPath,
            type: field.type,
            isArray: isArray,
            titles: field.titles.join(' > '),
            options: field.options,
            firstTen: firstTen,
        }

    }

    /////////////////////////////////////////////////



    /////////////////////////////////////////////////


    function addExamples(field) {

        var exampleValue;
        var exampleTitle;
        var fieldPath = field.path;
        var firstTen = field.firstTen;
        var isArray = field.isArray;

        /////////////////////////////////////////////////////////////

        var minimum = field.minimum;
        var maximum = field.maximum || field.firstTen.length || 5;

        /////////////////////////////////////////////////

        switch (field.type) {
            case 'integer':
                exampleTitle =
                    exampleValue = 12;
                break;
            case 'float':
                exampleTitle =
                    exampleValue = 3.56;
                break;
            case 'number':
                exampleTitle =
                    exampleValue = 15;
                break;
            case 'boolean':
                exampleTitle =
                    exampleValue = 'true';
                break;
            case 'email':
                exampleTitle =
                    exampleValue = "'example@fluro.io'";
                break;
            case 'url':
                exampleTitle =
                    exampleValue = "'https://www.fluro.io'";
                break;
            case 'date':
                exampleTitle = 'Date';
                exampleValue = new Date().getTime();
                break;
            case 'string':
                if (isArray) {

                    if (firstTen.length) {
                        exampleTitle =
                            exampleValue = '[' + _.chain(firstTen.slice(0, (maximum || 3)))
                            .compact()
                            .map(function(entry) {
                                return '"' + entry.value + '"'
                            })
                            .value()
                            .join(', ') + ']';

                    } else {
                        exampleTitle =
                            exampleValue = "['Michael', 'Susan', 'Jerry']";
                    }
                } else {
                    if (firstTen.length) {
                        exampleTitle = _.first(firstTen).name;
                        exampleValue = "'" + _.first(firstTen).value + "'";
                    } else {
                        exampleTitle =
                            exampleValue = "'Michael'";
                    }

                }
                break;
            case 'reference':
                if (isArray) {
                    exampleTitle =
                        exampleValue = '[Object, Object, Object]';
                } else {
                    exampleTitle =
                        exampleValue = '(Object)';
                }
                break;

        }

        /////////////////////////////////////////////////////////////


        var examples = [];

        /////////////////////////////////////////////////////////////

        //Check if this field is a child of a lower level multi group
        if (field.childPath) {
            // //Split the path
            // var rest = fieldPath.substring(0, fieldPath.lastIndexOf("/") + 1);
            // var last = fieldPath.substring(fieldPath.lastIndexOf("/") + 1, fieldPath.length);
            // var lastValue = exampleValue;
            // var parent = formatField(flattened, fieldIndex - 1);


            if (!firstTen.length) {
                examples.push({
                    path: "matchInArray(data." + field.parentPath + ", '" + field.childPath + "', " + exampleValue + ").length",
                    description: 'Returns the total number of  \'' + field.parentTitle + '\' values where \'' + field.title + '\' is equal to \'' + exampleTitle + '\'',
                })

                // examples.push({
                //     path: "matchInArray(data." + field.parentPath + ", '" + field.childPath + "', " + exampleValue + ")",
                //     description: 'Returns the actual array of  \'' + field.parentTitle + '\' values where \'' + field.title + '\' is equal to \'' + exampleTitle + '\'',
                // })
            }

            if (isArray) {
                examples.push({
                    path: "matchInArray(data." + field.parentPath + ", '" + field.childPath + ".length', 3).length",
                    description: 'Returns the total number of  \'' + field.parentTitle + '\' values where \'' + field.title + '\' has selected exactly 3 values',
                })

                examples.push({
                    path: "matchInArray(data." + field.parentPath + ", '" + field.childPath + ".length', 3, '>=').length",
                    description: 'Returns the total number of  \'' + field.parentTitle + '\' values where \'' + field.title + '\' has selected at least 3 values',
                })


            }



            _.each(firstTen, function(entry) {

                if (isArray) {
                    examples.push({
                        path: "matchInArray(data." + field.parentPath + ", '" + field.childPath + "', '" + entry.value + "', 'in').length",
                        description: 'Returns the total number of  \'' + field.parentTitle + '\' values where \'' + field.title + '\' has \'' + entry.name + '\' selected as one of it\'s values',
                    })


                    // examples.push({
                    //     path: "matchInArray(data." + field.parentPath + ", '" + field.childPath + "', '" + entry.value + "', 'in')",
                    //     description: 'Returns the actual array of  \'' + field.parentTitle + '\' values where \'' + field.title + '\' has \'' + entry.name + '\' selected as a value',
                    // })
                } else {

                    examples.push({
                        path: "matchInArray(data." + field.parentPath + ", '" + field.childPath + "', '" + entry.value + "').length",
                        description: 'Returns the total number of  \'' + field.parentTitle + '\' values where \'' + field.title + '\' is equal to \'' + entry.name + '\'',
                    })


                    // examples.push({
                    //     path: "matchInArray(data." + field.parentPath + ", '" + field.childPath + "', '" + entry.value + "')",
                    //     description: 'Returns the actual array of  \'' + field.parentTitle + '\' values where \'' + field.title + '\' is equal to \'' + entry.name + '\'',
                    // })
                }

            })

            


        } else {

            /////////////////////////////////////////////////////////////


            examples.push({
                path: 'data.' + fieldPath + '',
                description: 'The literal value of \'' + field.title + '\' eg. ' + exampleValue,
            })

            /////////////////////////////////////////////////////////////


            if (field.isArray) {

                examples.push({
                    path: 'data.' + fieldPath + '.length',
                    description: 'The number of \'' + field.title + '\' values',
                })

                examples.push({
                    path: 'data.' + fieldPath + '.length > 4',
                    description: 'Returns true if the number of \'' + field.title + '\' values is greater than 4',
                })

                //Get the first 10 possible options
                _.each(firstTen, function(option) {
                    examples.push({
                        path: 'data.' + fieldPath + '.includes(\'' + option.value + '\')',
                        description: 'Returns true if  \'' + option.name + '\' is included in the selection',
                    })
                })

                /////////////////////////////////////////////////
                /**
                    if (field.type == 'reference' || field.asObject) {


                        var nextIndex = fieldIndex + 1;
                        var nextField = formatField(flattened[nextIndex], nextIndex);
                        var subFieldPath = 'exampleField';
                        var subFieldValue = 'exampleValue';

                        if (nextField) {
                            subFieldPath = nextField.key;
                            if (nextField.options && nextField.options.length) {
                                subFieldValue = "'" + _.first(nextField.options).value + "'";
                            } else {
                                subFieldValue = nextField.exampleValue;
                            }
                        }

                        examples.push({
                            path: "matchInArray(data." + fieldPath + ", '" + subFieldPath + "', " + subFieldValue + ").length",
                            description: 'Returns the total number of  \'' + field.title + '\' values where \'' + subFieldPath + '\' is equal to \'' + subFieldValue + '\'',
                        })


                        examples.push({
                            path: "matchInArray(data." + fieldPath + ", '" + subFieldPath + "', " + subFieldValue + ")",
                            description: 'Returns an array of all  \'' + field.title + '\' values where \'' + subFieldPath + '\' is equal to \'' + subFieldValue + '\'',
                        })



                        /////////////////////////////////////////////////

                        var secondFieldIndex = fieldIndex + 2;
                        var secondFieldField = formatField(flattened[secondFieldIndex], secondFieldIndex);
                        var secondFieldPath = 'exampleField';
                        var secondFieldValue = 'exampleValue';

                        if (secondFieldField) {
                            secondFieldPath = secondFieldField.key;
                            if (secondFieldField.options && secondFieldField.options.length) {
                                secondFieldValue = "'" + _.first(secondFieldField.options).value + "'";
                            } else {
                                secondFieldValue = secondFieldField.exampleValue;
                            }

                            examples.push({
                                path: "matchInArray(data." + fieldPath + ", '" + secondFieldPath + "', " + secondFieldValue + ").length",
                                description: 'Returns the total number of  \'' + field.title + '\' values where \'' + secondFieldPath + '\' is equal to \'' + secondFieldValue + '\'',
                            })

                            examples.push({
                                path: "matchInArray(data." + fieldPath + ", '" + secondFieldPath + "', " + secondFieldValue + ")",
                                description: 'Returns an array of all  \'' + field.title + '\' values where \'' + secondFieldPath + '\' is equal to \'' + secondFieldValue + '\'',
                            })


                        }


                    }

    /**/

            } else {


                //Get the first 10 possible options
                _.each(firstTen, function(option) {
                    examples.push({
                        path: 'data.' + fieldPath + ' == \'' + option.value + '\'',
                        description: 'Returns true if  \'' + option.name + '\' has been selected',
                    })
                })


                switch (field.type) {
                    case 'boolean':
                        examples.push({
                            path: 'data.' + fieldPath + ' == ' + exampleValue,
                            description: 'If \'' + field.title + '\' is equal to ' + exampleValue,
                        })

                        examples.push({
                            path: 'data.' + fieldPath + ' != ' + exampleValue,
                            description: 'If \'' + field.title + '\' is not equal to ' + exampleValue,
                        })
                        break;

                    case 'string':

                        if (!firstTen.length) {
                            examples.push({
                                path: 'data.' + fieldPath + ' == ' + exampleValue,
                                description: 'If \'' + field.title + '\' is equal to ' + exampleValue,
                            })

                            examples.push({
                                path: 'data.'+fieldPath+' && data.' + fieldPath + '.toUpperCase() == ' + String(exampleValue).toUpperCase(),
                                description: 'If \'' + field.title + '\' converted to uppercase is equal to ' + String(exampleValue).toUpperCase(),
                            })

                            if (field.directive == 'wysiwyg' || field.directive == 'textarea') {
                                examples.push({
                                    path: 'data.' + fieldPath + '.length > 200',
                                    description: 'If text is more than 200 letters',
                                })
                            }
                        }
                        break;
                    case 'number':
                    case 'integer':
                    case 'float':
                        examples.push({
                            path: 'data.' + fieldPath + ' == ' + exampleValue,
                            description: 'If \'' + field.title + '\' is equal to ' + exampleValue,
                        })

                        examples.push({
                            path: 'data.' + fieldPath + ' != ' + exampleValue,
                            description: 'If \'' + field.title + '\' is not equal to ' + exampleValue,
                        })

                        examples.push({
                            path: 'data.' + fieldPath + ' > ' + exampleValue,
                            description: 'If \'' + field.title + '\' is greater than ' + exampleValue,
                        })

                        examples.push({
                            path: 'data.' + fieldPath + ' >= ' + exampleValue,
                            description: 'If \'' + field.title + '\' is greater than or equal to ' + exampleValue,
                        })

                        examples.push({
                            path: 'data.' + fieldPath + ' < ' + exampleValue,
                            description: 'If \'' + field.title + '\' is less than ' + exampleValue,
                        })

                        examples.push({
                            path: 'data.' + fieldPath + ' <= ' + exampleValue,
                            description: 'If \'' + field.title + '\' is less than or equal to ' + exampleValue,
                        })
                        break;
                }
            }

        }

        /////////////////////////////////////////////////////////////

        field.examples = examples;
        field.exampleValue = exampleValue;

        /////////////////////////////////////////////////////////////

        return field;


    }

}

app.controller('DefinitionFormController', function($scope, Fluro, FluroSocket, TypeService) {

    if (!$scope.item.definitionName) {
        $scope.$watch('item.title', function(newValue) {
            if (newValue) {
                $scope.item.definitionName = _.camelCase(newValue); //.toLowerCase();
            }
        })
    }

    if (!$scope.item.plural) {
        $scope.$watch('item.title', function(newValue) {
            if (newValue) {

                if (_.endsWith(newValue, 's')) {
                    $scope.item.plural = newValue + 'es';
                } else {
                    $scope.item.plural = newValue + 's';
                }
            }
        })
    }

    $scope.$watch('item.definitionName', function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.item.definitionName = $scope.item.definitionName.replace(regexp, '');
        }
    })

    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////

    $scope.cache = {
        buster: 1,
    }


    $scope.addReaction = function() {
        if (!$scope.item.triggerReactions) {
            $scope.item.triggerReactions = [];
        }

        $scope.item.triggerReactions.push({

        })
    }

    // $scope.previewURL = Fluro.apiURL + '/mailout/render/' + $scope.item.definitionName;
    // $scope.previewRefreshToken = 1;

    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////

    function socketRefresh(socketEvent) {


        var socketItemID = socketEvent.item;

        if (!socketItemID) {
            return;
        }

        if (socketItemID._id) {
            socketItemID = socketItemID._id;
        }

        //////////////////////////////////////////////

        //Same id content
        var sameID = (socketItemID == $scope.item._id);

        if (sameID) {
            $scope.cache.buster++;
            // console.log('UPDATE TO', $scope.cache.buster);
        }

    }

    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////

    $scope.$on("$destroy", function() {
        FluroSocket.off('content.edit', socketRefresh);
    });

    FluroSocket.on('content.edit', socketRefresh);

    ///////////////////////////////////////////////////////////////////



    ///////////////////////////////////////////////////////////////////


    if ($scope.definition && $scope.definition.data && $scope.definition.data.definitionSubType) {
        $scope.item.parentType = $scope.definition.data.definitionSubType;
    }

    //Set public by default for interaction types
    $scope.$watch('item.parentType', function(type) {
        if (type == 'interaction' && !$scope.item.privacy) {
            $scope.item.privacy = 'public'
        }
    })

    //Activate fields first instead of configuration
    if ($scope.item._id) {
        $scope.initFields = true;
    }

    /////////////////////////////////////////////////////////
    

    /////////////////////////////////////////////////////////


    $scope.definedTypes = TypeService.definedTypes;
    $scope.types = TypeService.types;

    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////








    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////

    // $scope.getFieldPath = function(target) {
    //     var trail = [];
    //     var result = {};
    //     getFieldPathTrail($scope.item.fields, target, trail, result);
    //     return 'data.' + result.trail.join('.');
    // }


    //  //////////////////////////////////////////

    // function getFieldPathTrail(array, target, trail, result) {
    //     //return $scope.getTrail($scope.model, field, []);

    //     for (var key in array) {
    //         var field = array[key];

    //         if (field == target) {

    //             if (!field.asObject && field.directive != 'embedded') {
    //                 trail.push(field.key);
    //             } else {

    //                 if (field.minimum == field.maximum == 1) {
    //                     trail.push(field.key);
    //                 } else {
    //                     trail.push(field.key + '[i]');
    //                 }

    //             }


    //             //trail.push(field.key);
    //             result.trail = trail.slice();
    //             return;

    //             //return callback(trail.slice());
    //         }

    //         if (field.fields && field.fields.length) {

    //             if (field.asObject || field.directive == 'embedded') {

    //                 if (field.minimum == field.maximum == 1) {
    //                     trail.push(field.key);
    //                 } else {
    //                     trail.push(field.key + '[i]');
    //                 }

    //             }

    //             getFieldPathTrail(field.fields, target, trail, result);

    //             if (field.asObject || field.directive == 'embedded') {
    //                 trail.pop();
    //             }

    //         }
    //     }
    // }


    /////////////////////////////////////////////////////////



    /////////////////////////////////////////////////

    $scope.addAlternativePaymentMethod = function() {
        if (!$scope.item.paymentDetails) {
            $scope.item.paymentDetails = {}
        }


        if (!$scope.item.paymentDetails.paymentMethods) {
            $scope.item.paymentDetails.paymentMethods = [];
        }

        $scope.item.paymentDetails.paymentMethods.push({
            title: 'New Payment Method'
        })
    }


    $scope.removeAlternativePaymentMethod = function(method) {
        _.pull($scope.item.paymentDetails.paymentMethods, method);
    }


    /////////////////////////////////////////////////

    $scope.addPaymentModifier = function() {
        if (!$scope.item.paymentDetails) {
            $scope.item.paymentDetails = {}
        }


        if (!$scope.item.paymentDetails.modifiers) {
            $scope.item.paymentDetails.modifiers = [];
        }

        $scope.item.paymentDetails.modifiers.push({})
    }


    $scope.removeModifier = function(entry) {
        _.pull($scope.item.paymentDetails.modifiers, entry);
    }

})