app.controller('DefinitionProcessController', function($scope, TypeService) {

    $scope.definedTypes = TypeService.definedTypes;
    $scope.types = TypeService.types;

});