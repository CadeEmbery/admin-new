app.controller('DefaultsRosterController', function($scope, $uibModal) {


    $scope.editSlot = function(slot) {

    	var parentScope = $scope;

        var modalInstance = $uibModal.open({
            template: '<default-roster-slot></default-roster-slot>',
            size: 'md',
            backdrop:'static',
            controller: function($scope) {
                $scope.slot = slot;
                
                $scope.remove = function(slot) {

                	parentScope.removeSlot(slot);

                	$scope.$close();
                }
                // $scope.closeModal = function() {
                //     console.log('close modal')
                //     $scope.$close();
                // }

                // $scope.removeTest = function() {
                //     $scope.$close();
                //     _.pull(tests, testEvent);
                // }
            }
        });

    }

    ////////////////////////////////////////////////////

    $scope.removeSlot = function(slot) {
    	return _.pull($scope.item.data.slots, slot);
    }

    ////////////////////////////////////////////////////

    $scope.createSlot = function() {


        //Check if a slots array is created
        var slots = _.get($scope.item, 'data.slots');
        if (!slots) {

            if(!$scope.item.data) {
                $scope.item.data = {}
            }


            $scope.item.data.slots = [];
        }

        ////////////////////////////////////////////////////

        var slot = {
            title: '',
            minimum: 1,
            maximum: 0,
            requireCapabilities: [],
            preferCapabilities: [],
        }

        ////////////////////////////////////////////////////

        // var existsAlready = _.some($scope.item.data.slots, function(slot) {
        //     var slotTitle = String(slot.title).toLowerCase();
        // })

        $scope.item.data.slots.push(slot);

        ////////////////////////////////////////////////////

        $scope.editSlot(slot);

    }


})