app.controller('ContactFormController', function($scope, CacheManager, $timeout, $rootScope, Notifications, Fluro, FluroContent, FluroAccess, FileUploader, TypeService, ModalService) {





    // if ($scope.extras) && $scope.extras.contactDetails) {
    //     $scope.details = $scope.extras.contactDetails;
    // } else {
    //     $scope.details = [];
    // }

    // console.log('OVERRIDE SAVING');


    $scope.editSheet = function(sheet) {
        ModalService.edit(sheet, function(res) {

            _.assign(sheet, res);
        })
    }


    $scope.addSheet = function(definition) {

        var params = {
            template: {
                realms: $scope.item.realms,
                contact: $scope.item,
            }
        };

        ModalService.create(definition.definitionName, params, function(res) {

            // console.log('CREATED!', res);
            res.fullDefinition = definition;

            $scope.extras.contactDetails.push(res);

            $scope.updateCreatableSheets();
        })
    }

    $scope.updateCreatableSheets = function() {
        $scope.createableSheets = _.filter($scope.extras.detailDefinitions, function(def) {
            return !_.some($scope.extras.contactDetails, { definition: def.definitionName });
        })
    }


    $scope.updateCreatableSheets();


    /////////////////////////////////////

    $scope.save = function(options) {

        console.log('PRESAVE DETAILS AND CLEAR CALENDAR')


        return $scope.contentSave(options);
    }



    /////////////////////////////////////

    $scope.dateOptions = {
        formatYear: 'yy',
        //startingDay: 1,
        showWeeks: false,
    };

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };


    $scope.photoReplace = {

    }


    $scope.deviceIcon = function(device) {

        if (device.platform == 'android') {
            return 'fa-mobile-android';
        } else {
            return 'fa-mobile';
        }
    }
    /////////////////////////////////////

    $scope.hasMetadata = _.keys($scope.item.data).length;


    /////////////////////////////////////

    $scope.removeDistinctions = function() {
        FluroContent.endpoint('contact/divide/' + $scope.item._id)
            .delete()
            .$promise
            .then(function(res) {


                Notifications.status($scope.item.distinctFrom.length + ' distinctions were removed from ' + $scope.item.firstName);

                CacheManager.clear('contact');
                //Clear the distinctions
                $scope.item.distinctFrom = [];
            }, function(err) {

                Notifications.error(err);
            })
    }

    /////////////////////////////////////

    $scope.subscriptions = {}

    if (_.get($scope, 'extras.unsubscribes.length')) {
        $scope.subscriptions.mailout = 'unsubscribed';
    }

    if (_.get($scope, 'extras.smsunsubscribes.length')) {
        $scope.subscriptions.sms = 'unsubscribed';
    }

    /////////////////////////////////////

    $scope.isSubscribed = function(key) {
        if ($scope.subscriptions[key] == 'unsubscribed') {
            return;
        } else {
            return true;
        }
    }




    /////////////////////////////////////

    $scope.isUnsubscribed = function(key) {
        return !$scope.isSubscribed(key);
    }

    /////////////////////////////////////

    $scope.toggleSubscription = function(key) {
        var isSubscribed = $scope.isSubscribed(key);
        if (isSubscribed) {
            $scope.unsubscribe(key);
        } else {
            $scope.resubscribe(key);
        }
    }


    /////////////////////////////////////

    $scope.unsubscribe = function(typeKey) {
        console.log('unsubscribe')
        $scope.subscriptions[typeKey] = 'processing';

        var postData = {};
        var url;

        switch (typeKey) {
            case 'sms':
                console.log('Phone Numbers different', $scope.item.phoneNumbers)
                url = 'sms/unsubscribe';
                postData.phoneNumbers = $scope.item.phoneNumbers;
                break;
            default:
                console.log('Emails different', $scope.item.emails)
                url = 'mailout/unsubscribe';
                postData.emails = $scope.item.emails;
                break;
        }
        FluroContent.endpoint(url)
            .save(postData)
            .$promise
            .then(function(res) {

                if (res.errors.length) {
                    $scope.subscriptions[typeKey] = null;
                }

                $scope.subscriptions[typeKey] = 'unsubscribed';

                Notifications.status($scope.item.firstName + ' was unsubscribed from receiving promotional ' + typeKey);
            }, function(err) {

                // Notifications.error($scope.item.firstName + ' was unsubscribed from receiving promotional ' + typeKey);
                $scope.subscriptions[typeKey] = null;
            })
    }

    /////////////////////////////////////

    $scope.resubscribe = function(typeKey) {
        console.log('resubscribe')
        $scope.subscriptions[typeKey] = 'processing';

        var postData = {};
        var url;

        switch (typeKey) {
            case 'sms':
                // console.log('Phone Numbers different', $scope.item.phoneNumbers)
                url = 'sms/resubscribe';
                postData.phoneNumbers = $scope.item.phoneNumbers;
                break;
            default:
                // console.log('Emails different', $scope.item.emails)
                url = 'mailout/resubscribe';
                postData.emails = $scope.item.emails;
                break;
        }

        FluroContent.endpoint(url)
            .save(postData)
            .$promise
            .then(function(res) {

                if (res.errors.length) {
                    $scope.subscriptions[typeKey] = null;
                }

                delete $scope.subscriptions[typeKey];

                Notifications.status($scope.item.firstName + ' was updated to allow promotional ' + typeKey);
            }, function(err) {

                // Notifications.error($scope.item.firstName + ' was unsubscribed from receiving promotional ' + typeKey);
                $scope.subscriptions[typeKey] = null;
            })
    }



    /////////////////////////////////////


    $scope.resetDevices = function() {
        FluroContent.endpoint('push/clear/' + $scope.item._id).get().$promise.then(function(res) {

            $scope.devices = [];

            Notifications.status('Devices reset for ' + $scope.item.firstName);
        }, function(err) {
            Notifications.error(err);
        })
    }

    $scope.ping = function(deviceID) {

        FluroContent.endpoint('push/ping/' + deviceID).get().$promise.then(function(res) {
            Notifications.status('Ping notification sent');
        }, function(err) {
            Notifications.error(err);
        })
    }

    /////////////////////////////////////



    $scope.createUser = function() {
        var contact = $scope.item;
        var realms = angular.copy(contact.realms);

        ///////////////////////////////////////////


        //Create new template
        var template = {
            //name: item.firstName + ' ' + item.lastName,
            firstName: contact.firstName,
            lastName: contact.lastName,
            collectionEmail: _.first(contact.emails),
            realms: realms
        }

        //Popup and create
        ModalService.create('persona', {
            template: template
        }, function(result) {
            console.log('Success', result);
            $scope.extras.personas = [];

            result.email = result.collectionEmail;
            $scope.extras.personas.push(result);

        }, function(result) {
            console.log('Fail', result)
        });
    }

    ///////////////////////////////////////////////

    $scope.canCreateUser = $scope.item._id && FluroAccess.can('create', 'persona');
    $scope.canDefine = $scope.item._id && FluroAccess.can('create', 'definition');


    //////////////////////////////////////////////////


    $scope.defineNew = function() {

        var params = {
            template: {
                parentType: 'contactdetail',
            }
        };

        ModalService.create('definition', params, function(res) {

            console.log('Definition', res);

            if(!$scope.extras.detailDefinitions) {
                $scope.extras.detailDefinitions =[];
            }
            $scope.extras.detailDefinitions.push(res);

            //Update the view
            $scope.updateCreatableSheets();
        });
    }



    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////

    $scope.uploader = new FileUploader({
        url: Fluro.apiURL + '/contact/' + $scope.item._id + '/image',
        //removeAfterUpload: true,
        withCredentials: true,
        formData: {},
    });


    $scope.getImageURL = function() {
        return $rootScope.contactAvatarURL($scope.item._id) + '?v=' + $scope.photoReplace.uuid; // + '&forceRefresh=true';
    }

    // var forceRefresh = false;

    $scope.saveAndUpload = function() {
        // console.log('Save and Upload');

        // if (!$scope.item || !$scope.item.realms || !$scope.item.realms.length) {
        //     return Notifications.warning('Please select a realm before attempting to upload');
        // }

        $scope.uploader.uploadAll();

    }

    /////////////////////////////////

    function refreshAfterUpload(response) {
        $scope.photoReplace.uuid = new Date();
        $scope.photoReplace.show = false;
    }


    /////////////////////////////////

    $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
        switch (status) {
            case 200:
                return refreshAfterUpload(response);
                break;
            default:
                //Failed
                Notifications.error(response);
                break;
        }
    };


    // if (!$scope.item.assetType && $scope.type.path != 'video' && $scope.type.path != 'audio') {
    //     $scope.item.assetType = 'upload';
    // }



    $scope.devices = _.uniq($scope.extras.devices, function(device) {
        return _.get(device, 'metadata.uuid');
    })


    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////

    $scope.edit = function(item) {
        ModalService.edit(item);
    }

    /////////////////////////////////////

    // $scope.getCreateableDetails = function() {
    //     return _.filter(TypeService.definedTypes, function(def) {
    //         var correctType = def.parentType == 'contactdetail';
    //         var exists = _.some($scope.details, function(existing) {
    //             return existing.definition == def.definitionName;
    //         })

    //         return (correctType && !exists);
    //     });
    // }

    /////////////////////////////////////

    // $scope.createDetailSheet = function(sheet) {

    //     var options = {};
    //     options.template = {
    //         contact: $scope.item,
    //     }

    //     ModalService.create(sheet.definitionName, options, function(result) {
    //         $scope.details.push(result);
    //     })

    // }

    /////////////////////////////////////

    $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item);
    }

    /////////////////////////////////////


    if ($scope.item._id) {
        // FluroContent.endpoint('info/checkins').query({
        //     contact:$scope.item._id
        // }).$promise.then(function(res) {
        //     $scope.checkins = res;
        // });


        // FluroContent.endpoint('info/interactions').query({
        //     contact:$scope.item._id
        // }).$promise.then(function(res) {
        //     $scope.interactions = res;
        // });

        ////////////////////////////////////////

        // FluroContent.endpoint('contact/details/' + $scope.item._id, true, true).query().$promise
        // .then(function(res) {
        //     $scope.details = res;
        // });

        ////////////////////////////////////////

        // FluroContent.endpoint('contact/' + $scope.item._id + '/persona', true, true).get().$promise
        // .then(function(res) {
        //     $scope.persona = res;
        // });

        ////////////////////////////////////////

        // FluroContent.endpoint('info/newassignments').query({
        //     contact:$scope.item._id
        // }).$promise.then(function(res) {

        //     var today = new Date();

        //     $scope.pastassignments = _.filter(res, function(assignment) {
        //         if(!assignment.event) {
        //             return;
        //         }
        //         return (new Date(assignment.event.startDate) < today);
        //     });

        //     $scope.upcomingassignments = _.filter(res, function(assignment) {
        //         if(!assignment.event) {
        //             return;
        //         }
        //         return (new Date(assignment.event.startDate) >= today);
        //     });

        // });
    }

})