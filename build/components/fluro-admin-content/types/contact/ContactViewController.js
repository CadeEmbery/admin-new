app.controller('ContactViewController', function($scope, $rootScope, $state, $uibModal, Fluro, FluroContent, FluroAccess, Notifications, TypeService, ModalService) {



    console.log($scope);

    /////////////////////////////////////////////////////////
    
    $scope.vcardURL = Fluro.apiURL + '/contact/' + $scope.item._id + '/vcard.vcf';

    /////////////////////////////////////////////////////////

    $scope.sendSMS = function() {

        function success(res) {
            console.log('Message Success', res)
        }

        function failure(err) {
            console.log('Message Error', err)
        }

        ModalService.sms([$scope.item._id], success, failure);
    }



   
    /////////////////////////////////////////////////////////

    $scope.firstNumber = function() {

        return _.chain([
            _.get($scope.item, 'local'),
            _.get($scope.item, 'phoneNumbers'),
            _.get($scope.item, 'family.phoneNumbers'),
        ])
        .flatten()
        .compact()
        .first()
        .value()

    }

    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////

    $scope.openGroupManager = function() {
        var modalInstance = $uibModal.open({
            templateUrl: 'admin-group-modal/admin-group-modal.html',
            controller: 'GroupModalController',
            size:'lg',
            resolve: {
                contacts: function() {
                    return [$scope.item];
                },
                groups: function() {
                    return FluroContent.resource('team').query({
                        fields: ['title', 'firstLine', 'realms', 'definition', '_type'],
                        allDefinitions: true
                    }).$promise;
                }
            }
        });
    }



    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////


     $scope.groupActions = function(group) {

        
        var possibleActions = [];

        possibleActions.push({
            title: 'Remove from Group',
            description: 'Remove ' + $scope.item.firstName + ' from ' + group.title + '?',
            key: 'remove',
        })

        console.log('Actions', possibleActions);

        var promise = ModalService.actions(possibleActions, group.title);

        ////////////////////////////////////////

        promise.then(function(action) {
            switch(action.key) {
                case 'remove':

                // router.delete('/:teamID/leave/:contactID', teamMiddleware('leave'), function(req, res) {

                    FluroContent.endpoint('teams/' + group._id + '/leave/' + $scope.item._id).delete().$promise.then(function(res) {
                        Notifications.status('Removed ' + $scope.item.firstName + ' was removed from ' + group.title);
                        
                        _.pull($scope.item.teams, group);
                    });
                break;
            }
        });
    }


    /////////////////////////////////////////////////////////

    $scope.sendEmail = function() {

        function success(res) {
            console.log('Email Success', res)
        }

        function failure(err) {
            console.log('Email Error', err)
        }

        ModalService.email([$scope.item._id], success, failure);
    }

    //////////////////////////////////////////////////////////////

    $scope.capabilities = groupByDefinition($scope.item.capabilities, 'capability');
    $scope.tags = groupByDefinition($scope.item.tags, 'tag');
    $scope.realms = _.chain($scope.item.realms)
    .filter(function(realm) {
        return !realm._discriminatorType
    })
    .sortBy(function(realm) {
        return realm.title;
    })
    .value();
    //groupByDefinition($scope.item.realms, 'realm');
    $scope.locations = groupByDefinition($scope.item.locations, 'location');


    function groupByDefinition(array, pathName) {
        return _.chain(array)
            .sortBy(function(capability) {
                return $rootScope.definitionName(capability.title, true);
            })
            .reduce(function(set, capability) {

                var key = capability.definition || pathName;
                var generic = (key ==  pathName);
                var existing = set[key];
                var formattedTitle = $rootScope.definitionName(key, true);

                if (!existing) {
                    existing =
                        set[key] = {
                            key:key,
                            title: formattedTitle,
                            items: []
                        }
                }

                existing.items.push(capability);

                return set;
            }, {})
            .values()
            .sortBy(function(entry) {
                if(entry.key == pathName) {
                    return 1;
                } else {
                    return 0;
                }
            })
            .value();
    }



    //////////////////////////////////////////////////////////////


    $scope.activeStateTitle = function(card) {
        var currentState = _.find(card.states, {
            key: card.state
        });

        if (!currentState) {
            return;
        }

        return currentState.title;
    }



    $scope.processPercentage = function(card) {
        var activeIndex = _.findIndex(card.states, {
            key: card.state
        });


        return Math.round(((activeIndex + 1) / card.states.length) * 100);
    }




    // FluroContent.endpoint('info/progress/' + $scope.item._id).query()
    // .$promise.then(function(res) {
    //     $scope.interactions = res;
    // });




    // FluroContent.endpoint('process/states/' + $scope.item._id).query().$promise.then(function(res) {
    //     $scope.processes = res;
    // });


    // FluroContent.endpoint('contact/details/' + $scope.item._id, true, true).query().$promise
    //     .then(function(res) {
    //         $scope.details = res;
    //     });

    // FluroContent.endpoint('contact/relationships/' + $scope.item._id, true, true).query().$promise
    //     .then(function(res) {
    //         $scope.relationships = res;
    //     });

    if($scope.item.academicCalendar) {
        var match = _.find($scope.item.academicCalendar.grades, {key:$scope.item.academicGrade});
        $scope.academicGrade = match ? match.title : null;
    }
    //     
    $scope.relationships = _.chunk($scope.extras.relationships, 2);
    $scope.processes = $scope.extras.processStates;
    $scope.details = $scope.extras.contactDetails;
    // _.filter($scope.extras.contactDetails, function(sheet) {
    //     return sheet.status != 'archived';
    // });

    // console.log('Got details', $scope.details.length);

    /////////////////////////////////////////////////////////

    $scope.viewDetail = function(sheet) {
            ModalService.view(sheet);
        }
        // $root.viewItem(team, $dismiss, 'team')

    /////////////////////////////////////////////////////////

    $scope.viewTeam = function(team) {

        console.log('GO FOR IT', team);

        // $rootScope.viewItem(team, $scope.$dismiss, 'team');

        // console.log('View team', team)
        $state.go('team.view', {
            id: team._id,
            definitionName: team.definition
        });
    }


})