app.directive('contactImageReplaceForm', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        templateUrl: 'fluro-admin-content/types/contact/contact-image-replace-form.html',
        controller: 'ContactImageReplaceController',
    };
});


app.controller('ContactImageReplaceController', function($scope, FluroContent, Fluro, ModalService) {


    /////////////////////////////////

/*
    if (!$scope.item._id) {
        $scope.photoReplace.show = true;
    }
*/

   
    /////////////////////////////////
    /////////////////////////////////
    /////////////////////////////////


    $scope.selectExisting = function() {

        var params = {};
        var modelSource = {};


        var typeName = 'image';

        //Limits
        params.minimum =
        params.maximum = 1;

        var modal = ModalService.browse(typeName, modelSource, params);


        /////////////////////////////////////

        function modalClosed() {
           

            var ID = _.chain(modelSource.items)
            .map(function(item) {
                if(item._id) {
                    return item._id;
                }

                return item;
            })
            .first()
            .value();

            /////////////////////////////////

            function success(res) {
                console.log('Success', res);
                $scope.photoReplace.uuid = new Date();
            }

            function failed(err) {
                console.log('Error', err);
            }

            /////////////////////////////////
            /////////////////////////////////

            FluroContent.endpoint('contact/' + $scope.item._id + '/image').update({
                image:ID
            })
            .$promise
            .then(success, failed);
        }

        /////////////////////////////////////


        modal.result.then(modalClosed, modalClosed)

    }

    /////////////////////////////////
    /////////////////////////////////
    /////////////////////////////////

    // $scope.showReplaceDialog = function() {
    //     $scope.photoReplace.show = true;
    // }

    /////////////////////////////////

});