

app.service('QueryExportService', function(Fluro, Notifications, FluroContent) {


    var controller = {};

    /////////////////////////////////////////////

    controller.export = function(queryID, type, ids) {
        switch (type) {
            case 'csv':

                //////////////////////////////////////////////////////

                function exportComplete(res) {

                    if (res.download) {
                        console.log('Opening', Fluro.apiURL + res.download)
                        Notifications.post('Your popup blocker may stop this file from downloading', 'warning');

                        if (Fluro.token) {
                            window.open(Fluro.apiURL + res.download + '?access_token=' + Fluro.token);
                        } else {
                            window.open(Fluro.apiURL + res.download);
                        }
                    }

                }

                function exportFailed(err) {
                    console.log(err)
                    Notifications.post(err, 'error');
                }

                //////////////////////////////////////////////////////

                var parameters = {};

                if(ids && ids.length) {
                    parameters.ids = ids;
                }
                
                //////////////////////////////////////////////////////

                var promise = FluroContent.endpoint('export/query/' + queryID + '/' + type, false, true)
                    .save(parameters)
                    .$promise;

                promise.then(exportComplete, exportFailed);

                return promise;
                break;
        }
    }
    
    /////////////////////////////////////////////

    return controller;
})