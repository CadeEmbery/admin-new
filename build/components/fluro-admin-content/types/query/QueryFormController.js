app.directive('json', function() {
    return {
        restrict: 'A', // only activate on element attribute
        require: 'ngModel', // get a hold of NgModelController
        link: function(scope, element, attrs, ngModelCtrl) {

            var lastValid;

            // push() if faster than unshift(), and avail. in IE8 and earlier (unshift isn't)
            ngModelCtrl.$parsers.push(fromUser);
            ngModelCtrl.$formatters.push(toUser);

            // clear any invalid changes on blur
            element.bind('blur', function() {
                element.val(toUser(scope.$eval(attrs.ngModel)));
            });

            // $watch(attrs.ngModel) wouldn't work if this directive created a new scope;
            // see http://stackoverflow.com/questions/14693052/watch-ngmodel-from-inside-directive-using-isolate-scope how to do it then
            scope.$watch(attrs.ngModel, function(newValue, oldValue) {
                lastValid = lastValid || newValue;

                if (newValue != oldValue) {
                    ngModelCtrl.$setViewValue(toUser(newValue));

                    // TODO avoid this causing the focus of the input to be lost..
                    ngModelCtrl.$render();
                }
            }, true); // MUST use objectEquality (true) here, for some reason..

            function fromUser(text) {
                // Beware: trim() is not available in old browsers
                if (!text || text.trim() === '') {
                    return {};
                } else {
                    try {
                        lastValid = angular.fromJson(text);
                        ngModelCtrl.$setValidity('invalidJson', true);
                    } catch (e) {
                        ngModelCtrl.$setValidity('invalidJson', false);
                    }
                    return lastValid;
                }
            }

            function toUser(object) {
                // better than JSON.stringify(), because it formats + filters $$hashKey etc.
                return angular.toJson(object, true);
            }
        }
    };
});




app.controller('QueryFormController', function($scope, Fluro, FluroContent) {


    var starterQuery = {
        "_type": "contact",
        "status": "active",
        "dob": {
            "$lte": "date('-18 years')"
        }
    }

    /////////////////////////////////////////////////

    if (!$scope.item.query || !$scope.item.query.length) {
        $scope.item.query = '//Find all active contacts over the age of 18\n' + JSON.stringify(starterQuery, null, 2)
    }

    // $scope.$watch('item.query', refreshTest, true);

    $scope.save = function(options) {

        refreshTest();
        $scope.contentSave(options);
    }

    if (!$scope.settings) {
        $scope.settings = {}
    }


    /////////////////////////////////////////////////



    $scope.docs = []

    $scope.docs.push({
        title: 'Dynamic Dates',
        help: "To use dynamic dates in your query you can use relative dates like date('now'), date('+1 week'), date('-8 hours') or absolute dates like date('1/5/2015')",
        example: JSON.stringify({
            dob: {
                "$gte": "date('-18 years')"
            },
            startDate: {
                "$lt": "date('now')"
            }
        }, null, 2),
    })

    /////////////////////////////////////////////////

    $scope.docs.push({
        title: 'Dynamic Variables',
        help: "Variables can be provided at exection time to change the query criteria dynamically. /content/_query/:id?variables[yourVariableName]=Jeff",
        example: JSON.stringify({
            firstName: "variable(yourVariableName)",
        }, null, 2),
    })


    $scope.docs.push({
        title: 'User Variables',
        help: "Data provided by the requesting user can also be used within your query to get the current users account, id, timezone, and related contact profiles",
        example: '//These variables\n' +
            JSON.stringify({
                account: "USER_ACCOUNT",
                _id: "USER_ID",
                persona: "USER_PERSONA",
                contacts: {
                    "$in": "USER_CONTACTS",
                },
                parentAccounts: {
                    "$nin": "USER_PARENT_ACCOUNTS",
                },
                timezone: "USER_ACCOUNT_TIMEZONE",
            }, null, 2) +
            ' \n\n //Are converted to become \n' +
            JSON.stringify({
                account: "5757c3f9999019975bae3955",
                _id: "55d819433e620604ea8c6d62",
                persona: "55d819433e620604ea8c6d62",
                contacts: {
                    "$in": [
                        "5aa213cb6883e77328d67fc9",
                        "5ae5124e124edf6048c507dd",
                    ],
                },
                parentAccounts: {
                    $nin: ["5757c3f9999019975bae3955"]
                },
                timezone: "Australia/Sydney",
            }, null, 2),
    })



    $scope.docs.push({
        title: 'Stats',
        help: "If you want to use 'stats' as query critera, eg. find all posts that a user has 'liked' you can use the 'stats' placeholder",
        example: '//These stats variables\n' +
            JSON.stringify({
                _id: {
                    "$in":"stats('_like.ids')",
                },
            }, null, 2) +
            ' \n\n //Are converted to become \n' +
            JSON.stringify({
       
                _id: {
                    $in:["55d819433e620604ea8c6d62", "5757c3f9999019975bae3955"],
                },
            }, null, 2),
    })

    /////////////////////////////////////////////////

    $scope.aggregateDocs = []






    $scope.aggregateDocs.push({
        title: 'Lookups',
        help: "Cross reference data from other tables",
        example: JSON.stringify({
            "$lookup": {
                "from": "nodes",
                "let": {
                    "contactID": "$_id"
                },
                "pipeline": [{
                    "$match": {
                        "$expr": {

                            "$and": [{
                                    "$or": [{
                                        "$in": ["$$contactID", "$provisionalMembers"]
                                    }, {
                                        "$in": ["$$contactID", "$assignments.contacts"]
                                    }, ]
                                }, {
                                    "$eq": ["$account", "USER_ACCOUNT"]
                                }, {
                                    "$eq": ["$_type", "team"]
                                }, {
                                    "$eq": ["$status", "active"]
                                },

                            ]
                        }
                    }
                }, {
                    "$project": {
                        "title": 1,
                        "definition": 1,
                    }
                }],
                "as": "teams",
            }
        }, null, 2)
    })



    /////////////////////////////////////////////////

    function refreshTest(force) {



        var isAggregate = $scope.item.aggregation;

        if (force || !isAggregate) {
            $scope.settings.testing = true;

            var request = FluroContent.resource('_query/' + $scope.item._id, true, true).query({
                limit: 10,
                sample:true,
            }).$promise;

            request.then(function(results) {
                console.log('RESULTS', results);
                $scope.testItems = results;
                $scope.settings.testing = false;
            }, function(err) {
                $scope.testItems = null;
                $scope.settings.testing = false;
            })
        }
    }

    /////////////////////////////////////////////////

    refreshTest();

    /////////////////////////////////////////////////

    $scope.forceLoad = function() {
        refreshTest(true);
    }

    /////////////////////////////////////////////////

    // if($scope.item._id) {
    $scope.getPreviewLink = function(test) {

        var url = Fluro.apiURL + '/content/_query/' + $scope.item._id + '?noCache=true';

        if (Fluro.token) {
            url += '&access_token=' + Fluro.token;
        }

        if (test) {
            url += '&showQuery=true';
        }

        return url;
    }


    $scope.addPipelineStep = function() {

        if (!$scope.item.aggregationPipeline) {
            $scope.item.aggregationPipeline = [];
        }

        $scope.item.aggregationPipeline.push({
            key: '$unwind',
            criteria: '$contacts',
        })
    }

    $scope.removePipelineStep = function(step) {
        _.pull($scope.item.aggregationPipeline, step);
    }

    // }
    /*
    if (!$scope.item.data) {
        $scope.item.data = {}
    }

    $scope.getObjectAsText = function() {

        if ($scope.item.data) {
            $scope.textAreaModel = JSON.stringify($scope.item.data);
        } else {
            $scope.textAreaModel = '{}';
        }
    };


    $scope.$watch('textAreaModel', function(n) {

        if (n) {
            try {
                $scope.item.data = JSON.parse($scope.textAreaModel);
            } catch (err) {
                //Exception handler
                console.log('Exception', err)
            }
        }
    });

    /*
    var aceEditor;

    $scope.aceLoaded = function(_editor) {
        // Options
        //_editor.setReadOnly(true);
        aceEditor = _editor;
        aceEditor.setTheme("ace/theme/tomorrow_night_eighties");
        updateSyntax();
    };

    function updateSyntax() {
        if (aceEditor) {

            var syntaxName
            switch($scope.item.syntax) {
                case 'js':
                    syntaxName = 'javascript';
                break;
                default:
                syntaxName = $scope.item.syntax;
                break;
            }

            if(syntaxName) {
                aceEditor.getSession().setMode("ace/mode/" +syntaxName);
            }
        }
    }

    $scope.$watch('item.syntax', updateSyntax)

    $scope.aceChanged = function(e) {
        //
    };
    */

});