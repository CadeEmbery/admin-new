app.controller('QueryViewController', function($rootScope, $http, $timeout, PathfinderService, TypeService, $q, FlattenService, FluroStorage, Selection, $state, $scope, QueryExportService, Notifications, Fluro, FluroContent, $filter) {

    console.log('Hello!!!')

    if (!$scope.settings) {
        $scope.settings = {};
    }
    $scope.rawQuery = '';

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    //Setup Local storage
    var local = FluroStorage.localStorage('query-' + $scope.item._id)
    var session = FluroStorage.sessionStorage('query-' + $scope.item._id)


    ///////////////////////////////////////////////




    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    //Setup Search    
    if (!session.search) {
        $scope.search = session.search = {}
    } else {
        $scope.search = session.search;
    }

    ///////////////////////////////////////////////

    $scope.clearFilters = function() {
        $scope.search.filters = {};
        $scope.search.ranges = {};
        $scope.search.dates = {};
        $scope.search.months = {};
        $scope.search.weeks = {};
        $scope.search.years = {};
    }


    //////////////////////////////////////////////////////////////

    $scope.filtersActive = function() {
        var filterKeys = _.keys($scope.search.filters);
        var rangeKeys = _.keys($scope.search.ranges);
        var dateKeys = _.keys($scope.search.dates);
        var weekKeys = _.keys($scope.search.weeks);
        var monthKeys = _.keys($scope.search.months);
        var yearKeys = _.keys($scope.search.years);
        return filterKeys.concat(rangeKeys, dateKeys, weekKeys, monthKeys, yearKeys).length;
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    //Setup Selection    
    if (!session.selection) {
        session.selection = []
    }

    $scope.selection = new Selection(session.selection, $scope.items, function(item) {
        return true
    });


    ///////////////////////////////////////////////

    $scope.selectPage = function() {
        $scope.selection.selectMultiple($scope.pageItems);
    }

    ///////////////////////////////////////////////

    $scope.deselectFiltered = function() {
        $scope.selection.deselectMultiple($scope.filteredItems);
    }

    ///////////////////////////////////////////////

    $scope.togglePage = function() {
        if ($scope.pageIsSelected()) {
            $scope.deselectPage();
        } else {
            $scope.selectPage();
        }
    }

    $scope.deselectPage = function() {
        $scope.selection.deselectMultiple($scope.pageItems);
    }

    $scope.pageIsSelected = function() {
        return $scope.selection.containsAll($scope.pageItems);
    }


    $scope.loaded = false;

    ///////////////////////////////////////////////

    var stop = $scope.$watch('items', function(items) {

        if (items && items.length) {
            var typeName = _.chain(items)
                .first()
                .get('_type')
                .value();

            $scope.definitions = TypeService.getSubTypes(typeName);
            return stop();
        }

    })

    ///////////////////////////////////////////////

    // $timeout(function() {


        $scope.$watch(function() {
            return JSON.stringify($scope.item.variableData);
        }, _.debounce(function(data) {
            console.log('REQUEST NOW DUE TO CHANGE', data);
            $scope.requestQuery();
        }, 1000));


    // }, 1000)




    ///////////////////////////////////////////////////////////////////////////////

    //Create a canceller
    var inflightRequests = [];

    ///////////////////////////////////////////////////////////////////////////////

    $scope.requestQuery = function() {

        console.log('REQUEST QUERY')

        _.each(inflightRequests, function(promise) {
            console.log('Cancel')
            promise.cancelled = true;
            promise.resolve();
        });

        inflightRequests.length = 0;

        /////////////////////////////////////////////

        $scope.settings.refreshing = true;

        ///////////////////////////////////////////////////////////////////////////////

        function arrayIDs(array, asObjectID) {

            if (!array) {
                return array;
            }

            return _.chain(array)
                .compact()
                .map(function(input) {
                    return getStringID(input, asObjectID);
                })
                .compact()
                .uniq()
                .value();

        }



        ///////////////////////////////////////////////////////////////////////////////

        //Helper function to get a basic ID!
        function getStringID(input) {


            if (!input) {
                return input;
            }

            /////////////////////////////////

            var output;

            if (input._id) {
                output = String(input._id);
            } else {
                output = String(input);
            }

            return output;
        }


        ///////////////////////////////////////////////////////

        function mapRecursiveKeys(data, collection, trail) {



            _.each(data, function(dataValue, dataKey) {

                trail.push(dataKey);

                var output = dataValue;

                if (_.isArray(dataValue)) {
                    if (dataValue.length) {
                        if (dataValue[0] && dataValue[0]._id) {
                            output = arrayIDs(dataValue).join(',');
                        } else {
                            mapRecursiveKeys(dataValue, collection, trail);
                        }
                    }
                }


                if (_.isObject(dataValue)) {
                    if (dataValue._id) {
                        output = getStringID(dataValue);
                    } else {
                        mapRecursiveKeys(dataValue, collection, trail);
                    }
                }

                //Flat key
                collection[trail.join('.')] = output;
                // _.set(collection, trail.join('.'), output);

                trail.pop();
            })
        }

        ///////////////////////////////////////////////////////

        var flattenedData = {}
        var trail = [];

        ///////////////////////////////////////////////////////

        mapRecursiveKeys({ data: $scope.item.variableData }, flattenedData, trail);

        ///////////////////////////////////////////////////////

        // var flattenedData =  PathfinderService.flatten({data:$scope.item.variableData});

        // // var flattenedData = FlattenService.flatten({ data: $scope.item.variableData })

        var variableString = _.map(flattenedData, function(value, key) {
            return 'variables[' + key + ']=' + value;
        }).join('&');

        function failed(err) {
            console.log('ERROR', err);
            $scope.loaded = true;
            $scope.settings.refreshing = false;
        }

        function createCancel() {
            var cancelPromise = $q.defer();
            inflightRequests.push(cancelPromise);
        }

        // var queryParams = _.map(params, function(v, k) {
        //     return encodeURIComponent(k) + '=' + encodeURIComponent(v);
        // }).join('&');

        $http.get(Fluro.apiURL + '/content/_query/' + $scope.item._id + '?showQuery=true&' + variableString, {
                timeout: createCancel(),
            })
            .then(function(res) {
                $scope.rawQuery = res.data;

                requestResults();
            }, function(err) {
                $scope.rawQuery = err;
                failed(err);
            });


        function requestResults() {

            // console.log('Request Results!');
            // $scope.loaded = false;
            
            //Run the query and return it
            var promise = FluroContent.endpoint('content/_query/' + $scope.item._id + '?' + variableString, {
                    timeout: createCancel(),
                }, true)
                .query({
                    noCache:true
                })
                .$promise;

            ///////////////////////////////////////////////

            promise.then(function(res) {

                if(promise.cancelled) {
                    return;
                }

                console.log('Got the stuff')


                //Get the results
                $scope.items = _.map(res, function(item) {
                    item.trackID = guid(); //item._id;
                    return item;
                });

                $scope.renderView();
                $scope.loaded = true;
                $scope.settings.refreshing = false;

            }, failed);
        }
    }


    //One for good measure
    // $scope.requestQuery();

    ///////////////////////////////////////////////


    ///////////////////////////////////////////////



    $scope.reload = function() {
        $scope.requestQuery();
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    //EXPORT TO JSON
    $scope.apiLink = function() {

        var url = Fluro.apiURL + '/content/_query/' + $scope.item._id + '?noCache=true';

        if (Fluro.token) {
            url += '&access_token=' + Fluro.token;
        }

        return url;
    }

    //EXPORT TO CSV
    $scope.export = function(type) {
        QueryExportService.export($scope.item._id, type, $scope.selection.ids);
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    $scope.pager = {
        limit: 100,
        maxSize: 8,
    }


    ///////////////////////////////////////////////

    function unwindColumn(key, renderer) {

        //Loop through each result row and get all the possible results


        //Loop through each row
        _.each($scope.items, function(row) {


            //Get the column value and see if it's an array
            var columnEntry = _.get(row, key);

            //If it's not an array
            if (!_.isArray(columnEntry)) {
                return;
            }

            //Map onto the keys
            _.each(columnEntry, function(entry) {
                _.set(row, 'keyed[' + key + ']' + entry.k, entry.v);
            })
        });


        ////////////////////////////////////////////////////

        var allValueColumns = _.chain($scope.items)
            .map(function(row) {
                var value = _.get(row, key);

                if (!_.isArray(value)) {
                    return value;
                }

                //Return an entry for each unwound value
                return value;
            })
            .compact()
            .flatten()
            .uniq(function(col) {
                return col.k;
            })
            .map(function(col, index) {
                return {
                    title: col.k,
                    key: 'keyed[' + key + ']' + col.k, //key + '[' + index + '].v', //eventColumns[3].v
                    s: col.s,
                    renderer: renderer,
                }
            })
            .sortBy(function(col) {
                return col.s;
            })
            // .map(function(col, index) {

            //     //Map the right index
            //     col.key = key + '[' + index + '].v';
            //     return col;
            // })

            .value()

        ///////////////////////////////////////////////




        // //Now we have allValueColumns we need to map them
        // _.each(allValueColumns, function(column, columnIndex) {

        //     _.each($scope.items, function(row) {

        //         row[columnIndex] =
        //     });


        // })

        ///////////////////////////////////////////////


        return allValueColumns;
    }

    ///////////////////////////////////////////////

    $scope.renderView = function() {
        $scope.renderableColumns = _.chain($scope.item.columns)
            .map(function(column) {

                var string = column.renderer;
                if (string && string.length) {
                    string = string.split('.');
                } else {
                    string = '';
                }

                //If its a columns entry
                switch (string[0]) {
                    case 'columns':
                        var columns = unwindColumn(column.key, string[1])
                        return columns;
                        break;
                }

                return column;
            })
            .flatten()
            .map(function(column) {


                column.trackID = guid();

                // console.log('COLUMN', column)
                return column;
            })
            .value();
        $scope.updateFilters();
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////



    function updateCharts() {

        var resultItems = $scope.filteredItems;

        $scope.charts = _.map($scope.item.charts, function(chartConfig) {

            var chart = chartConfig;

            /////////////////////////////////////////////////

            var cells = _.chain(resultItems)
                .map(function(item) {
                    var cell = _.get(item, chart.primaryData);

                    if (_.isArray(cell)) {
                        cell = _.chain(cell)
                            .flatten()
                            .value();
                    }




                    return cell;
                })
                .flattenDeep()
                .compact()
                .value()





            /////////////////////////////////////////////////

            chart.data = _.chain(cells)
                .map(function(cell) {
                    if (cell._id) {
                        return cell._id;
                    }

                    return cell;
                })
                .countBy(function(i) {
                    return i;
                })
                .values()
                .value();

            /////////////////////////////////////////////////

            chart.labels = _.chain(cells)
                .map(function(cell) {
                    if (cell.title) {
                        return cell.title;
                    }

                    return cell;
                })
                .groupBy(function(i) {
                    return i;
                })
                .keys()
                .value();


            /////////////////////////////////////////////////

            chart.colors = _.chain(cells)
                .map(function(cell) {
                    if (cell.bgColor) {
                        return cell.bgColor;
                    }

                    return cell;
                })
                .groupBy(function(i) {
                    return i;
                })
                .keys()
                .value();

            /////////////////////////////////////////////////

            /**
            chart.data = _.chain(resultItems)
                .map(function(item) {
                    var cell = _.get(item, chart.primaryData);

                    if (_.isArray(cell)) {
                        cell = _.chain(cell)
                            .flatten()
                            .value();
                    }

                    return cell;
                })
                .flattenDeep()
                .compact()
                .map(function(cell) {
                    if (cell._id) {
                        return cell._id;
                    }

                    return cell;
                })
                .countBy(function(i) {
                    return i;
                })
                .values()
                .value()



            chart.labels = _.chain(resultItems)
                .map(function(item) {
                    var cell = _.get(item, chart.primaryData);

                    if (_.isArray(cell)) {
                        cell = _.chain(cell)
                            .flatten()
                            .value();
                    }

                    return cell;
                })
                .flattenDeep()
                .compact()
                .map(function(cell) {
                    if (cell.title) {
                        return cell.title;
                    }

                    return cell;
                })
                .groupBy(function(i) {
                    return i;
                })
                .keys()
                .value()



                chart.colors = [
                    '#ffb5ba',
                    "#ffa3c4",
                    '#e7b3ff',
                    '#e2a3ff',
                    '#c6a3ff',
                    '#a5a3ff',
                    '#a6caff',
                    '#9ee2ff',
                    '#008b94',
                    '#97f0cb',
                    '#97f0a5',
                    '#a8ed87',
                    '#dbe37f',
                    '#e3cd7f',
                    '#ffe175',
                    '#faff00',
                ];

/**/

            return chart;
        })
    }
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////


    $scope.$watch('search', function() {
        $scope.updateFilters();
    }, true);

    //////////////////////////////

    $scope.setOrder = function(string, orderRenderer) {
        $scope.search.order = string;
        $scope.search.orderRenderer = orderRenderer;
        // $scope.updateFilters();
        $scope.updateSort();
    }

    $scope.setReverse = function(bol) {
        $scope.search.reverse = bol;
        // $scope.updateFilters();
        $scope.updateSort();
    }

    ///////////////////////////////////////////////

    var debounceTimer;

    $scope.updateFilters = function() {

        //Disable the debounce for now
        //as all it does is make things feel slower
        return searchChanged();

        if(debounceTimer) {
            $timeout.cancel(debounceTimer);
        }

        debounceTimer = $timeout(function() {
            console.log('Search update')
            // //console.log('update keywords', searchData)
            // $scope.updateFilters();
            searchChanged();
        }, 1000);
        
    }


    //Filter the items by all of our facets
    function searchChanged() {

        if ($scope.items) {
            //Start with all the results
            var filteredItems = $scope.items;

            //Filter search terms
            filteredItems = $filter('filter')(filteredItems, $scope.search.terms);

            //Order the items
            // filteredItems = $filter('orderBy')(filteredItems, $scope.search.order, $scope.search.reverse);






            if ($scope.search.filters) {
                _.forOwn($scope.search.filters, function(value, key) {

                    if (_.isArray(value)) {
                        _.forOwn(value, function(value) {
                            filteredItems = filterListReferences(filteredItems, key, value);
                        })
                    } else {
                        filteredItems = filterListReferences(filteredItems, key, value);
                    }
                });
            }

            ////////////////////////////////////////////

            if ($scope.search.ranges && _.keys($scope.search.ranges).length) {

                _.forOwn($scope.search.ranges, function(range, key) {

                    if (!range) {
                        console.log('Nope', key)
                        return;
                    }
                    var from = range.from;
                    var to = range.to;

                    //Filter to match ranges
                    filteredItems = _.filter(filteredItems, function(item) {
                        var val = _.get(item, key);

                        var matchMin = true;
                        var matchMax = true;

                        if (from) {
                            matchMin = (val >= parseInt(from));
                        }

                        if (to) {
                            matchMax = (val <= parseInt(to));
                        }



                        return matchMin && matchMax;
                    });

                });
            }


            ////////////////////////////////////////////

            if ($scope.search.dates && _.keys($scope.search.dates).length) {
                _.forOwn($scope.search.dates, function(dateRange, key) {
                    filteredItems = matchDateRange(filteredItems, dateRange, key, function(d) {
                        var datetime = new Date(d);
                        datetime.setHours(0, 0, 0, 0);
                        return datetime;
                    });
                });
            }


            ////////////////////////////////////////////

            if ($scope.search.weeks && _.keys($scope.search.weeks).length) {
                _.forOwn($scope.search.weeks, function(dateRange, key) {
                    filteredItems = matchDateRange(filteredItems, dateRange, key, function(d) {
                        return startOfWeek(new Date(d))
                    });
                });
            }

            ////////////////////////////////////////////

            if ($scope.search.months && _.keys($scope.search.months).length) {
                _.forOwn($scope.search.months, function(dateRange, key) {
                    filteredItems = matchDateRange(filteredItems, dateRange, key, function(d) {
                        return startOfMonth(new Date(d))
                    });
                });
            }

            ////////////////////////////////////////////

            if ($scope.search.years && _.keys($scope.search.years).length) {
                _.forOwn($scope.search.years, function(dateRange, key) {
                    filteredItems = matchDateRange(filteredItems, dateRange, key, function(d) {
                        return startOfYear(new Date(d))
                    });
                });
            }


            ////////////////////////////////////////////

            //Update the items with our search
            $scope.filteredItems = filteredItems;

            $scope.containsInherited = _.some(filteredItems, function(item) {

                if (!item.account) {
                    return;
                }

                var accountID = item.account;
                if (accountID._id) {
                    accountID = accountID._id;
                }

                return accountID != $rootScope.user.account._id;
            })

            //Update the current page
            // $scope.updatePage();

            $scope.updateSort();
        }
    }


    //////////////////////////////////////////////////////

    $scope.updateSort = function() {

        var filteredItems = $scope.filteredItems;

        //////////////////////////////////////////////////////

        //Order the items
        filteredItems = _.sortBy(filteredItems, function(item) {
            var val = _.get(item, $scope.search.order);

            ///////////////////////////////

            if (val) {

                if (_.isArray(val) && val.length) {
                    val = val[0];
                }

                if (val.title) {
                    val = val.title;
                } else {
                    if (val.name) {
                        val = val.name;
                    }
                }
            }

            ///////////////////////////////

            // console.log('Order Renderer', $scope.search.orderRenderer)
            switch ($scope.search.orderRenderer) {
                case 'boolean':
                    if (!val) {
                        return 0
                    } else {
                        return 1;
                    }
                    break;
                case 'number':


                    if (!val) {

                        return -999999999999999;
                    }

                    return parseInt(val);
                    break;
                case 'date':

                    var date = new Date(val);
                    return date.getTime();
                    break;
                default:
                    return val || '';
                    break;
            }

        });


        if ($scope.search.reverse) {
            filteredItems.reverse();
        }

        //////////////////////////////////////////////////////

        $scope.sorted = filteredItems;
        $scope.updatePage();
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    ///////////////////////////////////////////////

    //Update the current page
    $scope.updatePage = function() {

        if (!$scope.sorted) {
            $scope.sorted = $scope.filteredItems;
        }

        if ($scope.sorted.length < ($scope.pager.limit * ($scope.pager.currentPage - 1))) {
            $scope.pager.currentPage = 1;
        }

        // var pageItems = $filter('startFrom')($scope.sorted, ($scope.pager.currentPage - 1) * $scope.pager.limit);
        // pageItems = $filter('limitTo')(pageItems, $scope.pager.limit);

        var startIndex = ($scope.pager.currentPage - 1) * $scope.pager.limit;
        if (!startIndex) {
            startIndex = 0;
        }
        var endIndex = startIndex + $scope.pager.limit;


        var pageItems = $scope.sorted.slice(startIndex, endIndex);



        $scope.pageItems = pageItems;
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////





});