app.controller('CodeViewController', function($scope) {
       
       var aceEditor;

    $scope.aceLoaded = function(_editor) {
        // Options
        //_editor.setReadOnly(true);
        aceEditor = _editor;
        aceEditor.setTheme("ace/theme/tomorrow_night_eighties");
        updateSyntax();
    };

    function updateSyntax() {
        if (aceEditor) {

            var syntaxName
            switch($scope.item.syntax) {
                case 'js':
                    syntaxName = 'javascript';
                break;
                default:
                syntaxName = $scope.item.syntax;
                break;
            }

            if(syntaxName) {
                aceEditor.getSession().setMode("ace/mode/" +syntaxName);
            }
        }
    }

    updateSyntax();

    // $scope.$watch('item.syntax', updateSyntax)

    $scope.aceChanged = function(e) {
        //
    };

});
