app.controller('UserFormController', function($scope, Session, $state, Fluro, SearchService, FluroContent, $http,$rootScope, CacheManager, $rootScope, FluroAccess, Notifications) {


    //////////////////////////////////////////////////

    /*
    //Save
    $scope.save = function(options) {

        if (!options) {
            options = {}
        }
        //////////////////////////////////////////

        //Use save options

        if (options.status) {
            status = options.status;
        }

        if (options.exitOnSave) {
            saveSuccessCallback = exitAfterSave;
        }

        //////////////////////////////////////////

        if ($scope.item._id) {
            //Edit existing user
            FluroContent.resource($scope.definedType).update({
                id: $scope.item._id,
            }, $scope.item, $scope.saveSuccess, $scope.saveFail);
        } else {

            //Creating a new user so we need to deviate from the usual course here
            FluroContent.endpoint('invite').save($scope.item, $scope.saveSuccessExit, $scope.saveFail);
        }
    }
    */




    $scope.$watch('item.permissionSets', function() {

        var filtered = _.filter($scope.item.permissionSets, function(set) {
            if (set && set.account) {
                return set.account._id == $rootScope.user.account._id;
            }
        });

        if (!filtered.length) {
            $scope.item.permissionSets = [{
                account: $rootScope.user.account,
                sets: [],
            }]
        }

        $scope.allowedPermissionSets = filtered;

    })


    //////////////////////////////////////////////////

    $scope.saveSuccess = function(result) {


        if (result._id) {
            console.log('Updated to version', $scope.item.__v, result.__v);
            $scope.item.__v = result.__v;
        }

        //Update the current user
        if ($rootScope.user._id == result._id) {
           // CacheManager.clearAll();
SearchService.clear();;
            //$rootScope.user = result;
            Session.refresh();
        } else {
            console.log('Clear USER')
            CacheManager.clear('user');
        }


        Notifications.status(result.name + ' was saved successfully')

        if ($scope.$close) {
            return $scope.$close(result)
        } else {
            if ($rootScope.user._id == result._id) {
                $state.reload();
            }
        }
    }

    //////////////////////////////////////////////////

    $scope.saveSuccessExit = function(result) {
        $scope.saveSuccess(result);
        $state.go('user');
    }


 
    //Creating a new user
    if (!$scope.item._id) {
        //Populate with some defaults
        var account_id = $rootScope.user.account._id;
        $scope.item.account = account_id;
        $scope.item.availableAccounts = [account_id];
    }

    //Check if current user
    $scope.me = ($rootScope.user._id == $scope.item._id);

    $scope.allowAccountChange = ($scope.me && $scope.item.availableAccounts.length > 1);
    $scope.canEditAuthDetails = (($scope.me || FluroAccess.isFluroAdmin()) || !$scope.item._id);



    //Allow changes to roles and realms
    $scope.allowRoleChange = FluroAccess.can('assign', 'role');
    $scope.allowRealmChange = FluroAccess.can('assign', 'realm');




    $scope.resendEmail = function() {

        $scope.resendState = 'processing';

        /////////////////////////////////////////////////

        var request = $http.post(Fluro.apiURL + '/auth/resend', {
            email: $scope.item.email
        });
    
        /////////////////////////////////////////////////

        request.then(function(result) {
            $scope.resendState = 'ready';
            Notifications.status('Instructions on how to reset password have been sent to ' + $scope.item.email);
        }, function(res) {
            var err = res.data;
             $scope.resendState = 'ready';

            if (err.error) {
                Notifications.error(err.error);
            } else {
                Notifications.error('There was an error sending password instructions');
            }
        });

    }




    /*
    //Save
    if ($scope.me) {
        $scope.save = function() {
            Content.resource('user').update({
                id: $scope.item._id,
            }, $scope.item, selfSaveSuccess);
        }
    }

    function selfSaveSuccess(data) {

        //CLEAR ALL CACHES
        console.log('NEED TO CHANGE CACHE STUFF AND REFRESH SESSION')
        //AdminCacheManager.clearAll();
SearchService.clear();;
        Notifications.status('Updates to your account were made successfully');
        //Session.create(data);


        if (FluroAccess.canAccess('user')) {
            $state.go('app.user.default');
        } else {
            $state.go('app.home');

        }
        
    }

    */




    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////




});