app.controller('CheckinFormController', function($scope, $http, Fluro, FluroContent, $state, Notifications) {

    $scope.$watch('item.contact + item.title + item.firstName + item.lastName', function() {

        //Watch to see if the contact exists
        if ($scope.item.contact) {
            //if(!$scope.item.title || !$scope.item.title.length) {
            $scope.item.title = $scope.item.contact.title;
            //}
        } else {

            var title = '';
            if ($scope.item.firstName && $scope.item.firstName.length) {
                title += $scope.item.firstName;
            }

            if ($scope.item.lastName && $scope.item.lastName.length) {
                title += ' ' + $scope.item.lastName;
            }

            $scope.item.title = title;
        }

    })



    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////



    // var lastDate;
    // var runningTotal = 0;

    _.each($scope.item.trackedEvents, function(event) {
    //     var now = moment(event.date);

        if(event.data) {
            if(event.data.terms && event.data.terms.length) {
                event.message = "searched for '" + event.data.terms + "'";
            }

            if(event.data.selected) {
                event.message = "" + event.data.selected + " selected";
            }
        }

    //     // if (lastDate) {
    //     //     var difference = now.diff(lastDate);

    //     //     if(difference < 0) {
    //     //     	difference = 0;
    //     //     }
            
    //     //     runningTotal += difference;
    //     //     var seconds = moment.duration(difference, 'milliseconds').asSeconds().toFixed(1) + 's';
    //     //     // var time = convertMilliseconds();
            
    //     //     if (seconds != '0.0s') { 
    //     //         event.duration = seconds
    //     // 		event.running = moment.duration(runningTotal, 'milliseconds').asSeconds().toFixed(1) + 's';
    //     //     } else {
    //     //     	event.duration = '-';
    //     //     	event.running = '-';
    //     //     }
    //     // }


    //     //Set lastDate
    //     // lastDate = now;
    });



    /**
	$scope.estimateTime = function(events, event) {


		_.reduce(events, function(event) {

		})
        if (event) {
            var total = 0;
            var foundAmount = 0;

            var index = events.indexOf(target);

            _.every(events, function(item, key) {

                //No item so return running total
                if (!item) {
                    foundAmount = total;
                } else {

                    //Increment the total
                    if (item.duration) {
                        total += parseInt(item.duration);
                        foundAmount = (total - item.duration);
                    } else {
                        foundAmount = total;
                    }
                }

                return (index != key)
            })


            var laterTime = new Date($scope.defaults.startDate);

            var milliseconds = foundAmount * 60 * 1000;
            laterTime.setTime($scope.defaults.startDate.getTime() + milliseconds);




            return laterTime;
        } else {
            return;
        }
    }
    /**/





    $scope.checkout = function() {
        FluroContent.endpoint('checkout').save({
            ids: [$scope.item._id],
            pinNumber: $scope.item.pinNumber,
        }).$promise.then(function(res) {

            $state.reload();
            Notifications.post($scope.item.title + ' was checked out successfully');
        }, function(err) {
            Notifications.post(err, 'error');
        });
    }
});