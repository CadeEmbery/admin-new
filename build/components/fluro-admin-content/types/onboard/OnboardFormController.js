app.controller('OnboardFormController', function($scope, $http, Fluro, Notifications) {

/**
	$scope.defaults = {
		schemas:{}
	};

	if(!$scope.item.publicDetails) {
		$scope.item.publicDetails = {
			realms:[]
		}
	}

	/////////////////////////////////////////////

	 $scope.onChange = function (data) {
        console.log('Form changed!', data);

        _.set($scope.item, 'publicDetails.realms', data);
    };


	/////////////////////////////////////////////

    var realmSchema = {
    	type:'object',
    	format:'table',
    	properties:{
    		title:{
    			type:'string',
    			required:true,
    		},
    		color:{
    			type:'string',
    			format:'color'
    		},
    		bgColor:{
    			type:'string',
    			format:'color'
    		},
    	}
    }

    var clone = angular.copy(realmSchema);
    // clone.properties.children = _.clone(clone);
    realmSchema.properties.children = {
    	type:'array',
    	format:'table',
    	items:clone,
    }

	/////////////////////////////////////////////

	$scope.defaults.schemas.realms  = {
        type: 'array',
        format:'table',
        items:realmSchema
    };

	/**
	$scope.authorizeInstagram = function() {


		if(!$scope.item._id || !$scope.item._id.length) {
			Notifications.warning('Please save before attempting to authorize this integration')
		}

		var clientID = $scope.item.publicDetails.clientID;

		var redirectURI = Fluro.apiURL + '/integrate/instagram/' + $scope.item._id;

		if(Fluro.token) {
			redirectURI+= '?access_token=' +Fluro.token;
		}



		if(!clientID || !clientID.length) {
			Notifications.warning('Invalid or missing client ID')
		}

		if(!redirectURI || !redirectURI.length) {
			Notifications.warning('Invalid or missing Redirect URI')
		}

		var url = 'https://api.instagram.com/oauth/authorize/?client_id='+clientID+'&redirect_uri='+redirectURI+ '&response_type=code'

		if(Fluro.token) {
			url+= '&access_token=' +Fluro.token;
		}

		console.log('authorize', url);
		window.open(url);


	}

	/**/


});
