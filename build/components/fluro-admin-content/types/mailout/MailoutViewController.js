app.controller('MailoutViewController', function($scope, FluroSocket, $q, $filter, $state, $interval, ModalService, FluroContent, Fluro, $uibModal, Notifications, $interval, $rootScope) {


    $scope.cache = {
        buster: 0,
    }

    //Search object for filtering results
    $scope.search = {};
    $scope.emails = [];

    //Keep track of the status of whether the mailout is processing
    $scope.mailoutProcessing = false;


    ////////////////////////////////////////////////////

    //Listen for changes to the mailout template
    FluroSocket.on('content.edit', socketUpdate);

    //Destroy the polling listener when user navigates away from the form
    //and stop listening to the socket event
    $scope.$on('$destroy', function() {
        FluroSocket.off('content.edit', socketUpdate);
        stopTimer();
    });

    ////////////////////////////////////////////////////

    //Handle a socket update
    function socketUpdate(socketEvent) {

        var socketItemID = socketEvent.item;

        if (!socketItemID) {
            return;
        }

        if (socketItemID._id) {
            socketItemID = socketItemID._id;
        }

        if (socketItemID == $scope.definition._id) {
            $scope.cache.buster++;
        }
    }



    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    //Create a polling timer
    var pollingTimer;
    var polling;


    //Start the timer
    function startTimer() {
        stopTimer();

        pollingTimer = $interval(pollServer, 4000);
    }

    //Stop the timer
    function stopTimer() {
        if (pollingTimer) {
            $interval.cancel(pollingTimer)
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////

    //Function to refresh results of sent emails and retrieve their status
    function pollServer() {

        //Stop here if already midway through a polling request
        if (polling) {
            return;
        }

        /////////////////////////////////////////////////////////////////////////

        //Set the polling status to true
        //to avoid multiple requests
        polling = true;

        /////////////////////////////////////////////////////////////////////////

        //Get the current date
        var now = new Date();
        now = now.getTime();

        //Now find all correspondence results, add the current time to avoid browser caching results
        var promise = FluroContent.endpoint('mailout/' + $scope.item._id + '/correspondence')
            .query({
                timestamp: now
            })
            .$promise;

        /////////////////////////////////////////////////////////////////////////

        //Once we get the results
        promise.then(function(items) {
            //Get the emails
            $scope.emails = items;

            return refreshResults();
        }, function(err) {
            //console.log('Error loading correspondence');
            polling = false;
        });
    }

    //////////////////////////////////////////////////////////////////////////////////////

    $scope.$watch(function() {
        return $scope.search.terms;
    }, refreshResults, true);

    //////////////////////////////////////////////////////////////////////////////////////

    function refreshResults() {

        //Get the items
        var filteredItems = $scope.emails;
        var searchTerms = $scope.search.terms;

        /////////////////////////////////////////////////

        function removeURLParameter(url, parameter) {
            //prefer to use l.search if you have a location/link object
            var urlparts = url.split('?');
            if (urlparts.length >= 2) {

                var prefix = encodeURIComponent(parameter) + '=';
                var pars = urlparts[1].split(/[&;]/g);

                //reverse iteration as may be destructive
                for (var i = pars.length; i-- > 0;) {
                    //idiom for string.startsWith
                    if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                        pars.splice(i, 1);
                    }
                }

                url = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
                return url;
            } else {
                return url;
            }
        }


        $scope.urls = _.chain(filteredItems)
            .map(function(item) {
                return item.trackedEvents
            })
            .flatten()
            .filter(function(event) {
                return (event.type == 'click' || event.type == 'clicked');
            })
            .reduce(function(resultSet, event) {

                var eventURL = removeURLParameter(event.url, 'correspondence');

                var existing = _.find(resultSet, {
                    url: eventURL
                });

                if (!existing) {
                    existing = {
                        url: eventURL,
                        count: 1
                    }
                    resultSet.push(existing);
                } else {
                    existing.count++;
                }

                return resultSet;

            }, [])
            .sortBy(function(link) {
                return -link.count;
            })
            .value();

        /////////////////////////////////////////////////

        //check if any failed to send
        $scope.someFailed = _.some(filteredItems, {
            state: 'error'
        });



        /////////////////////////////////////////////////

        //If the user is searching
        if (searchTerms && searchTerms.length) {
            //Filter the items
            filteredItems = $filter('filter')(filteredItems, searchTerms);
        }

        /////////////////////////////////////////////////

        $scope.filteredEmails = filteredItems;

        /////////////////////////////////////////////////
        /////////////////////////////////////////////////
        /////////////////////////////////////////////////
        /////////////////////////////////////////////////

        function addToColumn(item, state, results) {

            var existing = _.find(results, {
                state: state
            });

            if (!existing) {
                existing = {
                    state: state,
                    emails: []
                }
                results.push(existing);
            }

            //////////////////////////////////////////

            existing.emails.push(item);
        }

        /////////////////////////////////////////////////



        /////////////////////////////////////////////////
        /////////////////////////////////////////////////
        /////////////////////////////////////////////////
        /////////////////////////////////////////////////

        $scope.results = _.chain(filteredItems)
            .sortBy(function(item) {
                return item.firstName;
            })
            .reduce(function(results, item) {

                var state = item.state;

                ///////////////////////////////////////
                ///////////////////////////////////////

                if (state == 'open') {
                    state = 'opened';
                }

                if (state == 'click') {
                    state = 'clicked';
                }

                ///////////////////////////////////////
                ///////////////////////////////////////

                switch (state) {
                    case 'clicked':
                        addToColumn(item, 'opened', results);
                        addToColumn(item, 'clicked', results);
                        break;
                    case 'sent':
                        addToColumn(item, 'unopened', results);
                        break;
                    default:
                        addToColumn(item, state, results);
                        break;
                }

                // ///////////////////////////////////////

                // var existing = _.find(results, {
                //     state: state
                // });

                // if (!existing) {
                //     existing = {
                //         state: state,
                //         emails: []
                //     }

                //     results.push(existing);
                // }

                // existing.emails.push(item);

                ///////////////////////////////////////

                return results;



            }, [{
                state: 'clicked',
                emails: [],
            }, {
                state: 'opened',
                emails: [],
            }, {
                state: 'unopened',
                emails: [],
            }, {
                state: 'queued',
                emails: [],
            }, {
                state: 'error',
                emails: [],
            }])
            .value();

        /////////////////////////////////////////////////

        //Mark that the polling request is complete
        polling = false;


    }

    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////


    //Helper function to get the percentage of emails in a specified
    //state
    $scope.getPercentage = function(state) {
        if (!$scope.emails) {
            return;
        }

        //Get the total emails sent
        var total = $scope.emails.length;
        var list = _.find($scope.results, {
            state: state
        });

        if (!list || !list.emails) {
            return;
        }

        var count = list.emails.length;

        // var count = getList.length;
        var average = Math.round(count / total * 100);
        return average;
    }

    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    $scope.getTotal = function(state) {
        if (!$scope.emails) {
            return;
        }

        var getList = _.filter($scope.emails, function(email) {
            if (state == 'open') {
                return (email.state == 'click' || email.state == 'open');
            }
            return email.state == state;
        });

        //Get the total emails sent
        var total = getList.length

        return total;
    }

    //////////////////////////////////////////////////////////////////////////////////////


    $scope.viewContact = function(correspondence) {
        ModalService.view({
            _type: 'contact',
            _id: correspondence.contact,
            realms: $scope.item.realms
        });
    }

    //////////////////////////////////////////////////////////////////////////////////////


    $scope.viewCorrespondence = function(correspondence) {
        var modalInstance = $uibModal.open({
            templateUrl: 'fluro-admin-content/types/mailout/correspondence.html',
            controller: function($scope) {
                $scope.correspondence = correspondence;
            }
        });
    }


    $scope.pushModal = function() {

        var mailout = $scope.item;
        var modalInstance = $uibModal.open({
            templateUrl: 'fluro-admin-content/types/mailout/push.html',
            controller: function($scope, FluroContent) {
                $scope.push = {};

                $scope.pushNow = function() {


                    FluroContent.endpoint('mailout/' + mailout._id + '/push')
                        .save($scope.push)
                        .$promise
                        .then(function(res) {

                            // console.log('TESTING', res);
                            Notifications.status(res.success.length + ' contacts were pushed through successfully');
                            $scope.$dismiss();

                            return pollServer();
                        }, function(err) {
                            Notifications.error(err);

                        })
                }
            }
        });
    }


    $scope.sendTest = function() {


        $scope.testProcessing = true;

        var contactIDs = _.map($scope.item.testContacts, '_id');

        /////////////////////////////////////////////

        FluroContent.endpoint('mailout/' + $scope.item._id + '/test')
            .save({
                contacts: contactIDs
            })
            .$promise
            .then(function(res) {



                $scope.testProcessing = false;
                var plural = 'test was';
                if (contactIDs.length != 1) {
                    plural = 'tests were';
                }

                Notifications.status(contactIDs.length + ' ' + plural + ' sent');

            }, function(err) {
                $scope.testProcessing = false;
                Notifications.error(err);
            });
    }


    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    $scope.previewCache = 0;
    // $scope.previewURL = Fluro.apiURL + '/mailout/render/' + $scope.item.definition + '?mailout=' + $scope.item._id;
    // $scope.previewRefreshToken = 1;

    //////////////////////////////////////////////////////////////////////////////////////

    $scope.preflightContacts = $scope.extras.preflightContacts;
    $scope.validContacts = [];
    $scope.invalidContacts = [];
    $scope.unsubscribedContacts = [];


    _.each($scope.preflightContacts, function(contact) {

        if (!contact.emails || !contact.emails.length) {
            return $scope.invalidContacts.push(contact);
        }

        if (contact.unsubscribed) {
            return $scope.unsubscribedContacts.push(contact);
        }

        return $scope.validContacts.push(contact);

    });


    //  = _.reduce($scope.extras.preflightContacts, function(results, contact) {

    //     var status = 'invalid';
    //     if(contact.emails && contact.emails.length) {
    //         status ='valid';
    //     }

    //     var existing = _.find(results, {status:status});

    //     if(!existing) {
    //         existing = {

    //             status:status,
    //             contacts:[]
    //         }

    //         results.push(existing);
    //     }

    //     existing.contacts.push(contact);

    //     return results;
    // }, []);


    //////////////////////////////////////////////////////////////////////////////////////

    checkScheduled();

    //////////////////////////////////////////////////////////////////////////////////////

    var scheduleTimer;

    function checkScheduled() {
        if ($scope.item.state == 'scheduled') {

            scheduleTimer = $interval(function() {

                if ($scope.item.state != 'scheduled') {
                    return $interval.cancel(scheduleTimer);
                }

                var publishDate = new Date($scope.item.publishDate);
                var now = new Date();

                var timeLeft = publishDate.getTime() - now.getTime();

                if (timeLeft <= 1) {
                    console.log('SWITCH TO SENT!')

                    $scope.item.state = 'sent'
                    startTimer();
                    pollServer();


                } else {
                    $scope.time = timeLeft;
                }
            }, 1000);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////

    $scope.abort = function() {
            $scope.mailoutProcessing = true;


            var promise = FluroContent.endpoint('mailout/' + $scope.item._id + '/abort').get().$promise;

            promise.then(function(mailout) {

                Notifications.warning('Mailout schedule cancelled');

                $scope.mailoutProcessing = false;
                //console.log('mailout sent!', mailout)

                $scope.item.state = mailout.state;
                $scope.item.publishDate = mailout.publishDate;
                checkScheduled();


            }, function(err) {
                $scope.mailoutProcessing = false;
                return Notifications.error(err)
            });



        }
        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

    $scope.publish = function() {


        if ($scope.mailoutProcessing) {
            return;
        }

        $scope.mailoutProcessing = true;

        var promise = FluroContent.endpoint('mailout/' + $scope.item._id + '/publish').get({
            date: $scope.item.publishDate,
        }).$promise;

        promise.then(function(mailout) {

            Notifications.status('Mailout published successfully');

            $scope.mailoutProcessing = false;
            //console.log('mailout sent!', mailout)

            $scope.item.state = mailout.state;

            switch($scope.item.state) {
                case 'scheduled':
                checkScheduled();
                break;
                case 'sent':
                    startTimer();
                    pollServer();
                break;
            } 

        }, function(err) {


            $scope.mailoutProcessing = false;
            return Notifications.error(err)
        });
    }

    //////////////////////////////////////////////////////////////////////////////////////

    if ($scope.item.state == 'sent' || _.get($scope.item, 'stats.mailoutSent')) {
        startTimer();
        pollServer();
    }

    // $scope.$watch('item.state', function(state) {
    //     if (state == 'sent') {
    //         $scope.publishTabText = 'Results'
    //     } else {
    //         $scope.publishTabText = 'Send / Publish'
    //     }
    // })

    //////////////////////////////////////////////////////////////////////////////////////



});