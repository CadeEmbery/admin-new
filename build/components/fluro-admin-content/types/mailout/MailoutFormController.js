app.controller('MailoutFormController', function($scope, FluroSocket, $rootScope) {


    var subjectWatcher;

    if(!$scope.item.fromFirstName || !$scope.item.fromFirstName.length) {
        $scope.item.fromFirstName = $rootScope.user.firstName;
    }

    if(!$scope.item.fromLastName || !$scope.item.fromLastName.length) {
        $scope.item.fromLastName = $rootScope.user.lastName;
    }

    if(!$scope.item.fromEmail || !$scope.item.fromEmail.length) {
        $scope.item.fromEmail = $rootScope.user.email;
    }

    if (!$scope.item._id && !_.get($scope.item, 'subject.length')) {
        subjectWatcher = $scope.$watch(function() {
            return $scope.item.title
        }, function(title) {

            // if(!$scope.item.subject || $scope.item.subject == $scope.item.title) {
            $scope.item.subject = title;
            // }
        });

        // subjectWatcher();
    }
    /////////////////////////////////////////////////

    $scope.cache = {
        buster:0,
    }
     // console.log('listen for update')
    
    FluroSocket.on('content.edit', socketUpdate);


    $scope.$on('$destroy', function() {
        FluroSocket.off('content.edit', socketUpdate);
    })

    ////////////////////////////////////////////////////

    function socketUpdate(socketEvent) {

        // console.log('Socket update!!')
        var socketItemID = socketEvent.item;

        if (!socketItemID) {
            return;
        }

        if (socketItemID._id) {
            socketItemID = socketItemID._id;
        }

        if (socketItemID == $scope.definition._id) {
            // console.log('bust cache!')
            $scope.cache.buster++;
        }
    }


    /////////////////////////////////////////////////
    
    $scope.viewOnExit = true;

    /////////////////////////////////////////////////

    //If we are providing existing contacts to send to
    if ($scope.extras && $scope.extras.contacts && $scope.extras.contacts.length) {

        var contacts = [];
        contacts = contacts.concat($scope.item.contacts, $scope.extras.contacts);
        contacts = _.chain(contacts)
            .compact()
            .uniq(function(contact) {
                return contact._id;
            })
            .value()

        console.log('Set unique contacts for mailout')
        $scope.item.contacts = contacts;
    }



});