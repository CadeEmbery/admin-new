app.controller('PersonaListController', function($scope, $rootScope, SearchService, $state, Fluro, FluroAccess, FluroTokenService, CacheManager, Notifications, TypeService) {



    $scope.canImpersonate = function(item) {


        // if(!item) {
        //     return $rootScope.user.accountType == 'administrator';
        // } else {
        //     return $rootScope.user.accountType == 'administrator' && !item.managed;
        // }

        ////////////////////////////////////////////////////

        if(!item) {
            return;
        }

        if(item._id == $rootScope.user.persona) {
            return false;
        }

        ////////////////////////////////////////////////////

        var canEditItem =  FluroAccess.canEditItem(item);
        var impersonateRealms = FluroAccess.retrieveActionableRealms('impersonate');

        if(canEditItem && impersonateRealms) {
            return true;
        }



    }

    ////////////////////////////////////////////////////////////

    $scope.signInAsPersona = function(personaID) {

        Fluro.sessionStorage = true;
        console.log('Sign in as persona', personaID);


        var request = FluroTokenService.signInAsPersona(personaID);

        function signInSuccess(res) {
            console.log('Signed in as persona', res)
            CacheManager.clearAll();
SearchService.clear();;
            TypeService.refreshDefinedTypes();
            Notifications.status('Switched this session to ' + res.data.account.title);

            $state.reload();
        }

        function signInFail(res) {
            console.log('Error signing in as persona', res)
        }

        //Listen for the promise resolution
        request.then(signInSuccess, signInFail);

        /**
        if (asSession) {
            Fluro.sessionStorage = true;

            function success(res) {
                 CacheManager.clearAll();
SearchService.clear();;
                 TypeService.refreshDefinedTypes();
                Notifications.status('Switched this session to ' + res.account.title);
            }

            function fail(res) {
                Notifications.error('Error creating token session');
            }
           
            FluroTokenService.getTokenForAccount(id, success, fail);
        } else {

            // //Update the user
            // FluroContent.resource('user').update({
            //     id: $rootScope.user._id,
            // }, {
            //     account: id
            // }, loginSuccess, updateFailed);





            //Update the user
            FluroContent.endpoint('auth/switch/' + id).save({}).$promise.then(loginSuccess, updateFailed);
        }
        /**/
    }



});