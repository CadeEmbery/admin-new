app.filter('ownership', function() {
    return function(input) {

        if(!input || !input.length) {
            return '';
        }

        var lastLetter = input.slice(-1);
        if(lastLetter.toLowerCase() == 's') {
            return input + "'";
        } else {
            return input + "'s";
        }
    }
})

app.controller('PersonaFormController', function($scope, Session, $state, Fluro, Asset, FluroContent, $http, $rootScope, CacheManager, $rootScope, FluroAccess, Notifications) {


    $scope.resendState = 'ready';

    // console.log('HAS PERMISSION', FluroAccess.has('assign policy'));
    $scope.canAssignPasses = FluroAccess.has('grant policy') || FluroAccess.has('revoke policy');
    $scope.canAssignRoles = FluroAccess.has('assign role');


    console.log(Asset);

    if (!$scope.purchases) {
        $scope.purchases = [];
    }

    ////////////////////////////////////////////////

    if ($scope.item._id) {


        ///////////////////////////////////////

        //Keep the details in sync with the user
        var user = $scope.item.user;

        if(user) {            
            $scope.item.firstName = user.firstName;
            $scope.item.lastName = user.lastName;
        }

        ///////////////////////////////////////

        FluroContent.endpoint('persona/purchases/' + $scope.item._id)
            .query()
            .$promise
            .then(function(res) {
                $scope.purchases = res;
            }, function(err) {
                console.log('Error loading purchases', err);
            });

        ////////////////////////////////////////////////

        FluroContent.endpoint('persona/methods/' + $scope.item._id)
            .query()
            .$promise
            .then(function(res) {
                $scope.methods = res;
            });

        // ////////////////////////////////////////////////

        // FluroContent.endpoint('persona/purchases/' + $scope.item._id)
        //     .query()
        //     .$promise
        //     .then(function(res) {
        //         $scope.purchases = res;
        //     });
    }

    ////////////////////////////////////////////////

    $scope.sendResetEmail = function() {

        $scope.resendState = 'processing';

        //Start off with the collection email
        var email = $scope.item.collectionEmail;


        if ($scope.item.user) {
            email = $scope.item.user.email;
        }

        /////////////////////////////////////////////////

        // var request = $http.post(Fluro.apiURL + '/user/reinvite', {
        //     email: email
        // });

        var request = $http.post(Fluro.apiURL + '/user/reinvite/' + $scope.item._id);

        /////////////////////////////////////////////////

        request.then(function(res) {
            console.log('Send', res.data);
            $scope.resendState = 'ready';
            Notifications.status('Instructions on how to reset password have been sent to ' + email);
        }, function(res) {

            var err = res.data;
            console.log('ERROR', res);

            $scope.resendState = 'ready';



            if (err.error) {
                Notifications.error(err.error);
            } else {
                Notifications.error('There was an error sending password instructions');
            }
        });
    }


    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////




});