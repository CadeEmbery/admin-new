app.directive('assetReplaceForm', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        templateUrl: 'fluro-admin-content/types/asset/asset-replace-form.html',
        controller: 'AssetReplaceController',
    };
});


app.controller('AssetReplaceController', function($scope, Fluro) {

    $scope.replaceSettings = {
        show:true,
    }

    if($scope.item && $scope.item._id) {
        $scope.replaceSettings.show = false;
    }

    /////////////////////////////////

/*
    if (!$scope.item._id) {
        $scope.replaceDialogShowing = true;
    }
*/

    /////////////////////////////////

    $scope.$watch('uploader.queue[0].isUploaded', function(val) {
        if(val) {
            $scope.replaceSettings.show = false;
        }
    })

    $scope.showReplaceDialog = function() {
        $scope.replaceSettings.show = true;
    }

    /////////////////////////////////

});