app.controller('AssetViewController', function($scope, $rootScope, Asset, Fluro) {


	var assetURL = Asset.getUrl($scope.item._id);

	
	var pdfURL = 'https://docs.google.com/viewer?url='  + Asset.getUrl($scope.item._id) + '&embedded=true';
    
    console.log('PDF URL', pdfURL);

    $scope.pdfURL = pdfURL;

});