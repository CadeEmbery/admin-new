app.controller('AssetFormController', function($scope, $timeout, Asset, FluroContent, Fluro, $state, $stateParams, Notifications, FileUploader) {


    /////////////////////////////////
    /////////////////////////////////
    /////////////////////////////////

    switch ($scope.type.path) {
        case 'image':
            if (!$scope.item.width) {
                $scope.needsWidth = true;
            }

            if (!$scope.item.height) {
                $scope.needsHeight = true;
            }
            break;
        case 'audio':
            $scope.variants = [];


            $scope.variants.push({
                title: '320kbps',
                id: '320',
                exists: _.get($scope.item, 'variants[320]')
            })

            $scope.variants.push({
                title: '128kbps',
                id: '128',
                exists: _.get($scope.item, 'variants[128]')
            })

            $scope.transcode = function(variant) {

                if (variant.exists || variant.processing) {
                    return;
                }

                variant.processing = true;

                return FluroContent.endpoint('system/transcode/' + $scope.item._id + '/' + variant.id + '?type=audio').get().$promise.then(function(res) {
                    variant.exists = true;
                    variant.processing = false;
                }, function(err) {
                    console.log('ERRROR', err);
                    variant.processing = false;
                });
            }

            break;
        case 'video':
            $scope.variants = [];


            $scope.variants.push({
                title: '360p',
                id: '360',
                exists: _.get($scope.item, 'variants[360]')
            })

            $scope.variants.push({
                title: '480p',
                id: '480',
                exists: _.get($scope.item, 'variants[480]')
            })

            $scope.variants.push({
                title: '720p',
                id: '720',
                exists: _.get($scope.item, 'variants[720]')
            })

            $scope.variants.push({
                title: '1080p',
                id: '1080',
                exists: _.get($scope.item, 'variants[1080]')
            })



            $scope.transcode = function(variant) {

                if (variant.exists || variant.processing) {
                    return;
                }

                variant.processing = true;

                return FluroContent.endpoint('system/transcode/' + $scope.item._id + '/' + variant.id).get().$promise.then(function(res) {
                    variant.exists = true;
                    variant.processing = false;
                }, function(err) {
                    console.log('ERRROR', err);
                    variant.processing = false;
                });
            }

            break;
    }

    /////////////////////////////////
    /////////////////////////////////
    /////////////////////////////////
    /////////////////////////////////

    var REPLACEMENT_UPLOAD_URL = Fluro.apiURL + '/file/upload';

    //If the file is uploaded with an external integration
    if ($scope.item.externalIntegration) {

        var externalIntegrationID = $scope.item.externalIntegration;
        if (externalIntegrationID._id) {
            externalIntegrationID = externalIntegrationID._id;
        }

        REPLACEMENT_UPLOAD_URL = Fluro.apiURL + '/file/integration/upload/' + externalIntegrationID;
        //It's saved on another S3 bucket

    }

    /////////////////////////////////
    /////////////////////////////////
    /////////////////////////////////
    /////////////////////////////////

    $scope.uploader = new FileUploader({
        url: REPLACEMENT_UPLOAD_URL,
        //removeAfterUpload: true,
        withCredentials: true,
        formData: {},
    });


    var forceRefresh = false;

    $scope.saveAndUpload = function() {
        console.log('Save and Upload');

        if (!$scope.item || !$scope.item.realms || !$scope.item.realms.length) {
            return Notifications.warning('Please select a realm before attempting to upload');
        }

        $scope.uploader.uploadAll();

    }


    if (!$scope.item.assetType && $scope.type.path != 'video' && $scope.type.path != 'audio') {
        $scope.item.assetType = 'upload';
    }


    /////////////////////////////////

    function refreshAfterUpload(response) {
        if (response._id == $scope.item._id) {
            $scope.item = response;

            $scope.refreshHide = true;
            $timeout(function() {
                $scope.refreshHide = false
            }, 500)
        }
    }

    $scope.getImageURL = function() {

        var params = {
            v: $scope.item.updated,
            filename: $scope.item.filename,
            forceRefresh: true
        }

        ///////////////////////////////////////

        return Asset.imageUrl($scope.item._id, 600, null, params);
        // &v=' + $scope.item.updated;// + '&forceRefresh=true';

    }

    //////////////////////////////////////////////////

    $scope.$watch('item._id + item.assetType', function() {
        if (!$scope.item._id && $scope.item.assetType == 'upload') {
            $scope.uploadSave = true;
        } else {
            $scope.uploadSave = false;
        }
    })

    /////////////////////////////////

    $scope.uploader.onBeforeUploadItem = function(item) {

        // item.removeAfterUpload = false;
        var details = {};

        if ($scope.item._id) {
            details._id = $scope.item._id;
        } else {

            //Add the definition before we send the data
            if ($scope.definition) {
                $scope.item.definition = $scope.definition.definitionName;
            }

            $scope.item.type = $scope.type.path;
            //console.log('Create Item', $scope.type, $scope.item.type);
            details.json = angular.toJson($scope.item);
        }

        console.log('Send details', details)
        item.formData = [details];
    };




    $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {

        _.each(response.errors, function(err) {
            if (err.message) {
                Notifications.error(err.message);
            } else {
                Notifications.error(err);
            }
        })

        //Reset File Item and add it back into the queue
        fileItem.isError = false;
        fileItem.isUploaded = false;
        $scope.uploader.queue.push(fileItem);
    };

    /////////////////////////////////

    $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
        switch (status) {
            case 200:
                if ($scope.item._id) {
                    $scope.saveSuccess(response);
                    refreshAfterUpload(response);
                } else {
                    $scope.saveSuccessExit(response);
                }
                break;
            default:
                //Failed
                $scope.saveFail(response);
                break;
        }
    };


});