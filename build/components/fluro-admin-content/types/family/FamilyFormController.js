app.controller('FamilyFormController', function($scope, ModalService, TypeService, Notifications, CacheManager, FluroContent) {

   

    _.each($scope.item.items, function(contact) {
        contact.newDefinition = contact.definition;
    })


     console.log('FAMILY CONTROLLER', $scope.item.items)

    //////////////////////////////////////////////

    //$scope.openContact = ModalService.edit(item);

    $scope.updateRole = function(contact) {
        FluroContent.resource('contact/' + contact._id)
        .update({householdRole:contact.householdRole})
        .$promise
        .then(function(res) {
            return Notifications.status(contact.firstName + ' was updated with the role of ' + contact.householdRole);
        })
    }

    //////////////////////////////////////////////

    TypeService.refreshDefinedTypes(true).then(function(res) {


        $scope.contactDefinitions = TypeService.getSubTypes('contact')

     })

     //////////////////////////////////////////////


    $scope.definitionChanged = function(contact, $event) {
        console.log('DEFINITION CHANGED', contact.definition, contact.newDefinition, $event);
        
        var definedName = contact.definition || 'contact';
        FluroContent.resource(definedName + '/' + contact._id)
        .update({definition:contact.newDefinition})
        .$promise
        .then(function(res) {

            console.log('Changed!', res)
            contact.definition = contact.newDefinition;

            // return Notifications.status(contact.firstName + ' was updated with the status of ' + contact.status);
        }, function(err) {
            return Notifications.error(err);
        })


    }
    //////////////////////////////////////////////

    $scope.updateStatus = function(contact, status) {

        console.log('UPDATE STATUS', status);
        // if(contact.status == status) {
        //     return;
        // }


        FluroContent.resource('contact/' + contact._id)
        .update({status:status})
        .$promise
        .then(function(res) {
            // return Notifications.status(contact.firstName + ' was updated with the status of ' + contact.status);
        }, function(err) {
            return Notifications.error(err);
        })
    }

    $scope.addContact = function() {

        console.log('Add Contact', $scope.item);

        if ($scope.item && $scope.item._id) {
            var params = {}
            params.template = {
                family: $scope.item,
                lastName:$scope.item.title,
            }

            ModalService.create('contact', params, function(res) {

                if(!$scope.item.items) {
                    $scope.item.items = [];
                }
                $scope.item.items.push(res);
            })

        }

    }

     /////////////////////////////////////

    $scope.removeDistinctions = function() {
        FluroContent.endpoint('family/divide/' + $scope.item._id)
        .delete()
        .$promise
        .then(function(res) {

            
            Notifications.status($scope.item.distinctFrom.length + ' distinctions were removed from ' + $scope.item.title);

                CacheManager.clear('family');
            //Clear the distinctions
            $scope.item.distinctFrom = [];
        }, function(err) {

            Notifications.error(err);
        })
    }




});