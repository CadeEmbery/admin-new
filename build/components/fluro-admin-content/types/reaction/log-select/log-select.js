app.directive('logSelect', function($filter) {


    return {
        restrict: 'E',
        replace: true,
        scope: {
            callback:'=callback',
            selection: '=ngModel',
            items: '=',
        },
        templateUrl: 'fluro-admin-content/types/reaction/log-select/log-select.html',
        link: function($scope, $element, $attrs, $ctrl) {

            if(!$scope.search) {
                $scope.search = {}
            }

            /////////////////////////////////////////////

            if (!$scope.selection) {
                $scope.selection = {};
            }

            /////////////////////////////////////////////

            $scope.$watch('search', updateFilters, true);

            /////////////////////////////////////////////

            //Filter the items by all of our facets
            function updateFilters() {

                //Start with all the results
                var filteredItems = $scope.items;

                //Filter search terms
                if ($scope.search.terms && $scope.search.terms.length) {
                    filteredItems = $filter('filter')(filteredItems, $scope.search.terms);
                }

                //Order the items
                filteredItems = $filter('orderBy')(filteredItems, $scope.search.order, $scope.search.reverse);

                //Check we have some items at this point
                var beforeFiltersLength = filteredItems.length;

                //If there are filters
                if ($scope.search.filters) {
                    _.forOwn($scope.search.filters, function(value, key) {

                        if (_.isArray(value)) {
                            _.forOwn(value, function(value) {
                                filteredItems = $filter('reference')(filteredItems, key, value);
                            })
                        } else {
                            filteredItems = $filter('reference')(filteredItems, key, value);
                        }
                    });

                    //If our filters are stopping any results come through
                    if (beforeFiltersLength && !filteredItems.length) {
                        // console.log('Reset Filters')
                        $scope.search.filters = {};
                        return;
                    }
                }

                ////////////////////////////////////////////

                //Set to our new crop
                $scope.filteredItems = filteredItems;
            }

        }
    }
})