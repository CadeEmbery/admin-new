app.controller('ReactionFormController', function($scope, $rootScope, $uibModal, TypeService, ModalService, $filter, Notifications, FluroContent) {

    if (!$scope.item.actions) {
        $scope.item.actions = [];
    }

    if (!$scope.item.triggers) {
        $scope.item.triggers = [];
    }

    $scope.addAction = function($index) {

        if ($index || $index === 0) {
            console.log('INDEX', $index)
            $scope.item.actions.splice($index + 1, 0, {});
        } else {
            $scope.item.actions.push({});
        }
    }

    $scope.removeAction = function(block) {
        _.pull($scope.item.actions, block);
    }


     var availableTriggers = [];

    function addTrigger(value, label) {
         availableTriggers.push({
            name: label || value,
            value: value,
        })
    }


    addTrigger("content.create");
    addTrigger("content.edit");
    addTrigger("content.delete");
    addTrigger("content.destroy");

    addTrigger("event.start");
    addTrigger("event.end");

    addTrigger("checkin.open");
    addTrigger("checkin.close");
    addTrigger("content.restore");
    addTrigger("payment.refund");
    addTrigger("payment.success");
    addTrigger("payment.failed");
    addTrigger("payment.hold");
    addTrigger("payment.capture");
    addTrigger("payment.cancelled");
    addTrigger("payment.pending");


    addTrigger("process.move");



    addTrigger("purchase.queued");
    addTrigger("reaction.error");
    addTrigger("reaction.success");
    addTrigger("batch.init");
    addTrigger("checkin.reprint");
    addTrigger("checkin");
    addTrigger("checkout");
    addTrigger("leader.checkout");

    addTrigger("contact.recognize");
    addTrigger("contact.divide");
    addTrigger("contact.merge");
    addTrigger("contact.age");
    addTrigger("contact.birthday");
    addTrigger("contact.graduation");
    // addTrigger("confirmation.confirmed");
    // addTrigger("confirmation.unavailable");
    // addTrigger("confirmation.unknown");
    addTrigger("assignment.swap.request");
    addTrigger("assignment.swap.response");
    addTrigger("assignment.swap.response.confirmed");
    addTrigger("assignment.swap.response.declined");

    addTrigger("deploy.success");
    addTrigger("deploy.error");
    addTrigger("family.divide");
    addTrigger("family.merge");
    addTrigger("sms.replied");
    // <!-- addTrigger("message.sms");
    // <!-- addTrigger("message.sms.replied");
    // <!-- addTrigger("message.email");
    addTrigger("team.merge");
    addTrigger("team.join");
    addTrigger("team.leave");
    addTrigger("compile.error");
    addTrigger("persona.impersonate");
    addTrigger("mention");


    //////////////////////////////////////////////////

    $scope.availableTriggers = _.sortBy(availableTriggers, function(item) {
        return item.name;
    })


    //////////////////////////////////////////////////

    $scope.basicTypes = _.sortBy(TypeService.types, function(obj) {
        return obj.singular;
    });
    $scope.definedTypes = _.sortBy(TypeService.definedTypes, function(obj) {
        return obj.title;
    });

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    function safelyParseJSON(json) {
        // This function cannot be optimised, it's best to
        // keep it small!
        var parsed

        try {
            parsed = JSON.parse(json)
        } catch (err) {
            console.log('JSON PARSE ERROR', err);
        }

        return parsed // Could be undefined!
    }

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    $scope.test = function(testEvent) {

        var json = safelyParseJSON(testEvent.json);


        if (!json) {
            Notifications.error('Test Syntax Error - "' + testEvent.title + '" JSON is invalid ')
            return;
        }


        json.testEmail = testEvent.testEmail;
        json.testPhone = testEvent.testPhone;

        ////////////////////////////////////////////

        var request = FluroContent.endpoint('reaction/test').save({
                reaction: $scope.item._id,
                triggerData: json,
            })
            .$promise;

        ////////////////////////////////////////////

        request.then(function(result) {

            _.each(result.log, function(log, key) {
                $scope.item.actions[key].test = log;
                $scope.item.actions[key].testText = JSON.stringify(log);
            });

        }, function(err) {
            Notifications.error(err);
        });

    }


    //////////////////////////////////////////////////

    $scope.editTest = function(testEvent) {


        var tests = $scope.item.tests;

        var modalInstance = $uibModal.open({
            template: '<test-event-configure ng-model="test" close="closeModal" remove="removeTest"></test-event-configure>',
            size: 'md',
            controller: function($scope) {
                $scope.test = testEvent;
                $scope.isModal = true;
                $scope.closeModal = function() {
                    console.log('close modal')
                    $scope.$close();
                }

                $scope.removeTest = function() {
                    $scope.$close();
                    _.pull(tests, testEvent);
                }
            }
        });
    }

    //////////////////////////////////////////////////

    $scope.addTestEvent = function() {


        ////////////////////////////////////////////////////


        async.waterfall([
            browseLogs,
            createTest,
        ])

        ////////////////////////////////////////////////////

        function browseLogs(next) {
            var modalInstance = $uibModal.open({
                template: '<div class="flex-column"><div class="flex-column-header"><div class="modal-header clearfix"><h4 style="line-height:34px;" class="title pull-left">Please select a trigger</h4><a class="pull-right btn btn-default" ng-click="$close()">Close</a></div></div><div class="flex-column-body"><log-select ng-model="selection" items="logs" callback="select"></log-select></div></div>',
                size: 'lg',
                resolve: {
                    logs: function(FluroContent, $q) {

                        var deferred = $q.defer();

                        FluroContent.endpoint('log', true, true).query({
                            limit: 500,
                            noCache: true,
                            account: $rootScope.user.account._id,
                        }).$promise.then(function(logs) {
                            logs = _.uniq(logs, function(log) {
                                var id = _.get(log, 'item._id');
                                if (!id) {
                                    return log._id;
                                }

                                return log.key + '.' + id;
                            });

                            deferred.resolve(logs);
                        }, deferred.reject);


                        return deferred.promise;
                    }
                },
                controller: function($scope, logs) {
                    $scope.logs = logs;

                    // $scope.test = testEvent;
                    // $scope.isModal = true;
                    $scope.select = function(item) {
                        // console.log('SELECT', item);
                        $scope.$close(item);
                        return next(null, item);
                    }
                }


            });

            // modalInstance.result.then(next, next);
        }

        ////////////////////////////////////////////////////

        function createTest(log, next) {

            var testEvent = {
                title: 'My Test Event',
            }

            var json;

            if (log) {

                json = {
                    trigger: log.key,
                    data: log.data,
                }

                if (log.item) {
                    var itemTitle = _.get(log, 'item.title');
                    if (itemTitle) {
                        testEvent.title = 'Test  (' + itemTitle + ')';
                    }
                    json.item = log.item;
                }

            } else {
                json = JSON.stringify({
                    trigger: 'content.create',
                    item: 'ITEM_ID',
                    data: log.data,
                }, true);
            }

            ////////////////////////////////////////////////////

            testEvent.json = JSON.stringify(json, null, 2);
            testEvent.testEmail = $rootScope.user.email;

            ////////////////////////////////////////////////////

            var modalInstance = $uibModal.open({
                template: '<test-event-configure ng-model="test" close="closeModal"></test-event-configure>',
                size: 'md',
                controller: function($scope) {
                    $scope.test = testEvent;
                    $scope.isModal = true;
                    $scope.closeModal = function() {
                        console.log('close modal')
                        $scope.$close();
                    }
                }
            });

            modalInstance.result.then(finish, finish);

            function finish() {
                if (!$scope.item.tests) {
                    $scope.item.tests = [];
                }
                $scope.item.tests.push(testEvent);
            }
        }
    }

    //////////////////////////////////////////////////

    $scope.configure = function(block) {

        var modalInstance = $uibModal.open({
            template: '<action-block ng-model="block" close="closeModal"></action-block>',
            size: 'md',
            backdrop: 'static',
            controller: function($scope) {
                $scope.block = block;
                $scope.isModal = true;
                $scope.closeModal = function() {
                    console.log('close modal')
                    $scope.$close();
                }
            }
        });

        // var promise = modalInstance.result;
        // promise.then(successCallback, cancelCallback);
    }

});