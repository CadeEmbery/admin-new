app.directive('testEventConfigure', [function () {


	return {
        restrict: 'E',
        replace: true,
        scope: {
            item: '=ngModel',
            close:'=',
            remove:'=',
        },
        templateUrl: 'fluro-admin-content/types/reaction/test-event-configure/test-event-configure.html',
        link: function($scope, $element, $attrs, $ctrl) {


        }
    }
}])