app.directive('reactionCodeEditor', function() {


    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            options: '=ngParams',
            block: '=',
        },
        templateUrl: 'fluro-admin-content/types/reaction/code-editor/code-editor.html',
        controller: function($scope) {

            /////////////////////////////////


            // $scope.inject = function(text) {

            // }

            var editor;

            /////////////////////////////////

            $scope.aceLoaded = function(e) {
                $scope.editor = editor = e;
                // editor.insert(script.body)
            }

            /////////////////////////////////

            $scope.select = function(row) {

                console.log('INJECT BABY', row, row.type);



                var script = '';
                switch(row.type) {
                    case 'array':

                        script += "<!-- Loop through each item in the array -->\n";
                        script += "<% for(var i = 0; i < get('input." + row.key +".length'); i++) { %>\n";
                        script += "<%= get('input." + row.key +"[' + i +']') %>\n";
                        script += "<% } %>\n";
                    break;
                    case 'date':
                        script = "<%= renderDate('input." +row.key+"', 'h:mm a - dddd, D MMMM  YYYY, ', 'Australia/Melbourne') %>";
                    break;
                    default:
                        script = "<%= get('input." +row.key+"') %>";
                    break;
                }
                
                editor.insert(script)
            }

        }

    }
})