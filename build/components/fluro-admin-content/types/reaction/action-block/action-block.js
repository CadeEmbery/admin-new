app.directive('actionBlock', function($uibModal, TypeService) {


    return {
        restrict: 'E',
        replace: true,
        scope: {
            block: '=ngModel',
            close: '='
        },
        templateUrl: 'fluro-admin-content/types/reaction/action-block/action-block.html',
        link: function($scope, $element, $attrs, $ctrl) {



            $scope.definedTypes = TypeService.definedTypes;
            $scope.types = TypeService.types.slice();

          

            /////////////////////////////////////////////

            $scope.definitionGroups = _.chain(TypeService.definedTypes)
                .reduce(function(set, definition) {

                    var parentType = _.startCase(definition.parentType);

                    var existing = set[parentType];
                    if (!existing) {
                        existing =
                            set[parentType] = {
                                title: parentType,
                                definitions: [],
                            }
                    }

                    existing.definitions.push(definition);

                    return set;
                }, {})
                .values()
                .value();


            $scope.modalCode = function(block, key, options) {


                // var tests = $scope.item.tests;

                var modalInstance = $uibModal.open({
                    // template: '<div class="panel-body"><div class="form-group"><label>Request Body</label><p class="help-block">Structure the data to send with the request</p></div></div><div class="code-body" ng-model="block.'+key+'" ui-ace="{useWrapMode : false, showGutter: true, theme:\'tomorrow_night_eighties\', mode: \'html\'}"></div>',
                    // templateUrl: '/fluro-admin-content/types/reaction/code-editor/code-editor.html',
                    template: '<reaction-code-editor block="block" ng-model="block.' + key + '" ng-params="options"></reaction-code-editor>',
                    size: 'lg',
                    controller: function($scope) {
                        $scope.block = block;
                        $scope.options = options;




                        // $scope.isModal = true;
                        // $scope.closeModal = function() {
                        //     console.log('close modal')
                        //     $scope.$close();
                        // }

                        // $scope.removeTest = function() {
                        //     $scope.$close();
                        //     _.pull(tests, testEvent);
                        // }
                    }
                });
            }

        }
    }
});


app.controller('ProcessActionBlockController', function($scope, TypeService) {

    $scope.$watch('block.data.process', function(processID) {

        if(!processID) {
            return;
        }

        if(processID && processID._id) {
            processID = processID._id;
        }

        var processTypes = TypeService.getSubTypes('process');

        // console.log('PROCESS TYPES', processID, processTypes);

        var targetProcess = _.find(processTypes, {_id:processID});
        if(targetProcess) {
           

            $scope.states = _.get(targetProcess, 'data.states');
        } else {
            $scope.states = [];
        }

    })
})