app.controller('PurchaseFormController', function($scope, $http, Fluro, Notifications) {


    //Save the original email
    $scope.originalEmail = String($scope.item.collectionEmail);



    $scope.$watch('item.product', function(product) {
        if (product) {
            $scope.item.title = product.title;
        }
    })


    $scope.originalEmail = String($scope.item.collectionEmail);



    ///////////////////////////////////////

    $scope.useCount = function() {
        //Get the count
        var licenseCount = 0;
        var managedLicenseCount = 0;

        ///////////////////////////////////////

        if ($scope.item.license && $scope.item.license.length) {
            licenseCount = $scope.item.license.length;
        }

        ///////////////////////////////////////

        if ($scope.item.managedLicense && $scope.item.managedLicense.length) {
            managedLicenseCount = $scope.item.managedLicense.length;
        }

        ///////////////////////////////////////

        //Total licensed count
        return (licenseCount + managedLicenseCount);
    }



    //////////////////////////////////////////////

    $scope.unlicensePersona = function(persona) {
        _.pull($scope.item.managedLicense, persona);
    }

    $scope.unlicenseUser = function(user) {
        _.pull($scope.item.license, user);
    }

    //////////////////////////////////////////////

    $scope.resendStatus = 'ready';


    $scope.resendPurchaseInvitation = function() {


        if (!$scope.item || !$scope.item._id) {
            return;
        }

        if (!$scope.item.collectionEmail || !$scope.item.collectionEmail.length) {
            return;
        }



        $scope.resendStatus = 'processing';

        //Start off with the collection email
        var email = $scope.item.collectionEmail;

        /////////////////////////////////////////////////

        var request = $http.post(Fluro.apiURL + '/purchase/resend/' + $scope.item._id);

        /////////////////////////////////////////////////

        request.then(function(result) {

            $scope.resendStatus = 'ready';
            Notifications.status('Purchase notification sent ' + email);

        }, function(result) {

            var err = result.data;
            
            $scope.resendStatus = 'ready';

            if (err.error) {
                Notifications.error(err.error);
            } else {
                Notifications.error('There was an error sending purchase notification');
            }
        })





    }




});