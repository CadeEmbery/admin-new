app.controller('InteractionFormController', function($scope, FluroAccess, Notifications, FluroContent, $state) {


	if($scope.item && _.keys($scope.item.reactionResults).length) {
		$scope.showReactions = true;
	}

	//////////////////////////////////////

	$scope.amountPaid = function() {
		var totalPaid = 0;

		if($scope.item.transactions && $scope.item.transactions.length) {
			totalPaid += _.chain($scope.item.transactions)
			.map(function(transaction) {
				return transaction.amount;
			})
			.sum()
			.value();
		}

		if($scope.item.manualPayments && $scope.item.manualPayments.length) {
			totalPaid += _.chain($scope.item.manualPayments)
			.map(function(transaction) {
				return transaction.amount;
			})
			.sum()
			.value();
		}

		return totalPaid;
	}

	//////////////////////////////////////
	//////////////////////////////////////
	//////////////////////////////////////

	$scope.retryReaction = function(reaction, key) {

		if(reaction.processing) {
			return;
		}

		//Start the process
		reaction.processing = true;


		function retriggerSuccess(res) {
			reaction.processing = false;
			Notifications.status('Reaction was retriggered successfully');


			_.assign($scope.item.reactionResults, res);

			//Reload the page
			// $state.reload();
		}

		function retriggerFail(res) {
			reaction.processing = false;
            Notifications.error('Reaction retrigger failed');
			console.log('Retrigger Fail', res);
		}

		FluroContent.endpoint('reaction/retrigger').save({
			reaction:key,
			interaction:$scope.item._id,
		}).$promise.then(retriggerSuccess, retriggerFail);




	}

	//////////////////////////////////////


	$scope.createContactLink = function() {

		//Check if the user can create a new contact
		var canCreateContact = FluroAccess.can('create', 'contact');

		//Create a new contact details thingo
		var contactDetails = {}

		//Get the interaction
		var interaction = $scope.item;
		interaction.linking = true;

		//Get the details
		contactDetails.firstName = _.get(interaction, 'rawData._firstName');
		contactDetails.lastName = _.get(interaction, 'rawData._lastName');
		contactDetails.emails = [_.get(interaction, 'rawData._email')];
		contactDetails.phoneNumbers = _.get(interaction, 'rawData._phoneNumber');
		contactDetails.gender = _.get(interaction, 'rawData._gender');

		/////////////////////////////////////////////////////////////////////////

		//Assume if they provide their DOB then it's correct
		var submittedDob = _.get(interaction, 'rawData._dob');
		if(submittedDob) {
			contactDetails.dob = new Date(submittedDob);
			contactDetails.dobVerified = true;
		}

		/////////////////////////////////////////////////////////////////////////

		if(!contactDetails.gender) {
			contactDetails.gender = 'unknown';
		}

		//Add the realms of the interaction
		contactDetails.realms = interaction.realms;

		/////////////////////////////////////////////////////////////////////////

		FluroContent.resource('contact').save(contactDetails).$promise.then(function(res) {


			interaction.contacts = [res];
			interaction.contact = res;

			if(interaction._id) {
				//Save the interaction
				$scope.save();
			}

			interaction.linking = false;
			// Notifications.status(contactDetails.firstName + ' ' + contactDetails.lastName + ' was created successfully');
		}, function(err) {
			interaction.linking = false;

			if(!err.data) {
				return Notifications.error('Error creating contact');
			}

			if(!err.data.errors) {
				if(err.data.message) {
					return Notifications.error(err.data.message);
				} else {
					return Notifications.error(err.data);
				}
			}

			//Mark all errors
			_.each(err.data.errors, function(err) {
				return Notifications.error(err.message);
			})
				
			

		})
		
	}

	//////////////////////////////////////
	//////////////////////////////////////

	$scope.amountDue = function() {

		var originalAmount = $scope.item.amount;
		var amountPaid = $scope.amountPaid();
		
		return originalAmount - amountPaid;
		
	}

});