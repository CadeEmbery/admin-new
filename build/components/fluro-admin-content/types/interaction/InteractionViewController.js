app.controller('InteractionViewController', function($scope, FluroContent, Fluro, $window, Notifications) {


	var contacts = [];
	contacts.push($scope.item.contact);
	contacts = contacts.concat($scope.item.contacts);

	////////////////////////////////////////////////

	$scope.linkedContacts = _.chain(contacts)
	.compact()
	.uniq(function(contact) {
		return contact._id;
	})
	.value();



	 FluroContent.endpoint('tickets/interaction/'+$scope.item._id).query().$promise.then(function(res) {
        $scope.tickets = res;
    });


     $scope.paymentMethod = _.chain($scope.definition)
     .get('paymentDetails.paymentMethods')
     .find({
         key:$scope.item.paymentMethod,
     })
     .value();


	 /////////////////////////////////////////////

    $scope.exportTickets = function() {

        $scope.ticketsExporting = true;
        FluroContent.endpoint('tickets/interaction/' + $scope.item._id + '/csv').get().$promise.then(function(res) {
            console.log('TICKETS EXPORT', res)
            $scope.ticketsExporting = false;

            Notifications.post('Your popup blocker may stop this file from downloading', 'warning');

            var downloadURL = Fluro.apiURL + res.download;

            if (Fluro.token) {
                $window.open(downloadURL + '?access_token=' + Fluro.token);
            } else {
                $window.open(downloadURL);
            }


        }, function(err) {
            console.log('TICKETS EXPORT Error', err)
            $scope.ticketsExporting = false;
        });
    }

	

});