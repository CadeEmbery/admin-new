app.controller('DeploymentFormController', function($scope, Fluro, FluroContent, Notifications) {



	$scope.deploymentStatus = {}

	$scope.redeployBranch = function(key) {

		if(!$scope.deploymentStatus[key]) {
			$scope.deploymentStatus[key] = {};
		}

		$scope.deploymentStatus[key].status = 'processing';

		console.log('Redeploy', key)
		FluroContent.endpoint('deployment/deploy/' + $scope.item._id).save({
			branch:key
		})
		.$promise
		.then(function(res) {
			$scope.deploymentStatus[key].status = 'complete';
			Notifications.status(key + ' branch was deployed successfully');

			_.assign($scope.item, res);

			
		}, function(err) {
			$scope.deploymentStatus[key].status = 'error';
			$scope.deploymentStatus[key].message = err;
			Notifications.error('Error deploying branch')
		})
	}


	$scope.$watch(function() {
		return $scope.item._id;
	}, function(id) {
		$scope.webhookURL = Fluro.apiURL + '/deployment/deploy/' + id + '?branch=master'
	})


});
