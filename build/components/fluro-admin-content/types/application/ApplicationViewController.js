app.controller('ApplicationViewController', function($scope) {

    if($scope.forceSSL) {
    $scope.url = 'https://' + $scope.item.domain;
    } else {
    $scope.url = 'http://' + $scope.item.domain;
    }
});