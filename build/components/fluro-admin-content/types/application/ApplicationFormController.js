app.controller('ApplicationFormController', function($scope, $rootScope, FluroAccess, $http, Notifications) {


    // $scope.timezones = moment.tz.names();


    var defaultDistribution = _.get($scope, 'definition.data.defaultDistribution');
    // var applicationType = _.get($scope, 'item.applicationType');


    //If there is a default distribution set
    if(defaultDistribution && defaultDistribution.length) {

        //If the deployment is not already set
        if(!$scope.item.deployment || !$scope.item.deployment.length) {
            //Use the default distribution
            $scope.item.deployment = defaultDistribution;
        }
    }

    $scope.isSuperUser = function() {
        return FluroAccess.isFluroAdmin();
    }

    if(_.keys($scope.item.pathRedirects).length) {
        $scope.showLegacyRedirects = true;
    }


    $scope.clearCSSCache = function() {


        if(!$scope.item.domain) {
            return;
        }
        $scope.refreshingCSS = true;

        var url = 'https://' + $scope.item.domain + '/appstyle.css?forceRefresh=true';
        $http.get(url).then(function(res) {
            $scope.refreshingCSS = false;
            console.log('SUCCESS CLEAR', $scope.item.domain);
        }, function(err) {
            $scope.refreshingCSS = false;
            console.log('ERROR CLEAR');
        })
    }

    /*
    $scope.$watch('item.permissionSets', function() {

        var filtered = _.filter($scope.item.permissionSets, function(set) {
            if (set && set.account) {
                return set.account._id == $rootScope.user.account._id;
            }
        });

        if (!filtered.length) {
            $scope.item.permissionSets = [{
                account: $rootScope.user.account,
                sets: [],
            }]
        }

        $scope.allowedPermissionSets = filtered;

    })
*/

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////




});