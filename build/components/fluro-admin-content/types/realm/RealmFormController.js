app.controller('RealmFormController', function($scope) {


    $scope.swatches = [
        //Black and white
        {
            title:'Black',
            color:'#eee',
            bgColor: '#555',
        },

        //Red
        {
            title:'Red',
            color:'#8f0f0f',
            bgColor: '#ffb5ba',
        },

        //Pink
        {
            title:'Pink',
            color:'#b3125f',
            bgColor: '#ffa3c4',
        },

        //Pink/Purple
        {
            title:'Purple',
            color:'#7f12b3',
            bgColor: '#e7b3ff',
        },

        //Purple
        {
            title:'Deep Purple',
            color:'#7f12b3',
            bgColor: '#e2a3ff',
        },

        //Violet / Purple
        {
            title:'Violet',
            color:'#4f12b3',
            bgColor: '#c6a3ff',
        },

        //Deep Blue
        {
            title:'Deep Blue',
            color:'#433aab',
            bgColor: '#a5a3ff',
        },

        //Blue
        {
            title:'Baby Blue',
            color:'#174b99',
            bgColor: '#a6caff',
        },

        //Light Blue
        {
            title:'Light Blue',
            color:'#1366a3',
            bgColor: '#9ee2ff',
        },

        //Cyan Teal
        {
            title:'Cyan',
            color:'#008b94',
            bgColor: '#97f0ed',
        },

        //Medical Green
        {
            title:'Medical Green',
            color:'#007357',
            bgColor: '#97f0cb',
        },

        //Light Green
        {
            title:'Light Green',
            color:'#00703a',
            bgColor: '#97f0a5',
        },

        //Lime Green
        {
            title:'Lime Green',
            color:'#00783e',
            bgColor: '#a8ed87',
        },

        //Banana Green
        {
            title:'Banana Green',
            color:'#306b13',
            bgColor: '#dbe37f',
        },

        //Desert Storm
        {
            title:'Desert Camo',
            color:'#6b5813',
            bgColor: '#e3cd7f',
        },

        //Warning Yellow
        {
            title:'Amber',
            color:'#966639',
            bgColor: '#ffe175',
        },

        //Fluro Yellow
        {
            title:'Fluro Yellow',
            color:'#695e00',
            bgColor: '#faff00',
        },

        


        
    ]

    $scope.selectSwatch = function(swatch) {
        $scope.item.bgColor = swatch.bgColor;
        $scope.item.color = swatch.color
    }

    console.log('TESTING', $scope.swatches);
});

app.filter('defaultRealmTitle', function() {
    return function(str) {


        if(!str || !str.length) {
            return 'Realm name';
        }

        return str;
    };
});



