app.controller('IntegrationFormController', function($scope, $http, Fluro, Notifications) {


    $scope.apiURL = Fluro.apiURL;


    $scope.editOnExit = true;

    if(!$scope.item.publicDetails) {
    	$scope.item.publicDetails = {};
    }



    $scope.$watch('item.module', function(module) {
        if (!module) {
            return;
        }
        if (!$scope.item.title) {
            $scope.item.title = 'New ' + module + ' integration';
        }
    })

    ////////////////////////////////////////////////////////////////////

    $scope.authorize = function(url) {


        //////////////////////////////////

        var promise = $scope.contentSave({
            exitOnSave: false,
            status: 'active',
        });

        //////////////////////////////////


        promise.then(function(result) {

	        //The url we want to redirect the user to
	        var url;

	        //Check the module of the integration
	        switch ($scope.item.module) {
	            case 'songselect':
	                url = Fluro.apiURL + '/integrate/songselect/' + $scope.item._id + '/oauth?mode=' + $scope.item.publicDetails.mode
	                break;
	            case 'pushpay':
	                url = Fluro.apiURL + '/integrate/pushpay/' + $scope.item._id + '/oauth?mode=' + $scope.item.publicDetails.mode
	                break;
	            default:
	                break;
	        }
	        //////////////////////////////////

	        //Tell the front end we are trying to load their
	        //url
	        $scope.item.processing = true

	        //Visit the url
	        window.location.href = url;

        })
    }

    ////////////////////////////////////

    $scope.authorizeInstagram = function() {


        if (!$scope.item._id || !$scope.item._id.length) {
            Notifications.warning('Please save before attempting to authorize this integration')
        }

        var clientID = $scope.item.publicDetails.clientID;

        var redirectURI = Fluro.apiURL + '/integrate/instagram/' + $scope.item._id;

        if (Fluro.token) {
            redirectURI += '?access_token=' + Fluro.token;
        }



        if (!clientID || !clientID.length) {
            Notifications.warning('Invalid or missing client ID')
        }

        if (!redirectURI || !redirectURI.length) {
            Notifications.warning('Invalid or missing Redirect URI')
        }

        var url = 'https://api.instagram.com/oauth/authorize/?client_id=' + clientID + '&redirect_uri=' + redirectURI + '&response_type=code'

        if (Fluro.token) {
            url += '&access_token=' + Fluro.token;
        }

        console.log('authorize', url);
        window.open(url);


    }





});