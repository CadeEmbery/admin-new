app.controller('PushPayIntegrationController', function($scope, $http, Fluro, FluroContent, Notifications) {




 // <!-- ng-href="{{apiURL}}/integrate/{{item.module}}/{{item._id}}/transactions" target="_blank" > -->
// <!-- <a ng-href="{{apiURL}}/integrate/{{item.module}}/{{item._id}}/webhook/createAll" target="_blank" class="btn btn-block btn-default"> -->



	$scope.resync = function() {
		console.log('RESYNC')

		$scope.resyncing = true;

		$http.get(Fluro.apiURL + '/integrate/pushpay/' + $scope.item._id + '/transactions')
		.then(function(res) {
			$scope.resyncing = false;
			Notifications.status('Transactions have been synced with Pushpay')
		}, function(err) {
			$scope.resyncing = false;
			Notifications.error('There was an error syncing transactions. Please try again.')
		})
	}


	$scope.relinkWebhooks = function() {
		console.log('RELINKING')

		$scope.webhooking = true;

		$http.get(Fluro.apiURL + '/integrate/pushpay/' + $scope.item._id + '/webhook/create')
		.then(function(res) {
			$scope.webhooking = false;
			Notifications.status('Webhooks created successfully!')
		}, function(err) {
			$scope.webhooking = false;
			Notifications.error('There was an error syncing transactions. Please try again.')
		})
	}


});
