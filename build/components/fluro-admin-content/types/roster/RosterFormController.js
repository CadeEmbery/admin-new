app.controller('RosterFormController', function($scope, $q, $rootScope, $timeout, FluroContent, CacheManager, FluroAccess, Notifications, $stateParams, ModalService, DateTools) {



    $scope.settings = {};
    $scope.proposed = {};

    //////////////////////////////////////////////////

    //Setup the defaults from the definition
    if ($scope.definition) {

        var defaultSlots = _.get($scope.definition, 'data.slots');

        if (!$scope.item.slots && _.isArray(defaultSlots)) {
            $scope.item.slots = defaultSlots;
        }
    }

    //////////////////////////////////////////////////

    if (!$scope.item.temporaryAssignments) {
        $scope.item.temporaryAssignments = [];
    }

    //////////////////////////////////////////////////

    $scope.getSlotAssignments = function(slot, status) {

        return _.chain(slot.assignments)
            .filter(function(assignment) {
                if (status && status.length) {
                    return assignment.status == status;
                } else {
                    return assignment.status != 'archived';
                }
            })
            .sortBy(function(assignment) {
                return assignment.confirmationStatus;
            })
            .sortBy(function(assignment) {
                return assignment.contactNAme;
            })
            
            .uniq(function(assignment) {
                return assignment._id;
            })
            .value();

    }

    //////////////////////////////////////////////////

    $scope.getSlotError = function(slot) {

        var minimum = parseInt(slot.minimum);
        var maximum = parseInt(slot.maximum);
        var count = 0;

        if (slot.assignments && slot.assignments.length) {
            count = slot.assignments.length;
        }

        if (minimum) {
            if (count < minimum) {
                // if (count) {
                // 
                var requireCount = minimum - count;
                var name = 'more';
                if (count) {
                    name = 'more';
                } else {
                    if (requireCount == 1) {
                        name = 'person';
                    } else {
                        name = 'people';
                    }
                }

                return requireCount + ' ' + name + ' required';
            }
        }

        if (maximum) {
            if (count > maximum) {
                return count - maximum + ' too many';
            }
        }
    }

    //////////////////////////////////////////////////

    //Keep track of all the assignments that exist currently on
    //the roster when you open it
    var existingAssignments = _.chain($scope.item.slots)
        .map(function(slot) {
            return slot.assignments;
        })
        .flatten()
        .value();

    //////////////////////////////////////////////////

    $scope.toggleExpanded = function(slot) {
        //console.log('EXPAND', slot);

        if ($scope.settings.expanded == slot) {
            $scope.settings = {};
        } else {
            $scope.settings.expanded = slot;
        }
    }

    //////////////////////////////////////////////////

    $scope.addProposedAssignments = function() {
        //Add the assignments
        $scope.addAssignments($scope.proposed.title, $scope.proposed.contacts);

        //And reset the proposed
        $scope.proposed = {};
    }

    //////////////////////////////////////////////////


    $scope.selectTeam = function() {

        var selection = {};
        var modal = ModalService.browse('team', selection);
        modal.result.then(addTeams, addTeams)

        function addTeams() {

            if (selection.items && selection.items.length) {

                ////////////////////////////////////////////////////

                var newAssignments = _.chain(selection.items)
                    .map(function(team) {
                        return team.assignments;
                    })
                    .flatten()
                    .map(function(proposed) {
                        $scope.addAssignments(proposed.title, proposed.contacts);
                    })
                    .value();
            }
        }
    }



    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    $scope.canEditAssignment = function(assignment) {

        //If it's temporary then you cant edit it
        if (assignment.confirmationStatus == 'temporary') {
            return;
        }

        //Add the _type and check using FluroAccess service
        assignment._type = 'assignment';
        return $rootScope.access.canEditItem(assignment);
    }

    //Check if you can delete an assignment
    $scope.canDeleteAssignment = function(assignment) {

        //If it's temporary then you can remove it easily
        if (assignment.confirmationStatus == 'temporary') {
            return true;
        }

        //Add the _type and check using FluroAccess service
        assignment._type = 'assignment';
        return $rootScope.access.canDeleteItem(assignment);
    }

    ////////////////////////////////////////////////////

    $scope.reactivate = function(assignment) {


        //Set the assignment to 'archived'
        FluroContent.resource('assignment/' + assignment._id).update({
                status: 'active',
            })
            .$promise
            .then(function(res) {
                assignment.status = res.status;
            }, function(err) {
                //console.log('ERROR hiding assignment', err)
            })
    }

    $scope.getAllReminders = function() {

        var definitionReminders = _.get($scope.definition, 'data.defaultReminders') || [];
        var rosterReminders = _.get($scope.item, 'reminders') || [];

        return definitionReminders.length + rosterReminders.length;
    }

    ////////////////////////////////////////////////////

    //Function for editing an assignment
    $scope.editAssignment = function(assignment) {

        if (assignment.confirmationStatus == 'temporary') {
            return;
        }

        //Copy the object
        var itemObject = angular.copy(assignment);

        //Add the extra meta details we need to open the 
        //editing panel
        itemObject._type = 'assignment';
        itemObject.realms = $scope.item.realms;

        ///////////////////////////////////////////////////

        // ////console.log('MODAL EDIT', itemObject);
        ModalService.edit(itemObject, editComplete);

        ///////////////////////////////////////////////////

        function editComplete(res) {

            //Find the assignment update it with an _id
            assignment._id = res._id

            //Update the status of the assignment so it removes from the list
            _.assign(assignment, res);
            //console.log('Donezo!');
            refreshList();
        }

    }


    ////////////////////////////////////////////////////

    $scope.removeAssignment = function(assignment) {

        //console.log('WOOT DELETE', assignment);

        if (assignment.confirmationStatus == 'temporary') {

            _.pull($scope.item.temporaryAssignments, assignment);
            refreshList();
            return;
        }

        /////////////////////////////////////////////////////

        //If the assignment has been rejected then we want
        //to archive it and hide it from the list
        if (assignment.confirmationStatus == 'denied') {
            //console.log('Hide the assignment')
            return $scope.hideAssignment(assignment);
        }

        /////////////////////////////////////////////////////

        //Create a copy
        var itemObject = angular.copy(assignment);
        itemObject._type = 'assignment';
        itemObject.realms = $scope.item.realms;

        /////////////////////////////////////////////////////

        //Remove the item by opening the delete panel to double check
        ModalService.remove(itemObject, removeComplete);

        function removeComplete(res) {

            //Find the assignment update it with an _id
            assignment._id = res._id

            //Update the status of the assignment so it removes from the list
            _.assign(assignment, res);
            //console.log('Donezo!');

            //Pull it out of each slot
            _.each($scope.item.slots, function(slot) {
                _.pull(existingAssignments, assignment);
                _.pull(slot.assignments, assignment);
            })

            refreshList();
        }
    }


    ////////////////////////////////////////////////////

    $scope.addToSlot = function(slot) {
        if (!slot) {
            return //console.log('Invalid slot');
        }

        if (!slot.assignments) {
            slot.assignments = [];
        }

        var selection = {};
        var modal = ModalService.browse('contact', selection, {});

        //Close
        function closed() {
            if (selection.items.length) {
                $scope.addAssignments(slot.title, selection.items);
            }
        }

        //Close of modal
        modal.result.then(closed, closed)
    }

    ////////////////////////////////////////////////////

    $scope.hideAssignment = function(assignment) {

        //Set the assignment to 'archived'
        FluroContent.resource('assignment/' + assignment._id).update({
                status: 'archived',
            })
            .$promise
            .then(function(res) {
                assignment.status = 'archived';
                refreshList();
            }, function(err) {
                //console.log('ERROR hiding assignment', err)
            })
    }

    //////////////////////////////////////////////////

    function refreshList() {

        var allAssignments = getAllAssignments();

        //////////////////////////////////////////////

        // $timeout(function() {
        _.each($scope.item.slots, function(slot) {
            slot.assignments = _.filter(allAssignments, function(assignment) {
                return String(assignment.title).toLowerCase() == String(slot.title).toLowerCase();
            });
        });

        // })
    }


    ////////////////////////////////////////////////////

    $scope.removeSlot = function(slot) {
        _.pull($scope.item.slots, slot);
    }

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    $scope.addAssignments = function(assignmentName, contacts) {
        //console.log('Add assignments to temporary')

        //////////////////////////////////////////////////

        if (!assignmentName || !assignmentName.length) {
            return //console.log('No assignment name specified');
        }

        //////////////////////////////////////////////////

        var matchingSlot = _.some($scope.item.slots, function(slot) {
            return String(slot.title).toLowerCase() == assignmentName.toLowerCase();
        })

        if (!matchingSlot) {
            //If no slots exist create an array
            if (!$scope.item.slots) {
                $scope.item.slots = [];
            }

            matchingSlot = {
                title: assignmentName,
                assignments: []
            }


            $scope.item.slots.push(matchingSlot)
        }

        

        //////////////////////////////////////////////////


        //Join it all together
        allAssignments = _.compact([].concat(existingAssignments, $scope.item.temporaryAssignments));

        //////////////////////////////////////////////////

        //Map the requests
        var assignments = _.chain(contacts)
            .map(function(contact) {

                var contactID = contact._id;
                if (contactID._id) {
                    contactID = contactID._id;
                }

                //////////////////////////////////////////////

                var alreadyAdded = _.some(allAssignments, function(ass) {

                    var existingContactID = ass.contact;
                    if (existingContactID._id) {
                        existingContactID = existingContactID._id;
                    }

                    var sameName = String(ass.title).toLowerCase() == String(assignmentName).toLowerCase()
                    var sameContact = existingContactID == contactID;

                    return sameName && sameContact;
                })


                if (alreadyAdded) {
                    console.log('ALREADY ADDED', contactID, matchingSlot.title);
                    return;
                }

                //////////////////////////////////////////////

                var data = {
                    _id: guid(),
                    title: assignmentName,
                    contact: contact,
                    contactName: contact.title,
                    confirmationStatus: 'temporary',
                    status: 'active',
                }




                return data;
            })
            .compact()
            .value();

        //////////////////////////////////////////////////

        $scope.item.temporaryAssignments = $scope.item.temporaryAssignments.concat(assignments);

        ///////////////////////////////////

        return refreshList();
    };

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    function getAllAssignments() {
        //Get all assignments
        var allAssignments = [];

        //////////////////////////////////////////////

        var temporaryAssignments = $scope.item.temporaryAssignments;

        //////////////////////////////////////////////
        //////////////////////////////////////////////

        //Join it all together
        allAssignments = _.compact(allAssignments.concat(existingAssignments, temporaryAssignments));

        return allAssignments;
    }

    ////////////////////////////////////////////////////

    $scope.nudging = false;


    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    $scope.nudgeAll = function() {
        $scope.nudging = true;


        var allAssignments = getAllAssignments();

        //////////////////////////////////////////

        var ids = _.chain(allAssignments)
            .filter(function(assignment) {
                return (assignment.confirmationStatus == 'unknown' && assignment.status != 'archived');
            })
            .map(function(assignment) {
                return assignment._id;
            })
            .value();

        //////////////////////////////////////////

        if (!ids.length) {
            $scope.nudging = false;
        }

        //////////////////////////////////////////

        FluroContent.endpoint('assignments/nudge').save({
                assignments: ids
            })
            .$promise
            .then(function(res) {
                $scope.nudging = false;

                //console.log('Nudged everyone!', res);
                if (res.success.length) {
                    Notifications.status(res.success.length + ' nudge notifications sent');
                } else {
                    Notifications.warning('No nudges were sent');
                }

            }, function(err) {
                $scope.nudging = false;
                //console.log('Error', err);

                Notifications.error(err.message);
            })
    }


    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    //If this roster is brand new and has no _id
    //then gather all the assignments and make them temporary
    if (!$scope.item._id) {

        $scope.item.temporaryAssignments = _.chain($scope.item.slots)
            .map(function(slot) {
                return _.map(slot.assignments, function(assignment) {
                    assignment.title = slot.title;
                    return assignment;
                });
            })
            .flatten()
            .map(function(assignment) {

                delete assignment._id;
                assignment.title
                assignment.confirmationStatus = 'temporary';
                assignment.roster = $scope.item._id;

                return assignment;
            })
            .value();
    }

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////



    $scope.save = function(options) {

        console.log('TEMPORARY', $scope.item.temporaryAssignments)

        var parentEvent = $scope.item.event;

        if (parentEvent && parentEvent.definition) {
            CacheManager.clear(parentEvent.definition);
        } else {
            CacheManager.clear('event');
        }


        return $scope.contentSave(options);
    }



    $scope.saveSuccess = function(res) {

        $scope.item.slots = res.slots;
        $scope.item.temporaryAssignments = [];

        $scope.contentSaveSuccess(res);
    };


    /**
        function saveTemporaryAssignments(rosterID, callback) {

            var array = _.get($scope.item, 'temporaryAssignments');

            if (!array) {
                //console.log('NO TEMPS TO SAVE');
                //No assignments
                return callback();
            }

            //////////////////////////////////////////////////

            var tempAssignments = array.slice(0);

            //////////////////////////////////////////////////

            //Map the requests
            var requests = _.map(tempAssignments, function(data) {
                var copy = angular.copy(data);
                copy.confirmationStatus = 'unknown';
                copy.ignoreRosterUpdate = true;

                var promise = FluroContent.endpoint('assignments/roster/' + rosterID).save(copy).$promise;

                //Listen for when its done
                promise.then(function(res) {
                    data._id = res._id;
                    data.confirmationStatus = res.confirmationStatus;
                    data.realms = res.realms;
                    data.owners = res.owners;
                    data.managedOwners = res.managedOwners;
                    data.author = res.author;
                    data.managedAuthor = res.managedAuthor;
                    data._type = res._type;
                    data.definition = res.definition;

                    //Pull from temporary
                    _.pull($scope.item.temporaryAssignments, data);

                    //Add to the assigned list
                    if (!$scope.item.assigned) {
                        $scope.item.assigned = [];
                    }

                    $scope.item.assigned.push(data)
                })


                return promise;
            });

            //////////////////////////////

            $q.all(requests)
                .then(function(res) {
                    // //console.log('RESULT', res);

                    return callback();
                }, function(err) {

                    if (err.data && err.data.errors && err.data.errors.contact) {
                        return Notifications.error(err.data.errors.contact.message);
                    }
                    // if(err.data) {
                    //     if(err.data.message) {
                    //         Notifications.error(err.data.message);
                    //     } else {
                    //         Notifications.error(err.data);
                    //     }
                    // } else {
                    Notifications.error(err);
                    // }


                });
        }

        //////////////////////////////////////////////////
    /**
        var lastSaveOptions;



        //////////////////////////////////////////////////


        $scope.save = function(options) {

                lastSaveOptions = options;

                if (!$scope.item._id) {

                    if (!options) {
                        options = {};
                    }
                    options.stayAfterSave = true;


                    var parentEvent = $scope.item.event;

                    if (parentEvent && parentEvent.definition) {
                        CacheManager.clear(parentEvent.definition);
                    } else {
                        CacheManager.clear('event');
                    }
                    // //console.log('NO ID SO SAVE AS NORMAL')
                    return $scope.contentSave(options);
                }

                if (!$scope.item.temporaryAssignments || !$scope.item.temporaryAssignments.length) {
                    // //console.log('NO ASSIGNMENTS SAVE AS NORMAL')
                    return $scope.contentSave(options);
                }

                ////////////////////////////////////////////

                //If we are updating an existing roster then we save all the temporary assignments
                //before we save the main thing

                saveTemporaryAssignments($scope.item._id, function() {
                    //Once all are saved then run the usual contentSave function
                    $scope.contentSave(options);
                });
            }
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////

        //Override the function that is usually fired on save success
        $scope.saveSuccess = function(res) {

            //console.log('SAVE SUCCESS');

            //if the content already existed
            if ($scope.item._id) {


                // refreshList();
                // //console.log('NO RESAVE', $scope.item)

                // //We need this when a roster is created in a modal
                // //so it returns the slots back to the event page
                // //its an ugly hack but necassary for the moment
                // return $timeout(function() {
                $scope.contentSaveSuccess(res);
                // });
            }

            ////////////////////////////////////////////

            if (!$scope.item.temporaryAssignments || !$scope.item.temporaryAssignments.length) {
                //console.log('no assignments so save as normal')
                return $scope.contentSaveSuccess(res);
            }

            //SEt the ID
            $scope.item._id = res._id;

            //Save Again
            //console.log('SET ID AND SAVE', $scope.item);
            $scope.save(lastSaveOptions);

            // //If the roster is new then save the temporary assignments now
            // //Save the temporary assignments
            // saveTemporaryAssignments(res._id, function() {

            //     //console.log('NEW ROSTER Temporary assignments saved')
            //         //Once all are saved then run the usual contentSave function
            //     $scope.contentSaveSuccess(res);
            // });

        };

        /**/


    //////////////////////////////////////////


})