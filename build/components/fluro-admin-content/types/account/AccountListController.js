app.controller('AccountListController', function($scope, $rootScope, SearchService, CacheManager, FluroSocket, UserSessionService, TypeService, Notifications, Fluro, FluroTokenService, FluroContent) {

    $scope.isActiveAccount = function(id) {

        return true;

        var found = _.find($rootScope.user.availableAccounts, {
            _id: id
        });
        if (found) {
            return true;
        }
    }

    ////////////////////////////////////////////////////////////

    $scope.isMyAccount = function(_id) {
        return ($rootScope.user.account._id == _id);
    }

    ////////////////////////////////////////////////////////////

    $scope.loginToAccount = function(id, asSession) {

        console.log('Login to account', asSession);

        if (asSession) {
            Fluro.sessionStorage = true;

            function success(res) {
                console.log('success now clear caches')
                CacheManager.clearAll();
                SearchService.clear();
                TypeService.refreshDefinedTypes();
                Notifications.status('Switched session to ' + res.data.account.title);
            }

            function fail(res) {
                Notifications.error('Error creating token session');
            }

            var request = FluroTokenService.getTokenForAccount(id);

            //Callback when the promise is complete
            request.then(success, fail);

        } else {

            // //Update the user
            // FluroContent.resource('user').update({
            //     id: $rootScope.user._id,
            // }, {
            //     account: id
            // }, loginSuccess, loginFailed);


            FluroSocket.off('session', UserSessionService.refresh)
                //Update the user
            FluroContent.endpoint('auth/switch/' + id).save({}).$promise.then(loginSuccess, loginFailed);
        }
    }

    ////////////////////////////////////////////////////////////

    $scope.toggleAccount = function(id) {
        if ($rootScope.user) {

            if (id == $rootScope.user.account._id) {
                Notifications.error('Can not deactivate active account');
                return;
            }

            //Get the current user
            var user = angular.copy($rootScope.user);
            var list = user.availableAccounts;

            var found = _.find(list, {
                _id: id
            })
            if (found) {
                _.pull(list, found);
            } else {
                list.push(id);
            }

            //Update the user
            FluroContent.resource('user').update({
                id: user._id,
            }, {
                availableAccounts: _.uniq(list)
            }, loginSuccess, loginFailed);
        }
    }

    ////////////////////////////////////////////////////////////

    // function updateSuccess(data) {
    //     console.log('success now clear caches')
    //     $rootScope.user = data;
    //     CacheManager.clearAll();
    SearchService.clear();;
    //     TypeService.refreshDefinedTypes();
    //     Notifications.status('Your account list has been updated');
    //     console.log('Update success', data);
    // }

    function loginSuccess(data) {

        FluroTokenService.deleteSession();
        CacheManager.clearAll();
        SearchService.clear();;
        $rootScope.user = data;
        TypeService.refreshDefinedTypes();
        Notifications.status('Logged in to ' + data.account.title);

        //Start Listening Again
        FluroSocket.on('session', UserSessionService.refresh)
    }

    function loginFailed(data) {
        Notifications.error('Failed to update account information');
        console.log('Update failed', data);

        //Start Listening Again
        FluroSocket.on('session', UserSessionService.refresh)
    }





});