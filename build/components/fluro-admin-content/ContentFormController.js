/////////////////////////////////////////////////

app.directive('contentForm', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            formModel: '=ngModel',
        },
        templateUrl: 'fluro-admin-content/types/form.html',
        controller:function($scope, $controller, $q, TypeService) {


            $scope.item = $scope.formModel;
            $scope.extras = {};
            $scope.type = TypeService.getTypeFromPath($scope.item._type);
            if($scope.item.definition) {
                $scope.definition = TypeService.getTypeFromPath($scope.item._type);
            }




            ///////////////////////////////////////////////////////

            // var item = $scope.formModel;
            // var extras = $q.defer();
            // extras.resolve({});


            // var type = $q.defer();
            // type.resolve(TypeService.getTypeFromPath(item._type));

            // var def;
            // if($scope.definition) {
            //     def = TypeService.getTypeFromPath(item.definition);
            // }

            //  var definition = $q.defer();
            // definition.resolve(def);
            

            ///////////////////////////////////////////////////////



            // extras.resolve({});


            // var extras = $q.defer();
            // extras.resolve({});
            $scope.disableHeaderAndFooter = true;
            

            $controller('ContentFormController', {
                $scope: $scope,
                item:$scope.item,
                extras:$scope.extras,
                type:$scope.type,
                definition:$scope.definition,
            });
        },
    };
});


app.controller('ContentFormController', function($rootScope, $q, Fluro, $filter, $uibModalStack, $stateParams, ModalService, $scope, FluroStorage, extras, FluroSocket, $rootScope, $state, $timeout, $controller, Asset, Notifications, CacheManager, TypeService, FluroContent, type, definition, item) {

    // console.log('CREATE', item, type, extras, definition);


    if (!item.data) {
        item.data = {};
    }

    $scope.settings = {};
    $scope.layout = {};


    $scope.item = item;
    $scope.extras = extras;


    $scope.importedData = {};

    $scope.dateScheduling = {};

    //////////////////////////////

    switch (type.path) {
        case 'article':
        case 'asset':
        case 'audio':
        case 'video':
        case 'image':
        case 'code':
        case 'collection':
        case 'team':
        case 'post':
        case 'product':
        case 'event':
            $scope.dateScheduling.enabled = true;
            break;

    }

    //////////////////////////////

    $scope.showFooter = function(str) {
        if ($scope.layout.footerState == str) {
            $scope.layout.footerState = null;
        } else {
            $scope.layout.footerState = str;
        }
    }

    //////////////////////////////

    $scope.dateSchedulingSummary = function() {

        var str;
        var now = new Date();

        if ($scope.item.publishDate) {
            if (new Date($scope.item.publishDate) > now) {
                return 'Auto publish ' + $filter('timeago')($scope.item.publishDate);
            }
        }

        if ($scope.item.archiveDate) {
            if (new Date($scope.item.archiveDate) > now) {
                return 'Auto archive ' + $filter('timeago')($scope.item.archiveDate);
            }
        }

        return str;
    }

    //////////////////////////////
    //////////////////////////////
    //////////////////////////////

    $scope.getTotalPostCount = function() {

        if ($scope.item && $scope.item._id) {

            if ($scope.item._type == 'process' && $scope.item.item) {

            } else {
                return _.sum($scope.item.posts, function(postType) {
                    return postType.count;
                });
            }

        }
    }


    ////////////////////////////////////////////////////////

    $scope.$watch('importedData.json', function(imported) {

        // ////console.log('Imported', imported);
        if (imported && imported.data) {



            var split = imported.data.split(',')[1];
            var json = window.atob(split);
            var object = JSON.parse(json);

            //Delete stupid data
            delete object.updatedBy;
            delete object._id;
            delete object.created;
            delete object.updated;
            delete object.realms;
            delete object.owners;
            delete object.author;
            delete object.managedOwners;
            delete object.managedAuthor;
            delete object.permissionSets;
            delete object.policies;
            delete object.definition;
            delete object.__v;
            delete object.account;
            delete object.slug;

            console.log('IMPORT CLEAN', object);


            //Reset the item
            _.assign($scope.item, object);
        }
    })


    $scope.viewText = 'View';

    var defaultSaveText;

    if ($scope.item._id) {
        defaultSaveText = $scope.saveText = 'Save';
        $scope.cancelText = 'Close';
    } else {
        defaultSaveText = $scope.saveText = 'Create';
        $scope.cancelText = 'Cancel';
    }

    //Force defaults for saving
    $scope.defaultSaveOptions = {};


    $scope.type = type;
    $scope.definition = definition;


    switch (type.path) {
        case 'query':
            $scope.viewText = 'View results';
            break;
        case 'mailout':
            $scope.viewText = 'Preview / Send';
            break;
    }

    //Template
    $scope.template = 'fluro-admin-content/types/' + type.path + '/form.html';

    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;


    //////////////////////////////

    $scope.layout = {};

    //////////////////////////////

    //Cancel path

    if (definition && definition.definitionName) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;
        $scope.definedType = definition.definitionName;
        $scope.icon = definition.parentType;

        $scope.hasDefinedFields = _.keys(definition.fields).length;
    } else {
        $scope.definedType = type.path;
        $scope.icon = type.path;
    }

    ///////////////////////////////////

    $scope.titleLabel = 'Title';
    $scope.bodyLabel = 'Body';

    if (definition && definition.definitionName) {
        if (definition.data) {
            if (definition.data.titleLabel && definition.data.titleLabel.length) {
                $scope.titleLabel = definition.data.titleLabel;
            }

            if (definition.data.bodyLabel && definition.data.bodyLabel.length) {
                $scope.bodyLabel = definition.data.bodyLabel;
            }
        }
    }

    //////////////////////////////

    //Setup Local storage
    var session;

    if ($scope.item._id) {
        session = FluroStorage.sessionStorage('form-' + $scope.item._id)
    }


    //////////////////////////////

    $scope.openAPIDocument = function() {
        var url = Fluro.apiURL + '/content/' + $scope.definedType + '/' + $scope.item._id + '?context=edit';

        if (Fluro.token) {
            url += '&access_token=' + Fluro.token;
        }

        //Open in a new window
        window.open(url);
    }

    //////////////////////////////
    //////////////////////////////
    //////////////////////////////
    //////////////////////////////

    $scope.getFullViewSref = function(item) {
        var typePath = $scope.type.path;
        var defName = item.definition;


        switch (item.parentType) {
            case 'process':
                return 'process.custom({definition:"' + item.definitionName + '"})';
                break;
        }

        var string = typePath + ".view({id: '" + item._id;

        if (defName && defName.length) {
            string += "', definitionName:'" + defName + "'})";
        } else {
            string += "'})";
        }

        return string;
    }


    //////////////////////////////



    var socketEnabled = true;
    /**/


    FluroSocket.on('content.edit', socketUpdate);

    if ($scope.item._type == 'purchase') {
        FluroSocket.on('purchase.expire', socketUpdate);
        FluroSocket.on('purchase.renew', socketUpdate);
    }



    function socketUpdate(socketEvent) {

        ////console.log('Socket updated', socketEvent);

        if (socketEnabled) {

            //////////////////////////////////////////////

            var socketItemID = socketEvent.item;

            if (!socketItemID) {
                return;
            }

            if (socketItemID._id) {
                socketItemID = socketItemID._id;
            }

            //////////////////////////////////////////////

            //Same id content
            var sameId = (socketItemID == $scope.item._id);

            //////////////////////////////////////////////

            var newVersion = (socketEvent.data.updated != item.updated);

            //Make sure we only update if we weren't the originating socket
            var differentSocket = (socketEvent.ignoreSocket != FluroSocket.getSocketID());

            //////////////////////////////////////////////

            if (sameId && newVersion && differentSocket) {

                $timeout(function() {
                    //Assign the new content
                    socketEnabled = false;

                    //Patch the differences
                    jsondiffpatch.patch($scope.item, socketEvent.data.diff);

                    //Renable the socket
                    socketEnabled = true;

                    // ////console.log($rootScope.user._id, data.user._id)
                    if (socketEvent.user) {
                        if ($rootScope.user._id != socketEvent.user._id) {
                            Notifications.warning('This content was just updated by ' + socketEvent.user.name);
                        } else {
                            Notifications.status('You updated this in another window');
                        }
                    } else {
                        Notifications.warning('This content was just updated by another user');
                    }
                })

            }
        }
    }

    /**/

    //////////////////////////////////////////

    /*
    $scope.$watch('item', function(newData, oldData) {

        if (socketEnabled) {
            //////////////////////////////////////////


            //newData.updated = new Date();

            //Broadcast the message
            var message = {
                event: 'content.live',
                item: newData,
                user: {
                    _id: $rootScope.user._id,
                    name: $rootScope.user.name,
                    account: {
                        _id: $rootScope.user.account._id
                    }
                }
            }


            //////////////////////////////////////////

            FluroSocket.emit('content', message, function(res) {
                ////console.log('FluroContent Emitted')
            })
        }

        //////////////////////////////////////////


    }, true)
*/


    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    $scope.useInlineTitle = (type.path == 'article');

    //////////////////////////////////////////////////

    $scope.getAssetUrl = Asset.getUrl;
    $scope.getDownloadUrl = Asset.downloadUrl;


    //Cleanup Pasted HTML
    $scope.cleanupPastedHTML = $rootScope.cleanupPastedHTML;


    //////////////////////////////////////////////////

    $(document).on('keydown', keyDown);

    function keyDown(event) {
        if (event.ctrlKey || event.metaKey) {
            switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's':
                    event.preventDefault();
                    event.stopPropagation();
                    $scope.save();
                    break;
            }
        }
    }
    //////////////////////////////////////////////////

    $scope.$on("$destroy", function() {
        $(document).off('keydown', keyDown);

        FluroSocket.off('content.edit', socketUpdate);
    });


    //////////////////////////////////////////////////

    $scope.saveState = 'ready';
    //////////////////////////////////////////////////

    //Save

    $scope.contentSave = function(options) {


        var deferred = $q.defer();

        //////////////////////////////////////////


        $scope.saveState = 'saving';
        $scope.saveText = 'Saving';

        //Disable Socket
        socketEnabled = false;

        // ////console.log('Disable socket')

        if (!options) {
            options = $scope.defaultSaveOptions;
        }
        //////////////////////////////////////////

        //Use save options

        if (options.status) {
            status = options.status;
        }

        if (options.exitOnSave) {
            saveSuccessCallback = exitAfterSave;
        }

        if (options.forceVersioning) {

            if (!$scope.item.version || $scope.item.version.length) {
                // ////console.log('Forcing a version save');
                $scope.item.version = 'Changes made by ' + $rootScope.user.name;
            }
        }

        //////////////////////////////////////////

        // ////console.log('Save item', $scope.item.applicationIcon, $scope.item.icon);

        //////////////////////////////////////////

        var socketID = FluroSocket.getSocketID();

        //Dont listen to it's own broadcast
        $scope.item.ignoreSocket = socketID;

        if ($scope.item._id) {


            //console.log('ITEM UPDATED');



            //Edit existing content
            FluroContent.resource($scope.definedType).update({
                id: $scope.item._id,
                replaceData: true, //replace the data and remove keys that we dont save back (dont merge)
            }, $scope.item, $scope.saveSuccess, $scope.saveFail)
            .$promise
            .then(deferred.resolve, deferred.reject);


        } else {

            /*if (type.path == 'event') {
                FluroContent.resource($scope.definedType).save($scope.item, $scope.saveSuccess, $scope.saveFail);
            } else {
                */

            if (options.stayAfterSave) {
                FluroContent.resource($scope.definedType).save($scope.item, $scope.saveSuccess, $scope.saveFail)
                .$promise
                .then(deferred.resolve, deferred.reject);
            } else {
                FluroContent.resource($scope.definedType).save($scope.item, $scope.saveSuccessExit, $scope.saveFail)
                .$promise
                .then(deferred.resolve, deferred.reject);
            }

        }


        return deferred.promise;
    }


    //////////////////////////////////////////////////

    $scope.contentSaveSuccess = function(result) {
        ////console.log('CONTENT SAVE SUCCESS');

        $scope.saveState = 'success';
        $scope.saveText = 'Saved';

        //Clear cache
        console.log('Clear cache', $scope.definedType, type.path, _.get(definition, 'definitionName'));
        CacheManager.clear(type.path);
        if (definition) {
            CacheManager.clear(definition.definitionName);
        }

        if (result._id) {
            //////console.log('Updated to version', $scope.item.__v, result.__v);
            $scope.item.__v = result.__v;
        }

        if (result._type == 'definition') {
            TypeService.sideLoadDefinition(result);
        }


        if (result.name) {
            Notifications.status(result.name + ' saved successfully')
        } else {
            Notifications.status(result.title + ' saved successfully')
        }


        //Disable Socket
        socketEnabled = true;
        //////console.log('Enable Socket')

        //////////////////////////////////////////////////


        //Able to close and keep moving
        if ($scope.closeOnSave) {

            if ($scope.$close) {
                //console.log('CLOSE 2')
                $scope.$close(result)
            }

            if ($stateParams.backOnClose) {
                console.log('go back')
                window.history.back();
            } else {

                if (definition && type.path != 'contact') {
                    $state.go(type.path + '.custom', {
                        definition: definition.definitionName
                    });
                } else {
                    //Go to path
                    $state.go(type.path)
                }
            }

            return true;
        }


        //////////////////////////////////////////////////

        //console.log('CALL SAVE FINISHED')
        //Finish the save
        return $scope.saveFinished(result);
    }

    //////////////////////////////////////////////////

    //Use content save by default so that other controllers
    //can override it
    $scope.save = $scope.contentSave;
    $scope.saveSuccess = $scope.contentSaveSuccess;

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    $scope.saveFinished = function(result) {

        // var updatedFields = _.pick(result, [
        //     'updated',
        //     'updatedBy',
        // ]);



        // //Update details from the result
        //  _.assign($scope.item, updatedFields);

        if ($scope.$close) {
            //console.log('CLOSE 3')
            return $scope.$close(result);
        } else {
            $timeout(function() {
                $scope.saveState = 'ready';
                $scope.saveText = defaultSaveText;



            }, 4000)
            return false;
        }
    }

    //////////////////////////////////////////////////

    $scope.saveSuccessExit = function(result) {
        // //console.log('Save Success EXIT')
        $scope.saveSuccess(result);

        // //console.log('TESTINGATTT', result, res);

        if ($scope.viewOnExit) {

            $uibModalStack.dismissAll();

            //console.log('View on exit', result);
            return $state.go(type.path + '.view', {
                definitionName: result.definition,
                id: result._id
            });
        }

        if ($scope.editOnExit) {

            $uibModalStack.dismissAll();

            //console.log('View on exit', result);
            return $state.go(type.path + '.edit', {
                definitionName: result.definition,
                id: result._id
            });
        }

        ////////////////////////////////////

        if ($scope.$close) {
            return $scope.$close(result);
        }

        ////////////////////////////////////

        // if (!result) {
        if (definition) {
            $state.go(type.path + '.custom', {
                definition: definition.definitionName
            });
        } else {
            //Go to path
            $state.go(type.path)
        }
        // }
    }



    //////////////////////////////////////////////////

    $scope.saveFail = function(result) {

        $scope.saveState = 'error';
        $scope.saveText = 'Error';

        $timeout(function() {
            $scope.saveState = 'ready';
            $scope.saveText = defaultSaveText;
        }, 4000)

        if (!result) {
            return Notifications.error('Failed to save')
        }


        if (!result.data) {
            return Notifications.error('Failed to save')
        }

        if (result.data.errors) {
            _.each(result.data.errors, function(err) {
                if (_.isObject(err)) {
                    Notifications.error(err.message)
                } else {
                    Notifications.error(err)
                }
            });
            return;
        }

        if (result.data) {
            return Notifications.error(result.data)
        }

        return Notifications.error('Failed to save document')


    }

    //////////////////////////////////////////////////

    $scope.cancel = function() {

        if ($scope.$dismiss) {
            return $scope.$dismiss();
        }


        // if ($stateParams.backOnClose || type.path == 'contact') {
        //         console.log('go back')
        //         window.history.back();
        //     } else {

        //         if (definition && type.path != 'contact') {
        //             $state.go(type.path + '.custom', {
        //                 definition: definition.definitionName
        //             });
        //         } else {
        //             //Go to path
        //             $state.go(type.path)
        //         }
        //     }




        if ($stateParams.backOnClose ) {
            window.history.back();
        } else {

            if ($scope.item._id == $rootScope.user._id) {
                return $state.go('home')
            }

            //Go to the normal path
            if (definition && definition.definitionName && type.path != 'contact') {
                $state.go(type.path + '.custom', {
                    definition: definition.definitionName
                });
            } else {
                $state.go(type.path)
            }
        }
    }





    var realmSelectEnabled = true;
    var tagSelectEnabled = true;

    switch ($scope.type.path) {
        case 'account':
            realmSelectEnabled = false;
            tagSelectEnabled = true;
            $controller('AccountFormController', {
                $scope: $scope
            });
            break;
        case 'role':
            tagSelectEnabled = false;
            break;
        case 'tag':
            tagSelectEnabled = false;
            break;
        case 'event':
            $controller('EventFormController', {
                $scope: $scope
            });
            break;
        
        case 'eventtrack':
            $controller('EventTrackFormController', {
                $scope: $scope
            });
            break;
        case 'roster':
            $controller('RosterFormController', {
                $scope: $scope
            });
            break;
        case 'ticketing':
            $controller('TicketingFormController', {
                $scope: $scope
            });
            break;
        case 'process':
            $controller('ProcessFormController', {
                $scope: $scope
            });
            break;
        case 'attendance':
            $controller('AttendanceFormController', {
                $scope: $scope
            });
            break;
        case 'integration':
            $controller('IntegrationFormController', {
                $scope: $scope
            });
            break;
            /**/
        case 'application':
            $controller('ApplicationFormController', {
                $scope: $scope
            });
            break;
        case 'method':
            $controller('PaymentMethodFormController', {
                $scope: $scope
            });
            break;
            /**/
        case 'user':
            //realmSelectEnabled = false;
            tagSelectEnabled = false;

            $controller('UserFormController', {
                $scope: $scope
            });
            break;
        case 'persona':
            //realmSelectEnabled = false;
            tagSelectEnabled = false;

            $controller('PersonaFormController', {
                $scope: $scope
            });
            break;
        case 'contactdetail':
            //realmSelectEnabled = false;
            tagSelectEnabled = false;

            $controller('ContactDetailFormController', {
                $scope: $scope
            });
            break;
        case 'contact':
            //realmSelectEnabled = false;
            // tagSelectEnabled = false;

            $controller('ContactFormController', {
                $scope: $scope
            });
            break;
        case 'family':


            $controller('FamilyFormController', {
                $scope: $scope
            });
            break;
        case 'query':
            $controller('QueryFormController', {
                $scope: $scope
            });
            break;
        case 'process':
            $controller('ProcessFormController', {
                $scope: $scope
            });
            break;
        case 'mailout':
            $controller('MailoutFormController', {
                $scope: $scope
            });
            break;
        case 'purchase':
            $controller('PurchaseFormController', {
                $scope: $scope
            });
            break;
        case 'realm':
            realmSelectEnabled = false;
            tagSelectEnabled = false;
            $controller('RealmFormController', {
                $scope: $scope
            });
            break;
        case 'deployment':
            $controller('DeploymentFormController', {
                $scope: $scope
            });
            break;
        case 'timetrigger':
            $controller('TimeTriggerFormController', {
                $scope: $scope
            });
            break;
        case 'resultset':
            $controller('ResultSetFormController', {
                $scope: $scope
            });
            break;
        case 'checkin':
            $controller('CheckinFormController', {
                $scope: $scope
            });
            break;
        case 'onboard':
            $controller('OnboardFormController', {
                $scope: $scope
            });
            break;
        case 'team':
            $controller('TeamFormController', {
                $scope: $scope
            });
            break;
        case 'reaction':
            $controller('ReactionFormController', {
                $scope: $scope
            });
            break;
        case 'transaction':
            $controller('TransactionFormController', {
                $scope: $scope
            });
            break;
        case 'asset':
        case 'video':
        case 'audio':
        case 'image':
            $controller('AssetFormController', {
                $scope: $scope
            });
            break;
    }

    //////////////////////////////////////////////////

    $scope.realmSelectEnabled = realmSelectEnabled;
    $scope.tagSelectEnabled = tagSelectEnabled;

    //////////////////////////////////////////////////


    $scope.item.save = function() {
        console.log('SAVE')
        $scope.save();
     }


    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    // $scope.getExportedJSON = function() {
    //     //Create the json object
    //     var json = angular.toJson($scope.item);

    //     //JSON.stringify(json)
    //     var data = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(json));

    //     return data;
    // }

    //////////////////////////////////////////////////

    /**
    $scope.import = function(object) {
       ModalService.import
    }

    //////////////////////////////////////////////////

    $scope.importDataObject = function(object) {
        $scope.item = object;
    }

    //////////////////////////////////////////////////

    $scope.export = function() {

        //Create the json object
        var json = angular.toJson($scope.item);

        //JSON.stringify(json)
        var data = "text/json;charset=utf-8," + encodeURIComponent(json);


        //Download data
        window.open('data:' + data, '_blank');
        //angular.element('<a href="data:' + data + '" download="'+$scope.item._id+'.json"><i class="far fa-share-square-o"></i></a>');
        //.appendTo('.status-summary');


    }
    /**/


})