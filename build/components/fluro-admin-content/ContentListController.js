//////////////////////////////////////////////////////////////////////////




//Helper function to keep datefiltering code DRY down below
function matchDateRange(filteredItems, dateRange, key, filterFunction) {
    if (!dateRange) {
        return;
    }

    ////////////////////////////////

    var from = new Date(parseInt(dateRange.from)).getTime();
    var to = new Date(parseInt(dateRange.to)).getTime();

    ////////////////////////////////

    //Filter to match ranges
    return _.filter(filteredItems, function(item) {
        var val = _.get(item, key);

        if (!val) {
            return;
        }

        //If it's an array of numbers
        //then we need to include any of them
        var matches;

        //If it's an array of dates
        if (_.isArray(val)) {

            //Check if any of those dates match
            matches = _.some(val, function(v) {

                //Get the value as a date
                var datetime = filterFunction(v);
                //Get the timestamp
                var timestamp = datetime.getTime();
                //Check the from and to
                return matchesRange(timestamp, from, to);
            });
        } else {

            //Get the date from the item field
            var datetime = filterFunction(val);
            var timestamp = datetime.getTime();
            matches = matchesRange(timestamp, from, to);
        }

        return matches;
    });

}


//////////////////////////////

function matchesRange(input, from, to) {
    var matchMin = true;
    var matchMax = true;

    if (from) {
        matchMin = (input >= parseInt(from));
    }

    if (to) {
        matchMax = (input <= parseInt(to));
    }

    return matchMin && matchMax;
}

//////////////////////////////

function matchesDateRange(input, from, to) {
    var matchMin = true;
    var matchMax = true;

    if (from) {
        matchMin = (parseInt(input) >= parseInt(from));
    }

    if (to) {
        matchMax = (parseInt(input) <= parseInt(to));
    }

    return matchMin && matchMax;
}


//////////////////////////////////////////////////////////////////////////

app.service('ContentListService', function($state) {

    var service = {};

    /////////////////////////

    service.selectContacts = function(newSelection) {

        if ($state.current.name == 'contact.default') {
            service.transformSelection = newSelection;
        } else {
            $state.go('contact.default').then(function() {

                console.log('Transition complete!!!');
                service.transformSelection = newSelection;
            });
        }

    }

    /////////////////////////

    return service;
})



//////////////////////////////////////////////////////////////////////////

app.controller('ContentListController', function($scope, Batch, $sessionStorage, $timeout, ContentListService, SearchService, $stateParams, $filter, TypeService, FluroContent, ContentFieldService, extras, Notifications, CacheManager, FluroSocket, $rootScope, $controller, $state, $stateParams, $filter, $compile, Fluro, FluroAccess, ModalService, Selection, FluroStorage, Tools, items, definition, $q, definitions, type) {

    if (definition && definition.filters && definition.filters.length) {
        _.each(definition.filters, function(filter) {
            switch (filter.type) {
                case 'range':
                case 'daterange':
                case 'weekrange':
                case 'monthrange':
                case 'yearrange':
                    filter.rangedType = true;
                    break;

            }
        })
    }
    //////////////////////////////

    switch (type.path) {
        case 'process':
        case 'contactdetail':
            if (!definition) {
                $scope.landingStyle = 'definition';
            }
            break;
    }


    //////////////////////////////

    if (items.massive) {

        $scope.massiveSearch = {};
        $scope.massive = true;
        $scope.total = items.total;
        $scope.items = items.items;
    } else if (items.indexed) {

        // //console.log('ITEMS', items);
        var legend = items;
        $scope.legend = legend;
        $scope.items = hydrateIndexedItems(legend.items);
        $scope.total = legend.total;
    } else {
        //Add the items to the scope
        $scope.items = items;
        $scope.total = items.length;
    }


    ////////////////////////////////////////////
    /**/
    function hydrateLegendKey(item, lookupPath) {

        var source = item[lookupPath];

        if (!source) {
            // //console.log('No match for ', lookupPath)
            return;
        }

        if (_.isArray(source)) {

            item[lookupPath] = _.chain(source)
                .map(function(key) {
                    return legend[lookupPath][key].v;
                })
                .compact()
                .value();

        } else {
            //item.title = legend.title.source

            // //console.log('BEFORE', item, source, 'legend.' + lookupPath + '.' + source + '.v');

            var val = legend[lookupPath][source].v;

            // //console.log('Get path', source, 'legend.' + lookupPath + '.' + source + '.v', val);


            item[lookupPath] = val;
            //// //console.log('UPDATED KEY', item[key], key);
        }
    }

    ////////////////////////////////////////////

    function hydrateIndexedItems(items) {

        return _.chain(items)
            // .filter({hydrated:false})
            .map(function(item) {
                if (item.hydrated) {
                    return item;
                }

                hydrateLegendKey(item, 'title');
                hydrateLegendKey(item, 'status');
                hydrateLegendKey(item, 'realms');
                hydrateLegendKey(item, 'keywords');
                item._type = type.path;
                if (definition) {
                    item.definition = definition.definitionName;
                }
                item.hydrated = true;
                return item;
            })
            .value();
    }
    /**/

    //////////////////////////////
    //////////////////////////////
    //////////////////////////////
    //////////////////////////////
    //////////////////////////////
    //////////////////////////////
    //////////////////////////////
    //////////////////////////////





    $scope.searchingInheritable = $stateParams.searchInheritable;

    ///////////////////////////////////////////////

    $scope.containsInherited = _.some($scope.items, function(item) {

        if (!item.account) {
            return;
        }

        var accountID = item.account;
        if (accountID._id) {
            accountID = accountID._id;
        }

        return accountID != $rootScope.user.account._id;
    });

    ///////////////////////////////////////////////

    $scope.canEditDefinition = function() {
        if (!$scope.definition) {
            return;
        }
        return FluroAccess.canEditItem($scope.definition);
    }

    $scope.editDefinition = function(modal) {
        if ($scope.definition) {
            if (modal) {


                return ModalService.edit($scope.definition, function(res) {

                    //Reload the state if the definition changed
                    reloadState();
                })
            } else {
                $state.go('definition.edit', {
                    definitionName: definition.definition,
                    id: definition._id
                });


            }
        }
    }


    //////////////////////////////

    $scope.showDefinitionButton = function() {
        if (!definition) {
            return;
        }

        if (!$scope.canEditDefinition()) {
            return;
        }

        switch (type.path) {
            case 'contactdetail':
                return true;
                break;
        }
    }

    ///////////////////////////////////////////////

    $scope.definitions = _.sortBy(definitions, function(definition) {
        return definition.title;
    });
    $scope.type = type;
    $scope.definition = definition;

    $scope.extras = extras;


    $scope.templates = _.filter($scope.items, function(item) {
        return item.status == 'template';
    })

    ///////////////////////////////////////////////

    $scope.createDefinition = function() {


        var params = {
            template: {
                parentType: type.path
            }
        };

        ModalService.create('definition', params, successCallback, successCallback);

        function successCallback(definition) {

            console.log('SUCCESS', definition)

            $state.go(type.path + '.custom', {
                definition: definition.definitionName
            });
            // ui-sref="{{type.path}}.custom({definition:definition.definitionName})"
            // reloadState();
        }

    }

    ///////////////////////////////////////////////

    function reloadState() {

        $scope.reloading = true;
        console.log('ReloadState');
        $state.reload();
    }

    ///////////////////////////////////////////////

    //Check which fields to exclude from search
    var excludes = [
        // '_id', 
        'created',
        'updated',
        'updatedBy',
        'author',
        'managedAuthor',
        'owners',
        'managedOwners',
        'distinctFrom',
        'dob',
        'color',
        'bgColor',
        'filesize',
    ]

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    $scope.centerColumn = function(column) {
        switch (column.renderer) {
            case 'number':
            case 'currency':
            case 'age':
            case 'privacy':
            case 'paymentStatus':
            case 'paymentMode':
                return true;
                break;
        }
    }

    ///////////////////////////////////////////////

    $scope.isFinal = function(item) {

        if (item._type == 'mailout' && item.state == 'sent') {
            return true;
        }

        return false;
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    function iterate(obj, keepers) {

        for (field in obj) {

            if (obj.hasOwnProperty(field)) {
                switch (typeof obj[field]) {
                    case 'object':
                        iterate(obj[field], keepers);
                        break;
                    case 'function':
                        break;
                    default:

                        //get the value
                        var value = obj[field];

                        ////////////////////////////////////////////////////

                        //If its a time or date field add readable information
                        switch (field) {
                            case 'startDate':
                            case 'endDate':
                                var date = new Date(value);
                                var readableTime = date.format('g:ia');
                                var readableDate = date.format('l j F Y');
                                keepers.push(readableTime)
                                keepers.push(readableDate);
                                break;
                            default:
                                //Check if this value should be excluded
                                var excluded = _.includes(excludes, field);

                                if (!excluded) {
                                    keepers.push(value)
                                }
                                break;
                        }

                        ////////////////////////////////////////////////////

                        break;
                }
            }

        }
    }

    ///////////////////////////////////////////////






    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    $scope.isInherited = function(item) {

        if (!item.account) {
            return;
        }

        var userAccountID = $rootScope.user.account;
        if (userAccountID._id) {
            userAccountID = userAccountID._id;
        }


        var itemAccountID = item.account;
        if (itemAccountID._id) {
            itemAccountID = itemAccountID._id;
        }

        if (userAccountID && itemAccountID) {
            return userAccountID != itemAccountID;
        }
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    $scope.canCustomise = function(item) {
        var differentAccount = $scope.isInherited(item);
        // var canView = FluroAccess.canViewItem(item);
        var canCreate;



        if ($scope.definition) {
            canCreate = FluroAccess.can('create', definition.definitionName, definition.parentType);
        } else {
            canCreate = FluroAccess.can('create', $scope.type.path);
        }

        return (differentAccount && canCreate)
    }

    ///////////////////////////////////////////////

    $scope.createCustom = function(item) {


        /////////////////////////////////

        function successCallback(result) {
            reloadState();
            $scope.selection.deselectAll();
        }

        /////////////////////////////////

        function cancelCallback(err) {
            console.log('cancelCallback callback', err);
        }

        /////////////////////////////////


        ModalService.edit(item, successCallback, cancelCallback, true);
    }


    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////



    ///////////////////////////////////////////////

    $scope.totalPostCount = function(item) {
        if (!item.posts) {
            return '';
        }

        return _.sum(item.posts, function(post) {
            return post.count;
        })
    }


    $scope.realtime = {
        count: 0
    }

    $scope.refreshPage = function() {

        //CLEAR THE CACHE HERE
        if (definition) {
            CacheManager.clear(definition.definitionName);
        } else {
            CacheManager.clear(type.path);
        }

        console.log('Cache Cleared')
        reloadState();
    }

    //var socketEnabled = true;

    FluroSocket.on('content.edit', socketUpdate);
    FluroSocket.on('content.create', socketUpdate);
    FluroSocket.on('content.delete', socketUpdate);
    FluroSocket.on('content.restore', socketUpdate);


    function socketUpdate(data) {

        if (Batch.current.length) {
            return console.log('NO REFRESH WHEN BATCH IS IN PROCESS');
        }

        ////////////////////////////////////////

        if (!data.item) {
            return;
        }

        ////////////////////////////////////////

        if (data.item._type == type.path) {

            if (data.item.definition) {
                CacheManager.clear(data.item.definition);
            } else {
                CacheManager.clear(type.path);
            }

            if (data.user) {
                if ($rootScope.user._id != data.user._id) {
                    Notifications.warning('Content on this page was just updated by ' + data.user.name);
                }
            }
            //  else {
            //     Notifications.warning('Some items on this page have been updated by another user');
            // }
            // 
            switch (type.path) {
                case 'process':
                    //Ignore for processes 
                    break;
                default:
                    switch (data.key) {
                        //THIS SUCKS WITH BIG BATCHES SO TURN IT OFF WHEN THERES A BACKGROUND PROCESS
                        // case 'content.create':
                        //     if(type.path == 'process') {

                        //     }
                        // break;
                        // case 'content.edit':
                        // case 'content.delete':
                        // case 'content.restore':


                        //     if (type.path != 'realm') {


                        //         return reloadState();
                        //     }
                        //     break;
                        default:
                            $scope.realtime.count++;
                            break;
                    }
                    break;
            }
        }
    }
    //////////////////////////////////////////////////

    $scope.$on("$destroy", function() {
        FluroSocket.off('content.edit', socketUpdate);
        FluroSocket.off('content.create', socketUpdate);
        FluroSocket.off('content.delete', socketUpdate);
        FluroSocket.off('content.restore', socketUpdate);
    });

    //////////////////////////////


    //////////////////////////////

    $scope.updatedTooltip = function(item) {
        if (item.updatedBy) {
            return 'By ' + item.updatedBy + ' ' + $filter('timeago')(item.updated);
        } else {
            return $filter('timeago')(item.updated);
        }

        //return $sce.trustAsHtml('<span ng-if="item.updatedBy">By ' +item.updatedBy+ '</span> {{item.updated | timeago}}');
    }

    $scope.createdTooltip = function(item) {
        return $filter('timeago')(item.created);
    }

    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;


    $scope.clearFilters = function() {
        $scope.search.filters = {};
        $scope.search.ranges = {};
        $scope.search.dates = {};
        $scope.search.months = {};
        $scope.search.weeks = {};
        $scope.search.years = {};
    }

    //////////////////////////////////////////////////////////////

    $scope.filtersActive = function() {

        var filterKeys = _.keys($scope.search.filters);
        var rangeKeys = _.keys($scope.search.ranges);
        var dateKeys = _.keys($scope.search.dates);
        var weekKeys = _.keys($scope.search.weeks);
        var monthKeys = _.keys($scope.search.months);
        var yearKeys = _.keys($scope.search.years);
        return filterKeys.concat(rangeKeys, dateKeys, weekKeys, monthKeys, yearKeys).length;
    }

    //////////////////////////////////////////////////////////////

    switch (type.path) {
        // case 'process':
        case 'event':
        case 'account':
            $scope.template = 'fluro-admin-content/types/' + type.path + '/list.html';
            break;
        default:
            $scope.template = 'fluro-admin-content/types/list.html';
            break;
            /**
        case 'process':
            $scope.template = 'fluro-admin-content/types/' + type.path + '/list.html';
            break;
        case 'account':
            $scope.template = 'fluro-admin-content/types/' + type.path + '/list.html';
            break;
            /**/

    }
    //////////////////////////////

    //Setup Local storage
    var local;
    var session;

    if (definition) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;
        $scope.definedType = definition.definitionName;

        local = FluroStorage.localStorage(definition.definitionName)
        session = FluroStorage.sessionStorage(definition.definitionName)
    } else {
        local = FluroStorage.localStorage(type.path)
        session = FluroStorage.sessionStorage(type.path)

        $scope.definedType = type.path;
    }

    //////////////////////////////

    //Setup Search    
    if (!session.search) {
        $scope.search =
            session.search = {
                filters: {
                    status: ['active'],
                },
                reverse: false,
            }
    } else {
        $scope.search = session.search;
    }

    ////////////////////////////////////////////

    //Force override the search terms
    if (SearchService.searchIn && SearchService.searchIn.length) {

        console.log('SEARCH IN GOES HERE!!!');

        if ($scope.massive) {
            $scope.massiveSearch.terms = SearchService.searchIn;
        } else {
            $scope.search.terms = SearchService.searchIn;
        }


        delete SearchService.searchIn;
    }


    //////////////////////////////

    //////////////////////////////

    //Setup Search    
    if (!local.settings) {
        $scope.settings =
            local.settings = {}
    } else {
        $scope.settings = local.settings;
    }

    //////////////////////////////

    switch ($scope.type.path) {
        case 'asset':
        case 'image':
        case 'video':
        case 'audio':
            $scope.uploadType = true;

            $scope.uploadName = $scope.type.path;
            if (definition) {
                $scope.uploadName = definition.definitionName;
            }
            break;

    }
    //////////////////////////////


    $scope.containsDefaultViewMode = _.includes(type.viewModes, 'default');
    if ($scope.containsDefaultViewMode) {
        if (!$scope.settings.viewMode || $scope.settings.viewMode == '') {
            $scope.settings.viewMode = type.viewModes[0];
        }
    }

    $scope.currentViewMode = function(viewMode) {
        if (viewMode == 'default') {
            return (!$scope.settings.viewMode || $scope.settings.viewMode == '' || $scope.settings.viewMode == viewMode);
        } else {
            return $scope.settings.viewMode == viewMode;
        }
    }

    //////////////////////////////

    //////////////////////////////

    $scope.setOrder = function(string, orderRenderer) {
        $scope.search.order = string;
        $scope.search.orderRenderer = orderRenderer;
        // $scope.updateFilters();
        $scope.updateSort();
    }

    $scope.setReverse = function(bol) {
        $scope.search.reverse = bol;
        // $scope.updateFilters();
        $scope.updateSort();
    }




    //////////////////////////////

    //Setup Selection    
    if (!session.selection) {
        session.selection = []
    }

    //////////////////////////////

    //Setup Pager    
    if (!session.pager) {
        $scope.pager =
            session.pager = {
                maxSize: 8,
            }
    } else {
        $scope.pager = session.pager;
    }

    $scope.pager.limit = 30;

    //////////////////////////////
    //////////////////////////////



    $scope.$watch('search', function() {
        $scope.updateFilters()
    }, true);

    //////////////////////////////

    $scope.$watch('items', function() {


        //Map each item to a search string
        _.each($scope.items, function(item) {

            //Filter search terms
            switch (type.path) {
                case 'account':
                    item.searchString = item.title + ' ' + item._id;
                    break;
                case 'persona':
                    item.searchString = [
                        item.title,
                        item.firstName,
                        item.lastName,
                        item._id,
                        item.collectionEmail,
                        item.username,
                        _.map(item.realms, 'title').join(' '),
                        _.map(item.tags, 'title').join(' '),

                    ].join(' ');

                    break;
                case 'contactdetail':
                    item.searchString = [
                        item.title,
                        item.contact.firstName,
                        item.contact.lastName,
                        item._id,
                        item.gender,
                    ].join(' ');
                    break;
                case 'contact':
                    item.searchString = [
                        item.title,
                        item.firstName,
                        item.lastName,
                        item._id,
                        item.gender,
                        _.get(item, 'family.title'),
                        // _.map(item.realms, 'title').join(' '),
                        // _.map(item.teams, 'title').join(' '),
                        _.map(item.emails).join(' '),
                        // _.map(item.tags, 'title').join(' '),
                        // _.map(item.keywords).join(' '),
                    ].join(' ');

                    break;
                case 'family':
                    item.searchString = [
                        item.title,
                        item.firstLine,
                        _.get(item, 'address.addressLine1'),
                        _.get(item, 'address.addressLine2'),
                        _.get(item, 'address.state'),
                        _.get(item, 'address.suburb'),
                        _.get(item, 'address.country'),
                        // _.map(item.realms, 'title').join(' '),
                        // _.map(item.keywords).join(' '),
                        // _.map(item.tags, 'title').join(' '),
                    ].join(' ');

                    break;
                default:
                    var keepers = [];
                    iterate(item, keepers)
                    item.searchString = keepers.join(' ').toLowerCase();
                    break;
            }
        });


        // updateSearch();
        $scope.updateFilters();
    });

    //////////////////////////////
    //////////////////////////////

    if ($scope.massive) {
        $scope.$watch('massiveSearch.terms', function(keywords) {



            var definedName = type.path;
            if ($scope.definition) {
                definedName = $scope.definition.definitionName;
            }

            if (!keywords || !keywords.length) {
                return $scope.items = items.items;
            }


            $scope.search = {};
            //.terms = '';

            //console.log('Clear terms')

            var queryConfig = {
                limit: 5000,
                types: definedName,
                // populateFields:'all',
                // populateAll:true,
                // populateFields: 'realms event tags contact contacts',
            }


            ///////////////////////////////////////////////////////////

            ContentFieldService.getFields(queryConfig, type, definition);

            ///////////////////////////////////////////////////////////

            var request = FluroContent.resource('search/' + keywords, false, true).query(queryConfig)
                .$promise;

            request.then(function(results) {
                //console.log('MASSIVE SEARCH', keywords, results.length);
                $scope.items = results;
            }, function(err) {
                //console.log('ERROR', err);
            })

        })
    }

    ///////////////////////////////////////////////

    $scope.canSelect = function(item) {
        return true;
    }

    ///////////////////////////////////////////////



    $scope.selection = new Selection(session.selection, $scope.items, $scope.canSelect);



    ///////////////////////////////////////////////

    $scope.selectPage = function() {
        $scope.selection.selectMultiple($scope.pageItems);
    }



    ///////////////////////////////////////////////

    $scope.deselectFiltered = function() {
        $scope.selection.deselectMultiple($scope.filteredItems);
    }

    ///////////////////////////////////////////////

    $scope.togglePage = function() {
        if ($scope.pageIsSelected()) {
            $scope.deselectPage();
        } else {
            $scope.selectPage();
        }
    }

    $scope.deselectPage = function() {
        $scope.selection.deselectMultiple($scope.pageItems);
    }

    $scope.pageIsSelected = function() {
        return $scope.selection.containsAll($scope.pageItems);
    }


    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    $scope.viewInModal = function(item) {
        ModalService.view(item);
    }

    ///////////////////////////////////////////////

    $scope.editInModal = function(item) {
        ModalService.edit(item, function(result) {
            reloadState();
        }, function(result) {});
    }

    ///////////////////////////////////////////////

    $scope.createInModal = function() {
        ModalService.create($scope.type, function(result) {
            reloadState();
        }, function(result) {
            //////console.log('Failed to create in modal', result)
        });
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    $scope.getFullViewSref = function(item) {
        var typePath = $scope.type.path;
        var defName = item.definition;

        var string = typePath + ".view({id: '" + item._id;

        if (defName && defName.length) {
            string += "', definitionName:'" + defName + "'})";
        } else {
            string += "'})";
        }

        return string;
    }

    $scope.getFullEditSref = function(item) {





        var typePath = $scope.type.path;
        var defName = item.definition;

        var string = typePath + ".edit({id: '" + item._id;


        //Check if the user can actually edit this thing
        var canEdit = FluroAccess.canEditItem(item);
        if (!canEdit) {
            return $scope.getFullViewSref(item);
        }


        if (defName && defName.length) {
            string += "', definitionName:'" + defName + "'})";
        } else {
            string += "'})";
        }

        return string;
    }


    $scope.getItemDefinition = function(item) {
        return item.definition;
    }

    $scope.viewInFull = function(item) {
        var typePath = $scope.type.path;
        $state.go(typePath + '.view', {
            id: item._id,
            definitionName: item.definition
        });
    }

    ///////////////////////////////////////////////

    $scope.editInFull = function(item) {
        var typePath = $scope.type.path;
        $state.go(typePath + '.edit', {
            id: item._id,
            definitionName: item.definition
        });
    }

    ///////////////////////////////////////////////

    $scope.deleteInFull = function(item) {
        var typePath = $scope.type.path;
        $state.go(typePath + '.delete', {
            id: item._id,
            definitionName: item.definition
        });
    }

    ///////////////////////////////////////////////

    $scope.createFromTemplate = function(template) {

        ModalService.edit(template, function(result) {
            //////console.log('Copied in modal', result)
            reloadState();
        }, function(result) {
            //////console.log('Failed to copy in modal', result)
        }, false, template, $scope);


    }

    ///////////////////////////////////////////////

    $scope.copyItem = function(item) {

        ModalService.edit(item, function(result) {
            //////console.log('Copied in modal', result)
            reloadState();
        }, function(result) {
            //////console.log('Failed to copy in modal', result)
        }, true);


    }

    ///////////////////////////////////////////////

    $scope.createNew = function() {

        console.log('Create new')
        if (definition) {

            ModalService.createType(type, definition.definitionName);
        } else {
            ModalService.createType(type);
        }

    }


    $scope.createNewFamily = function() {
        ModalService.createFamily();
    }

    ///////////////////////////////////////////////

    $scope.canCreate = function() {

        var isAdmin = FluroAccess.isFluroAdmin();

        if (!definition) {
            switch (type.path) {
                case 'contactdetail':
                case 'process':
                    // case 'post':
                case 'product':
                case 'interaction':
                    // case 'mailout':



                    return false;
                    break;
                case 'realm':
                    if (isAdmin) {
                        return true;
                    } else {
                        return false;
                    }
                    break;
            }
        }

        var allowed = FluroAccess.can('create', type.path);
        if (definition) {
            allowed = FluroAccess.can('create', definition.definitionName, definition.parentType);
        }

        return allowed;


    }


    ///////////////////////////////////////////////

    $scope.canCopy = function(item) {
        var typeName = $scope.type.path;
        var parentType;

        if (item.definition) {
            typeName = item.definition;
            parentType = item._type;
        }

        return FluroAccess.can('create', typeName, parentType);
    }

    ///////////////////////////////////////////////


    $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item, (type.path == 'user'));
    }

    ///////////////////////////////////////////////

    $scope.canDelete = function(item) {
        return FluroAccess.canDeleteItem(item, (type.path == 'user'));
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////



    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////



    var debounceTimer;

    $scope.updateFilters = function() {

        //SKIP THE DEBOUNCER
        return searchChanged();

        if (debounceTimer) {
            $timeout.cancel(debounceTimer);
        }

        debounceTimer = $timeout(function() {
            console.log('Search update')
            // //console.log('update keywords', searchData)
            // $scope.updateFilters();
            searchChanged();
        }, ContentFieldService.filterDebounce);

    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    //Filter the items by all of our facets
    function searchChanged() {
        // console.log('Search changed');

        // if($scope.legend) {
        //     $scope.items =[];
        // }

        //console.log('UPDATE FILTERS')
        if ($scope.items) {
            //Start with all the results
            var filteredItems = $scope.items;

            if ($scope.search.terms && $scope.search.terms.length) {
                //Old Search functionality
                filteredItems = $filter('filter')(filteredItems, {
                    searchString: $scope.search.terms
                });
            }

            //////////////////////////////////////////////////////


            if ($scope.search.filters) {
                _.forOwn($scope.search.filters, function(value, key) {

                    // console.log('FILTER', value, key);

                    if (_.isArray(value)) {
                        _.forOwn(value, function(value) {
                            filteredItems = filterListReferences(filteredItems, key, value);
                        })
                    } else {
                        filteredItems = filterListReferences(filteredItems, key, value);
                    }
                });
            }


            ////////////////////////////////////////////


            if ($scope.search.ranges && _.keys($scope.search.ranges).length) {
                // console.log('MATCH RANGES', $scope.search.ranges);

                _.forOwn($scope.search.ranges, function(range, key) {

                    if (!range) {
                        console.log('Nope', key)
                        return;
                    }
                    var from = range.from;
                    var to = range.to;

                    //Filter to match ranges

                    console.log('Get filtered items')
                    filteredItems = _.filter(filteredItems, function(item) {
                        var val = _.get(item, key);

                        //If it's an array of numbers
                        //then we need to include any of them
                        var matches;

                        // console.log('CHECK RANGE', val);

                        if (_.isArray(val)) {
                            matches = _.some(val, function(v) {
                                return matchesRange(v, from, to);
                            });
                        } else {
                            matches = matchesRange(val, from, to);
                        }



                        return matches;


                    });

                });
            }


            ////////////////////////////////////////////

            if ($scope.search.dates && _.keys($scope.search.dates).length) {
                _.forOwn($scope.search.dates, function(dateRange, key) {
                    filteredItems = matchDateRange(filteredItems, dateRange, key, function(d) {
                        var datetime = new Date(d);
                        datetime.setHours(0, 0, 0, 0);
                        return datetime;
                    });
                });
            }


            ////////////////////////////////////////////

            if ($scope.search.weeks && _.keys($scope.search.weeks).length) {
                _.forOwn($scope.search.weeks, function(dateRange, key) {
                    filteredItems = matchDateRange(filteredItems, dateRange, key, function(d) {
                        return startOfWeek(new Date(d))
                    });
                });
            }

            ////////////////////////////////////////////

            if ($scope.search.months && _.keys($scope.search.months).length) {
                _.forOwn($scope.search.months, function(dateRange, key) {
                    filteredItems = matchDateRange(filteredItems, dateRange, key, function(d) {
                        return startOfMonth(new Date(d))
                    });
                });
            }

            ////////////////////////////////////////////

            if ($scope.search.years && _.keys($scope.search.years).length) {
                _.forOwn($scope.search.years, function(dateRange, key) {
                    filteredItems = matchDateRange(filteredItems, dateRange, key, function(d) {
                        return startOfYear(new Date(d))
                    });
                });
            }



            ////////////////////////////////////////////

            //console.log('lengthy', filteredItems.length)
            //Update the items with our search
            $scope.filteredItems = filteredItems;



            //Update the current page
            $scope.updateSort();
        } else {
            //console.log('No items')
        }
    }

    //////////////////////////////////////////////////////

    $scope.updateSort = function() {

        var filteredItems = $scope.filteredItems;

        //////////////////////////////////////////////////////

        //Order the items
        filteredItems = _.sortBy(filteredItems, function(item) {
            var val = _.get(item, $scope.search.order);

            ///////////////////////////////

            if (val) {

                if (_.isArray(val) && val.length) {
                    val = val[0];
                }

                if (val.title) {
                    val = val.title;
                } else {
                    if (val.name) {
                        val = val.name;
                    }
                }
            }

            ///////////////////////////////

            switch ($scope.search.orderRenderer) {
                case 'boolean':
                    if (!val) {
                        return 0
                    } else {
                        return 1;
                    }
                    break;
                case 'number':
                    if (!val) {
                        return -999999999999999;
                    }
                    return parseInt(val);
                    // return 9999999999999999;



                    break;
                case 'date':

                    var date = new Date(val);
                    return date.getTime();
                    break;
                default:
                    return val || '';
                    break;
            }

        });


        if ($scope.search.reverse) {
            filteredItems.reverse();
        }

        //////////////////////////////////////////////////////

        $scope.sorted = filteredItems;
        $scope.updatePage();
    }

    ///////////////////////////////////////////////

    //Update the current page
    $scope.updatePage = function() {

        if (!$scope.sorted) {
            $scope.sorted = $scope.filteredItems;
        }

        if ($scope.sorted.length < ($scope.pager.limit * ($scope.pager.currentPage - 1))) {
            $scope.pager.currentPage = 1;
        }

        // var pageItems = $filter('startFrom')($scope.sorted, ($scope.pager.currentPage - 1) * $scope.pager.limit);
        // pageItems = $filter('limitTo')(pageItems, $scope.pager.limit);

        var startIndex = ($scope.pager.currentPage - 1) * $scope.pager.limit;
        if (!startIndex) {
            startIndex = 0;
        }
        var endIndex = startIndex + $scope.pager.limit;


        var pageItems = $scope.sorted.slice(startIndex, endIndex);



        $scope.pageItems = pageItems;
    }




    ////////////////////////////////////////////

    switch (type.path) {
        case 'query':
            $controller('QueryListController', {
                $scope: $scope
            });
            break;
        case 'event':
            $controller('EventListController', {
                $scope: $scope
            });
            break;
        case 'contact':
            $controller('ContactListController', {
                $scope: $scope
            });
            break;
        case 'user':
            $controller('UserListController', {
                $scope: $scope
            });
            break;
        case 'persona':
            $controller('PersonaListController', {
                $scope: $scope
            });
            break;
        case 'account':
            $controller('AccountListController', {
                $scope: $scope
            });
            break;
    }


    $scope.updateFilters();

    ///////////////////////////////////////////////


    ///////////////////////////////////////////////

    $scope.toggleArchived = function() {
        console.log('Toggle Archived');

        var extended = angular.copy($stateParams);

        if (!extended.searchArchived || extended.searchArchived == 'false') {
            extended.searchArchived = true;
        } else {
            extended.searchArchived = null;
        }

        ////////////////////////////////////////////

        CacheManager.clear($scope.type.path);

        if ($scope.definition) {
            CacheManager.clear($scope.definition.definitionName);
        }

        ////////////////////////////////////////////

        console.log('Switch From', $stateParams, extended);
        return $state.go($state.current, extended, {
            reload: true
        });
    }





    ///////////////////////////////////////////////

    function getTransformedSelection(newSelection) {

        if (!newSelection) {
            // console.log('No selection transform')
            return;
        }

        // console.log('TRANSFORM THE SELECTION!');
        ///////////////////////////////

        //IF there is a transform selection
        if (_.keys(newSelection).length) {

            //Find all the matches
            var matches = _.filter($scope.items, function(item) {
                var itemID = item._id;

                if (newSelection[itemID]) {
                    // console.log('Found a match!')
                    return true;
                }
                // return newSelection[itemID];

            })

            // console.log('matches', $scope.items.length, matches.length, newSelection)


            //Select all the contacts that match our transformed selection
            $scope.selection.deselectAll()
            $scope.selection.selectMultiple(matches)
            $scope.selection.items = matches;

            Notifications.status('Your selection has been changed');

            ///////////////////////////////

            //Clear it as we don't need it anymore
            ContentListService.transformSelection = null;
        }
    }

    ///////////////////////////////////////

    // $scope.stopWatchingSelection = function() {
    //     if($scope.stopSelectionWatcher) {
    //         $scope.stopSelectionWatcher();
    //     }
    // }

    // ///////////////////////////////////////

    // $scope.startWatchingSelection = function() {

    //         $scope.stopWatchingSelection();


    //Use the transformed selection
    $scope.$watch(function() {
        return ContentListService.transformSelection;
    }, getTransformedSelection)
    // }




    ///////////////////////////////////////



    /*
    ///////////////////////////////////////

    $scope.$watch('search.terms', function(keywords) {
        return $http.get(Fluro.apiURL + '/search', {
            ignoreLoadingBar: true,
            params: {
                keys: keywords,
                //type: $scope.restrictType,
                limit: 100,
            }
        }).then(function(response) {
           $scope.items = response.data;
        });
    })
    */

    ///////////////////////////////////////


});