
app.directive('contentView', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            viewModel: '=ngModel',
        },
        templateUrl: 'fluro-admin-content/types/view.html',
        controller:function($scope, $controller, $q, TypeService) {


            $scope.item = $scope.viewModel;
            $scope.extras = {};
            $scope.type = TypeService.getTypeFromPath($scope.item._type);

            if($scope.item.fullDefinition) {
                 $scope.definition = $scope.item.fullDefinition;
            } else {
                 if($scope.item.definition) {
                $scope.definition = TypeService.getTypeFromPath($scope.item._type);
            }
            }
           

            // var extras = $q.defer();
            // extras.resolve({});
            $scope.disableHeaderAndFooter = true;
            

            $controller('ContentViewController', {
                $scope: $scope,
                item:$scope.item,
                extras:$scope.extras,
                type:$scope.type,
                definition:$scope.definition,
            });
        },
    };
});




app.controller('ContentViewController', function($scope, extras, $timeout, $rootScope, $stateParams, $controller, FluroSocket, Notifications, ModalService, $state, FluroAccess, type, definition, item) {

    $scope.item = item;
    $scope.type = type;
    $scope.definition = definition;
    $scope.extras = extras;


    $scope.settings = {};

    //Template
    $scope.template = 'fluro-admin-content/types/' + type.path + '/view.html';

    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;

    //Cancel path

    if (definition) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;
        $scope.definedType = definition.definitionName;
    } else {
        $scope.definedType = type.path;
    }


    ///////////////////////////////////////////////

    $scope.canEdit = function(item) {
       var allowed = FluroAccess.canEditItem(item);
       
       if(item._type == 'mailout') {
           switch(item.state) {
               case 'scheduled':
               case 'sent':
                   return false;
               break;
           }
           
        } 
            
         return FluroAccess.canEditItem(item);
        
    }


    //////////////////////////////

    $scope.getTotalPostCount = function() {
        if ($scope.item && $scope.item._id) {
            return _.sum($scope.item.posts, function(postType) {
                return postType.count;
            });
        }
    }


    var socketEnabled = true;
    /**/
    FluroSocket.on('content.edit', socketUpdate);


    //////////////////////////////////////////////////

    $scope.$on("$destroy", function() {
        // $(document).off('keydown', keyDown);

        FluroSocket.off('content.edit', socketUpdate);
    });


    function socketUpdate(socketEvent) {

        // console.log('Socket updated', socketEvent);

        if (socketEnabled) {

            //////////////////////////////////////////////

            var socketItemID = socketEvent.item;

            if (!socketItemID) {
                return;
            }

            if (socketItemID._id) {
                socketItemID = socketItemID._id;
            }

            //////////////////////////////////////////////

            //Same id content
            var sameId = (socketItemID == $scope.item._id);

            //////////////////////////////////////////////

            var newVersion = (socketEvent.data.updated != item.updated);

            //Make sure we only update if we weren't the originating socket
            var differentSocket = (socketEvent.ignoreSocket != FluroSocket.getSocketID());

            //////////////////////////////////////////////

            if (sameId && newVersion && differentSocket) {

                $timeout(function() {
                    //Assign the new content
                    socketEnabled = false;

                    //Patch the differences
                    jsondiffpatch.patch($scope.item, socketEvent.data.diff);

                    //Renable the socket
                    socketEnabled = true;

                    // console.log($rootScope.user._id, data.user._id)
                    if ($rootScope.user._id != socketEvent.user._id) {
                        Notifications.warning('This content was just updated by ' + socketEvent.user.name);
                    } else {
                        Notifications.status('You updated this in another window');
                    }
                })

            }
        }
    }

    /**function(data) {

        if (socketEnabled) {
            if (data.item._id == item._id) {
                Notifications.warning('Changes to this ' + $scope.titleSingular + ' were just made by ' + data.user.name);
                //$state.reload();
                if (angular.equals(data.item, item)) {
                    console.log('No change');
                } else {

                    socketEnabled = false;
                    //_.assign($scope.item, data.item);
                    $scope.item = data.item;
                    socketEnabled = true;

                    Notifications.warning('This content was just updated by ' + data.user.name);
                }
            }
        }
    });
    /**/



    //ui-sref="{{type.path}}.edit({id:item._id})"

    //////////////////////////////////////////////////

    $scope.editItem = function() {

        if ($scope.$dismiss) {
            ModalService.edit(item);
            $scope.$dismiss();
            /*
            $state.go($scope.item._type + '.edit', {
                id: $scope.item._id,
                definitionName: $scope.definedType,
            });

       
           
            */
        } else {
            $state.go($scope.item._type + '.edit', {
                id: $scope.item._id,
                definitionName: $scope.definedType,
            });

        }

    }

    //////////////////////////////////////////////////

    $scope.cancel = function() {

        if ($scope.$dismiss) {
            return $scope.$dismiss();
        }

        if ($stateParams.backOnClose) {
            window.history.back();
        } else {
            if ($scope.item._id == $rootScope.user._id) {
                return $state.go('home')
            }

            //Go to the normal path
            if (definition && definition.definitionName && type.path != 'contact') {
                $state.go(type.path + '.custom', {
                    definition: definition.definitionName
                });
            } else {
                $state.go(type.path)
            }

            
            // //Go to the normal path
            // if (definition && definition.definitionName) {
            //     $state.go(type.path + '.custom', {
            //         definition: definition.definitionName
            //     });
            // } else {
            //     $state.go(type.path)
            // }
        }
    }


    switch ($scope.item._type) {
        case 'contact':
            $controller('ContactViewController', {
                $scope: $scope
            });
            break;
        case 'asset':
            $controller('AssetViewController', {
                $scope: $scope
            });
            break;
        case 'process':
            $controller('ProcessViewController', {
                $scope: $scope
            });
            break;
        case 'resultset':
            $controller('ResultSetViewController', {
                $scope: $scope
            });
            break;
        case 'interaction':
            $controller('InteractionViewController', {
                $scope: $scope
            });
            break;
        case 'post':
            $controller('PostViewController', {
                $scope: $scope
            });
            break;
        case 'ticket':
            $controller('TicketViewController', {
                $scope: $scope
            });
            break;

            
        case 'team':
            $controller('TeamViewController', {
                $scope: $scope
            });
            break;
        case 'code':
            $controller('CodeViewController', {
                $scope: $scope
            });
            break;
        case 'query':

        console.log('INIT QUERY')
            $controller('QueryViewController', {
                $scope: $scope
            });
            break;
        case 'family':
            $controller('FamilyFormController', {
                $scope: $scope
            });
            break;
        case 'definition':
            $controller('DefinitionViewController', {
                $scope: $scope
            });
            break;
        case 'application':
            $controller('ApplicationViewController', {
                $scope: $scope
            });
            break;
        case 'mailout':
            $controller('MailoutViewController', {
                $scope: $scope
            });
            break;
            // case 'process':
            //     $controller('ProcessViewController', {
            //         $scope: $scope
            //     });
            //     break;

    }





})