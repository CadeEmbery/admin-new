app.controller('ContentDeleteController', function($scope, deleteCheck, $rootScope, type, definition, $state, FluroContent, Notifications, CacheManager, item) {

    $scope.item = item;
    $scope.type = type;
    $scope.definition = definition;
    $scope.check = deleteCheck;

    var noCheckContent = _.all(deleteCheck, function(entry) {
        return !entry.count;
    })

    if(noCheckContent) {
        $scope.check = null;
    }

    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;


    $scope.cancelPath = type.path;

    //////////////////////////////////////////////////

    if (definition) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;
        $scope.cancelPath = type.path + ".custom({definition:'" + definition.definitionName + "'})";
    }

    //////////////////////////////////////////////////

    $scope.cancel = function() {

        if ($scope.$dismiss) {
            return $scope.$dismiss();
        }

        if ($scope.item._id == $rootScope.user._id) {
            return $state.go('home')
        }

        //Go to the normal path
        if (definition && definition.definitionName) {
            $state.go(type.path + '.custom', {
                definition: definition.definitionName
            });
        } else {
            $state.go(type.path)
        }
    }

    ///////////////////////////////////////////////////

    $scope.delete = function() {

        var definedName = type.path;
        if(definition && definition.definitionName) {
            definedName = definition.definitionName;
        }

		
		if($scope.item._id) {
			//Update existing article
			FluroContent.resource(definedName).delete({
                id: $scope.item._id,
            }, deleteSuccess, deleteFailed);
		}
	}

	//////////////////////////////////////////////////

	function deleteSuccess(data) {
        var title = data.title;
        if(data.name) {
            title = data.name;
        }

        Notifications.status(title + ' was deleted successfully')

        //If in a modal
        if ($scope.$close) {
            return $scope.$close(data);
        }

        //////////////////////////////////
		
        if(data.definition && data.definition != data._type) {
            CacheManager.clear(data.definition);
            $state.go(type.path + '.custom', {definition:data.definition});
        } else {
            CacheManager.clear(type.path);
            $state.go(type.path);
        }
	}

	function deleteFailed(data) {

        var title = $scope.item.title;
        if($scope.item.name) {
            title = $scope.item.name;
        }

		 Notifications.error('Failed to delete ' + title)
	}



})