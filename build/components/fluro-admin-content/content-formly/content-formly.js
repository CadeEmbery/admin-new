/////////////////////////////////////////////////////////////////

//WE NEED TO UPDATE THIS TO BE NICER (admin-content-select that is)
app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'reference-select',
        templateUrl: 'fluro-admin-content/content-formly/reference-select.html',
        controller: function($scope) {
            //Get definition
            var definition = $scope.to.definition;

            if (!definition.params) {
                definition.params = {}
            }

            //Create a config object
            $scope.config = {
                canCreate: true,
                minimum: definition.minimum,
                maximum: definition.maximum,
                type: definition.params.restrictType,
                searchInheritable: definition.params.searchInheritable
            };
        },
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });


    formlyConfig.setType({
        name: 'timezone-select',
        templateUrl: 'fluro-admin-content/content-formly/timezone-select.html',
        controller: function($scope) {},
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });


    formlyConfig.setType({
        name: 'simple-reference-select',
        templateUrl: 'fluro-admin-content/content-formly/simple-reference-select.html',
        controller: function($scope) {

            var key = $scope.options.key;

            if ($scope.model[key] && $scope.model[key]._id) {
                $scope.model[key] = $scope.model[key]._id;
            }
        },
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    // //console.log('SET TYPE');

    formlyConfig.setType({
        name: 'date-select',
        templateUrl: 'fluro-admin-content/content-formly/date-select.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        controller: function($scope) {


            // //console.log('GET MODEL SCOPE DATe', $scope);

            //     return {
            //     restrict : 'A',
            //     scope : {
            //         ngModel : '='
            //     },
            //     link: function (scope) {
            //         if (scope.ngModel) scope.ngModel = new Date(scope.ngModel);
            //     }
            // }
        }
    });

    formlyConfig.setType({
        name: 'dynamic-date-select',
        templateUrl: 'fluro-admin-content/content-formly/dynamic-date-select.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        controller: function($scope) {

            $scope.setOption = function(str) {
                switch (str) {
                    case 'now':
                        $scope.model[$scope.options.key] = 'DATE_NOW';
                        break;
                    case 'specific':
                        if (!$scope.model[$scope.options.key] || $scope.model[$scope.options.key] == 'DATE_NOW') {
                            $scope.model[$scope.options.key] = new Date();
                        }

                        break;
                    case 'none':
                        $scope.model[$scope.options.key] = undefined;
                        break;
                }
            }

            $scope.optionSet = function(str) {

                switch (str) {

                    case 'now':
                        if ($scope.model[$scope.options.key] == 'DATE_NOW') {
                            return true;
                        }
                        break;
                    case 'specific':
                        if ($scope.model[$scope.options.key] && $scope.model[$scope.options.key] != 'DATE_NOW') {
                            return true;
                        }
                        break;
                    case 'none':
                        if (!$scope.model[$scope.options.key]) {
                            return true;
                        }
                        break;
                }

            }
            // //console.log('GET MODEL SCOPE DATe', $scope);

            //     return {
            //     restrict : 'A',
            //     scope : {
            //         ngModel : '='
            //     },
            //     link: function (scope) {
            //         if (scope.ngModel) scope.ngModel = new Date(scope.ngModel);
            //     }
            // }
        }
    });







    formlyConfig.setType({
        name: 'object-editor',
        templateUrl: 'fluro-admin-content/content-formly/object-editor.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });


    formlyConfig.setType({
        name: 'custom',
        templateUrl: 'fluro-admin-content/content-formly/unknown.html',
        wrapper: ['bootstrapHasError'],
    });

    formlyConfig.setType({
        name: 'terms',
        templateUrl: 'fluro-admin-content/content-formly/terms.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfig.setType({
        name: 'multi-input',
        templateUrl: 'fluro-admin-content/content-formly/multi-input.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfig.setType({
        name: 'code',
        templateUrl: 'fluro-admin-content/content-formly/code.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        controller: function($scope) {

            var aceEditor;

            $scope.aceLoaded = function(_editor) {
                // Options
                //_editor.setReadOnly(true);
                aceEditor = _editor;

                var syntaxName;

                if ($scope.to.definition.params && $scope.to.definition.params.syntax) {
                    syntaxName = syntaxName = $scope.to.definition.params.syntax;
                }

                if (aceEditor && syntaxName) {
                    aceEditor.getSession().setMode("ace/mode/" + syntaxName);
                }
            }
        }
    });

    formlyConfig.setType({
        name: 'wysiwyg',
        templateUrl: 'fluro-admin-content/content-formly/wysiwyg.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        controller: function($scope) {

            if (guid) {
                $scope.uniqueID = guid();
            } else {
                $scope.uniqueID = $scope.options.key;
            }

        }
    });


    formlyConfig.setType({
        name: 'hiddenvalue',

        // 
        // templateUrl: 'fluro-admin-content/content-formly/value.html',
        template: '<div class="fluro-interaction-value" style="display:none;><pre>{{model[options.key] | json}}</pre></div>',
        // wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });


    /**
// set templates here
    formlyConfig.setType({
        name: 'contentnested',
        templateUrl: 'fluro-interaction-form/nested.html',
        controller: function($scope) {

            $scope.$watch('model[options.key]', function(keyModel) {
                if (!keyModel) {
                    console.log('Create it!')
                    $scope.model[$scope.options.key] = [];
                }

            })

            ////////////////////////////////////

            //Definition
            var def = $scope.to.definition;

            var minimum = def.minimum;
            var maximum = def.maximum;
            var askCount = def.askCount;

            if (!minimum) {
                minimum = 0;
            }

            if (!maximum) {
                maximum = 0;
            }

            if (!askCount) {
                askCount = 0;
            }

            //////////////////////////////////////

            if (minimum && askCount < minimum) {
                askCount = minimum;
            }

            if (maximum && askCount > maximum) {
                askCount = maximum;
            }

            //////////////////////////////////////

            if (maximum == 1 && minimum == 1 && $scope.options.key) {
                console.log('Only 1!!!', $scope.options)
            } else {
                if (askCount && !$scope.model[$scope.options.key].length) {
                    _.times(askCount, function() {
                        $scope.model[$scope.options.key].push({});
                    });
                }
            }

            ////////////////////////////////////

            $scope.canRemove = function() {
                if (minimum) {
                    if ($scope.model[$scope.options.key].length > minimum) {
                        return true;
                    }
                } else {
                    return true;
                }
            }

            ////////////////////////////////////

            $scope.canAdd = function() {
                if (maximum) {
                    if ($scope.model[$scope.options.key].length < maximum) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }
        //defaultValue:[],
        //noFormControl: true,
        //template: '<formly-form model="model[options.key]" fields="options.data.fields"></formly-form>'
    });

    /**/

    var unique = 1;

    // set templates here
    formlyConfig.setType({
        name: 'contentnested',
        templateUrl: 'fluro-admin-content/content-formly/nested.html',
        // defaultOptions: {
        //    noFormControl: true,
        // },
        controller: function($scope) {

            var endWatch = $scope.$watch('model[options.key]', function(keyModel) {
                if (!keyModel) {
                    $scope.model[$scope.options.key] = [];
                    return endWatch();
                }
            })


            ////////////////////////////////////

            $scope.copyFields = copyFields;


            function copyFields(fields) {
                fields = angular.copy(fields);
                addRandomIds(fields);
                return fields;
            }

            function addRandomIds(fields) {
                unique++;
                angular.forEach(fields, function(field, index) {
                    if (field.fieldGroup) {
                        addRandomIds(field.fieldGroup);
                        return; // fieldGroups don't need an ID
                    }

                    if (field.templateOptions && field.templateOptions.fields) {
                        addRandomIds(field.templateOptions.fields);
                    }

                    field.id = field.id || (field.key + '_' + index + '_' + unique + getRandomInt(0, 9999));
                });
            }

            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min)) + min;
            }

            ////////////////////////////////////

            //Definition
            var def = $scope.to.definition;

            var minimum = def.minimum;
            var maximum = def.maximum;
            var askCount = def.askCount;



            if (!minimum) {
                minimum = 0;
            }

            if (!maximum) {
                maximum = 0;
            }

            if (!askCount) {
                askCount = 0;
            }

            //////////////////////////////////////

            if (minimum && askCount < minimum) {
                askCount = minimum;
            }

            if (maximum && askCount > maximum) {
                askCount = maximum;
            }

            //////////////////////////////////////

            if (maximum == 1 && minimum == 1 && $scope.options.key) {
                // console.log('Only 1 Please!!!!', $scope.options)

                if (_.isArray($scope.model[$scope.options.key])) {
                    // console.log('CONVER TOT OBJECT')
                    $scope.model[$scope.options.key] = $scope.model[$scope.options.key][0];
                    if (!$scope.model[$scope.options.key]) {
                        $scope.model[$scope.options.key] = {}
                    }
                }
            } else {
                //Compact the values
                $scope.model[$scope.options.key] = _.compact($scope.model[$scope.options.key]);

                //console.log('Fill er up')
                // if (askCount && !$scope.model[$scope.options.key].length) {
                if (askCount && !$scope.model[$scope.options.key].length) {
                    _.times(askCount, function() {
                        $scope.model[$scope.options.key].push({
                            expanded: true
                        });
                    });
                }
            }

            //////////////////////////////////////
            //////////////////////////////////////

            // $scope.firstLine = function(item) {
            //     var keys = _.chain(item)
            //         .keys()
            //         .without('expanded')
            //         .filter(function(key) {
            //             var i = key.indexOf('$$');
            //             if (i == -1) {
            //                 return true;
            //             } else {
            //                 return false;
            //             }
            //         })
            //         .slice(0, 3)
            //         .value();

            //     return _.at(item, keys).join(',').slice(0, 150);
            // }

            // $scope.items = function() {
            //     return _.compact($scope.model[$scope.options.key]);
            // }

            ////////////////////////////////////

            $scope.canRemove = function() {
                if (minimum) {
                    if ($scope.model[$scope.options.key].length > minimum) {
                        return true;
                    }
                } else {
                    return true;
                }
            }

            $scope.remove = function(entry) {
                //model[options.key].splice($index, 1)
                // console.log('Pull Entry', entry, $scope.model[$scope.options.key].indexOf(entry));
                _.pull($scope.model[$scope.options.key], entry);
            }

            ////////////////////////////////////

            $scope.add = function() {

                var newObject = {
                    expanded: true
                }

                $scope.model[$scope.options.key].push(newObject);
            }


            ////////////////////////////////////

            $scope.canAdd = function() {
                if (maximum) {
                    if ($scope.model[$scope.options.key].length < maximum) {
                        return true;
                    }
                } else {
                    return true;
                }
            }

            // ////////////////////////////////////

            // if (maximum != 1 && $scope.model && $scope.model[$scope.options.key] && $scope.model[$scope.options.key].length) {
            //     $scope.settings.active = $scope.model[$scope.options.key][0];
            // }
        }
    });




    formlyConfig.setType({
        name: 'list-select',
        templateUrl: 'fluro-admin-content/content-formly/list-select.html',
        controller: function($scope, FluroValidate) {



            //Get the definition
            var definition = $scope.to.definition;

            //Minimum and maximum
            var minimum = definition.minimum;
            var maximum = definition.maximum;

            /////////////////////////////////////////////////////////////////////////


            $scope.multiple = (maximum != 1);

            /////////////////////////////////////////////////////////////////////////

            var to = $scope.to;
            var opts = $scope.options;

            $scope.selection = {
                values: [],
                value: null,
            }

            /////////////////////////////////////////////////////////////////////////

            $scope.contains = function(value) {
                if ($scope.multiple) {
                    //Check if the values are selected
                    return _.contains($scope.selection.values, value);
                } else {
                    return $scope.selection.value == value;
                }
            }

            /////////////////////////////////////////////////////////////////////////

            $scope.select = function(value) {
                //console.log('SELECTION', $scope.selection);
                if ($scope.multiple) {
                    $scope.selection.values.push(value);
                } else {
                    $scope.selection.value = value;
                }
            }

            /////////////////////////////////////////////////////////////////////////

            $scope.deselect = function(value) {
                if ($scope.multiple) {
                    _.pull($scope.selection.values, value);
                } else {
                    $scope.selection.value = null;
                }
            }

            /////////////////////////////////////////////////////////////////////////

            $scope.toggle = function(reference) {
                if ($scope.contains(reference)) {
                    $scope.deselect(reference);
                } else {
                    $scope.select(reference);
                }

                //Update model
                setModel();
            }


            /////////////////////////////////////////////////////////////////////////

            // initialize the checkboxes check property
            $scope.$watch('model', function(newModelValue) {
                var modelValue;

                //If there is properties in the FORM model
                if (_.keys(newModelValue).length) {

                    //Get the model for this particular field
                    modelValue = newModelValue[opts.key];



                    //$scope.$watch('to.options', function(newOptionsValues) {
                    if ($scope.multiple) {
                        if (_.isArray(modelValue)) {
                            $scope.selection.values = angular.copy(modelValue);
                        } else {
                            if (modelValue) {
                                $scope.selection.values = [angular.copy(modelValue)];
                            }
                        }
                    } else {
                        if (modelValue) {
                            $scope.selection.value = angular.copy(modelValue);
                        }
                    }
                }
            }, true);


            /////////////////////////////////////////////////////////////////////////

            function checkValidity() {

                var validRequired;
                var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

                //Check if multiple
                if ($scope.multiple) {
                    if ($scope.to.required) {
                        validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
                    }
                } else {
                    if ($scope.to.required) {
                        if ($scope.model[opts.key]) {
                            validRequired = true;
                        }
                    }
                }

                $scope.fc.$setValidity('required', validRequired);
                $scope.fc.$setValidity('validInput', validInput);
            }

            /////////////////////////////////////////////////////////////////////////

            function setModel() {
                if ($scope.multiple) {
                    $scope.model[opts.key] = angular.copy($scope.selection.values);
                } else {
                    $scope.model[opts.key] = angular.copy($scope.selection.value);
                }

                $scope.fc.$setTouched();
                checkValidity();
            }

            /////////////////////////////////////////////////////////////////////////






            if (opts.expressionProperties && opts.expressionProperties['templateOptions.required']) {
                $scope.$watch(function() {
                    return $scope.to.required;
                }, function(newValue) {
                    checkValidity();
                });
            }

            /////////////////////////////////////////////////////////////////////////

            if ($scope.to.required) {
                var unwatchFormControl = $scope.$watch('fc', function(newValue) {
                    if (!newValue) {
                        return;
                    }
                    checkValidity();
                    unwatchFormControl();
                });
            }

        },
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });
});


////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////


app.directive('contentFormly', function(FluroContent, FluroContentRetrieval, $parse, FluroValidate) {
    return {
        restrict: 'E',
        //replace: true,
        transclude: true,
        scope: {
            item: '=ngModel',
            definition: '=',
            definitionFields: '=?',
            dynamic: '=?',
            agreements: '=?',
            raw: '=',
            // isolateFields:'=?',
        },
        templateUrl: 'fluro-admin-content/content-formly/form.html',
        //controller: 'InteractionFormController',
        //templateUrl: 'fluro-interaction-form/fluro-web-form.html',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {


            if (!$scope.item) {
                $scope.item = {}
            }

            // $scope.$watch('isolateFields', function(object) {
            //     $scope.visibleTrails = _.keys(object);
            //     console.log('Visible trails', $scope.visibleTrails);
            // }, true)




            /////////////////////////////////////////////////////////////////

            $scope.$watch('item', update);
            $scope.$watch('definition', update);
            $scope.$watch('definitionFields', update);

            /////////////////////////////////////////////////////////////////

            function update() {

                var item = $scope.item;

                // The model object that we reference
                // on the  element in index.html
                $scope.vm.model = item; //$scope.item;

                // The model object that we reference
                // on the  element in index.html
                $scope.vm.model = $scope.item;


                // An array of our form fields with configuration
                // and options set. We make reference to this in
                // the 'fields' attribute on the  element
                $scope.vm.fields = [];

                /////////////////////////////////////////////////////////////////

                //Keep track of the state of the form
                $scope.vm.state = 'ready';

                /////////////////////////////////////////////////////////////////

                //$scope.vm.onSubmit = submitInteraction;

                /////////////////////////////////////////////////////////////////

                function addFieldDefinition(array, fieldDefinition) {


                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    //Create a new field
                    var newField = {};
                    newField.key = fieldDefinition.keyTrail || fieldDefinition.key;

                    /////////////////////////////

                    if (fieldDefinition.directive == 'embedded') {
                        // console.log('FIELD', newField.key, newField, fieldDefinition)
                        fieldDefinition.className = '';
                    }

                    /////////////////////////////

                    //Add the class name if applicable
                    if (fieldDefinition.className && !fieldDefinition.asObject) {
                        newField.className = fieldDefinition.className;
                    }

                    /////////////////////////////

                    //Template Options
                    var templateOptions = {};
                    templateOptions.type = 'text';
                    templateOptions.label = fieldDefinition.title;
                    templateOptions.description = fieldDefinition.description;

                    //Include the definition itself
                    templateOptions.definition = fieldDefinition;

                    /////////////////////////////

                    //Add a placeholder
                    if (fieldDefinition.placeholder && fieldDefinition.placeholder.length) {
                        templateOptions.placeholder = fieldDefinition.placeholder;
                    } else if (fieldDefinition.description && fieldDefinition.description.length) {
                        templateOptions.placeholder = fieldDefinition.description;
                    } else {
                        templateOptions.placeholder = fieldDefinition.title;
                    }

                    /////////////////////////////

                    //Require if minimum is greater than 1 and not a field group
                    templateOptions.required = (fieldDefinition.minimum > 0);

                    /////////////////////////////

                    //Directive or widget
                    switch (fieldDefinition.directive) {
                        case 'value-select':
                        case 'button-select':
                        case 'order-select':
                            newField.type = 'list-select';
                            break;
                        case 'select':
                            //If its a select but the maximum is 0
                            if (fieldDefinition.maximum != 1) {
                                newField.type = 'list-select';
                            } else {
                                newField.type = 'select';
                            }


                            break;
                        case 'value':
                            newField.type = 'hiddenvalue';
                            break;
                        default:

                            newField.type = fieldDefinition.directive;
                            // console.log('TYPE', newField.type)
                            break;
                    }


                    /////////////////////////////

                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////
                    switch (fieldDefinition.directive) {
                        case 'value':
                            //Don't change the widget
                        break;
                        default:
                            //Directive or widget that is more appropriate for the type of data
                            switch (fieldDefinition.type) {
                                case 'reference':
                                    //Detour here if we are using a select
                                    if (fieldDefinition.directive == 'select' && fieldDefinition.maximum == 1) {

                                        console.log('SELECT', $scope.vm.model[fieldDefinition.key]);
                                        //Map Reference objects to strings
                                        // if ($scope.vm.model[fieldDefinition.key] && $scope.vm.model[fieldDefinition.key]._id) {
                                        //     $scope.vm.model[fieldDefinition.key] = $scope.vm.model[fieldDefinition.key]._id;
                                        // }
                                        // newField.type = 'list-select';

                                        // console.log($scope.vm.model, $scope.vm.model[fieldDefinition.key])

                                        // console.log('FIELD', )
                                        newField.type = 'simple-reference-select';

                                    } else {
                                        newField.type = 'reference-select';
                                    }
                                    break;
                                case 'boolean':
                                    //Detour here

                                    // console.log('GOT PARAMS SORTED', fieldDefinition)
                                    if (fieldDefinition.params && fieldDefinition.params.storeData) {
                                        newField.type = 'terms';
                                        newField.data = {
                                            agreements: $scope.agreements,
                                        }
                                    } else {
                                        newField.type = 'checkbox';
                                    }

                                    break;
                                case 'object':
                                    //Detour here
                                    newField.type = 'object-editor';
                                    break;
                                case 'date':
                                    //Detour here
                                    if ($scope.dynamic) {
                                        newField.type = 'dynamic-date-select';
                                    } else {
                                        newField.type = 'date-select';

                                    }

                                    break;
                            }
                            break;
                    }


                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    //Allowed Options

                    if (fieldDefinition.type == 'reference') {

                        //If we have allowed references specified
                        if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) {
                            templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                                return {
                                    name: ref.title,
                                    value: ref._id,
                                }
                            });
                        } else {

                            //We want to load all the options from the server
                            templateOptions.options = [];

                            if (fieldDefinition.sourceQuery) {

                                //We use the query to find all the references we can find
                                var queryId = fieldDefinition.sourceQuery;
                                if (queryId._id) {
                                    queryId = queryId._id;
                                }


                                /////////////////////////

                                var options = {};

                                //If we need to template the query
                                if (fieldDefinition.queryTemplate) {
                                    options.template = fieldDefinition.queryTemplate;
                                    if (options.template._id) {
                                        options.template = options.template._id;
                                    }
                                }

                                /////////////////////////

                                //Now retrieve the query


                                var promise = FluroContentRetrieval.getQuery(queryId, options);
                                //var promise = FluroContentRetrieval.query(null, null, queryId, true);

                                //Now get the results from the query
                                promise.then(function(res) {
                                    templateOptions.options = _.map(res, function(ref) {
                                        return {
                                            name: ref.title,
                                            value: ref._id,
                                        }
                                    })
                                });


                            } else {

                                if (fieldDefinition.type != 'reference') {
                                    if (!fieldDefinition.params) {
                                        fieldDefinition.params = {};
                                    }

                                    var restrictToType = _.get(fieldDefinition, 'params.restrictType');

                                    if (restrictToType && restrictToType.length) {


                                        console.log('RESTRICT TYPE QUERY', fieldDefinition, restrictToType)
                                        //We want to load all the possible references we can select
                                        FluroContent.resource(restrictToType).query({ limit: 50 }).$promise.then(function(res) {
                                            templateOptions.options = _.map(res, function(ref) {
                                                return {
                                                    name: ref.title,
                                                    value: ref._id,
                                                }
                                            })
                                        });
                                    }
                                }
                            }
                        }
                    } else {
                        //Just list the options specified

                        if (fieldDefinition.options && fieldDefinition.options.length) {
                            templateOptions.options = fieldDefinition.options;
                        } else {
                            templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                                return {
                                    name: val,
                                    value: val
                                }
                            });
                        }
                    }

                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    //What kind of data type, override for things like checkbox
                    if (fieldDefinition.directive == 'input') {
                        switch (fieldDefinition.type) {
                            case 'boolean':
                                newField.type = 'checkbox';
                                break;


                            case 'number':
                            case 'integer':
                            case 'float':
                                templateOptions.type = 'number';
                                break;

                        }
                    }

                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    //Default Options

                    // console.log('REFERENCE SELECT TEST', fieldDefinition.title, fieldDefinition.minimum,  fieldDefinition.maximum, )

                    if (fieldDefinition.maximum == 1) {
                        if (fieldDefinition.type == 'reference') {
                            if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {

                                if (newField.type == 'reference-select') {
                                    newField.defaultValue = fieldDefinition.defaultReferences[0]; //._id;
                                } else {
                                    newField.defaultValue = fieldDefinition.defaultReferences[0]._id;
                                }
                            }
                        } else {
                            if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {
                                newField.defaultValue = fieldDefinition.defaultValues[0];
                            }
                        }
                    } else {
                        if (fieldDefinition.type == 'reference') {
                            if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {

                                if (newField.type == 'reference-select') {
                                    newField.defaultValue = fieldDefinition.defaultReferences;
                                } else {
                                    newField.defaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                                        return ref._id;
                                    });
                                }
                            } else {
                                newField.defaultValue = [];
                            }
                        } else {
                            if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {
                                newField.defaultValue = fieldDefinition.defaultValues;
                            }
                        }
                    }

                    /////////////////////////////

                    //Append the template options
                    newField.templateOptions = templateOptions;

                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    newField.validators = {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            var valid = FluroValidate.validate(value, fieldDefinition);
                            //   console.log(scope.name, 'VALID?', value, valid, scope);
                            return valid;
                        }
                    }

                    /////////////////////////////
                    /////////////////////////////


                    var isEmbedded = fieldDefinition.directive == 'embedded';
                    var hasSubFields = fieldDefinition.fields && fieldDefinition.fields.length;
                    var shouldExpand = !isEmbedded && hasSubFields;

                    //If 'raw' is set then we do want to expanded embedded fields
                    if ($scope.raw && isEmbedded) {
                        shouldExpand = true;
                    }

                    /////////////////////////////

                    //If there are sub items and this is just a group
                    if (shouldExpand) {

                        var fieldContainer;

                        if (isEmbedded || fieldDefinition.asObject) {
                            newField.type = 'contentnested';

                            //Check if it's an array or an object
                            newField.defaultValue = [];

                            if (fieldDefinition.key && fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                                newField.defaultValue = {};
                            }

                            newField.data = {
                                fields: [],
                                // className: fieldDefinition.className,
                            };

                            //Link to the definition of this nested object
                            fieldContainer = newField.data.fields;

                            newField.extras = {
                                skipNgModelAttrsManipulator: true,
                            }

                            //Link to the definition of this nested object
                            fieldContainer = newField.data.fields;

                        } else {
                            //Start again
                            newField = {
                                fieldGroup: [],
                                className: fieldDefinition.className,
                            }

                            //Link to the sub fields
                            fieldContainer = newField.fieldGroup;
                        }

                        //Loop through each sub field inside a group
                        _.each(fieldDefinition.fields, function(sub) {
                            addFieldDefinition(fieldContainer, sub);
                        });
                    }



                    /////////////////////////////

                    if (fieldDefinition.hideExpression && fieldDefinition.hideExpression.length) {
                        // newField.hideExpression = fieldDefinition.hideExpression;

                        newField.hideExpression = function($viewValue, $modelValue, scope) {



                            //Create a new scope object
                            var checkScope = {
                                model: scope.model,
                                data: $scope.vm.model,
                            }

                            var parsedValue = $parse(fieldDefinition.hideExpression)(checkScope);

                            //console.log('hide expression parsed value', fieldDefinition.hideExpression, checkScope, parsedValue);
                            return parsedValue;
                        }


                    }


                    //////////////////////////////////////////
                    //////////////////////////////////////////
                    //////////////////////////////////////////
                    //////////////////////////////////////////
                    //////////////////////////////////////////


                    if (fieldDefinition.expressions && _.keys(fieldDefinition.expressions).length) {


                        // console.log('EXPRESSIONS', fieldDefinition.expressions);

                        //////////////////////////////////////////

                        //Add the hide expression if added through another method
                        if (fieldDefinition.hideExpression && fieldDefinition.hideExpression.length) {
                            fieldDefinition.expressions.hide = fieldDefinition.hideExpression;
                        }

                        //////////////////////////////////////////

                        //Get all expressions and join them together so we just listen once
                        var allExpressions = _.values(fieldDefinition.expressions).join('+');

                        //////////////////////////////////////////



                        // newField.expressionProperties = {
                        //     // $scope.$watch(function() {
                        //     //     return $scope.to.required;
                        //     // }, function(newValue) {
                        //     //     checkValidity();
                        //     // });
                        // }


                        // newField.templateOptions.onChange = function(field, scope) {
                        //      // console.log('ON CHANGE', v1, v2, v3)
                        // }


                        //Now create a watcher
                        // newField.watcher = {
                        //     expression:function(field, scope) {


                        //         console.log('WATCH RECALCULATE', allExpressions)

                        //         var localWatchScope = {
                        //             model: scope.model,
                        //             data: $scope.vm.model,
                        //             interaction: $scope.vm.model,
                        //             // dateTools: DateTools,
                        //         }

                        //         //Return the result
                        //         var val = $parse(allExpressions)(localWatchScope);


                        //         console.log('PARSE EXPRESSIONS', allExpressions, val);

                        //         return val;

                        //     },
                        //     listener: function(field, newValue, oldValue, scope, stopWatching) {

                        //         console.log('Expression Changed', newValue);

                        //         //Create a new scope object
                        //         var checkScope = {
                        //             model: scope.model,
                        //             data: $scope.vm.model,
                        //             interaction: $scope.vm.model,
                        //             // dateTools: DateTools,
                        //         }

                        //         //Loop through each expression that needs to be evaluated
                        //         _.each(fieldDefinition.expressions, function(expression, key) {

                        //             //Get the value
                        //             var retrievedValue = $parse(expression)(checkScope);

                        //             //console.log('expression:', retrievedValue, checkScope);

                        //             //Get the field key
                        //             var fieldKey = field.key;

                        //             ///////////////////////////////////////

                        //             switch (key) {
                        //                 case 'defaultValue':
                        //                     if (!field.formControl || !field.formControl.$dirty) {
                        //                         return scope.model[fieldKey] = retrievedValue;
                        //                     }
                        //                     break;
                        //                 case 'value':
                        //                     return scope.model[fieldKey] = retrievedValue;
                        //                     break;
                        //                 case 'required':
                        //                     return field.templateOptions.required = retrievedValue;
                        //                     break;
                        //                 case 'hide':
                        //                     return field.hide = retrievedValue;
                        //                     break;
                        //             }
                        //         });
                        //     },
                        // }



                        /////////////////////////////////

                        if (!newField.data) {
                            newField.data = {}
                        }

                        /////////////////////////////////

                        newField.link = function(localScope, $element, $attrs) {

                            if (!newField.data.expressionWatcher) {
                                newField.data.expressionWatcher = $scope.$watch(function() {

                                    var localWatchScope = {
                                        model: localScope.model,
                                        data: $scope.vm.model,
                                        interaction: $scope.vm.model,
                                    }

                                    return $parse(allExpressions)(localWatchScope);

                                }, computeExpression);
                            }

                            ////////////////////////////////

                            function computeExpression() {

                                var checkScope = {
                                    model: localScope.model,
                                    data: $scope.vm.model,
                                    interaction: $scope.vm.model,
                                    // dateTools: DateTools,
                                }


                                _.each(fieldDefinition.expressions, function(expression, key) {
                                    //Get the value
                                    var retrievedValue = $parse(expression)(checkScope);

                                    //console.log('expression:', retrievedValue, checkScope);

                                    //Get the field key
                                    var fieldKey = newField.key;

                                    ///////////////////////////////////////

                                    switch (key) {
                                        case 'defaultValue':
                                            if (!newField.formControl || !newField.formControl.$dirty) {
                                                return localScope.model[fieldKey] = retrievedValue;
                                            }
                                            break;
                                        case 'value':
                                            return localScope.model[fieldKey] = retrievedValue;
                                            break;
                                        case 'required':
                                            return newField.templateOptions.required = retrievedValue;
                                            break;
                                        case 'hide':
                                            return newField.hide = retrievedValue ? true : false;
                                            break;
                                    }
                                });



                            }
                            // newField.watcher = {
                            //     expression:function(field, scope) {


                            //         console.log('WATCH RECALCULATE', allExpressions)

                            //         var localWatchScope = {
                            //             model: scope.model,
                            //             data: $scope.vm.model,
                            //             interaction: $scope.vm.model,
                            //             // dateTools: DateTools,
                            //         }

                            //         //Return the result
                            //         var val = $parse(allExpressions)(localWatchScope);


                            //         console.log('PARSE EXPRESSIONS', allExpressions, val);

                            //         return val;

                            //     },
                            //     listener: function(field, newValue, oldValue, scope, stopWatching) {

                            //         console.log('Expression Changed', newValue);

                            //         //Create a new scope object
                            //         var checkScope = {
                            //             model: scope.model,
                            //             data: $scope.vm.model,
                            //             interaction: $scope.vm.model,
                            //             // dateTools: DateTools,
                            //         }

                            //         //Loop through each expression that needs to be evaluated
                            //         _.each(fieldDefinition.expressions, function(expression, key) {

                            //             //Get the value
                            //             var retrievedValue = $parse(expression)(checkScope);

                            //             //console.log('expression:', retrievedValue, checkScope);

                            //             //Get the field key
                            //             var fieldKey = field.key;

                            //             ///////////////////////////////////////

                            //             switch (key) {
                            //                 case 'defaultValue':
                            //                     if (!field.formControl || !field.formControl.$dirty) {
                            //                         return scope.model[fieldKey] = retrievedValue;
                            //                     }
                            //                     break;
                            //                 case 'value':
                            //                     return scope.model[fieldKey] = retrievedValue;
                            //                     break;
                            //                 case 'required':
                            //                     return field.templateOptions.required = retrievedValue;
                            //                     break;
                            //                 case 'hide':
                            //                     return field.hide = retrievedValue;
                            //                     break;
                            //             }
                            //         });
                            //     },
                            // }



                            // console.log('CREATE WATCHER', allExpressions, localScope);
                        }

                        // console.log('CREATE WATCHER', allExpressions, newField);
                        // newField.runExpressions();

                        // newField.watcher.listener()

                        //console.log('GET WATCHER', newField.watcher);

                    }




                    /////////////////////////////

                    if (newField.key != '_contact' && newField.type != 'value') {
                        //Push our new field into the array
                        array.push(newField);
                    }
                }

                /////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////

                if ($scope.definitionFields && $scope.definitionFields.length) {

                    //Loop through each defined field and add it to our form
                    _.each($scope.definitionFields, function(fieldDefinition) {
                        addFieldDefinition($scope.vm.fields, fieldDefinition);
                    });

                } else {
                    if ($scope.definition) {
                        //Loop through each defined field and add it to our form
                        _.each($scope.definition.fields, function(fieldDefinition) {
                            addFieldDefinition($scope.vm.fields, fieldDefinition);
                        });
                    }
                }


            }



        },
    };
});