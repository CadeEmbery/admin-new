app.controller('ChannelController', function($scope, channel, items) {

	$scope.channel = channel;
    $scope.items = items;

    console.log('REDUCE', items.length)

    $scope.groups = _.reduce(items, function(results, item) {


        var key;

        if (item.definition && item.definition.length) {
            key = item.definition;
        } else {
            key = item._type;
        }

        console.log('KEY', key)

        ////////////////////////////////////////////

        var existing = _.find(results, {
            key: key
        });

        if (!existing) {

            existing = {
                title: key,
                key: key,
                items: []
            }
            results.push(existing);
        }

        existing.items.push(item);


        return results;

    }, []);

})