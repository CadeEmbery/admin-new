SongSelectRouteController.resolve = {
    integrations: function(FluroContent) {
        return FluroContent.endpoint('integrate/list/songselect').query().$promise;
    }
}

//////////////////////////////////////////////

function SongSelectRouteController($scope, integrations, $uibModal, Selection, FluroContent, CacheManager, Notifications) {


    $scope.settings = {
        selection: [],
        pager: {
            maxSize: 10,
            limit: 20,
            currentPage: 0,
        }
    };

    ////////////////////////////////////

    $scope.songs = [];

    ////////////////////////////////////

    $scope.preview = function(originalSong) {

        var integrationID = _.get($scope.settings.integration, '_id');

        var modalInstance = $uibModal.open({
            templateUrl: 'routes/import.songselect/preview.html',
            size: 'md',
            resolve: {
                song: function(FluroContent, $q, Notifications) {

                    var deferred = $q.defer();

                    ////////////////////////////
                    FluroContent.endpoint('integrate/songselect/' + integrationID + '/song/' + originalSong._id)
                        .get()
                        .$promise
                        .then(deferred.resolve, function(err) {

                            Notifications.error(err);
                            return deferred.reject(err);
                        });

                    ////////////////////////////

                    return deferred.promise;
                },
            },
            controller: function($scope, song, CacheManager) {

                $scope.song = song;

                $scope.selectedKeys = {}

                _.each(song.Keys, function(key) {
                    $scope.selectedKeys[key] = true;
                })

                
                $scope.availableKeys = [
                    'Ab',
                    'A',
                    'Bb',
                    'B',
                    'Cb',
                    'C',
                    'C#',
                    'Db',
                    'D',
                    'Eb',
                    'E',
                    'F',
                    'F#',
                    'Gb',
                    'G',
                ]

                ////////////////////////////////////////////

                $scope.getSelectedKeys =  function() {
                    return  _.chain($scope.selectedKeys)
                    .map(function(key, i) {
                        if(key) {
                            return i
                        }
                    })
                    .compact()
                    .value();

                }


                $scope.keyCount = function() {
                    return $scope.getSelectedKeys().length;
                }

                ////////////////////////////////////////////

                var songLyrics = [];

                //If there are lyrics
                if(song.LyricPreview && song.LyricPreview.length) {
                    songLyrics = String(song.LyricPreview).split('|');
                }

                $scope.lyrics = _.chain(songLyrics)
                .compact()
                .map(function(str) {
                    return str.trim();
                })
                .value();

                ////////////////////////////////////////////

                $scope.import = function() {

                    if (song.importing || song.imported) {
                        return;
                    }

                    ////////////////////////////

                    song.importing = true;
                    originalSong.importing = true;

                    ////////////////////////////

                    console.log('IMPORTING', song);

                    CacheManager.clear('song')
                    CacheManager.clear('sheetMusic');


                    var keysToImport = $scope.getSelectedKeys()

                    var request = FluroContent.endpoint('integrate/songselect/' + integrationID + '/song/' + song.ID + '/import').get({keys:keysToImport}).$promise;
                    request.then(function(res) {

                        Notifications.status(song.Title + ' was added to your library');
                        originalSong.importing = song.importing = false;
                        originalSong.imported =  song.imported = true;

                        if($scope.$dismiss) {
                            $scope.$dismiss();
                        }

                    }, function(err) {
                        console.log('ERROR IMPORTING',err);
                        // Notifications.error(err);

                        var message = _.get(err, 'data.advice');

                        if(message && message.length) {
                            Notifications.error(message);
                        } else {
                            Notifications.error('There was an error when trying to import this song');
                        }

                        originalSong.importing = song.importing = false;
                    })
                }

                ////////////////////////////////////////////

                $scope.themes = (_.get(song, 'Themes') || []).join(', ');
            }
        });
    }

    ////////////////////////////////////

    $scope.integrations = integrations;

    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////

    $scope.importSelected = function() {

        if ($scope.settings.importing) {
            return;
        }


        var integrationID = _.get($scope.settings.integration, '_id');

        ////////////////////////////

        $scope.settings.importing = true;

        ////////////////////////////

        //Get all the songs
        var songs = $scope.selection.items.slice();

        ////////////////////////////

        var details = {};

        details.songs = _.map(songs, function(song) {
            song.importing = true;
            return song.ID;
        })

        ////////////////////////////

        console.log('IMPORTING', songs);

        CacheManager.clear('song')
        CacheManager.clear('sheetMusic');

        ////////////////////////////

        var request = FluroContent.endpoint('integrate/songselect/' + integrationID + '/import').save(details).$promise;
        request.then(importComplete, importFailed);

        //////////////////////////////

        function importComplete(res) {

            Notifications.status(res.length + ' songs were added to your library');

            _.each(songs, function(song) {
                song.imported = true;
                song.importing = false;
            })

            $scope.settings.importing = false;

            //Deselect all songs
            $scope.selection.deselectAll()

        }

        //////////////////////////////

        function importFailed(err) {
            Notifications.error(err);

            _.each(songs, function(song) {
                song.importing = false;
            })


            $scope.settings.importing = false;
        }

        //////////////////////////////

    }



    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////

    $scope.selection = new Selection($scope.settings.selection, $scope.songs, function() {
        return true;
    });

    ////////////////////////////////////

    if (integrations.length == 1) {
        $scope.settings.integration = integrations[0];
    }

    //////////////////////////////////////////////////////


    // $scope.submit = function() {
    //     // console.log('SUBMITTTT', $scope.settings)
    //     _.set($scope.settings, 'search.terms', $scope.settings.proposedSearch);
    // }

    $scope.modelOptions = {
        debounce:700
    }

    $scope.$watch('settings.search', searchUpdate, true)

    //////////////////////////////////////////////////////


    function clearResults() {
        $scope.songs = [];
        $scope.settings.pager.total = 0;
        $scope.settings.processing = false;
    }


    //////////////////////////////////////////////////////

    function resultsLoaded(res) {

        var results = res['Results'];

        //////////////////////////////////////////

        $scope.songs = _.map(results, function(song) {
            song._id = song.ID;
            song.title = song.Title;
            song.authors = song.Authors.join(', ');
            song.keys = song.Keys.join(', ');
            song.chords = song.HasChordSheet;
            song.lead = song.HasLeadSheet;
            song.hymn = song.HasHymnSheet;
            song.lyrics = song.HasLyrics;
            song.ccli = song.SongNumber;

            return song;
        });

        //////////////////////////////////////////



        //////////////////////////////////////////

        $scope.settings.pager.total = res['TotalResults'];

        //////////////////////////////////////////

        if ($scope.settings.pager.total < ($scope.settings.pager.limit * ($scope.settings.pager.currentPage))) {
            $scope.settings.pager.currentPage = 0;
        }

        //////////////////////////////////////////

        //Use songs as the item list
        Selection.items = $scope.songs;
        $scope.settings.processing = false;
    }

    //////////////////////////////////////////////////////

    function searchUpdate() {

        var criteria = $scope.settings.search;

        // console.log('CRITERIA', criteria);

        if (!criteria) {
            return clearResults();
        }

        //Get the integration
        var integration = $scope.settings.integration;

        if (!integration) {
            return clearResults();
        }

        /////////////////////////////////////////

        //Get the integrationID
        var integrationID = integration._id || integration;

        /////////////////////////////////////////

        var keywords = criteria.terms;

        if (!keywords || !keywords.length) {
            return clearResults();
        }

        var limit = $scope.settings.pager.limit;
        var currentPage = $scope.settings.pager.currentPage;
        var offsetNumber = currentPage * limit;


        $scope.settings.processing = true;

        /////////////////////////////////////////

        var promise = FluroContent.endpoint('integrate/songselect/' + integrationID + '/search/' + keywords).get({
            limit: limit,
            offset: offsetNumber,
        }).$promise;

        /////////////////////////////////////////

        return promise.then(resultsLoaded, clearResults);
    }

    //////////////////////////////////////////////////////

    $scope.updatePage = searchUpdate;

    //////////////////////////////////////////////////////


}