MyCalendarController.resolve = {
    calendars: function(FluroContent) {
        return FluroContent.endpoint('ical/available').query().$promise;
    }
}

//////////////////////////////////////////////

function MyCalendarController($scope, calendars, Fluro) {

    

    $scope.calendarGroups = _.chain(calendars)
    .reduce(function(set, calendar) {

    	var type = calendar.type || 'default';

    	if(set[type]) {
    		set[type].calendars.push(calendar);
    	}
    	return set;
    }, {
    	default:{
    		title:'Events',
    		calendars:[],
    	},
    	realm:{
    		title:'Realm Events',
    		calendars:[],
    	},
    	eventtrack:{
    		title:'Event Tracks',
    		calendars:[],
    	},
    	
    })
    .values()
    .value();

console.log('CALENDAR GROUPS', $scope.calendarGroups);

// http://www.google.com/calendar/render?cid=webcal%3A%2F%2Fr.elvanto.com.au%2Ffullcalendar%2Ffb713e01-1b98-4755-ab69-4c10eea3c8a5%2F6e9884ba-0fed-11e5-bb6c-06ba798128be.ics
	// webcal://r.elvanto.com.au/fullcalendar/fb713e01-1b98-4755-ab69-4c10eea3c8a5/6e9884ba-0fed-11e5-bb6c-06ba798128be.ics
	$scope.calendarLink = function(calendar, type) {


		var webcalPrefix = 'webcal://';

		var fullURL = webcalPrefix += calendar.url;

		switch(type) {
			case 'google':
				return 'http://www.google.com/calendar/render?cid=' + fullURL;
			break;
			default:
				return fullURL;
			break;
		}
	}

}