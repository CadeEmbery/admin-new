MatrixRouteController.resolve = {
    // events: function($stateParams, $q, FluroContentRetrieval) {


    //     var eventIDs = $stateParams.events;

    //     var promise = FluroContentRetrieval.query({
    //         _id: {
    //             $in: eventIDs
    //         }
    //     }, null, null, {
    //         noCache: true,
    //         appendAssignments: true,
    //     })


    //     var deferred = $q.defer();
    //     promise.then(function(events) {

    //         var sorted = _.sortBy(events, function(event) {
    //             return new Date(event.startDate).getTime();
    //         })
    //         deferred.resolve(sorted);

    //     })

    //     return deferred.promise;

    //     // return $stateParams.events
    // },


    // assignments: function($stateParams, MatrixRouteService) {
    //     var eventIDs = $stateParams.events;
    //     return MatrixRouteService.loadAssignments(eventIDs);
    // },



    // rosterTypes: function($stateParams, MatrixRouteService) {
    //     // var eventIDs = $stateParams.events;
    //     return MatrixRouteService.loadRosterTypes();
    // },

    // rosters: function($stateParams, MatrixRouteService) {
    //     var eventIDs = $stateParams.events;
    //     return MatrixRouteService.loadRosters(eventIDs);
    // }


    columns: function($stateParams, MatrixRouteService, MatrixSelectService) {

        //Clear previous selections
        MatrixSelectService.deselect();

        /////////////////////////////////////

        var eventIDs = $stateParams.events;
        if (_.isArray(eventIDs)) {
            eventIDs = _.take(eventIDs, 50);
        }

        return MatrixRouteService.load(eventIDs);
    }
}





app.directive('matrixTable', function(MatrixSelectService) {
    return {
        restrict: 'A',
        link: function($scope, $element, $attrs) {



            //////console.log('get', $element)
            //
            $scope.$watch(function() {
                return MatrixSelectService.selected.slot;
            }, function(slot) {
                if (slot) {
                    $element.addClass('selection-active');
                } else {
                    $element.removeClass('selection-active');
                }
            })


            var $body = $element.find('.matrix-body');
            var $sidebar = $element.find('.matrix-sidebar .matrix-table');
            var $header = $element.find('.matrix-header .matrix-table');

            $body.on('scroll', bodyScrolled);

            function bodyScrolled() {
                //////console.log('scrolled')
                $sidebar.css('margin-top', -$body.scrollTop());
                $header.css('margin-left', -$body.scrollLeft());
                // $sidebar.css('transform', 'translateY(' + -$body.scrollTop() + 'px)');
                // $header.css('transform', 'translateX(' + -$body.scrollLeft() + 'px)');
            }

        }
    };
});




// app.directive('matrixSlotSummary', function() {
//     return {
//         restrict: 'E',
//         template:'<div>{{slotSummary}}</div>',
//         scope:{
//             model:"@ngModel"
//         },
//         link: function($scope, $element, $attrs) {


//             $scope.slotSummary = '';

//             //////console.log('get', $element)
//             //
//             $scope.$watch('model', function(slot) {

//                 //console.log('MODEL CHANGE');

//                 if(!slot.assignments) {
//                     return $scope.slotSummary = '';
//                 }

//                 if(slot.assignments.length == 1) {
//                     return $scope.slotSummary = (slot.assignments[0].contact.title || slot.assignments[0].contactName)
//                 }

//                 if(slot.assignments.length < 3) {
//                     return _.map(slot.assignments, function(assignment) {
//                         return (slot.assignments[0].contact.firstName || slot.assignments[0].contactName);
//                     })
//                     .join(', ')
//                 }

//                 return _.countBy(slot.assignments, function(assignment) {
//                     return assignment.confirmationStatus;
//                 })
//             })
//         }
//     };
// });


///////////////////////////////////////////////////////////////////

function MatrixRouteController($scope, $state, FluroStorage, $uibModal, FluroAccess, $stateParams, $timeout, FluroSocket, ModalService, MatrixSelectService, MatrixRouteService, columns, Notifications, FluroContent, $q, ModalService) {

    //////console.log('ASSIGNMENTS', assignments);

    $scope.selection = MatrixSelectService;

    ////////////////////////////////////////////////////

    $scope.local = FluroStorage.localStorage('matrix');
    $scope.session = FluroStorage.sessionStorage('matrix');

    if (!$scope.local.hide) {
        $scope.local.hide = {};
    }

    ////////////////////////////////////////////////////

    $scope.columns = columns;

    ////////////////////////////////////////////////////

    $scope.$watch('columns', columnsUpdated);

    $scope.$on('matrix-rerender', columnsUpdated);
    $scope.$on('matrix-tally', tallyAssignments);


    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    // var canCreatePlans = FluroAccess.can('create', 'plan');

    //////////////////////////////////////////////////////

    $scope.swap = function() {

    }

    // //Include the ar
    // $scope.removeAssignment = function(assignment) {


    // ////////////////////////////////////////////////////

    // $scope.removeAssignment = function(assignment) {

    //     MatrixRouteService.archiveAssignment(assignment)
    //     .then(function(res) {
    //         assignment.status = res.status;

    //     }, function(err) {
    //         //console.log('Error updating assignment', err);
    //     });
    // }

    function tallyAssignments() {
        //console.log('Tally Assignments')

        //Extract the information we need
        var allAssignments = _.chain(columns)
            .map(function(column) {

                //Get the assignments from this column
                var slotObjects = MatrixRouteService.retrieveSlotsFromColumn(column);

                //////////////////////////////////////////////

                //Map all the slots
                return _.map(slotObjects, function(slotObject) {

                    var rosterName = slotObject.rosterName;
                    var rosterTitle = slotObject.rosterTitle;
                    var slots = slotObject.slots;

                    //Map all the assignments
                    return _.map(slots, function(slot) {

                        return _.map(slot.assignments, function(assignment) {

                            //Return the assignments
                            return {
                                rosterTitle: rosterTitle,
                                rosterName: rosterName,
                                assignment: assignment,
                                slot: slot,
                                column: column,
                                event: column.event,
                                contact: assignment.contact,
                                confirmationStatus: assignment.confirmationStatus,
                            }
                        });

                    });


                })
            })
            .flattenDeep()
            .compact()
            .value();

        //////////////////////////////////////////////////////

        //Get a tally of all assignments
        var summary = MatrixRouteService.mapAssignmentsByStatus(allAssignments);;

        $timeout(function() {


            $scope.summary = summary;

            switch ($scope.local.view) {
                case 'denied':
                case 'unknown':
                case 'proposed':
                case 'confirmed':
                    var hasContent = _.get(summary, $scope.local.view + 'assignments.length');
                    if (!hasContent) {
                        $scope.local.view = 'matrix'
                    }
                    break;
            }

        });

        // $scope.summaries = MatrixRouteService.mapAssignmentsByStatus(summarized);
    }
    ////////////////////////////////////////////////////

    function columnsUpdated() {
        ////console.log('Update Render');


        var maxPlans = _.chain(columns)
            .map(function(column) {
                return column.plans.length;
            })
            .max()
            .value();


        // if(canCreatePlans) {
        //     maxPlans += 1;
        // } 

        $scope.maxPlanCount = Math.max(maxPlans, 1);

        //////////////////////////////////////////////////////

        $scope.contactDates = [];

        //////////////////////////////////////////////////////

        //Retally the assignment count
        tallyAssignments();

        //////////////////////////////////////////////////////

        function getRowCount(definition, slot) {

            var numbers = _.chain(columns)
                .map(function(column) {
                    return _.values(column.rosters);
                })
                .flatten()
                .filter(function(rosterDefinition) {
                    return (rosterDefinition.definitionName == definition.definitionName) && rosterDefinition.roster;
                })
                .map(function(rosterType) {

                    // ////console.log('GET ROSTER', rosterType.roster);
                    if (!rosterType.roster.slots) {
                        return;
                    }

                    return _.find(rosterType.roster.slots, function(rosterSlot) {
                        return String(rosterSlot.title).toLowerCase() == String(slot.title).toLowerCase();
                    });
                })
                .flatten()
                .compact()
                .map(function(slot) {
                    if (slot.assignments) {
                        // ////console.log('NUMBER', slot.assignments.length);
                        return Math.max(slot.assignments.length, 1);
                    }

                    return 1;
                })
                .max()
                .value();

            return numbers;
        }

        ////////////////////////////////////////////////////

        // $scope.teamSlots = _.reduce(columns, function(set, column) {

        //     //Loop through each slot defined on the column
        //     _.each(column.teamSlots, function(slot) {

        //         //Get the lowercase slot name
        //         var lowercaseSlotName = String(slot.title).toLowerCase()

        //         var existing = _.find(set, function(s) {
        //             return String(s.title).toLowerCase() == lowercaseSlotName;
        //         });

        //         if(!existing) {
        //             existing = {
        //                 title:slot.title,
        //                 count:slot.assignments.length,
        //             }
        //             set.push(existing);
        //         }

        //         if(slot.assignments.length > existing.count) {
        //             existing.count = slot.assignments.length;
        //         }
        //     });

        //     return set;


        // }, []);



        ////////////////////////////////////////////////////

        //Map all assignments to rosters
        $scope.rosterTypes = _.reduce(columns, function(set, column) {

            //Loop through each roster type
            _.each(column.rosters, function(definition) {

                //Check if there is an entry for this roster type
                var existing = _.find(set, {
                    definitionName: definition.definitionName
                });

                //If there isnt one create it now
                if (!existing) {
                    existing = {
                        _id: definition._id,
                        title: definition.title,
                        definitionName: definition.definitionName,
                        slots: [],
                        originalReference: definition,
                        realms: definition.realms,
                    };

                    //Get all the default slots
                    //Get the default slots if any exist
                    var defaultSlots = _.get(definition, 'slots');
                    existing.slots = existing.slots.concat(defaultSlots);

                    //Add to set
                    set.push(existing);
                }

                ///////////////////////////////////////////////////////////////////

                //Now we know there is an entry for this roster type
                //we need to add any 'ad-hoc' slots from each roster
                var rosterSlots = _.map(_.get(definition, 'roster.slots'), function(rosteredSlot) {

                    return {
                        title: rosteredSlot.title,
                        assignments: rosteredSlot.assignments,
                        //     confirmationBehaviour:rosteredSlot.confirmationBehaviour,
                        //     disableHistorySuggestion:rosteredSlot.disableHistorySuggestion,
                        //     disableTeamSuggestion:rosteredSlot.disableTeamSuggestion,
                        //     disableTagSuggestion:rosteredSlot.disableTagSuggestion,
                        //     requireCapabilities:rosteredSlot.requireCapabilities,
                    }
                });

                existing.slots = existing.slots.concat(rosterSlots);

                //Make sure we remove the duplicates
                existing.slots = _.chain(existing.slots)
                    .compact()
                    .uniq(function(slot) {
                        return slot.title.toLowerCase();
                    })
                    .map(function(slot) {

                        slot = _.clone(slot);
                        delete slot.assignments;

                        var count = getRowCount(definition, slot)

                        slot.rowCount = count;
                        return slot;
                    })
                    .value();

            })

            //Return the set
            return set;

        }, []);

        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////

        //Map the events as dates
        $scope.dates = _.chain($scope.columns)
            .reduce(function(results, column) {

                var event = column.event;

                //Unique date key
                var date = new Date(event.startDate);
                date.setHours(0, 0, 0, 0);

                ////////////////////////////////////////////

                var dateKey = date.format('j M Y');

                //Get existing if this date is already in the set
                var existing = _.find(results, {
                    dateKey: dateKey
                });

                //If its not, then create the date for the set
                if (!existing) {
                    existing = {
                        dateKey: dateKey,
                        date: date,
                        columns: [],
                    }

                    results.push(existing);
                }

                existing.columns.push(column);

                return results;

            }, [])

            .map(function(date, key) {

                var even = (key % 2) == 0;
                var columnIndexLimit = (date.columns.length - 1)

                //////////////////////////////////////////////////////

                _.each(date.columns, function(column, index) {


                    ////console.log('COLUMN', column._id, date.dateKey, column.event.title,  index, columnIndexLimit);

                    column.first = (index == 0);
                    column.last = (index == columnIndexLimit);
                    column.even = even;

                    ///////////////////////////////////////


                    var classes = [];

                    ///////////////////////////////////////

                    if (even) {
                        classes.push('even');
                    } else {
                        classes.push('odd');
                    }

                    ///////////////////////////////////////

                    if (column.last) {
                        classes.push('last');
                    }

                    if (column.first) {
                        classes.push('first');
                    }

                    ///////////////////////////////////////

                    column.class = classes.join(' ');

                })

                return date;
            })
            .value();


    }


    ////////////////////////////////////////////////////

    $scope.close = function() {

        var firstItem = _.first(columns);

        if (firstItem && firstItem.event && firstItem.event.definition) {
            $state.go('event.custom', {
                definition: firstItem.event.definition,
            });
        } else {
            $state.go('event');
        }
    }

    ////////////////////////////////////////////////////

    $scope.neededAlert = function(slot, roster) {
        var match = $scope.getRosterSlot(slot, roster);
        if (!match) {
            return;
        }

        var assignmentCount = _.get(match, 'assignments.length');

        if (match.minimum) {
            if (assignmentCount < match.minimum) {
                return assignmentCount + '/' + match.minimum;
            }
        }

        if (match.maximum) {
            if (assignmentCount > match.maximum) {
                return assignmentCount + '/' + match.minimum;
            }
        }

    }

    ////////////////////////////////////////////////////

    $scope.getAllProposed = function() {

        return _.chain($scope.columns)
            .map(function(column) {

                return _.reduce(column.rosters, function(set, roster) {

                    var slots = _.get(roster, 'roster.slots');
                    var assignments = _.map(slots, 'assignments');

                    set.push(assignments);

                    return set;
                }, [])
            })
            .flattenDeep()
            .compact()
            .filter(function(assignment) {
                return assignment.confirmationStatus == 'proposed';
            })
            .value()
    }

    $scope.getProposed = function(roster) {
        if (!roster) {
            return;
        }

        return _.chain(roster.slots)
            .map(function(slot) {
                return slot.assignments;
            })
            .flatten()
            .compact()
            .filter(function(assignment) {
                return assignment.confirmationStatus == 'proposed';
            })
            .value()
    }

    ////////////////////////////////////////////////////

    $scope.saveRoster = function(roster) {
        var assignments = $scope.getProposed(roster);
        return saveAssignments(assignments);
    }

    ////////////////////////////////////////////////////

    $scope.saveAll = function() {



        $scope.settings.saving = true;
        // socketEnabled = false;

        var assignments = $scope.getAllProposed();

        // if(assignments.length) {
        var confirmed = window.confirm('Are you sure you want to send and notify ' + assignments.length + ' assignments?');

        if (!confirmed) {
            return;
        }

        // }
        return saveAssignments(assignments);
    }

    ////////////////////////////////////////////////////

    function saveAssignments(assignments) {

        var promises = _.map(assignments, function(assignment) {

            assignment.processing = true;
            assignment.contact.title = 'saving'
            var request = FluroContent.resource('assignment/' + assignment._id).update({
                confirmationStatus: 'unknown'
            }).$promise;


            request.then(function(res) {
                assignment.processing = false;
                _.assign(assignment, res);
            }, function(err) {
                assignment.processing = false;
            });


        })

        ////////////////////////////////////////////////

        $q.all(promises).then(saveComplete, saveError);

        ////////////////////////////////////////////////

        function saveComplete(res) {
            $scope.settings.saving = false;
            Notifications.status(assignments.length + ' new assignments were created');
        }


        function saveError(err) {

            $scope.settings.saving = false;
            Notifications.warning(err);
        }

    }


    $scope.getRosterSlotAssignments = function(slot, roster) {

        var rosterMatch = $scope.getRosterSlot(slot, roster);

        if (!rosterMatch) {
            return [];
        }

        return _.chain(rosterMatch.assignments)
            .sortBy(function(assignment) {
                return assignment.confirmationStatus;
            })
            .value();



        // .assignments | orderBy:'confirmationStatus' 
    }
    ////////////////////////////////////////////////////

    $scope.getRosterSlot = function(slot, roster) {


        if (!roster) {
            return slot;
        }

        var match = $scope.getSlot(slot, roster);

        if (match) {
            return match;
        } else {
            return slot;
        }
    }


    ////////////////////////////////////////////////////

    $scope.getTeamSlot = function(slot, column) {

        var match = _.find(column.teamSlots, function(existingSlot) {
            return String(existingSlot.title).toLowerCase() == String(slot.title).toLowerCase();
        })

        return match;
    }


    ////////////////////////////////////////////////////

    $scope.canEditRoster = function(roster) {
        if (!roster) {
            return;
        }
        var itemObject = _.clone(roster);
        itemObject._type = 'roster';
        return FluroAccess.canEditItem(itemObject);
    }
    ////////////////////////////////////////////////////

    $scope.getSlot = function(slot, roster) {

        if (!roster) {
            return;
        }


        var match = _.find(roster.slots, function(existingSlot) {
            return String(existingSlot.title).toLowerCase() == String(slot.title).toLowerCase();
        })

        return match;
    }




    ////////////////////////////////////////////////////

    $scope.select = function(slot, column, definitionName) {

        //console.log('SELECT', slot, column, definitionName);



        var roster = _.get(column, 'rosters[' + definitionName + '].roster');
        var match = $scope.getSlot(slot, roster);


        // console.log('SLOT', match)

        if (!match) {


            match = {
                title: slot.title,
                minimum: slot.minimum,
                maximum: slot.maximum,
                assignments: [],
            }

            if (!roster.slots) {
                roster.slots = [];
            }
            roster.slots.push(match);
            // return MatrixSelectService.deselect();
            ////console.log('CREATE SLOT')
        }


        MatrixSelectService.selected = {
            roster: roster,
            event: column.event,
            slot: match,
        }

    }

    $scope.isSelected = function(slot, roster) {
        if (!MatrixSelectService.selected.slot) {
            return;
        }
        var match = $scope.getSlot(slot, roster);

        return MatrixSelectService.selected.slot == match;
    }

    $scope.deselect = function() {
        MatrixSelectService.deselect();
    }

    ////////////////////////////////////////////////////

    $scope.addRoster = function(column, definition) {

        column.addingRoster = definition.definitionName;
        var newRoster = {
            title: definition.title,
            realms: column.event.realms,
            event: column._id,
            slots: definition.slots,
        }

        ////console.log('Createnew roster', definition, newRoster);

        var request = FluroContent.resource(definition.definitionName).save(newRoster).$promise;

        request.then(function(res) {
            column.addingRoster = false;
            column.rosters[definition.definitionName].roster = res;
        }, function(err) {
            column.addingRoster = false;
            ////console.log('ERROR', err);
        });

        // //Add a new roster
        // column.rosters[definition.definitionName].roster = {

        // }

        return columnsUpdated();
    }


    ////////////////////////////////////////////////////

    $scope.addPlan = function(column, fromNew) {

        var eventCopy = _.clone(column.event);
        eventCopy._type = 'event';

        if (fromNew) {
            var params = {
                template: {
                    title: 'Run Sheet Plan',
                    event: eventCopy,
                    startDate: eventCopy.startDate,
                    realms: eventCopy.realms,
                }
            }

            ///////////////////////////////////////////////

            return ModalService.create('plan', params, planCreateSuccess, planCreateFail)
        }


        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        function planCreateSuccess(copyResult) {
            //console.log('Copy Result!', copyResult);
            if (copyResult) {
                column.plans.push(copyResult);
                return columnsUpdated();
            }

        }

        function planCreateFail(err) {
            //console.log('Plan Failed', err);
        }


        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        var selection = {};

        var modal = ModalService.browse('plan', selection);
        modal.result.then(selectPlan, selectPlan)




        /////////////////////////////////////////////////////////////////

        function selectPlan() {
            if (selection.items && selection.items.length) {

                //Create an array if there isnt one already
                if (!column.plans || !column.plans.length) {
                    column.plans = [];
                }

                /////////////////////////////////////////////////////////////////

                //Get the template
                var template = selection.items[0];
                template.event = eventCopy;

                var createCopy = true;
                ModalService.edit(template, planCreateSuccess, planCreateFail, createCopy, template, $scope);

            }
        }
    }

    ////////////////////////////////////////////////////

    $scope.nudgeRoster = function(roster) {


        roster.nudging = true;

        //////////////////////////////////////////

        FluroContent.endpoint('assignments/nudge/roster/' + roster._id).save()
            .$promise
            .then(function(res) {
                roster.nudging = false;

                ////console.log('Nudged everyone!', res);
                if (res.success.length) {
                    Notifications.status(res.success.length + ' nudge notifications sent');
                } else {
                    Notifications.warning('No nudges were sent');
                }

            }, function(err) {
                roster.nudging = false;
                ////console.log('Error', err);

                Notifications.error(err);
            })
    }

    ////////////////////////////////////////////////////

    $scope.nudgeEvent = function(event) {


        event.nudging = true;

        //////////////////////////////////////////

        FluroContent.endpoint('assignments/nudge/event/' + event._id).save()
            .$promise
            .then(function(res) {
                event.nudging = false;

                ////console.log('Nudged everyone!', res);
                if (res.success.length) {
                    Notifications.status(res.success.length + ' nudge notifications sent');
                } else {
                    Notifications.warning('No nudges were sent');
                }

            }, function(err) {
                event.nudging = false;
                ////console.log('Error', err);

                Notifications.error(err);
            })
    }

    ////////////////////////////////////////////////////

    $scope.assignTeam = function(roster) {



        var modalSource = {};
        var params = {}

        ModalService.browse('team', modalSource, params).result.then(modalClosed, modalClosed)

        function modalClosed() {

            var selection = modalSource.items;
            if (!selection || !selection.length) {
                return;
            }

            roster.addingTeam = true;

            /////////////////////////////////////

            var teamPromises = _.map(selection, function(team) {
                return FluroContent.resource('team').get({
                        id: team._id,
                        allDefinitions: true,
                    })
                    .$promise
            });

            /////////////////////////////////////

            Promise.all(teamPromises)
            .then(function(teams) {


                    //console.log('Got the team', team);
                    var promises = _.chain(teams)
                        .map('assignments')
                        .flatten()
                        .map(function(assignment) {

                            return _.map(assignment.contacts, function(contact) {
                                return {
                                    title: assignment.title,
                                    contact: contact,
                                    contactName: (contact.title || contact.firstName + ' ' + contact.lastName),
                                    confirmationStatus: 'proposed', //'unknown',
                                    roster: roster._id,
                                    realms: roster.realms,
                                }
                            })
                        })
                        .flatten()
                        .map(function(assignment) {

                            var request = FluroContent.resource('assignment', true, true).save(assignment).$promise;


                            request.then(function(res) {
                                //Push it into the temporary assignments
                                // MatrixSelectService.selected.slot.assignments.push(res);
                                _.assign(assignment, res);

                                //console.log('ROSTER SLOTS', roster)
                                if (!roster.slots) {

                                }
                                // return rerender();

                            }, function(err) {
                                //console.log('Error', err);
                                // _.pull(MatrixSelectService.selected.slot.assignments, assignmentData);
                            });

                            return request;
                        })
                        .value();

                        console.log('PROMISES', promises)


                    $q.all(promises).then(function(newAssignments) {

                        roster.addingTeam = false;

                        _.each(newAssignments, function(assignment) {

                            var rosterSlotName = _.startCase(assignment.title);
                            var match = _.find(roster.slots, function(slot) {
                                return _.startCase(slot.title) == rosterSlotName;
                            })

                            if (!match) {
                                match = {
                                    title: rosterSlotName,
                                    assignments: []
                                }

                                if (!roster.slots) {
                                    roster.slots = [];
                                }
                                roster.slots.push(match);
                            }

                            //Add the assignment to the matching slot
                            match.assignments.push(assignment);

                            //console.log('PUSH INTO ROSTER', assignment, match, roster);

                        });

                        return columnsUpdated();
                    }, function(err) {
                        roster.addingTeam = false;
                        return columnsUpdated();
                    })

                })







        }
        /**
        ////////////////////////////////////

        $scope.selectSuggestion = function(contact) {



            var assignmentData = {
                title: MatrixSelectService.selected.slot.title,
                contact: contact,
                contactName: (contact.title || contact.firstName + ' ' + contact.lastName),
                confirmationStatus: 'proposed', //'unknown',
                roster: $scope.rosterID,
            }

            MatrixSelectService.selected.slot.assignments.push(assignmentData);

            /////////////////////////////////////////////////////

            //Get the realms
            var eventRealms = MatrixSelectService.selected.event.realms;
            var rosterRealms = MatrixSelectService.selected.roster.realms;

            if (rosterRealms) {
                assignmentData.realms = rosterRealms;
            } else {
                assignmentData.realms = eventRealms;
            }

            /////////////////////////////////////////////////////

            var request = FluroContent.resource('assignment', true, true).save(assignmentData).$promise;

            request.then(function(res) {
                //Push it into the temporary assignments
                // MatrixSelectService.selected.slot.assignments.push(res);
                _.assign(assignmentData, res);
                return rerender();

            }, function(err) {
                _.pull(MatrixSelectService.selected.slot.assignments, assignmentData);
            });

            /////////////////////////////////////////////////////


            //Retally the count
            retally();

            /////////////////////////////////////////////////////

            return updateSuggestions();
        }



        roster._type = 'roster';
        ModalService.edit(roster, complete);

        function complete(res) {
            //console.log('ASSIGN NEW', roster, res);

            if (!roster.slots) {
                roster.slots = [];
            }

            res.slots = _.map(res.slots, function(newSlot) {



                var merged = _.find(roster.slots, function(existingSlot) {
                    return (existingSlot.title.toLowerCase() == newSlot.title.toLowerCase());
                })

                //////////////////////////////

                var cleanAssignments = []
                cleanAssignments = cleanAssignments.concat(newSlot.assignments)

                if (merged) {
                    cleanAssignments = cleanAssignments.concat(merged.assignments)
                }

                //////////////////////////////

                newSlot.assignments = _.chain(cleanAssignments)
                    .flatten()
                    .uniq(function(assignment) {
                        return assignment._id
                    })
                    .filter(function(assignment) {
                        return assignment.status != 'archived';
                    })
                    .compact()
                    .value();

                //////////////////////////////

                return newSlot;
            });


            //console.log('CLEANED SLOTS', res.slots)

            //Now update our local object with the new roster
            _.assign(roster, res);
            return columnsUpdated();
        }
        /**/
    }

    ////////////////////////////////////////////////////

    $scope.eventReminderCount = function(event) {

        // console.log('EVENT', event)
        //Definition reminders doesn't exist yet
        var definitionReminders = _.get(event, 'definitionReminders') || [];
        var trackReminders = _.get(event, 'trackReminders') || [];
        var eventReminders = _.get(event, 'reminders') || [];
        return trackReminders.length + definitionReminders.length + eventReminders.length;
    }

    $scope.reminderCount = function(rosterType) {

        var defaultReminders = _.get(rosterType, 'defaultReminders') || [];
        var rosterReminders = _.get(rosterType, 'roster.reminders') || [];
        return defaultReminders.length + rosterReminders.length;
    }

    ////////////////////////////////////////////////////

    $scope.editRoster = function(roster) {
        roster._type = 'roster';
        ModalService.edit(roster, complete);

        function complete(res) {
            //console.log('ASSIGN NEW', roster, res);

            if (!roster.slots) {
                roster.slots = [];
            }

            res.slots = _.map(res.slots, function(newSlot) {



                var merged = _.find(roster.slots, function(existingSlot) {
                    return (existingSlot.title.toLowerCase() == newSlot.title.toLowerCase());
                })

                //////////////////////////////

                var cleanAssignments = []
                cleanAssignments = cleanAssignments.concat(newSlot.assignments)

                if (merged) {
                    cleanAssignments = cleanAssignments.concat(merged.assignments)
                }

                //////////////////////////////

                newSlot.assignments = _.chain(cleanAssignments)
                    .flatten()
                    .uniq(function(assignment) {
                        return assignment._id
                    })
                    .filter(function(assignment) {
                        return assignment.status != 'archived';
                    })
                    .compact()
                    .value();

                //////////////////////////////

                return newSlot;
            });


            //console.log('CLEANED SLOTS', res.slots)

            //Now update our local object with the new roster
            _.assign(roster, res);
            return columnsUpdated();
        }
    }


    ////////////////////////////////////////////////////

    $scope.editReminders = function(roster, defaultReminders) {


        var modalStatus = {};

        if (!defaultReminders) {
            defaultReminders = [];
        }


        //////////////////////////////////////////////////

        var modalInstance = $uibModal.open({
            template: '<roster-reminders-modal title="\'Default reminders\'" default-reminders="defaultReminders" ng-model="roster" processing="modalStatus.processing" close="closeModal" dismiss="dismiss"></roster-reminders-modal>',
            size: 'md',
            controller: function($scope) {

                var copy = {
                    reminders: roster.reminders.slice(),
                }

                $scope.roster = roster;
                $scope.defaultReminders = defaultReminders;

                console.log('DEFAULT REMINDERS', defaultReminders);

                $scope.modalStatus = {};



                $scope.dismiss = function() {

                    console.log('Dismiss')
                    _.assign(roster, copy);
                    $scope.$dismiss();
                }


                $scope.closeModal = function() {


                    if (modalStatus.processing) {
                        return;
                    }

                    modalStatus.processing = true;

                    var definedName = 'roster';
                    if (roster.definition && roster.definition.length) {
                        definedName = roster.definition;
                    }

                    ////////////////////////////////////////////

                    //We only need to update the reminders object
                    var updateDetails = {
                        reminders: roster.reminders
                    }

                    ////////////////////////////////////////////

                    var request = FluroContent.resource(definedName).update({
                        id: roster._id,
                    }, updateDetails, saveComplete, saveFailed);

                    ////////////////////////////////////////////

                    function saveComplete(res) {
                        modalStatus.processing = false;
                        // //console.log('Save Completed', res, modalInstance);
                        Notifications.status('Updated  \'' + roster.title + '\' reminders');

                        modalInstance.close();

                        //Reload the state
                        return columnsUpdated();
                    }


                    function saveFailed(err) {
                        modalStatus.processing = false;
                        //console.log('Save Error', err);
                        Notifications.error(err);
                    }
                }

            },
        });



        // roster._type = 'roster';
        // ModalService.edit(roster, complete);

        // function complete(res) {
        //     //console.log('ASSIGN NEW', roster, res);

        //     if (!roster.slots) {
        //         roster.slots = [];
        //     }

        //     res.slots = _.map(res.slots, function(newSlot) {



        //         var merged = _.find(roster.slots, function(existingSlot) {
        //             return (existingSlot.title.toLowerCase() == newSlot.title.toLowerCase());
        //         })

        //         //////////////////////////////

        //         var cleanAssignments = []
        //         cleanAssignments = cleanAssignments.concat(newSlot.assignments)

        //         if (merged) {
        //             cleanAssignments = cleanAssignments.concat(merged.assignments)
        //         }

        //         //////////////////////////////

        //         newSlot.assignments = _.chain(cleanAssignments)
        //             .flatten()
        //             .uniq(function(assignment) {
        //                 return assignment._id
        //             })
        //             .filter(function(assignment) {
        //                 return assignment.status != 'archived';
        //             })
        //             .compact()
        //             .value();

        //         //////////////////////////////

        //         return newSlot;
        //     });


        //     //console.log('CLEANED SLOTS', res.slots)

        //     //Now update our local object with the new roster
        //     _.assign(roster, res);
        //     return columnsUpdated();
        // }
    }


    ////////////////////////////////////////////////////

    $scope.editEventReminders = function(column) {


        var event = column.event;
        var rosters = column.rosters;


        var modalStatus = {};

        ///////////////////////////////////////





        ///////////////////////////////////////

        var reminderSlots = _.chain(rosters)
            .map('slots')
            .flatten()
            .compact()
            .uniq(function(slot) {
                return slot.title;
            })
            .value();

        //////////////////////////////////////////////////

        var modalInstance = $uibModal.open({
            template: '<roster-reminders-modal title="trackTitle" default-reminders="trackReminders" ng-model="model" processing="modalStatus.processing" close="closeModal" dismiss="dismiss"></roster-reminders-modal>',
            size: 'md',
            controller: function($scope) {
                // $scope.event = event;



                var copy = {
                    reminders: event.reminders.slice(),
                }

                $scope.trackReminders = _.get(event, 'trackReminders');



                if (event.track) {
                    $scope.trackTitle = event.track.title + ' - Event Track reminders';
                }



                $scope.dismiss = function() {
                    console.log('Dismiss')
                    _.assign(event, copy);
                    $scope.$dismiss();
                }



                $scope.model = {
                    event: event,
                    slots: reminderSlots,
                    reminders: event.reminders,
                }

                $scope.modalStatus = {};


                $scope.closeModal = function() {
                    if (modalStatus.processing) {
                        return;
                    }

                    modalStatus.processing = true;

                    var definedName = 'event';
                    if (event.definition && event.definition.length) {
                        definedName = event.definition;
                    }

                    ////////////////////////////////////////////

                    //We only need to update the reminders object
                    var updateDetails = {
                        reminders: event.reminders
                    }

                    ////////////////////////////////////////////

                    var request = FluroContent.resource(definedName).update({
                        id: event._id,
                    }, updateDetails, saveComplete, saveFailed);

                    ////////////////////////////////////////////

                    function saveComplete(res) {
                        modalStatus.processing = false;
                        // //console.log('Save Completed', res, modalInstance);
                        Notifications.status('Updated  \'' + event.title + '\' reminders');

                        modalInstance.close();

                        //Reload the state
                        return columnsUpdated();
                    }


                    function saveFailed(err) {
                        modalStatus.processing = false;
                        //console.log('Save Error', err);
                        Notifications.error(err);
                    }
                }

            },
        });



        // roster._type = 'roster';
        // ModalService.edit(roster, complete);

        // function complete(res) {
        //     //console.log('ASSIGN NEW', roster, res);

        //     if (!roster.slots) {
        //         roster.slots = [];
        //     }

        //     res.slots = _.map(res.slots, function(newSlot) {



        //         var merged = _.find(roster.slots, function(existingSlot) {
        //             return (existingSlot.title.toLowerCase() == newSlot.title.toLowerCase());
        //         })

        //         //////////////////////////////

        //         var cleanAssignments = []
        //         cleanAssignments = cleanAssignments.concat(newSlot.assignments)

        //         if (merged) {
        //             cleanAssignments = cleanAssignments.concat(merged.assignments)
        //         }

        //         //////////////////////////////

        //         newSlot.assignments = _.chain(cleanAssignments)
        //             .flatten()
        //             .uniq(function(assignment) {
        //                 return assignment._id
        //             })
        //             .filter(function(assignment) {
        //                 return assignment.status != 'archived';
        //             })
        //             .compact()
        //             .value();

        //         //////////////////////////////

        //         return newSlot;
        //     });


        //     //console.log('CLEANED SLOTS', res.slots)

        //     //Now update our local object with the new roster
        //     _.assign(roster, res);
        //     return columnsUpdated();
        // }
    }




    ////////////////////////////////////////////////////

    // $scope.editRoster = function(roster) {
    //     roster._type = 'roster';
    //     ModalService.edit(roster, complete);

    //     function complete(res) {
    //         _.assign(roster, res);
    //         return columnsUpdated();
    //     }
    // }


    ///////////////////////////////////

    $scope.editRosterDefaults = function(originalRosterDefinition) {


        var definitionName = originalRosterDefinition.definitionName;

        //Load the definition object
        FluroContent.endpoint('defined/' + definitionName, true, true)
            .get()
            .$promise
            .then(definitionLoaded, definitionFailed);

        //////////////////////////////////////////////////

        function definitionFailed(err) {
            //console.log('Definition failed!', err);
        }

        //////////////////////////////////////////////////

        function definitionLoaded(definition) {

            var modalStatus = {};

            //////////////////////////////////////////////////

            var modalInstance = $uibModal.open({
                template: '<default-slots-editor ng-model="definition" processing="modalStatus.processing" close="closeModal"></default-slots-editor>',
                size: 'md',
                controller: modalController,
            });

            //////////////////////////////////////////////////

            function modalController($scope) {
                $scope.definition = definition;
                $scope.modalStatus = modalStatus;

                // $scope.test = testEvent;
                // $scope.isModal =true;
                $scope.closeModal = function() {

                    //console.log('Close Modal')

                    if (modalStatus.processing) {
                        return;
                    }

                    modalStatus.processing = true;

                    var definedName = 'definition';
                    if (definition.definition) {
                        definedName = definition.definition;
                    }

                    ////////////////////////////////////////////

                    //We only need to update the data object
                    var updateDetails = {
                        data: definition.data,
                        weight: definition.weight,
                    }

                    ////////////////////////////////////////////

                    var request = FluroContent.resource(definedName).update({
                        id: definition._id,
                    }, updateDetails, saveComplete, saveFailed);

                    ////////////////////////////////////////////

                    function saveComplete(res) {
                        modalStatus.processing = false;
                        // //console.log('Save Completed', res, modalInstance);
                        Notifications.status('Updated  \'' + definition.title + '\'');

                        modalInstance.close();

                        //Reload the state

                        //Update the slots
                        originalRosterDefinition.originalReference.slots = res.data.slots;
                        return columnsUpdated();

                    }


                    function saveFailed(err) {
                        modalStatus.processing = false;
                        //console.log('Save Error', err);
                        Notifications.error(err);
                    }
                }

            }
        }
    }

    /**
    $scope.editRosterDefinition = function(rosterDefinition) {

        var object = {
            _type: 'definition',
            _id: rosterDefinition._id,
        }



        ModalService.edit(object, complete);

        function complete(res) {
            // return reloadAll();
            // //console.log('ASSIGN SLOTs', rosterDefinition.slots, res.data.slots);
            // _.assign(rosterDefinition.slots, res.data.slots);

            // rosterDefinition.title = 'UPDATED';
            rosterDefinition.originalReference.slots = res.data.slots;
            // 
            return columnsUpdated();

        }


        // //console.log('eDIT DEFINITION', rosterDefinition);
    }
    /**/

    ////////////////////////////////////////////////////

    $scope.editEvent = function(event) {

        event._type = 'event';
        ModalService.edit(event, complete);

        function complete(res) {

            //console.log('EVENT')

            _.assign(event, res);
            // return columnsUpdated();
            return reloadAll();
        }
    }

    function reloadAll() {
        //console.log('RELOAD ALL')
        MatrixRouteService.load($stateParams.events).then(function(columns) {
            //console.log('DONE');
            $scope.columns = columns;
        })
    }

    ////////////////////////////////////////////////////

    $scope.editPlan = function(plan) {

        plan._type = 'plan';
        ModalService.edit(plan, complete);

        function complete(res) {
            _.assign(plan, res);
            return columnsUpdated();
        }
    }

    /**

    $scope.allRosters = rosters;

    ////console.log('ALL ROSTERS', rosters);


    var eventIDs = _.map(events, '_id');
    $scope.events = events;
    $scope.assignments = assignments;
    $scope.rosterTypes = _.map(rosterTypes, function(rosterType) {
        var slots = _.get(rosterType, 'data.slots');
        var slotCount = 0;
        if (slots) {
            slotCount = slots.length;
        }

        rosterType.minRows = Math.max(slotCount, 1);
        return rosterType;
    });

    ////////////////////////////////////////////////////

    $scope.settings = {};

    ////////////////////////////////////////////////////

    $scope.context = {};

    ////////////////////////////////////////////////////

    $scope.rosterExists = function(event, rosterType) {

        if (!event) {
            return;
        }

        if (!rosterType) {
            return;
        }

        /////////////////////////////////////////

        var eventID = event;
        if (eventID._id) {
            eventID = eventID._id;
        }

        /////////////////////////////////////////

        var rosterDefinitionName = rosterType.definitionName;

        //Find a roster with the same definition name linked to the specified event
        var matchingRoster = _.find($scope.allRosters, function(roster) {
            return roster.definition == rosterDefinitionName && (roster.event == eventID);
        })

        return matchingRoster;

    }


    $scope.addRoster = function(column, rosterType) {

        var rosterDefinitionName = rosterType.definitionName;

        var newRoster = {
            _id: Math.random(),
            title: rosterType.title,
            definition: rosterDefinitionName,
            event: column.event._id,
        }

        $scope.allRosters.push(newRoster);

        if (!column.rosters[rosterDefinitionName]) {
            column.rosters[rosterDefinitionName] = newRoster; //{title:rosterType.title, rosters:[]}
        }

        // //Push the new roster into the set
        // column.rosters[rosterDefinitionName].rosters.push(newRoster);

        return render();

    }

    ////////////////////////////////////////////////////

    $scope.maxRosterCells = function(rosterType) {
        var rosterSlotCount = _.get(rosterType, 'data.slots.length');
    }

    ////////////////////////////////////////////////////


    $scope.getRosterHeight = function(rosterType) {
        return _.chain(events)
            .map(function(event) {
                var count = event.plans.length;
                return Math.max(0, count);
            })
            .max()
            .value();
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    $scope.removeEvent = function(event) {
        _.pull($scope.events, event);
        _.pull(eventIDs, event._id);
        refreshSlots();
    }

    ////////////////////////////////////////////////////


    FluroSocket.on('content.edit', socketUpdate);
    FluroSocket.on('content.create', socketUpdate);
    FluroSocket.on('content.delete', socketUpdate);
    FluroSocket.on('content.restore', socketUpdate);


    function socketUpdate(data) {

        //////console.log('SOCKET UPDATE', data);

        if (!data.item) {
            return;
        }

        //////////////////////////////////////////////////////

        if (data.item._type == 'assignment') {
            switch (data.key) {
                case 'content.create':
                case 'content.edit':
                case 'content.delete':
                case 'content.restore':
                    return reloadAssignments();
                    break;
                default:
                    break;
            }
        }
    }


    $scope.$on("$destroy", function() {
        FluroSocket.off('content.edit', socketUpdate);
        FluroSocket.off('content.create', socketUpdate);
        FluroSocket.off('content.delete', socketUpdate);
        FluroSocket.off('content.restore', socketUpdate);
    });

    ////////////////////////////////////////////////////


    function reloadAssignments() {

        if ($scope.settings.saving) {
            return;
        }

        //////console.log('Reload Assignments')
        MatrixRouteService.loadAssignments(eventIDs).then(function(assignments) {
            $scope.assignments = assignments;
            render();
        });
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////



    $scope.save = function() {

        $scope.settings.saving = true;
        socketEnabled = false;

        var assignments = $scope.allTemporary();

        //////console.log('SAVING')

        var promises = _.map(assignments, function(assignment) {

            assignment.processing = true;
            assignment.contact.title = 'saving'
            var request = FluroContent.resource('assignment').save(assignment).$promise;

            request.then(function(res) {


                assignment.processing = false;
                _.assign(assignment, res);
            }, function(err) {
                assignment.processing = false;
            })
        })

        ////////////////////////////////////////////////

        $q.all(promises).then(saveComplete, saveError);

        ////////////////////////////////////////////////

        function saveComplete(res) {
            $scope.settings.saving = false;
            Notifications.status(assignments.length + ' new assignments were created');
        }


        function saveError(err) {

            $scope.settings.saving = false;
            Notifications.warning(err);
        }

    }


    ////////////////////////////////////////////////////


    function render() {
        ////console.log('Render');

        ////////////////////////////////////////////////////

        //Map all assignments to rosters
        _.each($scope.allRosters, function(roster) {


            //Get all the assignments that match this roster
            var matchingAssignments = _.filter($scope.assignments, function(assignment) {
                //Get the rosterID
                var rosterID = assignment.roster;

                //If no roster id then ignore
                if (!rosterID) {
                    return;
                }

                //If the roster is populated strip
                //it back down to the _id
                if (rosterID._id) {
                    rosterID = rosterID._id;
                }

                //Return true if the roster id matches
                return rosterID == roster._id;
            })


            /////////////////////////////////////////////////////

            roster.assigned = _.chain(roster.slots)
                .map(function(slot) {

                    var fields = [
                        'title',
                        'minimum',
                        'maximum',
                        'preferCapabilities',
                        'requireCapabilities',
                    ]

                    var output = _.pick(slot, fields);

                    //Get all assignments that match this slot
                    output.assignments = _.filter(matchingAssignments, function(assignment) {
                        return (assignment.title.toLowerCase() == slot.title.toLowerCase());
                    })

                    ////console.log('Matching', output.title, output.assignments.length, $scope.assignments);

                    return output;
                })
                .value();

        });

        ////////////////////////////////////////////////////

        //Map the events as dates
        $scope.dates = _.chain(events)
            .reduce(function(results, event) {

                //Unique date key
                var date = new Date(event.startDate);
                date.setHours(0, 0, 0, 0);

                ////////////////////////////////////////////

                var dateKey = date.format('j M Y');

                //Get existing if this date is already in the set
                var existing = _.find(results, {
                    dateKey: dateKey
                });

                //If its not, then create the date for the set
                if (!existing) {
                    existing = {
                        dateKey: dateKey,
                        date: date,
                        events: [],
                    }

                    results.push(existing);
                }

                existing.events.push(event);
                return results;

            }, [])
            .map(function(date, key) {

                var even = (key % 2) == 0;

                if (date.events.length) {
                    date.events[0].first = true;
                    date.events[date.events.length - 1].last = true;
                }

                _.each(date.events, function(event) {
                    event.even = even;

                    var classes = [];

                    if (even) {
                        classes.push('even');
                    } else {
                        classes.push('odd');
                    }

                    if (event.last) {
                        classes.push('last');
                    }

                    if (event.first) {
                        classes.push('first');
                    }

                    event.class = classes.join(' ');

                    // ng-class="{even:event.even, last:event.last, first:event.first}"
                })

                return date;
            })
            .value();

        ///////////////////////////////////////////////////////////


        $scope.columns = _.chain(events)
            .map(function(event) {

                var eventID = event._id;
                var column = {
                    _id: eventID,
                    class: event.class,
                };

                ////////////////////////////////////////////////////

                //Add the event
                column.event = event;
                column.plans = event.plans;

                ////////////////////////////////////////////////////

                column.rosters = _.reduce(rosterTypes, function(set, definition) {

                    //Find the matching roster
                    var firstMatchingRoster = _.find($scope.allRosters, function(roster) {

                        var correctEvent = (roster.event == eventID);
                        var correctType = (roster.definition == definition.definitionName);

                        return correctEvent && correctType;
                    })

                    set[definition.definitionName] = firstMatchingRoster;

                    return set;

                    /**
                    //Create a new set for the roster
                    var rosterSet =  {
                        title:definition.title,
                    }

                    //Get all rosters that match this roster type
                    rosterSet.rosters =  _.chain($scope.allRosters)
                    .filter(function(roster) {
                        var correctEvent = (roster.event == eventID);
                        var correctType = (roster.definition == definition.definitionName);
                        return correctEvent && correctType;
                    })
                    .map(function(roster) {

                        rosterSet.slots = 

                        return roster;
                    })
                    .value();

                    // ///////////////////////////////////////////////////////

                    // //Get all the available defined in the original definition slots
                    // var availableSlots = _.get(definition, 'data.slots');
                
                    // //Get all the existing slots
                    // var existingSlots = _.chain(rosterSet.rosters)
                    // .map(function(roster) {
                    //     return roster.slots;
                    // })
                    // .flatten()
                    // .value();

                    ///////////////////////////////////////////////////////


                    //Add the set as a key
                    set[definition.definitionName] = rosterSet;

                    //Return the set
                    return set;
                    

                }, {})

                ////////////////////////////////////////////////////
                // ng-class="{even:column.even, last:column.last, first:column.first}"

                return column;
            })
            .value();


        /**

                //Get the number of the most plans on a single event
                $scope.maxPlanCount = _.chain(events)
                        .map(function(event) {
                            var count = event.plans.length;
                            return Math.max(0, count);
                        })
                        .max()
                        .value();


                ////////////////////////////////////////////

                //Get all of the titles of assignments we know about
                var assignmentSlotNames = _.map(assignments, 'title');
                
                ////////////////////////////////////////////////////

                //Get all of the explicitly set assignments from 
                //the slots set on an event
                var eventSlotNames = _.chain(events)
                    .map(function(event) {
                        return event.assignmentSlots
                    })
                    .flatten()
                    .compact()
                    .map(function(slot) {
                        return slot.title;
                    })
                    .value();

                ////////////////////////////////////////////////////

                //Combine all the possible slot names together
                var allNames = [].concat(assignmentSlotNames, eventSlotNames);

                ////////////////////////////////////////////////////

                //Get all the slot names
                var allSlots = _.chain(allNames)
                    // .compact()
                    .uniq(function(slotName) {
                        return slotName.toLowerCase();
                    })
                    .sortBy(function(slotName) {
                        return slotName;
                    })
                    .map(function(name) {
                        return {
                            title: name
                        };
                    })
                    .value();

                $scope.assignmentSlots = allSlots;

                ////////////////////////////////////////////////////

                /**
                $scope.renderEvents = _.chain(events)
                .map(function(event) {


                    var eventID = event._id;

                    ////////////////////////////

                    var output = {
                        _id:eventID,
                        event:event,
                        startDate:event.startDate,
                        endDate:event.endDate,
                        realms:event.realms,
                        event:event.event,
                        first:event.first,
                        last:event.last,
                    }

                    ////////////////////////////

                    output.slots = _.chain(allSlots)
                    .map(function(slot) {


                        var slotTitle = String(slot.title).toLowerCase();

                        ///////////////////////////////////////////////////////

                        var matchedAssignments = _.filter(assignments, function(assignment) {

                            var assignmentTitle = String(assignment.title).toLowerCase();
                            var assignmentEvent = assignment.event;
                            if(assignmentEvent._id) {
                                assignmentEvent = assignmentEvent._id;
                            }

                            var sameAssignment = assignmentTitle == slotTitle;
                            var sameEvent = assignmentEvent == eventID;

                            return (sameAssignment && sameEvent);

                        });

                        ///////////////////////////////////////////////////////

                        return {
                            title:slot.title,
                            assignments:matchedAssignments,
                        }
                    })
                    .value();

                    ////////////////////////////

                    return output;
                })
                .value()
                
    }

    ////////////////////////////////////////////////////

    render();

    // refreshSlots();

    ////////////////////////////////////////////////////

    $scope.slotCount = function(slot, verbose) {
        return _.chain(events)
            .map(function(event) {

                var eventID = event._id;


                var matches = _.filter($scope.assignments, function(assignment) {

                    var assignmentEventID = assignment.event;
                    if (assignmentEventID._id) {
                        assignmentEventID = assignmentEventID._id;
                    }

                    var correctID = (assignmentEventID == eventID);
                    var correctAssignment = (assignment.title.toLowerCase() == slot.title.toLowerCase());

                    return correctID && correctAssignment;

                })

                var value = matches.length;
                value--;

                // if(verbose) {
                // // if(String(slot.titleString).toLowerCase() == String('Bass guitarist' ).toLowerCase()) {

                //     ////console.log('VALUE', slot.title, value, matches.length);
                // }

                return Math.max(0, value);
            })
            .max()
            .value();
    }

    ////////////////////////////////////////////////////

    $scope.isSelected = function(slot, event) {
        return _.get($scope.context, 'item.slot') == slot && _.get($scope.context, 'item.event') == event;
    }

    ////////////////////////////////////////////////////

    $scope.getSlotAssignments = function(slot, event) {

        // ////console.log('SLOT', slot);

        var eventID = event;
        if (eventID && eventID._id) {
            eventID = eventID._id;
        }

        //////////////////////////////////////

        return _.chain($scope.assignments)
            .filter(function(assignment) {


                var assignmentEventID = assignment.event;
                if (assignmentEventID && assignmentEventID._id) {
                    assignmentEventID = assignmentEventID._id;
                }

                ///////////////////////////////////////

                var correctSlot = String(assignment.title).toLowerCase() == String(slot.title).toLowerCase();
                var correctEvent = (assignmentEventID == eventID);



                return correctSlot && correctEvent;
            })
            .sortBy(function(assignment) {
                return assignment.contact.title;
            })
            .sortBy(function(assignment) {
                return assignment.confirmationStatus;
            })
            .value();
    }

    ////////////////////////////////////////////////////



    ////////////////////////////////////////////////////

    $scope.onDrop = function(item, slot, event) {
        // ////////console.log('ON DROP', item, slot, event);

        /////////////////////////////////////////

        switch (item._type) {
            case 'contact':
                return createContactAssignment(item, slot, event);
                break;
            case 'team':
                return createTeamAssignments(item, slot, event);
                break;
        }
    }

    ////////////////////////////////////////////////////

    function createTeamAssignments(item, slot, event) {

        ////console.log('ITEMS', item);

        /**
        var assignment = {
            title: slot.title,
            _type: 'assignment',
            event: event._id,
            contact: item,
            confirmationStatus: 'temporary',
            realms: event.realms,
        }

        //////////////////////////////////////

        var contactID = item;
        if (contactID._id) {
            contactID = contactID._id;
        }

        //////////////////////////////////////

        var eventID = event;
        if (eventID._id) {
            eventID = eventID._id;
        }

        //////////////////////////////////////

        var title = slot.title;

        //////////////////////////////////////

        var match = _.some($scope.assignments, function(existing) {

            var existingContactID = existing.contact;
            if (existingContactID && existingContactID._id) {
                existingContactID = existingContactID._id;
            }

            ////////////////////////////

            var existingEventID = existing.event;
            if (existingEventID && existingEventID._id) {
                existingEventID = existingEventID._id;
            }

            var existingTitle = existing.title;

            ////////////////////////////

            var sameContact = (contactID == existingContactID);
            var sameEvent = (eventID == existingEventID);
            var sameAssignment = (title.toLowerCase() == existingTitle.toLowerCase());

            return (sameContact && sameEvent && sameAssignment);
        });

        //////////////////////////////////////

        //If this contact is already assigned here
        //then stop and return
        if (match) {
            return;
        }

        //////////////////////////////////////

        //Push it into the list
        $scope.assignments.push(assignment);

       
    }

    ////////////////////////////////////////////////////

    $scope.editSlot = function(slot, event) {

        // ////console.log('CONTEXT', event, slot)

        $scope.context.item = {
            slot: slot,
            event: event,
        }
    }

    $scope.close = function() {
        $scope.context = {};
    }


    ////////////////////////////////////////////////////

    function createContactAssignment(item, slot, event) {

        var assignment = {
            title: slot.title,
            _type: 'assignment',
            event: event._id,
            contact: item,
            confirmationStatus: 'temporary',
            realms: event.realms,
        }

        //////////////////////////////////////

        var contactID = item;
        if (contactID._id) {
            contactID = contactID._id;
        }

        //////////////////////////////////////

        var eventID = event;
        if (eventID._id) {
            eventID = eventID._id;
        }

        //////////////////////////////////////

        var title = slot.title;

        //////////////////////////////////////

        var match = _.some($scope.assignments, function(existing) {

            var existingContactID = existing.contact;
            if (existingContactID && existingContactID._id) {
                existingContactID = existingContactID._id;
            }

            ////////////////////////////

            var existingEventID = existing.event;
            if (existingEventID && existingEventID._id) {
                existingEventID = existingEventID._id;
            }

            var existingTitle = existing.title;

            ////////////////////////////

            var sameContact = (contactID == existingContactID);
            var sameEvent = (eventID == existingEventID);
            var sameAssignment = (title.toLowerCase() == existingTitle.toLowerCase());

            return (sameContact && sameEvent && sameAssignment);
        });

        //////////////////////////////////////

        //If this contact is already assigned here
        //then stop and return
        if (match) {
            return;
        }

        //////////////////////////////////////

        //Push it into the list
        $scope.assignments.push(assignment);
    }


    ////////////////////////////////////////////////////

    $scope.allTemporary = function() {
        return _.chain($scope.assignments)
            .compact()
            .filter(function(assignment) {
                ////////console.log('TESTING', assignment)
                return assignment.confirmationStatus == 'temporary';
            })
            .value();
    }

    ////////////////////////////////////////////////////

    $scope.editAssignment = function(assignment) {

        if (!assignment._id) {
            return;
        }

        ////////console.log('Edit Assignment', assignment);
        assignment._type = 'assignment';

        ModalService.edit(assignment);
    }

    /**/


}