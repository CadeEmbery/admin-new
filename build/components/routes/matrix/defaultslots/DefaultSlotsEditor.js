app.directive('defaultSlotsEditor', function(){
	// Runs during compile
	return {
		// name: '',
		// priority: 1,
		// terminal: true,
        replace:true,
		scope: {
			model:'=ngModel',
			close: '=close',
			processing:'=',
		},
		controller: 'DefaultSlotsEditorController',
		// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
		// restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		// template: '',
		templateUrl: 'routes/matrix/defaultslots/defaultslots.html',
		// replace: true,
		// transclude: true,
		// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
		// link: function($scope, iElm, iAttrs, controller) {
			
		// }
	};
});


app.controller('DefaultSlotsEditorController', function($scope, $uibModal) {


$scope.processing = false;



	////////////////////////////////////////////////////

   	function removeSlot(slot) {
    	return _.pull($scope.model.data.slots, slot);
    }

        ////////////////////////////////////////////////////

    $scope.editSlot = function(slot) {

        var modalInstance = $uibModal.open({
            template: '<default-roster-slot></default-roster-slot>',
            size: 'md',
            backdrop:'static',
            controller: function($scope) {
                $scope.slot = slot;
                $scope.remove = function(slot) {
                	removeSlot(slot);
                	$scope.$close();
                }
            }
        });

    }

    

    ////////////////////////////////////////////////////

    $scope.createSlot = function() {



        //Check if a slots array is created
        var slots = _.get($scope.model, 'data.slots');
        if (!slots) {
            if(!$scope.model.data) {
                $scope.model.data = {}
            }

            $scope.model.data.slots = [];
        }

        ////////////////////////////////////////////////////

        var slot = {
            title: '',
            minimum: 1,
            maximum: 0,
            requireCapabilities: [],
            preferCapabilities: [],
        }

        ////////////////////////////////////////////////////

        $scope.model.data.slots.push(slot);

        ////////////////////////////////////////////////////

        $scope.editSlot(slot);

    }


})