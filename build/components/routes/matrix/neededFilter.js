app.filter('needed', function() {

    return function(slot) {

        if (!slot) {
            return;
        }

        var count = 0;

        if (slot.assignments) {
            count = slot.assignments.length;
        }

        var minimum = parseInt(slot.minimum);
        var maximum = parseInt(slot.maximum);

        if (minimum && count < minimum) {
            return minimum + " needed for '" + slot.title + "'";
        }

        if (maximum && count > maximum) {
            var over = count - maximum;
            return over + ' too many';
        }

        return;
    }
});