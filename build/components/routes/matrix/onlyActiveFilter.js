app.filter('onlyActive', function() {

    return function(items) {

        return _.filter(items, function(item) {
            
            switch(item.status) {
                case 'archived':
                    return false;
                break;
            }

            return true;
        })
    }
});

//////////////////////////////////////////////////////////////////


app.filter('notArchived', function() {

    return function(items) {
        return _.filter(items, function(item) {
           return item.status  != 'archived';
        })
    }
});