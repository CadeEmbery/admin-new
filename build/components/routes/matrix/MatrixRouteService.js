app.service('MatrixRouteService', function($q, Fluro, FluroContent, $http, $timeout) {

    var service = {};

    ////////////////////////////////////////////


    service.load = function(eventIDs) {

        var deferred = $q.defer();

        ///////////////////////////////

        $http.post(Fluro.apiURL + '/matrix/events', {
            events: eventIDs
        }).then(function(res) {

            console.log('COLUMNs', res.data);
            deferred.resolve(res.data);
        }, function(err) {
            deferred.reject(err);
        });

        ///////////////////////////////

        return deferred.promise;
    }

    ////////////////////////////////////////////////////////////////

    service.mapAssignmentsByStatus = function(assignments) {

        return _.reduce(assignments, function(results, assignment) {

            var set = results[assignment.confirmationStatus];

            if (!set) {

                var style = 'btn-default';

                switch (assignment.confirmationStatus) {
                    case 'denied':
                        style = 'brand-danger';
                        break;
                    case 'confirmed':
                        style = 'brand-success';
                        break;
                    case 'proposed':
                        style = 'brand-primary';
                        break;
                }

                set = {
                    status: assignment.confirmationStatus,
                    assignments: [],
                    style: style,
                }

                results[assignment.confirmationStatus] = set;
            }

            //Push the assignment
            set.assignments.push(assignment);

            return results;
        }, {});
    }


    ////////////////////////////////////////////////////

    service.archiveAssignment = function(assignment) {

        console.log('Archive the assignment please', assignment);

        return FluroContent.resource('assignment/' + assignment._id)
            .update({
                status: 'archived',
            })
            .$promise;
    }



    ////////////////////////////////////////////////////

    service.swap = function(assignment) {

        // console.log('Archive the assignment please', assignment);

        // return FluroContent.resource('assignment/' + assignment._id)
        //     .update({
        //         status: 'archived',
        //     })
        //     .$promise;
    }



    //////////////////////////////////////////////////////

    service.retrieveAssignmentsFromColumn = function(column) {

        return _.chain(column.rosters)
            .map(service.retrieveSlotsFromColumn)
            .map(function(slotObject) {
                return slotObject.slots;
            })
            .flatten()
            .compact
            .map(function(slot) {
                return slot.assignments;
            })
            .flattenDeep()
            .compact()
            .value();
    }


    //////////////////////////////////////////////////////

    service.retrieveSlotsFromColumn = function(column) {

        return _.chain(column.rosters)
            .map(function(rosterType) {
                return {
                    rosterTitle:rosterType.title,
                    rosterName:rosterType.definitionName,
                    slots:_.get(rosterType, 'roster.slots')
                }
            })
            .flattenDeep()
            .compact()
            .value();
    }

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////
    ////////////////////////////////////////////
    ////////////////////////////////////////////
    ////////////////////////////////////////////

    return service;
})