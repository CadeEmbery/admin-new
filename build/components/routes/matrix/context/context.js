app.directive('context', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'routes/matrix/context/context.html',
        controller: 'MatrixContextController',
        scope: true,
        // link: function($scope, $element, $attrs) {
        //     $scope.$field = $element.find('input');
        //     //console.log('GOT SEARCH FIELD', $scope.$field);
        // }
    };
});

//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////

app.service('MatrixSelectService', function() {

    var service = {
        selected: {},
    };

    /////////////////////////////////

    service.deselect = function() {
        service.selected = {};
    }

    /////////////////////////////////

    return service;
})


//////////////////////////////////////////////////

app.filter('conflictSummary', function() {

    return function(conflicts) {

        if (!conflicts || !conflicts.length) {
            return;
        }

        if (conflicts.length > 1) {
            return 'Rostered ' + conflicts.length + ' other positions';
        } else {
            return '\'' + conflicts[0].title + '\' at ' + new Date(conflicts[0].event.startDate).format('g:ia') + ' ' + conflicts[0].event.title;
        }



    }
})

//////////////////////////////////////////////////

app.filter('unavailableSummary', function($filter) {

    return function(contact) {

        if (!contact) {
            return;
        }

        /////////////////////////////////////////

        //If there are rejected assignments 
        if (contact.assignments && contact.assignments.length) {

            //If there are more than one rejected assi
            if (contact.assignments.length > 1) {

                //Get a summary of what was rejected
                var names = _.map(contact.assignments, function(assignment) {
                        return assignment.title;
                    })
                    .join(', ');

                return 'Marked unavailable for ' + names;
            } else {
                var firstAssignment = contact.assignments[0];

                if (firstAssignment.description && firstAssignment.description.length) {
                    return firstAssignment.description;
                } else {
                    return 'Marked unavailable for ' + firstAssignment.title;
                }
            }
        }

        /////////////////////////////////////////

        if (contact.periods) {

            var dates = _.map(contact.periods, function(period) {

                    var startDate = new Date(period.startDate);
                    var endDate = new Date(period.endDate);
                    var difference = (endDate - startDate);

                    //If it's all day
                    if (difference == 86399999) {
                        return 'All day';
                    }

                    // //console.log('DIFFERENCE', difference);

                    return $filter('readableDate')(period)
                })
                .join(', ');

            return 'Away ' + dates;
        }




    }
})

//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////

app.controller('MatrixContextController', function($scope, $q, $filter, Notifications, UnavailableAssignmentService, $rootScope, Fluro, $http, ModalService, MatrixSelectService, FluroContent) {

    $scope.service = MatrixSelectService;
    $scope.search = {};

    ////////////////////////////////////////////////////////////////////////

    $scope.grouped = {};

    ////////////////////////////////////////////////////////////////////////

    function rerender() {

        return $scope.$emit('matrix-rerender');
    }

    ////////////////////////////////////////////////////////////////////////

    function retally() {

        //console.log('Retally')
        return $scope.$emit('matrix-tally');
    }


    ////////////////////////////////////////////////////////////////////////

    $scope.$watch('service.selected', update)
    // $scope.$watch('service.selected.event', update)
    // $scope.$watch('service.selected.roster', update)

    ////////////////////////////////////////////////////////////////////////

    var promiseCache = {};

    ////////////////////////////////////////////////////////////////////////

    function update() {

        var field = angular.element('input#matrix-search');
        field.focus();

        var selected = MatrixSelectService.selected;

        ////////////////////////////////////
        ////////////////////////////////////

        //Reset all the options
        $scope.suggestions = [];
        $scope.unavailable = [];
        $scope.conflicts = [];
        $scope.unqualified = [];
        $scope.grouped = {};
        $scope.class = null;

        ////////////////////////////////////
        ////////////////////////////////////

        $scope.search.items = [];

        ////////////////////////////////////

        if (!selected || !selected.event || !selected.slot) {
            return;
        }


        ////////////////////////////////////

        var slot = selected.slot;
        var event = selected.event;
        var roster = selected.roster;

        ////////////////////////////////////
        ////////////////////////////////////
        ////////////////////////////////////

        groupAssignmentsByConfirmationStatus();

        ////////////////////////////////////
        ////////////////////////////////////
        ////////////////////////////////////

        var count = 0;

        if (slot.assignments) {
            count = _.filter(slot.assignments, function(assignment) {
                return assignment.confirmationStatus != 'denied';
            }).length;
        }


        if (count) {
            $scope.tabTitle = count + ' Rostered';
        } else {
            $scope.tabTitle = 'Roster';
        }

        var minimum = parseInt(slot.minimum);
        var maximum = parseInt(slot.maximum);

        var allConfirmed = _.every(slot.assignments, function(assignment) {
            return assignment.confirmationStatus != 'confirmed';
        })

        if (allConfirmed) {
            $scope.class = 'confirmed';
        } else {
            $scope.class = null;
        }

        ////////////////////////////////////

        if (minimum && count < minimum) {
            $scope.class = 'warning';
        }

        if (maximum && count > maximum) {
            $scope.class = 'warning';
        }


        return reloadSuggestions();

    }

    ////////////////////////////////////////////////////////////////////////

    function reloadSuggestions() {

        clearSuggestions()
        var selected = MatrixSelectService.selected;

        ////////////////////////////////////////////////////////////////////////

        if (!selected || !selected.event || !selected.slot) {
            return;
        }

        ////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////

        var eventID = selected.event;
        var rosterID = selected.roster;

        if (eventID && eventID._id) {
            eventID = eventID._id;
        }

        if (rosterID && rosterID._id) {
            rosterID = rosterID._id;
        }

        $scope.rosterID = rosterID;
        $scope.eventID = eventID;

        ////////////////////////////////////////////////////////////////////////

        //Send the criteria to the server
        var criteria = {
            title: selected.slot.title,
            event: eventID,
            roster: rosterID,
        }





        ////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////

        var key = String(criteria.title) + String(criteria.event) + String(criteria.roster);
        var request;

        if (promiseCache[key]) {
            request = promiseCache[key];
        } else {
            ////console.log('NEW SUGGESTION REQUEST', criteria)
            $scope.service.processing = true;

            request = $http.post(Fluro.apiURL + '/matrix/suggest?limit=3', criteria, {
                ignoreLoadingBar: true
            });
        }

        ////////////////////////////////////

        //Listen for the promise
        request.then(suggestionsFound, clearSuggestions);

        ////////////////////////////////////

        function suggestionsFound(res) {

            $scope.service.processing = false;
            $scope.suggestions = res.data.suggested;
            $scope.unavailable = res.data.unavailable;
            $scope.conflicts = res.data.conflicts;
            $scope.unqualified = res.data.unqualified;
            $scope.requirements = res.data.requirements;

            ////////////////////////////////////////////

            return updateSuggestions();

        }

        function clearSuggestions(err) {
            $scope.service.processing = false;
            $scope.suggestions = [];
            $scope.unavailable = [];
            $scope.conflicts = [];
            $scope.unqualified = [];
            $scope.requirements = null;

            ////////////////////////////////////////////

            return updateSuggestions();
        }
    }


    ////////////////////////////////////////////////////////////////////////


    $scope.searchContacts = function(val) {

        $scope.search.processing = true;

        ////////////////////////////////////////////////////////////////////////

        var deferred = $q.defer()

        ////////////////////////////////////////////////////////////////////////

        var request = FluroContent.endpoint('content/contact/search/' + val, true, true).query({
                limit: 5,
            })
            .$promise;

        ////////////////////////////////////////////////////////////////////////

        request.then(searchResultsLoaded, searchError)

        ////////////////////////////////////////

        function searchError(err) {
            $scope.search.processing = false;
            $scope.search.items = [];

            return deferred.reject(err);
        }

        ////////////////////////////////////////

        function searchResultsLoaded(results) {

            $scope.search.processing = false;
            $scope.search.items = _.reduce(results, function(set, item) {

                var contactID = item._id;


                var assignments = _.get(MatrixSelectService, 'selected.slot.assignments');

                var exists = _.some(assignments, function(assignment) {

                    var assignmentContactID = assignment.contact;
                    if (assignmentContactID._id) {
                        assignmentContactID = assignmentContactID._id;
                    }

                    return assignmentContactID == contactID;
                });

                if (!exists) {
                    set.push(item);
                }

                return set;

            }, []);


            return deferred.resolve($scope.search.items);

        }

        return deferred.promise;
    }

    ////////////////////////////////////////////////////////////////////////

    $scope.createContact = function() {

        var defaultRealms = _.get(MatrixSelectService, 'selected.event.realms');

        ///////////////////////////////////////////////////

        var params = {
            template: {
                firstName: $scope.search.terms,
                realms: defaultRealms,
            }
        }

        ///////////////////////////////////////////////////

        //Add him on in
        ModalService.create('contact', params, function(contact) {
            // //console.log('CrATED', contact);
            $scope.search = {};
            $scope.selectSuggestion(contact);
        }, function(err) {
            // //console.log('ERROR', err);
        })

    }

    ////////////////////////////////////////////////////////////////////////

    $scope.addFromSearch = function(contact) {
        $scope.selectSuggestion(contact);
        $scope.search = {};
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    $scope.getTooltip = function(contact) {

        var conflicts = UnavailableAssignmentService.conflictSummary(MatrixSelectService.selected.event, contact);
        var denied = UnavailableAssignmentService.deniedSummary(contact);
        var periods = UnavailableAssignmentService.periodSummary(contact);

        var string;

        ////////////////////////////

        if (periods) {
            if (!string) {
                string = '';
            }
            string += '<div style="padding: 5px 0; border-top: 2px solid #fff;"><h6 class="text-uppercase">Away</h6><p>' + periods + '</p></div>';
        }

        if (conflicts) {
            if (!string) {
                string = '';
            }
            string += '<div style="padding: 5px 0; border-top: 2px solid #fff;"><h6 class="text-uppercase">Already rostered</h6><p>' + conflicts + '</p></div>';
        }

        if (denied) {
            if (!string) {
                string = '';
            }
            string += '<div style="padding: 5px 0; border-top: 2px solid #fff;"><h6 class="text-uppercase">Marked Unavailable</h6><p>' + denied + '</p></div>';
        }

        if (!string) {
            return;
        }

        string = '<div class="text-left tooltip-block"><h5 class="text-capitalize title">' + contact.title + '</h5>' + string + '</div>';

        ////////////////////////////


        return string;



    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    $scope.suggestionTooltip = function(contact) {

        var event = _.get(MatrixSelectService, 'selected.event');
        var slot = _.get(MatrixSelectService, 'selected.slot');

        if (!event) {
            return;
        }

        var startDate = new Date(event.startDate);
        var endDate = new Date(event.endDate);
        var string;



        ////////////////////////////



        if (contact.previous) {

            if (!string) {
                string = '';
            }

            string += '<div class="tooltip-block border-top"><strong class="text-uppercase small">Prior Assignment</strong><br/>'
            var assignmentDate = new Date(contact.previous.endDate);
            var measure = moment(endDate).from(assignmentDate, true);
            string += '<div><strong>' + contact.previous.assignmentName + '</strong> at <strong>' + contact.previous.title + '</strong> ending <strong>' + measure + ' before</strong> this event</div>';
            string += '</div>'
        }




        ////////////////////////////

        if (contact.next) {

            if (!string) {
                string = '';
            }

            string += '<div class="tooltip-block border-top" style="padding-bottom:5px;"><strong class="text-uppercase small">Next Assignment</strong><br/>'

            var assignmentDate = new Date(contact.next.startDate);
            var measure = moment(startDate).from(assignmentDate, true);
            // Vocalist at Sunday Service 2 hours after this event
            string += '<div><strong>' + contact.next.assignmentName + '</strong> at <strong>' + contact.next.title + '</strong> starting <strong>' + measure + ' after</strong> this event ends</div>';
            string += '</div>'
        }


        ////////////////////////////

        if (contact.teams) {
            if (!string) {
                string = '';
            }

            string += '<div class="tooltip-block border-top"><strong class="text-uppercase">On Team</strong><br/>'
            _.each(contact.teams, function(teamName) {
                string += '<div><strong>' + slot.title + '</strong> in <strong>' + teamName + '</strong></div>';
            })
            string += '</div>'
        }


        ////////////////////////////

        if (contact.capabilities) {
            if (!string) {
                string = '';
            }

            string += '<div class="tooltip-block border-top"><strong class="text-uppercase">Capabilities</strong><br/>'
            string += _.map(contact.capabilities, 'title').join(', ');
            string += '</div>'
        }


        ////////////////////////////

        if (contact.tags) {
            if (!string) {
                string = '';
            }

            string += '<div class="tooltip-block border-top"><strong>Tagged</strong><br/>'
            string += _.map(contact.tags, 'title').join(', ');
            string += '</div>'
        }


        ////////////////////////////

        // if (contact.next) {
        //     if (!string) {
        //         string = '';
        //     }

        //     var measure = moment(date1).from(date2, true);
        //     var slotName = _.get(MatrixSelectService, 'selected.slot.title');
        //     if(contact.assignmentName == slotName) {
        //         string += 'Assigned again ' + measure + ' later at ' + contact.next.title;
        //     } else {
        //         string += 'Assigned as \'' + contact.next.assignmentName + '\' ' + measure + ' later at ' + contact.next.title;
        //     }

        //     // '<div style="padding: 5px 0; border-top: 2px solid #fff;"><h6 class="text-uppercase">Away</h6><p>' + periods + '</p></div>';
        // }

        if (!string) {
            return;
        }

        // var realmString = '<div>' + _.map(contact.realms, function(realm) {
        //     return '<span class="inline-tag" style="background-color:'+realm.bgColor+'; color:'+realm.color+';" >'+realm.title+'</span>';
        // }).join('') + '</div>';

        var image = '<div class="avatar"><img class="img-circle" src="' + $rootScope.contactAvatarURL(contact._id) + '"/></div>';

        string = '<div class="text-left tooltip-block"><div class="flex-row">' + image + '<div class="info"><h5 class="text-capitalize title">' + contact.title + '</h5>' + '' + '</div></div>' + string + '</div>';

        ////////////////////////////


        return '<div style="max-width:250px">' + string + '</div>';



    }


    ////////////////////////////////////////////////////

    $scope.editSuggestedContact = function(contact) {

        //Copy the object
        var itemObject = angular.copy(contact);

        //Add the extra meta details we need to open the 
        //editing panel
        itemObject._type = 'contact';

        ///////////////////////////////////////////////////
        ///////////////////////////////////////////////////
        ///////////////////////////////////////////////////

        ModalService.edit(itemObject, editComplete);

        ///////////////////////////////////////////////////

        function editComplete(res) {
            ////console.log('Contact Updated')
            return reloadSuggestions();
        }
    }

    ////////////////////////////////////////////////////

    //Function for editing an assignment
    $scope.editAssignment = function(assignment) {

        if (assignment.confirmationStatus == 'temporary') {
            return;
        }


        ///////////////////////////////////////////////////
        ///////////////////////////////////////////////////

        //Copy the object
        var itemObject = angular.copy(assignment);

        //Add the extra meta details we need to open the 
        //editing panel
        itemObject._type = 'assignment';

        ///////////////////////////////////////////////////
        ///////////////////////////////////////////////////

        var eventRealms = _.get(MatrixSelectService, 'selected.event.realms');
        var rosterRealms = _.get(MatrixSelectService, 'selected.roster.realms');
        var assignmentRealms = assignment.realms;

        var realms = [];
        realms = realms.concat(eventRealms, rosterRealms, assignmentRealms);

        //Mash all the realms we know about together
        itemObject.realms = _.chain(realms)
            .compact()
            .flatten()
            .value();

        ///////////////////////////////////////////////////
        ///////////////////////////////////////////////////
        ///////////////////////////////////////////////////

        ModalService.edit(itemObject, editComplete);

        ///////////////////////////////////////////////////

        function editComplete(res) {

            //Find the assignment update it with an _id
            assignment._id = res._id

            //Update the status of the assignment so it removes from the list
            _.assign(assignment, res);

            //rerender

            return updateSuggestions();
        }

    }

    ////////////////////////////////////////////////////

    //Function for editing an assignment
    ////////////////////////////////////////////////////

    $scope.saveProposed = function(assignment) {

        //Set the assignment to 'archived'


        console.log('SCOPE', $scope, MatrixSelectService)

        FluroContent.resource('assignment/' + assignment._id, true, true).update({
                confirmationStatus: 'unknown',
            })
            .$promise
            .then(function(res) {
                assignment.confirmationStatus = res.confirmationStatus
                groupAssignmentsByConfirmationStatus();
            }, function(err) {
                groupAssignmentsByConfirmationStatus();
            })
    }


    $scope.saveAll = function() {

        //Get all proposed
        var proposedAssignments = _.get($scope.grouped, 'proposed');

        //Map each proposed to a promise
        var promises = _.map(proposedAssignments, function(assignment) {
            //Set the assignment to 'archived'
            return FluroContent.resource('assignment/' + assignment._id, true, true).update({
                    confirmationStatus: 'unknown',
                })
                .$promise
                .then(function(res) {
                    assignment.confirmationStatus = res.confirmationStatus
                    // assignment.confirmationStatus = 'unknown';
                })
        })

        //Update our list once all promises are complete
        $q.all(promises).then(groupAssignmentsByConfirmationStatus);


    }


    ////////////////////////////////////////////////////

    $scope.removeAssignment = function(assignment) {

        if (assignment.confirmationStatus == 'temporary') {
            // _.pull($scope.item.temporaryAssignments, assignment);
            // refreshList();
            return;
        }

        /////////////////////////////////////////////////////

        //If the assignment has been rejected then we want
        //to archive it and hide it from the list
        switch (assignment.confirmationStatus) {
            case 'denied':
                //////console.log('Hide the assignment')
                return $scope.archiveAssignment(assignment);
                break;
            case 'proposed':
                return FluroContent.resource('assignment', true, true).delete({
                    id: assignment._id,
                }, removeComplete);
                break;
            case 'temporary':
                return removeComplete(assignment);
                break;
        }



        /////////////////////////////////////////////////////

        //Create a copy
        var itemObject = angular.copy(assignment);
        //Add the extra meta details we need to open the 
        //editing panel
        itemObject._type = 'assignment';

        ///////////////////////////////////////////////////
        ///////////////////////////////////////////////////

        var eventRealms = _.get(MatrixSelectService, 'selected.event.realms');
        var rosterRealms = _.get(MatrixSelectService, 'selected.roster.realms');
        var assignmentRealms = assignment.realms;

        var realms = [];
        realms = realms.concat(eventRealms, rosterRealms, assignmentRealms);

        //Mash all the realms we know about together
        itemObject.realms = _.chain(realms)
            .compact()
            .flatten()
            .value();

        /////////////////////////////////////////////////////


        //Remove the item by opening the delete panel to double check
        ModalService.remove(itemObject, removeComplete);

        function removeComplete(res) {

            //Find the assignment update it with an _id
            assignment._id = res._id

            //Update the status of the assignment so it removes from the list
            _.assign(assignment, res);


            var slot = _.get(MatrixSelectService, 'selected.slot');
            _.pull(slot.assignments, assignment);

            ////console.log('Remove Complete!!');

            return rerender();
        }
    }

    ////////////////////////////////////////////////////

    $scope.checkProposed = function(assignment) {

        if (assignment.confirmationStatus != 'proposed') {
            return
        }

        ////////////////////////////////////////////
        var contactID = assignment.contact;
        if (contactID._id) {
            contactID = contactID._id;
        }

        ////////////////////////////////////////////

        var conflicts = _.chain($scope.conflicts)
            .filter(function(conflictingContact) {
                return conflictingContact._id == contactID;
            })
            .value();

        ////////////////////////////////////////////

        var unavailable = _.chain($scope.unavailable)
            .filter(function(conflictingContact) {
                return conflictingContact._id == contactID;
            })
            .value();

        ////////////////////////////////////////////

        if (unavailable.length) {
            return 'Unavailable at this time';
            // return 'Warning!' + $filter('readableDate')(unavailable[0].periods)
        }

        if (conflicts.length) {
            if (conflicts.length != 1) {
                return conflicts.length + ' conflicts!';
            } else {
                return '1 conflict!';
            }
            // + $filter('conflictSummary')(conflicts[0].conflicts)
        }
    }

    ////////////////////////////////////////////////////

    $scope.archiveAssignment = function(assignment) {

        //Set the assignment to 'archived'
        FluroContent.resource('assignment/' + assignment._id, true, true).update({
                status: 'archived',
            })
            .$promise
            .then(function(res) {
                assignment.status = 'archived';
                var slot = _.get(MatrixSelectService, 'selected.slot');
                _.pull(slot.assignments, assignment);

                ////console.log('Archived');
                return rerender();
            }, function(err) {
                //////console.log('ERROR hiding assignment', err)
            })
    }

    ////////////////////////////////////////////////////

    function groupAssignmentsByConfirmationStatus() {

        $scope.grouped = {};

        //Get the assignments already on this slot
        var assignments = _.get(MatrixSelectService, 'selected.slot.assignments');

        if (!assignments || !assignments.length) {
            return;
        }

        _.each(assignments, function(assignment) {
            if (!$scope.grouped[assignment.confirmationStatus]) {
                $scope.grouped[assignment.confirmationStatus] = [];
            }

            $scope.grouped[assignment.confirmationStatus].push(assignment);
        });
    }


    ////////////////////////////////////////////////////

    function updateSuggestions() {

        //Get the assignments already on this slot
        var slot = _.get(MatrixSelectService, 'selected.slot');
        var assignments = slot.assignments;

        groupAssignmentsByConfirmationStatus();

        /////////////////////////////////////////////////

        //Keep track of which contacts are already rostered
        var alreadyRosteredContactIDs = _.chain(assignments)
            .map(function(assignment) {
                var contactID = assignment.contact;
                if (contactID._id) {
                    contactID = contactID._id;
                }
                return contactID;
            })
            .value();

        ////////////////////////////////////////////////////

        function alreadyRostered(contactID) {
            return _.includes(alreadyRosteredContactIDs, contactID);
        }

        ////////////////////////////////////////////////////

        $scope.filteredConflicts = _.chain($scope.conflicts)
            .filter(function(contact) {

                var match = _.find(assignments, function(assignment) {

                    var contactID = assignment.contact;
                    if (contactID._id) {
                        contactID = contactID._id;
                    }

                    return contactID == contact._id && assignment.confirmationStatus != 'proposed';
                })

                return !match;
            })
            .value();


        if (slot.maximum && assignments.length >= slot.maximum) {
            $scope.filteredSuggestions = [];
            return //console.log('Already filled!');
        }
        ////////////////////////////////////////////////////

        // var allSuggestions = _.flattenDeep($scope.suggestions);

        // if(allSuggestions.length < 4) {
        // $scope.filteredSuggestions = allSuggestions;
        // } else {

        //Map our suggestions to a filtered list
        var filteredSuggestions = _.chain($scope.suggestions)
            .filter(function(chunk) {
                return chunk.length;
            })
            .map(function(chunk) {
                //Get all contacts in this chunk that aren't already rostered
                var filtered = _.filter(chunk, function(contact) {
                    //Check if the contact is already rostered
                    return !alreadyRostered(contact._id);
                });

                // if(filtered && filtered.length) {
                //Return the first option from this chunk
                return filtered[0];
                // } else {
                // return _.sample(allSuggestions, 1)[0];
                // }

            })
            .compact()
            .value();
        // }

        // ////////////////////////////////////////////////////




        // //If we have less than 4 suggestions then just get a random sample of all our suggestions
        // if(filteredSuggestions.length < 4) {
        //     var allSuggestions = _.chain($scope.suggestions)
        //     .flattenDeep()
        //     .value();


        //     filteredSuggestions = _.sample(allSuggestions, 4)
        //     ////console.log('GET RANDOM 4', filteredSuggestions)
        // }

        ////////////////////////////////////////////////////

        $scope.filteredSuggestions = filteredSuggestions;
    }

    ////////////////////////////////////

    $scope.selectSuggestion = function(contact, options) {

        if (!options) {
            options = {};
        }


        /**
        var assignmentData = {
            title: MatrixSelectService.selected.slot.title,
            contact: contact,
            contactName: (contact.title || contact.firstName + ' ' + contact.lastName),
            // confirmationStatus: 'proposed', //'unknown',
            // roster: $scope.rosterID,
        }

        MatrixSelectService.selected.slot.assignments.push(assignmentData);

        /////////////////////////////////////////////////////

        //Get the realms
        var eventRealms = MatrixSelectService.selected.event.realms;
        var rosterRealms = MatrixSelectService.selected.roster.realms;

        if (rosterRealms) {
            assignmentData.realms = rosterRealms;
        } else {
            assignmentData.realms = eventRealms;
        }

        /////////////////////////////////////////////////////

    /**/
        var details = {
            title: MatrixSelectService.selected.slot.title,
            contact: contact,
            confirmationStatus: 'proposed',
            force: options.force,
        }

        /////////////////////////////////////////////////////

        return createAssignment(details);

        /////////////////////////////////////////////////////

        function createAssignment(details) {

            var request = FluroContent.endpoint('assignments/roster/' + $scope.rosterID).save(details).$promise

            //////////////////////////////////////

            request.then(function(res) {

                var matchingAssignment = res; //_.first(res.assignments);
                matchingAssignment.contactName = (matchingAssignment.contact.title || matchingAssignment.contact.firstName + ' ' + matchingAssignment.contact.lastName);

                //Push it into the temporary assignments
                MatrixSelectService.selected.slot.assignments.push(matchingAssignment);
                // _.assign(details, res);

                /////////////////////////////////////////////////////

                //Retally the count
                retally();

                /////////////////////////////////////////////////////

                updateSuggestions();
                return rerender();

            }, function(err) {
                console.log('ERROR', err);

                //If we don't have access
                

                var conflictError = _.get(err, 'data.error');
                var message = _.get(err, 'data.message');

                if(err.status == 403) {
                    message = 'You do not have permission to make this action.';
                }

                /////////////////////

                if (conflictError == 'AssignmentConflictError') {
                    // alert('Warning!');

                    var array = [{
                            title: 'Add them anyway',
                            description: (contact.firstName || 'this person') + ' can still fulfill this assignment',
                            force:true,
                        },
                        // {
                        //     title: `Don't add them`,
                        //     description: `Don't add them anymore`,
                        // }
                    ];

                    return ModalService.actions(array, 'Conflict detected!', message).then(function(action) {
                        

                        if(action.force) {
                            console.log('Force anyway');
                            details.force = true;
                            return createAssignment(details)
                        }
                    });
                }

                /////////////////////

                Notifications.error(message || err);
                // _.pull(MatrixSelectService.selected.slot.assignments, details);

                /////////////////////////////////////////////////////


                //Retally the count
                retally();

                /////////////////////////////////////////////////////

                updateSuggestions();
                return rerender();
            });
        }


    }




    ////////////////////////////////////////////////////

    $scope.nudging = false;

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    $scope.nudge = function() {
        $scope.nudging = true;

        var assignments = _.get(MatrixSelectService, 'selected.slot.assignments');

        //////////////////////////////////////////

        var ids = _.chain($scope.grouped['unknown'])
            .map(function(assignment) {
                return assignment._id;
            })
            .compact()
            .value();

        //////////////////////////////////////////

        if (!ids.length) {
            $scope.nudging = false;
        }

        //////////////////////////////////////////

        FluroContent.endpoint('assignments/nudge', true, true).save({
                assignments: ids
            })
            .$promise
            .then(function(res) {
                $scope.nudging = false;

                ////console.log('Nudged everyone!', res);
                if (res.success.length) {
                    Notifications.status(res.success.length + ' nudge notifications sent');
                } else {
                    Notifications.warning('No nudges were sent');
                }

            }, function(err) {
                $scope.nudging = false;
                ////console.log('Error', err);

                Notifications.error(err.message);
            })
    }

    ////////////////////////////////////


    // $scope.$watch('context.item', function(contextItem) {

    //     if(!contextItem) {
    //         $scope.item = null;
    //         return;
    //     }

    //     var item ={
    //         event:contextItem.event,
    //         slot:contextItem.slot,
    //         assignments:$scope.getSlotAssignments(contextItem.slot, contextItem.event),
    //     }
    //     $scope.item = item;



    // })


})