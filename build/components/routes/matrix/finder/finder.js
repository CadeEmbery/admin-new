app.directive('finder', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        templateUrl: 'routes/matrix/finder/finder.html',
        controller: 'MatrixFinderController',
        link: function(scope, element, attrs) {

        }
    };
});

//////////////////////////////////////////////////

app.controller('MatrixFinderController', function($scope, FluroContent) {


    $scope.search = {};

    //////////////////////////////////////////////////

    // $scope.dropSuccess = function($event,$index,men) {
    // 	console.log('DROP SUCCESS', $event,$index,men);
    // }
    //////////////////////////////////////////////////

    $scope.$watch('search.terms', function(terms) {


        if (!terms || !terms.length) {
            return $scope.results = []
        }

        //////////////////////////////////////

        $scope.search.processing = true;

        //////////////////////////////////////

        var promise = FluroContent.endpoint('content/search/' + terms).query({
            limit: $scope.limit,
            simple: true,
            select: 'title _type realms definition',
            allDefinitions: true,
            populateFields: 'realms',
            populateSelect: 'color bgColor',
            statuses:['active'],
            types: [
                'contact',
                // 'location',
                'team',
                // 'collection',
                // 'event',
                // 'article',
                // 'audio',
                // 'asset',
                // 'image',
                // 'video',
                // 'plan',
            ]
        }).$promise;

        //////////////////////////////////////

        promise.then(searchComplete, searchFailed);

        //////////////////////////////////////

        function searchComplete(res) {
            $scope.results = _.chain(res)
                .map(function(set) {
                    return set.results;
                })
                .flattenDeep()
                .reduce(function(list, item) {

                    var type = item._type;
                    if (item.definition) {
                        type = item.definition;
                    }

                    var existing = _.find(list, {
                        _type: type
                    });

                    if (!existing) {
                        existing = {
                            _type: type,
                            results: []
                        }

                        list.push(existing);
                    }

                    existing.results.push(item);

                    return list;
                }, [])
                .value();

            ////////////////////

            $scope.search.processing = false;

        }

        //////////////////////////////////////

        function searchFailed(err) {
            $scope.results = [];
            $scope.search.processing = false;
        }
    })

})