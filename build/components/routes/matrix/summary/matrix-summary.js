app.directive('matrixSummary', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'routes/matrix/summary/matrix-summary.html',

        link: function($scope, $element, $attr) {
            
            var viewStatus = $attr.status;

            if(viewStatus) {
                $scope.summaryStatus = viewStatus;

                switch(viewStatus) {
                    case 'denied':
                        $scope.readableStatus = 'declined';
                    break;
                    case 'unknown':
                        $scope.readableStatus = 'unconfirmed';
                    break;
                    default:
                        $scope.readableStatus = viewStatus;
                    break;
                }
            }

            $scope.rowIsSelected = function(row) {

                console.log('ROW', row);
                    // if (!MatrixSelectService.selected.slot) {
                    //     return;
                    // }
                    // var match = $scope.getSlot(slot, roster);

                    // return MatrixSelectService.selected.slot == match;
                }
        }
    };
});
