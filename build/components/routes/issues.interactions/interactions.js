IssuesInteractionsController.resolve = {
    issues: function(FluroContent) {
        return FluroContent.endpoint('interaction/issues').query().$promise;
    }
}

//////////////////////////////////////////////

function IssuesInteractionsController($scope, issues, $state, FluroContent) {

	$scope.issues = issues;


	// $scope.send = function(issue) {
	// 	console.log('ISSUE');


	// 	var transactionID = issue.transaction._id;

		
	// 	var body =  issue.data.body;
	// 	delete body.payment;

	// 	console.log('CREATE!', body)
	// 	// return;

	// 	FluroContent.endpoint('interact/'+body.key+'?existingTransactionID=' + transactionID).save(body)
	// 	.$promise
	// 	.then(function(res) {
	// 		console.log('SUBMITTED', res);
	// 	}, function(err) {
	// 		console.log(err);
	// 	})
	// }


	$scope.resolve = function(issue) {
		

		var id = issue._id;

		FluroContent.endpoint('log/'+id +'/resolve').save()
		.$promise
		.then(function(res) {
			console.log('RESOLVED', res);

			$state.reload();
		}, function(err) {
			console.log(err);
		})
	}
}

