ImportPodcastController.resolve = {
    // integrations: function(FluroContent) {
    //     return FluroContent.endpoint('integrate/list/songselect').query().$promise;
    // }
}

//////////////////////////////////////////////

function ImportPodcastController($scope, $rootScope, FluroContent, Batch, $state) {

    $scope.submission = {};


    /////////////////////////////////

    function isValidURL(string) {
        var matcher = /^(?:\w+:)?\/\/([^\s\.]+\.\S{2}|localhost[\:?\d]*)\S*$/;
  		return matcher.test(string);
    }

    /////////////////////////////////

    $scope.isValid = function() {
        var valid = isValidURL($scope.submission.url);
        var hasRealms = $scope.submission.realms && $scope.submission.realms.length;

        return valid && hasRealms;
    };

    /////////////////////////////////

    $scope.go = function() {

        var valid = isValidURL($scope.submission.url);

        if (!valid || !$scope.submission.realms || !$scope.submission.realms.length) {
            return;
        }

        console.log('SUBMISSION', $scope.submission);

        $scope.processing = true;

        FluroContent.endpoint('import/podcasts').save($scope.submission).$promise.then(function(res) {

        	$scope.processing = false;

        	console.log('CALLBACK TASK', res);
        	//Batch.callbackTask(res);

        	var task = res;

        	if(!task) {
        		$scope.processing = false;
        		return;
        	}

            $rootScope.$broadcast(Batch.BATCH_INIT);

            var taskID = task._id;
            

            //Push this batch to our current list
            Batch.current.push(task);
            Batch.showModal();






        }, function(err) {
        	console.log('Error', err);
        	$scope.processing = false;
        });
    }
}