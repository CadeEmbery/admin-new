MyAccountController.resolve = {
	account: function($rootScope, FluroContent) {
		return FluroContent.endpoint('account/' + $rootScope.user.account._id).get().$promise;
	},
	// recentSMS: function($rootScope, FluroContent) {
	// 	// This executes the 'All SMS Messages Sent last 4 weeks' Report form the Fluro Account
	// 	return FluroContent.endpoint('content/_query/5b298efd54205a148d8046ff').query({searchInheritable:true}).$promise;
	// },
	transactionHistory: function($rootScope, FluroContent) {
		return FluroContent.endpoint('account/transactions').query({searchInheritable:true}).$promise;
	},
	// chargifyTransactionHistory: function($rootScope, FluroContent) {
	// 	return FluroContent.endpoint('account/transactions/chargify').query({searchInheritable:true}).$promise;
	// },
	chargifyAccount: function($rootScope, FluroContent, $q) {

		var deferred = $q.defer();

		FluroContent.endpoint('account/chargify').get({searchInheritable:true}).$promise
		.then(deferred.resolve, function(err) {
			console.log('EROROR',err);
			return deferred.resolve(null);
		});

		return deferred.promise;
	},
}

//////////////////////////////////////////////
//function MyAccountController($scope, account, transactionHistory, chargifyTransactionHistory, chargifyAccount, FluroContent, Notifications) {

function MyAccountController($scope, $rootScope, account, transactionHistory, chargifyAccount, FluroContent, Notifications) {

	$scope.account = account;
	// $scope.recentSMS = recentSMS;
	$scope.transactionHistory = transactionHistory;
	$scope.chargifyAccount = chargifyAccount;
	console.log("What do we have", _.get(account, "paymentMethod"))
	$scope.billingEnabled = _.get(account, "paymentMethod") 
	//$scope.chargifyTransactionHistory = chargifyTransactionHistory;

    var currentYear = new Date().getFullYear();
    var years = [];
    for ( var i=0; i<15; i++ ) {
        years.push(currentYear++);
    }   
    $scope.years = years;
	
	$scope.card = {
	};

	$scope.credit = {
	};


	//////////////////////////////////////////////////////////

	$scope.resendReceipt = function(transaction) {

		transaction.processing = true;
		var email = $rootScope.user.email;

		FluroContent.endpoint('system/transaction/receipt').save({
			transaction:transaction._id,
			email:email,
		})
		.$promise
		.then(function(res) {
			Notifications.status('Receipt was emailed to ' + email);
			transaction.processing = false;
		}, function(err) {
			Notifications.error(err);
			transaction.processing = false;
		});
	}

	//////////////////////////////////////////////////////////

	$scope.updateCard = function() {

		console.log('Card details are', $scope.card);

		FluroContent.endpoint('account/' + account._id + '/card').update($scope.card).$promise.then(function(res) {
			//Done
			//console.log("res", res)
			$scope.chargifyAccount = res
			$scope.card.edit = false
		});
	}


	$scope.payment= {};


	$scope.updateCredit = function() {

		console.log('Adding credit', $scope.credit);


		$scope.payment.processing = true;
		
		//convert to dollars
		var chargeAmount = $scope.credit.amount * 100

		FluroContent.endpoint('account/' + account._id + '/credit').update({amount: chargeAmount}).$promise.then(function(res) {
			//Done
			
			console.log("res", res)
			
			$scope.account.creditAmount = $scope.account.creditAmount + chargeAmount

			$scope.credit.edit = false
			$scope.credit.amount = 0
	
			// update chargify history 
			$scope.updateTransactionHistory()

           	Notifications.status('Account Updated.');
           	$scope.payment.processing = false;
	
		}, function(err){
			Notifications.warning('Unable to process payment.');
			$scope.payment.processing = false;
                        
		});
	}

	$scope.updateTransactionHistory = function() {
		FluroContent.endpoint('account/transactions').query({searchInheritable:true}).$promise.then(function (res){
			$scope.transactionHistory = res
		});
	}

	$scope.updateAccount = function() {

		FluroContent.endpoint('account').update($scope.account).$promise.then(function(res) {
			//Done
			//console.log("res", res)
			$scope.account.edit = false
			
	
		});
	}
}