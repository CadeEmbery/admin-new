ImportElvantoController.resolve = {
    // integrations: function(FluroContent) {
    //     return FluroContent.endpoint('integrate/list/songselect').query().$promise;
    // }
}

//////////////////////////////////////////////

function ImportElvantoController($scope, $rootScope, FluroContent, Batch, $state) {

    $scope.submission = {
        enabled: {
            songs: true,
            people: true,
            groups: true,
            departments: true,
            categories: true,
            tags: true,
            locations: true,
        }
    };


  
    /////////////////////////////////

    $scope.isValid = function() {
        var valid = _.get($scope.submission, 'defaultRealm') && _.get($scope.submission, 'apiKey.length');

       return valid;
    };

    /////////////////////////////////

    $scope.go = function() {

       
        if (!$scope.isValid()) {
            return;
        }

        // console.log('SUBMISSION', $scope.submission);

        $scope.processing = true;

        FluroContent.endpoint('import/elvanto').save($scope.submission).$promise.then(function(res) {

        	$scope.processing = false;

        	console.log('CALLBACK TASK', res);
        	//Batch.callbackTask(res);

        	var task = res;

        	if(!task) {
        		$scope.processing = false;
        		return;
        	}

            $rootScope.$broadcast(Batch.BATCH_INIT);

            var taskID = task._id;
            

            //Push this batch to our current list
            Batch.current.push(task);
            Batch.showModal();






        }, function(err) {
        	console.log('Error', err);
        	$scope.processing = false;
        });
    }
}