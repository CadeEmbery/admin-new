app.directive('processProgressBar', function() {

    return {
        restrict: 'E',
        replace:true,
        scope: {
            card: '=',
            states: '=',
            mode:'@',
        },
        templateUrl: 'process-progress-bar/process-progress-bar.html',
        link: function($scope, $element, $attrs) {

            if(!$scope.mode) {
                $scope.mode = 'linear';
            }

            ///////////////////////////////////////

            $scope.$watch('states', updateStates);
            $scope.$watch('card', updateStates);



            function updateStates() {

                var states = $scope.states;

                if(!states) {
                    return $scope.filteredStates =[];
                }

                //////////////////////////////////////

                $scope.filteredStates = _.chain(states)
                .filter(function(state) {
                    return state.style != 'waiting';
                })
                .map(function(state) {

                    if(!$scope.card) {
                        return state;
                    }

                    var key = state.key;
                    if($scope.card.results) {
                        state.result = $scope.card.results[key];
                    }
                    return state;
                })
                .value();
            }


            ///////////////////////////////////////

            $scope.activeState = function(state) {

                if(!$scope.card) {
                    return;
                }
                
                var current = $scope.card.state == state.key;

                if(!current) {
                    return;
                }

                switch(state.result) {
                    case 'complete':
                    case 'failed':
                    case 'pending':
                    break;
                    default:
                        return true;
                    break;
                    
                }
            }
            // $scope.activeState = function(state) {
            //     if ($scope.mode == 'linear') {
            //         var currentIndex = _.findIndex($scope.states, {
            //             key: $scope.card.state
            //         });

            //         var index = _.findIndex($scope.states, {
            //             key: state.key
            //         });


            //         //Check if card is in the same state;
            //         return (index <= currentIndex)
            //     } else {
            //         //Check if card is in the same state;
            //         return ($scope.card.state == state.key)
            //     }
            // }


        }
    };
});