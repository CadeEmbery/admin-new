app.directive('adminPostForm', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            definition: '=',
            model: '=ngModel',
            thread:'='
        },
        templateUrl: 'admin-post-form/admin-post-form.html',
        controller: 'AdminPostFormController',
    };
});

app.controller('AdminPostFormController', function($scope, FluroContent, $q, Notifications, TypeService) {


    var _defaultPost = {
        data: {}
    };

    $scope.$watch('definition', function() {
        console.log('Definition changed')
        $scope.reset();
    });

    //////////////////////////////////////////////////////////

    $scope.new = _defaultPost;

    //////////////////////////////////////////////////////////

    $scope.reset = function() {
        $scope.new = angular.copy(_defaultPost);
    }

    //////////////////////////////////////////////////////////

    $scope.submit = function() {

        if (!$scope.model) {
            return console.log('No Model');
        }

        //Copy
        var copy = angular.copy($scope.new);

        //Parent
        var hostID = $scope.model._id;


        //////////////////////////////////////////////////////////

        function submissionSuccess(res) {
            console.log('Success', res);
            $scope.thread.push(res);
            $scope.reset();
        }


        function submissionFail(err) {
            console.log('Failure', err);
            Notifications.post(err.data, 'error');
        }

        //Attempt to send information to post endpoint
        var request = FluroContent.endpoint('post/' + hostID + '/' + $scope.definition.definitionName).save(copy).$promise;

        //When the promise results fire the callbacks
        request.then(submissionSuccess, submissionFail)





    }
});