app.directive('fieldEditor', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            parentItem: '=ngParentItem',
        },
        templateUrl: 'admin-field-edit/admin-field-editor.html',
        //templateUrl: 'views/ui/field-editor.html',
        controller: 'FieldEditorController',
    };
});




app.controller('FieldEditorController', function($scope, ModalService, $rootScope) {

    if (!$scope.model) {
        $scope.model = [];
    }


    $scope.selected = {};
    $rootScope.expressionSelectFunction = false;

    $scope.selectField = function(node) {

        if ($rootScope.expressionSelectFunction) {

            var path = $scope.getFieldPath(node);
            console.log('Inject the expression', path);
            $rootScope.expressionSelectFunction(path);


        } else {
            $scope.selected.field = node;
        }
    }

    ///////////////////////////////////////////////

    $scope.hasError = function(field) {
        if (!field.key || !field.key.length) {
            return 'Field does not have a key!'
        }

        if (!field.title || !field.title.length) {
            return 'Field does not have a title!'
        }
    }

    ///////////////////////////////////////////////

    //Add this so child fields can access it
    this.definition = $scope.parentItem;

    ///////////////////////////////////////////////

    $scope.hasSubFields = function(node) {
        return (node.type == 'group' || node.directive == 'embedded');
    }


    var newObject = {
        type: 'string',
        directive: 'input',
        minimum: 0,
        maximum: 1,
        fields: [],
    }

    var newGroup = {
        type: 'group',
        fields: [],
        collapsed: true,
    }

    //Create a new object
    $scope._new = angular.copy(newObject);
    $scope._newGroup = angular.copy(newGroup);

    $scope.sortableOptions = {
        handle: ' .handle'
            // items: ' .panel:not(.panel-heading)'
            // axis: 'y'
    }


    ////////////////////////////////////////////////////

    $scope.add = function(object, context) {
        if ($scope.contains(object)) {
            return;
        }

        //Add it in to the right place

        var parent = $scope.model;
        var selectedField = $scope.selected.field;

        if (context == 'duplicate') {

        }


        if (selectedField) {

            //If it's a group
            switch (context) {
                case 'duplicate':
                    parent = getParentArray(selectedField);
                    break;
                default:

                    var isGroupable = (selectedField.type == 'group' || selectedField.directive == 'embedded');
                    var isNotTheSame = (selectedField.directive != object.directive) && object.directive != 'embedded';

                    if (isGroupable && isNotTheSame) {
                        if(selectedField.collapsed) {
                            parent = selectedField.fields;
                        } else {
                             parent = getParentArray(selectedField);
                        }
                        
                    } else {


                        parent = getParentArray(selectedField);
                    }
                    break;
            }

            // if(parent.collapsed) {
            //     console.log('PARENT IS COLLAPSED', parent);
            // }




            var index = parent.indexOf(selectedField);
            if (index != -1) {

                parent.splice(index + 1, 0, object);
                return true;
            }
        }



        parent.push(object);
        return true;
    }



    //////////////////////////////////////////

    $scope.getFieldPath = function(target) {

        var trail = [];
        var result = {};

        // function callback(actualTrail) {
        //     return actualTrail;
        // }

        getFieldPathTrail($scope.model, target, trail, result);

        if (target.parentType == 'interaction') {
            return 'interaction.' + result.trail.join('.');
        } else {
            return 'data.' + result.trail.join('.');
        }
    }


    function getFieldPathTrail(array, target, trail, result) {
        //return $scope.getTrail($scope.model, field, []);

        for (var key in array) {
            var field = array[key];

            if (field == target) {

                if (!field.asObject && field.directive != 'embedded') {
                    trail.push(field.key);
                } else {

                    if (field.minimum == field.maximum == 1) {
                        trail.push(field.key);
                    } else {
                        trail.push(field.key + '[i]');
                    }

                }


                //trail.push(field.key);
                result.trail = trail.slice();
                return;

                //return callback(trail.slice());
            }

            if (field.fields && field.fields.length) {

                if (field.asObject || field.directive == 'embedded') {

                    if (field.minimum == field.maximum == 1) {
                        trail.push(field.key);
                    } else {
                        trail.push(field.key + '[i]');
                    }

                }

                getFieldPathTrail(field.fields, target, trail, result);

                if (field.asObject || field.directive == 'embedded') {
                    trail.pop();
                }

            }
        }
    }

    /*
    $scope.getTrail = function(parentArray, child, trail) {

        //////////////////////////////////////////

        return _.chain(parentArray)
            .reduce(function(trailResult, field, key) {

                if(field == child) {
                    return trailResult;
                }

                if (field.fields && field.fields.length) {
                    trailResult.push(field.key);

                    //Recursive search
                    var subTrail = $scope.getTrail(field.fields, child, trail);
                    trailResult.pop();
                }

                return trailResult;

            }, [])
            .flatten()
            .compact()
            .value();

    }
    */

    //////////////////////////////////////////

    //Now we have nested routes we need to flatten
    function getFlattenedFields(array) {
        return _.chain(array)
            .reduce(function(result, field, key) {

                if (field.type == 'group' || field.directive) {
                    result.push(field);
                }

                if (field.fields && field.fields.length) {
                    //Recursive search
                    var folders = getFlattenedFields(field.fields);

                    if (folders.length) {
                        result.push(folders)
                    }
                }

                return result;

            }, [])
            .flatten()
            .compact()
            .value();

    }


    //////////////////////////////////////////

    function getParentArray(item) {

        //Get the flattened routes
        var fieldFolders = getFlattenedFields($scope.model);

        //console.log('Remove item', item, $scope.model, fields)

        //Find the parent
        var parent = _.find(fieldFolders, function(field) {
            return _.includes(field.fields, item);
        })

        //Remove it
        if (parent) {
            return parent.fields;
        }

        return $scope.model;
    }

    //////////////////////////////////////////

    $scope.remove = function(item) {

        /*
        //Get the flattened routes
        var fieldFolders = getFlattenedFields($scope.model);

        //console.log('Remove item', item, $scope.model, fields)

        //Find the parent
        var parent = _.find(fieldFolders, function(field) {
            
            return _.includes(field.fields, item);
        })
        */

        var parentArray = getParentArray(item);

        //Remove it
        if (parentArray) {
            _.pull(parentArray, item);
        }

        //Deselect
        delete $scope.selected.field;

    }

    /**
    $scope.removeRoute = function() {

        //Get the item
        var item = $scope.selection.item;

        //Get the flattened routes
        var folders = getFlattenedFolders($scope.model);

        //Find the parent
        var parentFolder = _.find(folders, function(folder) {

            var found = _.includes(folder.routes, item);
            return found;
        })

        //Remove it
        if (parentFolder) {
            _.pull(parentFolder.routes, item);
        } else {
            _.pull($scope.model, item);
        }


        $scope.selection.deselect();
    }
    /**/

    ////////////////////////////////////////////////////

    /**
    $scope.remove = function(object) {

        console.log('Remove Object', object);
        if ($scope.contains(object)) {
            delete $scope.selected.field;
            _.pull($scope.model, object);
        }
    }
    /**/

    ////////////////////////////////////////////////////

    $scope.contains = function(object) {
        return _.includes($scope.model, object)
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {



        var insert = angular.copy($scope._new);
        var inserted = $scope.add(insert);
        $scope.selected.field = insert;

        if (inserted) {
            $scope._new = angular.copy(newObject);
        }


    }

    ////////////////////////////////////////////////////

    $scope.moreOptions = function() {

        console.log('More Options')
        var possibleActions = [];

        possibleActions.push({
            title: 'Add contact field',
            description: 'Add contact reference field with basic details inside it, including first name, last name etc',
            key: 'contact',
        })

        possibleActions.push({
            title: 'Add family household',
            description: 'Add a family household reference field with basic details inside it, including address, suburb etc',
            key: 'family',
        })

        possibleActions.push({
            title: 'Add a custom heading',
            description: 'Add a custom HTML area with a heading',
            key: 'heading',
        })

        possibleActions.push({
            title: 'Add an external link',
            description: 'Add a link to an external url',
            key: 'url',
        })

       


        console.log('Actions', possibleActions);


        var promise = ModalService.actions(possibleActions);

        ////////////////////////////////////////




        promise.then(function(action) {



            switch (action.key) {
                case 'contact':
                    var contact = {
                        title: '',
                        key: '',
                        type: 'reference',
                        directive: 'embedded',
                        minimum: 1,
                        maximum: 0,
                        askCount: 1,
                        fields: [],
                        params: {
                            restrictType: 'contact',

                        }
                    }
                    contact.fields.push({
                        type: 'group',
                        title: 'Row',
                        className: 'row',
                        key: 'row',
                        fields: [{
                            title: 'First Name',
                            key: 'firstName',
                            type: 'string',
                            minimum: 1,
                            maximum: 1,
                            className: 'col-xs-6',
                            directive: 'input',
                        }, {
                            title: 'Last Name',
                            key: 'lastName',
                            type: 'string',
                            minimum: 1,
                            maximum: 1,
                            className: 'col-xs-6',
                            directive: 'input',
                        }]
                    })

                    contact.fields.push({
                        title: 'Gender',
                        key: 'gender',
                        type: 'string',
                        directive: 'select',
                        minimum: 1,
                        maximum: 1,
                        options: [{
                            name: 'Male',
                            value: 'male',
                        }, {
                            name: 'Female',
                            value: 'female',
                        }]
                    })

                    contact.fields.push({
                        title: 'Email Address',
                        key: 'email',
                        type: 'email',
                        minimum: 1,
                        maximum: 1,
                        directive: 'input',
                    })

                    contact.fields.push({
                        title: 'Phone Number',
                        key: 'phoneNumber',
                        type: 'string',
                        minimum: 1,
                        maximum: 1,
                        directive: 'input',
                    })

                    var inserted = $scope.add(contact);
                    $scope.selected.field = contact;

                    break;
                case 'heading':
                    var heading = {
                        title: 'Heading',
                        key: 'heading',
                        type: 'void',
                        directive: 'custom',
                        template: '<h4>My heading</h4>\n<p>And some introductory text for this section</p>',
                    }

                    var inserted = $scope.add(heading);
                    $scope.selected.field = heading;


                    break;
                case 'url':
                    var heading = {
                        title: 'Link to URL',
                        key: 'link',
                        type: 'void',
                        directive: 'custom',
                        template: '<a href="https://www.fluro.io">Click here to view link</a>',
                    }

                    var inserted = $scope.add(heading);
                    $scope.selected.field = heading;


                    break;
                case 'family':
                    var family = {
                        title: 'Family Household / Address',
                        key: 'family',
                        type: 'reference',
                        directive: 'embedded',
                        minimum: 1,
                        maximum: 1,
                        askCount: 1,
                        fields: [],
                        params: {
                            restrictType: 'family',

                        }
                    }

                    // family.fields.push({
                    //     title:'Title',
                    //     description:'This field can be hidden and an expression can be used to automatically'
                    //     key:'title',
                    //     type:'string',
                    //     minimum:0,
                    //     maximum:1,
                    //     className:'hidden',
                    //     directive:'input',
                    // })

                    ///////////////////////////////

                    var address = {
                        title: 'Address',
                        key: 'address',
                        type: 'group',
                        minimum: 1,
                        maximum: 1,
                        askCount: 1,
                        asObject: true,
                        fields: [],
                    }

                    ///////////////////////////////

                    //Add the address
                    family.fields.push(address)

                    //Add the address pieces
                    address.fields.push({
                        title: 'Address Line 1',
                        key: 'addressLine1',
                        type: 'string',
                        directive: 'input',
                        maximum: 1,
                    })

                    address.fields.push({
                        title: 'Address Line 2',
                        key: 'addressLine2',
                        type: 'string',
                        directive: 'input',
                        maximum: 1,
                    })


                    address.fields.push({
                        title: 'Row',
                        className: 'row',
                        key: 'row',
                        type: 'group',
                        fields: [{
                            title: 'Suburb',
                            key: 'suburb',
                            type: 'string',
                            directive: 'input',
                            maximum: 1,
                            className: 'col-sm-4',
                        }, {
                            title: 'State',
                            key: 'state',
                            type: 'string',
                            directive: 'input',
                            maximum: 1,
                            className: 'col-sm-4',
                        }, {
                            title: 'Postal Code',
                            key: 'postalCode',
                            type: 'string',
                            directive: 'input',
                            maximum: 1,
                            className: 'col-sm-4',
                        }]
                    })

                    ///////////////////////////////

                    var inserted = $scope.add(family);
                    $scope.selected.field = family;

                    break;
            }






        })

    }


    // ////////////////////////////////////////////////////

    // $scope.createPerson = function() {
    //     var insert = angular.copy($scope._new);

    //     insert
    //     var inserted = $scope.add(insert);
    //     $scope.selected.field = insert;

    //     if (inserted) {
    //         $scope._new = angular.copy(newGroup);
    //     }
    // }




    ////////////////////////////////////////////////////

    $scope.duplicate = function() {
        if (!$scope.selected.field) {
            return;
        }

        var insert = angular.copy($scope.selected.field);
        insert.title = insert.title + ' Copy';
        insert.key = '';
        delete insert._id;


        var inserted = $scope.add(insert, 'duplicate');

        if (inserted) {
            $scope.selected.field = insert;
            $scope._new = angular.copy(newObject);
        }

    }






    ////////////////////////////////////////////////////

    $scope.createGroup = function() {
        var insert = angular.copy($scope._newGroup);
        var inserted = $scope.add(insert);
        $scope.selected.field = insert;

        if (inserted) {
            $scope._newGroup = angular.copy(newGroup);
        }
    }


    ////////////////////////////////////////////////////

    $scope.importFieldData = {};


    $scope.$watch('importFieldData.json', function(imported) {

        

        // ////console.log('Imported', imported);
        if (imported && imported.data) {

            var split = imported.data.split(',')[1];
            var json = window.atob(split);
            var object = JSON.parse(json);

            
            var fields = _.get(object, 'fields');

            // if(!$scope.item.fields) {
            //     $scope.item.fields = [];
            // }

            //Reset the item
            $scope.model = $scope.model.concat(fields);
        }


    });



    // $scope.importFields = function() {

    //     var insert = angular.copy($scope._newGroup);
    //     var inserted = $scope.add(insert);
    //     $scope.selected.field = insert;

    //     if (inserted) {
    //         $scope._newGroup = angular.copy(newGroup);
    //     }

    // }



    

    ////////////////////////////////////////////////////
    /*
    $scope.createGroup = function() {
        var newGroup = {
            title:'New Group',
            type:'group',
            fields:[]
        }

        $scope.add(newGroup); 
    }
    */

})


/////////////////////////////////////////////////