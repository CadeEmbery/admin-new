
/////////////////////////////////////////////////

app.directive('fieldGroupEdit', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-field-edit/admin-field-group-edit.html',
        require: '^fieldEditor',
        //templateUrl: 'views/ui/field-group-edit.html',
        controller: 'FieldGroupEditController',
        link:function($scope, $element, $attrs, $ctrl) {
            $scope.$ctrl = $ctrl;
        }
    };
});

app.controller('FieldGroupEditController', function($scope, $rootScope, TypeService) {

    if (!$scope.model) {
        $scope.model = {};
    }


    //  $scope.referenceParams =
    // $scope.definedTypes = TypeService.definedTypes;
    // $scope.types = TypeService.types;


    ////////////////////////////////////////////////////

    $scope.selectExpression = function(path) {

        delete $rootScope.expressionSelectFunction;

        if(!$scope.model.hideExpression) {
            $scope.model.hideExpression = '';
        } else {
            $scope.model.hideExpression += ' ';
        }

        $scope.model.hideExpression +=  path;


    }

    $scope.picker = function() {
        $rootScope.expressionSelectFunction = $scope.selectExpression;
    }


    ///////////////////////////////////////////////

    $scope.$watch('model.asObjectType', function(objectType) {
        if(objectType == 'contactdetail') {
            $scope.definedTypes = _.filter(TypeService.definedTypes, {'parentType':'contactdetail'});
        } else {
            $scope.definedTypes = null;
        }
    })

    ///////////////////////////////////////////////


    
    if (!$scope.model.key) {
        $scope.$watch('model.title', function(newValue) {
            if (newValue) {
                $scope.model.key = _.camelCase(newValue); //.toLowerCase();
            }
        })
    }

    $scope.$watch('model.key', function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.model.key = $scope.model.key.replace(regexp, '');
        }
    })
});