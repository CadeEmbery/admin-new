app.directive('fieldEdit', function() {

    return {
        restrict: 'E',
        replace:true,
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-field-edit/admin-field-edit.html',
        controller: 'FieldEditController',
        require: '^fieldEditor',
        link:function($scope, $element, $attrs, $ctrl) {
            $scope.$ctrl = $ctrl;
        }
    };
});

app.controller('FieldEditController', function($scope, $uibModal, TypeService) {


    //console.log('Field Edit Controller')
    //$scope.$watch('model', function(model) {

    if (!$scope.model) {
        $scope.model = {};
    }


    ////////////////////////////////////////////////

    $scope.selectFields = function() {

        //Create the modal
        var modalInstance = $uibModal.open({
            templateUrl: 'admin-field-edit/modal/field-select-modal.html',
            controller: 'FieldSelectModalController',
            size: 'md',
            backdrop: 'static',
            // resolve: {
                
            // }
        });

        modalInstance.result.then(function(response) {
            // console.log('SELECTED FIELDS IS', res);

            var definition = response.definition;

            // var detailsObject = {
            //     type:'group',
            //     key:'details',
            //     asObject:true,
            //     minimum:1,
            //     maximum:1,
            //     askCount:1,
            //     fields:[],
            // }

            // var sheetObject = {
            //     type:'group',
            //     key:definition.definitionName,
            //     asObject:true,
            //     minimum:1,
            //     maximum:1,
            //     askCount:1,
            //     fields:[],
            // }            

            //////////////////////////////

            if(!$scope.model.fields) {
                $scope.model.fields = [];
            }

            var detailBlock =  _.find($scope.model.fields, {key:'details'});
            if(!detailBlock) {
                detailBlock = {
                    title:'Details',
                    type:'group',
                    key:'details',
                    asObject:true,
                    minimum:1,
                    maximum:1,
                    askCount:1,
                    fields:[],
                    collapsed:true,

                }

                $scope.model.fields.push(detailBlock);
            }

            //////////////////////////////
            
            var sheetBlock =  _.find(detailBlock.fields, {key:definition.definitionName});
            var dataBlock;

            if(!sheetBlock) {
                sheetBlock = {
                    title:definition.title,
                    type:'group',
                    key:definition.definitionName,
                    asObject:true,
                    minimum:1,
                    maximum:1,
                    askCount:1,
                    fields:[],
                    collapsed:true,
                    
                }

                dataBlock = {
                    title:'Data',
                    type:'group',
                    key:'data',
                    asObject:true,
                    minimum:1,
                    maximum:1,
                    askCount:1,
                    fields:[],
                    collapsed:true,
                    
                }

                //Add as a pyramid
                sheetBlock.fields.push(dataBlock);
                detailBlock.fields.push(sheetBlock);
            }

            //////////////////////////////

            _.each(definition.fields, function(field) {
                var existingField = _.find(dataBlock.fields, {key:field.key});
                if(!existingField) {
                    console.log('Add the fields', field)
                    dataBlock.fields.push(field);
                }
            })


        })



    }

    ////////////////////////////////////////////////

    $scope.referenceParams =
        $scope.definedTypes = TypeService.definedTypes;
    $scope.types = TypeService.types;

    /////////////////////////////////////////////

    $scope.definitionGroups = _.chain(TypeService.definedTypes)
        .reduce(function(set, definition) {

            var parentType = _.startCase(definition.parentType);

            var existing = set[parentType];
            if (!existing) {
                existing =
                    set[parentType] = {
                        title: parentType,
                        definitions: [],
                    }
            }

            existing.definitions.push(definition);

            return set;
        }, {})
        .values()
        .value();


    $scope.contactDefinitions = _.filter(TypeService.definedTypes, {parentType:'contact'});
    

    /////////////////////////////////////////////

    $scope.showDescriptionAndPlaceholder = function() {

        if ($scope.model.type == 'void') {
            return;
        }

        if ($scope.model.directive == 'embedded') {
            return;
        }

        return true;
    }

    /////////////////////////////////////////////

    $scope.detailSheetDefinitions = _.filter(TypeService.definedTypes, function(definition) {
        return definition.parentType == 'contactdetail';
    })

    $scope.showOptions = function() {

        switch ($scope.model.directive) {
            case 'select':
            case 'button-select':
            case 'search-select':
            case 'order-select':
                return true;
                break;
        }
    }

    /////////////////////////////////////////////

    $scope.isNumberType = function(type) {
        switch (type) {
            case 'number':
            case 'integer':
            case 'float':
                return true;
                break;
        }
    }

    /////////////////////////////////////////////

    $scope.$watch('model', function(model) {
        if (!model.key || !model.key.length) {
            startWatchingTitle();
        } else {
            stopWatchingTitle();
        }
    })

    /////////////////////////////////////////////

    var watchTitle;

    function stopWatchingTitle() {
        if (watchTitle) {
            watchTitle();
        }
    }

    function startWatchingTitle() {

        if (watchTitle) {
            watchTitle();
        }

        watchTitle = $scope.$watch('model.title', function(newValue) {
            if (newValue) {
                $scope.model.key = _.camelCase(newValue); //.toLowerCase();
            }
        });

    }

    /////////////////////////////////////////////

    $scope.$watch('model.key', function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.model.key = $scope.model.key.replace(regexp, '');
        }
    })

    /////////////////////////////////////////////

    // $scope.$watch('model.type + model.directive + model.params.restrictType', function() {
    //     if($scope.model.type == 'reference' && $scope.model.directive == 'reference-create') {

    //         var definition = TypeService.getTypeFromPath($scope.model.params.restrictType);

    //         if(definition) {
    //             $scope.model.embeddedDefinition = definition;
    //         }
    //     } else {
    //         delete $scope.model.embeddedDefinition;
    //     }
    // })

    //});


});



app.controller('TemporaryUploadRealmController', function($scope, FluroAccess, TypeService) {

    $scope.$watch('model.params.restrictType', function(typeName) {

        console.log('UPDATE', typeName);

        if (typeName && typeName.length) {
            $scope.selectableRealms = FluroAccess.retrieveActionableRealms('create ' + typeName)
        } else {


            var realms = [];
            realms = realms.concat(FluroAccess.retrieveActionableRealms('create image'));
            realms = realms.concat(FluroAccess.retrieveActionableRealms('create video'));
            realms = realms.concat(FluroAccess.retrieveActionableRealms('create asset'));

            console.log('REALMS', realms);

            $scope.selectableRealms = _.chain(realms)
                .uniq(function(realm) {
                    return realm._id;
                })
                .value();
        }
    })
})