app.directive('contentSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            ngParams: '&',
        },
        templateUrl: 'admin-content-select/admin-content-select.html',
        controller: 'ContentSelectController',
    }

});

//////////////////////////////////////////////////////////////////////////

app.controller('ContentSelectController', function($scope, $location, $window, Asset, $rootScope, FluroContent, Fluro, ModalService, TypeService, FluroAccess, $http) {

    var totalLimit = 500;

    $scope.settings = {
        limitNumber:totalLimit,
    }

    /////////////////////////////////////

    if (!$scope.selectedItems) {
        $scope.selectedItems = {
            items:[]
        }
    }

    /////////////////////////////////////

    if (!$scope.params) {
        $scope.params = {};
    }

    /////////////////////////////////////

    $scope.asset = Asset;

    /////////////////////////////////////

    $scope.searchPlaceholder = 'Search for existing content';

    /////////////////////////////////////

    $scope.$watch(function() {

        return $scope.ngParams();

    }, function(p) {

        if (!p) {
            p = {}
        }
        $scope.params = p;

        // if ($scope.params.canCreate) {
        //     $scope.canCreate = $scope.params.canCreate;
        // }

        if ($scope.params.hideCreate) {
            $scope.canCreate = false;
        } else {
            $scope.canCreate = true;
        }

        //////////////////////////////////////////////

        if ($scope.params.type) {

            $scope.type = TypeService.getTypeFromPath($scope.params.type)

            if(!$scope.type) {
                $scope.browseAccessDisabled = true;
            } else {
                $scope.searchPlaceholder = 'Search for existing ' + $scope.type.plural;
            }
          
        }

        //////////////////////////////////////////////

        if ($scope.params.hideBrowse) {
            $scope.hideBrowse = true;
        }

        //console.log('TESTING', $scope.params.disableBrowse);

        if ($scope.params.minimum) {
            $scope.minimum = $scope.params.minimum;
        }
        if ($scope.params.maximum) {
            $scope.maximum = $scope.params.maximum;
        }

        if ($scope.params.allowedValues) {
            $scope.allowedValues = $scope.params.allowedValues;
        }

        if ($scope.params.defaultValues) {
            $scope.defaultValues = $scope.params.defaultValues;

            //If theres nothing in the list yet
            if (!$scope.selectedItems.items.length) {
                //Loop through each default value and include it
                _.each($scope.defaultValues, function(item) {
                    $scope.selectedItems.items.push(item);
                })
            }
        }

    }, true)

    ///////////////////////////////////////////////

    $scope.createLabel = function() {

        var typeLabel = $scope.type.singular;
        if($scope.params.maximum != 1) {
            typeLabel = $scope.type.plural;
        }

        switch($scope.type.path) {
            case 'asset':
            case 'audio':
            case 'image':
                return 'Add new ' + typeLabel;
            break;
            case 'video':
                return 'Add new ' + typeLabel;
            break;
            default:
                return 'Create new ' + typeLabel;
            break;
        }
    }

    ///////////////////////////////////////////////

    $scope.$watch('model', function() {
        if ($scope.model) {
            if (_.isArray($scope.model)) {
                if ($scope.model.length) {
                    if ($scope.maximum) {
                        //take the maximum first items from the existing model
                        $scope.selectedItems.items = _.take($scope.model, $scope.maximum);
                    } else {
                        $scope.selectedItems.items = angular.copy($scope.model);
                    }
                } else {
                    $scope.selectedItems.items = _.union($scope.model, $scope.defaultValues);
                }
            } else {
                $scope.selectedItems.items = [$scope.model];
            }
        } else {
            $scope.selectedItems.items = [];
        }
    }, true)

    ////////////////////////////////////////////////////

    $scope.$watch('selectedItems.items', function(items) {

       // console.log('Selected Items changed', items)
        if ($scope.maximum == 1) {
            $scope.model = _.first(items);
        } else {

            $scope.model = items;
        }

        //We need to nullify it otherwise the message doesnt get to the server
        if(!$scope.model) {
            $scope.model = null;
        }

        if(items.length > totalLimit) {
            $scope.settings.limitNumber = 5;
        }

    }, true)


    /////////////////////////////////////

    $scope.proposed = {}

    /////////////////////////////////////

    $scope.dynamicPopover = {
        templateUrl: 'views/ui/create-popover.html',
    }

    /////////////////////////////////////

    $scope.createEnabled = function() {

        if($scope.canCreate) {
            if ($scope.type) {
                return FluroAccess.can('create', $scope.type.path, $scope.type.parentType);
            } else {
                return false;
            }
        }
    }

    /////////////////////////////////////

    $scope.isSortable = function() {

        if ($scope.maximum) {
            return $scope.maximum > 1;
        }
        return true;
    }

    /////////////////////////////////////

    $scope.addEnabled = function() {
        if(!$scope.browseAccessDisabled) {
            if ($scope.maximum) {
                return ($scope.selectedItems.items.length < $scope.maximum)
            } else {
                return true;
            }
        }
    }

    /////////////////////////////////////

    $scope.add = function(item) {
        $scope.selectedItems.items.push(item);
        $scope.proposed = {};
    }

    /////////////////////////////////////

    $scope.remove = function(item) {
        if (_.contains($scope.selectedItems.items, item)) {
            _.pull($scope.selectedItems.items, item);
        }
    }

    /////////////////////////////////////

    $scope.edit = function(item) {
        ModalService.edit(item);
    }

    /////////////////////////////////////

    $scope.view = function(item) {
        ModalService.view(item);
    }

    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////

    function getBaseDomain(string) {

        var split = string.split('.');
        return split.slice(Math.max(split.length - 2, 1)).join('.');

        // var length = split.length;
        // if (length > 2) {
        //     return string.substr(string.indexOf(".") + 1);
        // } else {
        //     return string;
        // }
    }


    $scope.editInAdmin = function(item) {

        var def = item._type;
        if(item.definition && item.definition.length) {
            def = item.definition;
        }

        var baseDomain = getBaseDomain(Fluro.apiURL);
        // console.log('BASE DOMAIN', baseDomain);
        var domain = '';

        switch(baseDomain)  {
            case 'fluro.dev:3000':
                domain = 'http://admin.fluro.dev:3000';
            break;
            case 'inyerpocket.com':
                domain = 'http://admin.inyerpocket.com';
            break;
            default:
                domain = 'http://admin.fluro.io';
            break;
        }

        var url =  domain + '/' + item._type + '/' + def + '/' + item._id + '/edit';

        $window.open(url, '_blank');
    }

    $scope.canEditAdmin = function(item) {
        var canEdit = $scope.canEdit(item);
        var currentDomain = $location.host();

        var notAdmin = !_.startsWith(currentDomain, 'admin');
        return canEdit && notAdmin;
    }

    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////

    $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item);
    }

    /////////////////////////////////////

    $scope.canView = function(item) {
        return FluroAccess.canViewItem(item);
    }

    /////////////////////////////////////

    $scope.browse = function() {   


        if (!$scope.browseAccessDisabled) {

            var params = {};

            if ($scope.params.searchInheritable) {
                params.searchInheritable = $scope.params.searchInheritable = true;
            }

            if ($scope.params.syntax) {
                params.syntax = $scope.params.syntax;
            }

            if ($scope.params.parentType) {
                params.parentType = $scope.params.parentType;
            }

            // console.log('PARAMS', $scope.params.definition);
            
            if ($scope.params.definition) {
                params.definition = $scope.params.definition;
            }


            console.log('OPTION UP THE STUFF', params)
            
            if ($scope.type) {
                ModalService.browse($scope.type.path, $scope.selectedItems, params);
            } else {
                ModalService.browse(null, $scope.selectedItems, params);
            }
        }
    }

    /////////////////////////////////////

    $scope.createableTypes = TypeService.getAllCreateableTypes;

    /////////////////////////////////////

    $scope.create = function(typeToCreate) {

        var options = {};

        if($scope.params.template) {
            options.template = $scope.params.template;
        }

        if (!typeToCreate) {
            ModalService.create($scope.type.path, options, $scope.add)
        } else {
            ModalService.create(typeToCreate, options, $scope.add)
        }
    }

    /////////////////////////////////////

    $scope.getOptions = function(val) {

        if ($scope.allowedValues && $scope.allowedValues.length) {
            return _.reduce($scope.allowedValues, function(filtered, item) {
                var exists = _.some($scope.selectedItems.items, {
                    '_id': item._id
                });

                if (!exists) {
                    filtered.push(item);
                }
                return $filter('filter')(filtered, val);
            }, [])
        } else {

            ////////////////////////

            //Create Search Url
            // var searchUrl = Fluro.apiURL + '/content';
            var searchUrl = 'content';
            if ($scope.type) {
                searchUrl += '/' + $scope.type.path;
            }
            searchUrl += '/search';

            ////////////////////////

            // return $http.get(searchUrl +'/'+ val, {
            //     ignoreLoadingBar: true,
            //     params: {
            //         limit: 10,
            //     }
            // })
            return FluroContent.endpoint(searchUrl +'/'+ val, true, true).query({
                limit: 15,
                allDefinitions:$scope.params.allDefinitions,
                parentType:$scope.params.parentType,
            })
            .$promise
            .then(function(results) {

                console.log('SEARCH', results, val)

                // var results = response.data;

                return _.reduce(results, function(filtered, item) {
                    var exists = _.some($scope.selectedItems.items, {
                        '_id': item._id
                    });
                    if (!exists) {
                        filtered.push(item);
                    }
                    return filtered;
                }, []);
                
            });
        }
    };


});
