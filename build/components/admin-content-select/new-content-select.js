app.directive('newContentSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            config: '=',
        },
        templateUrl: 'admin-content-select/new-content-select.html',
        controller: 'NewContentSelectController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('NewContentSelectController', function($scope, Asset, $rootScope, Fluro, ModalService, TypeService, FluroAccess, $http) {

    /////////////////////////////////////

    if (!$scope.selectedItems) {
        $scope.selectedItems = {
            items: []
        }
    }

    /////////////////////////////////////

    if (!$scope.config) {
        $scope.config = {};
    }

    /////////////////////////////////////

    $scope.asset = Asset;

    /////////////////////////////////////

    /**/
    $scope.$watch('config', function(config) {

        //console.log('Config', config)
        /////////////////////////////////////////

        //Basics
        $scope.canCreate = config.canCreate;
        $scope.hideBrowse = config.hideBrowse;
        $scope.minimum = parseInt(config.minimum);
        $scope.maximum = parseInt(config.maximum);

        /////////////////////////////////////////


        if (config.type) {
            $scope.type = TypeService.getTypeFromPath(config.type)
            if (!$scope.type) {
                $scope.browseAccessDisabled = true;
            }
        }

        /////////////////////////////////////////

        if (config.allowedValues) {
            $scope.allowedValues = config.allowedValues;
        }

        /////////////////////////////////////////

        if (config.defaultValues) {
            $scope.defaultValues = config.defaultValues;

            //If theres nothing in the list yet
            if (!$scope.selectedItems.items.length) {
                //Loop through each default value and include it
                _.each($scope.defaultValues, function(item) {
                    $scope.selectedItems.items.push(item);
                });
            }
        }

        /////////////////////////////////////////

        if (config.type) {
            $scope.type = TypeService.getTypeFromPath(config.type)
            if (!$scope.type) {
                $scope.browseAccessDisabled = true;
            }
        }
    }, true);
    /**/


    ///////////////////////////////////////////////

    $scope.$watch('model', function() {


        //console.log('TEST MODEL', $scope.model);
        if ($scope.model) {
            if (_.isArray($scope.model)) {
                if ($scope.model.length) {
                    if ($scope.maximum) {
                        //take the maximum first items from the existing model
                        $scope.selectedItems.items = _.take($scope.model, $scope.maximum);
                    } else {
                        $scope.selectedItems.items = angular.copy($scope.model);
                    }
                } else {
                    $scope.selectedItems.items = _.union($scope.model, $scope.defaultValues);
                }
            } else {
                $scope.selectedItems.items = [$scope.model];
            }
        } else {
            $scope.model = [];
        }
    }, true)

    ////////////////////////////////////////////////////

    $scope.$watch('selectedItems.items', function(items) {
        // console.log('Selected Items changed', items)
        if ($scope.maximum == 1) {
            $scope.model = _.first(items);
        } else {
            $scope.model = items;
        }
    }, true)


    /////////////////////////////////////

    $scope.proposed = {}

    /////////////////////////////////////

    $scope.dynamicPopover = {
        templateUrl: 'views/ui/create-popover.html',
    };

    $scope.searchPlaceholder = 'Search for existing content';

    if ($scope.type) {
        $scope.searchPlaceholder = 'Search for ' + $scope.type.singular + ' items';
    }

    /////////////////////////////////////

    $scope.createEnabled = function() {
        if ($scope.type) {
            return FluroAccess.can('create', $scope.type.path, $scope.type.parentType);
        } else {
            return false;
        }
    }

    /////////////////////////////////////

    $scope.isSortable = function() {
        if ($scope.maximum) {
            return $scope.maximum > 1;
        }
        return true;
    }

    /////////////////////////////////////

    $scope.addEnabled = function() {

        //console.log('Testing', $scope.maximum, $scope.selectedItems);
        if ($scope.browseAccessDisabled) {
            return false;
        }

        if (!$scope.maximum) {
            return true;
        }
        
        var selectedItems = $scope.selectedItems.items;

        return (selectedItems.length < $scope.maximum); 
    }

    /////////////////////////////////////

    $scope.add = function(item) {
        $scope.selectedItems.items.push(item);
        $scope.proposed = {};
    }

    /////////////////////////////////////

    $scope.remove = function(item) {
        if (_.contains($scope.selectedItems.items, item)) {
            _.pull($scope.selectedItems.items, item);
        }
    }

    /////////////////////////////////////

    $scope.edit = function(item) {
        ModalService.edit(item);
    }

    /////////////////////////////////////

    $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item);
    }

    /////////////////////////////////////

    $scope.browse = function() {

        if (!$scope.browseAccessDisabled) {

            var params = {};

            if ($scope.config.searchInheritable) {
                params.searchInheritable = $scope.config.searchInheritable = true;
            }
            
            if ($scope.type) {
                ModalService.browse($scope.type.path, $scope.selectedItems, params);
            } else {
                ModalService.browse(null, $scope.selectedItems, params);
            }
        }
    }

    /////////////////////////////////////

    
    // $scope.createableTypes = TypeService.getAllCreateableTypes();

    //  console.log('GET TYPES', $scope.createableTypes)
    /////////////////////////////////////

    $scope.create = function(typeToCreate) {

        var options = {};

        if ($scope.config.template) {
            options.template = $scope.config.template;
        }

        if (!typeToCreate) {
            ModalService.create($scope.type.path, options, $scope.add)
        } else {
            ModalService.create(typeToCreate, options, $scope.add)
        }
    }

    /////////////////////////////////////

       
    $scope.getOptions = function(val) {

        if ($scope.allowedValues && $scope.allowedValues.length) {
            return _.reduce($scope.allowedValues, function(filtered, item) {
                var exists = _.some($scope.selectedItems.items, {
                    '_id': item._id
                });

                if (!exists) {
                    filtered.push(item);
                }
                return $filter('filter')(filtered, val);
            }, [])
        } else {

            ////////////////////////

            //Create Search Url
            var searchUrl = Fluro.apiURL + '/content';
            if ($scope.type) {
                searchUrl += '/' + $scope.type.path;
            }
            searchUrl += '/search';


            var searchParams = {
                limit: 10,
                statuses:'all',//['active', 'archived', 'draft']
            };


            if ($scope.config.searchInheritable) {
                searchParams.searchInheritable = true;
            }

            ////////////////////////

            return $http.get(searchUrl + '/' + val, {
                ignoreLoadingBar: true,
                params: searchParams,
            }).then(function(response) {

                var results = response.data;

                return _.reduce(results, function(filtered, item) {
                    var exists = _.some($scope.selectedItems.items, {
                        '_id': item._id
                    });
                    if (!exists) {
                        filtered.push(item);
                    }
                    return filtered;
                }, []);

            });
        }
    };

});